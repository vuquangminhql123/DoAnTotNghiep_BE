// -------------------------------------------------------------------------------------------------
// Copyright (c) Johan Boström. All rights reserved.
// Licensed under the Apache License, Version 2.0. See LICENSE in the project root for license information.
// -------------------------------------------------------------------------------------------------

using DigitalID.API;
using DigitalID.Business;
using DigitalID.Data;
using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Text;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
namespace DigitalID.API
{
    public static class CustomAuthExtensions
    {
        public static AuthenticationBuilder AddCustomAuth(this AuthenticationBuilder builder, Action<CustomAuthOptions> configureOptions)
        {
            return builder.AddScheme<CustomAuthOptions, CustomAuthHandler>("Custom Scheme", "Custom Auth", configureOptions);
        }
    }
    public class CustomAuthOptions : AuthenticationSchemeOptions
    {
        public CustomAuthOptions()
        {

        }
    }
    internal class CustomAuthHandler : AuthenticationHandler<CustomAuthOptions>
    {
        private readonly string _serviceValidate = Utils.GetConfig("Authentication:WSO2:Uri");
        private readonly string _clientId = Utils.GetConfig("Authentication:WSO2:Clientid");
        private readonly string _clientSecret = Utils.GetConfig("Authentication:WSO2:Secret");
        private readonly string _redirectUri = Utils.GetConfig("Authentication:WSO2:Redirecturi");


        public CustomAuthHandler(IOptionsMonitor<CustomAuthOptions> options, ILoggerFactory logger, UrlEncoder encoder, ISystemClock clock) : base(options, logger, encoder, clock)
        {
            // store custom services here...
        }
        protected override async Task<AuthenticateResult> HandleAuthenticateAsync()
        {
            string authHeader = Request.Headers["Authorization"];
            AuthenticateResult result = null;
            #region WSO2
            if (Utils.GetConfig("Authentication:WSO2:Enable") == "true")
            {
                if (authHeader != null && authHeader.StartsWith("Bearer "))
                {
                    string accessToken = Guid.NewGuid().ToString();
                    // Get the token
                    var token = authHeader.Split(' ', 2, StringSplitOptions.RemoveEmptyEntries)[1]?.Trim();
                    accessToken = token;
                    Uri refreshTokenUri = new Uri(_serviceValidate + "oauth2/userinfo")
                    .AddQuery("schema", "openid");

                    var webRequest = (System.Net.HttpWebRequest)WebRequest.Create(refreshTokenUri);
                    webRequest.Method = "POST";
                    webRequest.ContentType = "application/x-www-form-urlencoded";
                    webRequest.Accept = "application/json, text/javascript, */*";
                    webRequest.Headers.Add("Authorization", "Bearer " + accessToken);

                    using (var jsonResponse = (HttpWebResponse)webRequest.GetResponse())
                    {
                        var jsonStream = jsonResponse.GetResponseStream();

                        MemoryStream ms = new MemoryStream();
                        jsonStream.CopyTo(ms);
                        ms.Position = 0;
                        HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
                        response.Content = new StreamContent(ms);
                        response.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
                        await response.Content.ReadAsStringAsync();

                        var claimsWSO2 = new List<Claim>();
                        claimsWSO2.Add(new Claim(ClaimTypes.NameIdentifier, UserConstants.AdministratorId.ToString()));
                        ClaimsIdentity claimsIdentityWSO2 = new ClaimsIdentity(claimsWSO2, "Bear");
                        ClaimsPrincipal claimsPrincipalWSO2 = new ClaimsPrincipal(claimsIdentityWSO2);

                        result = AuthenticateResult.Success(new AuthenticationTicket(claimsPrincipalWSO2,
                                new AuthenticationProperties(), "Bear"));
                        return result;
                    }
                }
            }
            #endregion
            #region APIKEY
            if (Utils.GetConfig("Authentication:apikey:Enable") == "true")
            {
                if (authHeader != null && authHeader.StartsWith("APIKEY "))
                {
                    string accessToken = Guid.NewGuid().ToString();
                    // Get the token
                    var token = authHeader.Split(' ', 2, StringSplitOptions.RemoveEmptyEntries)[1]?.Trim();
                    // validatetoken

                    // var dateTimeExpired = TokenHelpers.GetDateTimeExpired(token, Utils.GetConfig("Authentication:apikey:Key"));
                    // if (dateTimeExpired.HasValue && dateTimeExpired.Value.CompareTo(DateTime.Now) != -1)
                    // {
                    var objectId = TokenHelpers.GetKeyFromBasicToken(token);
                    if (objectId != null)
                    {
                        var claimsAPIKEY = new List<Claim>();
                        // claimsAPIKEY.Add(new Claim(ClaimTypes.Sid, objectId.ToString()));
                        ClaimsIdentity claimsIdentityAPIKEY = new ClaimsIdentity(claimsAPIKEY, "Bear");
                        ClaimsPrincipal claimsPrincipalAPIKEY = new ClaimsPrincipal(claimsIdentityAPIKEY);
                        result = AuthenticateResult.Success(new AuthenticationTicket(claimsPrincipalAPIKEY, new AuthenticationProperties(), "APIKEY"));
                    }

                    // }

                    return result;
                }
            }
            #endregion
            #region Basic
            if (Utils.GetConfig("Authentication:Basic:Enable") == "true")
            {
                if (authHeader != null && authHeader.StartsWith("Basic "))
                {
                    // Get the encoded username and password
                    var encodedUsernamePassword = authHeader.Split(' ', 2, StringSplitOptions.RemoveEmptyEntries)[1]?.Trim();
                    // Decode from Base64 to string
                    var decodedUsernamePassword = Encoding.UTF8.GetString(Convert.FromBase64String(encodedUsernamePassword));
                    // Split username and password
                    var username = decodedUsernamePassword.Split(':', 2)[0];
                    var password = decodedUsernamePassword.Split(':', 2)[1];
                    // Check if login is correct
                    if (username == Utils.GetConfig("Authentication:AdminUser") && password == Utils.GetConfig("Authentication:AdminPassWord"))
                    {

                        var claimsBasic = new List<Claim>();
                        claimsBasic.Add(new Claim(ClaimTypes.NameIdentifier, UserConstants.AdministratorId.ToString()));
                        ClaimsIdentity claimsIdentityBasic = new ClaimsIdentity(claimsBasic, "Basic");
                        ClaimsPrincipal claimsPrincipalBasic = new ClaimsPrincipal(claimsIdentityBasic);

                        result = AuthenticateResult.Success(new AuthenticationTicket(claimsPrincipalBasic,
                                                new AuthenticationProperties(), "Basic"));
                        return result;
                    }
                }
            }
            #endregion
            #region NoAuth
            if (Utils.GetConfig("Authentication:NoAuth:Enable") == "true")
            {
                var claims = new List<Claim>();
                // claims.Add(new Claim(ClaimTypes.NameIdentifier, UserConstants.AdministratorId.ToString()));
                ClaimsIdentity claimsIdentity = new ClaimsIdentity(claims, "NONE_AUTH");
                ClaimsPrincipal claimsPrincipal = new ClaimsPrincipal(claimsIdentity);

                result = AuthenticateResult.Success(new AuthenticationTicket(claimsPrincipal,
                                    new AuthenticationProperties(), "NONE_AUTH"));
                return result;
            }
            #endregion
            #region JWT
            if (Utils.GetConfig("Authentication:Jwt:Enable") == "true")
            {
                if (authHeader != null && authHeader.StartsWith("Bearer "))
                {
                    string accessToken = Guid.NewGuid().ToString();
                    // Get the token
                    var token = authHeader.Split(' ', 2, StringSplitOptions.RemoveEmptyEntries)[1]?.Trim();
                    // validatetoken
                    var handerJwt = new JwtSecurityTokenHandler();
                    var tokenInfo = handerJwt.ReadJwtToken(token);
                    SecurityToken validatedToken;
                    handerJwt.ValidateToken(token, new TokenValidationParameters()
                    {
                        ValidateIssuer = true,
                        ValidateAudience = true,
                        ValidateLifetime = true,
                        ValidateIssuerSigningKey = true,
                        ValidIssuer = Utils.GetConfig("Authentication:Jwt:Issuer"),
                        ValidAudience = Utils.GetConfig("Authentication:Jwt:Issuer"),
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Utils.GetConfig(("Authentication:Jwt:Key"))))
                    }, out validatedToken);
                    if (validatedToken != null
                    && validatedToken.Issuer == Utils.GetConfig("Authentication:Jwt:Issuer")
                    && validatedToken.ValidFrom.CompareTo(DateTime.UtcNow) < 0
                    && validatedToken.ValidTo.CompareTo(DateTime.UtcNow) > 0)
                    {
                        var claimsJwt = new List<Claim>();
                        var userId = tokenInfo.Claims.Where(x => x.Type == "nameid").FirstOrDefault().Value;

                        //Check phân quyền API
                        //bool isAcceptAPI = UserHandler.IsApiPermission(userId, Request.Path.ToString());
                        //if (!isAcceptAPI)
                        //return AuthenticateResult.Fail("You not accepted API: " + Request.Path.ToString());

                        claimsJwt.Add(new Claim(ClaimTypes.NameIdentifier, userId.ToString()));
                        claimsJwt.AddRange(tokenInfo.Claims);
                        // claims.Add(new Claim(ClaimTypes.NameIdentifier, UserConstants.AdministratorId.ToString()));
                        ClaimsIdentity claimsIdentityJwt = new ClaimsIdentity(claimsJwt, "Jwt");
                        ClaimsPrincipal claimsPrincipalJwt = new ClaimsPrincipal(claimsIdentityJwt);

                        result = AuthenticateResult.Success(new AuthenticationTicket(claimsPrincipalJwt,
                                            new AuthenticationProperties(), "Jwt"));
                        return result;
                    }

                }
            }
            #endregion
            return AuthenticateResult.Fail("Không xác thực");

        }
    }
}