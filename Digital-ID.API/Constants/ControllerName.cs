namespace DigitalID.API.Constants
{
    public static class ControllerName
    {
        public const string Status = nameof(Status);
        public const string Home = nameof(Home);
    }
}