using DigitalID.Business;
using NetCore.Business;
using IUserHandler = DigitalID.Business.IUserHandler;
using UserHandler = DigitalID.Business.UserHandler;

namespace DigitalID.API
{
    using DigitalID.API.Services;
    using DigitalID.Data;
    using DigitalID.WF;
    using DigitalID.WF.DataAccess;
    using DigitalID.WF.Implementation;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;

    /// <summary>
    /// <see cref="IServiceCollection"/> extension methods add project services.
    /// </summary>
    /// <remarks>
    /// AddSingleton - Only one instance is ever created and returned.
    /// AddScoped - A new instance is created and returned for each request/response cycle.
    /// AddTransient - A new instance is created and returned each time.
    /// </remarks>
    public static class ProjectServiceCollectionExtensions
    {
        public static IServiceCollection AddProjectServices(this IServiceCollection services) =>
            services

                .AddSingleton<IEmailHandler, EmailHandler>()
                .AddSingleton<ICallStoreHelper, CallStoreHelper>()

        #region BSD
                .AddSingleton<IParameterHandler, ParameterHandler>()
                .AddSingleton<INavigationHanlder, NavigationHandler>()
                .AddSingleton<ILogHandler, LogHandler>()

                .AddSingleton<ITaxonomyTermsHandler, DbTaxonomyTermsHandler>()
                .AddSingleton<ITaxonomyVocabulariesHandler, DbTaxonomyVocabulariesHandler>()
                .AddSingleton<ITaxonomyVocabularyHandler, DbTaxonomyVocabularyHandler>()
                .AddSingleton<ITaxonomyVocabularyTypesHandler, TaxonomyVocabularyTypesHandler>()
                .AddSingleton<ICatalogItemHandler, DbCatalogItemHandler>()
                .AddSingleton<ICatalogHandler, DbCatalogHandler>()
                .AddSingleton<IFieldHandler, DbFieldHandler>()
                .AddSingleton<IMetadataFieldHandler, DbMetadataFieldHandler>()
                .AddSingleton<IMetadataTemplateHandler, DbMetadataTemplateHandler>()
        #endregion

        #region SYS
                .AddSingleton<IApplicationHandler, ApplicationHandler>()
        #endregion

        #region IDM
                .AddSingleton<IRightHandler, RightHandler>()
                .AddSingleton<IRoleHandler, RoleHandler>()               
                .AddSingleton<IUserHandler, UserHandler>()
                .AddSingleton<IRightMapRoleHandler, RightMapRoleHandler>()
                .AddSingleton<IRightMapUserHandler, RightMapUserHandler>()
                .AddSingleton<IUserMapRoleHandler, UserMapRoleHandler>()
                .AddSingleton<IUserMapDVHandler, UserMapDVHandler>()
                .AddSingleton<IDeviceHandler, DeviceHandler>()
                .AddSingleton<ITokenHandler, TokenHandler>()
        #endregion

        #region CMS
                .AddSingleton<IPositionHandler, PositionHandler>()
                .AddSingleton<IOrganizationHandler, OrganizationHandler>()
                
                .AddSingleton<ISignProfileHandler, SignProfileHandler>()
                .AddSingleton<ISignHandler, SignHandler>()
                .AddSingleton<ICertificateHandler, CertificateHandler>()
        #endregion

        #region Workflow
                .AddSingleton<IWorkflowInboxHandler, WorkflowInboxHandler>()
                .AddSingleton<IWorkflowSchemeAttributeHandler, WorkflowSchemeAttributeHandler>()
                .AddSingleton<IWorkflowSchemeVersionHandler, WorkflowSchemeVersionHandler>()
                .AddSingleton<IWorkflowGlobalParameterHandler, WorkflowGlobalParameterHandler>()
                .AddSingleton<IWorkflowDocumentHandler, WorkflowDocumentHandler>()
                .AddSingleton<IWorkflowSchemeHandler, WorkflowSchemeHandler>()
                .AddSingleton<IWorkflowHandler, WorkflowHandler>()
                .AddSingleton<IWorkflowApplicationHandler, WorkflowApplicationHandler>()
                .AddSingleton<IWorkflowDocumentHistoryHandler, WorkflowDocumentHistoryHandler>()
                .AddSingleton<IWorkflowDocumentAttachmentHandler, WorkflowDocumentAttachmentHandler>()
                .AddSingleton<IPersistenceProviderContainer, PersistenceProviderContainer>()
                .AddSingleton<IFormHandler, FormHandler>()
                .AddSingleton<IFormMasterHandler, FormMasterHandler>()
                .AddSingleton<IFormControlHandler, FormControlHandler>()
        #endregion

        

                .AddSingleton<IClockService, ClockService>();

        public static IServiceCollection RegisterDataContextServiceComponents(this IServiceCollection services, IConfiguration configuration)
        {
            services.RegisterCacheComponents();
            var appSettings = configuration.GetSection(nameof(AppSettings)).Get<AppSettings>();
            services.AddDbContext<DataContext>(x => x.UseSqlServer(appSettings.DatabaseConnectionString), ServiceLifetime.Transient);

            #region Resource
            //services.AddTransient<IProvinceHandler, ProvinceHandler>();
            #endregion

            #region CMS
            services.AddTransient<IOrganizationV2Handler, OrganizationV2Handler>();
            #endregion

            #region CRM
            services.AddTransient<ICrmAccountHandler, CrmAccountHandler>();

            #endregion

            #region IDM
            services.AddTransient<IRoleV2Handler, RoleV2Handler>();
            #endregion

            #region eTicket
            services.AddTransient<ICheckinAccountHandler, CheckinAccountHandler>();

            #endregion

            services.AddTransient<IDashboardHandler, DashboardHandler>();

            #region ComputerManagement

            services.AddTransient<IAccountHandler, AccountHandler>();
            services.AddTransient<ICategoryHandler, CategoryHandler>();
            services.AddTransient<ITagHandler, TagHandler>();
            services.AddTransient<ISupplierHandler, SupplierHandler>();
            services.AddTransient<IVoucherHandler, VoucherHandler>();
            services.AddTransient<IProductHandler, ProductHandler>();
            services.AddTransient<IPictureHandler, PictureHandler>();
            services.AddTransient<ICategoryMetaHandler, CategoryMetaHandler>();
            services.AddTransient<ICategoryMetaProductHandler, CategoryMetaProductHandler>();
            services.AddTransient<ICartHandler, CartHandler>();
            services.AddTransient<IOrderHandler, OrderHandler>();
            services.AddTransient<IProductReviewHandler, ProductReviewHandler>();
            services.AddTransient<ICustomerHandler, CustomerHandler>();
            services.AddTransient<IBaseAddressHandler, BaseAddressHandler>();


            #endregion
            return services;
        }
    }


}