﻿using DigitalID.Data;
using Microsoft.AspNetCore.Mvc.Filters;
using Serilog;
using System.Net;

namespace DigitalID.API
{
    /// <summary>
    /// The global exception handler
    /// </summary>
    public class ExceptionHandlingFilter : IExceptionFilter
    {
        /// <summary>
        /// The method raise when uncatch exception have been throws.
        /// </summary>
        /// <param name="context">The exception context.</param>
        public void OnException(ExceptionContext context)
        {
            //context.Result = ObjectResultUtils.CreateErrorObjectResult(HttpStatusCode.BadRequest, context.Exception.Message, context.ModelState);

            ////var errorDetail = string.Join(",", context.ModelState.Select(x => x.Value.Errors.Select(x => x.ErrorMessage)));
            context.Result = Helper.TransformData(new ResponseError(Code.BadRequest, context.Exception.Message));

            context.HttpContext.Response.StatusCode = (int)HttpStatusCode.BadRequest;
            context.ExceptionHandled = true;
            Log.Error(context.Exception, string.Empty);
        }
    }
}
