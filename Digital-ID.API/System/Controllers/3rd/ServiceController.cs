﻿using DigitalID.Business;
using DigitalID.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace DigitalID.API
{
    /// <summary>
    /// Service cho hệ thống khác kết nối
    /// </summary>
    [ApiVersion("1.0")]
    [ApiController]
    [Route("api/v{api-version:apiVersion}/service")]
    [ApiExplorerSettings(GroupName = "System - ServiceController", IgnoreApi = true)]
    public class ServiceController : ControllerBase
    {
        private readonly IConfiguration _config;
        private readonly IUserHandler _userHandler;
        private readonly IApplicationHandler _applicationHandler;
        private readonly ISignHandler _signHandler;
        private readonly ICertificateHandler _certHandler;
        private readonly IDeviceHandler _deviceHandler;
        private readonly ITokenHandler _tokenHandler;
        private readonly IHubContext<SignRHub> _hubContext;
        private static readonly HttpClient Client = new HttpClient();

        public ServiceController(IConfiguration config,
            IUserHandler userHandler, IApplicationHandler applicationHandler,
            ISignHandler signHandler, ICertificateHandler certHandler,
            IDeviceHandler deviceHandler, ITokenHandler tokenHandler, IHubContext<SignRHub> hubContext)
        {
            _config = config;
            _userHandler = userHandler;
            _applicationHandler = applicationHandler;
            _signHandler = signHandler;
            _certHandler = certHandler;
            _deviceHandler = deviceHandler;
            _tokenHandler = tokenHandler;
            _hubContext = hubContext;
        }

        #region  Challenge
        /// <summary>
        /// Đăng nhập và lấy kết quả JWT token
        /// API dành cho phần mềm khác kết nối vào hệ thống
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [Route("challenge")]
        [AllowAnonymous, HttpPost]
        public ResponseV2<ChallengesOutputModel> SignChallenge([FromBody] ChallengesInputModel data)
        {
            try
            {
                var rt = _userHandler.SignChallenge(data);
                return rt;
            }
            catch (Exception ex)
            {
                return new ResponseV2<ChallengesOutputModel>(-1, "Lỗi: " + ex.Message, null);
            }
        }
        #endregion

        #region Cert
        /// <summary>
        /// Lấy cert theo user name
        /// </summary> 
        /// <returns></returns> 
        /// <response code="200">Thành công</response>
        [AllowAnonymous, HttpGet, Route("certbyuser")]
        public ResponseV2<CertificateClientAPIModel> GetCertBase64ForUser(string userName)
        {
            //if (Helper.ValidAuthen(Request))
            //{
            // Call service
            var result = _certHandler.GetCertBase64ForUser(userName);
            // Hander response
            return result;
            //}
            //else
            //{
            //return new ResponseV2<CertificateClientAPIModel>(0, "Token is expire", null);
            //}
        }
        #endregion

        #region Device token
        /// <summary>
        /// Scan QR Code
        /// </summary>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [AllowAnonymous, HttpPost, Route("deviceupdate")]
        public async Task<ResponseV2<BaseUserModel>> UpdateAsync([FromBody] DeviceCreateUpdateModel data)
        {
            try
            {
                //if (Helper.ValidAuthen(Request))
                //{
                if (data.UserId == null || data.UserId == Guid.Empty)
                    data.UserId = new Guid(User.Identity.Name);
                var rt = _deviceHandler.UpdateAsync(data, data.UserId);
                await _hubContext.Clients.All.SendAsync("SCANQRCODE", rt.Message, data.UserId);
                return rt;
                //}
                //else
                //{
                // return new ResponseV2<BaseUserModel>(0, "Token is expired", null);
                //}
            }
            catch (Exception ex)
            {
                return new ResponseV2<BaseUserModel>(-1, ex.Message, null);
            }

        }

        /// <summary>
        /// Lấy cert theo user name
        /// </summary> 
        /// <returns></returns> 
        /// <response code="200">Thành công</response>
        [Authorize, HttpGet, Route("devicebyuser")]
        public ResponseV2<string> GetDeviceTokenForUser(string userName)
        {
            //Check token from DB
            if (Helper.ValidAuthen(Request))
            {
                // Call service
                var result = _deviceHandler.GetDeviceTokenForUser(userName);
                // Hander response
                return result;
            }
            else
            {
                return new ResponseV2<string>(0, "Token is expire", "");
            }
        }
        #endregion

        #region  JWT
        /// <summary>
        /// Đăng nhập và lấy kết quả JWT token
        /// API dành cho phần mềm khác kết nối vào hệ thống
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        [Route("gettoken")]
        [AllowAnonymous, HttpPost]
        public ResponseV2<string> GetToken([FromBody] LoginModel login)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    if (string.IsNullOrWhiteSpace(login.ApplicationId))
                        return new ResponseV2<string>(0, "ApplicationId is null", "");
                    var checkApp = unitOfWork.GetRepository<SysApplication>().Find(x => x.Id == new Guid(login.ApplicationId));
                    if (checkApp == null)
                        return new ResponseV2<string>(0, "Not found Application", "");
                    var user = _userHandler.AuthenticationV2(login.Username, login.Password);
                    if (user.Code == Code.Success && user is ResponseObject<UserModel> userData)
                    {
                        //Danh sách ứng dụng
                        //var getApp = await _applicationHandler.GetDefautByUserIdAsync(userData.Data.Id);
                        var tokenString = IdmHelper.BuildToken(userData.Data, login.RememberMe, 31536000);//~ 1 năm Time To Live Token pass Check Authen on APP

                        #region Read token and save to DB
                        var handerJwt = new JwtSecurityTokenHandler();
                        SecurityToken validatedToken;
                        handerJwt.ValidateToken(tokenString, new TokenValidationParameters()
                        {
                            ValidateIssuer = true,
                            ValidateAudience = true,
                            ValidateLifetime = true,
                            ValidateIssuerSigningKey = true,
                            ValidIssuer = Utils.GetConfig("Authentication:Jwt:Issuer"),
                            ValidAudience = Utils.GetConfig("Authentication:Jwt:Issuer"),
                            IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Utils.GetConfig(("Authentication:Jwt:Key")))),
                        }, out validatedToken);
                        TokenCreateUpdateModel modelToken = new TokenCreateUpdateModel();
                        modelToken.AccessToken = tokenString;

                        modelToken.ExpiryTime = validatedToken.ValidFrom.AddMinutes(30);//30 munite Time To Live Token - lưu DB
                        modelToken.IsActive = true;
                        modelToken.IssuedAt = validatedToken.ValidFrom;
                        modelToken.Issuer = checkApp.Name;
                        modelToken.TokenType = "JWT";
                        modelToken.UserId = userData.Data.Id;
                        modelToken.UserName = userData.Data.UserName;

                        //Delete token by user before create new token
                        _tokenHandler.DeleteByUser(userData.Data.Id);

                        //Add new token
                        var rt = _tokenHandler.CreateAsync(modelToken, new Guid(login.ApplicationId), userData.Data.Id);
                        if (rt.Result.Code != Code.Success)
                            return new ResponseV2<string>(0, "Create token fail", "");
                        #endregion

                        return new ResponseV2<string>(1, "Success", tokenString);
                    }
                    else
                    {
                        return new ResponseV2<string>(0, "Không tìm thấy thông tin tài khoản", "");
                    }
                }
            }
            catch (Exception ex)
            {
                return new ResponseV2<string>(-1, "Lỗi: " + ex.Message, "");
            }
        }
        #endregion

        #region Veryfi passphase
        [AllowAnonymous, HttpPost, Route("verifykey")]
        public ResponseV2<bool> VerifyKey([FromBody] KeyVerifyInputModel model)
        {
            if (!string.IsNullOrEmpty(model.userName))
            {
                using (var unitOfWork = new UnitOfWork())
                { 
                    
                }
            }
            var data = _signHandler.CheckKey(model.key, model.userId);
            if (data.Status == 1)
                return new ResponseV2<bool>(1, "Success", true);
            else
                return new ResponseV2<bool>(0, "Fail", false);
        }
        #endregion

        #region Sign
        /// <summary>
        /// Ký file PDF
        /// </summary>
        /// <param name="model">Dữ liệu</param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [Authorize, HttpPost, Route("signpdf")]
        public async Task<OutputFromSignMultiTrustCAFormat> SignPDFAsync([FromBody] DataInputSignPDFnCipher model)
        {
            var actorId = new Guid(User.Identity.Name);
            var result = await _signHandler.SignUrlPdfForThirdpartyFromFileContentBase64(model, actorId);
            // Hander response
            return result;
        }

        /// <summary>
        /// Ký file PDF from PFX
        /// </summary>
        /// <param name="model">Dữ liệu</param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [Authorize, HttpPost, Route("signpdf-son")]
        public async Task<OutputFromSignMultiTrustCAFormat> SignPDFSonAsync([FromBody] DataInputSignPDFnCipher model)
        {
            if (Helper.ValidAuthen(Request))
            {
                var actorId = new Guid(User.Identity.Name);
                var result = await _signHandler.SignUrlPdfForThirdpartyFromFileContentBase64WithPfx(model, actorId);
                // Hander response
                return result;
            }
            else
            {
                return new OutputFromSignMultiTrustCAFormat() { Code = 0, Data = null, Message = "Token is expire" };
            }
        }

        /// <summary>
        /// Ký file XML
        /// </summary>
        /// <param name="model">Dữ liệu</param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [Authorize, HttpPost, Route("signxml")]
        public async Task<OutputFromSignMultiTrustCA> SignXMLAsync([FromBody] DataInputSignXMLnCipher model)
        {
            var actorId = new Guid(User.Identity.Name);
            var result = await _signHandler.SignUrlXMLForThirdparty(model, actorId);
            // Hander response
            return result;
        }

        /// <summary>
        /// Ký file XML voi Base64Content
        /// </summary>
        /// <param name="model">Dữ liệu</param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [Authorize, HttpPost, Route("signxmlv2")]
        public async Task<OutputFromSignMultiTrustCA> SignXMLAsyncV2([FromBody] DataInputSignXMLnCipherV2 model)
        {
            var actorId = new Guid(User.Identity.Name);
            //switch 2 server
            //var result = await _signHandler.CheckAndSetLinkSign(model, actorId);
            //none switch
            var result = await _signHandler.SignUrlXMLForThirdpartyV2FromBase64(model, actorId);
            // Hander response
            return result;
        }

        /// <summary>
        /// Ký điện tử file PDF
        /// </summary>
        /// <param name="model">Dữ liệu</param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [Authorize, HttpPost, Route("signpdf-digital")]
        public OutputFromSignMultiDigital SignDigitalPDFAsync([FromBody] DataInputSignPDFDigital model)
        {
            var actorId = new Guid(User.Identity.Name);
            var result = _signHandler.SignDigitalPdf(model, actorId);
            // Hander response
            return result;
        }
        #endregion

        #region Verify
        /// <summary>
        /// Verify PDF
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Authorize, HttpPost, Route("verifypdf")]
        public async Task<DataOutputVerifySign> VerifyPDFAsync([FromBody] DataInputVerifySign model)
        {
            var actorId = new Guid(User.Identity.Name);
            var result = await _signHandler.VerifyPdf(model, actorId);
            // Hander response
            return result;
        }

        /// <summary>
        /// Verify XML
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Authorize, HttpPost, Route("verifyxml")]
        public async Task<DataOutputVerifySign> VerifyXMLAsync([FromBody] DataInputVerifySign model)
        {
            var actorId = new Guid(User.Identity.Name);
            var result = await _signHandler.VerifyXML(model, actorId);
            // Hander response
            return result;
        }
        #endregion        
    }
}