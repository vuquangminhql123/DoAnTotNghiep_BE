﻿using DigitalID.Business;
using DigitalID.Data;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Serilog;
using System;
using System.Collections.Generic;

namespace DigitalID.API
{
    /// <inheritdoc />
    /// <summary>
    /// Module danh mục catalog
    /// </summary>
    [ApiVersion("1.0")]
    [Route("")]
    [ApiExplorerSettings(GroupName = "12: Catalog", IgnoreApi = true)]
    public class CatalogController : ControllerBase
    {
        private readonly ICatalogHandler _catalogHandler;
        private readonly ICatalogItemHandler _catalogItemHandler;
        private readonly IMetadataTemplateHandler _metadataTemplateHandler;

        public CatalogController(ICatalogHandler catalogHandler, ICatalogItemHandler catalogItemHandler, IMetadataTemplateHandler metadataTemplateHandler)
        {
            _catalogHandler = catalogHandler;
            _catalogItemHandler = catalogItemHandler;
            _metadataTemplateHandler = metadataTemplateHandler;
        }

        #region Catalog controller CRUD
        /// <summary>
        /// Lấy danh sách danh mục theo danh mục Id
        /// </summary>
        /// <param name="catalogId"> Id danh mục</param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/catalogs/catalogmaster/{catalogId}")]
        public OldResponse<CatalogMasterModel> GetCatalogById([FromRoute]Guid catalogId)
        {
            

            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            var actorId = requestInfo.UserId;
            var appId = requestInfo.ApplicationId;
            _catalogHandler.init(appId, actorId);
            var catalog = _catalogHandler.GetCatalogMasterById(catalogId);
            return catalog;
        }
        /// <summary>
        /// Lấy danh sách Catalog theo bộ lọc
        /// </summary>
        /// <param name="stringFilter"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/catalogs/catalogmaster")]
        public OldResponse<List<CatalogMasterModel>> GetFilter(string stringFilter)
        {
            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            var actorId = requestInfo.UserId;
            var appId = requestInfo.ApplicationId;
            _catalogHandler.init(appId, actorId);
            var filter = new CatalogMasterQueryFilter();
            try
            {
                filter = JsonConvert.DeserializeObject<CatalogMasterQueryFilter>(stringFilter);
            }
            catch (Exception ex)
            {
                Log.Error("api/catalogs/catalogmaster, ex:" + ex.Message);
            }
            return _catalogHandler.GetFilter(filter);
        }

        /// <summary>
        /// Get các danh mục cùng cha
        /// </summary>
        /// <param name="parentId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/catalogs/catalogmaster/parent={parentId}")]
        public OldResponse<List<CatalogMasterModel>> GetCatalogsInParent([FromRoute]Guid parentId)
        {
            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            var actorId = requestInfo.UserId;
            var appId = requestInfo.ApplicationId;
            _catalogHandler.init(appId, actorId);
            var catalog = _catalogHandler.GetCatalogsInParent(parentId);
            return catalog;
        }

        //get in tree
        /// <summary>
        /// Get danh mục theo cây
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("api/catalogs/catalogmaster/tree")]
        public OldResponse<List<CatalogMasterClientModel>> GetCatalogTree()
        {
            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            var actorId = requestInfo.UserId;
            var appId = requestInfo.ApplicationId;
            _catalogHandler.init(appId, actorId);
            return _catalogHandler.GetCatalogViewTree();
        }

        /// <summary>
        /// Create new catalog master
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/catalogs/catalogmaster")]
        public OldResponse<CatalogMasterModel> Create([FromBody]CatalogMasterCreateRequestModel request)
        {
            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            var actorId = requestInfo.UserId;
            var appId = requestInfo.ApplicationId;
            _catalogHandler.init(appId, actorId);
            var newCatalog = _catalogHandler.Create(request);
            return newCatalog;
        }
        /// <summary>
        /// Update catalog master
        /// </summary>
        /// <param name="catalogmasterId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("api/catalogs/catalogmaster/{catalogmasterId}")]
        public OldResponse<CatalogMasterModel> UpdateCatalog([FromRoute]Guid catalogmasterId, [FromBody]CatalogMasterUpdateRequestModel request)
        {
            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            var actorId = requestInfo.UserId;
            var appId = requestInfo.ApplicationId;
            _catalogHandler.init(appId, actorId);
            request.CatalogMasterId = catalogmasterId;
            var updateCatalog = _catalogHandler.Update(request);
            return updateCatalog;
        }

        /// <summary>
        /// Xóa danh mục theo Id
        /// </summary>
        /// <param name="catalogmasterId"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("api/catalogs/catalogmaster/{catalogmasterId}")]
        public OldResponse<CatalogMasterDeleteResponseModel> DeleteById([FromRoute]Guid catalogmasterId)
        {
            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            var actorId = requestInfo.UserId;
            var appId = requestInfo.ApplicationId;
            _catalogHandler.init(appId, actorId);
            var deleteRepo = _catalogHandler.Delete(catalogmasterId);
            return deleteRepo;
        }

        /// <summary>
        /// Xóa nhiều danh mục
        /// </summary>
        /// <param name="listCatalogMasterId"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("api/catalogs/catalogmaster")]
        public OldResponse<IList<CatalogMasterDeleteResponseModel>> DeleteMany([FromBody]IList<Guid> listCatalogMasterId)
        {
            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            var actorId = requestInfo.UserId;
            var appId = requestInfo.ApplicationId;
            _catalogHandler.init(appId, actorId);
            var result = _catalogHandler.DeleteMany(listCatalogMasterId);
            return result;
        }
        #endregion

        #region Other 
        /// <summary>
        /// Lấy về thông tin metadata template
        /// </summary>
        /// <param name="catalogId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/catalogs/catalogmaster/{catalogId}/template")]
        public OldResponse<MetadataTemplateModel> GetTemplateData([FromRoute]Guid catalogId)
        {
            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            var actorId = requestInfo.UserId;
            var appId = requestInfo.ApplicationId;
            _catalogHandler.init(appId, actorId);
            //step1 get MetadataId

            var catalog = _catalogHandler.GetCatalogMasterById(catalogId);
            if (catalog.Status == 1)
            {
                return _metadataTemplateHandler.GetById(catalog.Data.MetadataTemplateId);
            }
            else
            {
                return new OldResponse<MetadataTemplateModel>(0, "catalogId" + catalogId + " not found", null);
            }
        }
        #endregion

        #region hien thi danh muc 
        /// <summary>
        /// get item tree
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/catalogs/catalogmaster/{id}/itemtree")]
        public OldResponse<List<CatalogItemTreeModel>> GetCatalogItemTree(Guid id)
        {
            var filter = new CatalogItemQueryFilter();
            filter.CatalogMasterId = id;
            return _catalogItemHandler.GetFilterItemTree(filter);
        }

        /// <summary>
        /// get item tree by catalog master code
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/catalogs/catalogmaster/{code}/itemstree")]
        public OldResponse<List<CatalogItemTreeModel>> GetCatalogItemTreeByCode(string code)
        {
            var filter = new CatalogItemQueryFilter();
            filter.CatalogMasterCode = code;
            return _catalogItemHandler.GetFilterItemTree(filter);
        }

        /// <summary>
        /// get item list
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/catalogs/catalogmaster/{id}/itemslist")]
        public OldResponse<IList<CatalogItemModel>> GetCatalogItemList(Guid id)
        {
            var filter = new CatalogItemQueryFilter();
            filter.CatalogMasterId = id;
            if (filter.Status == null)
                filter.Status = true;
            filter.Order = CatalogItemQueryOrder.ORDER_ASC;
            return _catalogItemHandler.GetFilter(filter);
        }
        /// <summary>
        /// get item by code
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/catalogs/catalogmaster/{code}/itemlist")]
        public OldResponse<IList<CatalogItemModel>> GetCatalogItemList(string code)
        {
            var filter = new CatalogItemQueryFilter();
            filter.CatalogMasterCode = code;
            if (filter.Status == null)
                filter.Status = true;
            filter.Order = CatalogItemQueryOrder.ORDER_ASC;
            return _catalogItemHandler.GetFilter(filter);
        }

        /// <summary>
        /// get item by code
        /// </summary>
        /// <param name="code">catalog code</param>
        /// <param name="stringFilter">string filter</param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/catalogs/catalogmaster/{code}/itemlistfilter/")]
        public OldResponse<IList<CatalogItemModel>> GetCatalogItemListFilter(string code, string stringFilter)
        {
            var filter = new CatalogItemQueryFilter();
            try
            {
                filter = JsonConvert.DeserializeObject<CatalogItemQueryFilter>(stringFilter);
            }
            catch (Exception ex)
            {
                Log.Error("api/catalogs/catalogitem, ex:" + ex.Message);
            }
            filter.CatalogMasterCode = code;
            if (filter.Status == null)
                filter.Status = true;
            filter.Order = CatalogItemQueryOrder.ORDER_ASC;

            return _catalogItemHandler.GetFilter(filter);
        }
        /// <summary>
        /// Get item by code base model
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/catalogs/catalogmaster/{code}/itemlistbase")]
        public List<BaseCatalogItemModel> GetCatalogItemListBase(string code)
        {
            var result = new List<BaseCatalogItemModel>();
            var filter = new CatalogItemQueryFilter();
            filter.CatalogMasterCode = code;
            if (filter.Status == null)
                filter.Status = true;
            var filterResult = _catalogItemHandler.GetFilter(filter);
            if (filterResult.DataCount > 0)
            {
                foreach (var item in filterResult.Data)
                {
                    var newItem = new BaseCatalogItemModel()
                    {
                        CatalogItemId = item.CatalogItemId,
                        CatalogMasterId = item.CatalogMasterId,
                        Code = item.Code,
                        Description = item.Description,
                        Status = item.Status,
                        Level = item.Level,
                        Name = item.Name,
                        Order = item.Order,
                        ParentTermId = item.ParentTermId,
                        TermId = item.TermId
                    };
                    result.Add(newItem);
                }
            }

            return result;
        }

        #endregion
        //GetFilterItemByLevel
        /// <summary>
        /// get item list
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/catalogs/catalogmaster/{id}/itembylevel")]
        public OldResponse<List<BaseCatalogItemModel>> GetCatalogItemByLevel(Guid id)
        {
            var filter = new CatalogItemQueryFilter();
            filter.CatalogMasterId = id;
            return _catalogItemHandler.GetFilterItemByLevel(filter);
        }

        /// <summary>
        /// GetCatalogItemByMetadata
        /// </summary>
        /// <param name="catalogMasterCode"></param>
        /// <param name="metadataCode"></param>
        /// <param name="metadataId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/catalogs/catalogmaster/{catalogMasterCode}/{metadataCode}/{metadataId}")]
        public OldResponse<List<BaseCatalogItemModel>> GetCatalogItemByMetadata(string catalogMasterCode, string metadataCode, Guid metadataId)
        {
            return _catalogItemHandler.GetAllByCodeAndParent(catalogMasterCode, metadataCode, metadataId);
        }

        /// <summary>
        /// GetAllByAtribute
        /// </summary>
        /// <param name="catalogMasterCode"></param>
        /// <param name="metadataCode"></param>
        /// <param name="stringFilter"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/catalogs/catalogmaster/filter-metadata/{catalogMasterCode}/{metadataCode}")]
        public OldResponse<List<BaseCatalogItemModel>> GetAllByAtribute(string catalogMasterCode, string metadataCode, string stringFilter)
        {
            var filter = new CatalogItemAttributeModel();
            try
            {
                filter = JsonConvert.DeserializeObject<CatalogItemAttributeModel>(stringFilter);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
            }
            return _catalogItemHandler.GetAllByAtribute(catalogMasterCode, metadataCode, filter, true, CatalogItemQueryOrder.ORDER_ASC);
        }
    }
}