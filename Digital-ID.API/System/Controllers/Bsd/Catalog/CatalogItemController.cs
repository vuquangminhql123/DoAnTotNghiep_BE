﻿using DigitalID.Business;
using DigitalID.Data;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Serilog;
using System;
using System.Collections.Generic;

namespace DigitalID.API
{
    /// <inheritdoc />
    /// <summary>
    /// Module danh mục catalog item
    /// </summary>
    [ApiVersion("1.0")]
    [Route("")]
    [ApiExplorerSettings(GroupName = "11: Catalog Item", IgnoreApi = true)]
    public class CatalogItemController : ControllerBase
    {
        private readonly ICatalogItemHandler _catalogItemHandler;

        public CatalogItemController(ICatalogItemHandler catalogItemHandler)
        {
            _catalogItemHandler = catalogItemHandler;
        }

        #region CRUD
        /// <summary>
        /// Lấy danh sách theo bộ lọc
        /// </summary>
        /// <param name="stringFilter"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/catalogitem")]
        public OldResponse<IList<CatalogItemModel>> GetFilter([FromQuery]string stringFilter)
        {
            // Get Token Info
            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            var actorId = requestInfo.UserId;
            var appId = requestInfo.ApplicationId;
            _catalogItemHandler.init(appId, actorId);
            var filter = new CatalogItemQueryFilter();
            try
            {
                filter = JsonConvert.DeserializeObject<CatalogItemQueryFilter>(stringFilter);
                if (filter.Status == null)
                    filter.Status = true;
                else
                    filter.Status = null;
            }
            catch (Exception ex)
            {
                Log.Error("api/catalogitem , ex:" + ex.Message);
            }
            var result = _catalogItemHandler.GetFilter(filter);
            return result;
        }
        /// <summary>
        /// Lấy theo Id
        /// </summary>
        /// <param name="catalogitemId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/catalogitem/{catalogitemId}")]
        public OldResponse<CatalogItemModel> GetById([FromRoute]Guid catalogitemId)
        {
            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            var actorId = requestInfo.UserId;
            var appId = requestInfo.ApplicationId;
            _catalogItemHandler.init(appId, actorId);

            var result = _catalogItemHandler.GetById(catalogitemId);
            return result;
        }
        /// <summary>
        /// Lấy data theo  Id
        /// </summary>
        /// <param name="catalogitemId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/catalogitem/{catalogitemId}/data")]
        public OldResponse<IList<CatalogItemAttributeModel>> GetAtributeById([FromRoute]Guid catalogitemId)
        {
            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            var actorId = requestInfo.UserId;
            var appId = requestInfo.ApplicationId;
            _catalogItemHandler.init(appId, actorId);

            var result = _catalogItemHandler.GetAtributeById(catalogitemId);
            return result;
        }

        /// <summary>
        /// Create new
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/catalogitem")]
        public OldResponse<CatalogItemModel> Create([FromBody]CatalogItemCreateRequestModel request)
        {
            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            var actorId = requestInfo.UserId;
            var appId = requestInfo.ApplicationId;
            _catalogItemHandler.init(appId, actorId);

            var result = _catalogItemHandler.Create(request);
            return result;
        }
        /// <summary>
        /// Update
        /// </summary>
        /// <param name="catalogitemId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("api/catalogitem/{catalogitemId}")]
        public OldResponse<CatalogItemModel> Update([FromRoute]Guid catalogitemId, [FromBody]CatalogItemUpdateRequestModel request)
        {
            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            var actorId = requestInfo.UserId;
            var appId = requestInfo.ApplicationId;
            _catalogItemHandler.init(appId, actorId);
            request.CatalogItemId = catalogitemId;
            var result = _catalogItemHandler.Update(request);
            return result;
        }
        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="catalogitemId"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("api/catalogitem/{catalogitemId}")]
        public OldResponse<CatalogItemDeleteResponseModel> Delete([FromRoute]Guid catalogitemId)
        {
            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            var actorId = requestInfo.UserId;
            var appId = requestInfo.ApplicationId;
            _catalogItemHandler.init(appId, actorId);

            var result = _catalogItemHandler.Delete(catalogitemId);
            return result;
        }
        /// <summary>
        /// Delete many
        /// </summary>
        /// <param name="ListCatalogItemId"></param> 
        /// <returns></returns>
        [HttpPost]
        [Route("api/catalogitem/delmany")]
        public OldResponse<IList<CatalogItemDeleteResponseModel>> DeleteMany([FromBody]IList<Guid> ListCatalogItemId)
        {
            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            var actorId = requestInfo.UserId;
            var appId = requestInfo.ApplicationId;
            _catalogItemHandler.init(appId, actorId);

            var result = _catalogItemHandler.DeleteMany(ListCatalogItemId);
            return result;
        }
        #endregion
    }
}

