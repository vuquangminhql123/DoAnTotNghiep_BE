﻿using DigitalID.Business;
using DigitalID.Data;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Serilog;
using System;
using System.Collections.Generic;

namespace DigitalID.API
{
    /// <inheritdoc />
    /// <summary>
    /// Module danh mục metadata field
    /// </summary>
    [ApiVersion("1.0")]
    [Route("")]
    [ApiExplorerSettings(GroupName = "10: Metadata field", IgnoreApi = true)]
    public class MetadataFieldController : ControllerBase
    {
        private readonly IMetadataFieldHandler _metadataFieldHandler;

        public MetadataFieldController(IMetadataFieldHandler metadataFieldHandler)
        {
            _metadataFieldHandler = metadataFieldHandler;
        }

        #region CRUD
        /// <summary>
        /// Lấy danh sách theo bộ lọc
        /// </summary>
        /// <param name="stringFilter"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/metadatafield")]
        public OldResponse<IList<MetadataFieldModel>> GetFilter([FromQuery]string stringFilter)
        {
            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            var actorId = requestInfo.UserId;
            var appId = requestInfo.ApplicationId;
            _metadataFieldHandler.init(appId, actorId);
            var filter = new MetadataFieldQueryFilter();
            try
            {
                filter = JsonConvert.DeserializeObject<MetadataFieldQueryFilter>(stringFilter);
            }
            catch (Exception ex)
            {
                Log.Error("api/metadatafield , ex:" + ex.Message);
            }
            var result = _metadataFieldHandler.GetFilter(filter);
            return result;
        }
        /// <summary>
        /// Lấy theo Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/metadatafield/{id}")]
        public OldResponse<MetadataFieldModel> GetById([FromRoute]Guid id)
        {
            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            var actorId = requestInfo.UserId;
            var appId = requestInfo.ApplicationId;
            _metadataFieldHandler.init(appId, actorId); 

            var result = _metadataFieldHandler.GetById(id);
            return result;
        }
        /// <summary>
        /// Create new
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/metadatafield")]
        public OldResponse<MetadataFieldModel> Create([FromBody]MetadataFieldCreateRequestModel request)
        {
            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            var actorId = requestInfo.UserId;
            var appId = requestInfo.ApplicationId;
            _metadataFieldHandler.init(appId, actorId);

            var result = _metadataFieldHandler.Create(request);
            return result;
        }
        /// <summary>
        /// Update
        /// </summary>
        /// <param name="id"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("api/metadatafield/{id}")]
        public OldResponse<MetadataFieldModel> Update([FromRoute]Guid id, [FromBody]MetadataFieldUpdateRequestModel request)
        {
            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            var actorId = requestInfo.UserId;
            var appId = requestInfo.ApplicationId;
            _metadataFieldHandler.init(appId, actorId);
            request.MetadataFieldId = id;
            var result = _metadataFieldHandler.Update(request);
            return result;
        }
        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("api/metadatafield/{id}")]
        public OldResponse<MetadataFieldDeleteResponseModel> Delete([FromRoute]Guid id)
        {
            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            var actorId = requestInfo.UserId;
            var appId = requestInfo.ApplicationId;
            _metadataFieldHandler.init(appId, actorId);
            var result = _metadataFieldHandler.Delete(id);
            return result;
        }
        /// <summary>
        /// Delete many
        /// </summary>
        /// <param name="deleteData"></param> 
        /// <returns></returns>
        [HttpDelete]
        [Route("api/metadatafield")]
        public OldResponse<IList<MetadataFieldDeleteResponseModel>> DeleteMany([FromQuery] string deleteData)
        {
            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            var actorId = requestInfo.UserId;
            var appId = requestInfo.ApplicationId;
            _metadataFieldHandler.init(appId, actorId);
            var request = JsonConvert.DeserializeObject<List<Guid>>(deleteData);
            var result = _metadataFieldHandler.DeleteMany(request);
            return result;
        }
        #endregion
    }
}
  
