﻿using DigitalID.Business;
using DigitalID.Data;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Serilog;
using System;
using System.Collections.Generic;

namespace DigitalID.API
{
    /// <inheritdoc />
    /// <summary>
    /// Module danh mục metadata template
    /// </summary>
    [ApiVersion("1.0")]
    [Route("")]
    [ApiExplorerSettings(GroupName = "09: Metadata Template", IgnoreApi = true)]
    public class MetadataTemplateController : ControllerBase
    {
        private readonly IMetadataTemplateHandler _metadataTemplateHandler;

        public MetadataTemplateController(IMetadataTemplateHandler metadataTemplateHandler)
        {
            _metadataTemplateHandler = metadataTemplateHandler;
        }
        #region CRUD
        /// <summary>
        /// Lấy danh sách theo bộ lọc
        /// </summary>
        /// <param name="stringFilter"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/metadatatemplate")]
        public OldResponse<IList<MetadataTemplateModel>> GetFilter([FromQuery]string stringFilter)
        {

            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            var actorId = requestInfo.UserId;
            var appId = requestInfo.ApplicationId;
            _metadataTemplateHandler.init(appId, actorId);
            var filter = new MetadataTemplateQueryFilter();
            try
            {
                filter = JsonConvert.DeserializeObject<MetadataTemplateQueryFilter>(stringFilter);
            }
            catch (Exception ex)
            {
                Log.Error("api/metadatatemplate , ex:" + ex.Message);
            }
            var result = _metadataTemplateHandler.GetFilter(filter);
            return result;
        }
        /// <summary>
        /// Lấy theo Id
        /// </summary>
        /// <param name="metadatatemplateId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/metadatatemplate/{metadatatemplateId}")]
        public OldResponse<MetadataTemplateModel> GetById([FromRoute]Guid metadatatemplateId)
        {
            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            var actorId = requestInfo.UserId;
            var appId = requestInfo.ApplicationId;
            _metadataTemplateHandler.init(appId, actorId);

            var result = _metadataTemplateHandler.GetById(metadatatemplateId);
            return result;
        }
        /// <summary>
        /// Create new
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/metadatatemplate")]
        public OldResponse<MetadataTemplateModel> Create([FromBody]MetadataTemplateCreateRequestModel request)
        {
            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            var actorId = requestInfo.UserId;
            var appId = requestInfo.ApplicationId;
            _metadataTemplateHandler.init(appId, actorId);

            var result = _metadataTemplateHandler.Create(request);
            return result;
        }
        /// <summary>
        /// Update
        /// </summary>
        /// <param name="metadatatemplateId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("api/metadatatemplate/{metadatatemplateId}/")]
        public OldResponse<MetadataTemplateModel> Update([FromBody]MetadataTemplateUpdateRequestModel request, [FromRoute]Guid metadatatemplateId)
        {
            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            var actorId = requestInfo.UserId;
            var appId = requestInfo.ApplicationId;
            _metadataTemplateHandler.init(appId, actorId);
            request.MetadataTemplateId = metadatatemplateId;
            var result = _metadataTemplateHandler.Update(request);
            return result;
        }
        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="metadatatemplateId"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("api/metadatatemplate/{metadatatemplateId}")]
        public OldResponse<MetadataTemplateDeleteResponseModel> Delete([FromRoute]Guid metadatatemplateId)
        {
            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            var actorId = requestInfo.UserId;
            var appId = requestInfo.ApplicationId;
            _metadataTemplateHandler.init(appId, actorId);

            var result = _metadataTemplateHandler.Delete(metadatatemplateId);
            return result;
        }
        /// <summary>
        /// Delete many
        /// </summary>
        /// <param name="deleteData"></param> 
        /// <returns></returns>
        [HttpDelete]
        [Route("api/metadatatemplate/")]
        public OldResponse<IList<MetadataTemplateDeleteResponseModel>> DeleteMany([FromQuery] string deleteData)
        {
            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            var actorId = requestInfo.UserId;
            var appId = requestInfo.ApplicationId;
            _metadataTemplateHandler.init(appId, actorId);
            var request = JsonConvert.DeserializeObject<List<Guid>>(deleteData);
            var result = _metadataTemplateHandler.DeleteMany(request);
            return result;
        }
        #endregion
        #region Other

        /// <summary>
        /// Lấy danh sách field một loại tài liệu
        /// </summary>
        /// <param name="metadatatemplateId"></param> 
        /// <returns></returns>
        [HttpGet]
        [Route("api/metadatatemplate/{metadatatemplateId}/field")]
        public OldResponse<IList<MetadataFieldModel>> GetAllFieldOfMetadataTemplateId(Guid metadatatemplateId)
        {
            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            var actorId = requestInfo.UserId;
            var appId = requestInfo.ApplicationId;
            _metadataTemplateHandler.init(appId, actorId);
            var result = _metadataTemplateHandler.GetFieldByMetadataTemplateId(metadatatemplateId);
            return result;
        }

        /// <summary>
        /// Sao chép 1 loại hình tài liệu
        /// </summary>
        /// <param name="metadatatemplateId">Id</param> 
        /// <returns>Dữ liệu trả về</returns>
        [HttpPost]
        [Route("api/metadatatemplate/{metadatatemplateId}/clone")]
        public OldResponse<MetadataTemplateModel> Clone(Guid metadatatemplateId)
        {
            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            var actorId = requestInfo.UserId;
            var appId = requestInfo.ApplicationId;
            _metadataTemplateHandler.init(appId, actorId);
            var result = _metadataTemplateHandler.CloneMetadataTemplate(metadatatemplateId);
            return result;
        }
        #endregion
    }
}

