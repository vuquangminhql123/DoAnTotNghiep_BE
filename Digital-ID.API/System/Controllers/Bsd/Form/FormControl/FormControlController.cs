﻿
using DigitalID.Business;
using DigitalID.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DigitalID.API
{
    /// <inheritdoc />
    /// <summary>
    /// Module trường tin
    /// </summary>
    [ApiVersion("1.0")][ApiController]
    [Route("api/v{api-version:apiVersion}/bsd/form-controls")]
    [ApiExplorerSettings(GroupName = "BSD FormControl", IgnoreApi = true)]
    public class BsdFormControlController : ControllerBase
    {
        private readonly IFormControlHandler _formControlHandler;

        public BsdFormControlController(IFormControlHandler formControlHandler)
        {
            _formControlHandler = formControlHandler;
        }

        /// <summary>
        /// Sao chép
        /// </summary>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [Authorize, HttpPost, Route("{id}/clone")]
        [ProducesResponseType(typeof(ResponseObject<FormControlModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> CreateAsync(Guid id)
        {
            // Get Token Info
            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            var actorId = requestInfo.UserId;
            var appId = requestInfo.ApplicationId;
            // 
            var current = await _formControlHandler.FindAsync(id);
            if (current.Code == Code.Success && current is ResponseObject<FormControlModel> currentData)
            {
                var newModel = AutoMapperUtils.AutoMap<FormControlModel, FormControlCreateModel>(currentData.Data);
                newModel.Name = newModel.Name + "_Copy";
                var result = await _formControlHandler.CreateAsync(newModel, appId, actorId);
                // Hander response
                return Helper.TransformData(result);
            }
            return Helper.TransformData(current);

        }
        #region CRUD
        /// <summary>
        /// Thêm mới
        /// </summary>
        /// <param name="model">Dữ liệu</param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [Authorize, HttpPost, Route("")]
        [ProducesResponseType(typeof(ResponseObject<FormControlModel>), StatusCodes.Status200OK)]
        //        [SwaggerRequestExample(typeof(FormControlCreateModel), typeof(MockupObject<FormControlCreateModel>))]
        public async Task<IActionResult> CreateAsync([FromBody] FormControlCreateModel model)
        {
            // Get Token Info
            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            var actorId = requestInfo.UserId;
            var appId = requestInfo.ApplicationId;
            // Call service
            var result = await _formControlHandler.CreateAsync(model, appId, actorId);
            // Hander response
            return Helper.TransformData(result);
        }
        /// <summary>
        /// Cập nhật
        /// </summary>
        /// <param name="id">Id bản ghi</param>
        /// <param name="model">Dữ liệu</param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [Authorize, HttpPut, Route("{id}")]
        [ProducesResponseType(typeof(ResponseUpdate), StatusCodes.Status200OK)]
        //        [SwaggerRequestExample(typeof(FormControlUpdateModel), typeof(MockupObject<FormControlUpdateModel>))]
        public async Task<IActionResult> UpdateAsync(Guid id, [FromBody] FormControlUpdateModel model)
        {
            // Get Token Info
            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            var actorId = requestInfo.UserId;
            var appId = requestInfo.ApplicationId;
            // Call service
            var result = await _formControlHandler.UpdateAsync(id, model, appId, actorId);

            // Hander response
            return Helper.TransformData(result);
        }
        /// <summary>
        /// Xóa
        /// </summary>
        /// <param name="id">Id bản ghi</param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [Authorize, HttpDelete, Route("{id}")]
        [ProducesResponseType(typeof(ResponseDelete), StatusCodes.Status200OK)]
        public async Task<IActionResult> DeleteAsync(Guid id)
        {
            // Get Token Info
            // var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            // var actorId = requestInfo.UserId;
            // var appId = requestInfo.ApplicationId;
            // Call service
            var result = await _formControlHandler.DeleteAsync(id);
            // Hander response
            return Helper.TransformData(result);
        }
        /// <summary>
        /// Xóa danh sách
        /// </summary>
        /// <param name="listId">Danh sách id bản ghi</param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [Authorize, HttpDelete, Route("")]
        [ProducesResponseType(typeof(ResponseDeleteMulti), StatusCodes.Status200OK)]
        public async Task<IActionResult> DeleteRangeAsync([FromQuery]List<Guid> listId)
        {
            // Get Token Info
            // var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            // var actorId = requestInfo.UserId;
            // var appId = requestInfo.ApplicationId;
            // Call service 
            var result = await _formControlHandler.DeleteRangeAsync(listId);
            // Hander response
            return Helper.TransformData(result);
        }
        /// <summary>
        /// Lấy về theo Id
        /// </summary>
        /// <param name="id">Id bản ghi</param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [Authorize, HttpGet, Route("{id}")]
        [ProducesResponseType(typeof(ResponseObject<FormControlModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetByIdAsync(Guid id)
        {
            // Get Token Info
            // var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            // var actorId = requestInfo.UserId;
            // var appId = requestInfo.ApplicationId;
            // Call service
            var result = await _formControlHandler.FindAsync(id);
            // Hander response
            return Helper.TransformData(result);
        }
        /// <summary>
        /// Lấy về theo bộ loc
        /// </summary>
        /// <param name="page">Số thứ tự trang tìm kiếm</param>
        /// <param name="size">Số bản ghi giới hạn một trang</param>
        /// <param name="filter">Thông tin lọc nâng cao (Object Json)</param>
        /// <param name="sort">Thông tin sắp xếp (Array Json)</param>
        /// <returns></returns>
        /// <remarks>
        ///  *filter*
        ///  ....
        ///  *sort*
        ///  ....
        /// </remarks>
        /// <response code="200">Thành công</response>
        [Authorize, HttpGet, Route("")]
        [ProducesResponseType(typeof(ResponsePagination<FormControlModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetFilterAsync([FromQuery]int page = 1, [FromQuery]int size = 20, [FromQuery]string filter = "{}", [FromQuery]string sort = "")
        {
            // Get Token Info
            // var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            // var actorId = requestInfo.UserId;
            // var appId = requestInfo.ApplicationId;
            // Call service
            var filterObject = JsonConvert.DeserializeObject<FormControlQueryModel>(filter);
            filterObject.Sort = sort;
            filterObject.Size = size;
            filterObject.Page = page;
            var result = await _formControlHandler.GetPageAsync(filterObject);
            // Hander response
            return Helper.TransformData(result);
        }
        /// <summary>
        /// Lấy về tất cả
        /// </summary> 
        /// <returns></returns> 
        /// <response code="200">Thành công</response>
        [Authorize, HttpGet, Route("all")]
        [ProducesResponseType(typeof(ResponseList<FormControlModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetAllAsync([FromQuery]string filter = "{}")
        {
            // Get Token Info
            // var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            // var actorId = requestInfo.UserId;
            // var appId = requestInfo.ApplicationId;
            // Call service
            var filterObject = JsonConvert.DeserializeObject<FormControlQueryModel>(filter);
            var result = await _formControlHandler.GetAllAsync(filterObject);
            // Hander response
            return Helper.TransformData(result);
        }
        #endregion 
    }
}
