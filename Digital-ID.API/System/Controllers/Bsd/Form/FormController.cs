﻿
using DigitalID.Business;
using DigitalID.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DigitalID.API
{
    /// <inheritdoc />
    /// <summary>
    /// Module biểu mẫu
    /// </summary>
    [ApiVersion("1.0")][ApiController]
    [Route("api/v{api-version:apiVersion}/bsd/form")]
    [ApiExplorerSettings(GroupName = "BSD Form", IgnoreApi = true)]
    public class BsdFormController : ControllerBase
    {
        private readonly IFormHandler _formHandler;

        public BsdFormController(IFormHandler formHandler)
        {
            _formHandler = formHandler;
        }
        #region CRUD
        /// <summary>
        /// Thêm mới
        /// </summary>
        /// <param name="masterId"></param>
        /// <param name="formCollection"></param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [Authorize, HttpPost, Route("")]
        public async Task<IActionResult> AddDataAsync(Guid masterId, [FromBody] dynamic formCollection)
        {
            // Get Token Info
            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            var actorId = requestInfo.UserId;
            var appId = requestInfo.ApplicationId;
            // Call service
            var result = await _formHandler.AddDataAsync(masterId, formCollection, appId, actorId);
            // Hander response
            return Helper.TransformData(result);
        }
        /// <summary>
        /// Cập nhật
        /// </summary>
        /// <param name="id">Id bản ghi</param>
        /// <param name="masterId"></param>
        /// <param name="formCollection"></param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [Authorize, HttpPut, Route("{id}")]
        public async Task<IActionResult> UpdateDataAsync(Guid id, Guid masterId, [FromBody] dynamic formCollection)
        {
            // Get Token Info
            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            var actorId = requestInfo.UserId;
            var appId = requestInfo.ApplicationId;
            // Call service
            var result = await _formHandler.UpdateDataAsync(id, masterId, formCollection, appId, actorId);
            // Hander response
            return Helper.TransformData(result);
        }
        /// <summary>
        /// Xóa
        /// </summary>
        /// <param name="id">Id bản ghi</param>
        /// <param name="masterId"></param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [Authorize, HttpDelete, Route("{id}")]
        [ProducesResponseType(typeof(ResponseDelete), StatusCodes.Status200OK)]
        public async Task<IActionResult> DeleteDataAsync(Guid id, Guid masterId)
        {
            // Get Token Info
            // var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            // var actorId = requestInfo.UserId;
            // var appId = requestInfo.ApplicationId;
            // Call service
            var result = await _formHandler.DeleteDataAsync(id, masterId);
            // Hander response
            return Helper.TransformData(result);
        }
        /// <summary>
        /// Xóa danh sách
        /// </summary>
        /// <param name="listId">Danh sách id bản ghi</param>
        /// <param name="masterId"></param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [Authorize, HttpDelete, Route("")]
        [ProducesResponseType(typeof(ResponseDeleteMulti), StatusCodes.Status200OK)]
        public async Task<IActionResult> DeleteRangeAsync([FromQuery]List<Guid> listId, [FromQuery] Guid masterId)
        {
            var result = new List<ResponseDelete>();
            foreach (var id in listId)
            {
                var resultItem = await _formHandler.DeleteDataAsync(id, masterId);
                result.Add(resultItem as ResponseDelete);
            }
            // Hander response
            return Helper.TransformData(new ResponseDeleteMulti(result));
        }

        /// <summary>
        /// Lấy về theo Id
        /// </summary>
        /// <param name="id">Id bản ghi</param>
        /// <param name="masterId"></param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [Authorize, HttpGet, Route("{id}")]
        public async Task<IActionResult> GetDataByIdAsync(Guid id, Guid masterId)
        {
            // Get Token Info
            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            var actorId = requestInfo.UserId;
            var appId = requestInfo.ApplicationId;
            // Call service
            var result = await _formHandler.GetDataByIdAsync(id, masterId);
            // Hander response
            return Helper.TransformData(result);
        }
        /// <summary>
        /// Lấy về theo bộ loc
        /// </summary>
        /// <param name="masterId"></param>
        /// <param name="page">Số thứ tự trang tìm kiếm</param>
        /// <param name="size">Số bản ghi giới hạn một trang</param>
        /// <param name="filter">Thông tin lọc nâng cao (Object Json)</param>
        /// <param name="connditions"></param>
        /// <param name="sort">Thông tin sắp xếp (Array Json)</param>
        /// <returns></returns>
        /// <remarks>
        ///  *filter*
        ///  ....
        ///  *sort*
        ///  ....
        /// </remarks>
        /// <response code="200">Thành công</response>
        [Authorize, HttpGet, Route("")]
        public async Task<IActionResult> GetFilterAsync([FromQuery] Guid masterId,
            [FromQuery]int page = 1,
            [FromQuery]int size = 20,
            [FromQuery]string filter = "{}",
            [FromQuery]string connditions = "[]",
            [FromQuery]string sort = "")
        {
            // Get Token Info
            // var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            // var actorId = requestInfo.UserId;
            // var appId = requestInfo.ApplicationId;
            // Call service
            var filterObject = JsonConvert.DeserializeObject<FormDataFilter>(filter);
            var conditionsObject = JsonConvert.DeserializeObject<List<SearchCondition>>(connditions);
            filterObject.Sort = sort;
            filterObject.Size = size;
            filterObject.Page = page;
            var result = await _formHandler.GetDataFilter(masterId, filterObject, conditionsObject);
            // Hander response
            return Helper.TransformData(result);
        }
        #endregion
    }
}
