﻿using DigitalID.Business;
using DigitalID.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DigitalID.API
{
    /// <inheritdoc />
    /// <summary>
    /// Module điều hướng
    /// </summary>
    [ApiVersion("1.0")][ApiController]
    [Route("api/v{api-version:apiVersion}/bsd/navigations")]
    [ApiExplorerSettings(GroupName = "BSD Navigation", IgnoreApi = true)]
    public class BsdNavigationController : ControllerBase
    {
        private readonly INavigationHanlder _navigationHanlder;

        public BsdNavigationController(INavigationHanlder navigationHanlder)
        {
            _navigationHanlder = navigationHanlder;
        }
        /// <summary>
        /// Lấy danh sách theo người dùng
        /// </summary>
        /// <param name="userId">Id người dùng</param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [Authorize, HttpGet, Route("user/{userId}")]
        [ProducesResponseType(typeof(ResponseList<NavigationModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetByUserIdAsync(Guid userId)
        {
            // Get Token Info
            var requestInfo =  Helper.GetRequestInfo(Request, HttpContext.User);
            var appId = requestInfo.ApplicationId;
            // Call service
            var result =await _navigationHanlder.GetByUserIdAsync(userId, appId);
            // Hander response
            return Helper.TransformData(result);
        }
        /// <summary>
        /// Lấy danh sách của bản thân
        /// </summary>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [Authorize, HttpGet, Route("owner")]
        [ProducesResponseType(typeof(ResponseList<NavigationModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetByUserIdAsync()
        {
            // Get Token Info
            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            var actorId = requestInfo.UserId;
            var appId = requestInfo.ApplicationId;
            // Call service
            var result = await _navigationHanlder.GetByUserIdAsync(actorId, appId);
            // Hander response
            return Helper.TransformData(result);
        }
        /// <summary>
        /// Lấy danh sách dạng tree
        /// </summary>
        /// <param name="isGetRoles">Lấy kèm nhóm quyền</param> 
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [Authorize, HttpGet, Route("tree")]
        [ProducesResponseType(typeof(ResponseList<NavigationModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetTreeAsync(bool isGetRoles)
        {
            // Get Token Info
            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
// var actorId = requestInfo.UserId;
            var appId = requestInfo.ApplicationId;
            // Call service
            var result = await _navigationHanlder.GetTreeAsync(appId, isGetRoles);
            // Hander response
            return Helper.TransformData(result);
        }
        /// <summary>
        /// Thêm mới
        /// </summary>
        /// <param name="request">Dữ liệu thêm mới</param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [Authorize, HttpPost, Route("")]
        [ProducesResponseType(typeof(ResponseObject<NavigationModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> CreateAsync([FromBody]NavigationCreateModel request)
        {
            // Get Token Info
            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            var actorId = requestInfo.UserId;
            var appId = requestInfo.ApplicationId;
            // Call service
            var result = await _navigationHanlder.CreateAsync(request,appId,actorId);
            // Hander response
            return Helper.TransformData(result);
        }
        /// <summary>
        /// Cập nhật
        /// </summary>
        /// <param name="id">Id bản ghi</param>
        /// <param name="request">Dữ liệu cập nhật</param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [Authorize, HttpPut, Route("{id}")]
        [ProducesResponseType(typeof(ResponseUpdate), StatusCodes.Status200OK)]
        public async Task<IActionResult> UpdateAsync(Guid id,[FromBody] NavigationUpdateModel request)
        {
            // Get Token Info
            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            var actorId = requestInfo.UserId;
            var appId = requestInfo.ApplicationId;
            // Call service
            var result = await _navigationHanlder.UpdateAsync(id,request, appId, actorId);
            // Hander response
            return Helper.TransformData(result);
        }
        /// <summary>
        /// Xóa
        /// </summary>
        /// <param name="id">Id bản ghi</param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [Authorize, HttpDelete, Route("{id}")]
        [ProducesResponseType(typeof(ResponseDelete), StatusCodes.Status200OK)]
        public async Task<IActionResult> DeleteAsync(Guid id)
        {
            // Get Token Info
// var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
// var actorId = requestInfo.UserId;
// var appId = requestInfo.ApplicationId;
            // Call service
            var result = await _navigationHanlder.DeleteAsync(id);
            // Hander response
            return Helper.TransformData(result);
        }
        /// <summary>
        /// Gán nhóm quyền 
        /// </summary>
        /// <param name="id">Id bản ghi</param>
        /// <param name="listRoleId">Danh sách id nhóm quyền</param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [Authorize, HttpPut, Route("{id}/map")]
        [ProducesResponseType(typeof(ResponseUpdate), StatusCodes.Status200OK)]
        public async Task<IActionResult> UpdateRoleInNavigations(Guid id, [FromBody] IList<Guid> listRoleId)
        {
            // Get Token Info
// var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
// var actorId = requestInfo.UserId;
// var appId = requestInfo.ApplicationId;
            // Call service
            var result = await _navigationHanlder.UpdateRoleInNavigations(id, listRoleId);
            // Hander response
            return Helper.TransformData(result);
        }

         

    }
}
