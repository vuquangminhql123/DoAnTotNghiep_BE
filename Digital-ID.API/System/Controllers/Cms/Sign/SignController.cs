﻿using DigitalID.Business;
using DigitalID.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace DigitalID.API
{
    /// <inheritdoc />
    /// <summary>
    /// Module
    /// </summary>
    [ApiVersion("1.0")]
    [ApiController]
    [Route("api/v{api-version:apiVersion}/cms/sign")]
    [ApiExplorerSettings(GroupName = "CMS Sign", IgnoreApi = true)]
    public class SignController : ControllerBase
    {
        private readonly ISignHandler _signHandler;

        public SignController(ISignHandler signHandler)
        {
            _signHandler = signHandler;
        }

        /// <summary>
        /// Ký file
        /// </summary>
        /// <param name="model">Dữ liệu</param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [Authorize, HttpPost, Route("")]
        [ProducesResponseType(typeof(ResponseObject<FileOuputModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> SignPDFAsync([FromBody] SignModel model)
        {
            // Get Token Info
            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            var actorId = new Guid(User.Identity.Name);
            var appId = requestInfo.ApplicationId;
            // Call service
            int typeSign = Convert.ToInt32(Utils.GetConfig("Sign:Type"));
            if (typeSign == 3)
            {
                var result = await _signHandler.SignUrlPdfV3SaveFromBase64Content(model, appId, actorId);
                // Hander response
                return Helper.TransformData(result);
            }else  if (typeSign == 2)
            {
                var result = await _signHandler.SignUrlPdfV2(model, appId, actorId);
                // Hander response
                return Helper.TransformData(result);
            }
            else
            {
                var result = await _signHandler.SignUrlPdfV1(model, appId, actorId);
                // Hander response
                return Helper.TransformData(result);
            }            
        }

        [Authorize, HttpPost, Route("userpin")]
        public OldResponse<KeyHashModel> CheckKey(string key)
        {
            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            var actorId = requestInfo.UserId;
            var result = _signHandler.CheckKey(key, actorId);
            return result;
        }
    }
}
