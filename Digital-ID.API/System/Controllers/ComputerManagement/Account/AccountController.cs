﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DigitalID.Business;
using DigitalID.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Nest;

namespace DigitalID.API
{
    [ApiVersion("1.0")]
    [ApiController]
    [Route("api/v{api-version:apiVersion}/account/create")]
    [ApiExplorerSettings(GroupName = "Create Account")]
    public class AccountController : ControllerBase
    {
        private readonly IAccountHandler _handler;
        public AccountController(IAccountHandler handler)
        {
            _handler = handler;
        }

        /// <summary>
        /// Tạo mới tài khoản
        /// </summary>
        /// <param name="model">Thông tin tài khoản</param>
        /// <returns>Model tài khoản</returns> 
        /// <response code="200">Thành công</response>
        [AllowAnonymous, HttpPost, Route("")]
        [ProducesResponseType(typeof(ResponseObject<Guid>), StatusCodes.Status200OK)]
        public async Task<IActionResult> Create([FromBody] AccountBaseModel model)
        {
            var result = await _handler.Create(model);
            // Hander response
            return Helper.TransformData(result);
        }


    }
    [Route("api/signalr-test")]
    public class TestController : Controller
    {
        private IHubContext<SignalrHub, IHubClient> _signalrHub;

        public TestController(IHubContext<SignalrHub, IHubClient> signalrHub)
        {
            _signalrHub = signalrHub;
        }

        [HttpPost]
        public async Task<string> Post([FromBody] MessageInstance msg)
        {
            var retMessage = string.Empty;
            try
            {
                msg.Timestamp = DateTime.Now.ToString();
                await _signalrHub.Clients.All.BroadcastMessage(msg);
                retMessage = "Success";
            }
            catch (Exception e)
            {
                retMessage = e.ToString();
            }
            return retMessage;
        }
    }
}
