﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DigitalID.Business;
using DigitalID.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NetCore.Business;

namespace DigitalID.API
{
    [ApiVersion("1.0")]
    [ApiController]
    [Route("api/v{api-version:apiVersion}/address")]
    [ApiExplorerSettings(GroupName = "Create Account")]
    public class BaseAddressController : ControllerBase
    {
        private readonly IBaseAddressHandler _handler;
        public BaseAddressController(IBaseAddressHandler handler)
        {
            _handler = handler;
        }

        /// <summary>
        /// lay DS thanh pho
        /// </summary>
        /// <param name="model">Thông tin thanh pho</param>
        /// <returns>Model tài khoản</returns> 
        /// <response code="200">Thành công</response>
        [AllowAnonymous, HttpGet, Route("city")]
        [ProducesResponseType(typeof(ResponseObject<List<City>>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetCity()
        {
            var result = await _handler.GetCity();
            // Hander response
            return Helper.TransformData(result);
        }

        /// <summary>
        /// lay DS quan huyen
        /// </summary>
        /// <param name="model">Thông tin thanh pho</param>
        /// <returns>Model tài khoản</returns> 
        /// <response code="200">Thành công</response>
        [AllowAnonymous, HttpGet, Route("district")]
        [ProducesResponseType(typeof(ResponseObject<List<District>>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetDistrict(string matp)
        {
            var result = await _handler.GetDistrictByCity(matp);
            // Hander response
            return Helper.TransformData(result);
        }

        /// <summary>
        /// lay DS xa phuong
        /// </summary>
        /// <param name="model">Thông tin thanh pho</param>
        /// <returns>Model tài khoản</returns> 
        /// <response code="200">Thành công</response>
        [AllowAnonymous, HttpGet, Route("commune")]
        [ProducesResponseType(typeof(ResponseObject<List<Commune>>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetCommune(string maqh)
        {
            var result = await _handler.GetCommuneByDistrict(maqh);
            // Hander response
            return Helper.TransformData(result);
        }
    }
}
