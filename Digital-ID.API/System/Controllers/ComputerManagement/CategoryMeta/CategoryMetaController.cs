﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DigitalID.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NetCore.Business;

namespace DigitalID.API
{
    [ApiVersion("1.0")]
    [ApiController]
    [Route("api/v{api-version:apiVersion}/computer-management/category-meta")]
    [ApiExplorerSettings(GroupName = "CategoryMeta")]
    public class CategoryMetaController : ApiControllerBase
    {
        private readonly ICategoryMetaHandler _handler;
        public CategoryMetaController(ICategoryMetaHandler handler)
        {
            _handler = handler;
        }

        /// <summary>
        /// Thêm mới CategoryMeta
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     {
        ///         "code": "Code",
        ///         "name": "Name",
        ///         "status": true,
        ///         "description": "Description",
        ///         "order": 1
        ///     }
        /// </remarks>
        /// <param name="model">Thông tin CategoryMeta</param>
        /// <returns>Id CategoryMeta</returns> 
        /// <response code="200">Thành công</response>
        [Authorize, HttpPost, Route("")]
        [ProducesResponseType(typeof(ResponseObject<Guid>), StatusCodes.Status200OK)]
        public async Task<IActionResult> Create([FromBody] CategoryMetaCreateModel model)
        {
            return await ExecuteFunction(async (RequestUser u) =>
            {
                model.CreatedUserId = u.UserId;
                model.ApplicationId = u.ApplicationId;
                var result = await _handler.Create(model);

                return result;
            });
        }

        /// <summary>
        /// Thêm mới CategoryMeta theo danh sách
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     [
        ///         {
        ///             "code": "Code",
        ///             "name": "Name",
        ///             "status": true,
        ///             "description": "Description",
        ///             "order": 1
        ///         }   
        ///     ]
        /// </remarks>
        /// <param name="list">Danh sách thông tin CategoryMeta</param>
        /// <returns>Danh sách kết quả thêm mới</returns> 
        /// <response code="200">Thành công</response>
        [Authorize, HttpPost, Route("create-many")]
        [ProducesResponseType(typeof(ResponseObject<Guid>), StatusCodes.Status200OK)]
        public async Task<IActionResult> CreateMany([FromBody] List<CategoryMetaCreateModel> list)
        {
            return await ExecuteFunction(async (RequestUser u) =>
            {
                foreach (var item in list)
                {
                    item.CreatedUserId = u.UserId;
                    item.ApplicationId = u.ApplicationId;
                }
                var result = await _handler.CreateMany(list);
                return result;
            });
        }
        /// <summary>
        /// Cập nhật CategoryMeta
        /// </summary> 
        /// <remarks>
        /// Sample request:
        ///
        ///     {
        ///         "id": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
        ///         "code": "Code",
        ///         "name": "Name",
        ///         "status": true,
        ///         "description": "Description",
        ///         "order": 1
        ///     }   
        /// </remarks>
        /// <param name="model">Thông tin CategoryMeta cần cập nhật</param>
        /// <returns>Id CategoryMeta đã cập nhật thành công</returns> 
        /// <response code="200">Thành công</response>
        [Authorize, HttpPut, Route("")]
        [ProducesResponseType(typeof(ResponseObject<Guid>), StatusCodes.Status200OK)]
        public async Task<IActionResult> Update([FromBody] CategoryMetaUpdateModel model)
        {
            return await ExecuteFunction(async (RequestUser u) =>
            {
                model.ModifiedUserId = u.UserId;
                var result = await _handler.Update(model);

                return result;
            });
        }

        /// <summary>
        /// Lấy thông tin CategoryMeta theo id
        /// </summary> 
        /// <param name="id">Id CategoryMeta</param>
        /// <returns>Thông tin chi tiết CategoryMeta</returns> 
        /// <response code="200">Thành công</response>
        [Authorize, HttpGet, Route("")]
        [ProducesResponseType(typeof(ResponseObject<CategoryMetaModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetById(Guid id)
        {
            return await ExecuteFunction(async (RequestUser u) =>
            {
                var result = await _handler.GetById(id);

                return result;
            });
        }

        /// <summary>
        /// Lấy danh sách CategoryMeta theo điều kiện lọc
        /// </summary> 
        /// <remarks>
        /// Sample request:
        ///
        ///     {
        ///         "textSearch": "",
        ///         "pageSize": 20,
        ///         "pageNumber": 1
        ///     }
        /// </remarks>
        /// <param name="filter">Điều kiện lọc</param>
        /// <returns>Danh sách CategoryMeta</returns> 
        /// <response code="200">Thành công</response>
        [Authorize, HttpPost, Route("filter")]
        [ProducesResponseType(typeof(ResponseObject<List<CategoryMetaBaseModel>>), StatusCodes.Status200OK)]
        public async Task<IActionResult> Filter([FromBody] CategoryMetaQueryFilter filter)
        {
            return await ExecuteFunction(async (RequestUser u) =>
            {
                var result = await _handler.Filter(filter);

                return result;
            });
        }
        /// <summary>
        /// Lấy tất cả danh sách CategoryMeta
        /// </summary> 
        /// <param name="ts">Từ khóa tìm kiếm</param>
        /// <returns>Danh sách CategoryMeta</returns> 
        /// <response code="200">Thành công</response>
        [AllowAnonymous, HttpGet, Route("all")]
        [ProducesResponseType(typeof(ResponseObject<List<CategoryMetaBaseModel>>), StatusCodes.Status200OK)]
        public async Task<Response> GetAll(string ts = null)
        {
            CategoryMetaQueryFilter filter = new CategoryMetaQueryFilter()
            {
                TextSearch = ts,
                PageNumber = null,
                PageSize = null
            };
            var result = await _handler.Filter(filter);

            return result;
        }
        /// <summary>
        /// Xóa CategoryMeta
        /// </summary> 
        /// <remarks>
        /// Sample request:
        ///
        ///     [
        ///         "3fa85f64-5717-4562-b3fc-2c963f66afa6"
        ///     ]
        /// </remarks>
        /// <param name="listId">Danh sách Id CategoryMeta</param>
        /// <returns>Danh sách kết quả xóa</returns> 
        /// <response code="200">Thành công</response>
        [Authorize, HttpDelete, Route("")]
        [ProducesResponseType(typeof(ResponseObject<List<ResponeDeleteModel>>), StatusCodes.Status200OK)]
        public async Task<IActionResult> Delete([FromBody] List<Guid> listId)
        {
            return await ExecuteFunction(async (RequestUser u) =>
            {
                var result = await _handler.Delete(listId);

                return result;
            });
        }

        /// <summary>
        /// Lấy danh sách CategoryMeta cho combobox
        /// </summary> 
        /// <param name="count">số bản ghi tối đa</param>
        /// <param name="ts">Từ khóa tìm kiếm</param>
        /// <returns>Danh sách CategoryMeta</returns> 
        /// <response code="200">Thành công</response>
        [Authorize, HttpGet, Route("for-combobox")]
        [ProducesResponseType(typeof(ResponseObject<List<SelectItemModel>>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetListCombobox(int count = 0, string ts = "")
        {
            return await ExecuteFunction(async (RequestUser u) =>
            {
                var result = await _handler.GetListCombobox(count, ts);

                return result;
            });
        }
    }
}
