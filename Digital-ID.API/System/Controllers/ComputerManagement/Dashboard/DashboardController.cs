﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DigitalID.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using NetCore.Business;

namespace DigitalID.API
{
    [ApiVersion("1.0")]
    [ApiController]
    [Route("api/v{api-version:apiVersion}/dashboard")]
    [ApiExplorerSettings(GroupName = "Quản lý")]
    public class DashboardController : ApiControllerBase
    {
        private readonly IProductHandler _handler;
        private DataContext _dataContext = new DataContext();
        public DashboardController(IProductHandler handler)
        {
            _handler = handler;
        }
        /// <summary>
        /// Xóa loại sản phẩm
        /// </summary> 
        /// <remarks>
        /// Sample request:
        ///
        ///     [
        ///         "3fa85f64-5717-4562-b3fc-2c963f66afa6"
        ///     ]
        /// </remarks>
        /// <param name="listId">Danh sách Id loại sản phẩm</param>
        /// <returns>Danh sách kết quả xóa</returns> 
        /// <response code="200">Thành công</response>
        [AllowAnonymous, HttpGet, Route("update-visit")]
        [ProducesResponseType(typeof(ResponseObject<VisitWebsiteResponse>), StatusCodes.Status200OK)]
        public async Task<VisitWebsiteResponse> Delete()
        {
            var visitModel = new VisitWebsite()
            {
                Id = Guid.NewGuid(),
                CreatedDate = DateTime.Now
            };
            await _dataContext.VisitWebsites.AddAsync(visitModel);
            var visitResponse = new VisitWebsiteResponse()
            {
                VisitWebsite = _dataContext.VisitWebsites.ToList().Count,
                VisitWebsiteToday = _dataContext.VisitWebsites.Where(x => x.CreatedDate.Day == DateTime.Now.Day && x.CreatedDate.Month == DateTime.Now.Month && x.CreatedDate.Year == DateTime.Now.Year).ToList().Count
            };
            var dbSave = await _dataContext.SaveChangesAsync();
            return visitResponse;
        }

        [AllowAnonymous, HttpGet, Route("")]
        [ProducesResponseType(typeof(ResponseObject<DashboardModel>), StatusCodes.Status200OK)]
        public async Task<DashboardModel> GetDataDashboard(string year = "")
        {

            var listOrder = await _dataContext.Orders.ToListAsync();
            var listOrdProd = await _dataContext.OrderProducts.ToListAsync();
            var listProd = listOrdProd.GroupBy(x => x.ProductId).Select(x => new ProductOrderModel()
            {
                ProductId = x.First().ProductId,
                Quantity = x.Count()
            }).ToList();
            var listRpYear = listOrder.Where(x => x.Status == 3 || x.Status == 5).GroupBy(x => x.CreatedDate.Year)
                .Select(od => new ReportYearModel()
                {
                    Year = od.First().CreatedDate.Year,
                    Total = od.Sum(c => c.GrandTotal)
                }).ToList();
            if (string.IsNullOrEmpty(year))
            {
                year = DateTime.Now.Year.ToString();
            }
            var orderRp = new OrderReport()
            {
                OrderReceived = listOrder.Count,
                OrderDelivered = listOrder.Where(x => x.Status == 3 || x.Status == 5).ToList().Count,
                OrderCancelled = listOrder.Where(x => x.Status == -1).ToList().Count,
                OrderReceivedMonth = listOrder.Where(x =>
                        x.CreatedDate.Month == DateTime.Now.Month && x.CreatedDate.Year == DateTime.Now.Year).ToList()
                    .Count,
                OrderReceivedToday = listOrder.Where(x =>
                    x.CreatedDate.Day == DateTime.Now.Day && x.CreatedDate.Year == DateTime.Now.Year &&
                    x.CreatedDate.Month == DateTime.Now.Month).ToList().Count,
                OrderNotProcess = listOrder.Where(x => x.Status == 0).ToList().Count,
                OrderNotProcessToday = listOrder.Where(x =>
                    x.Status == 0 && x.CreatedDate.Day == DateTime.Now.Day && x.CreatedDate.Year == DateTime.Now.Year &&
                    x.CreatedDate.Month == DateTime.Now.Month).ToList().Count,
                GrandTotal = listOrder.Where(x => x.Status != -1).ToList().Select(x => x.GrandTotal).Sum(),
                GrandTotalDelivered = listOrder.Where(x => x.Status == 3 || x.Status == 5).ToList().Select(x => x.GrandTotal).Sum(),
            };
            var model = new DashboardModel()
            {
                ReportYearModel = listRpYear.Where(x => x.Year >= (DateTime.Now.Year - 5)).ToList(),
                OrderReport = orderRp,
                ProductOrderModels = listProd.OrderByDescending(x=>x.Quantity).ToList()
            };
            return model;
        }

        [AllowAnonymous, HttpGet, Route("report-month-in-year")]
        [ProducesResponseType(typeof(ResponseObject<DashboardModel>), StatusCodes.Status200OK)]
        public async Task<DashboardModel> GetReportYear(string year = "")
        {
            var listOrder = await _dataContext.Orders.ToListAsync();
            if (string.IsNullOrEmpty(year))
            {
                year = DateTime.Now.Year.ToString();
            }
            var listRpMonth = listOrder.Where(x => x.Status == 3 || x.Status == 5).GroupBy(x => new { x.CreatedDate.Year, x.CreatedDate.Month }).Select(od => new ReportMonthModel()
            {
                Year = od.First().CreatedDate.Year,
                Month = od.First().CreatedDate.Month,
                Total = od.Sum(c => c.GrandTotal)
            }).ToList();
            var model = new DashboardModel()
            {
                ReportMonthModel = listRpMonth.Where(x => x.Year.ToString() == year).ToList()
            };
            return model;
        }
    }
    public class DashboardModel
    {
        public OrderReport OrderReport { get; set; }
        public List<ProductOrderModel> ProductOrderModels { get; set; }
        public List<ReportMonthModel> ReportMonthModel { get; set; }
        public List<ReportYearModel> ReportYearModel { get; set; }
    }

    public class OrderReport
    {
        public int OrderReceived { get; set; }
        public int OrderDelivered { get; set; }
        public int OrderCancelled { get; set; }
        public int OrderReceivedMonth { get; set; }
        public int OrderReceivedToday { get; set; }
        public int OrderNotProcess { get; set; }
        public int OrderNotProcessToday { get; set; }
        public double GrandTotal { get; set; }
        public double GrandTotalDelivered { get; set; }
    }
    public class VisitWebsiteResponse
    {
        public int VisitWebsiteToday { get; set; }
        public int VisitWebsite { get; set; }
    }
    public class ReportYearModel
    {
        public int Year { get; set; }
        public double Total { get; set; }
    }
    public class ProductOrderModel
    {
        public int Quantity { get; set; }
        public Guid ProductId { get; set; }
    }
    public class ReportMonthModel
    {
        public int Month { get; set; }
        public int Year { get; set; }
        public double Total { get; set; }
    }
}
