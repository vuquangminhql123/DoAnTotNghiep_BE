﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DigitalID.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NetCore.Business;

namespace DigitalID.API
{
    [ApiVersion("1.0")]
    [ApiController]
    [Route("api/v{api-version:apiVersion}/computer-management/product")]
    [ApiExplorerSettings(GroupName = "sản phẩm")]
    public class ProductController : ApiControllerBase
    {
        private readonly IProductHandler _handler;
        public ProductController(IProductHandler handler)
        {
            _handler = handler;
        }

        /// <summary>
        /// Thêm mới sản phẩm
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     {
        ///         "code": "Code",
        ///         "name": "Name",
        ///         "status": true,
        ///         "description": "Description",
        ///         "order": 1
        ///     }
        /// </remarks>
        /// <param name="model">Thông tin sản phẩm</param>
        /// <returns>Id sản phẩm</returns> 
        /// <response code="200">Thành công</response>
        [AllowAnonymous, HttpPost, Route("")]
        [ProducesResponseType(typeof(ResponseObject<Guid>), StatusCodes.Status200OK)]
        public async Task<IActionResult> Create([FromBody] ProductCreateModel model)
        {
            return await ExecuteFunction(async (RequestUser u) =>
            {
                model.CreatedUserId = u.UserId;
                model.ApplicationId = u.ApplicationId;
                var result = await _handler.Create(model);

                return result;
            });
        }

        /// <summary>
        /// Thêm mới sản phẩm theo danh sách
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     [
        ///         {
        ///             "code": "Code",
        ///             "name": "Name",
        ///             "status": true,
        ///             "description": "Description",
        ///             "order": 1
        ///         }   
        ///     ]
        /// </remarks>
        /// <param name="list">Danh sách thông tin sản phẩm</param>
        /// <returns>Danh sách kết quả thêm mới</returns> 
        /// <response code="200">Thành công</response>
        [AllowAnonymous, HttpPost, Route("create-many")]
        [ProducesResponseType(typeof(ResponseObject<Guid>), StatusCodes.Status200OK)]
        public async Task<IActionResult> CreateMany([FromBody] List<ProductCreateModel> list)
        {
            return await ExecuteFunction(async (RequestUser u) =>
            {
                foreach (var item in list)
                {
                    item.CreatedUserId = u.UserId;
                    item.ApplicationId = u.ApplicationId;
                }
                var result = await _handler.CreateMany(list);
                return result;
            });
        }
        /// <summary>
        /// Cập nhật sản phẩm
        /// </summary> 
        /// <remarks>
        /// Sample request:
        ///
        ///     {
        ///         "id": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
        ///         "code": "Code",
        ///         "name": "Name",
        ///         "status": true,
        ///         "description": "Description",
        ///         "order": 1
        ///     }   
        /// </remarks>
        /// <param name="model">Thông tin sản phẩm cần cập nhật</param>
        /// <returns>Id sản phẩm đã cập nhật thành công</returns> 
        /// <response code="200">Thành công</response>
        [AllowAnonymous, HttpPut, Route("")]
        [ProducesResponseType(typeof(ResponseObject<Guid>), StatusCodes.Status200OK)]
        public async Task<IActionResult> Update([FromBody] ProductUpdateModel model)
        {
            return await ExecuteFunction(async (RequestUser u) =>
            {
                model.ModifiedUserId = u.UserId;
                var result = await _handler.Update(model);

                return result;
            });
        }

        /// <summary>
        /// Lấy thông tin sản phẩm theo id
        /// </summary> 
        /// <param name="id">Id sản phẩm</param>
        /// <returns>Thông tin chi tiết sản phẩm</returns> 
        /// <response code="200">Thành công</response>
        [AllowAnonymous, HttpGet, Route("")]
        [ProducesResponseType(typeof(ResponseObject<ProductModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetById(Guid id)
        {
            return await ExecuteFunction(async (RequestUser u) =>
            {
                var result = await _handler.GetById(id);

                return result;
            });
        }
        /// <summary>
        /// Lấy thông tin sản phẩm liên quan
        /// </summary> 
        /// <param name="id">Mã sản phẩm</param>
        /// <returns>Thông tin chi tiết sản phẩm</returns> 
        /// <response code="200">Thành công</response>
        [AllowAnonymous, HttpGet, Route("similar")]
        [ProducesResponseType(typeof(ResponseObject<List<ProductBaseModel>>), StatusCodes.Status200OK)]
        public async Task<Response> GetSimilarByCode(string code)
        {
            var result = await _handler.GetListProductSimilar(code);

            return result;
        }
        /// <summary>
        /// Lấy thông tin sản phẩm theo id
        /// </summary> 
        /// <param name="id">Id sản phẩm</param>
        /// <returns>Thông tin chi tiết sản phẩm</returns> 
        /// <response code="200">Thành công</response>
        [AllowAnonymous, HttpPost, Route("code")]
        [ProducesResponseType(typeof(ResponseObject<ProductBaseModel>), StatusCodes.Status200OK)]
        public async Task<Response> GetByCode(GetByCode model)
        {
            var result = await _handler.GetByCode(model.Code);

            return result;
        }

        /// <summary>
        /// Lấy danh sách sản phẩm theo điều kiện lọc
        /// </summary> 
        /// <remarks>
        /// Sample request:
        ///
        ///     {
        ///         "textSearch": "",
        ///         "pageSize": 20,
        ///         "pageNumber": 1
        ///     }
        /// </remarks>
        /// <param name="filter">Điều kiện lọc</param>
        /// <returns>Danh sách sản phẩm</returns> 
        /// <response code="200">Thành công</response>
        [AllowAnonymous, HttpPost, Route("filter")]
        [ProducesResponseType(typeof(ResponseObject<List<ProductBaseModel>>), StatusCodes.Status200OK)]
        public async Task<Response> Filter([FromBody] ProductQueryFilter filter)
        {
            var result = await _handler.Filter(filter);

            return result;
        }

        [AllowAnonymous, HttpPost, Route("product-by-supplier")]
        [ProducesResponseType(typeof(ResponseObject<List<ProductBaseModel>>), StatusCodes.Status200OK)]
        public async Task<Response> GetListProductBySupplier([FromBody] ProductQueryFilter filter)
        {
            var result = await _handler.GetListProductBySupplier(filter);

            return result;
        }

        [AllowAnonymous, HttpPost, Route("update-visit-count")]
        [ProducesResponseType(typeof(ResponseObject<List<ProductBaseModel>>), StatusCodes.Status200OK)]
        public async Task<Response> UpdateVisitCount(VisitCountModel model)
        {
            var result = await _handler.UpdateVisitCount(model);

            return result;
        }
        /// <summary>
        /// Lấy tất cả danh sách sản phẩm
        /// </summary> 
        /// <param name="ts">Từ khóa tìm kiếm</param>
        /// <returns>Danh sách sản phẩm</returns> 
        /// <response code="200">Thành công</response>
        [AllowAnonymous, HttpGet, Route("all")]
        [ProducesResponseType(typeof(ResponseObject<List<ProductBaseModel>>), StatusCodes.Status200OK)]
        public async Task<Response> GetAll(string ts = null)
        {
            ProductQueryFilter filter = new ProductQueryFilter()
            {
                TextSearch = ts,
                PageNumber = null,
                PageSize = null
            };
            var result = await _handler.Filter(filter);
            return result;
        }
        /// <summary>
        /// Xóa sản phẩm
        /// </summary> 
        /// <remarks>
        /// Sample request:
        ///
        ///     [
        ///         "3fa85f64-5717-4562-b3fc-2c963f66afa6"
        ///     ]
        /// </remarks>
        /// <param name="listId">Danh sách Id sản phẩm</param>
        /// <returns>Danh sách kết quả xóa</returns> 
        /// <response code="200">Thành công</response>
        [AllowAnonymous, HttpDelete, Route("")]
        [ProducesResponseType(typeof(ResponseObject<List<ResponeDeleteModel>>), StatusCodes.Status200OK)]
        public async Task<IActionResult> Delete([FromBody] List<Guid> listId)
        {
            return await ExecuteFunction(async (RequestUser u) =>
            {
                var result = await _handler.Delete(listId);

                return result;
            });
        }

        /// <summary>
        /// Lấy danh sách sản phẩm cho combobox
        /// </summary> 
        /// <param name="count">số bản ghi tối đa</param>
        /// <param name="ts">Từ khóa tìm kiếm</param>
        /// <returns>Danh sách sản phẩm</returns> 
        /// <response code="200">Thành công</response>
        [AllowAnonymous, HttpGet, Route("for-combobox")]
        [ProducesResponseType(typeof(ResponseObject<List<SelectItemModel>>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetListCombobox(int count = 0, string ts = "")
        {
            return await ExecuteFunction(async (RequestUser u) =>
            {
                var result = await _handler.GetListCombobox(count, ts);

                return result;
            });
        }
    }
}
