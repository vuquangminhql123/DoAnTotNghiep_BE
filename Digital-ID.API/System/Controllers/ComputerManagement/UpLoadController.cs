﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using DigitalID.Data;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Serilog;

namespace DigitalID.API
{
    [Route("api/server-upload")]
    [ApiController]
    public class UpLoadController : ControllerBase
    {
        /// <summary>
        /// Upload file
        /// </summary> 
        /// <param name="id">File</param>
        /// <returns>UrlFile</returns> 
        /// <response code="200">Thành công</response>
        [HttpPost, Route("upload"), DisableRequestSizeLimit]
        public async Task<IActionResult> UploadLogo()
        {
            try
            {
                var file = Request.Form.Files[0];
                var folderName = Path.Combine("wwwroot", "Images");
                var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), folderName);
                if (file.Length > 0)
                {
                    var fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
                    var fullPath = Path.Combine(pathToSave, fileName);
                    var dbPath = Path.Combine(folderName, fileName);
                    using (var stream = System.IO.File.Create(fullPath))
                    {
                        await file.CopyToAsync(stream);
                    }

                    return Ok(new { dbPath });
                }

                return BadRequest();
            }
            catch (Exception ex)
            {
                Log.Error(ex, MessageConstants.ErrorLogMessage);
                return StatusCode(500, "Internal Server");
            }
        }
    }
}
