﻿using DigitalID.API;
using DigitalID.Business;
using DigitalID.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MISBI.API
{
    /// <summary>
    /// Module khách hàng
    /// </summary>
    [ApiVersion("1.0")]
    [ApiController]
    [Route("api/v{api-version:apiVersion}/crm/account")]
    [ApiExplorerSettings(GroupName = "CRM Account (Customer Relations Management)")]
    public class CrmAccountController : ApiControllerBase
    {
        private readonly ICrmAccountHandler _handler;
        public CrmAccountController(ICrmAccountHandler handler)
        {
            _handler = handler;
        }

        /// <summary>
        /// Thêm mới khách hàng - khách hàng tự đăng ký
        /// </summary>
        /// <param name="model">Thông tin khách hàng</param>
        /// <returns>Id khách hàng</returns> 
        /// <response code="200">Thành công</response>
        [AllowAnonymous, HttpPost, Route("register")]
        [ProducesResponseType(typeof(ResponseObject<CrmAccountQRCodeModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> Register([FromBody] CrmAccountCreateModel model)
        {
            var result = await _handler.Create(model);
            return Helper.TransformData(result);
        }

        /// <summary>
        /// Thêm mới khách hàng
        /// </summary>
        /// <param name="model">Thông tin khách hàng</param>
        /// <returns>Id khách hàng</returns> 
        /// <response code="200">Thành công</response>
        [Authorize, HttpPost, Route("")]
        [ProducesResponseType(typeof(ResponseObject<CrmAccountQRCodeModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> Create([FromBody] CrmAccountCreateModel model)
        {
            return await ExecuteFunction(async (RequestUser u) =>
            {
                model.CreatedByUserId = u.UserId;
                model.ApplicationId = u.ApplicationId;
                var result = await _handler.Create(model);

                return result;
            });
        }

        /// <summary>
        /// Cập nhật khách hàng
        /// </summary> 
        /// <param name="model">Thông tin khách hàng cần cập nhật</param>
        /// <returns>Id khách hàng đã cập nhật thành công</returns> 
        /// <response code="200">Thành công</response>
        [Authorize, HttpPut, Route("")]
        [ProducesResponseType(typeof(ResponseObject<Guid>), StatusCodes.Status200OK)]
        public async Task<IActionResult> Update([FromBody] CrmAccountUpdateModel model)
        {
            return await ExecuteFunction(async (RequestUser u) =>
            {
                model.ModifiedUserId = u.UserId;
                var result = await _handler.Update(model);

                return result;
            });
        }

        /// <summary>
        /// Xóa khách hàng
        /// </summary> 
        /// <param name="listId">Danh sách Id khách hàng</param>
        /// <returns>Danh sách kết quả xóa</returns> 
        /// <response code="200">Thành công</response>
        [Authorize, HttpDelete, Route("")]
        [ProducesResponseType(typeof(ResponseObject<List<ResponeDeleteModel>>), StatusCodes.Status200OK)]
        public async Task<IActionResult> Delete([FromBody] List<Guid> listId)
        {
            return await ExecuteFunction(async (RequestUser u) =>
            {
                var result = await _handler.Delete(listId);

                return result;
            });
        }

        /// <summary>
        /// Lấy danh sách khách hàng theo điều kiện lọc
        /// </summary> 
        /// <param name="filter">Điều kiện lọc</param>
        /// <returns>Danh sách khách hàng</returns> 
        /// <response code="200">Thành công</response>
        [Authorize, HttpPost, Route("filter")]
        [ProducesResponseType(typeof(ResponseObject<List<CrmAccountBaseModel>>), StatusCodes.Status200OK)]
        public async Task<IActionResult> Filter([FromBody] CrmAccountQueryFilter filter)
        {
            return await ExecuteFunction(async (RequestUser u) =>
            {
                var result = await _handler.Filter(filter);

                return result;
            });
        }

        /// <summary>
        /// Lấy tất cả danh sách khách hàng
        /// </summary> 
        /// <param name="ts">Từ khóa tìm kiếm</param>
        /// <returns>Danh sách khách hàng</returns> 
        /// <response code="200">Thành công</response>
        [Authorize, HttpGet, Route("all")]
        [ProducesResponseType(typeof(ResponseObject<List<CrmAccountBaseModel>>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetAll(string ts = null)
        {
            return await ExecuteFunction(async (RequestUser u) =>
            {
                CrmAccountQueryFilter filter = new CrmAccountQueryFilter()
                {
                    TextSearch = ts,
                    PageNumber = null,
                    PageSize = null
                };
                var result = await _handler.Filter(filter);

                return result;
            });
        }

        /// <summary>
        /// Lấy thông tin khách hàng theo id
        /// </summary> 
        /// <param name="id">Id khách hàng</param>
        /// <returns>Thông tin chi tiết khách hàng</returns> 
        /// <response code="200">Thành công</response>
        [Authorize, HttpGet, Route("")]
        [ProducesResponseType(typeof(ResponseObject<CrmAccountModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetById(Guid id)
        {
            return await ExecuteFunction(async (RequestUser u) =>
            {
                var result = await _handler.GetById(id);

                return result;
            });
        }

        /// <summary>
        /// Lấy danh sách khách hàng cho combobox
        /// </summary> 
        /// <param name="count">số bản ghi tối đa</param>
        /// <param name="ts">Từ khóa tìm kiếm</param>
        /// <returns>Danh sách khách hàng</returns> 
        /// <response code="200">Thành công</response>
        [Authorize, HttpGet, Route("for-combobox")]
        [ProducesResponseType(typeof(ResponseObject<List<SelectItemModel>>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetListCombobox(int count = 0, string ts = "")
        {
            return await ExecuteFunction(async (RequestUser u) =>
            {
                var result = await _handler.GetListCombobox(count, ts);

                return result;
            });
        }
    }
}
