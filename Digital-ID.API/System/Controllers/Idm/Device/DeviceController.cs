﻿using DigitalID.Business;
using DigitalID.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DigitalID.API
{
    /// <inheritdoc />
    /// <summary>
    /// Module người dùng
    /// </summary>
    [ApiVersion("1.0")]
    [ApiController]
    [Route("api/v{api-version:apiVersion}/idm_device")]
    [ApiExplorerSettings(GroupName = "IDM Device", IgnoreApi = true)]
    public class DeviceController : ControllerBase
    {        
        private readonly IHubContext<SignRHub> _hubContext;
        private readonly IDeviceHandler _deviceHandler;
        public DeviceController(IHubContext<SignRHub> hubContext, IDeviceHandler deviceHandler)
        {
            _hubContext = hubContext;
            _deviceHandler = deviceHandler;
        }

        /*
        /// <summary>
        /// Scan QR Code
        /// </summary>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [Authorize, HttpPost, Route("update")]
        public async Task<ResponseV2<int>> UpdateAsync([FromBody] DeviceCreateUpdateModel data)
        {
            try
            {
                if (Helper.ValidAuthen(Request))
                {
                    data.UserId = new Guid(User.Identity.Name);
                    var rt = _deviceHandler.UpdateAsync(data, data.UserId);
                    await _hubContext.Clients.All.SendAsync("SCANQRCODE", rt.Message);
                    return rt;
                }
                else
                {
                    return new ResponseV2<int>(0, "Token is expired", 0);
                }
            }
            catch (Exception ex)
            {
                return new ResponseV2<int>(-1, ex.Message, 0);
            }

        }
        */
    }
}
