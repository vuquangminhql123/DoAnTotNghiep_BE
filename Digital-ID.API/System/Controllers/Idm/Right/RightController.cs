﻿using DigitalID.Business;
using DigitalID.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Serilog;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
namespace DigitalID.API
{
    /// <inheritdoc />
    /// <summary>
    /// Module quyền hệ thống
    /// </summary>
    [ApiVersion("1.0")]
    [ApiController]
    [Route("api/v{api-version:apiVersion}/idm/rights")]
    [ApiExplorerSettings(GroupName = "IDM Right", IgnoreApi = true)]
    public class IdmRightController : ControllerBase
    {
        private readonly IRightHandler _rightHandler;
        private readonly IRightMapRoleHandler _rightMapRoleHandler;
        private readonly IRightMapUserHandler _rightMapUserHandler;
        public IdmRightController(IRightHandler rightHandler, IRightMapRoleHandler rightMapRoleHandler, IRightMapUserHandler rightMapUserHandler)
        {
            _rightHandler = rightHandler;
            _rightMapRoleHandler = rightMapRoleHandler;
            _rightMapUserHandler = rightMapUserHandler;
        }
        #region CRUD

        /// <summary>
        /// Thêm mới
        /// </summary>
        /// <param name="model">Dữ liệu</param>
        /// <param name="applicationId"></param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [Authorize, HttpPost, Route("")]
        [ProducesResponseType(typeof(ResponseObject<RightModel>), StatusCodes.Status200OK)]
        // [SwaggerRequestExample(typeof(RightCreateModel), typeof(MockupObject<RightCreateModel>))]
        public async Task<IActionResult> CreateAsync([FromBody] RightCreateModel model, [FromQuery] Guid? applicationId = null)
        {
            // Get Token Info
            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            var actorId = requestInfo.UserId;
            var appId = applicationId ?? requestInfo.ApplicationId;
            // Call service
            var result = await _rightHandler.CreateAsync(model, appId, actorId);
            if (result.Code == Code.Success)
            {
                Log.ForContext("ApplicationId", actorId)
                        .ForContext("UserId", actorId)
                        .Information("Thêm mới quyền: " + model.Name);
            }
            // Hander response
            return Helper.TransformData(result);
        }

        /// <summary>
        /// Cập nhật
        /// </summary>
        /// <param name="id">Id bản ghi</param>
        /// <param name="model">Dữ liệu</param>
        /// <param name="applicationId"></param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [Authorize, HttpPut, Route("{id}")]
        [ProducesResponseType(typeof(ResponseUpdate), StatusCodes.Status200OK)]
        // [SwaggerRequestExample(typeof(RightUpdateModel), typeof(MockupObject<RightUpdateModel>))]
        public async Task<IActionResult> UpdateAsync(Guid id, [FromBody] RightUpdateModel model, [FromQuery] Guid? applicationId = null)
        {
            // Get Token Info
            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            var actorId = requestInfo.UserId;
            var appId = applicationId ?? requestInfo.ApplicationId;
            // Call service
            var result = await _rightHandler.UpdateAsync(id, model, appId, actorId);

            if (result.Code == Code.Success)
            {
                Log.ForContext("ApplicationId", actorId)
                        .ForContext("UserId", actorId)
                        .Information("Cập nhật quyền: " + model.Name);
            }
            // Hander response
            return Helper.TransformData(result);
        }
        /// <summary>
        /// Xóa
        /// </summary>
        /// <param name="id">Id bản ghi</param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [Authorize, HttpDelete, Route("{id}")]
        [ProducesResponseType(typeof(ResponseDelete), StatusCodes.Status200OK)]
        public async Task<IActionResult> DeleteAsync(Guid id)
        {
            // Get Token Info
            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            var actorId = requestInfo.UserId;
            var appId = requestInfo.ApplicationId;
            // Call service
            var result = await _rightHandler.DeleteAsync(id);
            if (result.Code == Code.Success && result is ResponseDelete resultData)
            {
                Log.ForContext("ApplicationId", actorId)
                        .ForContext("UserId", actorId)
                        .Information("Cập nhật quyền: " + resultData.Data.Name);
            }
            // Hander response
            return Helper.TransformData(result);
        }
        /// <summary>
        /// Xóa danh sách
        /// </summary>
        /// <param name="listId">Danh sách id bản ghi</param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [Authorize, HttpDelete, Route("")]
        [ProducesResponseType(typeof(ResponseDeleteMulti), StatusCodes.Status200OK)]
        public async Task<IActionResult> DeleteRangeAsync([FromQuery]List<Guid> listId)
        {
            // Get Token Info
            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            var actorId = requestInfo.UserId;
            var appId = requestInfo.ApplicationId;
            // Call service 
            var result = await _rightHandler.DeleteRangeAsync(listId);
            if (result.Code == Code.Success && result is ResponseDeleteMulti resultData)
            {
                var listDeleteName = "";
                foreach (var item in resultData.Data)
                {
                    listDeleteName += item.Data.Name + ", ";
                }
                Log.ForContext("ApplicationId", actorId)
                        .ForContext("UserId", actorId)
                        .Information("Cập nhật quyền: " + listDeleteName);
            }
            // Hander response
            return Helper.TransformData(result);
        }
        /// <summary>
        /// Lấy về theo Id
        /// </summary>
        /// <param name="id">Id bản ghi</param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [Authorize, HttpGet, Route("{id}")]
        [ProducesResponseType(typeof(ResponseObject<RightModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetByIdAsync(Guid id)
        {
            // Get Token Info
            // var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            // var actorId = requestInfo.UserId;
            // var appId = applicationId??requestInfo.ApplicationId;
            // Call service
            var result = await _rightHandler.FindAsync(id);
            // Hander response
            return Helper.TransformData(result);
        }
        /// <summary>
        /// Lấy về theo bộ loc
        /// </summary>
        /// <param name="page">Số thứ tự trang tìm kiếm</param>
        /// <param name="size">Số bản ghi giới hạn một trang</param>
        /// <param name="filter">Thông tin lọc nâng cao (Object Json)</param>
        /// <param name="sort">Thông tin sắp xếp (Array Json)</param>
        /// <returns></returns>
        /// <remarks>
        ///  *filter*
        ///  ....
        ///  *sort*
        ///  ....
        /// </remarks>
        /// <response code="200">Thành công</response>
        [Authorize, HttpGet, Route("")]
        [ProducesResponseType(typeof(ResponsePagination<RightModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetFilterAsync([FromQuery]int page = 1, [FromQuery]int size = 20, [FromQuery]string filter = "{}", [FromQuery]string sort = "")
        {
            // Get Token Info
            // var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            // var actorId = requestInfo.UserId;
            // var appId = applicationId??requestInfo.ApplicationId;
            // Call service
            var filterObject = JsonConvert.DeserializeObject<RightQueryModel>(filter);
            filterObject.Sort = sort;
            if (string.IsNullOrEmpty(filterObject.Sort))
            {
                filterObject.Sort = "+Code";
            }
            filterObject.Size = size;
            filterObject.Page = page;
            var result = await _rightHandler.GetPageAsync(filterObject);
            // Hander response
            return Helper.TransformData(result);
        }
        /// <summary>
        /// Lấy về tất cả
        /// </summary> 
        /// <returns></returns> 
        /// <response code="200">Thành công</response>
        [Authorize, HttpGet, Route("all")]
        [ProducesResponseType(typeof(ResponseList<RightModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetAllAsync([FromQuery]string filter = "{}")
        {
            // Get Token Info
            // var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            // var actorId = requestInfo.UserId;
            // var appId = applicationId??requestInfo.ApplicationId;
            // Call service
            var filterObject = JsonConvert.DeserializeObject<RightQueryModel>(filter);
            var result = await _rightHandler.GetAllAsync(filterObject);
            // Hander response
            return Helper.TransformData(result);
        }
        #endregion

        /// <summary>
        /// Lấy về chi tiết quyền
        /// </summary> 
        /// <param name="id">Id bản ghi</param>
        /// <param name="applicationId"></param>
        /// <returns></returns> 
        /// <response code="200">Thành công</response>
        [Authorize, HttpGet, Route("{id}/detail")]
        [ProducesResponseType(typeof(ResponseList<RightDetailModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetDetail(Guid id, [FromQuery] Guid? applicationId = null)
        {
            // Get Token Info
            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            // var actorId = requestInfo.UserId;
            var appId = applicationId ?? requestInfo.ApplicationId;
            // Call service
            var result = await _rightHandler.GetDetail(id, appId);
            // Hander response
            return Helper.TransformData(result);
        }

        /// <summary>
        /// Lấy về danh sách người dùng có quyền
        /// </summary> 
        /// <param name="id">Id bản ghi</param>
        /// <param name="applicationId"></param>
        /// <returns></returns> 
        /// <response code="200">Thành công</response>
        [Authorize, HttpGet, Route("{id}/user")]
        [ProducesResponseType(typeof(ResponseList<BaseUserModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetUserMapRightAsync(Guid id, [FromQuery] Guid? applicationId = null)
        {
            // Get Token Info
            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            // var actorId = requestInfo.UserId;
            var appId = applicationId ?? requestInfo.ApplicationId;
            // Call service
            var result = await _rightMapUserHandler.GetUserMapRightAsync(id, appId);
            // Hander response
            return Helper.TransformData(result);
        }

        /// <summary>
        /// Lấy về danh sách nhóm có quyền
        /// </summary> 
        /// <param name="id">Id bản ghi</param>
        /// <param name="applicationId"></param>
        /// <returns></returns> 
        /// <response code="200">Thành công</response>
        [Authorize, HttpGet, Route("{id}/role")]
        [ProducesResponseType(typeof(ResponseList<BaseRoleModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetRoleMapRightAsync(Guid id, [FromQuery] Guid? applicationId = null)
        {
            // Get Token Info
            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            // var actorId = requestInfo.UserId;
            var appId = applicationId ?? requestInfo.ApplicationId;
            // Call service
            var result = await _rightMapRoleHandler.GetRoleMapRightAsync(id, appId);
            // Hander response
            return Helper.TransformData(result);
        }

        /// <summary>
        /// Gán quyền vào nhóm
        /// </summary> 
        /// <param name="id">Id bản ghi</param>
        /// <param name="listRoleId"></param>
        /// <param name="applicationId"></param>
        /// <returns></returns> 
        /// <response code="200">Thành công</response>
        [Authorize, HttpPost, Route("{id}/role")]
        [ProducesResponseType(typeof(Response), StatusCodes.Status200OK)]
        public async Task<IActionResult> AddRoleMapRightAsync([FromBody]IList<Guid> listRoleId, Guid id, [FromQuery] Guid? applicationId = null)
        {
            // Get Token Info
            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            var actorId = requestInfo.UserId;
            var appId = applicationId ?? requestInfo.ApplicationId;
            // Call service
            var result = await _rightMapRoleHandler.AddRightMapRoleAsync(listRoleId, id, appId, appId, actorId);
            // Hander response
            return Helper.TransformData(result);
        }

        /// <summary>
        /// Gán quyền cho người dùng
        /// </summary> 
        /// <param name="id">Id bản ghi</param>
        /// <param name="listUserId"></param>
        /// <param name="applicationId"></param>
        /// <returns></returns> 
        /// <response code="200">Thành công</response>
        [Authorize, HttpPost, Route("{id}/user")]
        [ProducesResponseType(typeof(Response), StatusCodes.Status200OK)]
        public async Task<IActionResult> AddRightMapUserAsync([FromBody]IList<Guid> listUserId, Guid id, [FromQuery] Guid? applicationId = null)
        {
            // Get Token Info
            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            var actorId = requestInfo.UserId;
            var appId = applicationId ?? requestInfo.ApplicationId;
            // Call service
            var result = await _rightMapUserHandler.AddRightMapUserAsync(listUserId, id, appId, appId, actorId);
            // Hander response
            return Helper.TransformData(result);
        }

        /// <summary>
        /// Gán quyền vào nhóm
        /// </summary> 
        /// <param name="id">Id bản ghi</param>
        /// <param name="listRoleId"></param>
        /// <param name="applicationId"></param>
        /// <returns></returns> 
        /// <response code="200">Thành công</response>
        [Authorize, HttpDelete, Route("{id}/role")]
        [ProducesResponseType(typeof(Response), StatusCodes.Status200OK)]
        public async Task<IActionResult> DeleteRightMapRoleAsync([FromBody]IList<Guid> listRoleId, Guid id, [FromQuery] Guid? applicationId = null)
        {
            // Get Token Info
            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            // var actorId = requestInfo.UserId;
            var appId = applicationId ?? requestInfo.ApplicationId;
            // Call service
            var result = await _rightMapRoleHandler.DeleteRightMapRoleAsync(listRoleId, id, appId);
            // Hander response
            return Helper.TransformData(result);
        }

        /// <summary>
        /// Xóa khỏi người dùng
        /// </summary> 
        /// <param name="id">Id bản ghi</param>
        /// <param name="listUserId"></param>
        /// <param name="applicationId"></param>
        /// <returns></returns> 
        /// <response code="200">Thành công</response>
        [Authorize, HttpDelete, Route("{id}/user")]
        [ProducesResponseType(typeof(Response), StatusCodes.Status200OK)]
        public async Task<IActionResult> DeleteRightMapUserAsync([FromBody]IList<Guid> listUserId, Guid id, [FromQuery] Guid? applicationId = null)
        {
            // Get Token Info
            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            // var actorId = requestInfo.UserId;
            var appId = applicationId ?? requestInfo.ApplicationId;
            // Call service
            var result = await _rightMapUserHandler.DeleteRightMapUserAsync(listUserId, id, appId);
            // Hander response
            return Helper.TransformData(result);
        }
    }
}
