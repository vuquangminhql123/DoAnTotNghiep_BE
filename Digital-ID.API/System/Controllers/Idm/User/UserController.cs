﻿using DigitalID.Business;
using DigitalID.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DigitalID.API
{
    /// <inheritdoc />
    /// <summary>
    /// Module người dùng
    /// </summary>
    [ApiVersion("1.0")]
    [ApiController]
    [Route("api/v{api-version:apiVersion}/idm/users")]
    [ApiExplorerSettings(GroupName = "IDM User")]
    public class IdmUserController : ControllerBase
    {
        private readonly IUserHandler _userHandler;
        private readonly IRightMapUserHandler _rightMapUserHandler;
        private readonly IUserMapRoleHandler _userMapRoleHandler;
        private readonly IUserMapDVHandler _userMapDVHandler;
        public IdmUserController(IUserHandler userHandler, IRightMapUserHandler rightMapUserHandler, IUserMapRoleHandler userMapRoleHandler, IUserMapDVHandler userMapDVHandler)
        {
            _userHandler = userHandler;
            _rightMapUserHandler = rightMapUserHandler;
            _userMapRoleHandler = userMapRoleHandler;
            _userMapDVHandler = userMapDVHandler;
        }

        #region Cert
        /// <summary>
        /// Tạo khóa
        /// </summary>
        /// <param name="model">Dữ liệu</param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [Authorize, HttpPost, Route("genkey")]
        [ProducesResponseType(typeof(ResponseUpdate), StatusCodes.Status200OK)]
        public async Task<IActionResult> CreateKey([FromBody] CertCreatModel model)
        {
            // Get Token Info
            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            // Call service
            var result = await _userHandler.CreateKey(model, requestInfo.ApplicationId, requestInfo.UserId);

            // Hander response
            return Helper.TransformData(result);
        }

        [Authorize, HttpGet, Route("getcertificateprofile")]
        [ProducesResponseType(typeof(ResponseObject<CertificateProfileRespone>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetCertificateProfile([FromQuery] string userId)
        {
            // Get Token Info
            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            // Call service
            var result = await _userHandler.GetCertificateProfile();

            // Hander response
            return Helper.TransformData(result);
        }

        /// <summary>
        /// Tạo chứng thư số
        /// </summary>
        /// <param name="model">Dữ liệu</param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [Authorize, HttpPost, Route("gencert")]
        [ProducesResponseType(typeof(ResponseUpdate), StatusCodes.Status200OK)]
        public async Task<IActionResult> CreateCSR([FromBody] CertCreatModel model)
        {
            // Get Token Info
            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            // Call service
            var result = await _userHandler.CreateCert(model, requestInfo.ApplicationId, requestInfo.UserId);

            // Hander response
            return Helper.TransformData(result);
        }

        /// <summary>
        /// Tải chứng thư số
        /// </summary>
        /// <param name="userId">Dữ liệu</param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [Authorize, HttpGet, Route("downloadcert")]
        [ProducesResponseType(typeof(ResponseObject<string>), StatusCodes.Status200OK)]
        public IActionResult DowloadCert([FromQuery] string userId)
        {
            // Get Token Info
            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            // Call service
            var result = _userHandler.DowloadCert(new Guid(userId), requestInfo.ApplicationId, requestInfo.UserId);

            // Hander response
            return Helper.TransformData(result);
        }
        #endregion

        #region QR Code
        /// <summary>
        /// Tạo qr code
        /// </summary>
        /// <param name="model">Dữ liệu</param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [Authorize, HttpPost, Route("genqrcode")]
        [ProducesResponseType(typeof(ResponseObject<string>), StatusCodes.Status200OK)]
        public IActionResult CreateQRCode([FromBody] QrCodeInputModel model)
        {
            // Get Token Info
            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            // Call service
            var result = _userHandler.CreateQRCode(model, requestInfo.ApplicationId, requestInfo.UserId);

            // Hander response
            return Helper.TransformData(result);
        }
        #endregion

        #region TOTP
        /// <summary>
        /// Register TOTP
        /// </summary>
        /// <param name="username"></param>
        /// <returns>QR code</returns>
        [Authorize, HttpGet, Route("totp")]
        public async Task<ResponseObject<string>> RegisterTOTP(string username)
        {
            // Get Token Info
            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            // Call service
            var result = await _userHandler.RegisterTOTPAsync(username, requestInfo.ApplicationId, requestInfo.UserId);

            // Hander response
            return result;
        }
        #endregion

        #region CRUD

        /// <summary>
        /// Thêm mới
        /// </summary>
        /// <param name="model">Dữ liệu</param>
        /// <param name="applicationId"></param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [Authorize, HttpPost, Route("")]
        [ProducesResponseType(typeof(ResponseObject<UserModel>), StatusCodes.Status200OK)]
        // [SwaggerRequestExample(typeof(UserCreateModel), typeof(MockupObject<UserCreateModel>))]
        public async Task<IActionResult> CreateAsync([FromBody] UserCreateModel model, [FromQuery] Guid? applicationId = null)
        {
            // Get Token Info
            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            // var actorId = requestInfo.UserId;
            var appId = applicationId ?? requestInfo.ApplicationId;
            // Call service
            var result = await _userHandler.CreateAsync(model, appId);
            // Hander response
            return Helper.TransformData(result);
        }
        /// <summary>
        /// Cập nhật mật khẩu
        /// </summary>
        /// <param name="id">Id bản ghi</param>
        /// <param name="password">Mật khẩu mới</param>
        /// <param name="currentPassword">Mật khẩu mới</param>
        /// <param name="applicationId"></param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [Authorize, HttpPut, Route("{id}/changepassword")]
        [ProducesResponseType(typeof(ResponseUpdate), StatusCodes.Status200OK)]
        public async Task<IActionResult> ChangePasswordAsync(Guid id, [FromQuery] string password, [FromQuery] Guid? applicationId = null, [FromQuery] string currentPassword = null)
        {
            // Get Token Info
            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            // var actorId = requestInfo.UserId;
            var appId = applicationId ?? requestInfo.ApplicationId;
            // Call service
            var result = await _userHandler.ChangePasswordAsync(id, password, appId, currentPassword);

            // Hander response
            return Helper.TransformData(result);
        }
        

        /// <summary>
        /// Cập nhật
        /// </summary>
        /// <param name="id">Id bản ghi</param>
        /// <param name="model">Dữ liệu</param>
        /// <param name="applicationId"></param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [Authorize, HttpPut, Route("{id}")]
        [ProducesResponseType(typeof(ResponseUpdate), StatusCodes.Status200OK)]
        // [SwaggerRequestExample(typeof(UserUpdateModel), typeof(MockupObject<UserUpdateModel>))]
        public async Task<IActionResult> UpdateAsync(Guid id, [FromBody] UserUpdateModel model, [FromQuery] Guid? applicationId = null)
        {
            // Get Token Info
            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            // var actorId = requestInfo.UserId;
            var appId = applicationId ?? requestInfo.ApplicationId;
            // Call service
            var result = await _userHandler.UpdateAsync(id, model, appId);

            // Hander response
            return Helper.TransformData(result);
        }
        /// <summary>
        /// Xóa
        /// </summary>
        /// <param name="id">Id bản ghi</param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [Authorize, HttpDelete, Route("{id}")]
        [ProducesResponseType(typeof(ResponseDelete), StatusCodes.Status200OK)]
        public async Task<IActionResult> DeleteAsync(Guid id)
        {
            // Get Token Info
            // var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            // var actorId = requestInfo.UserId;
            // var appId = applicationId??requestInfo.ApplicationId;
            // Call service
            var result = await _userHandler.DeleteAsync(id);
            // Hander response
            return Helper.TransformData(result);
        }
        /// <summary>
        /// Xóa danh sách
        /// </summary>
        /// <param name="listId">Danh sách id bản ghi</param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [Authorize, HttpDelete, Route("")]
        [ProducesResponseType(typeof(ResponseDeleteMulti), StatusCodes.Status200OK)]
        public async Task<IActionResult> DeleteRangeAsync([FromQuery]List<Guid> listId)
        {
            // Get Token Info
            // var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            // var actorId = requestInfo.UserId;
            // var appId = applicationId??requestInfo.ApplicationId;
            // Call service 
            var result = await _userHandler.DeleteRangeAsync(listId);
            // Hander response
            return Helper.TransformData(result);
        }
        /// <summary>
        /// Lấy về theo Id
        /// </summary>
        /// <param name="id">Id bản ghi</param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [Authorize, HttpGet, Route("{id}")]
        [ProducesResponseType(typeof(ResponseObject<UserModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetByIdAsync(Guid id)
        {
            // Get Token Info
            // var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            // var actorId = requestInfo.UserId;
            // var appId = applicationId??requestInfo.ApplicationId;
            // Call service
            var result = await _userHandler.FindAsync(id);
            // Hander response
            return Helper.TransformData(result);
        }
        /// <summary>
        /// Lấy về theo bộ loc
        /// </summary>
        /// <param name="page">Số thứ tự trang tìm kiếm</param>
        /// <param name="size">Số bản ghi giới hạn một trang</param>
        /// <param name="filter">Thông tin lọc nâng cao (Object Json)</param>
        /// <param name="sort">Thông tin sắp xếp (Array Json)</param>
        /// <returns></returns>
        /// <remarks>
        ///  *filter*
        ///  ....
        ///  *sort*
        ///  ....
        /// </remarks>
        /// <response code="200">Thành công</response>
        [Authorize, HttpGet, Route("")]
        [ProducesResponseType(typeof(ResponsePagination<UserModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetFilterAsync([FromQuery]int page = 1, [FromQuery]int size = 20, [FromQuery]string filter = "{}", [FromQuery]string sort = "")
        {
            // Get Token Info
            // var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            // var actorId = requestInfo.UserId;
            // var appId = applicationId??requestInfo.ApplicationId;
            // Call service
            var filterObject = JsonConvert.DeserializeObject<UserQueryModel>(filter);
            filterObject.Sort = sort;
            if (string.IsNullOrEmpty(filterObject.Sort))
            {
                filterObject.Sort = "+LastModifiedOnDate";
            }
            filterObject.Size = size;
            filterObject.Page = page;
            var result = await _userHandler.GetPageAsync(filterObject);
            // Hander response
            return Helper.TransformData(result);
        }

        /// <summary>
        /// Lấy về theo bộ loc
        /// </summary>
        /// <param name="page">Số thứ tự trang tìm kiếm</param>
        /// <param name="size">Số bản ghi giới hạn một trang</param>
        /// <param name="filter">Thông tin lọc nâng cao (Object Json)</param>
        /// <param name="sort">Thông tin sắp xếp (Array Json)</param>
        /// <returns></returns>
        /// <remarks>
        ///  *filter*
        ///  ....
        ///  *sort*
        ///  ....
        /// </remarks>
        /// <response code="200">Thành công</response>
        [Authorize, HttpGet, Route("customer")]
        [ProducesResponseType(typeof(ResponsePagination<UserModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetFilterCustomerAsync([FromQuery]int page = 1, [FromQuery]int size = 20, [FromQuery]string filter = "{}", [FromQuery]string sort = "")
        {
            // Get Token Info
            // var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            // var actorId = requestInfo.UserId;
            // var appId = applicationId??requestInfo.ApplicationId;
            // Call service
            var filterObject = JsonConvert.DeserializeObject<UserQueryModel>(filter);
            filterObject.Sort = sort;
            if (string.IsNullOrEmpty(filterObject.Sort))
            {
                filterObject.Sort = "+LastModifiedOnDate";
            }
            filterObject.Size = size;
            filterObject.Page = page;
            filterObject.Type = 2;
            var result = await _userHandler.GetPageAsync(filterObject);
            // Hander response
            return Helper.TransformData(result);
        }

        /// <summary>
        /// Lấy về tất cả
        /// </summary> 
        /// <returns></returns> 
        /// <response code="200">Thành công</response>
        [Authorize, HttpGet, Route("all")]
        [ProducesResponseType(typeof(ResponseList<UserModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetAllAsync([FromQuery]string filter = "{}")
        {
            // Get Token Info
            // var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            // var actorId = requestInfo.UserId;
            // var appId = applicationId??requestInfo.ApplicationId;
            // Call service
            var filterObject = JsonConvert.DeserializeObject<UserQueryModel>(filter);
            var result = await _userHandler.GetAllAsync(filterObject);
            // Hander response
            return Helper.TransformData(result);
        }
        #endregion


        /// <summary>
        /// Khóa người dùng
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns> 
        /// <response code="200">Thành công</response>
        [Authorize, HttpPut, Route("{id}/lock")]
        [ProducesResponseType(typeof(ResponseUpdate), StatusCodes.Status200OK)]
        public async Task<IActionResult> LockUser(Guid id)
        {
            // Get Token Info
            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            // Call service
            var result = await _userHandler.LockUser(id);
            // Hander response
            return Helper.TransformData(result);
        }
        /// <summary>
        /// Mở khóa người dùng
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns> 
        /// <response code="200">Thành công</response>
        [Authorize, HttpPut, Route("{id}/unlock")]
        [ProducesResponseType(typeof(ResponseUpdate), StatusCodes.Status200OK)]
        public async Task<IActionResult> UnLockUser(Guid id)
        {
            // Get Token Info
            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            // Call service
            var result = await _userHandler.UnLockUser(id);
            // Hander response
            return Helper.TransformData(result);
        }

        /// <summary>
        /// Cập nhật thông tin người dùng
        /// </summary>
        /// <param name="id"></param>
        /// <param name="model"></param>
        /// <returns></returns> 
        /// <response code="200">Thành công</response>
        [Authorize, HttpPut, Route("{id}/patch")]
        [ProducesResponseType(typeof(ResponseUpdate), StatusCodes.Status200OK)]
        public async Task<IActionResult> PatchUserInfo(Guid id, [FromBody]UserPatchModel model)
        {
            // Get Token Info
            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            // Call service
            var result = await _userHandler.PatchUserInfo(id, model);
            // Hander response
            return Helper.TransformData(result);
        }
        
        /// <summary>
        /// Kiểm tra tên
        /// </summary>
        /// <param name="name"></param>
        /// <param name="applicationId"></param> 
        /// <returns></returns> 
        /// <response code="200">Thành công</response>
        [Authorize, HttpGet, Route("checkname/{name}")]
        [ProducesResponseType(typeof(ResponseList<UserDetailModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> CheckNameAvailability(string name, [FromQuery] Guid? applicationId = null)
        {
            // Get Token Info
            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            // var actorId = requestInfo.UserId;
            var appId = applicationId ?? requestInfo.ApplicationId;
            // Call service
            var result = await _userHandler.CheckNameAvailability(name, appId);
            // Hander response
            return Helper.TransformData(result);
        }

        /// <summary>
        /// Lấy về chi tiết
        /// </summary> 
        /// <param name="id">Id bản ghi</param>
        /// <param name="applicationId"></param>
        /// <returns></returns> 
        /// <response code="200">Thành công</response>
        [Authorize, HttpGet, Route("{id}/detail")]
        [ProducesResponseType(typeof(ResponseList<UserDetailModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetDetail(Guid id, [FromQuery] Guid? applicationId = null)
        {
            // Get Token Info
            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            // var actorId = requestInfo.UserId;
            var appId = applicationId ?? requestInfo.ApplicationId;
            // Call service
            var result = await _userHandler.GetDetail(id, appId);
            // Hander response
            return Helper.TransformData(result);
        }

        /// <summary>
        /// Lấy về danh sách nhóm của người dùng
        /// </summary> 
        /// <param name="id">Id bản ghi</param>
        /// <param name="applicationId"></param>
        /// <returns></returns> 
        /// <response code="200">Thành công</response>
        [Authorize, HttpGet, Route("{id}/role")]
        [ProducesResponseType(typeof(ResponseList<BaseRoleModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetRoleMapUserAsync(Guid id, [FromQuery] Guid? applicationId = null)
        {
            // Get Token Info
            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            // var actorId = requestInfo.UserId;
            var appId = applicationId ?? requestInfo.ApplicationId;
            // Call service
            var result = await _userMapRoleHandler.GetRoleMapUserAsync(id, appId);
            // Hander response
            return Helper.TransformData(result);
        }

        /// <summary>
        /// Lấy về danh sách đơn vị của người dùng
        /// </summary> 
        /// <param name="id">Id bản ghi</param>
        /// <param name="applicationId"></param>
        /// <returns></returns> 
        /// <response code="200">Thành công</response>
        [Authorize, HttpGet, Route("{id}/dv")]
        [ProducesResponseType(typeof(ResponseList<BaseRoleModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetDVMapUserAsync(Guid id, [FromQuery] Guid? applicationId = null)
        {
            // Get Token Info
            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            // var actorId = requestInfo.UserId;
            var appId = applicationId ?? requestInfo.ApplicationId;
            // Call service
            var result = await _userMapDVHandler.GetDVMapUserAsync(id, appId);
            // Hander response
            return Helper.TransformData(result);
        }

        /// <summary>
        /// Lấy về danh sách quyền thuộc người dùng
        /// </summary> 
        /// <param name="id">Id bản ghi</param>
        /// <param name="applicationId"></param>
        /// <returns></returns> 
        /// <response code="200">Thành công</response>
        [Authorize, HttpGet, Route("{id}/right")]
        [ProducesResponseType(typeof(ResponseList<BaseRightModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetRightMapUserAsync(Guid id, [FromQuery] Guid? applicationId = null)
        {
            // Get Token Info
            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            // var actorId = requestInfo.UserId;
            var appId = applicationId ?? requestInfo.ApplicationId;
            // Call service
            var result = await _rightMapUserHandler.GetRightMapUserAsync(id, appId);
            // Hander response
            return Helper.TransformData(result);
        }

        /// <summary>
        /// Gán quyền vào người dùng
        /// </summary> 
        /// <param name="id">Id bản ghi</param>
        /// <param name="listRightId"></param>
        /// <param name="applicationId"></param>
        /// <returns></returns> 
        /// <response code="200">Thành công</response>
        [Authorize, HttpPost, Route("{id}/right")]
        [ProducesResponseType(typeof(Response), StatusCodes.Status200OK)]
        public async Task<IActionResult> AddRightMapRoleAsync([FromBody]IList<Guid> listRightId, Guid id, [FromQuery] Guid? applicationId = null)
        {
            // Get Token Info
            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            var actorId = requestInfo.UserId;
            var appId = applicationId ?? requestInfo.ApplicationId;
            // Call service
            var result = await _rightMapUserHandler.AddRightMapUserAsync(id, listRightId, appId, appId, actorId);
            // Hander response
            return Helper.TransformData(result);
        }

        /// <summary>
        /// Gán người dùng vào nhóm
        /// </summary> 
        /// <param name="id">Id bản ghi</param>
        /// <param name="listRoleId"></param>
        /// <param name="applicationId"></param>
        /// <returns></returns> 
        /// <response code="200">Thành công</response>
        [Authorize, HttpPost, Route("{id}/role")]
        [ProducesResponseType(typeof(Response), StatusCodes.Status200OK)]
        public async Task<IActionResult> AddUserMapRoleAsync([FromBody]IList<Guid> listRoleId, Guid id, [FromQuery] Guid? applicationId = null)
        {
            // Get Token Info
            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            var actorId = requestInfo.UserId;
            var appId = applicationId ?? requestInfo.ApplicationId;
            // Call service
            var result = await _userMapRoleHandler.AddUserMapRoleAsync(listRoleId, id, appId, appId, actorId);
            // Hander response
            return Helper.TransformData(result);
        }

        /// <summary>
        /// Gán người dùng vào đơn vị
        /// </summary> 
        /// <param name="id">Id bản ghi</param>
        /// <param name="listDVId"></param>
        /// <param name="applicationId"></param>
        /// <returns></returns> 
        /// <response code="200">Thành công</response>
        [Authorize, HttpPost, Route("{id}/dv")]
        [ProducesResponseType(typeof(Response), StatusCodes.Status200OK)]
        public async Task<IActionResult> AddUserMapDVAsync([FromBody]IList<Guid> listDVId, Guid id, [FromQuery] Guid? applicationId = null)
        {
            // Get Token Info
            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            var actorId = requestInfo.UserId;
            var appId = applicationId ?? requestInfo.ApplicationId;
            // Call service
            var result = await _userMapDVHandler.AddUserMapDVAsync(listDVId, id, appId, appId, actorId);
            // Hander response
            return Helper.TransformData(result);
        }

        /// <summary>
        /// Gỡ quyền khỏi người dùng
        /// </summary> 
        /// <param name="id">Id bản ghi</param>
        /// <param name="listRightId"></param>
        /// <param name="applicationId"></param>
        /// <returns></returns> 
        /// <response code="200">Thành công</response>
        [Authorize, HttpDelete, Route("{id}/right")]
        [ProducesResponseType(typeof(Response), StatusCodes.Status200OK)]
        public async Task<IActionResult> DeleteRightMapUserAsync([FromBody]IList<Guid> listRightId, Guid id, [FromQuery] Guid? applicationId = null)
        {
            // Get Token Info
            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            // var actorId = requestInfo.UserId;
            var appId = applicationId ?? requestInfo.ApplicationId;
            // Call service
            var result = await _rightMapUserHandler.DeleteRightMapUserAsync(id, listRightId, appId);
            // Hander response
            return Helper.TransformData(result);
        }

        /// <summary>
        /// Gỡ người khỏi vào nhóm
        /// </summary> 
        /// <param name="id">Id bản ghi</param>
        /// <param name="listRoleId"></param>
        /// <param name="applicationId"></param>
        /// <returns></returns> 
        /// <response code="200">Thành công</response>
        [Authorize, HttpDelete, Route("{id}/role")]
        [ProducesResponseType(typeof(Response), StatusCodes.Status200OK)]
        public async Task<IActionResult> DeleteUserMapRoleAsync([FromBody]IList<Guid> listRoleId, Guid id, [FromQuery] Guid? applicationId = null)
        {
            // Get Token Info
            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            // var actorId = requestInfo.UserId;
            var appId = applicationId ?? requestInfo.ApplicationId;
            // Call service
            var result = await _userMapRoleHandler.DeleteUserMapRoleAsync(listRoleId, id, appId);
            // Hander response
            return Helper.TransformData(result);
        }

        /// <summary>
        /// Gỡ người khỏi vào nhóm
        /// </summary> 
        /// <param name="id">Id bản ghi</param>
        /// <param name="listDVId"></param>
        /// <param name="applicationId"></param>
        /// <returns></returns> 
        /// <response code="200">Thành công</response>
        [Authorize, HttpDelete, Route("{id}/dv")]
        [ProducesResponseType(typeof(Response), StatusCodes.Status200OK)]
        public async Task<IActionResult> DeleteUserMapDVAsync([FromBody]IList<Guid> listDVId, Guid id, [FromQuery] Guid? applicationId = null)
        {
            // Get Token Info
            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            // var actorId = requestInfo.UserId;
            var appId = applicationId ?? requestInfo.ApplicationId;
            // Call service
            var result = await _userMapRoleHandler.DeleteUserMapRoleAsync(listDVId, id, appId);
            // Hander response
            return Helper.TransformData(result);
        }

        /// <summary>
        /// Bật quyền đối với người dùng
        /// </summary>
        /// <param name="id"></param>
        /// <param name="rightId"></param>
        /// <param name="applicationId"></param>
        /// <returns></returns>
        [Authorize, HttpPut, Route("{id}/right/enable/{rightId}")]
        [ProducesResponseType(typeof(Response), StatusCodes.Status200OK)]
        public async Task<IActionResult> EnableRightMapUserAsync(Guid id, Guid rightId, [FromQuery] Guid? applicationId = null)
        {
            // Get Token Info
            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            var actorId = requestInfo.UserId;
            var appId = applicationId ?? requestInfo.ApplicationId;
            // Call service
            var result = await _rightMapUserHandler.ToggleRightMapUserAsync(id, rightId, appId, true, appId, actorId);
            // Hander response
            return Helper.TransformData(result);
        }

        /// <summary>
        /// Tắt quyền đối với người dùng
        /// </summary>
        /// <param name="id"></param>
        /// <param name="rightId"></param>
        /// <param name="applicationId"></param>
        /// <returns></returns>
        [Authorize, HttpPut, Route("{id}/right/disable/{rightId}")]
        [ProducesResponseType(typeof(Response), StatusCodes.Status200OK)]
        public async Task<IActionResult> DisableRightMapUserAsync(Guid id, Guid rightId, [FromQuery] Guid? applicationId = null)
        {
            // Get Token Info
            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            var actorId = requestInfo.UserId;
            var appId = applicationId ?? requestInfo.ApplicationId;
            // Call service
            var result = await _rightMapUserHandler.ToggleRightMapUserAsync(id, rightId, appId, false, appId, actorId);
            // Hander response
            return Helper.TransformData(result);
        }
    }
}
