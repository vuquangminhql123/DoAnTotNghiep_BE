﻿using DigitalID.API;
using DigitalID.Business;
using DigitalID.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MISBI.API
{
    /// <summary>
    /// Module trang chủ
    /// </summary>
    [ApiVersion("1.0")]
    [ApiController]
    [Route("api/v{api-version:apiVersion}/dashboard")]
    [ApiExplorerSettings(GroupName = "Dashboard")]
    public class DashboardController : ApiControllerBase
    {
        private readonly IDashboardHandler _handler;
        public DashboardController(IDashboardHandler handler)
        {
            _handler = handler;
        }

        /// <summary>
        /// Lấy danh sách số lượng khách hàng đăng ký theo ngày
        /// </summary>
        /// <param name="model">Ngày bắt đầu ngày kết thúc</param>
        /// <returns>Danh sách số lượng khách hàng đăng ký theo ngày</returns> 
        /// <response code="200">Thành công</response>
        [HttpPost, Route("number-of-account-register")]
        [ProducesResponseType(typeof(ResponseObject<List<StatisticalAccountRegisterModel>>), StatusCodes.Status200OK)]
        public async Task<IActionResult> StatisticalAccountRegister([FromBody] StartDateEndDateModel model)
        {
            return await ExecuteFunction((RequestUser u) =>
            {
                var result = _handler.StatisticalAccountRegister(model);
                return Helper.TransformData(result);
            });
        }

        /// <summary>
        /// Lấy danh sách số lượng khách hàng checkin theo thời gian
        /// </summary>
        /// <param name="model">Ngày bắt đầu ngày kết thúc</param>
        /// <returns>Danh sách số lượng khách hàng checkin theo ngày</returns> 
        /// <response code="200">Thành công</response>
        [HttpPost, Route("number-of-account-checkin")]
        [ProducesResponseType(typeof(ResponseObject<List<StatisticalAccountRegisterModel>>), StatusCodes.Status200OK)]
        public async Task<IActionResult> StatisticalAccountCheckin([FromBody] StartDateEndDateModel model)
        {
            return await ExecuteFunction((RequestUser u) =>
            {
                var result = _handler.StatisticalAccountCheckin(model);
                return Helper.TransformData(result);
            });
        }
    }
}
