﻿using DigitalID.Business;
using DigitalID.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using Serilog;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace DigitalID.API
{
    /// <summary>
    /// JWT cho hệ thống
    /// </summary>
    [ApiVersion("1.0")]
    [ApiController]
    [Route("api/v{api-version:apiVersion}/authentication")]
    [ApiExplorerSettings(GroupName = "System - Authentication")]
    public class AuthenticationController : ControllerBase
    {
        private readonly IConfiguration _config;
        private readonly IRightMapUserHandler _rightMapUserHandler;
        private readonly IUserMapRoleHandler _userMapRoleHandler;
        private readonly IUserHandler _userHandler;
        private readonly IAccountHandler _accountHandler;
        private readonly IApplicationHandler _applicationHandler;
        private static readonly HttpClient Client = new HttpClient();

        public AuthenticationController(
            IConfiguration config,
            IAccountHandler accountHandler,
            IUserHandler userHandler,
            IApplicationHandler applicationHandler,
            IRightMapUserHandler rightMapUserHandler,
            IUserMapRoleHandler userMapRoleHandler
            )
        {
            _accountHandler = accountHandler;
            _config = config;
            _rightMapUserHandler = rightMapUserHandler;
            _userMapRoleHandler = userMapRoleHandler;
            _userHandler = userHandler;
            _applicationHandler = applicationHandler;
        }
        #region  JWT
        /// <summary>
        /// Đăng nhập và lấy kết quả JWT token
        /// API dành cho phần mềm khác kết nối vào hệ thống
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        [Route("gettoken")]
        [AllowAnonymous, HttpPost]
        public async Task<ResponseV2<string>> GetToken([FromBody] LoginModel login)
        {
            try
            {
                double timeToLive = Convert.ToDouble(_config["Authentication:Jwt:TimeToLive"]);
                var user = _userHandler.Authentication(login.Username, login.Password);
                if (user.Code == Code.Success && user is ResponseObject<UserModel> userData)
                {
                    //Danh sách ứng dụng
                    var getApp = await _applicationHandler.GetDefautByUserIdAsync(userData.Data.Id);
                    if (getApp.Code == Code.Success && getApp is ResponseObject<ApplicationModel> getAppData)
                    {
                        var tokenString = IdmHelper.BuildToken(userData.Data, login.RememberMe, timeToLive);
                        var logBuilder = Log.ForContext("ApplicationId", getAppData.Data.Id)
                                              .ForContext("UserId", userData.Data.Id)
                                              .ForContext("Action", "Đăng nhập")
                                              .ForContext("Module", "Hệ thống");
                        logBuilder.Information("{Action} {Module} thành công!");
                        return new ResponseV2<string>(1, "Success", tokenString);
                    }
                    else
                    {
                        return new ResponseV2<string>(0, "Người dùng chưa được cấp quyền vào ứng dụng nào", "");
                    }
                }
                else
                {
                    return new ResponseV2<string>(0, "Không tìm thấy thông tin tài khoản", "");
                }
            }
            catch (Exception ex)
            {
                return new ResponseV2<string>(-1, "Lỗi: " + ex.Message, "");
            }
        }
        /// <summary>
        /// Đăng nhập và lấy kết quả JWT token
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        [Route("jwt/login")]
        [AllowAnonymous, HttpPost]
        public async Task<IActionResult> SignInJwt([FromBody] LoginModel login)
        {
            try
            {
                IActionResult response = Unauthorized();
                double timeToLive = Convert.ToDouble(_config["Authentication:Jwt:TimeToLive"]);
                if (login.Username == _config["Authentication:AdminUser"] &&
                     login.Password == _config["Authentication:AdminPassWord"])
                {
                    //Lấy về danh sách quyền của người dùng
                    var resultRight = await _rightMapUserHandler.GetRightMapUserAsync(UserConstants.AdministratorId, AppConstants.RootAppId);
                    var listRight = new List<string>();
                    if (resultRight.Code == Code.Success && resultRight is ResponseList<BaseRightModelOfUser> rightData)
                    {
                        var ls = rightData.Data;
                        foreach (var item in ls)
                        {
                            listRight.Add(item.Code);
                        }
                    }

                    //Lấy về danh sách nhóm quyền của người dùng
                    var resultRole = await _userMapRoleHandler.GetRoleMapUserAsync(UserConstants.AdministratorId, AppConstants.RootAppId);
                    var listRole = new List<string>();
                    if (resultRole.Code == Code.Success && resultRole is ResponseList<BaseRoleModel> roleData)
                    {
                        var ls = roleData.Data;
                        foreach (var item in ls)
                        {
                            listRole.Add(item.Code);
                        }
                    }

                    var tokenString = IdmHelper.BuildToken(new UserModel()
                    {
                        AvatarUrl = "",
                        UserName = _config["Authentication:AdminUser"],
                        Email = "admin@admin.com",
                        LastActivityDate = DateTime.Now,
                        PhoneNumber = "967267469",
                        Type = 1,
                        Id = UserConstants.AdministratorId,
                        ApplicationId = AppConstants.RootAppId,
                        ListRole = listRole,
                        ListRight = listRight
                    }, login.RememberMe, timeToLive);
                    response = Helper.TransformData(new ResponseObject<LoginResponse>(new LoginResponse()
                    {
                        TokenString = tokenString,
                        UserId = UserConstants.AdministratorId,
                        ApplicationModel = ApplicationCollection.Instance.GetModel(AppConstants.RootAppId),
                        ApplicationId = AppConstants.RootAppId,
                        UserModel = UserCollection.Instance.GetModel(UserConstants.AdministratorId),
                        TimeExpride = DateTime.UtcNow.AddSeconds(timeToLive),
                        ListRight = listRight,
                        ListRole = listRole
                    }));
                    return response;
                }
                if (login.Username == _config["Authentication:GuestUser"] && login.Password == _config["Authentication:GuestPassWord"])
                {
                    var tokenString = IdmHelper.BuildToken(new UserModel()
                    {
                        AvatarUrl = "",
                        UserName = _config["Authentication:GuestUser"],
                        Email = "guest@admin.com",
                        LastActivityDate = DateTime.Now,
                        PhoneNumber = "967267469",
                        Type = 1,
                        Id = UserConstants.UserId,
                        ApplicationId = AppConstants.RootAppId
                    }, login.RememberMe, timeToLive);
                    response = Helper.TransformData(new ResponseObject<LoginResponse>(new LoginResponse()
                    {
                        TokenString = tokenString,
                        UserId = UserConstants.UserId,
                        ApplicationModel = ApplicationCollection.Instance.GetModel(AppConstants.RootAppId),
                        ApplicationId = AppConstants.RootAppId,
                        UserModel = UserCollection.Instance.GetModel(UserConstants.UserId),
                        TimeExpride = DateTime.UtcNow.AddSeconds(timeToLive)
                    }));
                    return response;
                }
                var user = _userHandler.Authentication(login.Username, login.Password);
                if (user.Code == Code.Success && user is ResponseObject<UserModel> userData)
                {
                    //Danh sách ứng dụng
                    var getApp = await _applicationHandler.GetDefautByUserIdAsync(userData.Data.Id);
                    if (getApp.Code == Code.Success && getApp is ResponseObject<ApplicationModel> getAppData)
                    {
                        //Lấy về danh sách quyền của người dùng
                        var resultRight = await _rightMapUserHandler.GetRightMapUserAsync(userData.Data.Id, getAppData.Data.Id);
                        var listRight = new List<string>();
                        if (resultRight.Code == Code.Success && resultRight is ResponseList<BaseRightModelOfUser> rightData)
                        {
                            var ls = rightData.Data;
                            foreach (var item in ls)
                            {
                                listRight.Add(item.Code);
                            }
                        }

                        //Lấy về danh sách nhóm quyền của người dùng
                        var resultRole = await _userMapRoleHandler.GetRoleMapUserAsync(userData.Data.Id, getAppData.Data.Id);
                        var listRole = new List<string>();
                        if (resultRole.Code == Code.Success && resultRole is ResponseList<BaseRoleModel> roleData)
                        {
                            var ls = roleData.Data;
                            foreach (var item in ls)
                            {
                                listRole.Add(item.Code);
                            }
                        }

                        var userModel = userData.Data;
                        userModel.ApplicationId = getAppData.Data.Id;
                        userModel.ListRole = listRole;
                        userModel.ListRight = listRight;
                        var tokenString = IdmHelper.BuildToken(userModel, login.RememberMe, timeToLive);
                        response = Helper.TransformData(new ResponseObject<LoginResponse>(new LoginResponse()
                        {
                            TokenString = tokenString,
                            UserId = userData.Data.Id,
                            ApplicationModel = getAppData.Data,
                            UserModel = userData.Data,
                            IsAdmin = userModel.IsAdmin,
                            ApplicationId = getAppData.Data.Id,
                            TimeExpride = DateTime.UtcNow.AddSeconds(timeToLive),
                            ListRight = listRight,
                            ListRole = listRole
                        }));

                        var logBuilder = Log.ForContext("ApplicationId", getAppData.Data.Id)
                                              .ForContext("UserId", userData.Data.Id)
                                              .ForContext("Action", "Đăng nhập")
                                              .ForContext("Module", "Hệ thống");
                        logBuilder.Information("{Action} {Module} thành công!");
                        return response;
                    }
                    else
                    {
                        response = Helper.TransformData(new Response(Code.Forbidden,
                            "Người dùng chưa được cấp quyền vào ứng dụng nào"));
                    }
                }
                return response;
            }
            catch
            {
                return Helper.TransformData(new Response(Code.ServerError,
                            "Có lỗi hệ thống xảy ra"));
            }
        }

        /// <summary>
        /// Đăng nhập khách và lấy kết quả JWT token
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        [Route("jwt/user/login")]
        [AllowAnonymous, HttpPost]
        public async Task<IActionResult> SignInJwtWithUser([FromBody] LoginModel login)
        {
            try
            {
                IActionResult response = BadRequest();
                double timeToLive = Convert.ToDouble(_config["Authentication:Jwt:TimeToLive"]);
                var user = _accountHandler.Authentication(login.Username, login.Password);
                if (user.Code == Code.Success && user is ResponseObject<AccountBaseModel> userData)
                {
                    //Danh sách ứng dụng
                    var getApp = await _applicationHandler.GetDefautByUserIdAsync(userData.Data.Id);
                    if (getApp.Code == Code.Success && getApp is ResponseObject<ApplicationModel> getAppData)
                    {
                        var userModel = userData.Data;
                        var tokenString = IdmHelper.BuildToken(userModel, login.RememberMe, timeToLive);
                        response = Helper.TransformData(new ResponseObject<LoginUserResponse>(new LoginUserResponse()
                        {
                            TokenString = tokenString,
                            UserId = userData.Data.Id,
                            ApplicationModel = getAppData.Data,
                            UserModel = userData.Data,
                            IsAdmin = userModel.IsAdmin,
                            ApplicationId = getAppData.Data.Id,
                            TimeExpride = DateTime.UtcNow.AddSeconds(timeToLive),
                        }));

                        var logBuilder = Log.ForContext("ApplicationId", getAppData.Data.Id)
                                              .ForContext("UserId", userData.Data.Id)
                                              .ForContext("Action", "Đăng nhập")
                                              .ForContext("Module", "Hệ thống");
                        logBuilder.Information("{Action} {Module} thành công!");
                        return response;
                    }
                    else
                    {
                        response = Helper.TransformData(new Response(Code.Forbidden,
                            "Người dùng chưa được cấp quyền vào ứng dụng nào"));
                    }
                }
                return response;
            }
            catch(Exception ex)
            {
                return Helper.TransformData(new Response(Code.ServerError,
                            "Có lỗi hệ thống xảy ra"));
            }
        }
        /// <summary>
        /// Đăng nhập google và lấy kết quả JWT token
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        [Route("jwt/google/login")]
        [AllowAnonymous, HttpPost]
        public async Task<IActionResult> SignInJwtWithGoogle([FromBody] LoginModel login)
        {
            try
            {
                IActionResult response = BadRequest();
                double timeToLive = Convert.ToDouble(_config["Authentication:Jwt:TimeToLive"]);
                var user = _accountHandler.AuthenticationGoogle(login.Username, login.Password, login.Provider);
                if (user.Code == Code.Success && user is ResponseObject<AccountBaseModel> userData)
                {
                    //Danh sách ứng dụng
                    var getApp = await _applicationHandler.GetDefautByUserIdAsync(userData.Data.Id);
                    if (getApp.Code == Code.Success && getApp is ResponseObject<ApplicationModel> getAppData)
                    {
                        var userModel = userData.Data;
                        var tokenString = IdmHelper.BuildToken(userModel, login.RememberMe, timeToLive);
                        response = Helper.TransformData(new ResponseObject<LoginUserResponse>(new LoginUserResponse()
                        {
                            TokenString = tokenString,
                            UserId = userData.Data.Id,
                            ApplicationModel = getAppData.Data,
                            UserModel = userData.Data,
                            IsAdmin = userModel.IsAdmin,
                            ApplicationId = getAppData.Data.Id,
                            TimeExpride = DateTime.UtcNow.AddSeconds(timeToLive),
                        }));

                        var logBuilder = Log.ForContext("ApplicationId", getAppData.Data.Id)
                                              .ForContext("UserId", userData.Data.Id)
                                              .ForContext("Action", "Đăng nhập")
                                              .ForContext("Module", "Hệ thống");
                        logBuilder.Information("{Action} {Module} thành công!");
                        return response;
                    }
                    else
                    {
                        response = Helper.TransformData(new Response(Code.Forbidden,
                            "Người dùng chưa được cấp quyền vào ứng dụng nào"));
                    }
                }
                return response;
            }
            catch (Exception ex)
            {
                return Helper.TransformData(new Response(Code.ServerError,
                            "Có lỗi hệ thống xảy ra"));
            }
        }
        /// <summary>
        /// Đăng xuat he thong
        /// </summary>
        /// <returns></returns>
        [Route("jwt/logout")]
        [Authorize, HttpPost]
        public IActionResult SignOutJwt()
        {
            // Get Token Info
            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            var actorId = requestInfo.UserId;
            var appId = requestInfo.ApplicationId;

            var logBuilder = Log.ForContext("ApplicationId", appId)
                                  .ForContext("UserId", actorId)
                                  .ForContext("Action", "Đăng xuất")
                                  .ForContext("Module", "Hệ thống");
            logBuilder.Information("{Action} {Module} thành công!");
            return NoContent();
        }
        #endregion
        #region Facebook
        /// <summary>
        /// Đăng nhập qua FB và lấy kết quả JWT token
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("fb/login")]
        [AllowAnonymous, HttpPost]
        public async Task<IActionResult> SignInFacebook([FromBody]FacebookAuthViewModel model)
        {
            IActionResult response = Unauthorized();
            double timeToLive = Convert.ToDouble(_config["Authentication:Jwt:TimeToLive"]);
            var _appId = _config["Authentication:Facebook:AppId"];
            var _appSecret = _config["Authentication:Facebook:AppSecret"];
            var appAccessTokenResponse = await Client.GetStringAsync($"https://graph.facebook.com/oauth/access_token?client_id={_appId}&client_secret={_appSecret}&grant_type=client_credentials");
            var appAccessToken = JsonConvert.DeserializeObject<FacebookAppAccessToken>(appAccessTokenResponse);

            // 2.Xác thực token truyền lên
            var userAccessTokenValidationResponse = await Client.GetStringAsync($"https://graph.facebook.com/debug_token?input_token={model.AccessToken}&access_token={appAccessToken.AccessToken}");
            var userAccessTokenValidation = JsonConvert.DeserializeObject<FacebookUserAccessTokenValidation>(userAccessTokenValidationResponse);

            if (!userAccessTokenValidation.Data.IsValid)
            {
                response = Helper.TransformData(new Response(Code.BadRequest,
                       "Đăng nhập thất bại"));
            }

            // 3. Lấy về thông tin sử dụng (key point email)
            var userInfoResponse = await Client.GetStringAsync($"https://graph.facebook.com/v2.8/me?fields=id,email,first_name,last_name,name,gender,locale,birthday,picture&access_token={model.AccessToken}");
            var userInfo = JsonConvert.DeserializeObject<FacebookUserData>(userInfoResponse);

            // 4. ready to create the local user account (if necessary) and jwt
            var userData = UserCollection.Instance.Collection.Where(x => x.Email == userInfo.Email).FirstOrDefault();

            if (userData == null)
            {
                response = Helper.TransformData(new Response(Code.BadRequest,
                        "Không tìm thấy tài khỏan trong hệ thống"));
            }
            //Danh sách ứng dụng
            var getApp = await _applicationHandler.GetDefautByUserIdAsync(userData.Id);
            if (getApp.Code == Code.Success && getApp is ResponseObject<ApplicationModel> getAppData)
            {
                var tokenString = IdmHelper.BuildToken(userData, false, timeToLive);
                response = Helper.TransformData(new ResponseObject<LoginResponse>(new LoginResponse()
                {
                    TokenString = tokenString,
                    UserId = userData.Id,
                    ApplicationModel = getAppData.Data,
                    ApplicationId = getAppData.Data.Id,
                    UserModel = UserCollection.Instance.GetModel(userData.Id),
                    TimeExpride = DateTime.UtcNow.AddSeconds(timeToLive)
                }));
                return response;
            }
            else
            {
                response = Helper.TransformData(new Response(Code.Forbidden,
                    "Người dùng chưa được cấp quyền vào ứng dụng nào"));
            }
            return response;
        }
        #endregion
    }
}