
using System;
using System.Collections.Generic;
using DigitalID.Business;

namespace DigitalID.API
{
    public class LoginModel
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public bool RememberMe { get; set; }
        public string ApplicationId { get; set; }
        public string Provider { get; set; }
    }


    public class LoginResponse
    {
        public Guid UserId { get; set; }
        public BaseUserModel UserModel  { get ; set; } 
        public string TokenString { get; set; }
        public bool IsAdmin { get; set; }
        public DateTime TimeExpride { get; set; }
        public BaseApplicationModel ApplicationModel { get; set; }
        public Guid ApplicationId { get; set; }
        public List<string> ListRight { get; set; }
        public List<string> ListRole { get; set; }
    }
    public class LoginUserResponse
    {
        public Guid UserId { get; set; }
        public AccountBaseModel UserModel { get; set; }
        public string TokenString { get; set; }
        public bool IsAdmin { get; set; }
        public DateTime TimeExpride { get; set; }
        public BaseApplicationModel ApplicationModel { get; set; }
        public Guid ApplicationId { get; set; }
        public List<string> ListRight { get; set; }
        public List<string> ListRole { get; set; }
    }
}