﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DigitalID.API
{
    public class GoogleModel
    {
        public string clientid = "XXXXXXXXXXXXXXXXXXXX";
        public string clientsecret = "XXXXXXXXXXXXXXXXXX";
        public string redirection_url = "XXXXXXXXXXXXXXXX";
        public string url = "https://accounts.google.com/o/oauth2/token";
    }


    public class Tokenclass
    {
        public string access_token {  get; set;  }
        public string token_type { get; set; }
        public int expires_in { get; set; }
        public string refresh_token { get; set; }
    }
    public class Userclass
    {
        public string id { get; set; }
        public string name { get; set; }
        public string given_name { get; set; }
        public string family_name { get; set; }
        public string link { get; set; }
        public string picture { get; set; }
        public string gender { get; set; }
        public string locale { get; set; }
    }

   public class SocialUserModel
    {
        public string Id { get; set; }
        public string AuthToken { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string IdToken { get; set; }
        public string LastName { get; set; }
        public string Name { get; set; }
        public string PhotoUrl { get; set; }
        public string Provider { get; set; }
    }
}
