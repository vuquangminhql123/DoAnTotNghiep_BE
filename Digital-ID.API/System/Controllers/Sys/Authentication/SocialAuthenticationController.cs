﻿using DigitalID.Business;
using DigitalID.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using Serilog;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

using DigitalID.API;
using DigitalID.Business;
using DigitalID.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using NetCore.Business;


namespace DigitalID.API
{
    /// <summary>
    /// Google authentication
    /// </summary>
    [ApiVersion("1.0")]
    [ApiController]
    [Route("api/v{api-version:apiVersion}/social-authentication")]
    [ApiExplorerSettings(GroupName = "Social - Authentication")]
    public class SocialAuthenticationController : ControllerBase
    {
        //your client id  
        private string clientid = "XXXXXXXXXXXXXXXXXXXX";
        //your client secret  
        private string clientsecret = "XXXXXXXXXXXXXXXXXX";
        //your redirection url  
        private string redirection_url = "http://localhost:8080";
        private string url = "https://accounts.google.com/o/oauth2/token";

        private readonly ICrmAccountHandler _handler;
        private readonly ICustomerHandler _userHandler;
        public SocialAuthenticationController(ICrmAccountHandler handler, ICustomerHandler userHandler)
        {
            _userHandler = userHandler;
            _handler = handler;
            clientid = Utils.GetConfig("Google:ClientId");
            clientsecret = Utils.GetConfig("Google:ClientSecret");
        }

        /// <summary>
        /// Thêm mới lịch sử checkin
        /// </summary>
        /// <param name="model">Thông tin lịch sử checkin</param>
        /// <returns></returns> 
        /// <response code="200">Thành công</response>
        [AllowAnonymous, HttpPost, Route("")]
        [ProducesResponseType(typeof(ResponseObject<Guid>), StatusCodes.Status200OK)]
        public async Task<IActionResult> SocialRegister([FromBody] GoogleUserModel model)
        {
            var result = await _userHandler.RegisterOrGetDetail(model);
            return Helper.TransformData(result);
        }

        private void GetUserProfile(string accesstoken)
        {
            string url = "https://www.googleapis.com/oauth2/v1/userinfo?alt=json&access_token=" + accesstoken + "";
            WebRequest request = WebRequest.Create(url);
            request.Credentials = CredentialCache.DefaultCredentials;
            WebResponse response = request.GetResponse();
            Stream dataStream = response.GetResponseStream();
            StreamReader reader = new StreamReader(dataStream);
            string responseFromServer = reader.ReadToEnd();
            reader.Close();
            response.Close();
            var userinfo = System.Text.Json.JsonSerializer.Deserialize<Userclass>(responseFromServer);
            var a = 1;
        }
    }
}
