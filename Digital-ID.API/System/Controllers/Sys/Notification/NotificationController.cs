﻿using DigitalID.Business;
using DigitalID.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DigitalID.API
{
    /// <inheritdoc />
    /// <summary>
    /// Application controller
    /// </summary>
    [ApiVersion("1.0")]
    [ApiController]
    [ApiExplorerSettings(GroupName = "System - Notification", IgnoreApi = true)]
    public class NotiController : ControllerBase
    {
        private readonly IHubContext<SignRHub> _hubContext;
        public NotiController(IHubContext<SignRHub> hubContext)
        {
            _hubContext = hubContext;
        }

        #region CRUD

        /// <summary>
        /// Notification tới người dùng
        /// </summary>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [AllowAnonymous, HttpGet, Route(WFConstants.SIGNRURL)]
        public async Task<IActionResult> NotifiClient()
        {
            await _hubContext.Clients.All.SendAsync("NOTIFICATION", "PROCESSED_DOCUMENT");
            return Ok();
        }
        #endregion
    }
}