﻿using DigitalID.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Serilog;
using System;
using System.Globalization;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace DigitalID.API
{
    /// <summary>
    /// Tích hợp Wso2
    /// </summary>
    [ApiVersion("1.0")][ApiController]
    [Route("api/v{api-version:apiVersion}/wso2")]
    [ApiExplorerSettings(GroupName = "System - Wso2", IgnoreApi = true)]
    public class AdminWso2Controller : ControllerBase
    {
        private readonly string _serviceValidate = Utils.GetConfig("Authentication:WSO2:Uri");
        private readonly string _clientId = Utils.GetConfig("Authentication:WSO2:Clientid");
        private readonly string _clientSecret = Utils.GetConfig("Authentication:WSO2:Secret");
        private readonly string _redirectUri = Utils.GetConfig("Authentication:WSO2:Redirecturi");


        /// <summary>
        /// API lấy về các thông tin xác thực lấy từ Identity Server theo Authentication Code
        /// </summary>
        /// <param name="authorizationCode"></param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [AllowAnonymous, HttpGet, Route("{authorizationCode}/authenticationinfo")]
        [ProducesResponseType(typeof(ResponseObject<Wso2Result>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetAuthenticationInfoByCode(string authorizationCode)
        {

            var refreshTokenUri = new Uri(_serviceValidate + "oauth2/token")
            .AddQuery("grant_type", "authorization_code")
            .AddQuery("redirect_uri", _redirectUri)
            .AddQuery("code", authorizationCode);

            var webRequest = (HttpWebRequest)WebRequest.Create(refreshTokenUri);
            webRequest.Method = "POST";
            var encoded = Convert.ToBase64String(Encoding.GetEncoding("ISO-8859-1").GetBytes(_clientId + ":" + _clientSecret));
            webRequest.ContentType = "application/x-www-form-urlencoded";
            webRequest.Accept = "application/json, text/javascript, */*";
            webRequest.Headers.Add("Authorization", "Basic " + encoded);
            try
            {
                using (var jsonResponse = (HttpWebResponse)webRequest.GetResponse())
                {
                    var jsonStream = jsonResponse.GetResponseStream();

                    var ms = new MemoryStream();
                    jsonStream?.CopyTo(ms);
                    ms.Position = 0;
                    var response = new HttpResponseMessage(HttpStatusCode.OK)
                    {
                        Content = new StreamContent(ms)
                    };
                    response.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
                    var responseBody = await response.Content.ReadAsStringAsync();
                    var wso2Result = JsonConvert.DeserializeObject<Wso2Result>(responseBody);

                    // Caculate expires time
                    var dt = DateTime.Now;
                    wso2Result.start_time = dt.ToString(CultureInfo.InvariantCulture);
                    wso2Result.expires_time = dt.AddSeconds(wso2Result.expires_in).ToString(CultureInfo.InvariantCulture);

                    // Get user info
                    if (wso2Result.access_token != null)
                    {
                        var getUserInfo = await GetUserInfoByAccessToken(wso2Result.access_token);

                        if (getUserInfo.Code == Code.Success)
                        {
                            if (getUserInfo is ResponseObject<Wso2UserInfo> getUserInfoData) wso2Result.UserInfo = getUserInfoData.Data;
                        }
                    }
                    var result = new ResponseObject<Wso2Result>(wso2Result);
                    return Helper.TransformData(result);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                var result = new Response(Code.ServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
                return Helper.TransformData(result);
            }
        }

        /// <summary>
        /// API làm mới access token dựa vào refresh token
        /// </summary>
        /// <param name="token"></param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [AllowAnonymous, HttpGet, Route("{token}/refreshtoken")]
        [ProducesResponseType(typeof(ResponseObject<Wso2Result>), StatusCodes.Status200OK)]

        public async Task<IActionResult> GetNewAccessTokenByRefreshToken(string token)
        {

            var refreshTokenUri = new Uri(_serviceValidate + "oauth2/token")
            .AddQuery("grant_type", "refresh_token")
            .AddQuery("refresh_token", token);

            var webRequest = (HttpWebRequest)WebRequest.Create(refreshTokenUri);
            webRequest.Method = "POST";
            var encoded = Convert.ToBase64String(Encoding.GetEncoding("ISO-8859-1").GetBytes(_clientId + ":" + _clientSecret));
            webRequest.ContentType = "application/x-www-form-urlencoded";
            webRequest.Accept = "application/json, text/javascript, */*";
            webRequest.Headers.Add("Authorization", "Basic " + encoded);
            try
            {
                using (var jsonResponse = (HttpWebResponse)webRequest.GetResponse())
                {
                    var jsonStream = jsonResponse.GetResponseStream();

                    var ms = new MemoryStream();
                    jsonStream?.CopyTo(ms);
                    ms.Position = 0;
                    var response = new HttpResponseMessage(HttpStatusCode.OK)
                    {
                        Content = new StreamContent(ms)
                    };
                    response.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
                    var responseBody = await response.Content.ReadAsStringAsync();
                    var wso2Result = JsonConvert.DeserializeObject<Wso2Result>(responseBody);

                    // Caculate expires time
                    var dt = DateTime.Now;
                    wso2Result.start_time = dt.ToString(CultureInfo.InvariantCulture);
                    wso2Result.expires_time = dt.AddSeconds(wso2Result.expires_in).ToString(CultureInfo.InvariantCulture);

                    var result = new ResponseObject<Wso2Result>(wso2Result);
                    return Helper.TransformData(result);
                }
            }
            catch (Exception ex)
            {

                Log.Error(ex, "");
                var result = new Response(Code.ServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
                return Helper.TransformData(result);
            }
        }


        /// <summary>
        /// API lấy claim user dựa vào access token
        /// </summary>
        /// <param name="token"></param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [AllowAnonymous, HttpGet, Route("{token}")]
        [ProducesResponseType(typeof(ResponseObject<Wso2UserInfo>), StatusCodes.Status200OK)]
        public async Task<Response> GetUserInfoByAccessToken(string token)
        {

            var userInfoUri = new Uri(_serviceValidate + "oauth2/userinfo")
            .AddQuery("schema", "openid");

            var webRequest = (HttpWebRequest)WebRequest.Create(userInfoUri);

            webRequest.Method = "POST";
            webRequest.ContentType = "application/x-www-form-urlencoded";
            webRequest.Accept = "application/json, text/javascript, */*";
            webRequest.Headers.Add("Authorization", "Bearer " + token);
            try
            {
                using (var jsonResponse = (HttpWebResponse)webRequest.GetResponse())
                {
                    var jsonStream = jsonResponse.GetResponseStream();

                    var ms = new MemoryStream();
                    jsonStream?.CopyTo(ms);
                    ms.Position = 0;
                    var response = new HttpResponseMessage(HttpStatusCode.OK)
                    {
                        Content = new StreamContent(ms)
                    };
                    response.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
                    var responseBody = await response.Content.ReadAsStringAsync();

                    var wso2Result = JsonConvert.DeserializeObject<Wso2UserInfo>(responseBody);

                    var result = new ResponseObject<Wso2UserInfo>(wso2Result);
                    return result;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                var result = new Response(Code.ServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
                return result;
            }
        }
    }

    public class Wso2Result
    {
#pragma warning disable IDE1006 // Naming Styles
        public string access_token { get; set; }
        public string refresh_token { get; set; }
        public string id_token { get; set; }
        public int expires_in { get; set; }
        public string start_time { get; set; }
        public string expires_time { get; set; }
        public string token_type { get; set; }
#pragma warning restore IDE1006 // Naming Styles
        public Wso2UserInfo UserInfo { get; set; }
    }
    public class Wso2UserInfo
    {
#pragma warning disable IDE1006 // Naming Styles
        public string user_id { get; set; }
        public string user_name { get; set; }
        public string display_name { get; set; }
        public string email { get; set; }
#pragma warning restore IDE1006 // Naming Styles
    }
}



