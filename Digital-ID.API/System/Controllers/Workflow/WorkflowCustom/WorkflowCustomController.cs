﻿using DigitalID.Business;
using DigitalID.Data;
using DigitalID.WF;
using DigitalID.WF.Workflow;
using IO.Swagger.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Configuration;
using OptimaJet.Workflow.Core.Model;
using OptimaJet.Workflow.Core.Runtime;
using Serilog;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace DigitalID.API
{
    /// <inheritdoc />
    /// <summary>
    /// API tích hợp workflow
    /// </summary>
    [ApiVersion("1.0")]
    [ApiController]
    [Route("api/v{api-version:apiVersion}/wf-custom")]
    [ApiExplorerSettings(GroupName = "Workflow - Workflow Custom", IgnoreApi = true)]
    public class WorkflowCustomController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private readonly IWorkflowSchemeHandler _workflowSchemeHandler;
        private readonly IWorkflowDocumentHistoryHandler _workflowDocumentHistoryHandler;
        private readonly IWorkflowInboxHandler _workflowInboxHandler;
        private readonly IWorkflowHandler _workflowHandler;
        private readonly IHubContext<SignRHub> _hubContext;

        public WorkflowCustomController(IConfiguration configuration,
          IHubContext<SignRHub> hubContext,
        IWorkflowSchemeHandler workflowSchemeHandler, IWorkflowHandler workflowHandler, IWorkflowDocumentHistoryHandler workflowDocumentHistoryHandler, IWorkflowInboxHandler workflowInboxHandler)
        {
            _hubContext = hubContext;
            _configuration = configuration;
            _workflowSchemeHandler = workflowSchemeHandler;
            _workflowHandler = workflowHandler;
            _workflowDocumentHistoryHandler = workflowDocumentHistoryHandler;
            _workflowInboxHandler = workflowInboxHandler;
        }


        /// <summary>
        /// Thi hành 1 command
        /// </summary>
        /// <param name="processId">Id processs</param>
        /// <param name="command">Tên câu lệnh</param>
        /// <param name="identityId">Định danh người xử lý</param>
        /// <param name="impersonatedIdentityId">Định danh người xử lý phụ</param>
        /// <param name="request"></param>
        [HttpPost]
        [Authorize]
        [Route("executecommand/{processId}")]
        public async Task<ActionResult<ResponseObject<IO.Swagger.Models.ExecuteCommandInfo>>> ExecuteCommand(
            [FromRoute]Guid processId,
            [FromQuery]string command,
            [FromQuery]string identityId,
            [FromQuery]string impersonatedIdentityId,
            [FromBody] ExecuteCommandBody request)
        {
            try
            {
                var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
                var actorId = requestInfo.UserId;
                var appId = requestInfo.ApplicationId;
                identityId = actorId.ToString();
                var processInstance = await WorkflowInit.Runtime.GetProcessInstanceAndFillProcessParametersAsync(processId);
                processInstance.SetParameter("FormInputJson", request.FormInputJson, ParameterPurpose.Persistence);
                processInstance.SetParameter("FileInputJson", request.FileInputJson, ParameterPurpose.Persistence);
                WorkflowInit.Runtime.PersistenceProvider.SavePersistenceParameters(processInstance);

                if (!await WorkflowInit.Runtime.IsProcessExistsAsync(processId))
                    return Helper.TransformData(new ResponseError(Code.BadRequest, "Instance did not existed", null));

                var commands = WorkflowInit.Runtime.GetAvailableCommands(processId, identityId);
                var commandObj = commands.FirstOrDefault(
                        c => c.CommandName.Equals(command, StringComparison.CurrentCultureIgnoreCase));
                if (commandObj == null)
                    return Helper.TransformData(new ResponseError(Code.BadRequest, "Command did not existed", null));
                if (request.processParameters != null)
                {
                    commandObj.Parameters = request.processParameters;
                }

                var result = await WorkflowInit.Runtime.ExecuteCommandAsync(commandObj, identityId, impersonatedIdentityId);

                return new ResponseObject<ExecuteCommandInfo>(AutoMapperUtils.AutoMap<CommandExeutionResult, ExecuteCommandInfo>(result));
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return Helper.TransformData(new ResponseError(Code.ServerError, "Server error please read the logs!"));
            }

        }


        /// <summary>
        /// Lấy về các command có thể có của một user
        /// </summary>
        /// <param name="processId">Id processs</param>
        /// <param name="identityId">Định danh người xử lý</param>
        /// <param name="impersonatedIdentityId">Định danh người xử lý phụ</param>
        /// <param name="culture"></param>
        /// <response code="200">Operation result</response>
        [HttpGet]
        [Authorize]
        [Route("getavailablecommands/{processId}")]
        public async Task<ActionResult<ResponseObject<IEnumerable<WorkflowCommand>>>> GetAvailableCommands(
            [FromRoute]Guid processId,
            [FromQuery]string identityId,
            [FromQuery]string impersonatedIdentityId,
            [FromQuery]string culture)
        {
            try
            {
                var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
                var actorId = requestInfo.UserId;
                var appId = requestInfo.ApplicationId;
                identityId = actorId.ToString();
                if (!await WorkflowInit.Runtime.IsProcessExistsAsync(processId))
                    return Helper.TransformData(new ResponseError(Code.BadRequest, "Instance did not existed", null));

                if (!string.IsNullOrEmpty(impersonatedIdentityId))
                {
                    var identityIds = new List<string>() {
                        identityId,
                        impersonatedIdentityId
                    };
                    var result = await WorkflowInit.Runtime.GetAvailableCommandsAsync(processId, identityIds, null, identityId, new CultureInfo(culture, false));
                    return new ResponseObject<IEnumerable<WorkflowCommand>>(result);

                }
                else
                {
                    var result = await WorkflowInit.Runtime.GetAvailableCommandsAsync(processId, identityId);
                    return new ResponseObject<IEnumerable<WorkflowCommand>>(result);
                }


            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return Helper.TransformData(new ResponseError(Code.ServerError, "Server error please read the logs!"));
            }
        }
        /// <summary>
        /// Lấy về lịch sử xử lý
        /// </summary>
        /// <param name="processId">Id processs</param>
        /// <response code="200">Operation result</response>
        [HttpGet]
        [Authorize]
        [Route("getprocesshistory/{processId}")]
        public async Task<IActionResult> GetProcessHistoryAsync([FromRoute]Guid processId)
        {
            var result = await _workflowDocumentHistoryHandler.GetAllAsync(new WorkflowDocumentHistoryQueryModel()
            {
                WorkflowDocumentId = processId,
                Sort = "+ExecutedTransition"
            });
            return Helper.TransformData(result);
        }
        /// <summary>
        /// Lấy về danh sách thông báo
        /// </summary>
        /// <response code="200">Operation result</response>
        [HttpGet]
        [Authorize]
        [Route("getprocessnotification")]
        public async Task<IActionResult> GetNotificatonAsync()
        {
            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            var actorId = requestInfo.UserId;
            var appId = requestInfo.ApplicationId;
            var result = await _workflowInboxHandler.GettAllByUserId(appId, actorId);
            return Helper.TransformData(result);
        }
        /// <summary>
        /// Lấy về thông tin biểu mẫu
        /// </summary>

        /// <param name="name">The name of the busines flow</param>
        /// <param name="processId">The id of the process</param>
        /// <param name="identityId">User&#39;s identifier</param>
        /// <response code="200">Operation result</response>
        [HttpGet]
        [Authorize]
        [Route("getformbyflow/{processId}")]
        public async Task<IActionResult> GetFormByFlow(
            [FromRoute]Guid processId,
            [FromQuery]string name,
            [FromQuery]string identityId)
        {
            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            var actorId = requestInfo.UserId;
            var appId = requestInfo.ApplicationId;
            identityId = actorId.ToString();
            // Call service
            var result = await _workflowHandler.GetFormByFlow(name, processId, identityId);
            // Hander response
            return Helper.TransformData(result);
        }
        /// <summary>
        /// Lấy về thông tin biểu mẫu
        /// </summary> 
        /// <param name="name">The name of the busines flow</param>
        /// <response code="200">Operation result</response>
        [HttpGet]
        [Authorize]
        [Route("getcreateformbyflow/{name}")]
        public async Task<IActionResult> GetCreateFormByFlow(string name)
        {
            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            var actorId = requestInfo.UserId;
            var appId = requestInfo.ApplicationId;
            // Call service
            var result = await _workflowHandler.GetCreateFormByFlow(name);
            // Hander response
            return Helper.TransformData(result);
        }

        /// <summary>
        /// Tạo mới một instance
        /// </summary>
        /// <param name="processId">Id process sẽ tạo</param>
        /// <param name="schemeCode">Mã quy trình</param>
        /// <param name="identityId">Người tạo</param>
        [HttpPost]
        [Route("createinstance/{processId}")]
        [Authorize]
        public async Task<ActionResult<ResponseUpdate>> CreateInstanceAsync(
            [FromRoute]Guid processId,
            [FromQuery]string schemeCode,
            [FromQuery]string identityId)
        {
            try
            {
                if (await WorkflowInit.Runtime.IsProcessExistsAsync(processId))
                    return new ResponseUpdate(processId, "Instance existed");
                await WorkflowInit.Runtime.CreateInstanceAsync(new CreateInstanceParams(schemeCode, processId)
                {
                    IdentityId = identityId
                });
                return new ResponseUpdate(processId, "Create instance success");
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return Helper.TransformData(new ResponseError(Code.ServerError, "Server error please read the logs!"));
            }

        }


        /// <summary>
        /// Lấy thông tin chi tiết một instance
        /// </summary>
        /// <param name="processId">Id process sẽ tạo</param> 
        [HttpGet]
        [Route("getinstanceinfo/{processId}")]
        [Authorize]
        public async Task<ActionResult<ResponseObject<dynamic>>> GetInstanceInfo(
            [FromRoute]Guid processId)
        {
            try
            {
                if (!await WorkflowInit.Runtime.IsProcessExistsAsync(processId))
                    return Helper.TransformData(new ResponseError(Code.BadRequest, "Instance did not existed", null));
                var resultData = new ProcessInfoModel();
                //step 1 get process status
                var response1 = await WorkflowInit.Runtime.GetProcessSchemeAsync(processId);
                var response2 = WorkflowInit.Runtime.GetProcessInstancesTree(processId);
                var response3 = await WorkflowInit.Runtime.GetProcessHistoryAsync(processId);
                var response4 = await WorkflowInit.Runtime.GetProcessInstanceAndFillProcessParametersAsync(processId);

                dynamic obj = new ExpandoObject();
                obj.response1 = response1;
                obj.response2 = response2;
                obj.response3 = response3;
                obj.response4 = response4;
                return new ResponseObject<dynamic>(obj);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return Helper.TransformData(new ResponseError(Code.ServerError, "Server error please read the logs!"));
            }

        }
    }
}
