﻿using IO.Swagger.Models;
using System;
using System.Collections.Generic;

namespace DigitalID.API
{
    public class ProcessInfoModel
    {
        public Guid ProcessId { get; set; }
        public Guid RootProcessId { get; set; }
        public string SchemeCode { get; set; }
        public string PreviousState { get; set; }
        public string PreviousActivity { get; set; }
        public string NextState { get; set; }
        public string NextActivity { get; set; }

        public List<ProcessInfoModel> ListSubProcess { get; set; }
        public List<Transition> Transitions { get; set; }
        public List<HistoryItem> History { get; set; }
        public ProcessParameters ProcessParameters { get; set; }
    }
    public class ExecuteCommandBody
    {
        public string FormInputJson { get; set; }
        public string FileInputJson { get; set; }
        public List<OptimaJet.Workflow.Core.Runtime.CommandParameter> processParameters{ get; set; }
    }
}
