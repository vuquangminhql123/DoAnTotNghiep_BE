﻿using DigitalID.Business;
using DigitalID.Data;
using DigitalID.WF.Workflow;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Newtonsoft.Json;
using OptimaJet.Workflow.Core.Persistence;
using OptimaJet.Workflow.Core.Runtime;
using Serilog;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DigitalID.API
{
    /// <inheritdoc />
    /// <summary>
    /// Module
    /// </summary>
    [ApiVersion("1.0")]
    [ApiController]
    [Route("api/v{api-version:apiVersion}/wf/document")]
    [ApiExplorerSettings(GroupName = "Workflow WorkflowDocument", IgnoreApi = true)]
    public class WorkflowDocumentController : ControllerBase
    {
        private readonly IWorkflowDocumentHandler _WorkflowDocumentHandler;
        private readonly IWorkflowSchemeAttributeHandler _WorkflowSchemeAttributeHandler;
        private readonly IHubContext<SignRHub> _hubContext;
        private readonly IWorkflowDocumentAttachmentHandler _WorkflowDocumentAttachmentHandler;

        public WorkflowDocumentController(IHubContext<SignRHub> hubContext, IWorkflowDocumentHandler WorkflowDocumentHandler,
            IWorkflowDocumentAttachmentHandler WorkflowDocumentAttachmentHandler, IWorkflowSchemeAttributeHandler WorkflowSchemeAttributeHandler)
        {
            _hubContext = hubContext;
            _WorkflowDocumentHandler = WorkflowDocumentHandler;
            _WorkflowDocumentAttachmentHandler = WorkflowDocumentAttachmentHandler;
            _WorkflowSchemeAttributeHandler = WorkflowSchemeAttributeHandler;
        }

        /// <summary>
        /// Lây về danh sách attachment
        /// </summary>
        /// <param name="id">Id bản ghi</param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [Authorize, HttpGet, Route("{id}/attachment")]
        [ProducesResponseType(typeof(ResponseUpdate), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetAllAttachment(Guid id)
        {
            // Get Token Info
            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            var actorId = requestInfo.UserId;
            var appId = requestInfo.ApplicationId;
            // Call service
            var result = await _WorkflowDocumentAttachmentHandler.GetAllAsync(new WorkflowDocumentAttachmentQueryModel() { WorkflowDocumentId = id });

            // Hander response
            return Helper.TransformData(result);
        }

        /// <summary>
        /// Cập nhật danh sách attachment
        /// </summary>
        /// <param name="id">Id bản ghi</param>
        /// <param name="request">Danh sách acttactment mới</param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [Authorize, HttpPost, Route("{id}/attachment")]
        [ProducesResponseType(typeof(ResponseUpdate), StatusCodes.Status200OK)]
        public async Task<IActionResult> UpdateListAttachment(Guid id, [FromBody] List<WorkflowDocumentAttachmentModel> request)
        {
            // Get Token Info
            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            var actorId = requestInfo.UserId;
            var appId = requestInfo.ApplicationId;
            // Call service
            var result = await _WorkflowDocumentHandler.UpdateListAttachment(request, id, appId, actorId);

            // Hander response
            return Helper.TransformData(result);
        }

        #region CRUD
        /// <summary>
        /// Thêm mới
        /// </summary>
        /// <param name="model">Dữ liệu</param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [Authorize, HttpPost, Route("")]
        [ProducesResponseType(typeof(ResponseObject<WorkflowDocumentModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> CreateAsync([FromBody] WorkflowDocumentCreateModel model)
        {


            // Get Token Info
            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            var actorId = requestInfo.UserId;
            var appId = requestInfo.ApplicationId;
            if (!model.Id.HasValue)
            {
                model.Id = Guid.NewGuid();
                model.AuthorId = actorId;
            }
            // Call service            
            var result = await _WorkflowDocumentHandler.CreateAsync(model, appId, actorId);
            try
            {
                var wfAttribute = _WorkflowSchemeAttributeHandler.FindByCode(model.Scheme);
                if (wfAttribute != null && wfAttribute.Code == Code.Success)
                {
                    if (wfAttribute.Data.ActiveVersion > 0)
                        model.Scheme = model.Scheme + "_" + wfAttribute.Data.ActiveVersion;
                }
                else
                {
                    Log.Error("Not found WF in WorkflowSchemeAttribute", "");
                    return Helper.TransformData(new ResponseError(Code.ServerError, "Không tìm thấy quy trình"));
                }
                await WorkflowInit.Runtime.CreateInstanceAsync(new CreateInstanceParams(model.Scheme, model.Id.Value)
                {
                    IdentityId = model.AuthorId.ToString()
                });
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return Helper.TransformData(new ResponseError(Code.ServerError, "Server error please read the logs!"));
            }
            var currentActivitiy = await WorkflowInit.Runtime.GetCurrentActivityNameAsync(model.Id.Value);
            var currentState = await WorkflowInit.Runtime.GetCurrentStateNameAsync(model.Id.Value);
            model.Activity = currentActivitiy;
            model.State = currentState;
            model.Status = ProcessStatus.Initialized.ToString();

            // Hander response
            return Helper.TransformData(result);
        }
        /// <summary>
        /// Cập nhật
        /// </summary>
        /// <param name="id">Id bản ghi</param>
        /// <param name="model">Dữ liệu</param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [Authorize, HttpPut, Route("{id}")]
        [ProducesResponseType(typeof(ResponseUpdate), StatusCodes.Status200OK)]
        //        [SwaggerRequestExample(typeof(WorkflowDocumentUpdateModel), typeof(MockupObject<WorkflowDocumentUpdateModel>))]
        public async Task<IActionResult> UpdateAsync(Guid id, [FromBody] WorkflowDocumentUpdateModel model)
        {
            // Get Token Info
            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            var actorId = requestInfo.UserId;
            var appId = requestInfo.ApplicationId;
            // Call service
            var result = await _WorkflowDocumentHandler.UpdateAsync(id, model, appId, actorId);

            // Hander response
            return Helper.TransformData(result);
        }
        /// <summary>
        /// Xóa
        /// </summary>
        /// <param name="id">Id bản ghi</param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [Authorize, HttpDelete, Route("{id}")]
        [ProducesResponseType(typeof(ResponseDelete), StatusCodes.Status200OK)]
        public async Task<IActionResult> DeleteAsync(Guid id)
        {
            // Get Token Info
            // var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            // var actorId = requestInfo.UserId;
            // var appId = requestInfo.ApplicationId;
            // Call service
            var result = await _WorkflowDocumentHandler.DeleteAsync(id);
            // Hander response
            if (result.Code == Code.Success)
            {
                await _hubContext.Clients.All.SendAsync("NOTIFICATION", "PROCESSED_DOCUMENT");
            }
            return Helper.TransformData(result);
        }
        /// <summary>
        /// Xóa danh sách
        /// </summary>
        /// <param name="listId">Danh sách id bản ghi</param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [Authorize, HttpDelete, Route("")]
        [ProducesResponseType(typeof(ResponseDeleteMulti), StatusCodes.Status200OK)]
        public async Task<IActionResult> DeleteRangeAsync([FromQuery]List<Guid> listId)
        {
            // Get Token Info
            // var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            // var actorId = requestInfo.UserId;
            // var appId = requestInfo.ApplicationId;
            // Call service 
            var result = await _WorkflowDocumentHandler.DeleteRangeAsync(listId);
            if (result.Code == Code.Success)
            {
                await _hubContext.Clients.All.SendAsync("NOTIFICATION", "PROCESSED_DOCUMENT");
            }
            // Hander response
            return Helper.TransformData(result);
        }
        /// <summary>
        /// Lấy về theo Id
        /// </summary>
        /// <param name="id">Id bản ghi</param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [Authorize, HttpGet, Route("{id}")]
        [ProducesResponseType(typeof(ResponseObject<WorkflowDocumentModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetByIdAsync(Guid id)
        {
            // Get Token Info
            // var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            // var actorId = requestInfo.UserId;
            // var appId = requestInfo.ApplicationId;
            // Call service
            var result = await _WorkflowDocumentHandler.FindAsync(id);
            // Hander response
            return Helper.TransformData(result);
        }
        /// <summary>
        /// Lấy về theo bộ loc
        /// </summary>
        /// <param name="page">Số thứ tự trang tìm kiếm</param>
        /// <param name="size">Số bản ghi giới hạn một trang</param>
        /// <param name="filter">Thông tin lọc nâng cao (Object Json)</param>
        /// <param name="sort">Thông tin sắp xếp (Array Json)</param>
        /// <returns></returns>
        /// <remarks>
        ///  *filter*
        ///  ....
        ///  *sort*
        ///  ....
        /// </remarks>
        /// <response code="200">Thành công</response>
        [Authorize, HttpGet, Route("")]
        [ProducesResponseType(typeof(ResponsePagination<WorkflowDocumentModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetFilterAsync([FromQuery]int page = 1, [FromQuery]int size = 20, [FromQuery]string filter = "{}", [FromQuery]string sort = "")
        {
            // Get Token Info
            // var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            // var actorId = requestInfo.UserId;
            // var appId = requestInfo.ApplicationId;
            // Call service
            var filterObject = JsonConvert.DeserializeObject<WorkflowDocumentQueryModel>(filter);
            filterObject.Sort = sort;
            filterObject.Size = size;
            filterObject.Page = page;
            var result = await _WorkflowDocumentHandler.GetPageAsync(filterObject);            
            // Hander response
            ActionResult dataReturn = Helper.TransformData(result);
            return dataReturn;
        }
        /// <summary>
        /// Lấy về tất cả
        /// </summary> 
        /// <returns></returns> 
        /// <response code="200">Thành công</response>
        [Authorize, HttpGet, Route("all")]
        [ProducesResponseType(typeof(ResponseList<WorkflowDocumentModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetAllAsync([FromQuery]string filter = "{}")
        {
            // Get Token Info
            // var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            // var actorId = requestInfo.UserId;
            // var appId = requestInfo.ApplicationId;
            // Call service 
            var filterObject = JsonConvert.DeserializeObject<WorkflowDocumentQueryModel>(filter);
            var result = await _WorkflowDocumentHandler.GetAllAsync(filterObject);
            // Hander response
            return Helper.TransformData(result);
        }
        /// <summary>
        /// Lấy về tất cả
        /// </summary> 
        /// <returns></returns> 
        /// <response code="200">Thành công</response>
        [Authorize, HttpGet, Route("statistic")]
        [ProducesResponseType(typeof(ResponseObject<WorkflowDocumentStatistic>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetAllAsync([FromQuery]DateTime? fromDate, [FromQuery]DateTime? toDate)
        {
            var result = await _WorkflowDocumentHandler.Statistic(fromDate, toDate);
            // Hander response
            return Helper.TransformData(result);
        }
        /// <summary>
        /// Lấy về tất cả
        /// </summary> 
        /// <returns></returns> 
        /// <response code="200">Thành công</response>
        [Authorize, HttpGet, Route("statistic-in-range")]
        [ProducesResponseType(typeof(ResponseList<WorkflowDocumentStatistic>), StatusCodes.Status200OK)]
        public async Task<IActionResult> StaticInRange([FromQuery]DateTime? fromDate, [FromQuery]DateTime? toDate, [FromQuery]DateRangeType typeRange)
        {
            var result = await _WorkflowDocumentHandler.StaticInRange(fromDate, toDate, typeRange);
            return Helper.TransformData(result);
        }
        #endregion 
    }
}
