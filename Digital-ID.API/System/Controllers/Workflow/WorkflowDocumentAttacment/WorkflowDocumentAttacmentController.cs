﻿
using DigitalID.Business;
using DigitalID.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DigitalID.API
{
    /// <inheritdoc />
    /// <summary>
    /// Module tài liệu
    /// </summary>
    [ApiVersion("1.0")][ApiController]
    [Route("api/v{api-version:apiVersion}/bsd/workflowDocumentAttachments")]
    [ApiExplorerSettings(GroupName = "BSD WorkflowDocumentAttachment", IgnoreApi = true)]
    public class WorkflowDocumentAttachmentController : ControllerBase
    {
        private readonly IWorkflowDocumentAttachmentHandler _workflowDocumentAttachmentHandler;

        public WorkflowDocumentAttachmentController(IWorkflowDocumentAttachmentHandler workflowDocumentAttachmentHandler)
        {
            _workflowDocumentAttachmentHandler = workflowDocumentAttachmentHandler;
        }
        #region CRUD
        /// <summary>
        /// Thêm mới
        /// </summary>
        /// <param name="model">Dữ liệu</param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [Authorize, HttpPost, Route("")]
        [ProducesResponseType(typeof(ResponseObject<WorkflowDocumentAttachmentModel>), StatusCodes.Status200OK)]
//        [SwaggerRequestExample(typeof(WorkflowDocumentAttachmentCreateModel), typeof(MockupObject<WorkflowDocumentAttachmentCreateModel>))]
        public async Task<IActionResult> CreateAsync([FromBody] WorkflowDocumentAttachmentCreateModel model)
        {
            // Get Token Info
            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            var actorId = requestInfo.UserId;
            var appId = requestInfo.ApplicationId;
            // Call service
            var result = await _workflowDocumentAttachmentHandler.CreateAsync(model,appId, actorId);
            // Hander response
            return Helper.TransformData(result);
        }
        /// <summary>
        /// Cập nhật
        /// </summary>
        /// <param name="id">Id bản ghi</param>
        /// <param name="model">Dữ liệu</param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [Authorize, HttpPut, Route("{id}")]
        [ProducesResponseType(typeof(ResponseUpdate), StatusCodes.Status200OK)]
//        [SwaggerRequestExample(typeof(WorkflowDocumentAttachmentUpdateModel), typeof(MockupObject<WorkflowDocumentAttachmentUpdateModel>))]
        public async Task<IActionResult> UpdateAsync(Guid id, [FromBody] WorkflowDocumentAttachmentUpdateModel model)
        {
            // Get Token Info
            var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            var actorId = requestInfo.UserId;
            var appId = requestInfo.ApplicationId;
            // Call service
            var result = await _workflowDocumentAttachmentHandler.UpdateAsync(id, model,appId, actorId);

            // Hander response
            return Helper.TransformData(result);
        }
        /// <summary>
        /// Xóa
        /// </summary>
        /// <param name="id">Id bản ghi</param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [Authorize, HttpDelete, Route("{id}")]
        [ProducesResponseType(typeof(ResponseDelete), StatusCodes.Status200OK)]
        public async Task<IActionResult> DeleteAsync(Guid id)
        {
            // Get Token Info
// var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
// var actorId = requestInfo.UserId;
// var appId = requestInfo.ApplicationId;
            // Call service
            var result = await _workflowDocumentAttachmentHandler.DeleteAsync(id);
            // Hander response
            return Helper.TransformData(result);
        }
        /// <summary>
        /// Xóa danh sách
        /// </summary>
        /// <param name="listId">Danh sách id bản ghi</param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [Authorize, HttpDelete, Route("")]
        [ProducesResponseType(typeof(ResponseDeleteMulti), StatusCodes.Status200OK)]
        public async Task<IActionResult> DeleteRangeAsync([FromQuery]List<Guid> listId)
        {
            // Get Token Info
// var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
// var actorId = requestInfo.UserId;
// var appId = requestInfo.ApplicationId;
            // Call service 
            var result = await _workflowDocumentAttachmentHandler.DeleteRangeAsync(listId);
            // Hander response
            return Helper.TransformData(result);
        }
        /// <summary>
        /// Lấy về theo Id
        /// </summary>
        /// <param name="id">Id bản ghi</param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [Authorize, HttpGet, Route("{id}")]
        [ProducesResponseType(typeof(ResponseObject<WorkflowDocumentAttachmentModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetByIdAsync(Guid id)
        {
            // Get Token Info
// var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
// var actorId = requestInfo.UserId;
// var appId = requestInfo.ApplicationId;
            // Call service
            var result = await _workflowDocumentAttachmentHandler.FindAsync(id);
            // Hander response
            return Helper.TransformData(result);
        }
        /// <summary>
        /// Lấy về theo bộ loc
        /// </summary>
        /// <param name="page">Số thứ tự trang tìm kiếm</param>
        /// <param name="size">Số bản ghi giới hạn một trang</param>
        /// <param name="filter">Thông tin lọc nâng cao (Object Json)</param>
        /// <param name="sort">Thông tin sắp xếp (Array Json)</param>
        /// <returns></returns>
        /// <remarks>
        ///  *filter*
        ///  ....
        ///  *sort*
        ///  ....
        /// </remarks>
        /// <response code="200">Thành công</response>
        [Authorize, HttpGet, Route("")]
        [ProducesResponseType(typeof(ResponsePagination<WorkflowDocumentAttachmentModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetFilterAsync([FromQuery]int page = 1, [FromQuery]int size = 20, [FromQuery]string filter = "{}", [FromQuery]string sort = "")
        {
            // Get Token Info
// var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
// var actorId = requestInfo.UserId;
// var appId = requestInfo.ApplicationId;
            // Call service
            var filterObject = JsonConvert.DeserializeObject<WorkflowDocumentAttachmentQueryModel>(filter);
            filterObject.Sort = sort;
            filterObject.Size = size;
            filterObject.Page = page;
            var result = await _workflowDocumentAttachmentHandler.GetPageAsync(filterObject);
            // Hander response
            return Helper.TransformData(result);
        }
        /// <summary>
        /// Lấy về tất cả
        /// </summary> 
        /// <returns></returns> 
        /// <response code="200">Thành công</response>
        [Authorize, HttpGet, Route("all")]
        [ProducesResponseType(typeof(ResponseList<WorkflowDocumentAttachmentModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetAllAsync([FromQuery]string filter = "{}")
        {
            // Get Token Info
// var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
// var actorId = requestInfo.UserId;
// var appId = requestInfo.ApplicationId;
            // Call service
            var filterObject = JsonConvert.DeserializeObject<WorkflowDocumentAttachmentQueryModel>(filter);
            var result = await _workflowDocumentAttachmentHandler.GetAllAsync(filterObject);
            // Hander response
            return Helper.TransformData(result);
        }
        #endregion 
    }
}
