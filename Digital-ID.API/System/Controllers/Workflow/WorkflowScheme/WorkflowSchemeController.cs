﻿
using DigitalID.Business;
using DigitalID.Data;
using DigitalID.Data.Models;
using DigitalID.WF.Workflow;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OptimaJet.Workflow;
using Serilog;
using System;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace DigitalID.API
{
    /// <inheritdoc />
    /// <summary>
    /// Module
    /// </summary>
    [ApiVersion("1.0")][ApiController]
    [Route("api/v{api-version:apiVersion}/wf/scheme")]
    [ApiExplorerSettings(GroupName = "Workflow - WorkflowScheme", IgnoreApi = true)]
    public class WorkflowSchemeController : ControllerBase
    {
        private readonly IWorkflowSchemeHandler _workflowSchemeHandler;

        public WorkflowSchemeController(IWorkflowSchemeHandler WorkflowSchemeHandler)
        {
            _workflowSchemeHandler = WorkflowSchemeHandler;
        }
           /// <summary>
        /// Thư viện handler với scheme
        /// </summary>
        [HttpGet,HttpPost]
        [AllowAnonymous]
        [Route("designerapi")]

        public IActionResult API()
        {
            Stream filestream = null;
            var isPost = Request.Method.Equals("POST", StringComparison.OrdinalIgnoreCase);
            if (isPost && Request.Form.Files != null && Request.Form.Files.Count > 0)
                filestream = Request.Form.Files[0].OpenReadStream();

            var pars = new NameValueCollection();
            foreach (var q in Request.Query)
            {
                pars.Add(q.Key, q.Value.First());
            }


            if (isPost)
            {
                var parsKeys = pars.AllKeys;
                //foreach (var key in Request.Form.AllKeys)
                foreach (var key in Request.Form.Keys)
                {
                    if (!parsKeys.Contains(key))
                    {
                        pars.Add(key, Request.Form[key]);
                    }
                }
            }

            var res = WorkflowInit.Runtime.DesignerAPI(pars, filestream);

            if (pars["operation"].ToLower() == "downloadscheme")
                return File(Encoding.UTF8.GetBytes(res), "text/xml", "scheme.xml");
            if (pars["operation"].ToLower() == "downloadschemebpmn")
                return File(Encoding.UTF8.GetBytes(res), "text/xml", "scheme.bpmn");

            return Content(res);

        }

        #region CRUD 
        /// <summary>
        /// Xóa
        /// </summary>
        /// <param name="code">Mã bản ghi</param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [Authorize, HttpDelete, Route("{code}")]
        [ProducesResponseType(typeof(ResponseDelete), StatusCodes.Status200OK)]
        public async Task<IActionResult> DeleteAsync(string code)
        {
            // Get Token Info
            // var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            // var actorId = requestInfo.UserId;
            // var appId = requestInfo.ApplicationId;
            // Call service
            var result = await _workflowSchemeHandler.DeleteAsync(code);
            // Hander response
            return Helper.TransformData(result);
        }
         /// <summary>
        /// Tạo mặc định
        /// </summary>
        /// <param name="code">Mã bản ghi</param>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [Authorize, HttpPost, Route("{code}")]
        [ProducesResponseType(typeof(ResponseObject<WorkflowScheme>), StatusCodes.Status200OK)]
        public async Task<IActionResult> CreateAsync(string code)
        {
            // Get Token Info
            // var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            // var actorId = requestInfo.UserId;
            // var appId = requestInfo.ApplicationId;
            // Call service
            var result = await _workflowSchemeHandler.CreateAsync(code);
            // Hander response
            return Helper.TransformData(result);
        }
        /// <summary>
        /// Xóa
        /// </summary>
        /// <returns>Kết quả trả về</returns>
        /// <response code="200">Thành công</response>
        [Authorize, HttpGet, Route("all")]
        [ProducesResponseType(typeof(ResponseList<WorkflowScheme>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetAllAsync([FromQuery]string filter = "{}")
        {
            // Get Token Info
            // var requestInfo = Helper.GetRequestInfo(Request, HttpContext.User);
            // var actorId = requestInfo.UserId;
            // var appId = requestInfo.ApplicationId;
            // Call service
            var result = await _workflowSchemeHandler.GetAllAsync();
            // Hander response
            return Helper.TransformData(result);
        }
        /// <summary>
        /// Lấy về tất cả state
        /// </summary> 
        /// <returns></returns> 
        /// <response code="200">Thành công</response>
        [Authorize, HttpGet, Route("{code}/states")]
        public async Task<ActionResult<ResponseList<SchemeOptionData>>> GetAllState(string code)
        {
            try
            {
                var result = await WorkflowInit.Runtime.GetAvailableStateToSetAsync(code);
                var resultData = result.Select(x=> new SchemeOptionData()
                {
                    Name = x.VisibleName,
                    Value = x.Name
                });
                return new ResponseList<SchemeOptionData>(resultData.ToList());
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return Helper.TransformData(new ResponseError(Code.ServerError, "Server error please read the logs!"));
            } ;
        }
        /// <summary>
        /// Lấy về tất cả rules
        /// </summary> 
        /// <returns></returns> 
        /// <response code="200">Thành công</response>
        [Authorize, HttpGet, Route("{code}/actors")]
        public async Task<ActionResult<ResponseList<SchemeOptionData>>> GetAllActor(string code)
        {
            try
            {
                var currentScheme =await _workflowSchemeHandler.GetByIdAsync(code);
                if(currentScheme.Code== Code.Success && currentScheme is ResponseObject<WorkflowScheme> currentSchemeData)
                {
                    var xml = XDocument.Parse(currentSchemeData.Data.Scheme);
                    var result = from c in xml.Root.Elements("Actors").FirstOrDefault().Elements("Actor").ToList()
                                 select c.Attribute("Name").Value;
                    var resultData = result.Select(x => new SchemeOptionData()
                    {
                        Name = x,
                        Value = x
                    });
                    return Helper.TransformData(new ResponseList<SchemeOptionData>(resultData.ToList()));
                }
                return Helper.TransformData(currentScheme);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return Helper.TransformData(new ResponseError(Code.ServerError, "Server error please read the logs!"));
            };
        }
        #endregion 
    }
}
