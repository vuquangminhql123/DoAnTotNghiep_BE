﻿namespace DigitalID.API
{

    public class SchemeOptionData  
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }
}
