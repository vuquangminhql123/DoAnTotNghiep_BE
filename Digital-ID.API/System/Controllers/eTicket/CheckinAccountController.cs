﻿using DigitalID.API;
using DigitalID.Business;
using DigitalID.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MISBI.API
{
    /// <summary>
    /// Module lịch sử checkin
    /// </summary>
    [ApiVersion("1.0")]
    [ApiController]
    [Route("api/v{api-version:apiVersion}/e-ticket/checkin")]
    [ApiExplorerSettings(GroupName = "Checkin Account")]
    public class CheckinAccountController : ApiControllerBase
    {
        private readonly ICheckinAccountHandler _handler;
        public CheckinAccountController(ICheckinAccountHandler handler)
        {
            _handler = handler;
        }

        /// <summary>
        /// Thêm mới lịch sử checkin
        /// </summary>
        /// <param name="model">Thông tin lịch sử checkin</param>
        /// <returns>Id lịch sử checkin</returns> 
        /// <response code="200">Thành công</response>
        [Authorize, HttpPost, Route("")]
        [ProducesResponseType(typeof(ResponseObject<Guid>), StatusCodes.Status200OK)]
        public async Task<IActionResult> Create([FromBody] CheckinAccountCreateModel model)
        {
            return await ExecuteFunction(async (RequestUser u) =>
            {
                model.CreatedByUserId = u.UserId;
                model.ApplicationId = u.ApplicationId;
                var result = await _handler.Create(model);

                return result;
            });
        }

        /// <summary>
        /// Lấy danh sách lịch sử checkin theo điều kiện lọc
        /// </summary> 
        /// <param name="filter">Điều kiện lọc</param>
        /// <returns>Danh sách lịch sử checkin</returns> 
        /// <response code="200">Thành công</response>
        [Authorize, HttpPost, Route("filter")]
        [ProducesResponseType(typeof(ResponseObject<List<CheckinAccountBaseModel>>), StatusCodes.Status200OK)]
        public async Task<IActionResult> Filter([FromBody] CheckinAccountQueryFilter filter)
        {
            return await ExecuteFunction(async (RequestUser u) =>
            {
                var result = await _handler.Filter(filter);

                return result;
            });
        }

        /// <summary>
        /// Lấy tất cả danh sách lịch sử checkin
        /// </summary> 
        /// <param name="ts">Từ khóa tìm kiếm</param>
        /// <returns>Danh sách lịch sử checkin</returns> 
        /// <response code="200">Thành công</response>
        [Authorize, HttpGet, Route("all")]
        [ProducesResponseType(typeof(ResponseObject<List<CheckinAccountBaseModel>>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetAll(string ts = null)
        {
            return await ExecuteFunction(async (RequestUser u) =>
            {
                CheckinAccountQueryFilter filter = new CheckinAccountQueryFilter()
                {
                    TextSearch = ts,
                    PageNumber = null,
                    PageSize = null
                };
                var result = await _handler.Filter(filter);

                return result;
            });
        }
    }
}
