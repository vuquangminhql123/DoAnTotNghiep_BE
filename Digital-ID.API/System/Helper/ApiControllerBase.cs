﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using DigitalID.Data;
using Microsoft.AspNetCore.Http;
using Serilog;
using System.Security.Authentication;

namespace DigitalID.API
{
    public class ApiControllerBase : ControllerBase
    {
        protected async Task<IActionResult> ExecuteFunction<T>(Func<RequestUser, Task<T>> func)
        {
            try
            {
                var currentUser = await Helper.GetRequestInfo(HttpContext.Request);
                var result = await func(currentUser);

                if (result is Response)
                {
                    return Helper.TransformData(result as Response);
                }
                if (result is IActionResult)
                {
                    return (IActionResult)result;
                }
                return Helper.TransformData(new ResponseObject<T>(result));
            }
            catch (ArgumentException agrEx)
            {
                Log.Information(agrEx, string.Empty);
                return Helper.TransformData(new ResponseError(Code.BadRequest, agrEx.Message));
            }
            catch (NullReferenceException nullEx)
            {
                Log.Information(nullEx, string.Empty);
                return Helper.TransformData(new ResponseError(Code.NotFound, nullEx.Message));
            }
            catch (AuthenticationException authEx)
            {
                Log.Warning(authEx, string.Empty);
                return Helper.TransformData(new ResponseError(Code.Unauthorized, authEx.Message));
            }
            catch (Exception ex)
            {
                Log.Error(ex, string.Empty);
                return Helper.TransformData(new ResponseError(Code.BadRequest, "An error was occur, read this message for more details: " + ex.Message));
            }
        }

        protected async Task<IActionResult> ExecuteFunction<T>(Func<RequestUser, T> func)
        {
            try
            {
                var currentUser = await Helper.GetRequestInfo(HttpContext.Request);
                var result = func(currentUser);

                if (result is Response)
                {
                    return Helper.TransformData(result as Response);
                }
                if (result is IActionResult)
                {
                    return (IActionResult)result;
                }
                return Helper.TransformData(new ResponseObject<T>(result));
            }
            catch (ArgumentException agrEx)
            {
                Log.Information(agrEx, string.Empty);
                return Helper.TransformData(new ResponseError(Code.BadRequest, agrEx.Message));
            }
            catch (NullReferenceException nullEx)
            {
                Log.Information(nullEx, string.Empty);
                return Helper.TransformData(new ResponseError(Code.NotFound, nullEx.Message));
            }
            catch (AuthenticationException authEx)
            {
                Log.Warning(authEx, string.Empty);
                return Helper.TransformData(new ResponseError(Code.Unauthorized, authEx.Message));
            }
            catch (Exception ex)
            {
                Log.Error(ex, string.Empty);
                return Helper.TransformData(new ResponseError(Code.BadRequest, "An error was occur, read this message for more details: " + ex.Message));
            }
        }
    }
}
