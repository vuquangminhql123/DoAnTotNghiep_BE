using Swashbuckle.AspNetCore.Filters;

namespace DigitalID.API
{
    public class MockupObject<T> : IExamplesProvider where T : new()
    {
        public object GetExamples()
        {
            return GenFu.GenFu.New<T>();
        }
    }
    public class MockupList<T> : IExamplesProvider where T : new()
    {
        public object GetExamples()
        {
            return GenFu.GenFu.ListOf<T>(2);
        }
    }
}