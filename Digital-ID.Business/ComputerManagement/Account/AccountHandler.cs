﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using DigitalID.Data;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using NetCore.Business;
using Org.BouncyCastle.Asn1.Ocsp;
using Serilog;

namespace DigitalID.Business
{
    public class AccountHandler:IAccountHandler
    {
        private readonly DataContext _dataContext;
        private readonly IEmailHandler _emailHandler;
        private readonly ICartHandler _cartHandler;
        private const string CodePrefix = "ACC.";
        private string frontendUrl = Utils.GetConfig("Url:Frontend");
        #region Message
        private string MessageEmailIsNotUnique = "Email đã tồn tại!";
        #endregion
        public AccountHandler(DataContext dataContext, ICartHandler cartHandler, IEmailHandler emailHandler)
        {
            _emailHandler = emailHandler;
            _dataContext = dataContext;
            _cartHandler = cartHandler;
        }
        public async Task<Response> Create(AccountBaseModel model)
        {
            try
            {
                var entity = AutoMapperUtils.AutoMap<AccountBaseModel, User>(model);

                #region Check Username Unique
                var ckUserName = await _dataContext.Users.AnyAsync(x => x.Username.Equals(entity.Username));
                if (ckUserName)
                {
                    return new ResponseError(Code.BadRequest, "Tài khoản đã tồn tại");
                }
                #endregion
                #region Check Email Unique
                var ckMail = await _dataContext.Users.AnyAsync(x => x.Email.Equals(entity.Email));
                if (ckMail)
                {
                    return new ResponseError(Code.BadRequest, MessageEmailIsNotUnique);
                }
                #endregion

                entity.CreatedDate = DateTime.Now;

                long identityNumber = await _dataContext.Users.DefaultIfEmpty().MaxAsync(x => x.IdentityNumber);

                entity.IdentityNumber = ++identityNumber;
                entity.Code = Utils.GenerateAutoCode(CodePrefix, identityNumber);
                entity.Id = Guid.NewGuid();
                entity.PasswordSalt = Utils.PassowrdCreateSalt512();
                entity.Password = Utils.PasswordGenerateHmac(entity.Password, entity.PasswordSalt);
                entity.CreatedDate = DateTime.Now;
                entity.ModifiedDate = DateTime.Now;
                await _dataContext.Users.AddAsync(entity);
                //var userModel = new UserCreateModel()
                //{
                //    Id = Guid.NewGuid(),
                //    UserName = entity.Username,
                //    AvatarUrl = entity.Avatar,
                //    Name = entity.Name
                //};
                //var request = AutoMapperUtils.AutoMap<UserCreateModel, IdmUser>(userModel);
                //request.Type = 1;//Loại tài khoản nội bộ
                //request.LastActivityDate = DateTime.Now;
                //request.PasswordSalt = Utils.PassowrdCreateSalt512();
                //request.Password = Utils.PasswordGenerateHmac(entity.Password, request.PasswordSalt);
                //var result = await _dataContext.IdmUser.AddAsync(request);
                var user_cart = new CartCreateModel()
                {
                    UserId = entity.Id,
                    Status = 1
                };
                var rs = _cartHandler.Create(user_cart);
                int dbSave = await _dataContext.SaveChangesAsync();

                AccountBaseModel accountBase = new AccountBaseModel()
                {
                    Id = entity.Id,
                    Name = entity.Name,
                    Email = entity.Email,
                    IsAdmin = entity.IsAdmin
                };

                if (dbSave > 0)
                {
                    #region Gửi email thông báo đến khách hàng

                    string title = "[Vân Anh - Computer] - Đăng ký tài khoản thành công";


                    StringBuilder htmlBody = new StringBuilder();
                    htmlBody.Append("<html><body>");
                    htmlBody.Append("<p>Xin chào <b>" + entity.Name + "</b>,</p>");
                    htmlBody.Append("<p>Bạn đã đăng ký tài khoản thành công. Hãy tiếp tục ghé thăm Vân anh Computer để chọn lựa cho mình những sản phẩm ưu đãi nhất.</p>");
                    htmlBody.Append("<p><a href='"+ frontendUrl + "'><span class='fas fa - laptop'></span> <p>Vân Anh<strong> Computer</strong> <span>Thế giới máy tính<span></p> </a> </p>");
                    htmlBody.Append("</body></html>");

                    _emailHandler.SendMailGoogle(new List<string>() { entity.Email }, null, null, title, htmlBody.ToString());

                    #endregion
                    //Log.Information("Add success: " + JsonSerializer.Serialize(entity));
                    return new ResponseObject<AccountBaseModel>(accountBase, MessageConstants.CreateSuccessMessage, Code.Success);
                }
                else
                {
                    return new ResponseError(Code.ServerError, MessageConstants.CreateErrorMessage);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, MessageConstants.ErrorLogMessage);
                return new ResponseError(Code.ServerError, $"{MessageConstants.CreateErrorMessage} - {ex.Message}");
            }
        }

        public Response Authentication(string userName, string password, bool isNotLogin = false)
        {
            using (var unitOfWork = new UnitOfWork())
            {
                AccountBaseModel userReturn = new AccountBaseModel();
                var userCollection = unitOfWork.GetRepository<User>().GetAll().ToList();
                var user = unitOfWork.GetRepository<User>().GetMany(x => x.Username == userName).FirstOrDefault();
                if (user != null)
                {
                    var passhash = Utils.PasswordGenerateHmac(password, user.PasswordSalt);
                    if (passhash == user.Password && user.IsLock == false)
                    {
                        foreach (var item in userCollection)
                        {
                            if (item.Username == userName)
                                userReturn = AutoMapperUtils.AutoMap<User, AccountBaseModel>(item);
                        }
                        return new ResponseObject<AccountBaseModel>(userReturn);
                    }
                    return new ResponseError(Code.BadRequest, "Sai mật khẩu");
                }

                return new ResponseError(Code.BadRequest, "Không tìm thấy tài khoản");
            }
        }

        public Response AuthenticationGoogle(string userName, string password, string provider, bool isNotLogin = false)
        {
            using (var unitOfWork = new UnitOfWork())
            {
                AccountBaseModel userReturn = new AccountBaseModel();
                var userCollection = unitOfWork.GetRepository<User>().GetAll().ToList();
                var user = unitOfWork.GetRepository<User>().GetMany(x => x.Username == userName && x.Provider.Equals(provider)).FirstOrDefault();
                if (user != null)
                {
                    if (password == user.Password && user.IsLock == false)
                    {
                        foreach (var item in userCollection)
                        {
                            if (item.Username == userName && item.Provider.Equals(provider))
                                userReturn = AutoMapperUtils.AutoMap<User, AccountBaseModel>(item);
                        }
                        return new ResponseObject<AccountBaseModel>(userReturn);
                    }
                    return new ResponseError(Code.BadRequest, "Sai mật khẩu");
                }

                return new ResponseError(Code.BadRequest, "Không tìm thấy tài khoản");
            }
        }
    }
}
