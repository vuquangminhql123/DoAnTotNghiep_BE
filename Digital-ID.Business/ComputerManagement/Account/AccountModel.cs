﻿using System;
using System.Collections.Generic;
using System.Text;
using DigitalID.Data;

namespace DigitalID.Business
{
   public class AccountBaseModel
    {
        public Guid Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string AvatarUrl { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public bool IsLock { get; set; }
        public bool IsSocialSign { get; set; }
        public bool IsAdmin { get; set; }
        public string Avatar { get; set; }
        public DateTime DateOfBirth { get; set; }
        public bool Sex { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
    }
   public class AccountQueryFilter
   {
       public string TextSearch { get; set; }
       public int? PageSize { get; set; }
       public int? PageNumber { get; set; }
       public bool? Status { get; set; }
       public string PropertyName { get; set; } = "CreatedDate";
       //asc - desc
       public string Ascending { get; set; } = "desc";
       public AccountQueryFilter()
       {
           PageNumber = QueryFilter.DefaultPageNumber;
           PageSize = QueryFilter.DefaultPageSize;
       }
   }
}
