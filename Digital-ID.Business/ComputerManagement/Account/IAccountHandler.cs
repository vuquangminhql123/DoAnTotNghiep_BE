﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DigitalID.Data;

namespace DigitalID.Business
{
    public interface IAccountHandler
    {
        /// <summary>
        /// Thêm mới khách hàng
        /// </summary>
        /// <param name="model">Model thêm mới khách hàng</param>
        /// <returns>Id khách hàng</returns>
        Task<Response> Create(AccountBaseModel model);
        Response Authentication(string userName, string password, bool isNotLogin = false);

        Response AuthenticationGoogle(string userName, string password,string provider, bool isNotLogin = false);
    }
}
