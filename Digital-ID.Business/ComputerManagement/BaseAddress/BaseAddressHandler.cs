﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DigitalID.Business;
using DigitalID.Data;
using Microsoft.EntityFrameworkCore;
using Serilog;

namespace NetCore.Business
{
   public class BaseAddressHandler : IBaseAddressHandler
    {
        private readonly DataContext _dataContext;
        private readonly ICacheService _cacheService;

        public BaseAddressHandler(DataContext dataContext, ICacheService cacheService)
        {
            _dataContext = dataContext;
            _cacheService = cacheService;
        }
        public async Task<Response> GetCity()
        {
            try
            {
                var cities = await _dataContext.Cities.ToListAsync();
                if (cities.Count > 0)
                {
                    return new ResponseObject<List<City>>(cities, MessageConstants.UpdateSuccessMessage, Code.Success);
                }
                else
                {
                    return new ResponseError(Code.ServerError, MessageConstants.UpdateErrorMessage);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, MessageConstants.ErrorLogMessage);
                return new ResponseError(Code.ServerError, $"{MessageConstants.GetDataErrorMessage} - {ex.Message}");
            }
        }
        public async Task<Response> GetDistrictByCity(string matp)
        {
            try
            {
                var districts = await _dataContext.Districts.Where(x=> x.matp == matp).ToListAsync();
                if (districts.Count > 0)
                {
                    return new ResponseObject<List<District>>(districts, MessageConstants.UpdateSuccessMessage, Code.Success);
                }
                else
                {
                    return new ResponseError(Code.ServerError, MessageConstants.UpdateErrorMessage);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, MessageConstants.ErrorLogMessage);
                return new ResponseError(Code.ServerError, $"{MessageConstants.GetDataErrorMessage} - {ex.Message}");
            }
        }
        public async Task<Response> GetCommuneByDistrict(string maqh)
        {
            try
            {
                var listCommunesRs = new List<Commune>();
                var communes = await _dataContext.Communes.ToListAsync();
                if (!string.IsNullOrEmpty(maqh))
                {
                    foreach (var item in communes)
                    {
                        var idQh = item.maqh.TrimStart('0');
                        if (idQh == maqh)
                        {
                            listCommunesRs.Add(item);
                        }
                    }
                }
                if (communes.Count > 0)
                {
                    return new ResponseObject<List<Commune>>(listCommunesRs, MessageConstants.UpdateSuccessMessage, Code.Success);
                }
                else
                {
                    return new ResponseError(Code.Success, "Không có dữ liệu");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, MessageConstants.ErrorLogMessage);
                return new ResponseError(Code.ServerError, $"{MessageConstants.GetDataErrorMessage} - {ex.Message}");
            }
        }
    }
}
