﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DigitalID.Data;

namespace DigitalID.Business
{
    public interface IBaseAddressHandler
    {
        Task<Response> GetCity();
        Task<Response> GetDistrictByCity(string matp);
        Task<Response> GetCommuneByDistrict(string maqh);
    }
}
