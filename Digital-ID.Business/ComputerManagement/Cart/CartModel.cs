﻿using DigitalID.Data;
using System;
using System.Collections.Generic;

namespace NetCore.Business
{
    public class CartBaseModel
    {
        public Guid Id { get; set; }
        public string Code { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string ListProducts { get; set; }
        public Guid UserId { get; set; }
        public string Name { get; set; }
        public string Username { get; set; }
        public int Status { get; set; }
    }

    public class CartModel : CartBaseModel
    {
        public int Order { get; set; } = 0;

    }

    public class CartDetailModel : CartModel
    {
        public Guid? CreatedUserId { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public Guid? ModifiedUserId { get; set; }
    }

    public class CartCreateModel : CartModel
    {
        public Guid? CreatedUserId { get; set; }
        public Guid? ApplicationId { get; set; }
    }

    public class CartUpdateModel : CartModel
    {
        public Guid ModifiedUserId { get; set; }

        public void UpdateToEntity(Cart entity)
        {
            entity.Code = this.Code;
            entity.Name = this.Name;
            entity.Status = this.Status;
            entity.ModifiedDate = DateTime.Now;
            entity.UserId = UserId;
        }
    }

    public class CartQueryFilter
    {
        public string TextSearch { get; set; }
        public int? PageSize { get; set; }
        public Guid? UserId { get; set; }
        public int? PageNumber { get; set; }
        public int? Status { get; set; }
        public string PropertyName { get; set; } = "CreatedDate";
        //asc - desc
        public string Ascending { get; set; } = "desc";
        public CartQueryFilter()
        {
            PageNumber = QueryFilter.DefaultPageNumber;
            PageSize = QueryFilter.DefaultPageSize;
        }
    }
}