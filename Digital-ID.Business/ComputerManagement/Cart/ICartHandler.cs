﻿
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DigitalID.Data;

namespace NetCore.Business
{
    /// <summary>
    /// Interface quản lý loại chứng chỉ
    /// </summary>
    public interface ICartHandler
    {
        /// <summary>
        /// Thêm mới giỏ hàng
        /// </summary>
        /// <param name="model">Model thêm mới giỏ hàng</param>
        /// <returns>Id giỏ hàng</returns>
        Task<Response> Create(CartCreateModel model);

        /// <summary>
        /// Thêm mới giỏ hàng theo danh sách
        /// </summary>
        /// <param name="list">Danh sách thông tin giỏ hàng</param>
        /// <returns>Danh sách kết quả thêm mới</returns> 
        Task<Response> CreateMany(List<CartCreateModel> list);

        /// <summary>
        /// Cập nhật giỏ hàng
        /// </summary>
        /// <param name="model">Model cập nhật giỏ hàng</param>
        /// <returns>Id giỏ hàng</returns>
        Task<Response> Update(Guid userId, string listProducts);

        /// <summary>
        /// Xóa giỏ hàng
        /// </summary>
        /// <param name="listId">Danh sách Id giỏ hàng</param>
        /// <returns>Danh sách kết quả xóa</returns>
        Task<Response> Delete(List<Guid> listId);

        /// <summary>
        /// Lấy danh sách giỏ hàng theo điều kiện lọc
        /// </summary>
        /// <param name="filter">Model điều kiện lọc</param>
        /// <returns>Danh sách giỏ hàng</returns>
        Task<Response> Filter(CartQueryFilter filter);

        /// <summary>
        /// Lấy giỏ hàng theo Id
        /// </summary>
        /// <param name="id">Id giỏ hàng</param>
        /// <returns>Thông tin giỏ hàng</returns>
        Task<Response> GetById(Guid id);

        /// <summary>
        /// Lấy danh sách giỏ hàng cho combobox
        /// </summary>
        /// <param name="count">Số bản ghi tối đa</param>
        /// <param name="textSearch">Từ khóa tìm kiếm</param>
        /// <returns>Danh sách giỏ hàng cho combobox</returns>
        Task<Response> GetListCombobox(int count = 0, string textSearch = "");
    }
}
