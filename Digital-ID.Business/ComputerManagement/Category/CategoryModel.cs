﻿using DigitalID.Data;
using System;
using System.Collections.Generic;

namespace NetCore.Business
{
    public class CategoryBaseModel
    {
        public Guid Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string ParentName { get; set; }
        public bool? Status { get; set; }
        public string Content { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public Guid? ParentId { get; set; }
    }

    public class CategoryModel : CategoryBaseModel
    {
        public int Order { get; set; } = 0;

    }

    public class CategoryDetailModel : CategoryModel
    {
        public Guid? CreatedUserId { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public Guid? ModifiedUserId { get; set; }
    }

    public class CategoryCreateModel : CategoryModel
    {
        public Guid? CreatedUserId { get; set; }
        public Guid? ApplicationId { get; set; }
    }

    public class CategoryUpdateModel : CategoryModel
    {
        public Guid ModifiedUserId { get; set; }

        public void UpdateToEntity(Category entity)
        {
            entity.Code = this.Code;
            entity.Name = this.Name;
            entity.Status = this.Status;
            entity.ModifiedDate = DateTime.Now;
            entity.Content = this.Content;
            entity.ParentId = this.ParentId;
        }
    }

    public class CategoryQueryFilter
    {
        public string TextSearch { get; set; }
        public int? PageSize { get; set; }
        public Guid? ParentId { get; set; }
        public int? PageNumber { get; set; }
        public bool? Status { get; set; }
        public string PropertyName { get; set; } = "CreatedDate";
        //asc - desc
        public string Ascending { get; set; } = "desc";
        public CategoryQueryFilter()
        {
            PageNumber = QueryFilter.DefaultPageNumber;
            PageSize = QueryFilter.DefaultPageSize;
        }
    }
}