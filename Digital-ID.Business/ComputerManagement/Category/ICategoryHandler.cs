﻿
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DigitalID.Data;

namespace NetCore.Business
{
    /// <summary>
    /// Interface quản lý loại chứng chỉ
    /// </summary>
    public interface ICategoryHandler
    {
        /// <summary>
        /// Thêm mới loại sản phẩm
        /// </summary>
        /// <param name="model">Model thêm mới loại sản phẩm</param>
        /// <returns>Id loại sản phẩm</returns>
        Task<Response> Create(CategoryCreateModel model);

        /// <summary>
        /// Thêm mới loại sản phẩm theo danh sách
        /// </summary>
        /// <param name="list">Danh sách thông tin loại sản phẩm</param>
        /// <returns>Danh sách kết quả thêm mới</returns> 
        Task<Response> CreateMany(List<CategoryCreateModel> list);

        /// <summary>
        /// Cập nhật loại sản phẩm
        /// </summary>
        /// <param name="model">Model cập nhật loại sản phẩm</param>
        /// <returns>Id loại sản phẩm</returns>
        Task<Response> Update(CategoryUpdateModel model);

        /// <summary>
        /// Xóa loại sản phẩm
        /// </summary>
        /// <param name="listId">Danh sách Id loại sản phẩm</param>
        /// <returns>Danh sách kết quả xóa</returns>
        Task<Response> Delete(List<Guid> listId);

        /// <summary>
        /// Lấy danh sách loại sản phẩm theo điều kiện lọc
        /// </summary>
        /// <param name="filter">Model điều kiện lọc</param>
        /// <returns>Danh sách loại sản phẩm</returns>
        Task<Response> Filter(CategoryQueryFilter filter);

        /// <summary>
        /// Lấy loại sản phẩm theo Id
        /// </summary>
        /// <param name="id">Id loại sản phẩm</param>
        /// <returns>Thông tin loại sản phẩm</returns>
        Task<Response> GetById(Guid id);

        /// <summary>
        /// Lấy danh sách loại sản phẩm cho combobox
        /// </summary>
        /// <param name="count">Số bản ghi tối đa</param>
        /// <param name="textSearch">Từ khóa tìm kiếm</param>
        /// <returns>Danh sách loại sản phẩm cho combobox</returns>
        Task<Response> GetListCombobox(int count = 0, string textSearch = "");
    }
}
