﻿using System;
using System.Collections.Generic;
using System.Text;
using DigitalID.Data;

namespace DigitalID.Business
{
    public class CategoryMetaProductBaseModel
    {
        public Guid Id { get; set; }
        public string Code { get; set; }
        public Guid CategoryMetaId { get; set; }
        public Guid ProductId { get; set; }
        public bool? Status { get; set; }
        public string Content { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
    }

    public class CategoryMetaProductModel : CategoryMetaProductBaseModel
    {
        public int Order { get; set; } = 0;

    }

    public class CategoryMetaProductDetailModel : CategoryMetaProductModel
    {
        public Guid? CreatedUserId { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public Guid? ModifiedUserId { get; set; }
    }

    public class CategoryMetaProductCreateModel : CategoryMetaProductModel
    {
        public Guid? CreatedUserId { get; set; }
        public Guid? ApplicationId { get; set; }
    }

    public class CategoryMetaProductUpdateModel : CategoryMetaProductModel
    {
        public Guid ModifiedUserId { get; set; }

        public void UpdateToEntity(Category_Meta_Product entity)
        {
            entity.ProductId = ProductId;
            entity.Status = Status;
            entity.CategoryMetaId = CategoryMetaId;
            entity.Content = Content;
            entity.ModifiedDate = DateTime.Now;
        }
    }

    public class CategoryMetaProductQueryFilter
    {
        public string TextSearch { get; set; }
        public int? PageSize { get; set; }
        public int? PageNumber { get; set; }
        public bool? Status { get; set; }

        public string PropertyName { get; set; } = "CreatedDate";

        //asc - desc
        public string Ascending { get; set; } = "desc";

        public CategoryMetaProductQueryFilter()
        {
            PageNumber = QueryFilter.DefaultPageNumber;
            PageSize = QueryFilter.DefaultPageSize;
        }
    }
}
