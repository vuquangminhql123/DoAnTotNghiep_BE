﻿
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DigitalID.Business;
using DigitalID.Data;

namespace NetCore.Business
{
    /// <summary>
    /// Interface quản lý loại chứng chỉ
    /// </summary>
    public interface ICategoryMetaProductHandler
    {
        /// <summary>
        /// Thêm mới thông tin bổ sung
        /// </summary>
        /// <param name="model">Model thêm mới thông tin bổ sung</param>
        /// <returns>Id thông tin bổ sung</returns>
        Task<Response> Create(CategoryMetaProductCreateModel model);

        /// <summary>
        /// Thêm mới thông tin bổ sung theo danh sách
        /// </summary>
        /// <param name="list">Danh sách thông tin thông tin bổ sung</param>
        /// <returns>Danh sách kết quả thêm mới</returns> 
        Task<Response> CreateMany(List<CategoryMetaProductCreateModel> list);

        /// <summary>
        /// Cập nhật thông tin bổ sung
        /// </summary>
        /// <param name="model">Model cập nhật thông tin bổ sung</param>
        /// <returns>Id thông tin bổ sung</returns>
        Task<Response> Update(CategoryMetaProductUpdateModel model);

        /// <summary>
        /// Xóa thông tin bổ sung
        /// </summary>
        /// <param name="listId">Danh sách Id thông tin bổ sung</param>
        /// <returns>Danh sách kết quả xóa</returns>
        Task<Response> Delete(List<Guid> listId);

        /// <summary>
        /// Lấy danh sách thông tin bổ sung theo điều kiện lọc
        /// </summary>
        /// <param name="filter">Model điều kiện lọc</param>
        /// <returns>Danh sách thông tin bổ sung</returns>
        Task<Response> Filter(CategoryMetaProductQueryFilter filter);

        /// <summary>
        /// Lấy thông tin bổ sung theo Id
        /// </summary>
        /// <param name="id">Id thông tin bổ sung</param>
        /// <returns>Thông tin thông tin bổ sung</returns>
        Task<Response> GetById(Guid id);

        /// <summary>
        /// Lấy danh sách thông tin bổ sung cho combobox
        /// </summary>
        /// <param name="count">Số bản ghi tối đa</param>
        /// <param name="textSearch">Từ khóa tìm kiếm</param>
        /// <returns>Danh sách thông tin bổ sung cho combobox</returns>
        Task<Response> GetListCombobox(int count = 0, string textSearch = "");
    }
}
