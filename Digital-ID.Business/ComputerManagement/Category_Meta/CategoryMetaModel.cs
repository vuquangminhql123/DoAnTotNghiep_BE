﻿using DigitalID.Data;
using System;
using System.Collections.Generic;

namespace NetCore.Business
{
    public class CategoryMetaBaseModel
    {
        public Guid Id { get; set; }
        public DateTime CreatedDate { get; set; }
        public string Code { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string Key { get; set; }
        public string CategoryName { get; set; }
        public bool? Status { get; set; }
    }

    public class CategoryMetaModel : CategoryMetaBaseModel
    {
        public int Order { get; set; } = 0;

    }

    public class CategoryMetaDetailModel : CategoryMetaModel
    {
        public Guid? CreatedUserId { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public Guid? ModifiedUserId { get; set; }
    }

    public class CategoryMetaCreateModel : CategoryMetaModel
    {
        public Guid? CreatedUserId { get; set; }
        public Guid? ApplicationId { get; set; }
    }

    public class CategoryMetaUpdateModel : CategoryMetaModel
    {
        public Guid ModifiedUserId { get; set; }

        public void UpdateToEntity(Category_Meta entity)
        {
            entity.Code = this.Code;
            entity.Status = this.Status;
            entity.ModifiedDate = DateTime.Now;
            entity.Key = this.Key;
        }
    }

    public class CategoryMetaQueryFilter
    {
        public string TextSearch { get; set; }
        public int? PageSize { get; set; }
        public int? PageNumber { get; set; }
        public bool? Status { get; set; }
        public string PropertyName { get; set; } = "CreatedDate";
        //asc - desc
        public string Ascending { get; set; } = "desc";
        public CategoryMetaQueryFilter()
        {
            PageNumber = QueryFilter.DefaultPageNumber;
            PageSize = QueryFilter.DefaultPageSize;
        }
    }
}