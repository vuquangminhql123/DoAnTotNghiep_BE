﻿using Microsoft.EntityFrameworkCore;
using Serilog;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.util;
using DigitalID.Business;
using DigitalID.Data;
using Microsoft.AspNetCore.SignalR;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using JsonSerializer = System.Text.Json.JsonSerializer;
using Nancy.Json;

namespace NetCore.Business
{
    public class OrderHandler : IOrderHandler
    {
        #region Message
        #endregion
        private readonly IEmailHandler _emailHandler;
                private string frontendUrl = Utils.GetConfig("Url:Frontend");
        private const string CachePrefix = "Order";
        private const string SelectItemCacheSubfix = "list-select";
        private const string CodePrefix = "Order.";
        private const string CodeOrderProdPrefix = "OrderProd.";
        private readonly DataContext _dataContext;
        private readonly ICacheService _cacheService;
        private readonly IHubContext<SignalrHub, IHubClient> _signalrHub;
        public OrderHandler(DataContext dataContext, ICacheService cacheService, IEmailHandler emailHandler, IHubContext<SignalrHub, IHubClient> signalrHub)
        {
            _signalrHub = signalrHub;
            _emailHandler = emailHandler;
            _dataContext = dataContext;
            _cacheService = cacheService;
        }

        public async Task<Response> Create(OrderCreateModel model)
        {
            try
            {
                #region Check is exist document Type
                var isExistEmail = _dataContext.Orders.Any(c => c.Code == model.Code);
                if (isExistEmail)
                    return new ResponseError(Code.NotFound, "Mã đơn đặt đã tồn tại");

                #endregion

                var entity = AutoMapperUtils.AutoMap<OrderCreateModel, Order>(model);

                entity.CreatedDate = DateTime.Now;

                long identityNumber = await _dataContext.Orders.DefaultIfEmpty().MaxAsync(x => x.IdentityNumber);

                entity.IdentityNumber = ++identityNumber;
                entity.Code = Utils.GenerateAutoCode(CodePrefix, identityNumber);

                entity.Id = Guid.NewGuid();
                await _dataContext.Orders.AddAsync(entity);
                JavaScriptSerializer js = new JavaScriptSerializer();
                ProductBaseModel[] products = js.Deserialize<ProductBaseModel[]>(model.ListProducts);
                foreach (var prod in products)
                {

                    var orderProduct = new Order_Product()
                    {
                        Id = Guid.NewGuid(),
                        ProductId = prod.Id,
                        OrderId = entity.Id,
                        Price = prod.Price,
                        Discount = prod.Discount,
                        Quantity = prod.Count,
                        CreatedDate = DateTime.Now,
                        ModifiedDate = DateTime.Now,
                        CreatedUserId = entity.CreatedUserId,
                        ModifiedUserId = entity.ModifiedUserId
                    };
                    long identityNumberOp = await _dataContext.OrderProducts.DefaultIfEmpty().MaxAsync(x => x.IdentityNumber);
                    orderProduct.Code = Utils.GenerateAutoCode(CodeOrderProdPrefix, identityNumberOp);
                    orderProduct.IdentityNumber = ++identityNumberOp;
                   await  _dataContext.OrderProducts.AddAsync(orderProduct);
                }

                if (model.UserId.HasValue)
                {
                    var user = await _dataContext.Users.Where(x => x.Id.ToString().Trim() == model.UserId.Value.ToString().Trim()).FirstOrDefaultAsync();
                    if (user != null)
                    {
                        user.Vouchers = model.Vouchers;
                        _dataContext.Users.Update(user);
                    }
                    var cart =_dataContext.Carts.Where(x => x.UserId.ToString().Trim() == model.UserId.Value.ToString().Trim()).FirstOrDefault();
                    if (cart != null)
                    {
                        cart.ListProducts = null;
                        _dataContext.Carts.Update(cart);
                        int save = await _dataContext.SaveChangesAsync();
                    }
                }
                //Log.Information("Add success: " + JsonSerializer.Serialize(entity));
                InvalidCache();
                #region Gửi email thông báo đến khách hàng

                string title = "[Vân Anh - Computer] - Đặt hàng thành công";


                StringBuilder htmlBody = new StringBuilder();
                htmlBody.Append("<html><body>");
                htmlBody.Append("<p>Xin chào <b>" + entity.Name + "</b>,</p>");
                htmlBody.Append("<p>Đơn hàng " + entity.Code + "đã được đặt thành công</p>");
                htmlBody.Append(" <table><tr><th> STT </th ><th > Tên </th ><th > Số lượng </th ><th > Giá </th ></tr >");
                for (int i = 0; i < products.Length; i++)
                {
                    htmlBody.Append("<tr ><td > "+(i + 1) +" </td ><td > "+products[i].Name+" </td ><td >"+products[i].Count+" </td ><td > "+ String.Format("{0:#,##0.00đ;(#,##0.00đ);Zero}", products[i].Price) +" </td ></tr >");
                }
                htmlBody.Append("</table >");
                htmlBody.Append("Tổng tiền: "+ String.Format("{0:#,##0.00đ;(#,##0.00đ);Zero}", entity.GrandTotal) );
                htmlBody.Append(
                    "<br>Hãy tiếp tục ghé thăm Vân anh Computer để chọn lựa cho mình những sản phẩm ưu đãi nhất.");
                htmlBody.Append("<p><a href='"+ frontendUrl + "'><span class='fas fa - laptop'></span> <p>Vân Anh<strong> Computer</strong> <span>Thế giới máy tính<span></p> </a> </p>");
                htmlBody.Append("</body></html>");

                _emailHandler.SendMailGoogle(new List<string>() { entity.Email }, null, null, title, htmlBody.ToString());

                #endregion
                var notify = new Notification()
                {
                    Id = Guid.NewGuid(),
                    CreatedDate = DateTime.Now,
                    IsRead = false,
                    Description = entity.Name + " đã đặt một đơn hàng với giá: "+ String.Format("{0:#,##0.00đ;(#,##0.00đ);Zero}",entity.GrandTotal),
                    Title = "Thông báo đơn đặt hàng mới",
                    UserId = model.CreatedUserId.HasValue? model.CreatedUserId.Value:UserConstants.UserId
                };
                await _dataContext.Notifications.AddAsync(notify);

                int dbSave = await _dataContext.SaveChangesAsync();
                if (dbSave > 0)
                {
                    var listNotify = from noti in _dataContext.Notifications where noti.IsRead == false
                        select (new Notification()
                        {
                            Id = noti.Id,
                            CreatedDate = noti.CreatedDate,
                            IsRead = noti.IsRead,
                            Description = noti.Description,
                            Title = noti.Title,
                            UserId = noti.UserId
                        });
                    MessageInstance msg = new MessageInstance()
                    {
                        Timestamp = DateTime.Now.ToString("yyyy-MM-dd"),
                        From = entity.Name,
                        ListNotifications = listNotify.ToList()
                    };
                    await _signalrHub.Clients.All.BroadcastMessage(msg);
                }
                return new ResponseObject<string>(entity.Code, MessageConstants.CreateSuccessMessage, Code.Success);
            }
            catch (Exception ex)
            {
                Log.Error(ex, MessageConstants.ErrorLogMessage);
                return new ResponseError(Code.ServerError, $"{MessageConstants.CreateErrorMessage} - {ex.Message}");
            }
        }

        public async Task<Response> CreateMany(List<OrderCreateModel> list)
        {
            try
            {

                var listId = new List<Guid>();
                var listRS = new List<Order>();
                foreach (var item in list)
                {
                    var entity = AutoMapperUtils.AutoMap<OrderCreateModel, Order>(item);

                    entity.CreatedDate = DateTime.Now;
                    await _dataContext.Orders.AddAsync(entity);
                    listId.Add(entity.Id);
                    listRS.Add(entity);
                }

                int dbSave = await _dataContext.SaveChangesAsync();

                if (dbSave > 0)
                {
                    Log.Information("Add success: " + JsonSerializer.Serialize(listRS));
                    InvalidCache();

                    return new ResponseObject<List<Guid>>(listId, MessageConstants.CreateSuccessMessage, Code.Success);
                }
                else
                {
                    return new ResponseError(Code.ServerError, MessageConstants.CreateErrorMessage);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, MessageConstants.ErrorLogMessage);
                return new ResponseError(Code.ServerError, $"{MessageConstants.CreateErrorMessage} - {ex.Message}");
            }
        }

        public async Task<Response> Update(OrderUpdateModel model)
        {
            try
            {
                #region Check is exist document Type
                var isExistEmail = _dataContext.Orders.Any(c => c.Code == model.Code && c.Id != model.Id);
                if (isExistEmail)
                    return new ResponseError(Code.NotFound, "Mã đơn đặt đã tồn tại");

                #endregion

                var entity = await _dataContext.Orders
                         .FirstOrDefaultAsync(x => x.Id == model.Id);
                //Log.Information("Before Update: " + JsonSerializer.Serialize(entity));

                model.UpdateToEntity(entity);

                _dataContext.Orders.Update(entity);

                int dbSave = await _dataContext.SaveChangesAsync();
                if (dbSave > 0)
                {
                    //Log.Information("After Update: " + JsonSerializer.Serialize(entity));
                    InvalidCache(model.Id.ToString());

                    return new ResponseObject<Guid>(model.Id, MessageConstants.UpdateSuccessMessage, Code.Success);
                }
                else
                {
                    return new ResponseError(Code.ServerError, MessageConstants.UpdateErrorMessage);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, MessageConstants.ErrorLogMessage);
                return new ResponseError(Code.ServerError, $"{MessageConstants.GetDataErrorMessage} - {ex.Message}");
            }
        }

        public async Task<Response> UpdateStatusOrder(OrderUpdateModel model, Guid? userId)
        {
            try
            {
                var orderModel = await _dataContext.Orders.Where(x => x.Id == model.Id).FirstOrDefaultAsync();
                if (orderModel != null)
                {
                    orderModel.Status = model.Status;
                    orderModel.ModifiedDate = DateTime.Now;
                    orderModel.ModifiedUserId = userId;
                    switch (model.Status)
                    {
                        case -1:
                            orderModel.CanceledDate = DateTime.Now;
                            break;
                        case 1:
                            orderModel.ConfirmedDate = DateTime.Now;
                            break;
                        case 2:
                            orderModel.DeliverDate = DateTime.Now;
                            break;
                        case 3:
                            orderModel.RecivedDate = DateTime.Now;
                            break;
                        default:
                            break;
                    }
                    _dataContext.Orders.Update(orderModel);
                    var dbSave = await _dataContext.SaveChangesAsync();
                    var orderBaseModel = AutoMapperUtils.AutoMap<Order, OrderModel>(orderModel);
                    if (dbSave > 0)
                    {
                        return new ResponseObject<OrderModel>(orderBaseModel, MessageConstants.GetDataSuccessMessage, Code.Success);
                    }
                    return new ResponseObject<OrderModel>(null, MessageConstants.GetDataErrorMessage, Code.Success);

                }
                return new ResponseObject<OrderModel>(null, MessageConstants.GetDataErrorMessage, Code.Success);
            }
            catch (Exception ex)
            {
                Log.Error(ex, MessageConstants.ErrorLogMessage);
                return new ResponseError(Code.ServerError, $"{MessageConstants.DeleteErrorMessage} - {ex.Message}");
            }
        }

        public async Task<Response> Delete(List<Guid> listId)
        {
            try
            {
                var listResult = new List<ResponeDeleteModel>();
                var name = "";
                Log.Information("List Delete: " + JsonSerializer.Serialize(listId));
                foreach (var item in listId)
                {
                    name = "";
                    var entity = await _dataContext.Orders.FindAsync(item);

                    if (entity == null)
                    {
                        listResult.Add(new ResponeDeleteModel()
                        {
                            Id = item,
                            Name = name,
                            Result = false,
                            Message = MessageConstants.DeleteItemNotFoundMessage
                        });
                    }
                    else
                    {
                        name = entity.Name;
                        _dataContext.Orders.Remove(entity);
                        try
                        {
                            int dbSave = await _dataContext.SaveChangesAsync();
                            if (dbSave > 0)
                            {
                                InvalidCache(item.ToString());

                                listResult.Add(new ResponeDeleteModel()
                                {
                                    Id = item,
                                    Name = name,
                                    Result = true,
                                    Message = MessageConstants.DeleteItemSuccessMessage
                                });
                            }
                            else
                            {
                                listResult.Add(new ResponeDeleteModel()
                                {
                                    Id = item,
                                    Name = name,
                                    Result = false,
                                    Message = MessageConstants.DeleteItemErrorMessage
                                });
                            }
                        }
                        catch (Exception ex)
                        {
                            Log.Error(ex, MessageConstants.ErrorLogMessage);
                            listResult.Add(new ResponeDeleteModel()
                            {
                                Id = item,
                                Name = name,
                                Result = false,
                                Message = ex.Message
                            });
                        }
                    }
                }
                Log.Information("List Result Delete: " + JsonSerializer.Serialize(listResult));
                return new ResponseObject<List<ResponeDeleteModel>>(listResult, MessageConstants.DeleteSuccessMessage, Code.Success);

            }
            catch (Exception ex)
            {
                Log.Error(ex, MessageConstants.ErrorLogMessage);
                return new ResponseError(Code.ServerError, $"{MessageConstants.DeleteErrorMessage} - {ex.Message}");
            }
        }

        public async Task<Response> Filter(OrderQueryFilter filter)
        {
            try
            {

                var data = (from c in _dataContext.Orders
                            join us in _dataContext.Users on c.UserId equals us.Id
                            join city in _dataContext.Cities on c.CityId equals city.matp
                            join dis in _dataContext.Districts on c.DistrictId equals dis.maqh
                            join com in _dataContext.Communes on c.CommuneId equals com.xaid

                            select new OrderBaseModel()
                            {
                                Id = c.Id,
                                Code = c.Code,
                                Commune = com.name,
                                City = city.name,
                                District = dis.name,
                                Name = c.Name,
                                Status = c.Status,
                                AddressDetail = c.AddressDetail,
                                CityId = c.CityId,
                                Discount = c.Discount,
                                Email = c.Email,
                                GrandTotal = c.GrandTotal,
                                ListProducts = c.ListProducts,
                                ItemDiscount = c.ItemDiscount,
                                MaGiamGia = c.MaGiamGia,
                                PhoneNumber = c.PhoneNumber,
                                PhuongThucThanhToan = c.PhuongThucThanhToan,
                                CommuneId = c.CommuneId,
                                DistrictId = c.DistrictId,
                                Shipping = c.Shipping,
                                SubTotal = c.SubTotal,
                                Total = c.Total,
                                ModifiedDate = DateTime.Now,
                                UserId = c.UserId,
                                CreatedDate = c.CreatedDate
                            });
                var listDefault = data.ToList();
                if (!string.IsNullOrEmpty(filter.TextSearch))
                {
                    string ts = filter.TextSearch.Trim().ToLower();
                    data = data.Where(x => x.Name.ToLower().Contains(ts) || x.Code.ToLower().Contains(ts));
                }

                if (filter.Status.HasValue)
                {
                    data = data.Where(x => x.Status == filter.Status);
                }

                if (filter.UserId.HasValue)
                {
                    data = data.Where(x => x.UserId == filter.UserId);
                }
                data = data.OrderByField(filter.PropertyName, filter.Ascending);

                int totalCount = data.Count();

                // Pagination
                if (filter.PageSize.HasValue && filter.PageNumber.HasValue)
                {
                    if (filter.PageSize <= 0)
                    {
                        filter.PageSize = QueryFilter.DefaultPageSize;
                    }

                    //Calculate nunber of rows to skip on pagesize
                    int excludedRows = (filter.PageNumber.Value - 1) * (filter.PageSize.Value);
                    if (excludedRows <= 0)
                    {
                        excludedRows = 0;
                    }

                    // Query
                    data = data.Skip(excludedRows).Take(filter.PageSize.Value);
                }
                int dataCount = data.Count();

                var listResult = await data.ToListAsync();
                return new ResponseObject<PaginationList<OrderBaseModel>>(new PaginationList<OrderBaseModel>()
                {
                    DataCount = dataCount,
                    TotalCount = totalCount,
                    PageNumber = filter.PageNumber ?? 0,
                    PageSize = filter.PageSize ?? 0,
                    Data = listResult
                }, MessageConstants.GetDataSuccessMessage, Code.Success);
            }
            catch (Exception ex)
            {
                Log.Error(ex, MessageConstants.ErrorLogMessage);
                return new ResponseError(Code.ServerError, $"{MessageConstants.GetDataErrorMessage} - {ex.Message}");
            }
        }

        public async Task<Response> GetById(Guid id, string ts)
        {
            try
            {
                var rs = (from od in _dataContext.Orders
                    join ct in _dataContext.Cities on od.CityId equals ct.matp
                    join dt in _dataContext.Districts on od.DistrictId equals dt.maqh
                    join cm in _dataContext.Communes on od.CommuneId equals cm.xaid
                    where od.UserId == id
                    select new OrderBaseModel()
                    {
                        Id = od.Id,
                        AddressDetail = od.AddressDetail,
                        City = ct.name,
                        CityId = od.CityId,
                        Code = od.Code,
                        Commune = cm.name,
                        CommuneId = od.CommuneId,
                        CreatedDate = od.CreatedDate,
                        CanceledDate = od.CanceledDate,
                        ConfirmedDate = od.ConfirmedDate,
                        DeliverDate = od.DeliverDate,
                        RecivedDate = od.RecivedDate,
                        Description = od.Description,
                        Discount = od.Discount,
                        Status = od.Status,
                        District = dt.name,
                        DistrictId = od.DistrictId,
                        SubTotal = od.SubTotal,
                        ItemDiscount = od.ItemDiscount,
                        Shipping = od.Shipping,
                        Total = od.Total,
                        MaGiamGia = od.MaGiamGia,
                        ListProducts = od.ListProducts,
                        UserId = od.UserId,
                        Name = od.Name,
                        PhoneNumber = od.PhoneNumber,
                        Email = od.Email,
                        PhuongThucThanhToan = od.PhuongThucThanhToan,
                        GrandTotal = od.GrandTotal,
                        ModifiedDate = od.ModifiedDate

                    }).OrderByDescending(x=>x.CreatedDate).ToList();
                 //var entity = await _dataContext.Orders
                 //   .Where(x => x.UserId.ToString().ToLower().Trim() == id.ToString().ToLower().Trim()).OrderByDescending(x=>x.CreatedDate).ToListAsync();
                if (!string.IsNullOrEmpty(ts))
                {
                    ts = ts.ToLower().Trim();
                    rs = rs.Where(x => x.Code.ToString().Trim().Contains(ts)).ToList();
                }
                return new ResponseObject<List<OrderBaseModel>>(rs, MessageConstants.GetDataSuccessMessage, Code.Success);
            }
            catch (Exception ex)
            {
                Log.Error(ex, MessageConstants.ErrorLogMessage);
                return new ResponseError(Code.ServerError, $"{MessageConstants.GetDataErrorMessage} - {ex.Message}");
            }
        }

        public async Task<Response> GetListCombobox(int count = 0, string textSearch = "")
        {
            try
            {
                string cacheKey = BuildCacheKey(SelectItemCacheSubfix);
                var list = await _cacheService.GetOrCreate(cacheKey, async () =>
                {
                    var data = (from item in _dataContext.Orders.OrderBy(x => x.CreatedDate).ThenBy(x => x.Name)
                                select new OrderBaseModel()
                                {
                                    Id = item.Id,
                                    Name = item.Name,
                                    UserId = item.UserId
                                });

                    return await data.ToListAsync();
                });

                if (!string.IsNullOrEmpty(textSearch))
                {
                    textSearch = textSearch.ToLower().Trim();
                    list = list.Where(x => x.Name.ToLower().Contains(textSearch) || x.Name.ToLower().Contains(textSearch)).ToList();
                }

                if (count > 0)
                {
                    list = list.Take(count).ToList();
                }

                return new ResponseObject<List<OrderBaseModel>>(list, MessageConstants.GetDataSuccessMessage, Code.Success);
            }
            catch (Exception ex)
            {
                Log.Error(ex, MessageConstants.ErrorLogMessage);
                return new ResponseError(Code.ServerError, $"{MessageConstants.GetDataErrorMessage} - {ex.Message}");
            }
        }

        private void InvalidCache(string id = "")
        {
            if (!string.IsNullOrEmpty(id))
            {
                string cacheKey = BuildCacheKey(id);
                _cacheService.Remove(cacheKey);
            }

            string selectItemCacheKey = BuildCacheKey(SelectItemCacheSubfix);
            _cacheService.Remove(selectItemCacheKey);
        }

        private string BuildCacheKey(string id)
        {
            return $"{CachePrefix}-{id}";
        }

        public async Task<Response> CheckInsurance(string serial)
        {
            try
            {
                var orders = _dataContext.Orders.ToList();
                var orderModel = new Order();
                var prodModel = new ProductBaseModel();
                var flag = false;
                if (orders.Count > 0)
                {
                    foreach (var order in orders)
                    {
                        var listProd = JsonConvert.DeserializeObject<List<ProductBaseModel>>(order.ListProducts);
                        var prod = listProd.Where(x => x.SerialNumber == serial).FirstOrDefault();
                        if (prod != null)
                        {
                            flag = true;
                            orderModel = order;
                            prodModel = prod;
                            break;
                        }
                    }
                }
                if (flag)
                {
                    var orderResult = new InsuranceFilterModel()
                    {
                        CustomerName = orderModel.Name,
                        OrderDate = orderModel.CreatedDate,
                        SerialNumber = serial,
                        ProductName = prodModel.Name,
                        InsuranceTime = prodModel.ThoiGianBaoHanh + " " + prodModel.LoaiBaoHanh
                    };
                    return new ResponseObject<InsuranceFilterModel>(orderResult, MessageConstants.GetDataSuccessMessage, Code.Success);
                }
                return new ResponseObject<InsuranceFilterModel>(null, MessageConstants.GetDataSuccessMessage, Code.Success);
            }
            catch (Exception ex)
            {
                Log.Error(ex, MessageConstants.ErrorLogMessage);
                return new ResponseError(Code.ServerError, $"{MessageConstants.GetDataErrorMessage} - {ex.Message}");
            }
        }
    }
}