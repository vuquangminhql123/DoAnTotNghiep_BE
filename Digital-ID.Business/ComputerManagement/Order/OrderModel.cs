﻿using DigitalID.Data;
using System;
using System.Collections.Generic;

namespace NetCore.Business
{
    public class OrderBaseModel
    {
        public Guid Id { get; set; }
        public string Code { get; set; }
        public int Status { get; set; }
        public double SubTotal { get; set; }
        public double? ItemDiscount { get; set; }
        public double Shipping { get; set; }
        public double Total { get; set; }
        public string MaGiamGia { get; set; }
        public string ListProducts { get; set; }
        //giam gia cua voucher
        public double? Discount { get; set; }

        public Guid? UserId { get; set; }

        //Tong gia phai tra
        public string Name { get; set; }
        public string Description { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string CityId { get; set; }
        public string City { get; set; }
        public string DistrictId{ get; set; }
        public string District { get; set; }
        public string CommuneId { get; set; }
        public DateTime ConfirmedDate { get; set; }
        public DateTime RecivedDate { get; set; }
        public DateTime CanceledDate { get; set; }
        //Đang giao hàng
        public DateTime DeliverDate { get; set; }
        public string Commune { get; set; }
        public int PhuongThucThanhToan { get; set; }
        public string AddressDetail { get; set; }
        public double GrandTotal { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public List<Voucher> Vouchers { get; set; }
    }

    public class OrderModel : OrderBaseModel
    {
        public int Order { get; set; } = 0;

    }

    public class OrderDetailModel : OrderModel
    {
        public Guid? CreatedUserId { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public Guid? ModifiedUserId { get; set; }
    }

    public class OrderCreateModel : OrderModel
    {
        public Guid? CreatedUserId { get; set; }
        public Guid? ApplicationId { get; set; }
    }
    public class InsuranceFilterModel
    {
        public string CustomerName { get; set; }
        public DateTime OrderDate { get; set; }
        public string SerialNumber { get; set; }
        public string ProductName { get; set; }
        public string InsuranceTime { get; set; }
    }
    public class OrderUpdateModel : OrderModel
    {
        public Guid ModifiedUserId { get; set; }

        public void UpdateToEntity(Order entity)
        {
            entity.Code = this.Code;
            entity.Name = this.Name;
            entity.Status = this.Status;
            entity.AddressDetail = AddressDetail;
            entity.City = City;
            entity.Discount = Discount;
            entity.Email = Email;
            entity.GrandTotal = GrandTotal;
            entity.ItemDiscount = ItemDiscount;
            entity.MaGiamGia = entity.MaGiamGia;
            entity.PhoneNumber = PhoneNumber;
            entity.PhuongThucThanhToan = PhuongThucThanhToan;
            entity.Commune = Commune;
            entity.District = District;
            entity.Shipping = Shipping;
            entity.SubTotal = SubTotal;
            entity.Total = Total;
            entity.ModifiedDate = DateTime.Now;
            entity.UserId = UserId;
        }
    }

    public class OrderQueryFilter
    {
        public string TextSearch { get; set; }
        public int? PageSize { get; set; }
        public Guid? UserId { get; set; }
        public int? PageNumber { get; set; }
        public int? Status { get; set; }
        public string PropertyName { get; set; } = "CreatedDate";
        //asc - desc
        public string Ascending { get; set; } = "desc";
        public OrderQueryFilter()
        {
            PageNumber = QueryFilter.DefaultPageNumber;
            PageSize = QueryFilter.DefaultPageSize;
        }
    }
}