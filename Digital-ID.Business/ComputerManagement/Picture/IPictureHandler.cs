﻿
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DigitalID.Data;

namespace NetCore.Business
{
    /// <summary>
    /// Interface quản lý Picture
    /// </summary>
    public interface IPictureHandler
    {
        /// <summary>
        /// Thêm mới Picture
        /// </summary>
        /// <param name="model">Model thêm mới Picture</param>
        /// <returns>Id Picture</returns>
        Task<Response> Create(PictureCreateModel model);

        /// <summary>
        /// Thêm mới Picture theo danh sách
        /// </summary>
        /// <param name="list">Danh sách thông tin Picture</param>
        /// <returns>Danh sách kết quả thêm mới</returns> 
        Task<Response> CreateMany(List<PictureCreateModel> list);

        /// <summary>
        /// Cập nhật Picture
        /// </summary>
        /// <param name="model">Model cập nhật Picture</param>
        /// <returns>Id Picture</returns>
        Task<Response> Update(PictureUpdateModel model);

        /// <summary>
        /// Xóa Picture
        /// </summary>
        /// <param name="listId">Danh sách Id Picture</param>
        /// <returns>Danh sách kết quả xóa</returns>
        Task<Response> Delete(List<Guid> listId);

        /// <summary>
        /// Lấy danh sách Picture theo điều kiện lọc
        /// </summary>
        /// <param name="filter">Model điều kiện lọc</param>
        /// <returns>Danh sách Picture</returns>
        Task<Response> Filter(PictureQueryFilter filter);

        /// <summary>
        /// Lấy Picture theo Id
        /// </summary>
        /// <param name="id">Id Picture</param>
        /// <returns>Thông tin Picture</returns>
        Task<Response> GetById(Guid id);


        /// <summary>
        /// Lấy danh sách Picture cho combobox
        /// </summary>
        /// <param name="count">Số bản ghi tối đa</param>
        /// <param name="textSearch">Từ khóa tìm kiếm</param>
        /// <returns>Danh sách Picture cho combobox</returns>
        Task<Response> GetListCombobox(int count = 0, string textSearch = "");
    }
}
