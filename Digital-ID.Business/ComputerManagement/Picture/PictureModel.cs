﻿using DigitalID.Data;
using System;
using System.Collections.Generic;

namespace NetCore.Business
{
    public class PictureBaseModel
    {
        public Guid Id { get; set; }
        public Guid ProductId { get; set; }
        public string PictureUrl { get; set; }
        public string ProductName { get; set; }
        public string Code { get; set; }
        public bool Status { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
    }

    public class PictureModel : PictureBaseModel
    {
        public int Order { get; set; } = 0;

    }

    public class PictureDetailModel : PictureModel
    {
        public Guid? CreatedUserId { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public Guid? ModifiedUserId { get; set; }
    }

    public class PictureCreateModel : PictureModel
    {
        public Guid? CreatedUserId { get; set; }
        public Guid? ApplicationId { get; set; }
    }

    public class PictureUpdateModel : PictureModel
    {
        public Guid ModifiedUserId { get; set; }

        public void UpdateToEntity(Picture entity)
        {
            entity.ProductId = ProductId;
            entity.PictureUrl = PictureUrl;
            entity.ModifiedDate = DateTime.Now;
        }
    }

    public class PictureQueryFilter
    {
        public string TextSearch { get; set; }
        public int? PageSize { get; set; }
        public int? PageNumber { get; set; }
        public bool? Status { get; set; }
        public string PropertyName { get; set; } = "CreatedDate";
        //asc - desc
        public string Ascending { get; set; } = "desc";
        public PictureQueryFilter()
        {
            PageNumber = QueryFilter.DefaultPageNumber;
            PageSize = QueryFilter.DefaultPageSize;
        }
    }
}