﻿
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DigitalID.Data;

namespace NetCore.Business
{
    /// <summary>
    /// Interface quản lý Product
    /// </summary>
    public interface IProductHandler
    {
        /// <summary>
        /// Thêm mới Product
        /// </summary>
        /// <param name="model">Model thêm mới Product</param>
        /// <returns>Id Product</returns>
        Task<Response> Create(ProductCreateModel model);

        /// <summary>
        /// Thêm mới Product theo danh sách
        /// </summary>
        /// <param name="list">Danh sách thông tin Product</param>
        /// <returns>Danh sách kết quả thêm mới</returns> 
        Task<Response> CreateMany(List<ProductCreateModel> list);

        /// <summary>
        /// Cập nhật Product
        /// </summary>
        /// <param name="model">Model cập nhật Product</param>
        /// <returns>Id Product</returns>
        Task<Response> Update(ProductUpdateModel model);



        /// <summary>
        /// Xóa Product
        /// </summary>
        /// <param name="listId">Danh sách Id Product</param>
        /// <returns>Danh sách kết quả xóa</returns>
        Task<Response> Delete(List<Guid> listId);

        /// <summary>
        /// Lấy danh sách Product theo điều kiện lọc
        /// </summary>
        /// <param name="filter">Model điều kiện lọc</param>
        /// <returns>Danh sách Product</returns>
        Task<Response> Filter(ProductQueryFilter filter);
        /// <summary>
        /// Lấy danh sách Product theo nhà cung cấp
        /// </summary>
        /// <param name="filter">Model điều kiện lọc</param>
        /// <returns>Danh sách Product</returns>
        Task<Response> GetListProductBySupplier(ProductQueryFilter filter);

        /// <summary>
        /// Lấy danh sách Product theo nhà cung cấp
        /// </summary>
        /// <param name="filter">Model điều kiện lọc</param>
        /// <returns>Danh sách Product</returns>
        Task<Response> GetListProductSimilar(string code);
        /// <summary>
        /// Cập nhật lượt truy cập
        /// </summary>
        /// <param name="filter">Model điều kiện lọc</param>
        /// <returns>Danh sách Product</returns>
        Task<Response> UpdateVisitCount(VisitCountModel model);
        /// <summary>
        /// Lấy Product theo Id
        /// </summary>
        /// <param name="id">Id Product</param>
        /// <returns>Thông tin Product</returns>
        Task<Response> GetById(Guid id);

        /// <summary>
        /// Lấy Product theo Code
        /// </summary>
        /// <param name="id">Code Product</param>
        /// <returns>Thông tin Product</returns>
        Task<Response> GetByCode(string code);

        /// <summary>
        /// Lấy danh sách Product cho combobox
        /// </summary>
        /// <param name="count">Số bản ghi tối đa</param>
        /// <param name="textSearch">Từ khóa tìm kiếm</param>
        /// <returns>Danh sách Product cho combobox</returns>
        Task<Response> GetListCombobox(int count = 0, string textSearch = "");
    }
}
