﻿using Microsoft.EntityFrameworkCore;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using DigitalID.Business;
using DigitalID.Data;

namespace NetCore.Business
{
    public class ProductHandler : IProductHandler
    {
        #region Message
        #endregion

        private const string CachePrefix = "Product";
        private const string SelectItemCacheSubfix = "list-select";
        private const string CodePrefix = "Product.";
        private const string CodePicturePrefix = "Pic.";
        private const string CodeTagPrefix = "PR_TAG.";
        private const string CodeCategoryPrefix = "PR_CATEGORY.";
        private const string CodeCategoryMetaPrefix = "PR_CATEMETA.";
        private readonly DataContext _dataContext;
        private readonly ICacheService _cacheService;

        public ProductHandler(DataContext dataContext, ICacheService cacheService)
        {
            _dataContext = dataContext;
            _cacheService = cacheService;
        }

        public async Task<Response> Create(ProductCreateModel model)
        {
            try
            {
                #region Check is exist document Type
                var isExistEmail = _dataContext.Products.Any(c => c.Code == model.Code);
                if (isExistEmail)
                    return new ResponseError(Code.ServerError, "Mã Product đã tồn tại");

                #endregion
                var isExistSerial = _dataContext.Products.Any(c => c.SerialNumber == model.SerialNumber);
                if (isExistSerial)
                    return new ResponseError(Code.ServerError, "Serial Number đã tồn tại");
                var entity = AutoMapperUtils.AutoMap<ProductCreateModel, Product>(model);
                entity.Pictures.Clear();
                //entity.CategoryProducts.Clear();
                //entity.ProductTags.Clear();
                //entity.CategoryMetaProducts.Clear();
                entity.CreatedDate = DateTime.Now;

                //long identityNumber = await _dataContext.DocumentType.DefaultIfEmpty().MaxAsync(x => x.IdentityNumber);

                //entity.IdentityNumber = ++identityNumber;
                //entity.Code = Utils.GenerateAutoCode(CodePrefix, identityNumber);

                entity.Id = Guid.NewGuid();
                entity.VisitCount = 0;
                await _dataContext.Products.AddAsync(entity);

                if (model.Pictures.Count > 0)
                {
                    foreach (var item in model.Pictures)
                    {
                        var pictureModel = new Picture();

                        long identityNumber = await _dataContext.Pictures.DefaultIfEmpty().MaxAsync(x => x.IdentityNumber);

                        pictureModel.IdentityNumber = ++identityNumber;
                        pictureModel.Code = Utils.GenerateAutoCode(CodePicturePrefix, identityNumber);
                        pictureModel.Id = Guid.NewGuid();
                        pictureModel.Status = true;
                        pictureModel.PictureUrl = item;
                        pictureModel.ProductId = entity.Id;
                        pictureModel.CreatedDate = DateTime.Now;
                        pictureModel.ModifiedDate = DateTime.Now;
                        await _dataContext.Pictures.AddAsync(pictureModel);
                    }
                }
                if (model.ListTag.Count > 0)
                {
                    foreach (var item in model.ListTag)
                    {
                        var tagModel = new Product_Tag();

                        long identityNumber = await _dataContext.ProductTags.DefaultIfEmpty().MaxAsync(x => x.IdentityNumber);

                        tagModel.IdentityNumber = ++identityNumber;
                        tagModel.Code = Utils.GenerateAutoCode(CodeTagPrefix, identityNumber);
                        tagModel.Id = Guid.NewGuid();
                        tagModel.Status = true;
                        tagModel.TagId = item;
                        tagModel.ProductId = entity.Id;
                        tagModel.CreatedDate = DateTime.Now;
                        tagModel.ModifiedDate = DateTime.Now;
                        await _dataContext.ProductTags.AddAsync(tagModel);
                    }
                }
                if (model.ListCategory.Count > 0)
                {
                    foreach (var item in model.ListCategory)
                    {
                        var categoryModel = new CategoryProduct();

                        long identityNumber = await _dataContext.CategoryProducts.DefaultIfEmpty().MaxAsync(x => x.IdentityNumber);

                        categoryModel.IdentityNumber = ++identityNumber;
                        categoryModel.Code = Utils.GenerateAutoCode(CodeCategoryPrefix, identityNumber);
                        categoryModel.Id = Guid.NewGuid();
                        categoryModel.Status = true;
                        categoryModel.CategoryId = item;
                        categoryModel.ProductId = entity.Id;
                        categoryModel.CreatedDate = DateTime.Now;
                        categoryModel.ModifiedDate = DateTime.Now;
                        await _dataContext.CategoryProducts.AddAsync(categoryModel);
                    }
                }
                if (model.ListCategoryMeta.Count > 0)
                {
                    foreach (var item in model.ListCategoryMeta)
                    {
                        var categoryModel = new Category_Meta_Product();

                        long identityNumber = await _dataContext.CategoryMetaProducts.DefaultIfEmpty().MaxAsync(x => x.IdentityNumber);

                        categoryModel.IdentityNumber = ++identityNumber;
                        categoryModel.Code = Utils.GenerateAutoCode(CodeCategoryMetaPrefix, identityNumber);
                        categoryModel.Id = Guid.NewGuid();
                        categoryModel.Status = true;
                        categoryModel.Content = item.Content;
                        categoryModel.CategoryMetaId = item.CategoryMetaId;
                        categoryModel.ProductId = entity.Id;
                        categoryModel.CreatedDate = DateTime.Now;
                        categoryModel.ModifiedDate = DateTime.Now;
                        await _dataContext.CategoryMetaProducts.AddAsync(categoryModel);
                    }
                }
                int dbSave = await _dataContext.SaveChangesAsync();

                if (dbSave > 0)
                {
                    //Log.Information("Add success: " + JsonSerializer.Serialize(entity));
                    InvalidCache();

                    return new ResponseObject<Guid>(entity.Id, MessageConstants.CreateSuccessMessage, Code.Success);
                }
                else
                {
                    return new ResponseError(Code.ServerError, MessageConstants.CreateErrorMessage);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, MessageConstants.ErrorLogMessage);
                return new ResponseError(Code.ServerError, $"{MessageConstants.CreateErrorMessage} - {ex.Message}");
            }
        }

        public async Task<Response> CreateMany(List<ProductCreateModel> list)
        {
            try
            {

                var listId = new List<Guid>();
                var listRS = new List<Product>();
                foreach (var item in list)
                {
                    var entity = AutoMapperUtils.AutoMap<ProductCreateModel, Product>(item);

                    entity.CreatedDate = DateTime.Now;
                    await _dataContext.Products.AddAsync(entity);
                    listId.Add(entity.Id);
                    listRS.Add(entity);
                }

                int dbSave = await _dataContext.SaveChangesAsync();

                if (dbSave > 0)
                {
                    Log.Information("Add success: " + JsonSerializer.Serialize(listRS));
                    InvalidCache();

                    return new ResponseObject<List<Guid>>(listId, MessageConstants.CreateSuccessMessage, Code.Success);
                }
                else
                {
                    return new ResponseError(Code.ServerError, MessageConstants.CreateErrorMessage);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, MessageConstants.ErrorLogMessage);
                return new ResponseError(Code.ServerError, $"{MessageConstants.CreateErrorMessage} - {ex.Message}");
            }
        }

        public async Task<Response> Update(ProductUpdateModel model)
        {
            try
            {
                #region Check is exist document Type
                var isExistEmail = _dataContext.Products.Any(c => c.Code == model.Code && c.Id != model.Id);
                if (isExistEmail)
                    return new ResponseError(Code.NotFound, "Mã Product đã tồn tại");

                #endregion
                var isExistSerial = _dataContext.Products.Any(c => c.SerialNumber == model.SerialNumber);
                if (isExistSerial)
                    return new ResponseError(Code.ServerError, "Serial Number đã tồn tại");
                var entity = await _dataContext.Products
                         .FirstOrDefaultAsync(x => x.Id == model.Id);
                //Log.Information("Before Update: " + JsonSerializer.Serialize(entity));

                model.UpdateToEntity(entity);
                var listPictureBefore = await _dataContext.Pictures.Where(x => x.ProductId == entity.Id).ToListAsync();
                _dataContext.Pictures.RemoveRange(listPictureBefore);
                var listTagBefore = await _dataContext.ProductTags.Where(x => x.ProductId == entity.Id).ToListAsync();
                _dataContext.ProductTags.RemoveRange(listTagBefore);
                var listCategoryBefore = await _dataContext.CategoryProducts.Where(x => x.ProductId == entity.Id).ToListAsync();
                _dataContext.CategoryProducts.RemoveRange(listCategoryBefore);
                var listCategoryMetaBefore = await _dataContext.CategoryMetaProducts.Where(x => x.ProductId == entity.Id).ToListAsync();
                _dataContext.CategoryMetaProducts.RemoveRange(listCategoryMetaBefore);
                int db = await _dataContext.SaveChangesAsync();
                _dataContext.Products.Update(entity);
                if (model.Pictures.Count > 0)
                {
                    foreach (var item in model.Pictures)
                    {
                        var pictureModel = new Picture();
                        long identityNumber = await _dataContext.Pictures.DefaultIfEmpty().MaxAsync(x => x.IdentityNumber);

                        pictureModel.IdentityNumber = ++identityNumber;
                        pictureModel.Code = Utils.GenerateAutoCode(CodePicturePrefix, identityNumber);
                        pictureModel.Id = Guid.NewGuid();
                        pictureModel.Status = true;
                        pictureModel.CreatedDate = DateTime.Now;
                        pictureModel.ModifiedDate = DateTime.Now;
                        pictureModel.PictureUrl = item;
                        pictureModel.ProductId = entity.Id;
                        await _dataContext.Pictures.AddAsync(pictureModel);
                    }
                }
                if (model.ListTag.Count > 0)
                {
                    foreach (var item in model.ListTag)
                    {
                        var tagModel = new Product_Tag();

                        long identityNumber = await _dataContext.ProductTags.DefaultIfEmpty().MaxAsync(x => x.IdentityNumber);

                        tagModel.IdentityNumber = ++identityNumber;
                        tagModel.Code = Utils.GenerateAutoCode(CodeTagPrefix, identityNumber);
                        tagModel.Id = Guid.NewGuid();
                        tagModel.Status = true;
                        tagModel.TagId = item;
                        tagModel.ProductId = entity.Id;
                        tagModel.CreatedDate = DateTime.Now;
                        tagModel.ModifiedDate = DateTime.Now;
                        await _dataContext.ProductTags.AddAsync(tagModel);
                    }
                }
                if (model.ListCategory.Count > 0)
                {
                    foreach (var item in model.ListCategory)
                    {
                        var categoryModel = new CategoryProduct();

                        long identityNumber = await _dataContext.CategoryProducts.DefaultIfEmpty().MaxAsync(x => x.IdentityNumber);

                        categoryModel.IdentityNumber = ++identityNumber;
                        categoryModel.Code = Utils.GenerateAutoCode(CodeCategoryPrefix, identityNumber);
                        categoryModel.Id = Guid.NewGuid();
                        categoryModel.Status = true;
                        categoryModel.CategoryId = item;
                        categoryModel.ProductId = entity.Id;
                        categoryModel.CreatedDate = DateTime.Now;
                        categoryModel.ModifiedDate = DateTime.Now;
                        await _dataContext.CategoryProducts.AddAsync(categoryModel);
                    }
                }
                if (model.ListCategoryMeta.Count > 0)
                {
                    foreach (var item in model.ListCategoryMeta)
                    {
                        var categoryModel = new Category_Meta_Product();

                        long identityNumber = await _dataContext.CategoryMetaProducts.DefaultIfEmpty().MaxAsync(x => x.IdentityNumber);

                        categoryModel.IdentityNumber = ++identityNumber;
                        categoryModel.Code = Utils.GenerateAutoCode(CodeCategoryMetaPrefix, identityNumber);
                        categoryModel.Id = Guid.NewGuid();
                        categoryModel.Status = true;
                        categoryModel.Content = item.Content;
                        categoryModel.CategoryMetaId = item.CategoryMetaId;
                        categoryModel.ProductId = entity.Id;
                        categoryModel.CreatedDate = DateTime.Now;
                        categoryModel.ModifiedDate = DateTime.Now;
                        await _dataContext.CategoryMetaProducts.AddAsync(categoryModel);
                    }
                }
                int dbSave = await _dataContext.SaveChangesAsync();
                if (dbSave > 0)
                {
                    //Log.Information("After Update: " + JsonSerializer.Serialize(entity));
                    InvalidCache(model.Id.ToString());

                    return new ResponseObject<Guid>(model.Id, MessageConstants.UpdateSuccessMessage, Code.Success);
                }
                else
                {
                    return new ResponseError(Code.ServerError, MessageConstants.UpdateErrorMessage);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, MessageConstants.ErrorLogMessage);
                return new ResponseError(Code.ServerError, $"{MessageConstants.GetDataErrorMessage} - {ex.Message}");
            }
        }

        public async Task<Response> Delete(List<Guid> listId)
        {
            try
            {
                var listResult = new List<ResponeDeleteModel>();
                var name = "";
                Log.Information("List Delete: " + JsonSerializer.Serialize(listId));
                foreach (var item in listId)
                {
                    name = "";
                    var entity = await _dataContext.Products.FindAsync(item);

                    if (entity == null)
                    {
                        listResult.Add(new ResponeDeleteModel()
                        {
                            Id = item,
                            Name = name,
                            Result = false,
                            Message = MessageConstants.DeleteItemNotFoundMessage
                        });
                    }
                    else
                    {
                        name = entity.Name;
                        _dataContext.Products.Remove(entity);
                        try
                        {
                            int dbSave = await _dataContext.SaveChangesAsync();
                            if (dbSave > 0)
                            {
                                InvalidCache(item.ToString());

                                listResult.Add(new ResponeDeleteModel()
                                {
                                    Id = item,
                                    Name = name,
                                    Result = true,
                                    Message = MessageConstants.DeleteItemSuccessMessage
                                });
                            }
                            else
                            {
                                listResult.Add(new ResponeDeleteModel()
                                {
                                    Id = item,
                                    Name = name,
                                    Result = false,
                                    Message = MessageConstants.DeleteItemErrorMessage
                                });
                            }
                        }
                        catch (Exception ex)
                        {
                            Log.Error(ex, MessageConstants.ErrorLogMessage);
                            listResult.Add(new ResponeDeleteModel()
                            {
                                Id = item,
                                Name = name,
                                Result = false,
                                Message = ex.Message
                            });
                        }
                    }
                }
                Log.Information("List Result Delete: " + JsonSerializer.Serialize(listResult));
                return new ResponseObject<List<ResponeDeleteModel>>(listResult, MessageConstants.DeleteSuccessMessage, Code.Success);

            }
            catch (Exception ex)
            {
                Log.Error(ex, MessageConstants.ErrorLogMessage);
                return new ResponseError(Code.ServerError, $"{MessageConstants.DeleteErrorMessage} - {ex.Message}");
            }
        }

        public async Task<Response> Filter(ProductQueryFilter filter)
        {
            try
            {
                var data = (from c in _dataContext.Products
                            join sp in _dataContext.Suppliers on c.SupplierId equals sp.Id

                            select new ProductBaseModel()
                            {
                                Id = c.Id,
                                Name = c.Name,
                                Status = c.Status,
                                Code = c.Code,
                                Pictures = new List<string>(),
                                TagName = new List<string>(),
                                CategoryName = new List<string>(),
                                ListCategory = new List<Guid>(),
                                ListTag = new List<Guid>(),
                                ListCategoryMetaProducts = new List<Category_Meta_Product>(),
                                ListCategoryMeta = new List<CategoryMetaProductModel>(),
                                Title = c.Title,
                                ModifiedDate = c.ModifiedDate,
                                MetaTitle = c.MetaTitle,
                                SerialNumber = c.SerialNumber,
                                Like = c.Like,
                                Dislike = c.Dislike,
                                Summary = c.Summary,
                                ThoiGianBaoHanh = c.ThoiGianBaoHanh,
                                Price = c.Price,
                                VisitCount = c.VisitCount,
                                LoaiBaoHanh = c.LoaiBaoHanh,
                                SupplierName = sp.Name,
                                Discount = c.Discount,
                                SoLuong = c.SoLuong,
                                SupplierId = c.SupplierId,
                                Description = c.Description,
                                CreatedDate = c.CreatedDate,
                            });

                double maxDefault = 0;
                if (data.ToList().Count > 0)
                {
                    maxDefault = data.ToList().Max(x => x.Price);
                }
                var listPicture = _dataContext.Pictures.ToList();
                var listTag = _dataContext.ProductTags.ToList();
                var listTags = _dataContext.Tags.ToList();
                var listCategoryProduct = _dataContext.CategoryProducts.ToList();
                var listCategoryMeta = _dataContext.CategoryMetaProducts.ToList();
                var listResult = await data.ToListAsync();
                for (int i = 0; i < listResult.Count; i++)
                {
                    listResult[i].Pictures = listPicture.Where(x => x.ProductId == listResult[i].Id).OrderByDescending(x=>x.CreatedDate).Select(x => x.PictureUrl).ToList();
                    listResult[i].Rating = _dataContext.ProductReviews.Where(x => x.ProductId == listResult[i].Id && x.IsRating == true).Select(x=>x.Rating).DefaultIfEmpty(0).Average();
                    listResult[i].Amount = listResult[i].Price - listResult[i].Discount;
                    listResult[i].ListCategory = listCategoryProduct.Where(x => x.ProductId == listResult[i].Id).Select(x => x.CategoryId).ToList();
                    listResult[i].ListTag = listTag.Where(x => x.ProductId == listResult[i].Id).Select(x => x.TagId).ToList();
                    var rs = from lt in listResult[i].ListTag
                        join lts in listTags on lt equals lts.Id
                        select lts.Name;
                    listResult[i].TagName = rs.ToList();

                    listResult[i].ListCategoryMetaProducts = listCategoryMeta.Where(x => x.ProductId == listResult[i].Id).ToList();
                    listResult[i].CategoryName = (from c in _dataContext.Category
                        join cp in _dataContext.CategoryProducts on c.Id equals cp.CategoryId
                        where cp.ProductId == listResult[i].Id
                        select c.Name).ToList();
                    listResult[i].CategoryString = String.Join(',',listResult[i].CategoryName);
                    listResult[i].TagString = String.Join(',',listResult[i].TagName);
                    listResult[i].CategoryMetaProdString = String.Join(',', listResult[i].ListCategoryMetaProducts.Select(x=>x.Content));
                }
                //Filter
                var listFilter = listResult.Where(x => x.Code != null);
                if (!string.IsNullOrEmpty(filter.TextSearch))
                {
                    string ts = filter.TextSearch.Trim().ToLower();
                    listFilter = listResult.Where(x => x.Name.ToLower().Contains(ts) || x.Code.ToLower().Contains(ts) || x.SupplierName.ToLower().Contains(ts) || x.Title.ToLower().Contains(ts) || x.MetaTitle.ToLower().Contains(ts) || x.LoaiBaoHanh.ToLower().Contains(ts) || x.Summary.ToLower().Contains(ts) || x.CategoryString.ToLower().Trim().Contains(ts) || x.TagString.ToLower().Trim().Contains(ts) || x.CategoryMetaProdString.ToLower().Trim().Contains(ts));
                }
                if (filter.Status.HasValue)
                {
                    listFilter = listFilter.Where(x => x.Status == filter.Status);
                }

                if (filter.SupplierId.HasValue)
                {
                    listFilter = listFilter.Where(x => x.SupplierId == filter.SupplierId);
                }
                if (filter.CategoryId.HasValue)
                {
                    listFilter = listFilter.Where(x => x.ListCategory.Where(x => x == filter.CategoryId).ToList().Count() != 0);
                }
                listFilter = listFilter.OrderByDescending(x=>x.CreatedDate);

                if (filter.SortType.HasValue)
                {
                    switch (filter.SortType)
                    {
                        case 0:
                            listFilter = listFilter.OrderByDescending(x => x.CreatedDate);
                            break;
                        case 1:
                            listFilter = listFilter.OrderBy(x => x.Amount);
                            break;
                        case 2:
                            listFilter = listFilter.OrderByDescending(x => x.Amount);
                            break;
                        case 3:
                            listFilter = listFilter.OrderByDescending(x => x.VisitCount);
                            break;
                        case 4:
                            listFilter = listFilter.OrderByDescending(x => x.Rating);
                            break;
                        case 5:
                            listFilter = listFilter.OrderBy(x => x.Name);
                            break;
                        default:
                            listFilter = listFilter.OrderByDescending(x => x.CreatedDate);
                            break;
                    }
                }


                if (filter.MinPrice.HasValue)
                {
                    listFilter = listFilter.Where(x => x.Amount >= filter.MinPrice);
                }
                if (filter.MaxPrice.HasValue)
                {
                    listFilter = listFilter.Where(x => x.Amount <= filter.MaxPrice);
                }
                int totalCount = listFilter.Count();

                // Pagination
                if (filter.PageSize.HasValue && filter.PageNumber.HasValue)
                {
                    if (filter.PageSize <= 0)
                    {
                        filter.PageSize = QueryFilter.DefaultPageSize;
                    }

                    //Calculate nunber of rows to skip on pagesize
                    int excludedRows = (filter.PageNumber.Value - 1) * (filter.PageSize.Value);
                    if (excludedRows <= 0)
                    {
                        excludedRows = 0;
                    }

                    // Query
                    listFilter = listFilter.Skip(excludedRows).Take(filter.PageSize.Value);
                }
                int dataCount = listFilter.Count();
                return new ResponseObject<PaginationList<ProductBaseModel>>(new PaginationList<ProductBaseModel>()
                {
                    MaxDefault = maxDefault,
                    DataCount = dataCount,
                    TotalCount = totalCount,
                    PageNumber = filter.PageNumber ?? 0,
                    PageSize = filter.PageSize ?? 0,
                    Data = listFilter.ToList()
                }, MessageConstants.GetDataSuccessMessage, Code.Success);
            }
            catch (Exception ex)
            {
                Log.Error(ex, MessageConstants.ErrorLogMessage);
                return new ResponseError(Code.ServerError, $"{MessageConstants.GetDataErrorMessage} - {ex.Message}");
            }
        }

        public async Task<Response> GetListProductSimilar(string code)
        {
            try
            {
                var data = (from c in _dataContext.Products
                            join sp in _dataContext.Suppliers on c.SupplierId equals sp.Id
                            select new ProductBaseModel()
                            {
                                Id = c.Id,
                                Name = c.Name,
                                Status = c.Status,
                                Code = c.Code,
                                Pictures = new List<string>(),
                                TagName = new List<string>(),
                                CategoryName = new List<string>(),
                                ListCategory = new List<Guid>(),
                                ListTag = new List<Guid>(),
                                ListCategoryMetaProducts = new List<Category_Meta_Product>(),
                                ListCategoryMeta = new List<CategoryMetaProductModel>(),
                                Title = c.Title,
                                ModifiedDate = c.ModifiedDate,
                                MetaTitle = c.MetaTitle,
                                SerialNumber = c.SerialNumber,
                                Like = c.Like,
                                Dislike = c.Dislike,
                                Summary = c.Summary,
                                ThoiGianBaoHanh = c.ThoiGianBaoHanh,
                                Price = c.Price,
                                VisitCount = c.VisitCount,
                                LoaiBaoHanh = c.LoaiBaoHanh,
                                SupplierName = sp.Name,
                                Discount = c.Discount,
                                SoLuong = c.SoLuong,
                                SupplierId = c.SupplierId,
                                Description = c.Description,
                                CreatedDate = c.CreatedDate,
                            });
                double maxDefault = 0;
                if (data.ToList().Count > 0)
                {
                    maxDefault = data.ToList().Max(x => x.Price);
                }
                var listPicture = _dataContext.Pictures.ToList();
                var listTag = _dataContext.ProductTags.ToList();
                var listTags = _dataContext.Tags.ToList();
                var listCategoryProduct = _dataContext.CategoryProducts.ToList();
                var listCategoryMeta = _dataContext.CategoryMetaProducts.ToList();
                var listResult = await data.ToListAsync();
                for (int i = 0; i < listResult.Count; i++)
                {
                    listResult[i].Pictures = listPicture.Where(x => x.ProductId == listResult[i].Id).OrderByDescending(x => x.CreatedDate).Select(x => x.PictureUrl).ToList();
                    listResult[i].Rating = _dataContext.ProductReviews.Where(x => x.ProductId == listResult[i].Id && x.IsRating == true).Select(x => x.Rating).DefaultIfEmpty(0).Average();
                    listResult[i].Amount = listResult[i].Price - listResult[i].Discount;
                    listResult[i].ListCategory = listCategoryProduct.Where(x => x.ProductId == listResult[i].Id).Select(x => x.CategoryId).ToList();
                    listResult[i].ListTag = listTag.Where(x => x.ProductId == listResult[i].Id).Select(x => x.TagId).ToList();
                    var rs = from lt in listResult[i].ListTag
                             join lts in listTags on lt equals lts.Id
                             select lts.Name;
                    listResult[i].TagName = rs.ToList();

                    listResult[i].ListCategoryMetaProducts = listCategoryMeta.Where(x => x.ProductId == listResult[i].Id).ToList();
                    listResult[i].CategoryName = (from c in _dataContext.Category
                                                  join cp in _dataContext.CategoryProducts on c.Id equals cp.CategoryId
                                                  where cp.ProductId == listResult[i].Id
                                                  select c.Name).ToList();
                    listResult[i].CategoryString = String.Join(',', listResult[i].CategoryName);
                    listResult[i].TagString = String.Join(',', listResult[i].TagName);
                    listResult[i].CategoryMetaProdString = String.Join(',', listResult[i].ListCategoryMetaProducts.Select(x => x.Content));
                }
                return new ResponseObject<List<ProductBaseModel>>(listResult, MessageConstants.GetDataSuccessMessage, Code.Success);
            }
            catch (Exception ex)
            {
                Log.Error(ex, MessageConstants.ErrorLogMessage);
                return new ResponseError(Code.ServerError, $"{MessageConstants.GetDataErrorMessage} - {ex.Message}");
            }
        }

        public async Task<Response> UpdateVisitCount(VisitCountModel model)
        {
            try
            {
                var prodModel = await _dataContext.Products.Where(x => x.Code.Trim().ToLower() == model.ProdCode.Trim().ToLower()).FirstOrDefaultAsync();
                if (prodModel != null)
                {
                    if (prodModel.VisitCount == null || prodModel.VisitCount < 0)
                    {
                        prodModel.VisitCount = 0;
                    }
                    prodModel.VisitCount++;
                    prodModel.ModifiedDate = DateTime.Now;
                    _dataContext.Products.Update(prodModel);
                }
                int dbSave = await _dataContext.SaveChangesAsync();

                if (dbSave > 0)
                {
                    Log.Information("Update success:");
                    InvalidCache();

                    return new ResponseObject<int>(prodModel.VisitCount.Value, MessageConstants.CreateSuccessMessage, Code.Success);
                }
                else
                {
                    return new ResponseError(Code.ServerError, MessageConstants.CreateErrorMessage);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, MessageConstants.ErrorLogMessage);
                return new ResponseError(Code.ServerError, $"{MessageConstants.CreateErrorMessage} - {ex.Message}");
            }
        }

        public async Task<Response> GetById(Guid id)
        {
            try
            {
                string cacheKey = BuildCacheKey(id.ToString());
                var rs = await _cacheService.GetOrCreate(cacheKey, async () =>
                {
                    var entity = await _dataContext.Products
                        .FirstOrDefaultAsync(x => x.Id == id);
                    var listPicture = _dataContext.Pictures.Where(x => x.ProductId == entity.Id).OrderByDescending(x=>x.CreatedDate).ToList();
                    var entityMapping = AutoMapperUtils.AutoMap<Product, ProductModel>(entity);
                    foreach (var picture in listPicture)
                    {
                        entityMapping.Pictures.Add(picture.PictureUrl);
                    }
                    return entityMapping;
                });
                return new ResponseObject<ProductModel>(rs, MessageConstants.GetDataSuccessMessage, Code.Success);
            }
            catch (Exception ex)
            {
                Log.Error(ex, MessageConstants.ErrorLogMessage);
                return new ResponseError(Code.ServerError, $"{MessageConstants.GetDataErrorMessage} - {ex.Message}");
            }
        }

        public async Task<Response> GetByCode(string code)
        {
            try
            {
                var vsModel = new VisitCountModel();
                vsModel.ProdCode = code;
               var udRs = await UpdateVisitCount(vsModel);
                var entity = await _dataContext.Products
                    .FirstOrDefaultAsync(x => x.Code == code);
                var supplier = _dataContext.Suppliers.Where(x=> x.Id == entity.SupplierId).ToList().FirstOrDefault();
                var rs = new ProductBaseModel()
                {
                    Id = entity.Id,
                    Name = entity.Name,
                    Status = entity.Status,
                    Code = entity.Code,
                    Pictures = new List<string>(),
                    TagName = new List<string>(),
                    CategoryName = new List<string>(),
                    ListCategory = new List<Guid>(),
                    ListTag = new List<Guid>(),
                    ListCategoryMetaProducts = new List<Category_Meta_Product>(),
                    ListCategoryMeta = new List<CategoryMetaProductModel>(),
                    Title = entity.Title,
                    MetaTitle = entity.MetaTitle,
                    Like = entity.Like,
                    Dislike = entity.Dislike,
                    SerialNumber = entity.SerialNumber,
                    Summary = entity.Summary,
                    ThoiGianBaoHanh = entity.ThoiGianBaoHanh,
                    Price = entity.Price,
                    VisitCount = entity.VisitCount,
                    LoaiBaoHanh = entity.LoaiBaoHanh,
                    SupplierName = supplier != null ? supplier.Name:"",
                    Discount = entity.Discount,
                    SoLuong = entity.SoLuong,
                    SupplierId = entity.SupplierId,
                    Description = entity.Description
                };
                rs.Rating = _dataContext.ProductReviews.Where(x => x.ProductId == rs.Id && x.IsRating == true).Select(x => x.Rating).DefaultIfEmpty(0).Average();
                var listPicture = _dataContext.Pictures.Where(x => x.ProductId == entity.Id).OrderByDescending(x=>x.CreatedDate).Select(x=>x.PictureUrl).ToList();
                rs.Pictures = listPicture;
                var listTag = _dataContext.ProductTags.ToList();
                var listTags = _dataContext.Tags.ToList();
                var listCategoryProduct = _dataContext.CategoryProducts.ToList();
                var listCategoryMeta = _dataContext.CategoryMetaProducts.ToList();
                rs.ListCategory = listCategoryProduct.Where(x => x.ProductId == rs.Id).Select(x => x.CategoryId).ToList();
                rs.ListTag = listTag.Where(x => x.ProductId == rs.Id).Select(x => x.TagId).ToList();
                var rs_S = from lt in rs.ListTag
                    join lts in listTags on lt equals lts.Id
                    select lts.Name;
                rs.TagName = rs_S.ToList();
                rs.ListCategoryMetaProducts = listCategoryMeta.Where(x => x.ProductId == rs.Id).ToList();
                foreach (var item in rs.ListCategoryMetaProducts)
                {
                    item.Product = null;
                }
                rs.CategoryName = (from c in _dataContext.Category
                    join cp in _dataContext.CategoryProducts on c.Id equals cp.CategoryId
                    where cp.ProductId == rs.Id
                    select c.Name).ToList();

                return new ResponseObject<ProductBaseModel>(rs, MessageConstants.GetDataSuccessMessage, Code.Success);
            }
            catch (Exception ex)
            {
                Log.Error(ex, MessageConstants.ErrorLogMessage);
                return new ResponseError(Code.ServerError, $"{MessageConstants.GetDataErrorMessage} - {ex.Message}");
            }
        }

        public async Task<Response> GetListCombobox(int count = 0, string textSearch = "")
        {
            try
            {
                string cacheKey = BuildCacheKey(SelectItemCacheSubfix);
                var list = await _cacheService.GetOrCreate(cacheKey, async () =>
                {
                    var data = (from item in _dataContext.Products.Where(x => x.Status == true).OrderBy(x => x.CreatedDate).ThenBy(x => x.Name)
                                select new SelectItemModel()
                                {
                                    Id = item.Id,
                                    Name = item.Name,
                                    Note = item.Code
                                });

                    return await data.ToListAsync();
                });

                if (!string.IsNullOrEmpty(textSearch))
                {
                    textSearch = textSearch.ToLower().Trim();
                    list = list.Where(x => x.Name.ToLower().Contains(textSearch) || x.Note.ToLower().Contains(textSearch)).ToList();
                }

                if (count > 0)
                {
                    list = list.Take(count).ToList();
                }

                return new ResponseObject<List<SelectItemModel>>(list, MessageConstants.GetDataSuccessMessage, Code.Success);
            }
            catch (Exception ex)
            {
                Log.Error(ex, MessageConstants.ErrorLogMessage);
                return new ResponseError(Code.ServerError, $"{MessageConstants.GetDataErrorMessage} - {ex.Message}");
            }
        }

        private void InvalidCache(string id = "")
        {
            if (!string.IsNullOrEmpty(id))
            {
                string cacheKey = BuildCacheKey(id);
                _cacheService.Remove(cacheKey);
            }

            string selectItemCacheKey = BuildCacheKey(SelectItemCacheSubfix);
            _cacheService.Remove(selectItemCacheKey);
        }

        private string BuildCacheKey(string id)
        {
            return $"{CachePrefix}-{id}";
        }

        public async Task<Response> GetListProductBySupplier(ProductQueryFilter filter)
        {
            try
            {
                var data = (from c in _dataContext.Products
                            join sp in _dataContext.Suppliers on c.SupplierId equals sp.Id
                            select new ProductBaseModel()
                            {
                                Id = c.Id,
                                Name = c.Name,
                                Status = c.Status,
                                Code = c.Code,
                                Pictures = new List<string>(),
                                TagName = new List<string>(),
                                CategoryName = new List<string>(),
                                ListCategory = new List<Guid>(),
                                ListTag = new List<Guid>(),
                                ListCategoryMetaProducts = new List<Category_Meta_Product>(),
                                ListCategoryMeta = new List<CategoryMetaProductModel>(),
                                Title = c.Title,
                                ModifiedDate = c.ModifiedDate,
                                SerialNumber = c.SerialNumber,
                                MetaTitle = c.MetaTitle,
                                Like = c.Like,
                                Dislike = c.Dislike,
                                Summary = c.Summary,
                                ThoiGianBaoHanh = c.ThoiGianBaoHanh,
                                Price = c.Price,
                                VisitCount = c.VisitCount,
                                LoaiBaoHanh = c.LoaiBaoHanh,
                                SupplierName = sp.Name,
                                Discount = c.Discount,
                                SoLuong = c.SoLuong,
                                SupplierId = c.SupplierId,
                                Description = c.Description,
                                CreatedDate = c.CreatedDate,
                            });
                double maxDefault = 0;
                if (data.ToList().Count > 0)
                {
                    maxDefault = data.ToList().Max(x => x.Price);
                }
                var listPicture = _dataContext.Pictures.ToList();
                var listTag = _dataContext.ProductTags.ToList();
                var listTags = _dataContext.Tags.ToList();
                var listCategoryProduct = _dataContext.CategoryProducts.ToList();
                var listCategoryMeta = _dataContext.CategoryMetaProducts.ToList();
                var listResult = await data.ToListAsync();
                for (int i = 0; i < listResult.Count; i++)
                {
                    listResult[i].Pictures = listPicture.Where(x => x.ProductId == listResult[i].Id).OrderByDescending(x => x.CreatedDate).Select(x => x.PictureUrl).ToList();
                    listResult[i].Rating = _dataContext.ProductReviews.Where(x => x.ProductId == listResult[i].Id && x.IsRating == true).Select(x => x.Rating).DefaultIfEmpty(0).Average();
                    listResult[i].Amount = listResult[i].Price - listResult[i].Discount;
                    listResult[i].ListCategory = listCategoryProduct.Where(x => x.ProductId == listResult[i].Id).Select(x => x.CategoryId).ToList();
                    listResult[i].ListTag = listTag.Where(x => x.ProductId == listResult[i].Id).Select(x => x.TagId).ToList();
                    var rs = from lt in listResult[i].ListTag
                             join lts in listTags on lt equals lts.Id
                             select lts.Name;
                    listResult[i].TagName = rs.ToList();

                    listResult[i].ListCategoryMetaProducts = listCategoryMeta.Where(x => x.ProductId == listResult[i].Id).ToList();
                    listResult[i].CategoryName = (from c in _dataContext.Category
                                                  join cp in _dataContext.CategoryProducts on c.Id equals cp.CategoryId
                                                  where cp.ProductId == listResult[i].Id
                                                  select c.Name).ToList();
                    listResult[i].CategoryString = String.Join(',', listResult[i].CategoryName);
                    listResult[i].TagString = String.Join(',', listResult[i].TagName);
                    listResult[i].CategoryMetaProdString = String.Join(',', listResult[i].ListCategoryMetaProducts.Select(x => x.Content));
                }
                //Filter
                var listFilter = listResult.Where(x => x.Code != null);
                if (!string.IsNullOrEmpty(filter.TextSearch))
                {
                    string ts = filter.TextSearch.Trim().ToLower();
                    listFilter = listResult.Where(x => x.Name.ToLower().Contains(ts) || x.Code.ToLower().Contains(ts) || x.SupplierName.ToLower().Contains(ts) || x.Title.ToLower().Contains(ts) || x.MetaTitle.ToLower().Contains(ts) || x.LoaiBaoHanh.ToLower().Contains(ts) || x.Summary.ToLower().Contains(ts) || x.CategoryString.ToLower().Trim().Contains(ts) || x.TagString.ToLower().Trim().Contains(ts) || x.CategoryMetaProdString.ToLower().Trim().Contains(ts));
                }
                if (filter.Status.HasValue)
                {
                    listFilter = listFilter.Where(x => x.Status == filter.Status);
                }

                if (filter.SupplierId.HasValue)
                {
                    listFilter = listFilter.Where(x => x.SupplierId == filter.SupplierId);
                }
                if (filter.CategoryId.HasValue)
                {
                    listFilter = listFilter.Where(x => x.ListCategory.Where(o=> o == filter.CategoryId).ToList().Count() != 0 );
                }
                listFilter = listFilter.OrderByDescending(x => x.CreatedDate);

                if (filter.SortType.HasValue)
                {
                    switch (filter.SortType)
                    {
                        case 0:
                            listFilter = listFilter.OrderByDescending(x => x.CreatedDate);
                            break;
                        case 1:
                            listFilter = listFilter.OrderBy(x => x.Amount);
                            break;
                        case 2:
                            listFilter = listFilter.OrderByDescending(x => x.Amount);
                            break;
                        case 3:
                            listFilter = listFilter.OrderByDescending(x => x.VisitCount);
                            break;
                        case 4:
                            listFilter = listFilter.OrderByDescending(x => x.Rating);
                            break;
                        case 5:
                            listFilter = listFilter.OrderBy(x => x.Name);
                            break;
                        default:
                            listFilter = listFilter.OrderByDescending(x => x.CreatedDate);
                            break;
                    }
                }


                if (filter.MinPrice.HasValue)
                {
                    listFilter = listFilter.Where(x => x.Amount >= filter.MinPrice);
                }
                if (filter.MaxPrice.HasValue)
                {
                    listFilter = listFilter.Where(x => x.Amount <= filter.MaxPrice);
                }
                int totalCount = listFilter.Count();

                // Pagination
                if (filter.PageSize.HasValue && filter.PageNumber.HasValue)
                {
                    if (filter.PageSize <= 0)
                    {
                        filter.PageSize = QueryFilter.DefaultPageSize;
                    }

                    //Calculate nunber of rows to skip on pagesize
                    int excludedRows = (filter.PageNumber.Value - 1) * (filter.PageSize.Value);
                    if (excludedRows <= 0)
                    {
                        excludedRows = 0;
                    }

                    // Query
                    listFilter = listFilter.Skip(excludedRows).Take(filter.PageSize.Value);
                }
                int dataCount = listFilter.Count();
                return new ResponseObject<PaginationList<ProductBaseModel>>(new PaginationList<ProductBaseModel>()
                {
                    MaxDefault = maxDefault,
                    DataCount = dataCount,
                    TotalCount = totalCount,
                    PageNumber = filter.PageNumber ?? 0,
                    PageSize = filter.PageSize ?? 0,
                    Data = listFilter.ToList()
                }, MessageConstants.GetDataSuccessMessage, Code.Success);
            }
            catch (Exception ex)
            {
                Log.Error(ex, MessageConstants.ErrorLogMessage);
                return new ResponseError(Code.ServerError, $"{MessageConstants.GetDataErrorMessage} - {ex.Message}");
            }
        }
    }
}