﻿using DigitalID.Data;
using System;
using System.Collections.Generic;
using DigitalID.Business;

namespace NetCore.Business
{
    public class ProductBaseModel
    {
        public Guid Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string LoaiBaoHanh { get; set; }
        public string Title { get; set; }
        public string SerialNumber { get; set; }
        public string MetaTitle { get; set; }
        public int ReviewCount { get; set; }
        public int? Like { get; set; }
        public int Count { get; set; }
        public int? Dislike { get; set; }
        public string Summary { get; set; }
        public int ThoiGianBaoHanh { get; set; }
        public bool? Status { get; set; }
        public double Price { get; set; }
        public double Rating { get; set; }
        public double Amount { get; set; }
        public double Discount { get; set; }
        public int? VisitCount { get; set; }
        public int SoLuong { get; set; }
        public Guid SupplierId { get; set; }
        public string SupplierName { get; set; }
        public string Description { get; set; }
        public List<CategoryMetaProductModel> ListCategoryMeta { get; set; }
        public List<string> Pictures { get; set; }
        public List<string> CategoryName { get; set; }
        public string CategoryString { get; set; }
        public List<string> TagName { get; set; }
        public string TagString { get; set; }
        public List<Category_Meta_Product> ListCategoryMetaProducts { get; set; }
        public string CategoryMetaProdString { get; set; }
        public List<Guid> ListCategory { get; set; }
        public List<Guid> ListTag { get; set; }
    }

    public class ProductModel : ProductBaseModel
    {
        public int Order { get; set; } = 0;

    }
    public class GetByCode
    {
        public string Code { get; set; }
    }
    public class ProductDetailModel : ProductModel
    {
        public Guid? CreatedUserId { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public Guid? ModifiedUserId { get; set; }
    }

    public class ProductCreateModel : ProductModel
    {
        public Guid? CreatedUserId { get; set; }
        public Guid? ApplicationId { get; set; }
    }

    public class ProductUpdateModel : ProductModel
    {
        public Guid ModifiedUserId { get; set; }

        public void UpdateToEntity(Product entity)
        {
            entity.Name = this.Name;
            entity.Status = this.Status;
            entity.ModifiedDate = DateTime.Now;
            entity.Title = Title;
            entity.MetaTitle = MetaTitle;
            entity.Like = Like;
            entity.SerialNumber = SerialNumber;
            entity.Dislike = Dislike;
            entity.Summary = Summary;
            entity.ThoiGianBaoHanh = ThoiGianBaoHanh;
            entity.Status = Status;
            entity.Price = Price;
            entity.Discount = Discount;
            entity.SoLuong = SoLuong;
            entity.SupplierId = SupplierId;
            entity.Description = Description;
        }
    }
    public class ProductModelFilter
    {
        public string TextSearch { get; set; }
        public string SortText { get; set; } = "default";
        public int? PageSize { get; set; }
        public int? PageNumber { get; set; }
    }

    public class VisitCountModel
    {
        public string ProdCode { get; set; }
    }
    public class ProductQueryFilter
    {
        public string TextSearch { get; set; }
        public Guid? SupplierId { get; set; }
        public Guid? CategoryId { get; set; }
        public int? PageSize { get; set; }
        public string CategoryCode { get; set; }
        public string ProductCode { get; set; }
        public double? MaxPrice { get; set; }
        public double? MinPrice { get; set; }
        public int? PageNumber { get; set; }
        public int? SortType { get; set; }
        public bool? Status { get; set; }
        public string PropertyName { get; set; } = "CreatedDate";
        //asc - desc
        public string Ascending { get; set; } = "desc";
        public ProductQueryFilter()
        {
            PageNumber = QueryFilter.DefaultPageNumber;
            PageSize = QueryFilter.DefaultPageSize;
        }
    }
}