﻿
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DigitalID.Data;

namespace NetCore.Business
{
    /// <summary>
    /// Interface quản lý Supplier
    /// </summary>
    public interface ISupplierHandler
    {
        /// <summary>
        /// Thêm mới Supplier
        /// </summary>
        /// <param name="model">Model thêm mới Supplier</param>
        /// <returns>Id Supplier</returns>
        Task<Response> Create(SupplierCreateModel model);

        /// <summary>
        /// Thêm mới Supplier theo danh sách
        /// </summary>
        /// <param name="list">Danh sách thông tin Supplier</param>
        /// <returns>Danh sách kết quả thêm mới</returns> 
        Task<Response> CreateMany(List<SupplierCreateModel> list);

        /// <summary>
        /// Cập nhật Supplier
        /// </summary>
        /// <param name="model">Model cập nhật Supplier</param>
        /// <returns>Id Supplier</returns>
        Task<Response> Update(SupplierUpdateModel model);

        /// <summary>
        /// Xóa Supplier
        /// </summary>
        /// <param name="listId">Danh sách Id Supplier</param>
        /// <returns>Danh sách kết quả xóa</returns>
        Task<Response> Delete(List<Guid> listId);

        /// <summary>
        /// Lấy danh sách Supplier theo điều kiện lọc
        /// </summary>
        /// <param name="filter">Model điều kiện lọc</param>
        /// <returns>Danh sách Supplier</returns>
        Task<Response> Filter(SupplierQueryFilter filter);

        /// <summary>
        /// Lấy Supplier theo Id
        /// </summary>
        /// <param name="id">Id Supplier</param>
        /// <returns>Thông tin Supplier</returns>
        Task<Response> GetById(Guid id);


        /// <summary>
        /// Lấy danh sách Supplier cho combobox
        /// </summary>
        /// <param name="count">Số bản ghi tối đa</param>
        /// <param name="textSearch">Từ khóa tìm kiếm</param>
        /// <returns>Danh sách Supplier cho combobox</returns>
        Task<Response> GetListCombobox(int count = 0, string textSearch = "");
    }
}
