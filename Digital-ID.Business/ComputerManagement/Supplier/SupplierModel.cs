﻿using DigitalID.Data;
using System;
using System.Collections.Generic;

namespace NetCore.Business
{
    public class SupplierBaseModel
    {
        public Guid Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string AvatarUrl { get; set; }
        public bool? Status { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
    }

    public class SupplierModel : SupplierBaseModel
    {
        public int Order { get; set; } = 0;

    }

    public class SupplierDetailModel : SupplierModel
    {
        public Guid? CreatedUserId { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public Guid? ModifiedUserId { get; set; }
    }

    public class SupplierCreateModel : SupplierModel
    {
        public Guid? CreatedUserId { get; set; }
        public Guid? ApplicationId { get; set; }
    }

    public class SupplierUpdateModel : SupplierModel
    {
        public Guid ModifiedUserId { get; set; }

        public void UpdateToEntity(Supplier entity)
        {
            entity.Code = this.Code;
            entity.Name = this.Name;
            entity.Status = this.Status;
            entity.AvatarUrl = AvatarUrl;
            entity.ModifiedDate = DateTime.Now;
        }
    }

    public class SupplierQueryFilter
    {
        public string TextSearch { get; set; }
        public int? PageSize { get; set; }
        public int? PageNumber { get; set; }
        public bool? Status { get; set; }
        public string PropertyName { get; set; } = "CreatedDate";
        //asc - desc
        public string Ascending { get; set; } = "desc";
        public SupplierQueryFilter()
        {
            PageNumber = QueryFilter.DefaultPageNumber;
            PageSize = QueryFilter.DefaultPageSize;
        }
    }
}