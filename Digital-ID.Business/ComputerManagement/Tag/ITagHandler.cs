﻿
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DigitalID.Data;

namespace NetCore.Business
{
    /// <summary>
    /// Interface quản lý Tag
    /// </summary>
    public interface ITagHandler
    {
        /// <summary>
        /// Thêm mới Tag
        /// </summary>
        /// <param name="model">Model thêm mới Tag</param>
        /// <returns>Id Tag</returns>
        Task<Response> Create(TagCreateModel model);

        /// <summary>
        /// Thêm mới Tag theo danh sách
        /// </summary>
        /// <param name="list">Danh sách thông tin Tag</param>
        /// <returns>Danh sách kết quả thêm mới</returns> 
        Task<Response> CreateMany(List<TagCreateModel> list);

        /// <summary>
        /// Cập nhật Tag
        /// </summary>
        /// <param name="model">Model cập nhật Tag</param>
        /// <returns>Id Tag</returns>
        Task<Response> Update(TagUpdateModel model);

        /// <summary>
        /// Xóa Tag
        /// </summary>
        /// <param name="listId">Danh sách Id Tag</param>
        /// <returns>Danh sách kết quả xóa</returns>
        Task<Response> Delete(List<Guid> listId);

        /// <summary>
        /// Lấy danh sách Tag theo điều kiện lọc
        /// </summary>
        /// <param name="filter">Model điều kiện lọc</param>
        /// <returns>Danh sách Tag</returns>
        Task<Response> Filter(TagQueryFilter filter);

        /// <summary>
        /// Lấy Tag theo Id
        /// </summary>
        /// <param name="id">Id Tag</param>
        /// <returns>Thông tin Tag</returns>
        Task<Response> GetById(Guid id);

        /// <summary>
        /// Lấy danh sách Tag cho combobox
        /// </summary>
        /// <param name="count">Số bản ghi tối đa</param>
        /// <param name="textSearch">Từ khóa tìm kiếm</param>
        /// <returns>Danh sách Tag cho combobox</returns>
        Task<Response> GetListCombobox(int count = 0, string textSearch = "");
    }
}
