﻿using DigitalID.Data;
using System;
using System.Collections.Generic;

namespace NetCore.Business
{
    public class TagBaseModel
    {
        public Guid Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Uri { get; set; }
        public bool? Status { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
    }

    public class TagModel : TagBaseModel
    {
        public int Order { get; set; } = 0;

    }

    public class TagDetailModel : TagModel
    {
        public Guid? CreatedUserId { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public Guid? ModifiedUserId { get; set; }
    }

    public class TagCreateModel : TagModel
    {
        public Guid? CreatedUserId { get; set; }
        public Guid? ApplicationId { get; set; }
    }

    public class TagUpdateModel : TagModel
    {
        public Guid ModifiedUserId { get; set; }

        public void UpdateToEntity(Tag entity)
        {
            entity.Code = this.Code;
            entity.Name = this.Name;
            entity.Status = this.Status;
            entity.ModifiedDate = DateTime.Now;
        }
    }

    public class TagQueryFilter
    {
        public string TextSearch { get; set; }
        public int? PageSize { get; set; }
        public int? PageNumber { get; set; }
        public bool? Status { get; set; }
        public string PropertyName { get; set; } = "CreatedDate";
        //asc - desc
        public string Ascending { get; set; } = "desc";
        public TagQueryFilter()
        {
            PageNumber = QueryFilter.DefaultPageNumber;
            PageSize = QueryFilter.DefaultPageSize;
        }
    }
}