﻿
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DigitalID.Data;

namespace NetCore.Business
{
    /// <summary>
    /// Interface quản lý User
    /// </summary>
    public interface ICustomerHandler
    {
        /// <summary>
        /// Thêm mới User
        /// </summary>
        /// <param name="model">Model thêm mới User</param>
        /// <returns>Id User</returns>
        Task<Response> Create(UserCusCreateModel model);

        /// <summary>
        /// Thêm mới User theo danh sách
        /// </summary>
        /// <param name="list">Danh sách thông tin User</param>
        /// <returns>Danh sách kết quả thêm mới</returns> 
        Task<Response> CreateMany(List<UserCusCreateModel> list);

        /// <summary>
        /// Cập nhật User
        /// </summary>
        /// <param name="model">Model cập nhật User</param>
        /// <returns>Id User</returns>
        Task<Response> Update(UserCusUpdateModel model);

        /// <summary>
        /// Cập nhật User
        /// </summary>
        /// <param name="model">Model cập nhật User</param>
        /// <returns>Id User</returns>
        Task<Response> UpdateListVoucher(UserCusUpdateModel model);

        /// <summary>
        /// Xóa User
        /// </summary>
        /// <param name="listId">Danh sách Id User</param>
        /// <returns>Danh sách kết quả xóa</returns>
        Task<Response> Delete(List<Guid> listId);

        /// <summary>
        /// Cập nhật quyền Qtv
        /// </summary>
        /// <param name="listId">Danh sách Id User</param>
        /// <returns>Danh sách kết quả xóa</returns>
        Task<Response> updateRole(List<Guid> listId);

        /// <summary>
        /// Lấy danh sách User theo điều kiện lọc
        /// </summary>
        /// <param name="filter">Model điều kiện lọc</param>
        /// <returns>Danh sách User</returns>
        Task<Response> Filter(UserCusQueryFilter filter);

        /// <summary>
        /// Lấy User theo Id
        /// </summary>
        /// <param name="id">Id User</param>
        /// <returns>Thông tin User</returns>
        Task<Response> GetById(Guid id);
        /// <summary>
        /// Cập nhật mật khẩu
        /// </summary>
        /// <param name="model">Model cập nhật mật khẩu</param>
        /// <returns>Id người dùng</returns>
        Task<Response> UpdatePassword(UserUpdatePasswordModel model);
        /// <summary>
        /// Lấy danh sách User cho combobox
        /// </summary>
        /// <param name="count">Số bản ghi tối đa</param>
        /// <param name="textSearch">Từ khóa tìm kiếm</param>
        /// <returns>Danh sách User cho combobox</returns>
        Task<Response> GetListCombobox(int count = 0, string textSearch = "");

        /// <summary>
        /// Quên mật khẩu
        /// </summary>
        /// <param name="id">Email User</param>
        /// <returns>Password User</returns>
        Task<Response> FogotPassword(string email);

        /// <summary>
        /// signup google
        /// </summary>
        /// <param name="id">Email User</param>
        /// <returns>Password User</returns>
        Task<Response> RegisterOrGetDetail(GoogleUserModel model);

    }
}
