﻿using Microsoft.EntityFrameworkCore;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using DigitalID.Business;
using DigitalID.Data;
using System.Text;

namespace NetCore.Business
{
    public class CustomerHandler : ICustomerHandler
    {
        #region Message
        #endregion
        private readonly Random _random = new Random();
                private string frontendUrl = Utils.GetConfig("Url:Frontend");
        private readonly IEmailHandler _emailHandler;
        private const string CachePrefix = "User";
        private const string SelectItemCacheSubfix = "list-select";
        private const string CodePrefix = "User.";
        private readonly DataContext _dataContext;
        private readonly ICacheService _cacheService;
        private readonly ICartHandler _cartHandler;

        public CustomerHandler(DataContext dataContext, ICacheService cacheService, ICartHandler cartHandler, IEmailHandler emailHandler)
        {
            _emailHandler = emailHandler;
            _dataContext = dataContext;
            _cacheService = cacheService;
            _cartHandler = cartHandler;
        }

        public async Task<Response> Create(UserCusCreateModel model)
        {
            try
            {
                var isExistUsername = _dataContext.Users.Any(c => c.Username == model.Username);
                if (isExistUsername)
                    return new ResponseError(Code.NotFound, "Tài khoản đã tồn tại");
                #region Check is exist document Type
                var isExistEmail = _dataContext.Users.Any(c => c.Email == model.Email);
                if (isExistEmail)
                    return new ResponseError(Code.NotFound, "Email đã tồn tại");

                #endregion
                var entity = AutoMapperUtils.AutoMap<UserCusCreateModel, User>(model);

                entity.CreatedDate = DateTime.Now;

                long identityNumber = await _dataContext.Users.DefaultIfEmpty().MaxAsync(x => x.IdentityNumber);

                entity.IdentityNumber = ++identityNumber;
                entity.Code = Utils.GenerateAutoCode(CodePrefix, identityNumber);

                entity.Id = Guid.NewGuid();
                await _dataContext.Users.AddAsync(entity);
                var user_cart = new CartCreateModel()
                {
                    UserId = entity.Id,
                    Status = 1
                };
                var rs = _cartHandler.Create(user_cart);
                int dbSave = await _dataContext.SaveChangesAsync();

                if (dbSave > 0)
                {
                    //Log.Information("Add success: " + JsonSerializer.Serialize(entity));
                    InvalidCache();

                    return new ResponseObject<Guid>(entity.Id, MessageConstants.CreateSuccessMessage, Code.Success);
                }
                else
                {
                    return new ResponseError(Code.ServerError, MessageConstants.CreateErrorMessage);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, MessageConstants.ErrorLogMessage);
                return new ResponseError(Code.ServerError, $"{MessageConstants.CreateErrorMessage} - {ex.Message}");
            }
        }

        public async Task<Response> CreateMany(List<UserCusCreateModel> list)
        {
            try
            {

                var listId = new List<Guid>();
                var listRS = new List<User>();
                foreach (var item in list)
                {
                    var entity = AutoMapperUtils.AutoMap<UserCusCreateModel, User>(item);

                    entity.CreatedDate = DateTime.Now;
                    await _dataContext.Users.AddAsync(entity);
                    listId.Add(entity.Id);
                    listRS.Add(entity);
                }

                int dbSave = await _dataContext.SaveChangesAsync();

                if (dbSave > 0)
                {
                    Log.Information("Add success: " + JsonSerializer.Serialize(listRS));
                    InvalidCache();

                    return new ResponseObject<List<Guid>>(listId, MessageConstants.CreateSuccessMessage, Code.Success);
                }
                else
                {
                    return new ResponseError(Code.ServerError, MessageConstants.CreateErrorMessage);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, MessageConstants.ErrorLogMessage);
                return new ResponseError(Code.ServerError, $"{MessageConstants.CreateErrorMessage} - {ex.Message}");
            }
        }

        public async Task<Response> Update(UserCusUpdateModel model)
        {
            try
            {
                var isExistEmail = _dataContext.Users.Any(c => c.Email == model.Email && c.Id != model.Id);
                if (isExistEmail)
                    return new ResponseError(Code.NotFound, "Email đã tồn tại");
                var entity = await _dataContext.Users
                         .FirstOrDefaultAsync(x => x.Id == model.Id);

                entity.DateOfBirth = model.DateOfBirth;
                entity.Name = model.Name;
                entity.Email = model.Email;
                entity.Sex = model.Sex;
                entity.Avatar = model.Avatar;
                entity.Phone = model.Phone;
                _dataContext.Users.Update(entity);

                int dbSave = await _dataContext.SaveChangesAsync();
                if (dbSave > 0)
                {
                    //Log.Information("After Update: " + JsonSerializer.Serialize(entity));
                    InvalidCache(model.Id.ToString());

                    return new ResponseObject<Guid>(model.Id, MessageConstants.UpdateSuccessMessage, Code.Success);
                }
                else
                {
                    return new ResponseError(Code.ServerError, MessageConstants.UpdateErrorMessage);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, MessageConstants.ErrorLogMessage);
                return new ResponseError(Code.ServerError, $"{MessageConstants.GetDataErrorMessage} - {ex.Message}");
            }
        }

        public async Task<Response> UpdateListVoucher(UserCusUpdateModel model)
        {
            try
            {
                var entity = await _dataContext.Users
                    .FirstOrDefaultAsync(x => x.Id == model.Id);
                entity.Vouchers = new List<Voucher>();
                entity.Vouchers = model.Vouchers;
                _dataContext.Users.Update(entity);
                var listVoucher = new List<Voucher>();
                foreach (var item in model.Vouchers)
                {
                    var voucher = await _dataContext.Vouchers.Where(x => x.Id == item.Id).FirstOrDefaultAsync();
                    voucher.Used++;
                    if (voucher.Used == voucher.Quantity)
                    {
                        voucher.Status = false;
                    }
                    listVoucher.Add(voucher);
                }
                _dataContext.Vouchers.UpdateRange(listVoucher);
                int dbSave = await _dataContext.SaveChangesAsync();
                if (dbSave > 0)
                {
                    //Log.Information("After Update: " + JsonSerializer.Serialize(entity));
                    InvalidCache(model.Id.ToString());

                    return new ResponseObject<List<Voucher>>(listVoucher, MessageConstants.UpdateSuccessMessage, Code.Success);
                }
                else
                {
                    return new ResponseError(Code.ServerError, MessageConstants.UpdateErrorMessage);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, MessageConstants.ErrorLogMessage);
                return new ResponseError(Code.ServerError, $"{MessageConstants.GetDataErrorMessage} - {ex.Message}");
            }
        }

        public async Task<Response> Delete(List<Guid> listId)
        {
            try
            {
                var listResult = new List<ResponeDeleteModel>();
                var name = "";
                Log.Information("List Delete: " + JsonSerializer.Serialize(listId));
                foreach (var item in listId)
                {
                    name = "";
                    var entity = await _dataContext.Users.FindAsync(item);

                    if (entity == null)
                    {
                        listResult.Add(new ResponeDeleteModel()
                        {
                            Id = item,
                            Name = name,
                            Result = false,
                            Message = MessageConstants.DeleteItemNotFoundMessage
                        });
                    }
                    else
                    {
                        name = entity.Name;
                        entity.IsLock = !entity.IsLock;
                        _dataContext.Users.Update(entity);
                        try
                        {
                            int dbSave = await _dataContext.SaveChangesAsync();
                            if (dbSave > 0)
                            {
                                InvalidCache(item.ToString());

                                listResult.Add(new ResponeDeleteModel()
                                {
                                    Id = item,
                                    Name = name,
                                    Result = true,
                                    Message = MessageConstants.DeleteItemSuccessMessage
                                });
                            }
                            else
                            {
                                listResult.Add(new ResponeDeleteModel()
                                {
                                    Id = item,
                                    Name = name,
                                    Result = false,
                                    Message = MessageConstants.DeleteItemErrorMessage
                                });
                            }
                        }
                        catch (Exception ex)
                        {
                            Log.Error(ex, MessageConstants.ErrorLogMessage);
                            listResult.Add(new ResponeDeleteModel()
                            {
                                Id = item,
                                Name = name,
                                Result = false,
                                Message = ex.Message
                            });
                        }
                    }
                }
                Log.Information("List Result Delete: " + JsonSerializer.Serialize(listResult));
                return new ResponseObject<List<ResponeDeleteModel>>(listResult, MessageConstants.DeleteSuccessMessage, Code.Success);

            }
            catch (Exception ex)
            {
                Log.Error(ex, MessageConstants.ErrorLogMessage);
                return new ResponseError(Code.ServerError, $"{MessageConstants.DeleteErrorMessage} - {ex.Message}");
            }
        }

        public async Task<Response> updateRole(List<Guid> listId)
        {
            try
            {
                var listResult = new List<ResponeDeleteModel>();
                var name = "";
                Log.Information("List Delete: " + JsonSerializer.Serialize(listId));
                foreach (var item in listId)
                {
                    name = "";
                    var entity = await _dataContext.Users.FindAsync(item);

                    if (entity == null)
                    {
                        listResult.Add(new ResponeDeleteModel()
                        {
                            Id = item,
                            Name = name,
                            Result = false,
                            Message = MessageConstants.DeleteItemNotFoundMessage
                        });
                    }
                    else
                    {
                        name = entity.Name;
                        entity.IsQtv = !entity.IsQtv;
                        _dataContext.Users.Update(entity);
                        try
                        {
                            int dbSave = await _dataContext.SaveChangesAsync();
                            if (dbSave > 0)
                            {
                                InvalidCache(item.ToString());

                                listResult.Add(new ResponeDeleteModel()
                                {
                                    Id = item,
                                    Name = name,
                                    Result = true,
                                    Message = "Thay đổi thành công"
                                });
                            }
                            else
                            {
                                listResult.Add(new ResponeDeleteModel()
                                {
                                    Id = item,
                                    Name = name,
                                    Result = false,
                                    Message = MessageConstants.DeleteItemErrorMessage
                                });
                            }
                        }
                        catch (Exception ex)
                        {
                            Log.Error(ex, MessageConstants.ErrorLogMessage);
                            listResult.Add(new ResponeDeleteModel()
                            {
                                Id = item,
                                Name = name,
                                Result = false,
                                Message = ex.Message
                            });
                        }
                    }
                }
                Log.Information("List Result Delete: " + JsonSerializer.Serialize(listResult));
                return new ResponseObject<List<ResponeDeleteModel>>(listResult, MessageConstants.DeleteSuccessMessage, Code.Success);

            }
            catch (Exception ex)
            {
                Log.Error(ex, MessageConstants.ErrorLogMessage);
                return new ResponseError(Code.ServerError, $"{MessageConstants.DeleteErrorMessage} - {ex.Message}");
            }
        }

        public async Task<Response> Filter(UserCusQueryFilter filter)
        {
            try
            {
                var data = (from c in _dataContext.Users

                            select new UserBaseModel()
                            {
                                Id = c.Id,
                                Code = c.Code,
                                Name = c.Name,
                                Username = c.Username,
                                Password = c.Password,
                                PasswordSalt = c.PasswordSalt,
                                Email = c.Email,
                                Phone = c.Phone,
                                IsLock = c.IsLock,
                                IsQtv = c.IsQtv,
                                IsAdmin = c.IsAdmin,
                                Avatar = c.Avatar,
                                DateOfBirth = c.DateOfBirth,
                                Sex = c.Sex,
                                CreatedDate = c.CreatedDate,
                            });

                if (!string.IsNullOrEmpty(filter.TextSearch))
                {
                    string ts = filter.TextSearch.Trim().ToLower();
                    data = data.Where(x => x.Name.ToLower().Contains(ts) || x.Code.ToLower().Contains(ts));
                }

                if (filter.Status.HasValue)
                {
                    data = data.Where(x => x.IsLock == filter.Status);
                }

                data = data.OrderByField(filter.PropertyName, filter.Ascending);

                int totalCount = data.Count();

                // Pagination
                if (filter.PageSize.HasValue && filter.PageNumber.HasValue)
                {
                    if (filter.PageSize <= 0)
                    {
                        filter.PageSize = QueryFilter.DefaultPageSize;
                    }

                    //Calculate nunber of rows to skip on pagesize
                    int excludedRows = (filter.PageNumber.Value - 1) * (filter.PageSize.Value);
                    if (excludedRows <= 0)
                    {
                        excludedRows = 0;
                    }

                    // Query
                    data = data.Skip(excludedRows).Take(filter.PageSize.Value);
                }
                int dataCount = data.Count();

                var listResult = await data.ToListAsync();
                return new ResponseObject<PaginationList<UserBaseModel>>(new PaginationList<UserBaseModel>()
                {
                    DataCount = dataCount,
                    TotalCount = totalCount,
                    PageNumber = filter.PageNumber ?? 0,
                    PageSize = filter.PageSize ?? 0,
                    Data = listResult
                }, MessageConstants.GetDataSuccessMessage, Code.Success);
            }
            catch (Exception ex)
            {
                Log.Error(ex, MessageConstants.ErrorLogMessage);
                return new ResponseError(Code.ServerError, $"{MessageConstants.GetDataErrorMessage} - {ex.Message}");
            }
        }
        public async Task<Response> UpdatePassword(UserUpdatePasswordModel model)
        {
            try
            {
                var entity = await _dataContext.Users
                    .FirstOrDefaultAsync(x => x.Id == model.UserId);
                //Log.Information("Before Update Password: " + JsonSerializer.Serialize(entity));

                if (!entity.Password.Equals(Utils.PasswordGenerateHmac(model.OldPassword, entity.PasswordSalt)))
                {
                    return new ResponseObject<Guid>(Guid.Empty, "Mật khẩu cũ không chính xác", Code.ServerError); ;
                }

                entity.PasswordSalt = Utils.PassowrdCreateSalt512();
                entity.Password = Utils.PasswordGenerateHmac(model.NewPassword, entity.PasswordSalt);
                entity.ModifiedDate = DateTime.Now;
                entity.ModifiedUserId = model.ModifiedUserId;

                _dataContext.Users.Update(entity);

                int dbSave = await _dataContext.SaveChangesAsync();
                if (dbSave > 0)
                {
                    //Log.Information("After Update Password: " + JsonSerializer.Serialize(entity));
                    InvalidCache(model.UserId.ToString());

                    return new ResponseObject<Guid>(model.UserId, MessageConstants.UpdateSuccessMessage, Code.Success);
                }
                else
                {
                    return new ResponseError(Code.ServerError, MessageConstants.UpdateErrorMessage);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, MessageConstants.ErrorLogMessage);
                return new ResponseError(Code.ServerError, $"{MessageConstants.GetDataErrorMessage} - {ex.Message}");
            }
        }
        public async Task<Response> GetById(Guid id)
        {
            try
            {
                string cacheKey = BuildCacheKey(id.ToString());
                var entity = await _dataContext.Users
                    .FirstOrDefaultAsync(x => x.Id == id);

                var rs = AutoMapperUtils.AutoMap<User, UserBaseModel>(entity);
                return new ResponseObject<UserBaseModel>(rs, MessageConstants.GetDataSuccessMessage, Code.Success);
            }
            catch (Exception ex)
            {
                Log.Error(ex, MessageConstants.ErrorLogMessage);
                return new ResponseError(Code.ServerError, $"{MessageConstants.GetDataErrorMessage} - {ex.Message}");
            }
        }

        public async Task<Response> GetListCombobox(int count = 0, string textSearch = "")
        {
            try
            {
                string cacheKey = BuildCacheKey(SelectItemCacheSubfix);
                var list = await _cacheService.GetOrCreate(cacheKey, async () =>
                {
                    var data = (from item in _dataContext.Users.Where(x => x.IsLock == false).OrderBy(x => x.CreatedDate).ThenBy(x => x.Name)
                                select new SelectItemModel()
                                {
                                    Id = item.Id,
                                    Name = item.Name,
                                    Note = item.Code
                                });

                    return await data.ToListAsync();
                });

                if (!string.IsNullOrEmpty(textSearch))
                {
                    textSearch = textSearch.ToLower().Trim();
                    list = list.Where(x => x.Name.ToLower().Contains(textSearch) || x.Note.ToLower().Contains(textSearch)).ToList();
                }

                if (count > 0)
                {
                    list = list.Take(count).ToList();
                }

                return new ResponseObject<List<SelectItemModel>>(list, MessageConstants.GetDataSuccessMessage, Code.Success);
            }
            catch (Exception ex)
            {
                Log.Error(ex, MessageConstants.ErrorLogMessage);
                return new ResponseError(Code.ServerError, $"{MessageConstants.GetDataErrorMessage} - {ex.Message}");
            }
        }

        private void InvalidCache(string id = "")
        {
            if (!string.IsNullOrEmpty(id))
            {
                string cacheKey = BuildCacheKey(id);
                _cacheService.Remove(cacheKey);
            }

            string selectItemCacheKey = BuildCacheKey(SelectItemCacheSubfix);
            _cacheService.Remove(selectItemCacheKey);
        }

        private string BuildCacheKey(string id)
        {
            return $"{CachePrefix}-{id}";
        }

        public async Task<Response> FogotPassword(string email)
        {
            try
            {
                var userModel = _dataContext.Users.Where(x => x.Email.Equals(email)).FirstOrDefault();
                if (userModel == null)
                {
                    return new ResponseObject<string>(null, "Email không tồn tại trong hệ thống!", Code.ServerError);
                }
                userModel.PasswordSalt = Utils.PassowrdCreateSalt512();
                var newPassword = RandomPassword();
                userModel.Password = Utils.PasswordGenerateHmac(newPassword, userModel.PasswordSalt);
                _dataContext.Users.Update(userModel);
                var dbSave = await _dataContext.SaveChangesAsync();
                if (dbSave >= 1)
                {
                    #region Gửi email thông báo đến khách hàng

                    string title = "[Vân Anh - Computer] - Lấy lại mật khẩu thành công";


                    StringBuilder htmlBody = new StringBuilder();
                    htmlBody.Append("<html><body>");
                    htmlBody.Append("<p>Xin chào <b>" + userModel.Name + "</b>,</p>");
                    htmlBody.Append("<p>Bạn đã lấy lại mật khẩu thành công. Bạn hãy truy cập vào trang web để thay đổi mật khẩu.</p>");
                    htmlBody.Append("<p>Mật khẩu mới của bạn là: <b>" + newPassword + "</b></p>");
                    htmlBody.Append("<p><a href='"+frontendUrl+"'><span class='fas fa - laptop'></span> <p>Vân Anh<strong> Computer</strong> <span>Thế giới máy tính<span></p> </a> </p>");
                    htmlBody.Append("</body></html>");

                    _emailHandler.SendMailGoogle(new List<string>() { userModel.Email }, null, null, title, htmlBody.ToString());

                    #endregion
                    return new ResponseObject<string>(null, "Lấy mật khẩu thành công!", Code.Success);
                }
                return new ResponseObject<string>(null, "Lấy mật khẩu thất bại!", Code.ServerError);
            }
            catch (Exception ex)
            {
                Log.Error(ex, MessageConstants.ErrorLogMessage);
                return new ResponseError(Code.ServerError, $"{MessageConstants.GetDataErrorMessage} - {ex.Message}");
            }
        }
        public async Task<Response> RegisterOrGetDetail(GoogleUserModel model)
        {
            try
            {
                var userModel = await _dataContext.Users.Where(x => x.Username.Equals(model.Email) && x.Provider.Equals(model.Provider)).FirstOrDefaultAsync();
                if (userModel != null)
                {
                    return new ResponseObject<User>(userModel, MessageConstants.CreateSuccessMessage, Code.Success);
                }
                var userCreate = new UserCusCreateModel()
                {
                    Avatar = model.PhotoUrl,
                    Email = model.Email,
                    Username = model.Email,
                    Name = model.Name,
                    IsSocialSign = true,
                    Provider = model.Provider
                };
                var isExistUsername = _dataContext.Users.Any(c => c.Username == model.Email && c.Provider.Equals(model.Provider));
                if (isExistUsername)
                    return new ResponseError(Code.NotFound, "Tài khoản đã tồn tại");
                #region Check is exist document Type
                var isExistEmail = _dataContext.Users.Any(c => c.Email == model.Email && c.Provider.Equals(model.Provider));
                if (isExistEmail)
                    return new ResponseError(Code.NotFound, "Email đã tồn tại");

                #endregion
                var entity = AutoMapperUtils.AutoMap<UserCusCreateModel, User>(userCreate);

                entity.CreatedDate = DateTime.Now;

                long identityNumber = await _dataContext.Users.DefaultIfEmpty().MaxAsync(x => x.IdentityNumber);
                entity.PasswordSalt = Utils.PassowrdCreateSalt512();
                var newPassword = "123456@a";
                entity.Password = Utils.PasswordGenerateHmac(newPassword, entity.PasswordSalt);
                entity.IdentityNumber = ++identityNumber;
                entity.Code = Utils.GenerateAutoCode(CodePrefix, identityNumber);

                entity.Id = Guid.NewGuid();
                await _dataContext.Users.AddAsync(entity);
                var user_cart = new CartCreateModel()
                {
                    UserId = entity.Id,
                    Status = 1
                };
                var rs = _cartHandler.Create(user_cart);
                int dbSave = await _dataContext.SaveChangesAsync();

                if (dbSave > 0)
                {
                    //Log.Information("Add success: " + JsonSerializer.Serialize(entity));
                    InvalidCache();
                    #region Gửi email thông báo đến khách hàng

                    string title = "[Vân Anh - Computer] - Đăng ký tài khoản thành công";


                    StringBuilder htmlBody = new StringBuilder();
                    htmlBody.Append("<html><body>");
                    htmlBody.Append("<p>Xin chào <b>" + entity.Name + "</b>,</p>");
                    htmlBody.Append("<p>Bạn đã đăng ký tài khoản thành công. Hãy tiếp tục ghé thăm Vân anh Computer để chọn lựa cho mình những sản phẩm ưu đãi nhất.</p>");
                    htmlBody.Append("<p><a href='"+frontendUrl+"'><span class='fas fa - laptop'></span> <p>Vân Anh<strong> Computer</strong> <span>Thế giới máy tính<span></p> </a> </p>");
                    htmlBody.Append("</body></html>");

                    _emailHandler.SendMailGoogle(new List<string>() { entity.Email }, null, null, title, htmlBody.ToString());

                    #endregion
                    return new ResponseObject<User>(entity, MessageConstants.CreateSuccessMessage, Code.Success);
                }
                return new ResponseError(Code.ServerError, MessageConstants.CreateErrorMessage);
            }
            catch (Exception ex)
            {
                Log.Error(ex, MessageConstants.ErrorLogMessage);
                return new ResponseError(Code.ServerError, $"{MessageConstants.GetDataErrorMessage} - {ex.Message}");
            }
        }
        // Generates a random password.  
        // 4-LowerCase + 4-Digits + 2-UpperCase  
        public string RandomPassword()
        {
            var passwordBuilder = new StringBuilder();

            // 4-Letters lower case   
            passwordBuilder.Append(RandomString(4, true));

            // 4-Digits between 1000 and 9999  
            passwordBuilder.Append(RandomNumber(1000, 9999));

            // 2-Letters upper case  
            passwordBuilder.Append(RandomString(2));
            return passwordBuilder.ToString();
        }
        // Generates a random string with a given size.    
        public string RandomString(int size, bool lowerCase = false)
        {
            var builder = new StringBuilder(size);

            // Unicode/ASCII Letters are divided into two blocks
            // (Letters 65–90 / 97–122):
            // The first group containing the uppercase letters and
            // the second group containing the lowercase.  

            // char is a single Unicode character  
            char offset = lowerCase ? 'a' : 'A';
            const int lettersOffset = 26; // A...Z or a..z: length=26  

            for (var i = 0; i < size; i++)
            {
                var @char = (char)_random.Next(offset, offset + lettersOffset);
                builder.Append(@char);
            }

            return lowerCase ? builder.ToString().ToLower() : builder.ToString();
        }
        // Generates a random number within a range.      
        public int RandomNumber(int min, int max)
        {
            return _random.Next(min, max);
        }


    }
}