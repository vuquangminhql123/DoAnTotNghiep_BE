﻿using DigitalID.Data;
using System;
using System.Collections.Generic;

namespace NetCore.Business
{
    public class UserBaseModel
    {
        public Guid Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string PasswordSalt { get; set; }
        public string Email { get; set; }
        public bool IsSocialSign { get; set; }
        public bool IsQtv { get; set; }
        public string Phone { get; set; }
        public string VoucherJson { get; set; }
        public string Provider { get; set; }
        public bool IsLock { get; set; }
        public bool IsAdmin { get; set; }
        public List<Voucher> Vouchers { get; set; }
        public string Avatar { get; set; }
        public DateTime DateOfBirth { get; set; }
        public bool Sex { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public Guid? CartId { get; set; }
    }
    public class GoogleUserModel
    {
        public string Id { get; set; }
        public string AuthToken { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string IdToken { get; set; }
        public string LastName { get; set; }
        public string Name { get; set; }
        public string PhotoUrl { get; set; }
        public string Provider { get; set; }
    }
    public class UserCusModel : UserBaseModel
    {
        public int Order { get; set; } = 0;

    }
    public class UserUpdatePasswordModel
    {
        public Guid UserId { get; set; }
        public string OldPassword { get; set; }
        public string NewPassword { get; set; }
        public Guid? ModifiedUserId { get; set; }
    }
    public class UserDetailModel : UserCusModel
    {
        public Guid? CreatedUserId { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public Guid? ModifiedUserId { get; set; }
    }

    public class UserCusCreateModel : UserCusModel
    {
        public Guid? CreatedUserId { get; set; }
        public Guid? ApplicationId { get; set; }
    }

    public class UserCusUpdateModel : UserCusModel
    {
        public Guid ModifiedUserId { get; set; }

        public void UpdateToEntity(User entity)
        {
            entity.Code = this.Code;
            entity.Name = this.Name;
            entity.Email = this.Email;
            entity.Phone = this.Phone;
            entity.IsLock = this.IsLock;
            entity.IsAdmin = this.IsAdmin;
            entity.DateOfBirth = this.DateOfBirth;
            entity.Sex = this.Sex;
            entity.ModifiedDate = DateTime.Now;
        }


    }

    public class UserCusQueryFilter
    {
        public string TextSearch { get; set; }
        public int? PageSize { get; set; }
        public int? PageNumber { get; set; }
        public bool? Status { get; set; }
        public string PropertyName { get; set; } = "CreatedDate";
        //asc - desc
        public string Ascending { get; set; } = "desc";
        public UserCusQueryFilter()
        {
            PageNumber = QueryFilter.DefaultPageNumber;
            PageSize = QueryFilter.DefaultPageSize;
        }
    }
}