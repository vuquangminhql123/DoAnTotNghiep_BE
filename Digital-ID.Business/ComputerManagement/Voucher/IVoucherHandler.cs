﻿
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DigitalID.Data;

namespace NetCore.Business
{
    /// <summary>
    /// Interface quản lý Voucher
    /// </summary>
    public interface IVoucherHandler
    {
        /// <summary>
        /// Thêm mới Voucher
        /// </summary>
        /// <param name="model">Model thêm mới Voucher</param>
        /// <returns>Id Voucher</returns>
        Task<Response> Create(VoucherCreateModel model);

        /// <summary>
        /// Thêm mới Voucher theo danh sách
        /// </summary>
        /// <param name="list">Danh sách thông tin Voucher</param>
        /// <returns>Danh sách kết quả thêm mới</returns> 
        Task<Response> CreateMany(List<VoucherCreateModel> list);

        /// <summary>
        /// Cập nhật Voucher
        /// </summary>
        /// <param name="model">Model cập nhật Voucher</param>
        /// <returns>Id Voucher</returns>
        Task<Response> Update(VoucherUpdateModel model);

        /// <summary>
        /// Xóa Voucher
        /// </summary>
        /// <param name="listId">Danh sách Id Voucher</param>
        /// <returns>Danh sách kết quả xóa</returns>
        Task<Response> Delete(List<Guid> listId);

        /// <summary>
        /// Lấy danh sách Voucher theo điều kiện lọc
        /// </summary>
        /// <param name="filter">Model điều kiện lọc</param>
        /// <returns>Danh sách Voucher</returns>
        Task<Response> Filter(VoucherQueryFilter filter);

        /// <summary>
        /// Lấy Voucher theo Id
        /// </summary>
        /// <param name="id">Id Voucher</param>
        /// <returns>Thông tin Voucher</returns>
        Task<Response> GetById(Guid id);


        /// <summary>
        /// Lấy danh sách Voucher cho combobox
        /// </summary>
        /// <param name="count">Số bản ghi tối đa</param>
        /// <param name="textSearch">Từ khóa tìm kiếm</param>
        /// <returns>Danh sách Voucher cho combobox</returns>
        Task<Response> GetListCombobox(int count = 0, string textSearch = "");
    }
}
