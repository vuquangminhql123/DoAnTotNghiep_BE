﻿using DigitalID.Data;
using System;
using System.Collections.Generic;

namespace NetCore.Business
{
    public class VoucherBaseModel
    {
        public Guid Id { get; set; }
        public string Code { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ExpiredTime { get; set; }
        public DateTime StartTime { get; set; }
        public int Used { get; set; }
        public int Quantity { get; set; }
        //Phần trăm giảm giá
        public bool Status { get; set; }
        public double? PercentDiscount { get; set; }
        public int Type { get; set; }
        public double? Discount { get; set; }
    }

    public class VoucherModel : VoucherBaseModel
    {
        public int Order { get; set; } = 0;

    }

    public class VoucherDetailModel : VoucherModel
    {
        public Guid? CreatedUserId { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public Guid? ModifiedUserId { get; set; }
    }

    public class VoucherCreateModel : VoucherModel
    {
        public Guid? CreatedUserId { get; set; }
        public Guid? ApplicationId { get; set; }
    }

    public class VoucherUpdateModel : VoucherModel
    {
        public Guid ModifiedUserId { get; set; }

        public void UpdateToEntity(Voucher entity)
        {
            entity.Code = this.Code;
            entity.ExpiredTime = this.ExpiredTime;
            entity.StartTime = this.StartTime;
            entity.Discount = Discount;
            entity.Status = Status;
            entity.PercentDiscount = PercentDiscount;
            entity.Quantity = Quantity;
            entity.Type = Type;
        }
    }

    public class VoucherQueryFilter
    {
        public string TextSearch { get; set; }
        public int? PageSize { get; set; }
        public int? PageNumber { get; set; }
        public bool? Type { get; set; }
        public bool? Status { get; set; }
        public string PropertyName { get; set; } = "CreatedDate";
        //asc - desc
        public string Ascending { get; set; } = "desc";
        public VoucherQueryFilter()
        {
            PageNumber = QueryFilter.DefaultPageNumber;
            PageSize = QueryFilter.DefaultPageSize;
        }
    }
}