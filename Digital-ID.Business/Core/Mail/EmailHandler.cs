﻿using DigitalID.Data;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text.Json;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace DigitalID.Business
{
    public class EmailHandler : IEmailHandler
    {
        public EmailConfiguration mailConfig;

        public EmailHandler()
        {
            LoadMailConfiguration();
        }

        public EmailHandler(EmailConfiguration request)
        {
            LoadCustomConfiguration(request);
        }

        private void LoadMailConfiguration()
        {
            mailConfig = new EmailConfiguration();
            mailConfig.MailConfigEnable = Utils.GetConfig(MailConstants.MailConfigParameter.MAIL_CONFIG_ENABLED).Equals("1");
            mailConfig.MailConfigPort = Utils.GetConfig(MailConstants.MailConfigParameter.MAIL_CONFIG_PORT);
            mailConfig.MailConfigFrom = Utils.GetConfig(MailConstants.MailConfigParameter.MAIL_CONFIG_FROM);
            mailConfig.MailConfigSmtp = Utils.GetConfig(MailConstants.MailConfigParameter.MAIL_CONFIG_SMTP);
            mailConfig.MailConfigSsl = Utils.GetConfig(MailConstants.MailConfigParameter.MAIL_CONFIG_SSL).Equals("1");
            mailConfig.MailConfigPassword = Utils.GetConfig(MailConstants.MailConfigParameter.MAIL_CONFIG_PASSWORD);
            mailConfig.MailConfigSendType = Utils.GetConfig(MailConstants.MailConfigParameter.MAIL_CONFIG_SEND_TYPE);
            mailConfig.MailConfigUser = Utils.GetConfig(MailConstants.MailConfigParameter.MAIL_CONFIG_USER);
        }

        private void LoadCustomConfiguration(EmailConfiguration request)
        {
            mailConfig = new EmailConfiguration();
            mailConfig.MailConfigEnable = request.MailConfigEnable.Equals("1");
            mailConfig.MailConfigSmtp = request.MailConfigSmtp;
            mailConfig.MailConfigPort = request.MailConfigPort;
            mailConfig.MailConfigSendType = request.MailConfigSendType;
            mailConfig.MailConfigSsl = request.MailConfigSsl.Equals("1");
            mailConfig.MailConfigFrom = request.MailConfigFrom;
            mailConfig.MailConfigUser = request.MailConfigUser;
            mailConfig.MailConfigPassword = request.MailConfigPassword;
            mailConfig.MailConfigTemplate = request.MailConfigTemplate;
        }

        public bool SendMailExchange(List<string> toEmails, List<string> ccEmails, List<string> bccEmail, string title, string body, Dictionary<string, byte[]> fileAttach)
        {
            if (!mailConfig.MailConfigEnable)
                return true;
            SmtpClient mySmtpClient = new SmtpClient();
            MailMessage mail = new MailMessage();
            try
            {
                mail.BodyEncoding = System.Text.Encoding.UTF8;

                mySmtpClient.UseDefaultCredentials = false;
                mySmtpClient.EnableSsl = mailConfig.MailConfigSsl;
                // mySmtpClient.
                // Set port
                string tempPort = mailConfig.MailConfigPort;
                int port;
                if (int.TryParse(tempPort, out port))
                {
                    mySmtpClient.Port = port;
                }

                // Set host
                string tempHost = mailConfig.MailConfigSmtp;
                if (!string.IsNullOrEmpty(tempHost))
                {
                    mySmtpClient.Host = tempHost;
                }

                // Set from
                string tempFrom = mailConfig.MailConfigFrom;
                if (!string.IsNullOrEmpty(tempFrom))
                {
                    mail.From = new MailAddress(tempFrom, mailConfig.MailConfigUser);
                }

                // Set credential password
                string tempPassword = mailConfig.MailConfigPassword;
                if (!string.IsNullOrEmpty(tempPassword) && !string.IsNullOrEmpty(mailConfig.MailConfigFrom))
                {
                    mySmtpClient.Credentials = new NetworkCredential(mailConfig.MailConfigFrom, tempPassword);
                }
                if (toEmails != null)
                {
                    foreach (string email in toEmails)
                    {
                        if (IsValidEmail(email))
                        {
                            mail.To.Add(email);
                        }
                    }
                }

                if (ccEmails != null)
                {
                    foreach (string email in ccEmails)
                    {
                        if (IsValidEmail(email))
                        {
                            mail.CC.Add(email);
                        }
                    }
                }

                if (bccEmail != null)
                {
                    foreach (string email in bccEmail)
                    {
                        if (IsValidEmail(email))
                        {
                            mail.Bcc.Add(email);
                        }
                    }
                }

                mail.Subject = title;
                mail.Body = body;

                ServicePointManager.ServerCertificateValidationCallback +=
                    delegate (
                        object sender,
                        X509Certificate certificate,
                        X509Chain chain,
                        SslPolicyErrors sslPolicyErrors)
                    {
                        return true;
                    };

                mail.IsBodyHtml = true;
                Attachment attach;
                ContentType contentType;
                MemoryStream ms = null;
                if (fileAttach != null)
                {
                    foreach (KeyValuePair<string, byte[]> keyvalue in fileAttach)
                    {
                        if (!string.IsNullOrEmpty(keyvalue.Key) && keyvalue.Value != null && keyvalue.Value.Length > 0)
                        {
                            contentType = new ContentType(GetContentTypeFromFileName(keyvalue.Key));
                            ms = new MemoryStream(keyvalue.Value);
                            ms.Flush();
                            attach = new Attachment(ms, contentType);
                            attach.Name = GetFileNameForDisplay(keyvalue.Key);
                            mail.Attachments.Add(attach);
                        }
                    }
                }

                // Log
                Log.Information("MainConfig : " + JsonSerializer.Serialize(mailConfig));
                Log.Information("toEmails : " + JsonSerializer.Serialize(toEmails));

                if (mailConfig.MailConfigSendType == MailConstants.MailConfigSendType.SYNC || mailConfig.MailConfigSendType == MailConstants.MailConfigSendType.BOTH)
                {
                    mySmtpClient.Send(mail);
                }

                if (mailConfig.MailConfigSendType == MailConstants.MailConfigSendType.ASYNC || mailConfig.MailConfigSendType == MailConstants.MailConfigSendType.BOTH)
                {
                    mySmtpClient.SendAsync(mail, Guid.NewGuid());
                }

                if (ms != null)
                {
                    ms.Close();
                    ms.Dispose();
                }
            }
            catch (SmtpException stmpEx)
            {
                Log.Error(stmpEx, "Send Mail Error");
                //try SendMail with no attachment
                try
                {
                    mail.Attachments.Clear();
                    if (mailConfig.MailConfigSendType == MailConstants.MailConfigSendType.SYNC || mailConfig.MailConfigSendType == MailConstants.MailConfigSendType.BOTH)
                    {
                        mySmtpClient.Send(mail);
                    }

                    if (mailConfig.MailConfigSendType == MailConstants.MailConfigSendType.ASYNC || mailConfig.MailConfigSendType == MailConstants.MailConfigSendType.BOTH)
                    {
                        mySmtpClient.SendAsync(mail, Guid.NewGuid());
                    }
                }
                catch (Exception ex)
                {
                    Log.Error(ex, "Send Mail Error");
                    return false;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Send Mail Error");
                return false;
            }

            return true;
        }

        public bool SendMailGoogle(List<string> toEmails, List<string> ccEmails, List<string> bccEmail, string title, string body)
        {
            if (!mailConfig.MailConfigEnable)
                return true;
            SmtpClient mySmtpClient = new SmtpClient();
            MailMessage mail = new MailMessage();
            try
            {
                mail.BodyEncoding = System.Text.Encoding.UTF8;
                mail.Priority = MailPriority.Normal;

                mySmtpClient.UseDefaultCredentials = false;
                mySmtpClient.EnableSsl = mailConfig.MailConfigSsl;
                mail.From = new MailAddress(mailConfig.MailConfigFrom, mailConfig.MailConfigUser);

                mySmtpClient.Timeout = 10000;

                // Set port
                int port;
                if (int.TryParse(mailConfig.MailConfigPort, out port))
                {
                    mySmtpClient.Port = port;
                }

                // Set host
                if (!string.IsNullOrEmpty(mailConfig.MailConfigSmtp))
                {
                    mySmtpClient.Host = mailConfig.MailConfigSmtp;
                }

                // Set credential password
                if (!string.IsNullOrEmpty(mailConfig.MailConfigPassword) && !string.IsNullOrEmpty(mailConfig.MailConfigFrom))
                {
                    mySmtpClient.Credentials = new NetworkCredential(mailConfig.MailConfigFrom, mailConfig.MailConfigPassword);
                }
                if (toEmails != null && toEmails.Count > 0)
                {
                    foreach (string email in toEmails)
                    {
                        if (IsValidEmail(email))
                        {
                            mail.To.Add(email);
                        }
                    }
                }

                if (ccEmails != null && ccEmails.Count > 0)
                {
                    foreach (string email in ccEmails)
                    {
                        if (IsValidEmail(email))
                        {
                            mail.CC.Add(email);
                        }
                    }
                }

                if (bccEmail != null && bccEmail.Count > 0)
                {
                    foreach (string email in bccEmail)
                    {
                        if (IsValidEmail(email))
                        {
                            mail.Bcc.Add(email);
                        }
                    }
                }

                mail.Subject = title;
                mail.Body = body;

                mail.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(body, new ContentType("text/html")));

                // Log
                Log.Information("MainConfig : " + JsonSerializer.Serialize(mailConfig));
                Log.Information("toEmails : " + JsonSerializer.Serialize(toEmails));

                if (mailConfig.MailConfigSendType == MailConstants.MailConfigSendType.SYNC || mailConfig.MailConfigSendType == MailConstants.MailConfigSendType.BOTH)
                {
                    mySmtpClient.Send(mail);
                }

                if (mailConfig.MailConfigSendType == MailConstants.MailConfigSendType.ASYNC || mailConfig.MailConfigSendType == MailConstants.MailConfigSendType.BOTH)
                {
                    mySmtpClient.SendAsync(mail, Guid.NewGuid());
                }
            }
            catch (SmtpException stmpEx)
            {

                Log.Error(stmpEx, "Send Mail Error");
                //try SendMail with no attachment
                try
                {
                    mail.Attachments.Clear();
                    if (mailConfig.MailConfigSendType == MailConstants.MailConfigSendType.SYNC || mailConfig.MailConfigSendType == MailConstants.MailConfigSendType.BOTH)
                    {
                        mySmtpClient.Send(mail);
                    }

                    if (mailConfig.MailConfigSendType == MailConstants.MailConfigSendType.ASYNC || mailConfig.MailConfigSendType == MailConstants.MailConfigSendType.BOTH)
                    {
                        mySmtpClient.SendAsync(mail, Guid.NewGuid());
                    }
                }
                catch (Exception ex)
                {
                    Log.Error(ex, "Send Mail Error");
                    return false;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Send Mail Error");
                return false;
            }

            return true;
        }


        public bool SendMailGoogleWithQRCode(List<string> toEmails, List<string> ccEmails, List<string> bccEmail, string title, string body, string base64Image)
        {
            if (!mailConfig.MailConfigEnable)
                return true;
            SmtpClient mySmtpClient = new SmtpClient();
            MailMessage mail = new MailMessage();
            try
            {
                mail.BodyEncoding = System.Text.Encoding.UTF8;
                mail.Priority = MailPriority.Normal;

                mySmtpClient.UseDefaultCredentials = false;
                mySmtpClient.EnableSsl = mailConfig.MailConfigSsl;
                mail.From = new MailAddress(mailConfig.MailConfigFrom, mailConfig.MailConfigUser);

                mySmtpClient.Timeout = 10000;

                // Set port
                int port;
                if (int.TryParse(mailConfig.MailConfigPort, out port))
                {
                    mySmtpClient.Port = port;
                }

                // Set host
                if (!string.IsNullOrEmpty(mailConfig.MailConfigSmtp))
                {
                    mySmtpClient.Host = mailConfig.MailConfigSmtp;
                }

                // Set credential password
                if (!string.IsNullOrEmpty(mailConfig.MailConfigPassword) && !string.IsNullOrEmpty(mailConfig.MailConfigFrom))
                {
                    mySmtpClient.Credentials = new NetworkCredential(mailConfig.MailConfigFrom, mailConfig.MailConfigPassword);
                }
                if (toEmails != null && toEmails.Count > 0)
                {
                    foreach (string email in toEmails)
                    {
                        if (IsValidEmail(email))
                        {
                            mail.To.Add(email);
                        }
                    }
                }

                if (ccEmails != null && ccEmails.Count > 0)
                {
                    foreach (string email in ccEmails)
                    {
                        if (IsValidEmail(email))
                        {
                            mail.CC.Add(email);
                        }
                    }
                }

                if (bccEmail != null && bccEmail.Count > 0)
                {
                    foreach (string email in bccEmail)
                    {
                        if (IsValidEmail(email))
                        {
                            mail.Bcc.Add(email);
                        }
                    }
                }

                mail.Subject = title;
                mail.Body = body;

                if (!string.IsNullOrEmpty(base64Image))
                {
                    var imageData = Convert.FromBase64String(base64Image);
                    var contentId = "imageUniqueId";
                    var linkedResource = new LinkedResource(new MemoryStream(imageData), "image/jpeg");
                    linkedResource.ContentId = contentId;
                    linkedResource.TransferEncoding = TransferEncoding.Base64;

                    var htmlView = AlternateView.CreateAlternateViewFromString(body, null, "text/html");
                    htmlView.LinkedResources.Add(linkedResource);
                    mail.AlternateViews.Add(htmlView);
                }

                // Log
                Log.Information("MainConfig : " + JsonSerializer.Serialize(mailConfig));
                Log.Information("toEmails : " + JsonSerializer.Serialize(toEmails));

                if (mailConfig.MailConfigSendType == MailConstants.MailConfigSendType.SYNC || mailConfig.MailConfigSendType == MailConstants.MailConfigSendType.BOTH)
                {
                    mySmtpClient.Send(mail);
                }

                if (mailConfig.MailConfigSendType == MailConstants.MailConfigSendType.ASYNC || mailConfig.MailConfigSendType == MailConstants.MailConfigSendType.BOTH)
                {
                    mySmtpClient.SendAsync(mail, Guid.NewGuid());
                }
            }
            catch (SmtpException stmpEx)
            {

                Log.Error(stmpEx, "Send Mail Error");
                //try SendMail with no attachment
                try
                {
                    mail.Attachments.Clear();
                    if (mailConfig.MailConfigSendType == MailConstants.MailConfigSendType.SYNC || mailConfig.MailConfigSendType == MailConstants.MailConfigSendType.BOTH)
                    {
                        mySmtpClient.Send(mail);
                    }

                    if (mailConfig.MailConfigSendType == MailConstants.MailConfigSendType.ASYNC || mailConfig.MailConfigSendType == MailConstants.MailConfigSendType.BOTH)
                    {
                        mySmtpClient.SendAsync(mail, Guid.NewGuid());
                    }
                }
                catch (Exception ex)
                {
                    Log.Error(ex, "Send Mail Error");
                    return false;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Send Mail Error");
                return false;
            }

            return true;
        }


        public string GetContentTypeFromFileName(string fileName)
        {
            if (string.IsNullOrEmpty(fileName)) return string.Empty;

            try
            {
                string[] temp = fileName.Split(new string[] { ";#" }, StringSplitOptions.RemoveEmptyEntries);
                if (temp.Length > 0)
                {
                    return temp[1];
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "GetContentTypeFromFileName Error");
            }

            return string.Empty;
        }

        public string GetFileNameForDisplay(string fileName)
        {
            if (string.IsNullOrEmpty(fileName)) return string.Empty;

            try
            {
                string[] temp = fileName.Split(new string[] { ";#" }, StringSplitOptions.RemoveEmptyEntries);
                if (temp.Length > 0)
                {
                    return temp[0];
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "GetFileNameForDisplay Error");
            }

            return string.Empty;
        }

        private bool IsValidEmail(string emailAddress)
        {
            try
            {
                MailAddress m = new MailAddress(emailAddress);
                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }

    }

    public class EmailConfiguration
    {
        public bool MailConfigEnable { get; set; }
        public string MailConfigSmtp { get; set; }
        public string MailConfigPort { get; set; }
        public bool MailConfigSsl { get; set; }
        public string MailConfigSendType { get; set; }
        public string MailConfigFrom { get; set; }
        public string MailConfigUser { get; set; }
        public string MailConfigPassword { get; set; }
        public string MailConfigTemplate { get; set; }
    }
}