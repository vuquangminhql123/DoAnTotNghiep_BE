﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DigitalID.Business
{
    public interface IEmailHandler
    {       
        bool SendMailExchange(List<string> toEmails, List<string> ccEmails, List<string> bccEmail, string title, string body, Dictionary<string, byte[]> fileAttach);
               
        bool SendMailGoogle(List<string> toEmails, List<string> ccEmails, List<string> bccEmail, string title, string body);

        bool SendMailGoogleWithQRCode(List<string> toEmails, List<string> ccEmails, List<string> bccEmail, string title, string body, string base64Image);
    }
}
