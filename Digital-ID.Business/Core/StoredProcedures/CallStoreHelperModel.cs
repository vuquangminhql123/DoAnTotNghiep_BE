﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace DigitalID.Business
{
    public class CallStoreHelperModel
    {
        public DataTable Data { get; set; }
        public bool Success { get; set; }
        public string Message { get; set; }
    }
}
