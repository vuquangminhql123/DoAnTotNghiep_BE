﻿using DigitalID.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace DigitalID.Business
{
    public interface ICallStoreHelper
    {
        Response CallStoreWithStartAndEndDateAsync(string storeName, DateTime startDate, DateTime endDate);
    }
}
