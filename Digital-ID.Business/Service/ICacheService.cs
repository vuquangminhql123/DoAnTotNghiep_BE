﻿using System;
using System.Threading.Tasks;

namespace DigitalID.Business
{
    public interface ICacheService
    {
        Task<TItem> GetOrCreate<TItem>(string key, Func<Task<TItem>> createItem);
        void Remove(string key);
    }
}