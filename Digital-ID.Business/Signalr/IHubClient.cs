﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DigitalID.Business
{
   public interface IHubClient
    {
        Task BroadcastMessage(MessageInstance msg);
    }
}
