﻿using System;
using System.Collections.Generic;
using System.Text;
using DigitalID.Data;

namespace DigitalID.Business
{
    public class MessageInstance
    {
        public string Timestamp { get; set; }
        public string From { get; set; }
        public List<Notification> ListNotifications { get; set; }
    }
}
