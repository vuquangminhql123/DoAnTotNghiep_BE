﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DigitalID.Business
{
    public class CatalogItemCollection
    {
        private readonly ICatalogItemHandler _catalogItemHandler;
        private HashSet<CatalogItemModel> collection;
        // private static readonly CatalogItemCollection _instance = new CatalogItemCollection(_catalogItemHandler);
        // public static CatalogItemCollection Instance { get { return _instance; } }

        public CatalogItemCollection(ICatalogItemHandler catalogItemHandler)
        {
            _catalogItemHandler = catalogItemHandler;
            LoadToHashSet();
        }

        public void LoadToHashSet()
        {
            collection = new HashSet<CatalogItemModel>();

            // Query to list
            var listResponse = _catalogItemHandler.GetAll();

            // Add to hashset

            foreach (var response in listResponse.Data)
            {
                collection.Add(response);
            }
        }
        public CatalogItemModel GetById(Guid id)
        {
            var result = collection.FirstOrDefault(u => u.CatalogItemId == id);
            return result;
        }
    }
}
