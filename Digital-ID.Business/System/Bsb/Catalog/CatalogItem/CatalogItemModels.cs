﻿using System;
using System.Collections.Generic;

namespace DigitalID.Business
{
    #region Main  

    public class BaseCatalogItemModel
    {
        public Guid CatalogItemId { get; set; }
        public string Name { get; set; }
        public int Order { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public Guid TermId { get; set; }
        public Guid? ParentTermId { get; set; }
        public Guid CatalogMasterId { get; set; }
        public bool Status { get; set; }
        public int Level { get; set; }
    }
    public class CatalogItemModel : BaseCatalogItemModel
    {
        public DateTime CreatedOnDate { get; set; }
        public DateTime LastModifiedOnDate { get; set; }
        public BaseUserModel CreatedByUser { get; set; }
        public BaseUserModel LastModifiedByUser { get; set; }
        //Plus
        public List<CatalogItemAttributeModel> DataAttribute { get; set; }

        public string IdPath { get; set; }
        public string Path { get; set; }
    }
    public class CatalogItemTreeModel : BaseCatalogItemModel
    {
        public DateTime CreatedOnDate { get; set; }
        public DateTime LastModifiedOnDate { get; set; }
        public BaseUserModel CreatedByUser { get; set; }
        public BaseUserModel LastModifiedByUser { get; set; }
        //Plus
        public List<CatalogItemTreeModel> SubChild { get; set; }
        public string IdPath { get; set; }
        public string Path { get; set; }
        public string[] PathList { get; set; }
    }
    public class CatalogItemCreateRequestModel
    { 
        public string Name { get; set; }
        public string Code { get; set; }
        public int Order { get; set; }
        public string Description { get; set; }
        public Guid TermId { get; set; }
        public Guid? ParentTermId { get; set; }
        public Guid CatalogMasterId { get; set; }
        public bool Status { get; set; }
        //
        public Guid? UserId { get; set; }
        //Plus
        public List<CatalogItemAttributeModel> DataAttribute { get; set; }
    }

    public class CatalogItemUpdateRequestModel
    {

        public Guid CatalogItemId { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public int Order { get; set; }
        public string Description { get; set; }
        public Guid TermId { get; set; }
        public Guid? ParentTermId { get; set; }
        public Guid CatalogMasterId { get; set; }// Không cho phép sửa 
        public bool Status { get; set; }
        //
        public Guid? UserId { get; set; }
        //Plus
        public List<CatalogItemAttributeModel> DataAttribute { get; set; }
    }

    public class CatalogItemQueryFilter
    {
        public string Name { get; set; }
        public bool? Status { get; set; }
        public int? PageSize { get; set; }
        public int? PageNumber { get; set; }
        public string TextSearch { get; set; }
        public int? Level { get; set; }
        public Guid? ParentTermId { get; set; }
        public List<DataAttribute> SearchList { get; set; }
        public Guid? CatalogItemId { get; set; }
        public Guid? MappedTermId { get; set; }
        public string Code { get; set; }
        public Guid? CatalogMasterId { get; set; }
        public string CatalogMasterCode { get; set; }
        public bool IsFullDetail { get; set; }
        public CatalogItemQueryOrder Order { get; set; }
        public CatalogItemQueryFilter()
        {
            //PageSize = 0;
            PageNumber = 1;
            TextSearch = string.Empty;
            Order = CatalogItemQueryOrder.CREATE_DATE_ASC;
        }
    }

    public class CatalogItemDeleteResponseModel
    {
        public BaseCatalogItemModel Model { get; set; }
        public int Result { get; set; }
        public string Message { get; set; }
    }
    public class CatalogItemDeleteRequestModel
    {
        public IList<Guid> ListId { get; set; }
    }
    public enum CatalogItemQueryOrder
    {
        ID_DESC,
        ID_ASC,
        ORDER_DESC,
        ORDER_ASC,
        CREATE_DATE_ASC,
        MODIFIED_DATE_ASC,
        CREATE_DATE_DESC,
        MODIFIED_DATE_DESC,
    }
    #endregion
}
