﻿using DigitalID.Data;
using Newtonsoft.Json;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DigitalID.Business
{
    public class DbCatalogItemHandler : ICatalogItemHandler
    {
        private Guid? _applicationId { get; set; }
        private Guid? _userId { get; set; }
        private readonly IMetadataFieldHandler _metadataFieldHandler;

        public DbCatalogItemHandler(IMetadataFieldHandler metadataFieldHandler)
        {
            _metadataFieldHandler = metadataFieldHandler;
        }

        public void init(Guid applicationId, Guid userId)
        {
            _applicationId = applicationId;
            _userId = userId;
        }

        private CatalogItemModel ConvertData(CatalogItem model, List<CatalogItemAttribute> listAttribute, bool isFullDetail)
        {
            CatalogItemModel result = new CatalogItemModel()
            {
                CreatedByUser = new BaseUserModel() { Id = model.CreatedByUserId },
                LastModifiedByUser = new BaseUserModel() { Id = model.LastModifiedByUserId },
                CreatedOnDate = model.CreatedOnDate,
                LastModifiedOnDate = model.LastModifiedOnDate,
                Code = model.Code,
                Order = model.Order,
                TermId = model.MappedTermId,
                Name = model.Name,
                Description = model.Description,
                CatalogItemId = model.CatalogItemId,
                Status = model.Status,
            };
            if (model.MappedTerm != null)
            {

                result.ParentTermId = model.MappedTerm.ParentId;
            }
            result.DataAttribute = new List<CatalogItemAttributeModel>();
            if (listAttribute != null)
            {
                foreach (var a in listAttribute)
                {
                    result.DataAttribute.Add(ConvertData(a, isFullDetail));
                }
            }
            return result;
        }

        private CatalogItemAttributeModel ConvertData(CatalogItemAttribute model, bool isFullDetail)
        {
            var metadatafieldCol = new MetadataFieldCollection(_metadataFieldHandler);
            var temp = metadatafieldCol.GetById(model.MetadataFieldId);
            CatalogItemAttributeModel result = new CatalogItemAttributeModel()
            {
                Key = model.MetadataFieldCode,
                Name = temp.Name,
                CatalogItemAttributeId = model.CatalogItemAttributeId,
                Code = temp.Code,
                DataType = model.DataType,
            };
            if (isFullDetail)
            {
                result.NvarcharValue = model.NvarcharValue;
                result.VarcharValue = model.VarcharValue;
                result.BitValue = model.BitValue;
                result.IntValue = model.IntValue;
                result.DatetimeValue = model.DatetimeValue;
                result.TimeValue = model.TimeValue;
                result.DateValue = model.DateValue;
                result.MonthValue = model.MonthValue;
                result.YearValue = model.YearValue;
                result.GuidValue = model.GuidValue;
                result.JsonValue = model.JsonValue;
                result.JsonGuidValue = model.JsonGuidValue;
                result.TypeDateTime = model.TypeDateTime;
            }
            else
            {
                switch (model.DataType)
                {
                    #region NvarcharValue
                    case "NvarcharValue":
                        {
                            result.Value = model.NvarcharValue;
                            break;
                        }
                    #endregion
                    #region VarcharValue
                    case "VarcharValue":
                        {
                            result.Value = model.VarcharValue;
                            break;
                        }
                    #endregion
                    #region BitValue
                    case "BitValue":
                        {
                            if (model.BitValue.HasValue && model.BitValue.Value)
                            {
                                result.Value = "đúng";
                            }
                            else
                            {
                                result.Value = "sai";
                            }

                            break;
                        }
                    #endregion
                    #region IntValue
                    case "IntValue":
                        {
                            if (model.IntValue.HasValue)
                            {
                                result.Value = model.IntValue.ToString();
                            }
                            else
                            {
                                result.Value = "";
                            }
                            break;
                        }
                    #endregion
                    #region DatetimeValue
                    case "DatetimeValue":
                        {
                            if (model.DatetimeValue.HasValue)
                            {
                                result.Value = String.Format("{0:dd/MM/yyyy hh:mm:ss}", model.DatetimeValue.Value);
                            }
                            else
                            {
                                result.Value = "";
                            }
                            break;
                        }
                    #endregion
                    #region TimeValue
                    case "TimeValue":
                        {
                            if (model.TimeValue.HasValue)
                            {
                                result.Value = String.Format("{0:hh:mm:ss}", model.TimeValue.Value);
                            }
                            else
                            {
                                result.Value = "";
                            }
                            break;
                        }
                    #endregion
                    #region DateValue
                    case "DateValue":
                        {
                            if (model.DateValue.HasValue)
                            {
                                if (model.TypeDateTime.HasValue)
                                {
                                    switch (model.TypeDateTime.Value)
                                    {
                                        case 0:
                                            {
                                                result.Value = String.Format("{0:dd/MM/yyyy }", model.DateValue.Value);
                                                break;
                                            }
                                        case 1:
                                            {
                                                result.Value = String.Format("{0:MM/yyyy }", model.DateValue.Value);
                                                break;
                                            }
                                        case 2:
                                            {
                                                result.Value = String.Format("{0:yyyy }", model.DateValue.Value);
                                                break;
                                            }
                                        default:
                                            result.Value = String.Format("{0:yyyy }", model.DateValue.Value);
                                            break;
                                    }
                                }
                                else
                                {
                                    result.Value = String.Format("{0:dd/MM/yyyy }", model.DateValue.Value);
                                }
                            }
                            else
                            {
                                result.Value = "";
                            }
                            break;
                        }
                    #endregion
                    #region MonthValue
                    case "MonthValue":
                        {
                            if (model.MonthValue.HasValue)
                            {
                                result.Value = String.Format("{0:MM/yyyy}", model.MonthValue.Value);
                            }
                            else
                            {
                                result.Value = "";
                            }
                            break;
                        }
                    #endregion
                    #region YearValue
                    case "YearValue":
                        {
                            if (model.YearValue.HasValue)
                            {
                                result.Value = String.Format("{0:yyyy}", model.YearValue.Value);
                            }
                            else
                            {
                                result.Value = "";
                            }
                            break;
                        }
                    #endregion
                    #region GuidValue
                    case "GuidValue":
                        {
                            try
                            {
                                result.Value = metadatafieldCol.GetById(model.GuidValue.Value).Name;
                            }
                            catch (Exception)
                            {
                                result.Value = "";
                            }
                            break;
                        }

                    #endregion
                    #region JsonValue
                    case "JsonValue":
                        {
                            try
                            {
                                var list = JsonConvert.DeserializeObject<List<string>>(model.JsonValue);

                                result.Value = string.Join(", ", list);
                            }
                            catch (Exception)
                            {
                                result.Value = "";
                            }
                            break;
                        }

                    #endregion
                    #region JsonGuidValue
                    case "JsonGuidValue":
                        {
                            try
                            {
                                var list = JsonConvert.DeserializeObject<List<Guid>>(model.JsonGuidValue);
                                var listName = new List<string>();
                                foreach (var item in list)
                                {
                                    try
                                    {
                                        var value = metadatafieldCol.GetById(item).Name;
                                        listName.Add(value);
                                    }
                                    catch (Exception)
                                    {
                                        throw;
                                    }
                                }

                                result.Value = string.Join(", ", listName);
                            }
                            catch (Exception)
                            {
                                result.Value = "";
                            }
                            break;
                        }

                    #endregion
                    default:
                        break;
                }
            }
            return result;
        }

        #region CRUD
        public OldResponse<IList<CatalogItemModel>> GetAll()
        {
            try
            {
                // Create result variable
                var result = new OldResponse<IList<CatalogItemModel>>(0, string.Empty, new List<CatalogItemModel>(), 0, 0);

                using (var unitOfWork = new UnitOfWork())
                {
                    var datas = unitOfWork.GetRepository<CatalogItem>().GetAll();
                    var fullQueryData = datas.ToList();
                    // Convert Data
                    if (fullQueryData != null && fullQueryData.Count() >= 0)
                    {
                        result.Message = "Success get all";
                        result.Status = 1;
                        result.DataCount = fullQueryData.Count();
                        result.TotalCount = fullQueryData.Count(); ;
                        foreach (var item in fullQueryData)
                        {
                            result.Data.Add(ConvertData(item, null, false));
                        }
                    }
                    return result;
                }
            }
            catch (Exception ex)
            {
                Log.Error("Success get all", ex);
                return new OldResponse<IList<CatalogItemModel>>(-1, "Success get all", null, 0, 0);
            }
        }
        public OldResponse<IList<CatalogItemModel>> GetFilter(CatalogItemQueryFilter filter)
        {
            try
            {
                // Create result variable
                var result = new OldResponse<IList<CatalogItemModel>>(0, string.Empty, new List<CatalogItemModel>(), 0, 0);

                using (var unitOfWork = new UnitOfWork())
                {
                    var datas = unitOfWork.GetRepository<CatalogItem>().GetAll();
                    //filter
                    #region filter
                    if (filter.MappedTermId.HasValue)
                    {
                        datas = from dt in datas
                                where dt.MappedTermId == filter.MappedTermId.Value
                                select dt;
                    }
                    //by CatalogItemId
                    if (filter.CatalogItemId.HasValue)
                    {
                        datas = from dt in datas
                                where dt.CatalogItemId == filter.CatalogItemId.Value
                                select dt;
                    }

                    if (filter.CatalogMasterId.HasValue)
                    {
                        datas = from dt in datas
                                join term in unitOfWork.GetRepository<TaxonomyTerm>().GetAll()
                                on dt.MappedTermId equals term.TermId
                                join cm in unitOfWork.GetRepository<CatalogMaster>().GetAll()
                                on term.VocabularyId equals cm.MappedVocabularyId
                                where cm.CatalogMasterId == filter.CatalogMasterId.Value
                                select dt;
                    }
                    var HasFilterCode = false;
                    if (!string.IsNullOrEmpty(filter.CatalogMasterCode))
                    {
                        HasFilterCode = true;
                        datas = from dt in datas
                                join term in unitOfWork.GetRepository<TaxonomyTerm>().GetAll()
                                on dt.MappedTermId equals term.TermId
                                join cm in unitOfWork.GetRepository<CatalogMaster>().GetAll()
                                on term.VocabularyId equals cm.MappedVocabularyId
                                where cm.Code == filter.CatalogMasterCode
                                select dt;
                    }
                    //by Status
                    if (filter.Status.HasValue)
                    {
                        datas = from dt in datas
                                where dt.Status == filter.Status
                                select dt;
                    }
                    //by level
                    if (filter.Level.HasValue)
                    {
                        datas = from dt in datas
                                join term in unitOfWork.GetRepository<TaxonomyTerm>().GetAll()
                                on dt.MappedTermId equals term.TermId
                                where term.Level == filter.Level
                                select dt;
                    }
                    if (!string.IsNullOrEmpty(filter.ParentTermId.ToString()) && filter.ParentTermId != Guid.Empty)
                    {
                        datas = from dt in datas
                                join term in unitOfWork.GetRepository<TaxonomyTerm>().GetAll()
                                on dt.MappedTermId equals term.TermId
                                where term.ParentId == filter.ParentTermId
                                select dt;
                    }
                    //by Code
                    if (!string.IsNullOrEmpty(filter.Code))
                    {
                        datas = from dt in datas
                                where dt.Code == filter.Code
                                select dt;
                    }
                    //by  TextSearch
                    if (!string.IsNullOrEmpty(filter.TextSearch))
                    {
                        filter.TextSearch = filter.TextSearch.ToLower().Trim();
                        datas = from dt in datas
                                where dt.Name.ToLower().Contains(filter.TextSearch)
                                select dt;
                    }

                    // xử lý dữ liệu meta
                    if (filter.SearchList != null && filter.SearchList.Count > 0)
                    {

                        var predicate = LinqKit.PredicateBuilder.New<CatalogItemAttribute>();
                        //predicate.Start(u => true);
                        #region Build Query
                        foreach (var item in filter.SearchList)
                        {
                            switch (item.DataType)
                            {
                                case "nvarchar(512)":
                                    {
                                        switch (item.CompareExpression)
                                        {
                                            case 0:
                                                {
                                                    if (true)//!item.IsAndClause)
                                                    {
                                                        predicate = predicate.Or(u => u.NvarcharValue == item.NvarcharValue);
                                                    }
                                                    // else
                                                    // {
                                                    //     predicate = predicate.And(u => u.NvarcharValue == item.NvarcharValue);
                                                    // }
                                                    break;
                                                }
                                            default:
                                                break;
                                        }
                                        break;
                                    }
                                case "varchar(512)":
                                    {
                                        switch (item.CompareExpression)
                                        {
                                            case 0:
                                                {
                                                    if (true)//!item.IsAndClause)
                                                    {
                                                        predicate = predicate.Or(u => u.VarcharValue == item.VarcharValue);
                                                    }
                                                    // else
                                                    // {
                                                    //     predicate = predicate.And(u => u.VarcharValue == item.VarcharValue);
                                                    // }
                                                    break;
                                                }
                                            default:
                                                break;
                                        }
                                        break;
                                    }
                                case "int":
                                    {
                                        switch (item.CompareExpression)
                                        {
                                            case -11:
                                                {
                                                    if (true)//!item.IsAndClause)
                                                    {
                                                        predicate = predicate.Or(u => u.IntValue <= item.IntValue);
                                                    }
                                                    // else
                                                    // {
                                                    //     predicate = predicate.And(u => u.IntValue <= item.IntValue);
                                                    // }
                                                    break;
                                                }
                                            case -10:
                                                {
                                                    if (true)//!item.IsAndClause)
                                                    {
                                                        predicate = predicate.Or(u => u.IntValue < item.IntValue);
                                                    }
                                                    // else
                                                    // {
                                                    //     predicate = predicate.And(u => u.IntValue < item.IntValue);
                                                    // }
                                                    break;
                                                }
                                            case 0:
                                                {
                                                    if (true)//!item.IsAndClause)
                                                    {
                                                        predicate = predicate.Or(u => u.IntValue == item.IntValue);
                                                    }
                                                    // else
                                                    // {
                                                    //     predicate = predicate.And(u => u.IntValue == item.IntValue);
                                                    // }
                                                    break;
                                                }
                                            case 10:
                                                {
                                                    if (true)//!item.IsAndClause)
                                                    {
                                                        predicate = predicate.Or(u => u.IntValue > item.IntValue);
                                                    }
                                                    // else
                                                    // {
                                                    //     predicate = predicate.And(u => u.IntValue > item.IntValue);
                                                    // }
                                                    break;
                                                }
                                            case 11:
                                                {
                                                    if (true)//!item.IsAndClause)
                                                    {
                                                        predicate = predicate.Or(u => u.IntValue >= item.IntValue);
                                                    }
                                                    // else
                                                    // {
                                                    //     predicate = predicate.And(u => u.IntValue >= item.IntValue);
                                                    // }
                                                    break;
                                                }
                                            default:
                                                break;
                                        }
                                        break;
                                    }
                                case "uniqueidentifier":
                                    {
                                        switch (item.CompareExpression)
                                        {

                                            case 0:
                                                {
                                                    if (true)//!item.IsAndClause)
                                                    {
                                                        predicate = predicate.Or(u => u.GuidValue == item.GuidValue);
                                                    }
                                                    // else
                                                    // {
                                                    //     predicate = predicate.And(u => u.GuidValue == item.GuidValue);
                                                    // }
                                                    break;
                                                }

                                            default:
                                                break;
                                        }
                                        break;
                                    }
                                case "datetime":
                                    {
                                        switch (item.CompareExpression)
                                        {
                                            case -11:
                                                {
                                                    if (true)//!item.IsAndClause)
                                                    {
                                                        predicate = predicate.Or(u => u.DatetimeValue.Value.Date <= item.DatetimeValue.Value.Date);
                                                    }
                                                    // else
                                                    // {
                                                    //     predicate = predicate.And(u => u.DatetimeValue.Value.Date <= item.DatetimeValue.Value.Date);
                                                    // }
                                                    break;
                                                }
                                            case -10:
                                                {
                                                    if (true)//!item.IsAndClause)
                                                    {
                                                        predicate = predicate.Or(u => u.DatetimeValue.Value.Date < item.DatetimeValue.Value.Date);
                                                    }
                                                    // else
                                                    // {
                                                    //     predicate = predicate.And(u => u.DatetimeValue.Value.Date < item.DatetimeValue.Value.Date);
                                                    // }
                                                    break;
                                                }
                                            case 0:
                                                {
                                                    if (true)//!item.IsAndClause)
                                                    {
                                                        predicate = predicate.Or(u => u.DatetimeValue.Value.Date == item.DatetimeValue.Value.Date);
                                                    }
                                                    // else
                                                    // {
                                                    //     predicate = predicate.And(u => u.DatetimeValue.Value.Date == item.DatetimeValue.Value.Date);
                                                    // }
                                                    break;
                                                }
                                            case 10:
                                                {
                                                    if (true)//!item.IsAndClause)
                                                    {
                                                        predicate = predicate.Or(u => u.DatetimeValue.Value.Date > item.DatetimeValue.Value.Date);
                                                    }
                                                    // else
                                                    // {
                                                    //     predicate = predicate.And(u => u.DatetimeValue.Value.Date > item.DatetimeValue.Value.Date);
                                                    // }
                                                    break;
                                                }
                                            case 11:
                                                {
                                                    if (true)//!item.IsAndClause)
                                                    {
                                                        predicate = predicate.Or(u => u.DatetimeValue.Value.Date >= item.DatetimeValue.Value.Date);
                                                    }
                                                    // else
                                                    // {
                                                    //     predicate = predicate.And(u => u.DatetimeValue.Value.Date >= item.DatetimeValue.Value.Date);
                                                    // }
                                                    break;
                                                }
                                            default:
                                                break;
                                        }
                                        break;
                                    }
                                case "bit":
                                    {
                                        switch (item.CompareExpression)
                                        {

                                            case 0:
                                                {
                                                    if (true)//!item.IsAndClause)
                                                    {
                                                        predicate = predicate.Or(u => u.BitValue == item.BitValue);
                                                    }
                                                    // else
                                                    // {
                                                    //     predicate = predicate.And(u => u.BitValue == item.BitValue);
                                                    // }
                                                    break;
                                                }
                                            default:
                                                break;
                                        }
                                        break;
                                    }
                                default:
                                    break;
                            }
                        }
                        #endregion
                        var fullquery = unitOfWork.GetRepository<CatalogItemAttribute>().GetAll().Where(predicate);
                        //Process And clause onlu
                        if (filter.SearchList.Count > 1 && filter.SearchList[1].IsAndClause)
                        {
                            var fullquery2 = from sh in fullquery
                                             group sh by new { sh.CatalogItemId } into grp
                                             where grp.Count() == filter.SearchList.Count
                                             select grp.Key.CatalogItemId;
                        }
                        else
                        {
                            datas = from dt in datas
                                    join meta in fullquery
                                    on dt.CatalogItemId equals meta.CatalogItemId
                                    group dt by dt.CatalogItemId into DistrictCatalogItem
                                    select DistrictCatalogItem.FirstOrDefault();
                        }
                        datas.Distinct();
                    }

                    #endregion
                    var totalCount = datas.Count();
                    //Order
                    #region ordering
                    switch (filter.Order)
                    {
                        case CatalogItemQueryOrder.CREATE_DATE_ASC:
                            datas = datas.OrderBy(sp => sp.CreatedOnDate);
                            break;
                        case CatalogItemQueryOrder.CREATE_DATE_DESC:
                            datas = datas.OrderByDescending(sp => sp.CreatedOnDate);
                            break;
                        case CatalogItemQueryOrder.MODIFIED_DATE_ASC:
                            datas = datas.OrderBy(sp => sp.LastModifiedOnDate);
                            break;
                        case CatalogItemQueryOrder.MODIFIED_DATE_DESC:
                            datas = datas.OrderByDescending(sp => sp.LastModifiedOnDate);
                            break;
                        case CatalogItemQueryOrder.ORDER_ASC:
                            datas = datas.OrderBy(sp => sp.Order).ThenByDescending(sp => sp.LastModifiedOnDate);
                            break;
                        case CatalogItemQueryOrder.ORDER_DESC:
                            datas = datas.OrderByDescending(sp => sp.Order).ThenByDescending(sp => sp.LastModifiedOnDate);
                            break;
                        case CatalogItemQueryOrder.ID_ASC:
                            datas = datas.OrderBy(sp => sp.CatalogItemId);
                            break;
                        case CatalogItemQueryOrder.ID_DESC:
                            datas = datas.OrderByDescending(sp => sp.CatalogItemId);
                            break;
                        default:
                            datas = datas.OrderByDescending(sp => sp.LastModifiedOnDate);
                            break;
                    }
                    #endregion
                    // Pagination
                    #region Pagination
                    if (filter.PageSize.HasValue && filter.PageNumber.HasValue)
                    {
                        if (filter.PageSize.Value <= 0) filter.PageSize = 20;

                        filter.PageNumber = filter.PageNumber - 1;
                        //Calculate nunber of rows to skip on pagesize
                        int excludedRows = (filter.PageNumber.Value) * (filter.PageSize.Value);
                        if (excludedRows <= 0) excludedRows = 0;

                        datas = datas.Skip(excludedRows).Take(filter.PageSize.Value);
                    }
                    #endregion
                    var fullQueryData = datas.ToList();
                    // CatalogItem attributes
                    var listCatalogItemId = (from i in fullQueryData select i.CatalogItemId);
                    var listCatalogItemAttributes = (from a in unitOfWork.GetRepository<CatalogItemAttribute>().GetAll()
                                                     where listCatalogItemId.Contains(a.CatalogItemId)
                                                     select a).ToList();


                    // Convert Data
                    if (fullQueryData != null && fullQueryData.Count() >= 0)
                    {
                        result.Message = "Success get filter with filter : " + JsonConvert.SerializeObject(filter);
                        result.Status = 1;
                        result.DataCount = fullQueryData.Count();
                        result.TotalCount = totalCount;
                        foreach (var item in fullQueryData)
                        {
                            var attribute = from a in listCatalogItemAttributes
                                            where a.CatalogItemId == item.CatalogItemId
                                            select a;

                            attribute = attribute.OrderBy(s => s.MetadataFieldCode);
                            result.Data.Add(ConvertData(item, attribute.ToList(), filter.IsFullDetail));
                        }
                        if (result.DataCount == 0 && HasFilterCode)
                        {
                            result.Status = -2;
                            result.Message = "Error get filter with Code catalog: " + filter.Code;
                        }
                    }
                    else
                    {
                        result.Message = "Error get filter with filter: " + JsonConvert.SerializeObject(filter);
                        result.Status = 0;
                        result.DataCount = fullQueryData.Count();
                        result.TotalCount = totalCount;
                    }
                    return result;
                }
            }
            catch (Exception ex)
            {
                Log.Error("Error get filter with filter:", ex);
                return new OldResponse<IList<CatalogItemModel>>(-1, "Error get filter with filter: " + JsonConvert.SerializeObject(filter), null, 0, 0);
            }
        }
        public OldResponse<CatalogItemModel> GetById(Guid CatalogItemId)
        {
            var filter = new CatalogItemQueryFilter() { CatalogItemId = CatalogItemId };
            var OldResponse = GetFilter(filter);
            if (OldResponse.Status == 1)
            {
                return new OldResponse<CatalogItemModel>(1, "Success", OldResponse.Data.FirstOrDefault());
            }
            else
            {
                return new OldResponse<CatalogItemModel>(OldResponse.Status, OldResponse.Message, null);
            }
        }

        public OldResponse<CatalogItemModel> GetByCode(string code, string name)
        {
            try
            {
                // Create result variable
                using (var unitOfWork = new UnitOfWork())
                {
                    var datas = (from dt in unitOfWork.GetRepository<CatalogItem>().GetAll()
                                 join term in unitOfWork.GetRepository<TaxonomyTerm>().GetAll()
                                 on dt.MappedTermId equals term.TermId
                                 join cm in unitOfWork.GetRepository<CatalogMaster>().GetAll()
                                 on term.VocabularyId equals cm.MappedVocabularyId
                                 where cm.Code == code && dt.Name.ToLower().Contains(name)
                                 select dt).FirstOrDefault();
                    CatalogItemModel item = new CatalogItemModel();
                    item.CatalogItemId = datas.CatalogItemId;
                    item.Code = datas.Code;
                    item.Name = datas.Name;
                    item.Description = datas.Description;
                    return new OldResponse<CatalogItemModel>(0, string.Empty, item);
                }
            }
            catch (Exception ex)
            {
                Log.Error("Error get filter with Code, Name:", ex);
                return new OldResponse<CatalogItemModel>(-1, "Error get filter with Code, Name", null);
            }
        }
        public OldResponse<Guid> GetIdByCode(string catalogCode, string code)
        {
            try
            {
                // Create result variable
                using (var unitOfWork = new UnitOfWork())
                {
                    var data = (from dt in unitOfWork.GetRepository<CatalogItem>().GetAll()
                                join term in unitOfWork.GetRepository<TaxonomyTerm>().GetAll()
                                on dt.MappedTermId equals term.TermId
                                join cm in unitOfWork.GetRepository<CatalogMaster>().GetAll()
                                on term.VocabularyId equals cm.MappedVocabularyId
                                where cm.Code == catalogCode && dt.Code == code
                                select dt).FirstOrDefault();
                    return new OldResponse<Guid>(0, string.Empty, data.CatalogItemId);
                }
            }
            catch (Exception ex)
            {
                Log.Error("Error get filter with Code:", ex);
                return new OldResponse<Guid>(-1, "Error get filter with Code", Guid.Empty);
            }
        }
        //
        public OldResponse<IList<CatalogItemAttributeModel>> GetAtributeById(Guid CatalogItemId)
        {
            try
            {
                // Create result variable
                var result = new OldResponse<IList<CatalogItemAttributeModel>>(0, string.Empty, new List<CatalogItemAttributeModel>(), 0, 0);

                using (var unitOfWork = new UnitOfWork())
                {
                    var datas = unitOfWork.GetRepository<CatalogItemAttribute>().GetAll();
                    datas = from dt in datas
                            where dt.CatalogItemId == CatalogItemId
                            select dt;

                    var totalCount = datas.Count();
                    datas = datas.OrderByDescending(sp => sp.MetadataFieldCode);



                    var fullQueryData = datas.ToList();
                    // Convert Data
                    if (fullQueryData != null && fullQueryData.Count() >= 0)
                    {
                        result.Message = "Success get data atribute";
                        result.DataCount = fullQueryData.Count();
                        result.TotalCount = totalCount;
                        foreach (var item in fullQueryData)
                        {
                            result.Data.Add(ConvertData(item, true));
                        }
                    }
                    return result;
                }
            }
            catch (Exception ex)
            {
                Log.Error("Error get data atribute:", ex);
                return new OldResponse<IList<CatalogItemAttributeModel>>(-1, "Error get get data atribute", null, 0, 0);
            }
        }

        public OldResponse<CatalogItemModel> Create(CatalogItemCreateRequestModel request)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    var repo = unitOfWork.GetRepository<CatalogItem>();
                    //check unique Name: not open
                    #region check unique Name => Chuyển sang check bằng unique trong sql bảng term
                    // >>Trong một danh mục tổng, các item có cùng cha hoặc không có cha sẽ phải đặt tên khác nhau

                    var check = repo.GetMany(s => s.Name == request.Name).FirstOrDefault();
                    if (check != null)
                    {
                       Log.Error("Name:" + request.Name + "' already exist!");
                       return new OldResponse<CatalogItemModel>(0, "Tên thuộc tính: " + request.Name + "' đã tồn tại!", null);
                    }
                    // check unique Code 
                    var checkCode = repo.GetMany(s => s.Code == request.Code).FirstOrDefault();
                    if (checkCode != null)
                    {
                       Log.Error("Code:" + request.Code + "' already exist!");
                       return new OldResponse<CatalogItemModel>(0, "Mã thuộc tính: " + request.Code + "' đã tồn tại!", null);
                    }
                    #endregion

                    ///Get Info
                    var currentMaster = unitOfWork.GetRepository<CatalogMaster>().Find(request.CatalogMasterId);
                    #region Step 1. Add Taxonomy Term
                    var termRequest = new TaxonomyTermCreateRequestModel();
                    termRequest.Name = request.Name;
                    termRequest.VocabularyId = currentMaster.MappedVocabularyId;
                    termRequest.Order = request.Order;
                    termRequest.Description = request.Description;
                    if (request.ParentTermId != null && request.ParentTermId != Guid.Empty)
                    {
                        termRequest.ParentTerm = new ParentTaxonomyTermModel();
                        termRequest.ParentTerm.TermId = request.ParentTermId;
                    }
                    termRequest.CreatedByUserId = request.UserId.HasValue ? request.UserId.Value : _userId.Value;
                    DbTaxonomyTermsHandler termHandler = new DbTaxonomyTermsHandler();

                    var termRepo = termHandler.Create(termRequest);
                    if (termRepo == null || termRepo.Status < 1)
                    {
                        Log.Error("Lỗi term");
                        return new OldResponse<CatalogItemModel>(0, "Kiểm tra lại dữ liệu, có thể bị trùng tên!", null);
                    }
                    #endregion

                    #region Step 2. Add Catalog Item
                    var data = new CatalogItem();
                    data.CatalogItemId = Guid.NewGuid();
                    data.CreatedOnDate = DateTime.Now;
                    data.LastModifiedOnDate = DateTime.Now;
                    data.CreatedByUserId = request.UserId.HasValue ? request.UserId.Value : _userId.Value;
                    data.LastModifiedByUserId = request.UserId.HasValue ? request.UserId.Value : _userId.Value;
                    data.Name = request.Name;
                    data.Code = request.Code;
                    data.Order = request.Order;
                    data.Description = request.Description;
                    data.Status = request.Status;
                    data.MappedTermId = termRepo.Data.TermId;
                    #endregion
                    //exe query
                    repo.Add(data);
                    var result = unitOfWork.Save();
                    if (result >= 1)
                    {
                        //Save value CatalogItem
                        var repoAtribute = unitOfWork.GetRepository<CatalogItemAttribute>();
                        if (request.DataAttribute != null)
                        {
                            if (request.DataAttribute.Count > 0)
                            {
                                foreach (var att in request.DataAttribute)
                                {
                                    var model = new CatalogItemAttribute()
                                    {
                                        CatalogItemId = data.CatalogItemId,
                                        DataType = att.DataType,
                                        MetadataFieldCode = att.Key,
                                        IntValue = att.IntValue,
                                        NvarcharValue = att.NvarcharValue,
                                        BitValue = att.BitValue,
                                        DatetimeValue = att.DatetimeValue,
                                        GuidValue = att.GuidValue,
                                        MetadataFieldId = att.MetadataFieldId,
                                        VarcharValue = att.VarcharValue,
                                        DateValue = att.DateValue,
                                        JsonGuidValue = att.JsonGuidValue,
                                        JsonValue = att.JsonValue,
                                        MonthValue = att.MonthValue,
                                        TimeValue = att.TimeValue,
                                        TypeDateTime = att.TypeDateTime,
                                        YearValue = att.YearValue,
                                        CatalogItemAttributeId = Guid.NewGuid(),
                                        MetadataFieldName = att.Name ?? "",
                                    };
                                    repoAtribute.Add(model);
                                }
                                unitOfWork.Save();
                                //Task.Run(() => CatalogItemCollection.Instance.LoadToHashSet());
                                // CatalogItemCollection.Instance.LoadToHashSet();
                            }
                        }
                        Log.Error("create successfull with request :" + JsonConvert.SerializeObject(request));
                        return new OldResponse<CatalogItemModel>(1, "Create successfull with request :" + JsonConvert.SerializeObject(request), ConvertData(data, null, true));
                    }
                    else
                    {
                        Log.Error("SQL can not process query");
                        return new OldResponse<CatalogItemModel>(0, "SQL can not process query", null);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error("Create error", ex);
                return new OldResponse<CatalogItemModel>(-1, "Create error:" + ex.Message, null);
            }
        }

        public OldResponse<CatalogItemModel> Update(CatalogItemUpdateRequestModel request)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    //check exest
                    var repo = unitOfWork.GetRepository<CatalogItem>();
                    var current = repo.Find(request.CatalogItemId);
                    if (current == null)
                    {
                        Log.Error("Time constraint Id not found +'" + request.CatalogItemId + "'");
                        return new OldResponse<CatalogItemModel>(0, "Time constraint Id not found +'" + request.CatalogItemId + "'", null);
                    }
                    ////check new unique name
                    //if (request.Name != current.Name)
                    //{
                    //    var currentHasUniqueName = repo.GetMany(s => s.Name == request.Name).FirstOrDefault();
                    //    if (currentHasUniqueName != null)
                    //    {
                    //        Log.Error("ColumnName:" + request.Name + "' already exist!");
                    //        return new OldResponse<CatalogItemModel>(0, "ColumnName:" + request.Name + "' already exist!", null);
                    //    }
                    //}

                    ////check new unique Code
                    //if (request.Code != current.Code)
                    //{
                    //    var currentHasUniqueCode = repo.GetMany(s => s.Code == request.Code).FirstOrDefault();
                    //    if (currentHasUniqueCode != null)
                    //    {
                    //        Log.Error("Code:" + request.Code + "' already exist!");
                    //        return new OldResponse<CatalogItemModel>(0, "Code:" + request.Code + "' already exist!", null);
                    //    }
                    //}
                    #region Step 3. Update Term

                    DbTaxonomyTermsHandler termHandler = new DbTaxonomyTermsHandler();
                    var repoMapTerm = new TaxonomyTermUpdateRequestModel();
                    repoMapTerm.TermId = current.MappedTermId;
                    repoMapTerm.ParentTerm = new ParentTaxonomyTermModel();
                    if (request.ParentTermId != null)
                    {
                        if (request.ParentTermId != Guid.Empty)
                        {
                            repoMapTerm.ParentTerm.TermId = request.ParentTermId;
                        }
                    }
                    repoMapTerm.LastModifiedOnDate = DateTime.Now;
                    repoMapTerm.LastModifiedByUserId = current.LastModifiedByUserId;

                    if (!string.IsNullOrEmpty(request.Name))
                    {
                        repoMapTerm.Name = request.Name;
                    }
                    if (request.Order > -1)
                    {
                        repoMapTerm.Order = request.Order;
                    }
                    if (!string.IsNullOrEmpty(request.Description))
                    {
                        repoMapTerm.Description = request.Description;
                    }
                    var termRepo = termHandler.Update(repoMapTerm);
                    if (termRepo == null || termRepo.Status < 1)
                    {
                        Log.Error("Lỗi term");
                        return new OldResponse<CatalogItemModel>(0, "Kiểm tra lại dữ liệu, có thể bị trùng tên!", null);
                    }

                    #endregion

                    //start update 
                    current.LastModifiedOnDate = DateTime.Now;
                    current.CreatedByUserId = request.UserId.HasValue ? request.UserId.Value : _userId.Value;
                    current.LastModifiedByUserId = request.UserId.HasValue ? request.UserId.Value : _userId.Value;
                    current.Name = request.Name;
                    current.Code = request.Code;
                    current.Status = request.Status;
                    current.Order = request.Order;
                    current.Description = request.Description;
                    //exe query
                    repo.Update(current);
                    var result = unitOfWork.Save();
                    if (result >= 1)
                    {

                        if (request.DataAttribute.Count > 0)
                        {
                            //Save value CatalogItem
                            var repoAtribute = unitOfWork.GetRepository<CatalogItemAttribute>();
                            repoAtribute.Delete(sp => sp.CatalogItemId == request.CatalogItemId);
                            foreach (var atr in request.DataAttribute)
                            {

                                //check  atr.MetadataFieldId
                                var check2 = (from mftr in unitOfWork.GetRepository<MetadataFieldTemplateRelationship>().GetAll()
                                              join cm in unitOfWork.GetRepository<CatalogMaster>().GetAll()
                                              on mftr.MetadataTemplateId equals cm.MetadataTemplateId
                                              where mftr.MetadataFieldId == atr.MetadataFieldId && cm.CatalogMasterId == request.CatalogMasterId
                                              select mftr).FirstOrDefault();
                                if (check2 != null)
                                {
                                    var model = new CatalogItemAttribute()
                                    {
                                        CatalogItemId = request.CatalogItemId,
                                        DataType = atr.DataType,
                                        MetadataFieldCode = atr.Key,
                                        IntValue = atr.IntValue,
                                        NvarcharValue = atr.NvarcharValue,
                                        BitValue = atr.BitValue,
                                        DatetimeValue = atr.DatetimeValue,
                                        GuidValue = atr.GuidValue,
                                        MetadataFieldId = atr.MetadataFieldId,
                                        VarcharValue = atr.VarcharValue,
                                        DateValue = atr.DateValue,
                                        JsonGuidValue = atr.JsonGuidValue,
                                        JsonValue = atr.JsonValue,
                                        MonthValue = atr.MonthValue,
                                        TimeValue = atr.TimeValue,
                                        TypeDateTime = atr.TypeDateTime,
                                        YearValue = atr.YearValue,
                                        MetadataFieldName = atr.Name ?? "",
                                        CatalogItemAttributeId = Guid.NewGuid(),
                                    };
                                    repoAtribute.Add(model);
                                }
                            }
                            unitOfWork.Save();
                            //Task.Run(() => CatalogItemCollection.Instance.LoadToHashSet());
                            // CatalogItemCollection.Instance.LoadToHashSet();
                        }
                        Log.Error("Update successfull with request :" + JsonConvert.SerializeObject(request));
                        return new OldResponse<CatalogItemModel>(1, "Update successfull with request :" + JsonConvert.SerializeObject(request), ConvertData(current, null, true));
                    }
                    else
                    {
                        Log.Error("SQL can not process query");
                        return new OldResponse<CatalogItemModel>(0, "SQL can not process query", null);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error("Update error", ex);
                return new OldResponse<CatalogItemModel>(-1, "Update error" + ex.Message, null);
            }
        }
        public OldResponse<CatalogItemDeleteResponseModel> Delete(Guid CatalogItemId)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    var deleteResponse = new OldResponse<CatalogItemDeleteResponseModel>(1, "", new CatalogItemDeleteResponseModel());

                    deleteResponse.Data.Model = new BaseCatalogItemModel();

                    //check exitest
                    var repo = unitOfWork.GetRepository<CatalogItem>();
                    var current = repo.Find(CatalogItemId);
                    if (current == null)
                    {
                        Log.Error(" CatalogItemId not found +'" + CatalogItemId + "'");
                        deleteResponse.Message = " Không tìm thấy bản ghi +'" + CatalogItemId + "'";
                        deleteResponse.Data.Message = "Không tìm thấy bản ghi +'" + CatalogItemId + "'";
                        deleteResponse.Data.Result = -1;
                        deleteResponse.Data.Model.CatalogItemId = CatalogItemId;
                        return deleteResponse;
                    }
                    else
                    {
                        var taxonomyTermsRepo = unitOfWork.GetRepository<TaxonomyTerm>();
                        var dTaxonomyTerm = taxonomyTermsRepo.Find(current.MappedTermId);
                        if (dTaxonomyTerm != null)
                        {
                            //kiểm tra item con
                            var childTermList = (from t in unitOfWork.GetRepository<TaxonomyTerm>().GetAll()
                                                 where t.ParentId == dTaxonomyTerm.TermId
                                                 select t).FirstOrDefault();
                            if (childTermList == null)
                            {
                                //xóa attribute
                                var catalogItemAttRepo = unitOfWork.GetRepository<CatalogItemAttribute>();
                                catalogItemAttRepo.Delete(sp => sp.CatalogItemId == CatalogItemId);
                                //xóa item
                                repo.Delete(current);
                                //xóa term
                                taxonomyTermsRepo.Delete(dTaxonomyTerm);
                                if (unitOfWork.Save() >= 1)
                                {
                                    //Task.Run(() => CatalogItemCollection.Instance.LoadToHashSet());
                                    // CatalogItemCollection.Instance.LoadToHashSet();
                                    Log.Error("delete sucess");
                                    deleteResponse.Data = new CatalogItemDeleteResponseModel()
                                    {
                                        Model = new BaseCatalogItemModel { CatalogItemId = current.CatalogItemId, Name = current.Name },
                                        Result = 1,
                                        Message = "Xóa thành công"
                                    };
                                    deleteResponse.Status = 1;
                                }
                                else
                                {
                                    Log.Error("SQL can not process query");
                                    deleteResponse.Data = new CatalogItemDeleteResponseModel()
                                    {
                                        Model = new BaseCatalogItemModel { CatalogItemId = current.CatalogItemId, Name = current.Name },
                                        Result = 0,
                                        Message = "Bản ghi đang được sử dụng, không xóa được!"
                                    };
                                    deleteResponse.Message = "Bản ghi đang được sử dụng, không xóa được!";
                                    deleteResponse.Status = 0;
                                }
                            }
                            else
                            {
                                Log.Error("SQL can not process query");
                                deleteResponse.Data = new CatalogItemDeleteResponseModel()
                                {
                                    Model = new BaseCatalogItemModel { CatalogItemId = current.CatalogItemId, Name = current.Name },
                                    Result = 0,
                                    Message = "Bản ghi chứa bản ghi con!"
                                };
                                deleteResponse.Message = "Bản ghi chứa bản ghi con, không xóa được!";
                                deleteResponse.Status = 0;
                            }
                        }
                        else
                        {
                            Log.Error("SQL can not process query");
                            deleteResponse.Data = null;
                            deleteResponse.Message = "Không tìm thấy bản ghi!";
                            deleteResponse.Status = 0;
                        }
                    }
                    return deleteResponse;
                }
            }
            catch (Exception ex)
            {
                Log.Error("Delete time constraint error", ex);
                var result = new OldResponse<CatalogItemDeleteResponseModel>(1, "", new CatalogItemDeleteResponseModel());
                result.Data.Model = new BaseCatalogItemModel();
                Log.Error("Delete error");
                result.Message = "Delete error";
                result.Data.Message = "Delete error";
                result.Data.Result = -1;
                result.Data.Model.CatalogItemId = CatalogItemId;
                return result;
            }
        }

        public OldResponse<IList<CatalogItemDeleteResponseModel>> DeleteMany(IList<Guid> listCatalogItemId)
        {
            var result = new OldResponse<IList<CatalogItemDeleteResponseModel>>(1, "", new List<CatalogItemDeleteResponseModel>());
            foreach (var item in listCatalogItemId)
            {
                var deleteResult = Delete(item);
                result.Data.Add(deleteResult.Data);
            }
            return result;
        }

        #endregion

        #region  hien thi item danh muc
        /// <summary>
        /// Get filter
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        public OldResponse<List<CatalogItemModel>> GetFilterItemList(CatalogItemQueryFilter filter)
        {
            try
            {
                var result = new OldResponse<List<CatalogItemModel>>(0, string.Empty, new List<CatalogItemModel>(), 0, 0);

                using (var unitOfWork = new UnitOfWork())

                //select* from CatalogItem a join TaxonomyTerm b on a.MappedTermId = b.TermId
                //--join CatalogItemAttribute c on a.CatalogItemId = c.CatalogItemId
                //join CatalogMaster d on b.VocabularyId = d.MappedVocabularyId
                {
                    var datas = from item in unitOfWork.GetRepository<CatalogItem>().GetAll()
                                join term in unitOfWork.GetRepository<TaxonomyTerm>().GetAll()
                                on item.MappedTermId equals term.TermId
                                join cata in unitOfWork.GetRepository<CatalogMaster>().GetAll()
                                on term.VocabularyId equals cata.MappedVocabularyId
                                select new
                                {
                                    Item = item,
                                    Term = term,
                                    Cata = cata
                                };
                    //filter
                    #region filter
                    //by Mapped term Id
                    if (filter.CatalogItemId.HasValue)
                    {
                        datas = from dt in datas
                                where dt.Item.CatalogItemId == filter.CatalogItemId.Value
                                select dt;
                    } //by CatalogMasterId  
                    if (filter.CatalogMasterId.HasValue)
                    {
                        datas = from dt in datas
                                where dt.Cata.CatalogMasterId == filter.CatalogMasterId.Value
                                select dt;
                    }
                    //by CatalogMasterCode 
                    if (!string.IsNullOrEmpty(filter.CatalogMasterCode))
                    {
                        datas = from dt in datas
                                where dt.Cata.Code == filter.CatalogMasterCode
                                select dt;
                    }
                    //by Name
                    if (!string.IsNullOrEmpty(filter.Name))
                    {
                        datas = from dt in datas
                                where dt.Item.Name == filter.Name
                                select dt;
                    }
                    if (filter.Status.HasValue)
                    {
                        datas = from dt in datas
                                where dt.Item.Status == filter.Status
                                select dt;
                    }
                    //by TextSearch
                    if (!string.IsNullOrEmpty(filter.TextSearch))
                    {
                        filter.TextSearch = filter.TextSearch.ToLower().Trim();
                        datas = from dt in datas
                                where dt.Item.Name.ToLower().Contains(filter.TextSearch)
                                select dt;
                    }
                    //by level
                    if (filter.Level.HasValue)
                    {
                        datas = from dt in datas
                                where dt.Term.Level == filter.Level
                                select dt;
                    }
                    #endregion
                    var totalCount = datas.Count();
                    //Order
                    #region ordering
                    switch (filter.Order)
                    {
                        case CatalogItemQueryOrder.CREATE_DATE_ASC:
                            datas = datas.OrderBy(sp => sp.Term.CreatedOnDate);
                            break;
                        case CatalogItemQueryOrder.CREATE_DATE_DESC:
                            datas = datas.OrderByDescending(sp => sp.Term.CreatedOnDate);
                            break;
                        case CatalogItemQueryOrder.MODIFIED_DATE_ASC:
                            datas = datas.OrderBy(sp => sp.Term.LastModifiedOnDate);
                            break;
                        case CatalogItemQueryOrder.MODIFIED_DATE_DESC:
                            datas = datas.OrderByDescending(sp => sp.Term.LastModifiedOnDate);
                            break;
                        case CatalogItemQueryOrder.ORDER_ASC:
                            datas = datas.OrderBy(sp => sp.Item.Order).ThenBy(sp => sp.Item.CreatedOnDate);
                            break;
                        case CatalogItemQueryOrder.ORDER_DESC:
                            datas = datas.OrderByDescending(sp => sp.Item.Order).ThenByDescending(sp => sp.Item.CreatedOnDate);
                            break;
                        case CatalogItemQueryOrder.ID_ASC:
                            datas = datas.OrderBy(sp => sp.Item.CatalogItemId);
                            break;
                        case CatalogItemQueryOrder.ID_DESC:
                            datas = datas.OrderByDescending(sp => sp.Item.CatalogItemId);
                            break;
                        default:
                            datas = datas.OrderBy(sp => sp.Term.Order);
                            break;
                    }
                    #endregion
                    // Pagination
                    #region Pagination
                    if (filter.PageSize.HasValue && filter.PageNumber.HasValue)
                    {
                        if (filter.PageSize.Value <= 0) filter.PageSize = 20;

                        filter.PageNumber = filter.PageNumber - 1;
                        //Calculate nunber of rows to skip on pagesize
                        int excludedRows = (filter.PageNumber.Value) * (filter.PageSize.Value);
                        if (excludedRows <= 0) excludedRows = 0;

                        datas = datas.Skip(excludedRows).Take(filter.PageSize.Value);
                    }
                    #endregion

                    var fullQueryData = datas.ToList();
                    // Convert Data
                    if (fullQueryData != null && fullQueryData.Count() > 0)
                    {
                        result.Message = "Success get filter with filter : " + JsonConvert.SerializeObject(filter);
                        result.Status = 1;
                        result.DataCount = fullQueryData.Count();
                        result.TotalCount = totalCount;
                        foreach (var model in fullQueryData)
                        {
                            CatalogItemModel ctIterm = new CatalogItemModel()
                            {
                                //todo: ko cho user null
                                CreatedByUser = new BaseUserModel() { Id = model.Term.CreatedByUserId??Guid.Empty },
                                LastModifiedByUser = new BaseUserModel() { Id = model.Term.LastModifiedByUserId.HasValue ? model.Term.LastModifiedByUserId.Value : Guid.Empty },
                                CatalogItemId = model.Item.CatalogItemId,
                                CatalogMasterId = model.Cata.CatalogMasterId,
                                Name = model.Item.Name,
                                Code = model.Item.Code,
                                TermId = model.Term.TermId,
                                //Order = model.Term.Order.Value,
                                //VocabularyId = model.Term.VocabularyId,
                                Status = model.Item.Status,
                                Description = model.Term.Description,
                                IdPath = model.Term.IdPath,
                                Path = model.Term.Path,
                                ParentTermId = model.Term.ParentId,
                                Level = model.Term.Level.Value,
                            };
                            result.Data.Add(ctIterm);
                        }
                    }
                    return result;
                }
            }
            catch (Exception ex)
            {
                Log.Error("Error get filter with filter: " + JsonConvert.SerializeObject(filter), ex);
                return new OldResponse<List<CatalogItemModel>>(-1, "Error get filter with filter: " + JsonConvert.SerializeObject(filter), null, 0, 0);
            }
        }

        #region Get item tree
        /// <summary>
        /// Get item tree
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        public OldResponse<List<CatalogItemTreeModel>> GetFilterItemTree(CatalogItemQueryFilter filter)
        {
            try
            {
                List<CatalogItemTreeModel> result = new List<CatalogItemTreeModel>();
                filter.Order = CatalogItemQueryOrder.ORDER_ASC;
                List<CatalogItemModel> listCatalogItem1 = GetFilterItemList(filter).Data;
                List<CatalogItemTreeModel> listCatalogItem = new List<CatalogItemTreeModel>();
                foreach (var item in listCatalogItem1)
                {
                    var catalogItem = new CatalogItemTreeModel()
                    {
                        CatalogMasterId = item.CatalogMasterId,
                        Name = item.Name,
                        ParentTermId = item.ParentTermId,
                        TermId = item.TermId,
                        Order = item.Order,
                        Description = item.Description,
                        IdPath = item.IdPath,
                        Path = item.Path,
                        Level = item.Level,
                        Code = item.Code,
                        CreatedOnDate = item.CreatedOnDate,
                        LastModifiedOnDate = item.LastModifiedOnDate,
                        LastModifiedByUser = item.LastModifiedByUser,
                        Status = item.Status,
                        CatalogItemId = item.CatalogItemId,
                    };
                    // Xu ly folder path
                    var paths = item.Path.Split('/');

                    var pathList = new List<string>();

                    for (int i = 0; i < paths.Length; i++)
                    {
                        if (paths[i] != "." && !string.IsNullOrEmpty(paths[i]))
                        {
                            pathList.Add(paths[i]);
                        }
                    }
                    catalogItem.PathList = pathList.ToArray();

                    listCatalogItem.Add(catalogItem);
                }
                using (var unitOfWork = new UnitOfWork())
                {
                    if (listCatalogItem != null && listCatalogItem.Count > 0)
                    {
                        foreach (var item in listCatalogItem)
                        {
                            if (item.ParentTermId == Guid.Empty || item.ParentTermId == null)
                            {
                                result.Add(GetItemChild(listCatalogItem, item));
                            }
                        }
                        return new OldResponse<List<CatalogItemTreeModel>>(1, string.Empty, result);
                    }
                    else return new OldResponse<List<CatalogItemTreeModel>>(0, "Data not found", null);
                }
            }
            catch (Exception ex)
            {
                Log.Error("Error get filter with filter: " + JsonConvert.SerializeObject(filter), ex);
                return new OldResponse<List<CatalogItemTreeModel>>(-1, "Error get filter with filter: " + JsonConvert.SerializeObject(filter), null, 0, 0);
            }
        }
        private CatalogItemTreeModel GetItemChild(List<CatalogItemTreeModel> itemList, CatalogItemTreeModel itemParent)
        {
            CatalogItemTreeModel rItem = new CatalogItemTreeModel();
            rItem = itemParent;
            rItem.SubChild = new List<CatalogItemTreeModel>();
            using (var unitOfWork = new UnitOfWork())
            {
                foreach (CatalogItemTreeModel item in itemList)
                {
                    if (item.ParentTermId == itemParent.TermId)
                    {
                        rItem.SubChild.Add(GetItemChild(itemList, item));
                    }
                }
            }
            return rItem;
        }
        #endregion

        //Get danh sách item theo thứ tự cây phẳng: cha->con
        public OldResponse<List<BaseCatalogItemModel>> GetFilterItemByLevel(CatalogItemQueryFilter filter)
        {
            try
            {
                List<BaseCatalogItemModel> result = new List<BaseCatalogItemModel>();
                filter.Order = CatalogItemQueryOrder.ORDER_ASC;
                List<CatalogItemModel> listCatalogItem1 = GetFilterItemList(filter).Data;
                List<BaseCatalogItemModel> listCatalogItem = new List<BaseCatalogItemModel>();
                foreach (var item in listCatalogItem1)
                {
                    var catalogItem = new BaseCatalogItemModel()
                    {
                        Name = item.Name,
                        ParentTermId = item.ParentTermId,
                        TermId = item.TermId,
                        Order = item.Order,
                        Level = item.Level,
                        Code = item.Code,
                        Status = item.Status,
                        CatalogItemId = item.CatalogItemId,
                        CatalogMasterId = item.CatalogMasterId
                    };
                    //int level = catalogItem.Level;
                    //while (level>0)
                    //{
                    //    catalogItem.Name = "-" + catalogItem.Name;
                    //    level--;
                    //}
                    listCatalogItem.Add(catalogItem);
                }
                if (listCatalogItem != null && listCatalogItem.Count > 0)
                {
                    foreach (var item in listCatalogItem)
                    {
                        if (item.ParentTermId == Guid.Empty || item.ParentTermId == null)
                        {
                            result.Add(item);
                            result.AddRange(GetItemChildList(listCatalogItem, item));
                        }
                    }
                    return new OldResponse<List<BaseCatalogItemModel>>(1, string.Empty, result);
                }
                else return new OldResponse<List<BaseCatalogItemModel>>(0, "Data not found", null);
            }
            catch (Exception ex)
            {
                Log.Error("Error get filter with filter: " + JsonConvert.SerializeObject(filter), ex);
                return new OldResponse<List<BaseCatalogItemModel>>(-1, "Error get filter with filter: " + JsonConvert.SerializeObject(filter), null, 0, 0);
            }
        }
        //get danh sách các item con
        private List<BaseCatalogItemModel> GetItemChildList(List<BaseCatalogItemModel> itemList, BaseCatalogItemModel itemParent)
        {
            try
            {
                List<BaseCatalogItemModel> result = new List<BaseCatalogItemModel>();

                foreach (BaseCatalogItemModel item in itemList)
                {
                    if (item.ParentTermId == itemParent.TermId)
                    {
                        result.Add(item);
                        result.AddRange(GetItemChildList(itemList, item));
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                Log.Error("Error get filter with filter: ", ex);
                return null;
            }
        }
        #endregion

        //undone: trong một danh mục tổng, các item có cùng cha (hoặc không có cha) sẽ phải đặt tên khác nhau
        //không bắt trùng code của danh mục
        public OldResponse<List<CatalogItemModel>> GetAllByCode(string code, bool? status)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    var datas = from dt in unitOfWork.GetRepository<CatalogItem>().GetAll()
                                join term in unitOfWork.GetRepository<TaxonomyTerm>().GetAll()
                                on dt.MappedTermId equals term.TermId
                                join cm in unitOfWork.GetRepository<CatalogMaster>().GetAll()
                                on term.VocabularyId equals cm.MappedVocabularyId
                                where cm.Code == code && dt.Status == status
                                select new CatalogItemModel()
                                {
                                    CatalogItemId = dt.CatalogItemId,
                                    Name = dt.Name,
                                    Code = dt.Code,
                                    Status = dt.Status
                                };
                    if (status.HasValue)
                        datas = datas.Where(x => x.Status == status);
                    var data1 = datas.ToList();
                    return new OldResponse<List<CatalogItemModel>>(1, string.Empty, data1, data1.Count, data1.Count);
                }
            }
            catch (Exception ex)
            {
                Log.Error("Xảy ra lỗi trong quá trình lấy dữ liệu:", ex);
                return new OldResponse<List<CatalogItemModel>>(-1, "Xảy ra lỗi trong quá trình lấy dữ liệu: " + ex.Message, null);
            }
        }
        public OldResponse<List<BaseCatalogItemModel>> GetAllByCodeAndParent(string code, string metadataCode, Guid metadataId)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    var datas = (from dt in unitOfWork.GetRepository<CatalogItem>().GetAll()
                                 join term in unitOfWork.GetRepository<TaxonomyTerm>().GetAll()
                                 on dt.MappedTermId equals term.TermId
                                 join cm in unitOfWork.GetRepository<CatalogMaster>().GetAll()
                                 on term.VocabularyId equals cm.MappedVocabularyId
                                 join attribute in unitOfWork.GetRepository<CatalogItemAttribute>().GetAll()
                                 on dt.CatalogItemId equals attribute.CatalogItemId
                                 join metadata in unitOfWork.GetRepository<MetadataField>().GetAll()
                                 on attribute.MetadataFieldId equals metadata.MetadataFieldId
                                 where cm.Code == code && metadata.Code == metadataCode && attribute.GuidValue == metadataId
                                 select new BaseCatalogItemModel()
                                 {
                                     CatalogItemId = dt.CatalogItemId,
                                     Name = dt.Name,
                                     Code = dt.Code
                                 }).ToList();

                    return new OldResponse<List<BaseCatalogItemModel>>(1, string.Empty, datas, datas.Count, datas.Count);
                }
            }
            catch (Exception ex)
            {
                Log.Error("Xảy ra lỗi trong quá trình lấy dữ liệu:", ex);
                return new OldResponse<List<BaseCatalogItemModel>>(-1, "Xảy ra lỗi trong quá trình lấy dữ liệu: " + ex.Message, null);
            }
        }
        public OldResponse<List<BaseCatalogItemModel>> GetAllByAtribute(string catalogMasterCode, string metadataCode, CatalogItemAttributeModel value, bool? status, CatalogItemQueryOrder order)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    var datas = from dt in unitOfWork.GetRepository<CatalogItem>().GetAll()
                                join term in unitOfWork.GetRepository<TaxonomyTerm>().GetAll()
                                on dt.MappedTermId equals term.TermId
                                join cm in unitOfWork.GetRepository<CatalogMaster>().GetAll()
                                on term.VocabularyId equals cm.MappedVocabularyId
                                join attribute in unitOfWork.GetRepository<CatalogItemAttribute>().GetAll()
                                on dt.CatalogItemId equals attribute.CatalogItemId
                                //join metadata in unitOfWork.GetRepository<Metadata_Field>().GetAll()
                                //on attribute.MetadataFieldId equals metadata.MetadataFieldId
                                where cm.Code == catalogMasterCode && attribute.MetadataFieldCode == metadataCode
                                select new
                                {
                                    CatalogItemId = dt.CatalogItemId,
                                    Name = dt.Name,
                                    Code = dt.Code,
                                    Status = dt.Status,
                                    CreatedOnDate = dt.CreatedOnDate,
                                    Order = dt.Order,
                                    LastModifiedOnDate = dt.LastModifiedOnDate,
                                    attribute = attribute
                                };
                    if (status.HasValue)
                        datas = datas.Where(x => x.Status == status);
                    if (!string.IsNullOrEmpty(value.NvarcharValue))
                    {
                        datas = datas.Where(x => x.attribute.DataType == "NvarcharValue" && x.attribute.NvarcharValue == value.NvarcharValue);
                    }
                    if (value.IntValue.HasValue)
                    {
                        datas = datas.Where(x => x.attribute.DataType == "IntValue" && x.attribute.IntValue == value.IntValue.Value);
                    }
                    if (value.DatetimeValue.HasValue)
                    {
                        datas = datas.Where(x => x.attribute.DataType == "DatetimeValue" && x.attribute.DatetimeValue == value.DatetimeValue.Value);
                    }
                    if (value.BitValue.HasValue)
                    {
                        datas = datas.Where(x => x.attribute.DataType == "BitValue" && x.attribute.BitValue == value.BitValue.Value);
                    }
                    if (!string.IsNullOrEmpty(value.VarcharValue))
                    {
                        datas = datas.Where(x => x.attribute.DataType == "VarcharValue" && x.attribute.VarcharValue == value.VarcharValue);
                    }
                    if (value.GuidValue.HasValue)
                    {
                        datas = datas.Where(x => x.attribute.DataType == "GuidValue" && x.attribute.GuidValue == value.GuidValue.Value);
                    }
                    //Type datetime
                    //if (value.DateValue.HasValue)
                    //{
                    //    datas = datas.Where(x => x.attribute.TypeDateTime == "DateValue" && x.attribute.DateValue == value.DateValue.Value);
                    //}
                    //if (value.YearValue.HasValue)
                    //{
                    //    datas = datas.Where(x => x.attribute.DataType == "YearValue" && x.attribute.YearValue == value.YearValue.Value);
                    //} 
                    //Order
                    #region ordering
                    switch (order)
                    {
                        case CatalogItemQueryOrder.CREATE_DATE_ASC:
                            datas = datas.OrderBy(sp => sp.CreatedOnDate);
                            break;
                        case CatalogItemQueryOrder.CREATE_DATE_DESC:
                            datas = datas.OrderByDescending(sp => sp.CreatedOnDate);
                            break;
                        case CatalogItemQueryOrder.MODIFIED_DATE_ASC:
                            datas = datas.OrderBy(sp => sp.LastModifiedOnDate);
                            break;
                        case CatalogItemQueryOrder.MODIFIED_DATE_DESC:
                            datas = datas.OrderByDescending(sp => sp.LastModifiedOnDate);
                            break;
                        case CatalogItemQueryOrder.ORDER_ASC:
                            datas = datas.OrderBy(sp => sp.Order).ThenByDescending(sp => sp.LastModifiedOnDate);
                            break;
                        case CatalogItemQueryOrder.ORDER_DESC:
                            datas = datas.OrderByDescending(sp => sp.Order).ThenByDescending(sp => sp.LastModifiedOnDate);
                            break;
                        default:
                            datas = datas.OrderBy(sp => sp.Order).ThenByDescending(sp => sp.LastModifiedOnDate);
                            break;
                    }
                    #endregion
                    var data1 = (from dt in datas
                                 select new BaseCatalogItemModel
                                 {
                                     CatalogItemId = dt.CatalogItemId,
                                     Name = dt.Name,
                                     Code = dt.Code
                                 }).ToList();
                    return new OldResponse<List<BaseCatalogItemModel>>(1, string.Empty, data1, data1.Count, data1.Count);
                }
            }
            catch (Exception ex)
            {
                Log.Error("Xảy ra lỗi trong quá trình lấy dữ liệu:", ex);
                return new OldResponse<List<BaseCatalogItemModel>>(-1, "Xảy ra lỗi trong quá trình lấy dữ liệu: " + ex.Message, null);
            }
        }
    }
}
