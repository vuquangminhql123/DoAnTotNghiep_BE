﻿using DigitalID.Data;
using System;
using System.Collections.Generic;

namespace DigitalID.Business
{
    public interface ICatalogItemHandler
    {
        void init(Guid applicationId, Guid userId);
        #region CRUD
        
        OldResponse<IList<CatalogItemModel>> GetAll();
        OldResponse<IList<CatalogItemModel>> GetFilter(CatalogItemQueryFilter filter);
        OldResponse<CatalogItemModel> GetById(Guid CatalogItemId);
        OldResponse<CatalogItemModel> GetByCode(String code, String name);
        OldResponse<IList<CatalogItemAttributeModel>> GetAtributeById(Guid CatalogItemId);
        OldResponse<CatalogItemModel> Create(CatalogItemCreateRequestModel request);

        OldResponse<CatalogItemModel> Update(CatalogItemUpdateRequestModel request);
        OldResponse<CatalogItemDeleteResponseModel> Delete(Guid CatalogItemId);

        OldResponse<IList<CatalogItemDeleteResponseModel>> DeleteMany(IList<Guid> listCatalogItemId);
        #endregion

        OldResponse<List<CatalogItemModel>> GetFilterItemList(CatalogItemQueryFilter filter);
        OldResponse<List<CatalogItemTreeModel>> GetFilterItemTree(CatalogItemQueryFilter filter);
        OldResponse<List<BaseCatalogItemModel>> GetFilterItemByLevel(CatalogItemQueryFilter filter);
        OldResponse<List<BaseCatalogItemModel>> GetAllByCodeAndParent(string code, string metadataCode, Guid metadataId);
        OldResponse<List<BaseCatalogItemModel>> GetAllByAtribute(string catalogMasterCode, string metadataCode, CatalogItemAttributeModel value, bool? status, CatalogItemQueryOrder order);
    }
}
