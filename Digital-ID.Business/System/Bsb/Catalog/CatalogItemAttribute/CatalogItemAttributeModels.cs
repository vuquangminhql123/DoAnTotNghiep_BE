﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitalID.Business
{
    public class CatalogItemAttributeModel
    { 
        public Guid CatalogItemAttributeId { get; set; }
        public Guid MetadataFieldId { get; set; }
        public string Key { get; set; }
        public string Value { get; set; }
        public string Code { get; set; }
        public string DataType { get; set; }
        public string NvarcharValue { get; set; }
        public string VarcharValue { get; set; }
        public bool? BitValue { get; set; }
        public int? IntValue { get; set; }
        public Guid? GuidValue { get; set; }
        public DateTime? DatetimeValue { get; set; }
        public string JsonValue { get; set; }
        public string JsonGuidValue { get; set; }
        public DateTime? TimeValue { get; set; }
        public DateTime? DateValue { get; set; }
        public DateTime? YearValue { get; set; }
        public DateTime? MonthValue { get; set; }
        public Int32? TypeDateTime { get; set; }
        public bool IsAndClause { get; set; } // AND|OR clause 
        public int CompareExpression { get; set; }//-10: < | -11: <= | 0: = | 10 :>= | 11: > 0
        public string Name { get; set; }
    }
}
