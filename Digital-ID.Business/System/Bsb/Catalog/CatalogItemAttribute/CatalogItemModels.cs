﻿using System;

namespace DigitalID.Business
{
    // Create request model
    public class CatalogItemAttributeCreateRequestModel
    {
        
    }
    //public class CatalogFieldItem
    //{
    //    public Guid FieldId { get; set; }
    //    public int Order { get; set; }
    //    public int Type { get; set; }
    //    public bool DisplayCategory { get; set; }
    //    public bool DisplaySearchCategory { get; set; }
    //    public bool DisplayReport { get; set; }
    //    public bool DisplaySearchReport { get; set; }
    //}
    // Update request model
    public class CatalogItemAttributeUpdateRequestModel
    {
       
    }
    // RequestModel thường được sử dụng cho việc Delete nhiều bản ghi
    public class CatalogItemAttributeMultiDeleteRequestModel
    {
      
    }
    public class CatalogItemAttributeDeleteResponseModel
    {
       
    }
    public enum CatalogItemAttributeQueryOrder
    {
        ID_DESC,
        ID_ASC,
        ORDER_DESC,
        ORDER_ASC,
        CREATE_DATE_ASC,
        MODIFIED_DATE_ASC,
        CREATE_DATE_DESC,
        MODIFIED_DATE_DESC,
    }

    public class CatalogItemAttributeQueryFilter
    {
        public int? PageSize { get; set; }
        public int? PageNumber { get; set; }
        public string TextSearch { get; set; }
        public System.Guid MappedTermId { get; set; }
        public bool IsActive { get; set; }
        public Guid? CatalogItemId { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        //todo: thêm truonq code và des vào cơ sơ dữ liệu
        public string Description { get; set; }
        public CatalogItemQueryOrder Order { get; set; }
        public CatalogItemAttributeQueryFilter()
        {
            PageSize = 0;
            PageNumber = 1;
            TextSearch = string.Empty;
            Order = CatalogItemQueryOrder.CREATE_DATE_DESC;
        }
    }
}
