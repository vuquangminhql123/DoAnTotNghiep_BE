﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitalID.Business
{
    public class BaseCatalogModel
    {
        public Guid CatalogMasterId { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }

    }
    public class BaseCatalogTermModel
    {
        public Guid CatalogMasterId { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public Guid TermId { get; set; }
        public Guid? ParentTermId { get; set; }

    }

    public class CatalogMasterClientModel : BaseCatalogModel
    {
        public Guid VocabularyId { get; set; }
        public Guid MappedTermId { get; set; }
        public Guid MetadataTemplateId { get; set; }
        //todo: delete vc name
        public string VocabularyName { get; set; }
        public Guid? CreatedByUserId { get; set; }
        public DateTime? CreatedOnDate { get; set; }
        public Guid? LastModifiedByUserId { get; set; }
        public DateTime? LastModifiedOnDate { get; set; }
        public int Order { get; set; }
        public Guid? ParentTermId { get; set; }
        public bool? Status { get; set; }
        public string Description { get; set; }
        public string IdPath { get; set; }
        public string Path { get; set; }
        public List<CatalogMasterClientModel> SubChild { get; set; }

        public PathListModel PathList;
        //public string[] PathList { get; set; }
        
    }
    public class PathListModel
    {
        public string[] IdPathList { get; set; }
        public string[] NamePathList { get; set; }
    }
    public class CatalogMasterModel : BaseCatalogModel
    {
        public Guid VocabularyId { get; set; }
        public Guid TermId { get; set; }
        public Guid MetadataTemplateId { get; set; }
        //todo: delete vc name
        public string VocabularyName { get; set; }
        public Guid? CreatedByUserId { get; set; }
        public DateTime? CreatedOnDate { get; set; }
        public Guid? LastModifiedByUserId { get; set; }
        public DateTime? LastModifiedOnDate { get; set; }
        public int Order { get; set; }
        public Guid? ParentTermId { get; set; }
        public bool? Status { get; set; }
        public string Description { get; set; }

    }

    // Create request model
    public class CatalogMasterCreateRequestModel
    {
        public string Name { get; set; } 
        public string Code { get; set; } 
        public Guid? UserId { get; set; }
        public Guid VocabularyId { get; set; }
        public Guid? ParentTermId { get; set; }
        public int Order { get; set; }
        public string Description { get; set; }
        public bool Status { get; set; }
        public Guid? ApplicationId { get; set; } 
        public MetadataTemplateCreateRequestModel TemplateModel { get; set; } 
    }
    // Update request model
    public class CatalogMasterUpdateRequestModel : BaseCatalogModel
    {
        //public Guid CatalogId { get; set; }
        //public string Name { get; set; }
        //public string Code { get; set; }
        //public int Level { get; set; }
        public int Order { get; set; }
        public Guid? ParentTermId { get; set; }
        public Guid? LastModifiedByUserId { get; set; }
        public DateTime LastModifiedOnDate { get; set; }
        public string Description { get; set; }
        public bool Status { get; set; }
        //
        public MetadataTemplateUpdateRequestModel TemplateModel { get; set; } 
    }


    // RequestModel thường được sử dụng cho việc Delete nhiều bản ghi
    public class CatalogMasterMultiDeleteRequestModel
    {
        public List<Guid> ListCatalogMasterId { get; set; }
        public int? DeleteType { get; set; }//
    }
    public class CatalogMasterDeleteResponseModel
    {
        public Guid CatalogMasterId { get; set; }
        public string Name { get; set; }
        public int? Result { get; set; }
        public string Message { get; set; }
    }
    public enum CatalogMasterQueryOrder
    {
        ID_DESC,
        ID_ASC,
        ORDER_DESC,
        ORDER_ASC,
        CREATE_DATE_ASC,
        MODIFIED_DATE_ASC,
        CREATE_DATE_DESC,
        MODIFIED_DATE_DESC,
    }

    public class CatalogMasterQueryFilter
    {
        public int? PageSize { get; set; }
        public int? PageNumber { get; set; }
        public string TextSearch { get; set; }
        public Guid? CatalogMasterId { get; set; }
        public Guid? TermId { get; set; }
        public Guid? ParentId { get; set; }
        public string Name { get; set; }
        public Guid? VocabularyId { get; set; }
        public string VocabularyCode { get; set; }
        public string Description { get; set; }
        public int? Level { get; set; }
        public bool? Status { get; set; }
        public CatalogMasterQueryOrder Order { get; set; }
        public CatalogMasterQueryFilter()
        {
            PageSize = 0;
            PageNumber = 1;
            TextSearch = string.Empty;
            Order = CatalogMasterQueryOrder.ORDER_ASC;
        }
    }
}
