﻿using System;
using System.Collections.Generic;

namespace DigitalID.Business
{
    public class DataAttribute
    {
        public Guid FieldId { get; set; }
        public string Code { get; set; }
        public string Key { get; set; }
        public string Value { get; set; }
        //
        public string DataType { get; set; }
        public string NvarcharValue { get; set; }
        public string VarcharValue { get; set; }
        public bool? BitValue { get; set; }
        public int? IntValue { get; set; }
        public Guid? GuidValue { get; set; }
        public DateTime? DateValue { get; set; }
        public string JsonValue { get; set; }
        public string JsonGuidValue { get; set; }
        public DateTime? TimeValue { get; set; }
        public DateTime? DatetimeValue { get; set; }
        public DateTime? YearValue { get; set; }
        public DateTime? MonthValue { get; set; }
        public Int32? TypeDateTime { get; set; }
        public bool IsAndClause { get; set; } // AND|OR clause 
        public int CompareExpression { get; set; }//-10: < | -11: <= | 0: = | 10 :>= | 11: > 0
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public Guid? ArchiveTypeId { get; set; }
        public bool IsFullText { get; set; }
    }
}
