﻿using DigitalID.Data;
using Newtonsoft.Json;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DigitalID.Business
{
    public class DbCatalogHandler : ICatalogHandler
    {
        private Guid? _applicationId { get; set; }
        private Guid? _userId { get; set; }

        private readonly ITaxonomyVocabularyHandler _taxonomyVocabularyHandler;

        public DbCatalogHandler(ITaxonomyVocabularyHandler taxonomyVocabularyHandler)
        {
            _taxonomyVocabularyHandler = taxonomyVocabularyHandler;
        }

        public void init(Guid applicationId, Guid userId)
        {
            _applicationId = applicationId;
            _userId = userId;
        }

        /// <summary>
        /// Get filter
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        public OldResponse<List<CatalogMasterModel>> GetFilter(CatalogMasterQueryFilter filter)
        {
            try
            {
                // Create result variable
                var result = new OldResponse<List<CatalogMasterModel>>(0, string.Empty, new List<CatalogMasterModel>(), 0, 0);

                using (var unitOfWork = new UnitOfWork())
                {
                    var datas = from catalog in unitOfWork.GetRepository<CatalogMaster>().GetAll()
                                join term in unitOfWork.GetRepository<TaxonomyTerm>().GetAll()
                                on catalog.MappedTermId equals term.TermId
                                join voca in unitOfWork.GetRepository<TaxonomyVocabulary>().GetAll()
                                on catalog.MappedVocabularyId equals voca.VocabularyId
                                select new
                                {
                                    Catalog = catalog,
                                    // Catalog.ParentTermId=term.ParentId,
                                    Term = term,
                                    Vocabulary = voca
                                };
                    //filter
                    #region filter

                    if (filter.CatalogMasterId.HasValue)
                    {
                        datas = from dt in datas
                                where dt.Catalog.CatalogMasterId == filter.CatalogMasterId.Value
                                select dt;
                    }
                    if (!string.IsNullOrEmpty(filter.VocabularyCode))
                    {
                        datas = from dt in datas
                                where dt.Catalog.Code == filter.VocabularyCode
                                select dt;
                    }
                    //todo: thêm trường này vào cơ sở dữ liệu không?(bảng catalog)
                    if (!string.IsNullOrEmpty(filter.Description))
                    {
                        datas = from dt in datas
                                where dt.Term.Description == filter.Description
                                select dt;
                    }
                    //by Level
                    if (filter.Level.HasValue)
                    {
                        datas = from dt in datas
                                where dt.Term.Level == filter.Level.Value
                                select dt;
                    }
                    //by Mapped term Id
                    if (filter.TermId.HasValue)
                    {
                        datas = from dt in datas
                                where dt.Catalog.MappedTermId == filter.TermId.Value
                                select dt;
                    }
                    //by Parent Id
                    if (filter.ParentId.HasValue)
                    {
                        datas = from dt in datas
                                where dt.Term.ParentId == filter.ParentId.Value
                                select dt;
                    }
                    //by Name
                    if (!string.IsNullOrEmpty(filter.Name))
                    {
                        datas = from dt in datas
                                where dt.Catalog.Name == filter.Name
                                select dt;
                    }
                    if (filter.Status.HasValue)
                    {
                        datas = from dt in datas
                                where dt.Catalog.Status == filter.Status
                                select dt;
                    }

                    //by TextSearch
                    if (!string.IsNullOrEmpty(filter.TextSearch))
                    {
                        filter.TextSearch = filter.TextSearch.ToLower().Trim();
                        datas = from dt in datas
                                where dt.Catalog.Name.ToLower().Contains(filter.TextSearch)
                                select dt;
                    }

                    #endregion
                    var totalCount = datas.Count();
                    //Order
                    #region ordering
                    switch (filter.Order)
                    {
                        case CatalogMasterQueryOrder.CREATE_DATE_ASC:
                            datas = datas.OrderBy(sp => sp.Catalog.CreatedOnDate);
                            break;
                        case CatalogMasterQueryOrder.CREATE_DATE_DESC:
                            datas = datas.OrderByDescending(sp => sp.Catalog.CreatedOnDate);
                            break;
                        case CatalogMasterQueryOrder.MODIFIED_DATE_ASC:
                            datas = datas.OrderBy(sp => sp.Catalog.LastModifiedOnDate);
                            break;
                        case CatalogMasterQueryOrder.MODIFIED_DATE_DESC:
                            datas = datas.OrderByDescending(sp => sp.Catalog.LastModifiedOnDate);
                            break;
                        case CatalogMasterQueryOrder.ORDER_ASC:
                            datas = datas.OrderBy(sp => sp.Term.Order);
                            break;
                        case CatalogMasterQueryOrder.ORDER_DESC:
                            datas = datas.OrderByDescending(sp => sp.Term.Order);
                            break;
                        case CatalogMasterQueryOrder.ID_ASC:
                            datas = datas.OrderBy(sp => sp.Catalog.CatalogMasterId);
                            break;
                        case CatalogMasterQueryOrder.ID_DESC:
                            datas = datas.OrderByDescending(sp => sp.Catalog.CatalogMasterId);
                            break;
                        default:
                            datas = datas.OrderByDescending(sp => sp.Catalog.LastModifiedOnDate);
                            break;
                    }
                    #endregion
                    // Pagination
                    #region Pagination
                    if (filter.PageSize.HasValue && filter.PageNumber.HasValue)
                    {
                        if (filter.PageSize.Value <= 0) filter.PageSize = 20;

                        filter.PageNumber = filter.PageNumber - 1;
                        //Calculate nunber of rows to skip on pagesize
                        int excludedRows = (filter.PageNumber.Value) * (filter.PageSize.Value);
                        if (excludedRows <= 0) excludedRows = 0;

                        datas = datas.Skip(excludedRows).Take(filter.PageSize.Value);
                    }
                    #endregion

                    var fullQueryData = datas.ToList();
                    // Convert Data
                    if (fullQueryData != null && fullQueryData.Count() > 0)
                    {
                        result.Message = "Success get filter with filter: " + JsonConvert.SerializeObject(filter);
                        result.Status = 1;
                        result.DataCount = fullQueryData.Count();
                        result.TotalCount = totalCount;
                        foreach (var item in fullQueryData)
                        {
                            var cata = ConvertData(item.Catalog);
                            cata.ParentTermId = item.Term.ParentId;
                            result.Data.Add(cata);
                        }
                    }
                    return result;
                }
            }
            catch (Exception ex)
            {
                Log.Error("Error get filter with filter: " + JsonConvert.SerializeObject(filter), ex);
                return new OldResponse<List<CatalogMasterModel>>(-1, "Error get filter with filter: " + JsonConvert.SerializeObject(filter), null, 0, 0);
            }
        }

        /// <summary>
        /// Get all catalog list
        /// </summary>
        /// <returns></returns>
        public OldResponse<List<CatalogMasterModel>> GetAllCatalog()
        {
            var filter = new CatalogMasterQueryFilter() { };
            var response = GetFilter(filter);
            if (response.Status == 1)
            {
                return new OldResponse<List<CatalogMasterModel>>(1, "Success", response.Data);
            }
            else
            {
                return new OldResponse<List<CatalogMasterModel>>(response.Status, response.Message, null);
            }
        }

        /// <summary>
        /// Get by Catalog Id
        /// </summary>
        /// <param name="catalogId"></param>
        /// <returns></returns>
        public OldResponse<CatalogMasterModel> GetCatalogMasterById(Guid catalogId)
        {
            var filter = new CatalogMasterQueryFilter() { };
            filter.CatalogMasterId = catalogId;
            var response = GetFilter(filter);
            if (response.Status == 1)
            {
                return new OldResponse<CatalogMasterModel>(1, "Success", response.Data.FirstOrDefault());
            }
            else
            {
                return new OldResponse<CatalogMasterModel>(response.Status, response.Message, null);
            }
        }
        /// <summary>
        /// Get by parentId
        /// </summary>
        /// <param name="parentId"></param>
        /// <returns></returns>
        public OldResponse<List<CatalogMasterModel>> GetCatalogsInParent(Guid parentId)
        {
            var filter = new CatalogMasterQueryFilter() { ParentId = parentId };
            var response = GetFilter(filter);
            if (response.Status == 1)
            {
                return new OldResponse<List<CatalogMasterModel>>(1, "Success", response.Data);
            }
            else
            {
                return new OldResponse<List<CatalogMasterModel>>(response.Status, response.Message, null);
            }
        }
        ///// <summary>
        ///// get instant list
        ///// </summary>
        ///// <param name="filter"></param>
        ///// <returns></returns>
        //public OldResponse<List<CatalogItemModel>> GetCatalogItemById(CatalogItemAttributeQueryFilter filter)
        //{
        //    try
        //    {
        //        // Create result variable
        //        var result = new OldResponse<List<CatalogItemModel>>(0, string.Empty, new List<CatalogItemModel>(), 0, 0);

        //        using (var unitOfWork = new UnitOfWork())
        //        {
        //            var datas = from catalog in unitOfWork.GetRepository<CatalogMaster>().GetAll()
        //                        join term in unitOfWork.GetRepository<TaxonomyTerm>().GetAll()
        //                        on catalog.MappedVocabularyId equals term.VocabularyId
        //                        join item in unitOfWork.GetRepository<Catalog_Item>().GetAll()
        //                        on term.TermId equals item.MappedTermId
        //                        join att in unitOfWork.GetRepository<Catalog_Item_Attribute>().GetAll()
        //                        on item.CatalogItemId equals att.CatalogItemId
        //                        select new
        //                        {
        //                            Item = item,
        //                            Attribute = att
        //                        };
        //            //filter
        //            #region filter
        //            //by TextSearch
        //            if (!string.IsNullOrEmpty(filter.TextSearch))
        //            {
        //                filter.TextSearch = filter.TextSearch.ToLower().Trim();
        //                datas = from dt in datas
        //                        where dt.Item.Name.ToLower().Contains(filter.TextSearch)
        //                        select dt;
        //            }

        //            #endregion
        //            var totalCount = datas.Count();
        //            //Order
        //            #region ordering
        //            switch (filter.Order)
        //            {
        //                case CatalogItemQueryOrder.NAME:
        //                    datas = datas.OrderBy(sp => sp.Item.Name);
        //                    break;
        //                //case CatalogItemQueryOrder.CODE:
        //                //    datas = datas.OrderByDescending(sp => sp.Item.);
        //                //    break;
        //                default:
        //                    datas = datas.OrderByDescending(sp => sp.Item.CatalogItemId);
        //                    break;
        //            }
        //            #endregion
        //            // Pagination
        //            #region Pagination
        //            if (filter.PageSize.HasValue && filter.PageNumber.HasValue)
        //            {
        //                if (filter.PageSize.Value <= 0) filter.PageSize = 20;

        //                filter.PageNumber = filter.PageNumber - 1;
        //                //Calculate nunber of rows to skip on pagesize
        //                int excludedRows = (filter.PageNumber.Value) * (filter.PageSize.Value);
        //                if (excludedRows <= 0) excludedRows = 0;

        //                datas = datas.Skip(excludedRows).Take(filter.PageSize.Value);
        //            }
        //            #endregion

        //            var fullQueryData = datas.ToList();
        //            // Convert Data
        //            if (fullQueryData != null && fullQueryData.Count() > 0)
        //            {
        //                result.Message = "Success get filter with filter : " + JsonConvert.SerializeObject(filter);
        //                result.Status = 1;
        //                result.DataCount = fullQueryData.Count();
        //                result.TotalCount = totalCount;
        //                foreach (var item in fullQueryData)
        //                {
        //                    result.Data.Add(ConvertDataItem(item.Item));
        //                }
        //            }
        //            return result;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Log.Error("Error get filter with filter: " + JsonConvert.SerializeObject(filter), ex);
        //        return new OldResponse<List<CatalogItemModel>>(-1, "Error get filter with filter: " + JsonConvert.SerializeObject(filter), null, 0, 0);
        //    }
        //}

        public OldResponse<List<CatalogMasterClientModel>> GetAllCatalogTerm()
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    var listCatalogClients = new List<CatalogMasterClientModel>();

                    //chỉ lấy cha
                    var listCatalogQuery = from catalog in unitOfWork.GetRepository<CatalogMaster>().GetAll()
                                           join term in unitOfWork.GetRepository<TaxonomyTerm>().GetAll().OrderBy(x => x.Order) on catalog.MappedTermId equals term.TermId
                                           //where term.ParentId == Guid.Empty || term.ParentId == null
                                           select new
                                           {
                                               Id = catalog.CatalogMasterId,
                                               Name = catalog.Name,
                                               MappedTermId = catalog.MappedTermId,
                                               ParentId = term.ParentId,
                                               Order = term.Order,
                                               Status = catalog.Status,
                                               Description = catalog.Description,
                                               IdPath = term.IdPath,
                                               Path = term.Path,
                                               Code = catalog.Code,
                                               CreatedOnDate = catalog.CreatedOnDate,
                                               LastModifiedOnDate = catalog.LastModifiedOnDate,
                                               LastModifiedByUserId = catalog.LastModifiedByUserId,
                                               MetadataTemplateId = catalog.MetadataTemplateId,
                                               VocabularyId = catalog.MappedVocabularyId,

                                           };

                    var listCatalogs = listCatalogQuery.ToList();

                    if (listCatalogs != null && listCatalogs.Count > 0)
                    {
                        foreach (var catalog in listCatalogs)
                        {
                            var catalogMasterClient = new CatalogMasterClientModel()
                            {
                                CatalogMasterId = catalog.Id,
                                Name = catalog.Name,
                                ParentTermId = catalog.ParentId,
                                MappedTermId = catalog.MappedTermId,
                                Order = catalog.Order.Value,
                                Status = catalog.Status,
                                Description = catalog.Description,
                                IdPath = catalog.IdPath,
                                Path = catalog.Path,
                                Code = catalog.Code,
                                CreatedOnDate = catalog.CreatedOnDate,
                                LastModifiedOnDate = catalog.LastModifiedOnDate,
                                LastModifiedByUserId = catalog.LastModifiedByUserId,
                                MetadataTemplateId = catalog.MetadataTemplateId,
                                VocabularyId = catalog.VocabularyId,
                            };
                            // Xu ly folder path
                            var namePaths = new string[]{};
                            if (!string.IsNullOrEmpty(catalog.Path))
                            {
                                namePaths = catalog.Path.Split('/');
                            }
                            var idPaths = new string[]{};
                            if (!string.IsNullOrEmpty(catalog.IdPath))
                            {
                                idPaths = catalog.IdPath.Split('/');
                            }
                            var namePathList = new List<string>();
                            var idPathList = new List<string>();

                            for (int i = 0; i < namePaths.Length; i++)
                            {
                                if (namePaths[i] != "." && !string.IsNullOrEmpty(namePaths[i]))
                                {
                                    namePathList.Add(namePaths[i]);
                                }
                            }

                            for (int i = 0; i < idPaths.Length; i++)
                            {
                                if (idPaths[i] != "." && !string.IsNullOrEmpty(idPaths[i]))
                                {
                                    idPathList.Add(idPaths[i]);
                                }
                            }

                            catalogMasterClient.PathList = new PathListModel { NamePathList = namePathList.ToArray(), IdPathList = idPathList.ToArray() };

                            listCatalogClients.Add(catalogMasterClient);

                        }
                        return new OldResponse<List<CatalogMasterClientModel>>(1, "", listCatalogClients.OrderBy(f => f.Order).ToList());

                    }

                    return new OldResponse<List<CatalogMasterClientModel>>(0, "", null);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return new OldResponse<List<CatalogMasterClientModel>>(-1, ex.Message, null);
            }
        }


        #region Get in tree
        public OldResponse<List<CatalogMasterClientModel>> GetCatalogViewTree()
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    List<CatalogMasterClientModel> rTerms = new List<CatalogMasterClientModel>();
                    List<CatalogMasterClientModel> listCatalogs = GetAllCatalogTerm().Data;

                    if (listCatalogs != null && listCatalogs.Count > 0)
                    {
                        foreach (var catalog in listCatalogs)
                        {
                            if (catalog.ParentTermId == Guid.Empty || catalog.ParentTermId == null)
                            {
                                rTerms.Add(GetCatalogChild(listCatalogs, catalog));
                            }
                        }
                        return new OldResponse<List<CatalogMasterClientModel>>(1, string.Empty, rTerms);
                    }
                    else return new OldResponse<List<CatalogMasterClientModel>>(0, "Data not found", null);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex,"");
                return new OldResponse<List<CatalogMasterClientModel>>(-1, ex.Message, null);
            }
        }
        private CatalogMasterClientModel GetCatalogChild(List<CatalogMasterClientModel> catalogList, CatalogMasterClientModel catalogParent)
        {
            CatalogMasterClientModel rCatalog = new CatalogMasterClientModel();
            rCatalog = catalogParent;
            rCatalog.SubChild = new List<CatalogMasterClientModel>();
            using (var unitOfWork = new UnitOfWork())
            {
                foreach (CatalogMasterClientModel item in catalogList)
                {
                    if (item.ParentTermId == catalogParent.MappedTermId)
                    {
                        rCatalog.SubChild.Add(GetCatalogChild(catalogList, item));
                    }
                }
            }
            return rCatalog;
        }
        #endregion

        /// <summary>
        /// Create new catalog master | Tạo danh mục tổng 
        /// </summary>
        /// <param name="createModel"></param>
        /// <returns></returns>
        public OldResponse<CatalogMasterModel> Create(CatalogMasterCreateRequestModel createModel)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    var newCatalog = unitOfWork.GetRepository<CatalogMaster>();
                    #region validation
                    //check unique Name 
                    var check = newCatalog.Get(s => s.Name == createModel.Name).FirstOrDefault();
                    if (check != null)
                    {
                        Log.Error("Name: " + createModel.Name + " is exit!");
                        return new OldResponse<CatalogMasterModel>(0, "Tên danh mục: " + createModel.Name + " đã tồn tại!", null);
                    }
                    var checkCode = newCatalog.Get(s => s.Code == createModel.Code).FirstOrDefault();
                    if (checkCode != null)
                    {
                        Log.Error("Code: " + createModel.Code + " is exit!");
                        return new OldResponse<CatalogMasterModel>(0, "Mã danh mục: " + createModel.Name + " đã tồn tại!", null);
                    }
                    #endregion

                    #region Step 1. Add Taxonomy Term 
                    var termRequest = new TaxonomyTermCreateRequestModel();
                    termRequest.Name = createModel.Name;
                    //undone:  có thể truyền vào một danh mục tĩnh hoặc gán trực tiếp -> fixed code
                    var tvCol = new TaxonomyVocabularyCollection(_taxonomyVocabularyHandler);
                    termRequest.VocabularyId = tvCol.GetCategoryMaster().VocabularyId;
                    termRequest.Order = createModel.Order;
                    termRequest.Description = createModel.Description;
                    if (createModel.ParentTermId != null && createModel.ParentTermId != Guid.Empty)
                    {
                        termRequest.ParentTerm = new ParentTaxonomyTermModel();
                        termRequest.ParentTerm.TermId = createModel.ParentTermId;
                    }
                    termRequest.CreatedByUserId = createModel.UserId.HasValue ? createModel.UserId.Value : _userId.Value;
                    DbTaxonomyTermsHandler termHandler = new DbTaxonomyTermsHandler();
                    var termRepo = termHandler.Create(termRequest, unitOfWork);
                    #endregion

                    #region Step 2. Create new TaxonomyVocabulary
                    var vocabularyRequest = new TaxonomyVocabularyCreateRequestModel();
                    vocabularyRequest.Name = createModel.Name;
                    vocabularyRequest.Code = createModel.Code;
                    vocabularyRequest.ApplicationId = createModel.ApplicationId.HasValue ? createModel.ApplicationId : null;
                    vocabularyRequest.CreatedByUserId = createModel.UserId.HasValue ? createModel.UserId.Value : _userId.Value;
                    vocabularyRequest.CreatedOnDate = DateTime.Now;
                    vocabularyRequest.Description = createModel.Description;
                    DbTaxonomyVocabularyHandler vocabularyHandler = new DbTaxonomyVocabularyHandler();
                    var vocabularyRepo = vocabularyHandler.Create(vocabularyRequest, unitOfWork);

                    #endregion

                    #region Step 3. Create metadataTemplate
                    DbMetadataTemplateHandler metadataTemplateHandler = new DbMetadataTemplateHandler();
                    if (createModel.TemplateModel == null)
                    {
                        createModel.TemplateModel = new MetadataTemplateCreateRequestModel();
                    }
                    createModel.TemplateModel.Code = !string.IsNullOrEmpty(createModel.TemplateModel.Code) ? createModel.TemplateModel.Code : createModel.Code + "_template_v1";
                    createModel.TemplateModel.Name = !string.IsNullOrEmpty(createModel.TemplateModel.Name) ? createModel.TemplateModel.Name : createModel.Name + "_template_v1";
                    createModel.TemplateModel.ApplicationId = createModel.TemplateModel.ApplicationId.HasValue ? createModel.TemplateModel.ApplicationId.Value : createModel.ApplicationId;
                    createModel.TemplateModel.UserId = createModel.TemplateModel.UserId.HasValue ? createModel.TemplateModel.UserId.Value : _userId.Value;
                    var metadataTemplateRepo = metadataTemplateHandler.Create(createModel.TemplateModel, unitOfWork);
                    #endregion

                    #region Step 4.Create new Catalog master 
                    var data = new CatalogMaster();
                    data.CatalogMasterId = Guid.NewGuid();
                    data.Name = createModel.Name;
                    data.Code = createModel.Code;
                    data.MappedTermId = termRepo.Data.TermId;
                    data.Description = createModel.Description;
                    data.Status = createModel.Status;
                    data.MappedVocabularyId = vocabularyRepo.Data.VocabularyId;
                    data.MetadataTemplateId = metadataTemplateRepo.Data.MetadataTemplateId;
                    data.CreatedOnDate = DateTime.Now;
                    data.LastModifiedOnDate = DateTime.Now;
                    data.CreatedByUserId = createModel.UserId.HasValue ? createModel.UserId.Value : _userId.Value;
                    data.LastModifiedByUserId = createModel.UserId.HasValue ? createModel.UserId.Value : _userId.Value;
                    //executed query
                    newCatalog.Add(data);
                    //Save all handller
                    var result = unitOfWork.Save();
                    #endregion

                    if (result >= 1)
                    {
                        //Task.Run(() => TaxonomyVocabularyCollection.Instance.LoadToHashset());
                        // TaxonomyVocabularyCollection.Instance.LoadToHashset();
                        return new OldResponse<CatalogMasterModel>(1, "Thêm mới thành công: " + JsonConvert.SerializeObject(createModel), ConvertData(data));
                        //undone: Có chuyển từ hàm convert sang hàm get by id không?
                    }
                    else
                    {
                        Log.Error("SQL can not process query ");
                        return new OldResponse<CatalogMasterModel>(0, "Lỗi khi thêm vào cơ sở dữ liệu!", null);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error("Create error", ex);
                return new OldResponse<CatalogMasterModel>(-1, "Lỗi ngoại lệ!" + ex.Message, null);
            }
        }

        /// <summary>
        /// update catalog
        /// </summary>
        /// <param name="updateModel"></param>
        /// <returns></returns>
        public OldResponse<CatalogMasterModel> Update(CatalogMasterUpdateRequestModel request)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    #region Step1. Update Catalog 
                    var repoCatalog = unitOfWork.GetRepository<CatalogMaster>();
                    var current = repoCatalog.Find(request.CatalogMasterId);
                    #region Checkin
                    if (current == null)
                    {
                        Log.Error("Id not found +'" + request.CatalogMasterId + "'");
                        return new OldResponse<CatalogMasterModel>(0, "Id not found +'" + request.CatalogMasterId + "'", null);
                    }
                    //check new Name
                    if (request.Name != current.Name)
                    {
                        var currentHasUniqueName = repoCatalog.Get(s => s.Name == request.Name).FirstOrDefault();
                        if (currentHasUniqueName != null)
                        {
                            Log.Error("Name:" + request.Name + "' is exiest !");
                            return new OldResponse<CatalogMasterModel>(0, "Tên danh mục: " + request.Name + "' đã tồn tại!", null);
                        }
                    }

                    //check new Code
                    if (request.Code != current.Code)
                    {
                        var currentHasCode = repoCatalog.Get(s => s.Code == request.Code).FirstOrDefault();
                        if (currentHasCode != null)
                        {
                            Log.Error("Name:" + request.Code + "' is exiest !");
                            return new OldResponse<CatalogMasterModel>(0, "Mã danh mục:" + request.Code + "' đã tồn tại!", null);
                        }
                    }
                    #endregion
                    if (!string.IsNullOrEmpty(request.Name))
                    {
                        current.Name = request.Name;
                    }
                    if (!string.IsNullOrEmpty(request.Code))
                    {
                        current.Code = request.Code;
                    }

                    current.Description = request.Description;
                    current.LastModifiedOnDate = DateTime.Now;
                    current.LastModifiedByUserId = request.LastModifiedByUserId.HasValue ? request.LastModifiedByUserId.Value : Guid.Empty;
                    current.Status = request.Status;
                    #endregion

                    #region  Step2. Update Vocabulary
                    var repoMapVocabulary = unitOfWork.GetRepository<TaxonomyVocabulary>();
                    var currentVocabulary = repoMapVocabulary.Find(current.MappedVocabularyId);
                    if (!string.IsNullOrEmpty(request.Name))
                    {
                        currentVocabulary.Name = request.Name;
                    }
                    if (!string.IsNullOrEmpty(request.Code))
                    {
                        currentVocabulary.Code = request.Code;
                    }
                    if (!string.IsNullOrEmpty(request.Description))
                    {
                        currentVocabulary.Description = request.Description;
                    }
                    repoMapVocabulary.Update(currentVocabulary);

                    repoCatalog.Update(current);
                    #endregion

                    #region Step 3. Update Term

                    DbTaxonomyTermsHandler termHandler = new DbTaxonomyTermsHandler();
                    var repoMapTerm = new TaxonomyTermUpdateRequestModel();
                    repoMapTerm.TermId = current.MappedTermId;
                    repoMapTerm.Order = request.Order;
                    //todo: Khi update cha: chú ý, ko được chọn cha là con của term hiện tại
                    //đã bắt lỗi trong handler term nhưng gọi ở đây ko có message trả về
                    //nên có thể bắt trên giao diện chọn cha của danh mục, ko load danh mục con của nó lên.... 
                    repoMapTerm.ParentTerm = new ParentTaxonomyTermModel();
                    if (request.ParentTermId != null)
                    {
                        if (request.ParentTermId != Guid.Empty)
                        {
                            repoMapTerm.ParentTerm.TermId = request.ParentTermId;
                        }
                    }
                    repoMapTerm.LastModifiedOnDate = DateTime.Now;
                    repoMapTerm.LastModifiedByUserId = current.LastModifiedByUserId.Value;

                    if (!string.IsNullOrEmpty(request.Name))
                    {
                        repoMapTerm.Name = request.Name;
                    }

                    if (!string.IsNullOrEmpty(request.Description))
                    {
                        repoMapTerm.Description = request.Description;
                    }

                    var termRepo = termHandler.Update(repoMapTerm, unitOfWork);

                    #endregion

                    #region Step 4. Update Template

                    DbMetadataTemplateHandler templateHander = new DbMetadataTemplateHandler();

                    if (request.TemplateModel != null)
                    {
                        request.TemplateModel.ApplicationId = request.TemplateModel.ApplicationId.HasValue ? request.TemplateModel.ApplicationId.Value : _applicationId.Value;
                        request.TemplateModel.UserId = request.TemplateModel.UserId.HasValue ? request.TemplateModel.UserId.Value : _userId.Value;
                        var templateRepo = templateHander.Update(request.TemplateModel, unitOfWork);
                    }
                    #endregion
                    var result = unitOfWork.Save();

                    if (result >= 1)
                    {
                        Log.Error("Update successfull with request:" + JsonConvert.SerializeObject(request));
                        return new OldResponse<CatalogMasterModel>(1, "Sửa danh mục thành công:" + JsonConvert.SerializeObject(request), ConvertData(current));
                    }
                    else
                    {
                        Log.Error("SQL can not process query ");
                        return new OldResponse<CatalogMasterModel>(0, "Không sửa được", null);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error("Update error ", ex);
                return new OldResponse<CatalogMasterModel>(-1, "Lỗi ngoại lệ, không sửa được!" + ex.Message, null);
            }
        }

        /// <summary>
        /// Delete a catalog by Id
        /// </summary>
        /// <param name="catalogMasterId"></param>
        /// <returns></returns>
        public OldResponse<CatalogMasterDeleteResponseModel> Delete(Guid catalogMasterId)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    var deleteResponse = new OldResponse<CatalogMasterDeleteResponseModel>(1, "", new CatalogMasterDeleteResponseModel());
                    //var deleteResponse = new OldResponse<TaxonomyTermDeleteResponseModel>(0, string.Empty, new TaxonomyTermDeleteResponseModel());
                    var catalogMasterRepo = unitOfWork.GetRepository<CatalogMaster>();

                    //check exits
                    var current = catalogMasterRepo.Find(catalogMasterId);
                    if (current == null)
                    {
                        Log.Error("Catalog Master Id not found: '" + catalogMasterId + "'");
                        deleteResponse.Message = "Không tìm thấy danh mục này: '" + catalogMasterId + "'";
                        deleteResponse.Data.Message = "Không tìm thấy danh mục này: '" + catalogMasterId + "'";
                        deleteResponse.Data.Result = 0;
                        deleteResponse.Data.CatalogMasterId = catalogMasterId;
                        return deleteResponse;
                    }
                    else
                    {
                        var taxonomyTermsRepo = unitOfWork.GetRepository<TaxonomyTerm>();
                        var instant = taxonomyTermsRepo.GetAll().Where(x => x.VocabularyId == current.MappedVocabularyId).FirstOrDefault();
                        if (instant != null)
                        {
                            Log.Error("SQL can not process query");
                            deleteResponse.Message = "Danh mục đã chứa dữ liệu, không thể xóa!";
                            deleteResponse.Data = new CatalogMasterDeleteResponseModel() { CatalogMasterId = current.CatalogMasterId, Name = current.Name, Result = 0, Message = "Không xóa được" };
                            deleteResponse.Status = 0;
                            return deleteResponse;
                        }
                        var vocaRepo = unitOfWork.GetRepository<TaxonomyVocabulary>();
                        var voca = vocaRepo.Find(current.MappedVocabularyId);
                        //var metaTemplate = unitOfWork.GetRepository<Metadata_Template>();

                        var dTaxonomyTerm = taxonomyTermsRepo.Find(current.MappedTermId);
                        if (dTaxonomyTerm != null)
                        {
                            var childTermList = (from t in unitOfWork.GetRepository<TaxonomyTerm>().GetAll()
                                                 where t.ParentId == dTaxonomyTerm.TermId
                                                 select t).FirstOrDefault();
                            if (childTermList == null)
                            {

                                //Xóa template
                                var tempRelRepo = unitOfWork.GetRepository<MetadataFieldTemplateRelationship>();
                                var tempRel = tempRelRepo.Get(m => m.MetadataTemplateId == current.MetadataTemplateId).FirstOrDefault();
                                var tempRepo = unitOfWork.GetRepository<MetadataTemplate>();
                                var temp = tempRepo.Find(current.MetadataTemplateId);
                                //xóa term
                                //xóa catalog master
                                taxonomyTermsRepo.Delete(dTaxonomyTerm);
                                catalogMasterRepo.Delete(current);
                                if (tempRel != null)
                                    tempRelRepo.Delete(tempRel);
                                tempRepo.Delete(temp);
                                vocaRepo.Delete(voca);

                                if (unitOfWork.Save() >= 1)
                                {
                                    Log.Error("delete sucess");
                                    deleteResponse.Data = new CatalogMasterDeleteResponseModel() { CatalogMasterId = current.CatalogMasterId, Name = current.Name, Result = 1, Message = "Xóa thành công" };
                                    deleteResponse.Status = 1;
                                }
                                else
                                {
                                    Log.Error("SQL can not process query");
                                    deleteResponse.Data = new CatalogMasterDeleteResponseModel() { CatalogMasterId = current.CatalogMasterId, Name = current.Name, Result = 0, Message = "Không xóa được" };
                                    deleteResponse.Status = 0;
                                }
                            }
                            else
                            {
                                Log.Error("SQL can not process query");
                                deleteResponse.Data = new CatalogMasterDeleteResponseModel() { CatalogMasterId = current.CatalogMasterId, Name = current.Name, Result = 0, Message = "Không xóa được" };
                                deleteResponse.Status = 0;
                            }
                        }
                        else
                        {
                            Log.Error("SQL can not process query");
                            deleteResponse.Data = null;
                            deleteResponse.Message = "Không tìm thấy bản ghi!";
                            deleteResponse.Status = 0;
                        }
                        //Không xóa nếu danh mục đã được sử dụng
                        //check bảng field danh mục động
                        var armFieldRepo = unitOfWork.GetRepository<CatalogField>();
                        var dArmFieldRepo = armFieldRepo.GetAll().Where(x => x.CatalogMasterId == catalogMasterId).FirstOrDefault();
                        if (dArmFieldRepo != null)
                        {
                            Log.Error("SQL can not process query");
                            deleteResponse.Message = "Danh mục đang được sử dụng, không thể xóa!";
                            deleteResponse.Data = new CatalogMasterDeleteResponseModel()
                            {
                                CatalogMasterId = current.CatalogMasterId,
                                Name = current.Name,
                                Result = 0,
                                Message = "Danh mục đang được sử dụng, không thể xóa!"
                            };
                            deleteResponse.Status = 0;
                            return deleteResponse;
                        }
                    }
                    return deleteResponse;
                }
            }
            catch (Exception ex)
            {
                Log.Error("Delete constraint error" + ex.Message, ex);
                return new OldResponse<CatalogMasterDeleteResponseModel>(-1, "Danh mục chứa ràng buộc, không xóa được", null);
            }
        }

        /// <summary>
        /// delete list catalog
        /// </summary>
        /// <param name="listCatalogMasterId"></param>
        /// <returns></returns>
        public OldResponse<IList<CatalogMasterDeleteResponseModel>> DeleteMany(IList<Guid> listCatalogMasterId)
        {
            var result = new OldResponse<IList<CatalogMasterDeleteResponseModel>>(1, "", new List<CatalogMasterDeleteResponseModel>());
            foreach (var item in listCatalogMasterId)
            {
                var deleteResult = Delete(item);
                result.Data.Add(deleteResult.Data);
            }
            return result;
        }


        public OldResponse<CatalogMasterModel> GetByCatalogIdAndMetadataFieldId(Guid CatalogId, Guid metadataFieldId)
        {
            throw new NotImplementedException();
        }

        private CatalogMasterModel ConvertData(CatalogMaster model)
        {
            CatalogMasterModel result = new CatalogMasterModel()
            {
                CatalogMasterId = model.CatalogMasterId,
                CreatedByUserId = model.CreatedByUserId.Value,
                LastModifiedByUserId = model.CreatedByUserId.Value,
                CreatedOnDate = model.CreatedOnDate,
                LastModifiedOnDate = model.LastModifiedOnDate,
                Name = model.Name,
                Code = model.Code,
                MetadataTemplateId = model.MetadataTemplateId,
                TermId = model.MappedTermId,
                VocabularyId = model.MappedVocabularyId,
                Status = model.Status,
                Description = model.Description,
            };

            return result;
        }
    }
}