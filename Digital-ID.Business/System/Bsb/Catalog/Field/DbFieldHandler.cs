﻿using DigitalID.Data;
using Newtonsoft.Json;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DigitalID.Business
{
    public class DbFieldHandler : IFieldHandler
    {
        private Guid? _applicationId { get; set; }
        private Guid? _userId { get; set; }

        public void init(Guid applicationId, Guid userId)
        {
            _applicationId = applicationId;
            _userId = userId;
        }

        public static FieldModel ConvertData(CatalogField model)
        {
            FieldModel result = new FieldModel()
            {
                CreatedByUser = new BaseUserModel() { Id = model.CreatedByUserId },
                LastModifiedByUser = new BaseUserModel() { Id = model.LastModifiedByUserId },
                CreatedOnDate = model.CreatedOnDate,
                LastModifiedOnDate = model.LastModifiedOnDate,
                FieldId = model.FieldId,
                FormlyContent = model.FormlyContent,
                IsRecordCommonField = model.IsRecordCommonField,
                IsDocumentCommonField = model.IsDocumentCommonField,
                Name = model.Name,
                DisplayName = model.DisplayName,
                Code = model.Code,
                DisplayCatalog = model.DisplayCatalog,
                DisplayReport = model.DisplayReport,
                DisplayReader = model.DisplayReader,
                DisplayReportSearch = model.DisplayReportSearch,
                DisplayReaderSearch = model.DisplayReaderSearch,
                Description = model.Description,
                CatalogMasterId = model.CatalogMasterId,
                DataType = model.DataType,
                CatalogCode = model.CatalogCode,
                FieldWidth = model.FieldWidth,
            };
            return result;
        }

        #region CRUD

        public OldResponse<IList<FieldModel>> GetAll()
        {
            try
            {
                // Create result variable
                var result = new OldResponse<IList<FieldModel>>(0, string.Empty, new List<FieldModel>(), 0, 0);

                using (var unitOfWork = new UnitOfWork())
                {
                    var datas = unitOfWork.GetRepository<CatalogField>().GetAll();
                    var fullQueryData = datas.ToList();
                    // Convert Data
                    if (fullQueryData != null && fullQueryData.Count() >= 0)
                    {
                        result.Message = "Success get all";
                        result.Status = 1;
                        result.DataCount = fullQueryData.Count();
                        result.TotalCount = fullQueryData.Count(); ;
                        foreach (var item in fullQueryData)
                        {
                            result.Data.Add(ConvertData(item));
                        }
                    }
                    return result;
                }
            }
            catch (Exception ex)
            {
                Log.Error("Success get all", ex);
                return new OldResponse<IList<FieldModel>>(-1, "Success get all", null, 0, 0);
            }
        }
        public OldResponse<IList<FieldModel>> GetFilter(FieldQueryFilter filter)
        {
            try
            {
                // Create result variable
                var result = new OldResponse<IList<FieldModel>>(0, string.Empty, new List<FieldModel>(), 0, 0);

                using (var unitOfWork = new UnitOfWork())
                {
                    var datas = unitOfWork.GetRepository<CatalogField>().GetAll();
                    //filter
                    #region filter
                    //by FieldId
                    if (filter.FieldId.HasValue)
                    {
                        datas = from dt in datas
                                where dt.FieldId == filter.FieldId.Value
                                select dt;
                    }
                    //by IsDocumentField
                    if (filter.IsDocumentCommonField.HasValue)
                    {
                        datas = from dt in datas
                                where dt.IsDocumentCommonField == filter.IsDocumentCommonField.Value
                                select dt;
                    }
                    if (filter.IsRecordCommonField.HasValue)
                    {
                        datas = from dt in datas
                                where dt.IsRecordCommonField == filter.IsRecordCommonField.Value
                                select dt;
                    }
                    if (filter.CatalogMasterId.HasValue)
                    {
                        datas = from dt in datas
                                where dt.CatalogMasterId == filter.CatalogMasterId.Value
                                select dt;
                    }
                    if (filter.DisplayCatalog.HasValue) { datas = from dt in datas where dt.DisplayCatalog == filter.DisplayCatalog.Value select dt; }
                    if (filter.DisplayReport.HasValue) { datas = from dt in datas where dt.DisplayReport == filter.DisplayReport.Value select dt; }
                    if (filter.DisplayReader.HasValue) { datas = from dt in datas where dt.DisplayReader == filter.DisplayReader.Value select dt; }
                    if (filter.DisplayReportSearch.HasValue) { datas = from dt in datas where dt.DisplayReportSearch == filter.DisplayReportSearch.Value select dt; }
                    if (filter.DisplayReaderSearch.HasValue) { datas = from dt in datas where dt.DisplayReaderSearch == filter.DisplayReaderSearch.Value select dt; }

                    if (!string.IsNullOrEmpty(filter.Code))
                    {
                        datas = from dt in datas
                                where dt.Code == filter.Code
                                select dt;
                    }
                    //by  TextSearch
                    if (!string.IsNullOrEmpty(filter.TextSearch))
                    {
                        filter.TextSearch = filter.TextSearch.ToLower().Trim();
                        datas = from dt in datas
                                where dt.Name.ToLower().Contains(filter.TextSearch)
                                //|| dt.ColumnName.ToLower().Contains(filter.TextSearch) 
                                select dt;
                    }
                    #endregion
                    var totalCount = datas.Count();
                    //Order
                    #region ordering
                    switch (filter.Order)
                    {
                        case FieldQueryOrder.CREATE_DATE_ASC:
                            datas = datas.OrderBy(sp => sp.CreatedOnDate);
                            break;
                        case FieldQueryOrder.CREATE_DATE_DESC:
                            datas = datas.OrderByDescending(sp => sp.CreatedOnDate);
                            break;
                        case FieldQueryOrder.MODIFIED_DATE_ASC:
                            datas = datas.OrderBy(sp => sp.LastModifiedOnDate);
                            break;
                        case FieldQueryOrder.MODIFIED_DATE_DESC:
                            datas = datas.OrderByDescending(sp => sp.LastModifiedOnDate);
                            break;
                        default:
                            datas = datas.OrderByDescending(sp => sp.LastModifiedOnDate);
                            break;
                    }
                    #endregion
                    // Pagination
                    #region Pagination
                    if (filter.PageSize.HasValue && filter.PageNumber.HasValue)
                    {
                        if (filter.PageSize.Value <= 0) filter.PageSize = 20;

                        filter.PageNumber = filter.PageNumber - 1;
                        //Calculate nunber of rows to skip on pagesize
                        int excludedRows = (filter.PageNumber.Value) * (filter.PageSize.Value);
                        if (excludedRows <= 0) excludedRows = 0;

                        datas = datas.Skip(excludedRows).Take(filter.PageSize.Value);
                    }
                    #endregion

                    var fullQueryData = datas.ToList();
                    // Convert Data
                    if (fullQueryData != null && fullQueryData.Count() >= 0)
                    {
                        result.Message = "Success get filter with filter : " + JsonConvert.SerializeObject(filter);
                        result.Status = 1;
                        result.DataCount = fullQueryData.Count();
                        result.TotalCount = totalCount;
                        foreach (var item in fullQueryData)
                        {
                            result.Data.Add(ConvertData(item));
                        }
                    }
                    return result;
                }
            }
            catch (Exception ex)
            {
                Log.Error("Error get filter with filter: " + JsonConvert.SerializeObject(filter), ex);
                return new OldResponse<IList<FieldModel>>(-1, "Error get filter with filter: " + JsonConvert.SerializeObject(filter), null, 0, 0);
            }
        }
        public OldResponse<FieldModel> GetById(Guid FieldId)
        {
            var filter = new FieldQueryFilter() { FieldId = FieldId };
            var response = GetFilter(filter);
            if (response.Status == 1)
            {
                return new OldResponse<FieldModel>(1, "Success", response.Data.FirstOrDefault());
            }
            else
            {
                return new OldResponse<FieldModel>(response.Status, response.Message, null);
            }
        }
        public OldResponse<FieldModel> Create(FieldCreateRequestModel request)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    var repo = unitOfWork.GetRepository<CatalogField>();
                    //check unique Name 
                    var check = repo.GetMany(s =>
                    //s.ColumnName == request.ColumnName||
                    s.Name == request.Name).FirstOrDefault();
                    if (check != null)
                    {
                        Log.Error(
                            //"ColumnName:"+ request.ColumnName + 
                            "  Name :" + request.Name + "' is exiest !");
                        return new OldResponse<FieldModel>(0,
                            //"ColumnName:"+ request.ColumnName + 
                            "  Name :" + request.Name + "' is exiest !", null);
                    }



                    var data = new CatalogField();
                    data.FieldId = Guid.NewGuid();
                    data.CreatedOnDate = DateTime.Now;
                    data.LastModifiedOnDate = DateTime.Now;

                    data.CreatedByUserId = request.UserId.HasValue ? request.UserId.Value : _userId.Value;
                    data.LastModifiedByUserId = request.UserId.HasValue ? request.UserId.Value : _userId.Value;
                    data.Name = request.Name;
                    data.DisplayName = request.DisplayName;
                    data.Code = request.Code;
                    data.Description = request.Description;
                    data.FormlyContent = request.FormlyContent;
                    data.IsDocumentCommonField = request.IsDocumentCommonField;
                    data.IsRecordCommonField = request.IsRecordCommonField;

                    data.DisplayCatalog = request.DisplayCatalog;
                    data.DisplayReport = request.DisplayReport;
                    data.DisplayReader = request.DisplayReader;
                    data.DisplayReportSearch = request.DisplayReportSearch;
                    data.DisplayReaderSearch = request.DisplayReaderSearch;
                    data.CatalogMasterId = request.CatalogMasterId;
                    data.FieldWidth = request.FieldWidth;
                    data.CatalogCode = request.CatalogCode;
                    data.DataType = request.DataType;
                    //exe query
                    repo.Add(data);
                    var result = unitOfWork.Save();
                    if (result >= 1)
                    {
                        CreateSub(data.FieldId, request);
                        //Task.Run(() => FieldCollection.Instance.LoadToHashSet());
                        // FieldCollection.Instance.LoadToHashSet();
                        Log.Error("create successfull with request :" + JsonConvert.SerializeObject(request));
                        return new OldResponse<FieldModel>(1, "create successfull with request :" + JsonConvert.SerializeObject(request), ConvertData(data));
                    }
                    else
                    {
                        Log.Error("SQL can not process query ");
                        return new OldResponse<FieldModel>(0, "SQL can not process query", null);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error("Create error", ex);
                return new OldResponse<FieldModel>(-1, "Create error" + ex.Message, null);
            }
        }
        private void CreateSub(Guid FieldId, FieldCreateRequestModel request)
        {
            try
            {
                /**Connect to server*/
                //var uri = ConfigurationManager.AppSettings["elsaticsearch:apiuri"];
                //var index = ConfigurationManager.AppSettings["elsaticsearch:index"];
                //var type = ConfigurationManager.AppSettings["elsaticsearch:type"];
                //var node = new Uri(uri);
                //var settings = new ConnectionSettings(node);
                //settings.DefaultFieldNameInferrer(p => p);
                //var client = new ElasticClient(settings);
                ////
                //IndexRequest<object> indexRequest = new IndexRequest<object>(putData, index, type, RecordId.ToString().ToLower());
                //client.Index<object>(indexRequest);
            }
            catch (Exception ex)
            {
                Log.Error(ex,"");
            }
        }
        public OldResponse<FieldModel> Update(FieldUpdateRequestModel request)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    //check exest
                    var repo = unitOfWork.GetRepository<CatalogField>();
                    var current = repo.Find(request.FieldId);
                    if (current == null)
                    {
                        Log.Error("Time constraint Id not found +'" + request.FieldId + "'");
                        return new OldResponse<FieldModel>(0, "Time constraint Id not found +'" + request.FieldId + "'", null);
                    }
                    //check new unique name
                    if (
                        //request.ColumnName != current.ColumnName&& 
                        request.Name != current.Name)
                    {
                        var currentHasUniqueName = repo.GetMany(s =>
                        //s.ColumnName == request.ColumnName || 
                        s.Name == request.Name).FirstOrDefault();
                        if (currentHasUniqueName != null)
                        {
                            Log.Error(
                                //"ColumnName:" + request.ColumnName + 
                                " Name :" + request.Name + "' is exiest !");
                            return new OldResponse<FieldModel>(0,
                                //"ColumnName:" + request.ColumnName +
                                " Name :" + request.Name + "' is exiest !", null);
                        }
                    }
                    //Check đổi kiểu dữ liệu
                    if (request.IsCreateNew)
                    {
                        //Bảng RecordAttribute //  
                        //var currentAtt =   unitOfWork.GetRepository<arm_Record_Atribute>().GetMany(s => s.FieldId == current.FieldId);
                        //foreach (var item in currentAtt)
                        //{
                        //    item.DataType = request.DataType;
                        //    unitOfWork.GetRepository<arm_Record_Atribute>().Update(item);
                        //} 
                    }
                    //start update
                    current.LastModifiedOnDate = DateTime.Now;
                    current.LastModifiedByUserId = request.UserId.HasValue ? request.UserId.Value : _userId.Value;
                    current.Name = request.Name;
                    current.DisplayName = request.DisplayName;
                    current.Code = request.Code;
                    current.Description = request.Description;
                    current.FormlyContent = request.FormlyContent;
                    current.IsDocumentCommonField = request.IsDocumentCommonField;
                    current.IsRecordCommonField = request.IsRecordCommonField;
                    current.DisplayCatalog = request.DisplayCatalog;
                    current.DisplayReport = request.DisplayReport;
                    current.DisplayReader = request.DisplayReader;
                    current.DisplayReportSearch = request.DisplayReportSearch;
                    current.DisplayReaderSearch = request.DisplayReaderSearch;
                    current.CatalogMasterId = request.CatalogMasterId;

                    current.FieldWidth = request.FieldWidth;
                    current.CatalogCode = request.CatalogCode;
                    current.DataType = request.DataType;
                    repo.Update(current);
                    var result = unitOfWork.Save();
                    if (result >= 1)
                    {
                        UpdateSub(request.FieldId, request);
                        //Task.Run(() => FieldCollection.Instance.LoadToHashSet());
                        // FieldCollection.Instance.LoadToHashSet();
                        Log.Error("Update successfull with request :" + JsonConvert.SerializeObject(request));
                        return new OldResponse<FieldModel>(1, "create successfull with request :" + JsonConvert.SerializeObject(request), ConvertData(current));
                    }
                    else
                    {
                        Log.Error("SQL can not process query ");
                        return new OldResponse<FieldModel>(0, "SQL can not process query", null);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error("Update error", ex);
                return new OldResponse<FieldModel>(-1, "Update error" + ex.Message, null);
            }
        }

        private void UpdateSub(Guid FieldId, FieldUpdateRequestModel request)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    //Check data field
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex,"");
            }
        }
        public OldResponse<FieldDeleteResponseModel> Delete(Guid FieldId)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    var result = new OldResponse<FieldDeleteResponseModel>(1, "", new FieldDeleteResponseModel());

                    result.Data.Model = new BaseFieldModel();

                    //check exitest
                    var repo = unitOfWork.GetRepository<CatalogField>();
                    var current = repo.Find(FieldId);
                    if (current == null)
                    {
                        Log.Error(" FieldId not found +'" + FieldId + "'");
                        result.Message = " FieldId not found +'" + FieldId + "'";
                        result.Data.Message = " FieldId not found +'" + FieldId + "'";
                        result.Data.Result = false;
                        result.Data.Model.FieldId = FieldId;
                        return result;
                    }
                    //check Instance
                    repo.Delete(current);
                    var processSQL = unitOfWork.Save();
                    if (processSQL >= 1)
                    {
                        //Task.Run(() => FieldCollection.Instance.LoadToHashSet());
                        // FieldCollection.Instance.LoadToHashSet();
                        Log.Error("delete sucess");
                        result.Message = "delete sucess";
                        result.Data.Message = "delete sucess";
                        result.Data.Result = true;
                        result.Data.Model.FieldId = FieldId;
                        result.Data.Model.DisplayName = current.DisplayName;
                        result.Data.Model.Name = current.Name;
                        result.Data.Model.Code = current.Code;
                        result.Data.Model.Description = current.Description;
                        result.Data.Model.FormlyContent = current.FormlyContent;
                        result.Data.Model.IsDocumentCommonField = current.IsDocumentCommonField;
                        result.Data.Model.IsRecordCommonField = current.IsRecordCommonField;

                        return result;
                    }
                    else
                    {
                        Log.Error("SQL can not process query");
                        result.Message = "SQL can not process query";
                        result.Data.Message = "SQL can not process query";
                        result.Data.Result = false;
                        result.Data.Model.FieldId = FieldId;
                        result.Data.Model.DisplayName = current.DisplayName;
                        result.Data.Model.Name = current.Name;
                        result.Data.Model.Code = current.Code;
                        result.Data.Model.Description = current.Description;
                        result.Data.Model.FormlyContent = current.FormlyContent;
                        result.Data.Model.IsDocumentCommonField = current.IsDocumentCommonField;
                        result.Data.Model.IsRecordCommonField = current.IsRecordCommonField;

                        return result;
                    }
                }
            }
            catch (Exception ex)
            {

                Log.Error("Delete time constraint error", ex);
                var result = new OldResponse<FieldDeleteResponseModel>(1, "", new FieldDeleteResponseModel());

                result.Data.Model = new BaseFieldModel();
                Log.Error("Delete error");
                result.Message = "Delete error";
                result.Data.Message = "Delete error";
                result.Data.Result = false;
                result.Data.Model.FieldId = FieldId;
                return result;
            }

        }

        public OldResponse<IList<FieldDeleteResponseModel>> DeleteMany(IList<Guid> ListFieldId)
        {
            var result = new OldResponse<IList<FieldDeleteResponseModel>>(1, "", new List<FieldDeleteResponseModel>());
            foreach (var item in ListFieldId)
            {
                var deleteResult = Delete(item);
                result.Data.Add(deleteResult.Data);
            }
            return result;
        }
        #endregion

    }
}
