﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DigitalID.Business
{
    public class FieldCollection
    {
        private readonly IFieldHandler _fieldHandler;
        private HashSet<FieldModel> collection;

        public FieldCollection(IFieldHandler fieldHandler)
        {
            _fieldHandler = fieldHandler;
            LoadToHashSet();
        }

        public void LoadToHashSet()
        {
            collection = new HashSet<FieldModel>();

            // Query to list
            var listResponse = _fieldHandler.GetAll();

            // Add to hashset

            foreach (var response in listResponse.Data)
            {
                collection.Add(response);
            }
        }

        public FieldModel GetById(Guid id)
        {
            var result = collection.FirstOrDefault(u => u.FieldId == id);
            return result;
        }
    }
}