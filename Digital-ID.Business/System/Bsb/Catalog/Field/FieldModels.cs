﻿
using System;
using System.Collections.Generic;

namespace DigitalID.Business
{
    #region Main
    public class BaseFieldModel
    {
        public Guid FieldId { get; set; }
        public string FormlyContent { get; set; }
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public bool IsRecordCommonField { get; set; }
        public bool IsDocumentCommonField { get; set; }
        public bool? DisplayCatalog { get; set; }
        public bool? DisplayReport { get; set; }
        public bool? DisplayReader { get; set; }
        public bool DisplayReportSearch { get; set; }
        public bool DisplayReaderSearch { get; set; }
        public Guid? CatalogMasterId { get; set; }
        public string DataType { get; set; }
        public string CatalogCode { get; set; }
        public int? FieldWidth { get; set; }

    }
    public class FieldModel : BaseFieldModel
    {
        public DateTime CreatedOnDate { get; set; }
        public DateTime LastModifiedOnDate { get; set; }
        public BaseUserModel CreatedByUser { get; set; }
        public BaseUserModel LastModifiedByUser { get; set; }

    }

    public class FieldCreateRequestModel
    {
        public string FormlyContent { get; set; }
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public bool IsRecordCommonField { get; set; }
        public bool IsDocumentCommonField { get; set; }
        public bool DisplayCatalog { get; set; }
        public bool DisplayReport { get; set; }
        public bool DisplayReader { get; set; }
        public bool DisplayReportSearch { get; set; }
        public bool DisplayReaderSearch { get; set; }
        public Guid? CatalogMasterId { get; set; }
        //
        public Guid? UserId { get; set; }
        public string DataType { get; set; }
        public string CatalogCode { get; set; }
        public int? FieldWidth { get; set; }
    }

    public class FieldUpdateRequestModel
    {

        public Guid FieldId { get; set; }
        public string FormlyContent { get; set; }
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public bool IsRecordCommonField { get; set; }
        public bool IsDocumentCommonField { get; set; }
        public bool DisplayCatalog { get; set; }
        public bool DisplayReport { get; set; }
        public bool DisplayReader { get; set; }
        public bool DisplayReportSearch { get; set; }
        public bool DisplayReaderSearch { get; set; }
        public Guid? CatalogMasterId { get; set; }
        public Guid? UserId { get; set; }
        public string DataType { get; set; }
        public string CatalogCode { get; set; }
        public int? FieldWidth { get; set; }
        public bool IsCreateNew { get; set; }
    }

    public class FieldQueryFilter
    {
        public int? PageSize { get; set; }
        public int? PageNumber { get; set; }
        public string TextSearch { get; set; }
        //
        public Guid? FieldId { get; set; }
        public string Code { get; set; }
        public bool? IsRecordCommonField { get; set; }
        public bool? IsDocumentCommonField { get; set; }
        public bool? DisplayCatalog { get; set; }
        public bool? DisplayReport { get; set; }
        public bool? DisplayReader { get; set; }
        public bool? DisplayReportSearch { get; set; }
        public bool? DisplayReaderSearch { get; set; }
        public Guid? CatalogMasterId { get; set; }

        public FieldQueryOrder Order { get; set; }
        public FieldQueryFilter()
        {
            TextSearch = string.Empty;
            Order = FieldQueryOrder.CREATE_DATE_ASC;

        }
    }

    public class FieldDeleteResponseModel
    {
        public BaseFieldModel Model { get; set; }
        public bool Result { get; set; }
        public string Message { get; set; }
    }
    public class FieldDeleteRequestModel
    {
        public IList<Guid> ListId { get; set; }
    }
    public enum FieldQueryOrder
    {
        CREATE_DATE_ASC,
        MODIFIED_DATE_ASC,

        //
        CREATE_DATE_DESC,
        MODIFIED_DATE_DESC,

    }
    #endregion
}
