﻿using DigitalID.Data;
using System;
using System.Collections.Generic;

namespace DigitalID.Business
{
    public interface IFieldHandler
    {
        void init(Guid applicationId, Guid userId);
        #region CRUD
        OldResponse<IList<FieldModel>> GetAll();
        OldResponse<IList<FieldModel>> GetFilter(FieldQueryFilter filter);
        OldResponse<FieldModel> GetById(Guid FieldId);
        OldResponse<FieldModel> Create(FieldCreateRequestModel request);

        OldResponse<FieldModel> Update(FieldUpdateRequestModel request);
        OldResponse<FieldDeleteResponseModel> Delete(Guid FieldId);

        OldResponse<IList<FieldDeleteResponseModel>> DeleteMany(IList<Guid> ListFieldId);
        #endregion
    }
}
