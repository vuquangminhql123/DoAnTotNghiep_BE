﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DigitalID.Data;

namespace DigitalID.Business
{
    public interface ICatalogHandler
    {
        void init(Guid applicationId, Guid userId);
        OldResponse<CatalogMasterModel> GetByCatalogIdAndMetadataFieldId(Guid CatalogId, Guid metadataFieldId);
        OldResponse<List<CatalogMasterModel>> GetAllCatalog();
        OldResponse<CatalogMasterModel> GetCatalogMasterById(Guid catalogId);
        OldResponse<List<CatalogMasterModel>> GetFilter(CatalogMasterQueryFilter filter);
        OldResponse<List<CatalogMasterModel>> GetCatalogsInParent(Guid parentId);
        OldResponse<List<CatalogMasterClientModel>> GetCatalogViewTree();
        OldResponse<CatalogMasterModel> Create(CatalogMasterCreateRequestModel createModel);
        OldResponse<CatalogMasterModel> Update(CatalogMasterUpdateRequestModel updateModel);
        OldResponse<CatalogMasterDeleteResponseModel> Delete(Guid catalogMasterId);
        OldResponse<IList<CatalogMasterDeleteResponseModel>> DeleteMany(IList<Guid> listCatalogMasterId);
    }
}
