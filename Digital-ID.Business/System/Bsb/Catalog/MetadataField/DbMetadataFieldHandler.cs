﻿using DigitalID.Data;
using Newtonsoft.Json;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DigitalID.Business
{
    public class DbMetadataFieldHandler : IMetadataFieldHandler
    {
        private Guid? _applicationId { get; set; }
        private Guid? _userId { get; set; }

        public void init(Guid applicationId, Guid userId)
        {
            _applicationId = applicationId;
            _userId = userId;
        }

        public static MetadataFieldModel ConvertData(MetadataField model, int order =1)
        {
            MetadataFieldModel result = new MetadataFieldModel()
            {
                CreatedByUser = new BaseUserModel() { Id = model.CreatedByUserId  },
                LastModifiedByUser = new BaseUserModel() { Id = model.LastModifiedByUserId },
                CreatedOnDate = model.CreatedOnDate,
                LastModifiedOnDate = model.LastModifiedOnDate,
                MetadataFieldId = model.MetadataFieldId,
                FormlyContent = model.FormlyContent,
                Name = model.Name,
                Code = model.Code, 
                Description = model.Description,
                ApplicationId = model.ApplicationId,
                Order = order
            };
            return result;
        }
        #region CRUD
        public OldResponse<IList<MetadataFieldModel>> GetFilter(MetadataFieldQueryFilter filter)
        {
            try
            {
                // Create result variable
                var result = new OldResponse<IList<MetadataFieldModel>>(0, string.Empty, new List<MetadataFieldModel>(), 0, 0);

                using (var unitOfWork = new UnitOfWork())
                {
                    var datas = unitOfWork.GetRepository<MetadataField>().GetAll();
                    //filter
                    #region filter
                    //by MetadataFieldId
                    if (filter.MetadataFieldId.HasValue)
                    {
                        datas = from dt in datas
                                where dt.MetadataFieldId == filter.MetadataFieldId.Value
                                select dt;
                    }
                    
                    //by  TextSearch
                    if (!string.IsNullOrEmpty(filter.TextSearch))
                    {
                        filter.TextSearch = filter.TextSearch.ToLower().Trim();
                        datas = from dt in datas
                                where dt.Name.ToLower().Contains(filter.TextSearch) 
                                select dt;
                    }
                    #endregion
                    var totalCount = datas.Count();
                    //Order
                    #region ordering
                    switch (filter.Order)
                    {
                        case MetadataFieldQueryOrder.CREATE_DATE_ASC:
                            datas = datas.OrderBy(sp => sp.CreatedOnDate);
                            break;
                        case MetadataFieldQueryOrder.CREATE_DATE_DESC:
                            datas = datas.OrderByDescending(sp => sp.CreatedOnDate);
                            break;
                        case MetadataFieldQueryOrder.MODIFIED_DATE_ASC:
                            datas = datas.OrderBy(sp => sp.LastModifiedOnDate);
                            break;
                        case MetadataFieldQueryOrder.MODIFIED_DATE_DESC:
                            datas = datas.OrderByDescending(sp => sp.LastModifiedOnDate);
                            break;
                        default:
                            datas = datas.OrderByDescending(sp => sp.LastModifiedOnDate);
                            break;
                    }
                    #endregion
                    // Pagination
                    #region Pagination
                    if (filter.PageSize.HasValue && filter.PageNumber.HasValue)
                    {
                        if (filter.PageSize.Value <= 0) filter.PageSize = 20;

                        filter.PageNumber = filter.PageNumber - 1;
                        //Calculate nunber of rows to skip on pagesize
                        int excludedRows = (filter.PageNumber.Value) * (filter.PageSize.Value);
                        if (excludedRows <= 0) excludedRows = 0;

                        datas = datas.Skip(excludedRows).Take(filter.PageSize.Value);
                    }
                    #endregion

                    var fullQueryData = datas.ToList();
                    // Convert Data
                    if (fullQueryData != null && fullQueryData.Count() >= 0)
                    {
                        result.Message = "Success get filter with filter : " + JsonConvert.SerializeObject(filter);
                        result.Status = 1;
                        result.DataCount = fullQueryData.Count();
                        result.TotalCount = totalCount;
                        foreach (var item in fullQueryData)
                        {
                            result.Data.Add(ConvertData(item));
                        }
                    }
                    return result;
                }
            }
            catch (Exception ex)
            {
                Log.Error("Error get filter with filter: " + JsonConvert.SerializeObject(filter), ex);
                return new OldResponse<IList<MetadataFieldModel>>(-1, "Error get filter with filter: " + JsonConvert.SerializeObject(filter), null, 0, 0);
            }
        }
        public OldResponse<MetadataFieldModel> GetById(Guid MetadataFieldId)
        {
            var filter = new MetadataFieldQueryFilter() { MetadataFieldId = MetadataFieldId };
            var response = GetFilter(filter);
            if (response.Status == 1)
            {
                return new OldResponse<MetadataFieldModel>(1, "Success", response.Data.FirstOrDefault());
            }
            else
            {
                return new OldResponse<MetadataFieldModel>(response.Status, response.Message, null);
            }
        }
        public OldResponse<MetadataFieldModel> Create(MetadataFieldCreateRequestModel request)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    var repo = unitOfWork.GetRepository<MetadataField>();
                    //check unique Name 
                    var check = repo.Get(s =>
                    //s.ColumnName == request.ColumnName||
                    s.Name == request.Name).FirstOrDefault();
                    if (check != null)
                    {
                        Log.Error(
                            //"ColumnName:"+ request.ColumnName + 
                            "  Name :" + request.Name + "' is exiest !");
                        return new OldResponse<MetadataFieldModel>(0,
                            //"ColumnName:"+ request.ColumnName + 
                            "  Name :" + request.Name + "' is exiest !", null);
                    }
                    //check unique Code 
                    var checkCode = repo.Get(s =>
                    //s.ColumnName == request.ColumnName||
                    s.Code == request.Code).FirstOrDefault();
                    if (checkCode != null)
                    {
                        Log.Error(
                            //"ColumnName:"+ request.ColumnName + 
                            "  Name :" + request.Code + "' is exiest !");
                        return new OldResponse<MetadataFieldModel>(0,
                            //"ColumnName:"+ request.ColumnName + 
                            "  Name :" + request.Code + "' is exiest !", null);
                    }


                    var data = new MetadataField();
                    data.MetadataFieldId = Guid.NewGuid();
                    data.CreatedOnDate = DateTime.Now;
                    data.LastModifiedOnDate = DateTime.Now;

                    data.CreatedByUserId = request.UserId.HasValue ? request.UserId.Value : _userId.Value;
                    data.LastModifiedByUserId = request.UserId.HasValue ? request.UserId.Value : _userId.Value;
                    data.Name = request.Name;
                    data.Code = request.Code; 
                    data.Description = request.Description;
                    data.FormlyContent = request.FormlyContent;
                    data.ApplicationId = request.ApplicationId;

                    //exe query
                    repo.Add(data);
                    var result = unitOfWork.Save();
                    if (result >= 1)
                    {
                        //Task.Run(() => MetadataFieldCollection.Instance.LoadToHashset());
                        // MetadataFieldCollection.Instance.LoadToHashset();
                        Log.Error("create successfull with request :" + JsonConvert.SerializeObject(request));
                        return new OldResponse<MetadataFieldModel>(1, "create successfull with request :" + JsonConvert.SerializeObject(request), ConvertData(data));
                    }
                    else
                    {
                        Log.Error("SQL can not process query ");
                        return new OldResponse<MetadataFieldModel>(0, "SQL can not process query", null);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error("Create error", ex);
                return new OldResponse<MetadataFieldModel>(-1, "Create error" + ex.Message, null);
            }
        }

        public OldResponse<MetadataFieldModel> Update(MetadataFieldUpdateRequestModel request)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    //check exest
                    var repo = unitOfWork.GetRepository<MetadataField>();
                    var current = repo.Find(request.MetadataFieldId);
                    if (current == null)
                    {
                        Log.Error("Time constraint Id not found +'" + request.MetadataFieldId + "'");
                        return new OldResponse<MetadataFieldModel>(0, "Time constraint Id not found +'" + request.MetadataFieldId + "'", null);
                    }
                    //check new unique name
                    if (
                        //request.ColumnName != current.ColumnName&& 
                        request.Name != current.Name)
                    {
                        var currentHasUniqueName = repo.Get(s =>
                        //s.ColumnName == request.ColumnName || 
                        s.Name == request.Name).FirstOrDefault();
                        if (currentHasUniqueName != null)
                        {
                            Log.Error(
                                //"ColumnName:" + request.ColumnName + 
                                " Name :" + request.Name + "' is exiest !");
                            return new OldResponse<MetadataFieldModel>(0,
                                //"ColumnName:" + request.ColumnName +
                                " Name :" + request.Name + "' is exiest !", null);
                        }
                    }
                    //check new unique name
                    if (
                        //request.ColumnName != current.ColumnName&& 
                        request.Code != current.Code)
                    {
                        var currentHasUniqueCode = repo.Get(s =>
                        //s.ColumnName == request.ColumnName || 
                        s.Code == request.Code).FirstOrDefault();
                        if (currentHasUniqueCode != null)
                        {
                            Log.Error(
                                //"ColumnName:" + request.ColumnName + 
                                " Code :" + request.Code + "' is exiest !");
                            return new OldResponse<MetadataFieldModel>(0,
                                //"ColumnName:" + request.ColumnName +
                                " Code :" + request.Code + "' is exiest !", null);
                        }
                    }

                    //start update
                    current.LastModifiedOnDate = DateTime.Now;
                    current.LastModifiedByUserId = request.UserId.HasValue ? request.UserId.Value : _userId.Value;
                    current.Name = request.Name;
                    current.Code = request.Code;
                    current.Description = request.Description;
                    current.FormlyContent = request.FormlyContent;
                    current.ApplicationId = request.ApplicationId;

                    repo.Update(current);
                    var result = unitOfWork.Save();
                    if (result >= 1)
                    {
                        //Task.Run(() => MetadataFieldCollection.Instance.LoadToHashset());
                        // MetadataFieldCollection.Instance.LoadToHashset();
                        Log.Error("Update successfull with request :" + JsonConvert.SerializeObject(request));
                        return new OldResponse<MetadataFieldModel>(1, "create successfull with request :" + JsonConvert.SerializeObject(request), ConvertData(current));
                    }
                    else
                    {
                        Log.Error("SQL can not process query ");
                        return new OldResponse<MetadataFieldModel>(0, "SQL can not process query", null);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error("Update error", ex);
                return new OldResponse<MetadataFieldModel>(-1, "Update error" + ex.Message, null);
            }
        }
        public OldResponse<MetadataFieldDeleteResponseModel> Delete(Guid MetadataFieldId)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    var result = new OldResponse<MetadataFieldDeleteResponseModel>(1, "", new MetadataFieldDeleteResponseModel());

                    result.Data.Model = new BaseMetadataFieldModel();

                    //check exitest
                    var repo = unitOfWork.GetRepository<MetadataField>();
                    var current = repo.Find(MetadataFieldId);
                    if (current == null)
                    {
                        Log.Error(" MetadataFieldId not found +'" + MetadataFieldId + "'");
                        result.Message = " MetadataFieldId not found +'" + MetadataFieldId + "'";
                        result.Data.Message = " MetadataFieldId not found +'" + MetadataFieldId + "'";
                        result.Data.Result = false;
                        result.Data.Model.MetadataFieldId = MetadataFieldId;
                        return result;
                    }
                    //check Instance
                    repo.Delete(current);
                    var processSQL = unitOfWork.Save();
                    if (processSQL >= 1)
                    {
                        //Task.Run(() => MetadataFieldCollection.Instance.LoadToHashset());
                        // MetadataFieldCollection.Instance.LoadToHashset();
                        Log.Error("delete sucess");
                        result.Message = "delete sucess";
                        result.Data.Message = "delete sucess";
                        result.Data.Result = true;
                        result.Data.Model.MetadataFieldId = MetadataFieldId;
                        result.Data.Model.Name = current.Name; 
                        result.Data.Model.Description = current.Description;
                        result.Data.Model.FormlyContent = current.FormlyContent; 
                        return result;
                    }
                    else
                    {
                        Log.Error("SQL can not process query");
                        result.Message = "SQL can not process query";
                        result.Data.Message = "SQL can not process query";
                        result.Data.Result = false;
                        result.Data.Model.MetadataFieldId = MetadataFieldId;
                        result.Data.Model.Name = current.Name; 
                        result.Data.Model.Description = current.Description;
                        result.Data.Model.FormlyContent = current.FormlyContent; 
                        return result;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error("Delete time constraint error", ex);
                var result = new OldResponse<MetadataFieldDeleteResponseModel>(1, "", new MetadataFieldDeleteResponseModel());

                result.Data.Model = new BaseMetadataFieldModel();
                Log.Error("Delete error");
                result.Message = "Delete error";
                result.Data.Message = "Delete error";
                result.Data.Result = false;
                result.Data.Model.MetadataFieldId = MetadataFieldId;
                return result;
            }
        }

        public OldResponse<IList<MetadataFieldDeleteResponseModel>> DeleteMany(IList<Guid> ListMetadataFieldId)
        {
            var result = new OldResponse<IList<MetadataFieldDeleteResponseModel>>(1, "", new List<MetadataFieldDeleteResponseModel>());
            foreach (var item in ListMetadataFieldId)
            {
                var deleteResult = Delete(item);
                result.Data.Add(deleteResult.Data);
            }
            return result;
        }
        #endregion
    }
}
