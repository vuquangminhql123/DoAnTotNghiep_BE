﻿using DigitalID.Data;
using System;
using System.Collections.Generic;

namespace DigitalID.Business
{
    public interface IMetadataFieldHandler
    {
        void init(Guid applicationId, Guid userId);
        #region CRUD
        OldResponse<IList<MetadataFieldModel>> GetFilter(MetadataFieldQueryFilter filter);
        OldResponse<MetadataFieldModel> GetById(Guid MetadataFieldId);
        OldResponse<MetadataFieldModel> Create(MetadataFieldCreateRequestModel request);

        OldResponse<MetadataFieldModel> Update(MetadataFieldUpdateRequestModel request);
        OldResponse<MetadataFieldDeleteResponseModel> Delete(Guid MetadataFieldId);

        OldResponse<IList<MetadataFieldDeleteResponseModel>> DeleteMany(IList<Guid> ListMetadataFieldId);
        #endregion
    }
}
