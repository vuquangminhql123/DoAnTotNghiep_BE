﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DigitalID.Business
{
    public class MetadataFieldCollection
    {
        private readonly IMetadataFieldHandler _metadataFieldHandler;
        private HashSet<MetadataFieldModel> collection;


        public MetadataFieldCollection(IMetadataFieldHandler metadataFieldHandler)
        {
            _metadataFieldHandler = metadataFieldHandler;
            LoadToHashset();
        }

        public void LoadToHashset()
        {
            collection = new HashSet<MetadataFieldModel>();

            // Query to list
            var listResponse = _metadataFieldHandler.GetFilter(new MetadataFieldQueryFilter()
            {
                PageSize = null,
                PageNumber = null
            });

            // Add to hashset
            foreach (var response in listResponse.Data)
            {
                collection.Add(response);
            }
        }

        public MetadataFieldModel GetById(Guid id)
        {
            var result = collection.FirstOrDefault(u => u.MetadataFieldId == id);
            return result;
        }
    }
}