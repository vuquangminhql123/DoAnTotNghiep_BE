﻿
using System;
using System.Collections.Generic;

namespace DigitalID.Business
{
    #region Main
    public class BaseMetadataFieldModel
    {
        public Guid MetadataFieldId { get; set; }
        public string Name { get; set; }
        public string Code { get; set; } 
        public string FormlyContent { get; set; }
        public string Description { get; set; }
        public Guid? ApplicationId { get; set; }

    }
    public class MetadataFieldModel : BaseMetadataFieldModel
    {
        public DateTime CreatedOnDate { get; set; }
        public DateTime LastModifiedOnDate { get; set; }
        public BaseUserModel CreatedByUser { get; set; }
        public BaseUserModel LastModifiedByUser { get; set; }
        public int Order { get; set; }

    }

    public class MetadataFieldCreateRequestModel
    {

        public string Name { get; set; }
        public string Code { get; set; }
        public string FormlyContent { get; set; }
        public string Description { get; set; }
        public Guid? UserId { get; set; }
        public Guid? ApplicationId { get; set; }
    }

    public class MetadataFieldUpdateRequestModel
    {

        public Guid MetadataFieldId { get; set; }
        public string FormlyContent { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public Guid? UserId { get; set; }
        public Guid? ApplicationId { get; set; }
    }

    public class MetadataFieldQueryFilter
    {
        public int? PageSize { get; set; }
        public int? PageNumber { get; set; }
        public string TextSearch { get; set; }
        //
        public Guid? MetadataFieldId { get; set; }

        public MetadataFieldQueryOrder Order { get; set; }
        public MetadataFieldQueryFilter()
        {
            PageSize = 0;
            PageNumber = 1;
            TextSearch = string.Empty;
            Order = MetadataFieldQueryOrder.CREATE_DATE_ASC;

        }
    }

    public class MetadataFieldDeleteResponseModel
    {
        public BaseMetadataFieldModel Model { get; set; }
        public bool Result { get; set; }
        public string Message { get; set; }
    }
    public class MetadataFieldDeleteRequestModel
    {
        public IList<Guid> ListId { get; set; }
    }
    public enum MetadataFieldQueryOrder
    {
        CREATE_DATE_ASC,
        MODIFIED_DATE_ASC,
        CREATE_DATE_DESC,
        MODIFIED_DATE_DESC,

    }
    #endregion
}
