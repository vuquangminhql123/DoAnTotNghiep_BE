﻿using DigitalID.Data;
using Newtonsoft.Json;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DigitalID.Business
{
    public class DbMetadataTemplateHandler : IMetadataTemplateHandler
    {
        private Guid? _applicationId { get; set; }
        private Guid? _userId { get; set; }

        public void init(Guid applicationId, Guid userId)
        {
            _applicationId = applicationId;
            _userId = userId;
        }

        private MetadataTemplateModel ConvertData(MetadataTemplate model)
        {
            MetadataTemplateModel result = new MetadataTemplateModel()
            {
                CreatedByUser = new BaseUserModel() { Id = model.CreatedByUserId },
                LastModifiedByUser = new BaseUserModel() { Id = model.LastModifiedByUserId },
                CreatedOnDate = model.CreatedOnDate,
                LastModifiedOnDate = model.LastModifiedOnDate,
                Name = model.Name,
                Code = model.Code,
                Description = model.Description,
                MetadataTemplateId = model.MetadataTemplateId,
                FormConfig = model.FormConfig,
                ApplicationId = model.ApplicationId,
            };
            return result;
        }

        #region CRUD
        public OldResponse<IList<MetadataTemplateModel>> GetFilter(MetadataTemplateQueryFilter filter)
        {
            try
            {
                // Create result variable
                var result = new OldResponse<IList<MetadataTemplateModel>>(0, string.Empty, new List<MetadataTemplateModel>(), 0, 0);

                using (var unitOfWork = new UnitOfWork())
                {
                    var datas = unitOfWork.GetRepository<MetadataTemplate>().GetAll();
                    //filter
                    #region filter
                    //by MetadataTemplateId
                    if (filter.MetadataTemplateId.HasValue)
                    {
                        datas = from dt in datas
                                where dt.MetadataTemplateId == filter.MetadataTemplateId.Value
                                select dt;
                    }
                    //by IsDocumentMetadataTemplate
                    if (filter.MetadataTemplateId.HasValue)
                    {
                        datas = from dt in datas
                                where dt.MetadataTemplateId == filter.MetadataTemplateId.Value
                                select dt;
                    }
                    //by  TextSearch
                    if (!string.IsNullOrEmpty(filter.TextSearch))
                    {
                        filter.TextSearch = filter.TextSearch.ToLower().Trim();
                        datas = from dt in datas
                                where dt.Name.ToLower().Contains(filter.TextSearch)
                                select dt;
                    }
                    #endregion
                    var totalCount = datas.Count();
                    //Order
                    #region ordering
                    switch (filter.Order)
                    {
                        case MetadataTemplateQueryOrder.CREATE_DATE_ASC:
                            datas = datas.OrderBy(sp => sp.CreatedOnDate);
                            break;
                        case MetadataTemplateQueryOrder.CREATE_DATE_DESC:
                            datas = datas.OrderByDescending(sp => sp.CreatedOnDate);
                            break;
                        case MetadataTemplateQueryOrder.MODIFIED_DATE_ASC:
                            datas = datas.OrderBy(sp => sp.LastModifiedOnDate);
                            break;
                        case MetadataTemplateQueryOrder.MODIFIED_DATE_DESC:
                            datas = datas.OrderByDescending(sp => sp.LastModifiedOnDate);
                            break;
                        default:
                            break;
                    }
                    #endregion
                    // Pagination
                    #region Pagination
                    if (filter.PageSize.HasValue && filter.PageNumber.HasValue)
                    {
                        if (filter.PageSize.Value <= 0) filter.PageSize = 20;

                        filter.PageNumber = filter.PageNumber - 1;
                        //Calculate nunber of rows to skip on pagesize
                        int excludedRows = (filter.PageNumber.Value) * (filter.PageSize.Value);
                        if (excludedRows <= 0) excludedRows = 0;

                        datas = datas.Skip(excludedRows).Take(filter.PageSize.Value);
                    }
                    #endregion

                    var fullQueryData = datas.ToList();
                    // Convert Data
                    if (fullQueryData != null && fullQueryData.Count() >= 0)
                    {
                        result.Message = "Success get filter with filter : " + JsonConvert.SerializeObject(filter);
                        result.Status = 1;
                        result.DataCount = fullQueryData.Count();
                        result.TotalCount = totalCount;
                        foreach (var item in fullQueryData)
                        {
                            result.Data.Add(ConvertData(item));
                        }
                    }
                    return result;
                }
            }
            catch (Exception ex)
            {
                Log.Error("Error get filter with filter: " + JsonConvert.SerializeObject(filter), ex);
                return new OldResponse<IList<MetadataTemplateModel>>(-1, "Error get filter with filter: " + JsonConvert.SerializeObject(filter), null, 0, 0);
            }
        }
        public OldResponse<MetadataTemplateModel> GetById(Guid MetadataTemplateId)
        {
            var filter = new MetadataTemplateQueryFilter() { MetadataTemplateId = MetadataTemplateId };
            var response = GetFilter(filter);
            if (response.Status == 1)
            {
                return new OldResponse<MetadataTemplateModel>(1, "Success", response.Data.FirstOrDefault());
            }
            else
            {
                return new OldResponse<MetadataTemplateModel>(response.Status, response.Message, null);
            }
        }
        public OldResponse<MetadataTemplateModel> Create(MetadataTemplateCreateRequestModel request)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    var repo = unitOfWork.GetRepository<MetadataTemplate>();
                    //check unique Name 
                    var check = repo.GetMany(s => s.Name == request.Name).FirstOrDefault();
                    if (check != null)
                    {
                        Log.Error("Name:" + request.Name + "' is exiest !", null);
                        return new OldResponse<MetadataTemplateModel>(0, "Name:" + request.Name + "' is exiest !", null);
                    }

                    var data = new MetadataTemplate();
                    data.MetadataTemplateId = Guid.NewGuid();
                    data.CreatedOnDate = DateTime.Now;
                    data.LastModifiedOnDate = DateTime.Now;

                    data.CreatedByUserId = request.UserId.HasValue ? request.UserId.Value : _userId.Value;
                    data.LastModifiedByUserId = request.UserId.HasValue ? request.UserId.Value : _userId.Value;

                    data.Name = request.Name;
                    data.Code = request.Code;
                    data.Description = request.Description;
                    data.FormConfig = request.FormConfig;
                    data.ApplicationId = request.ApplicationId;

                    //exe query
                    repo.Add(data);
                    var result = unitOfWork.Save();
                    if (result >= 1)
                    {
                        var repoInstant = unitOfWork.GetRepository<MetadataFieldTemplateRelationship>();
                        //Start update relationship
                        if (request.ListMetadataField != null && request.ListMetadataField.Count() > 0)
                        {
                            for (int i = 0; i < request.ListMetadataField.Count; i++)
                            {
                                var field = request.ListMetadataField[i];
                                var instantIem = new MetadataFieldTemplateRelationship()
                                {
                                    MetadataTemplateId = data.MetadataTemplateId,
                                    Order = field.Order,
                                    MetadataFieldId = field.MetadataFieldId,
                                    Status = field.Status,
                                };
                                repoInstant.Add(instantIem);
                            }
                            if (unitOfWork.Save() >= 1)
                            {
                                Log.Error("create successfull + instant successfull with request :" + JsonConvert.SerializeObject(request),null);
                                return new OldResponse<MetadataTemplateModel>(1, "create successfull + instant successfull with request :" + JsonConvert.SerializeObject(request), ConvertData(data));
                            }
                            else
                            {
                                Log.Error("create successfull but instant false with request :" + JsonConvert.SerializeObject(request),null);
                                return new OldResponse<MetadataTemplateModel>(1, "create successfull but instant false  with request :" + JsonConvert.SerializeObject(request), ConvertData(data));
                            }
                        }
                        else
                        {
                            Log.Error("create successfull with request :" + JsonConvert.SerializeObject(request),null);
                            return new OldResponse<MetadataTemplateModel>(1, "create successfull with request :" + JsonConvert.SerializeObject(request), ConvertData(data));
                        }
                    }
                    else
                    {
                        Log.Error("SQL can not process query ",null);
                        return new OldResponse<MetadataTemplateModel>(0, "SQL can not process query", null);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error("Create error", ex);
                return new OldResponse<MetadataTemplateModel>(-1, "Create error" + ex.Message, null);
            }
        }

        //tái sử dụng, ko save -> gọi xong -> save(), truyền vào transation
        public OldResponse<MetadataTemplateModel> Create(MetadataTemplateCreateRequestModel request, UnitOfWork unitOfWork)
        {
            try
            {
                var repo = unitOfWork.GetRepository<MetadataTemplate>();
                //check unique Name 
                var check = repo.GetMany(s => s.Name == request.Name).FirstOrDefault();
                if (check != null)
                {
                    Log.Error("Name:" + request.Name + "' is exiest !", null);
                    return new OldResponse<MetadataTemplateModel>(0, "Name:" + request.Name + "' is exiest !", null);
                }

                var data = new MetadataTemplate();
                data.MetadataTemplateId = Guid.NewGuid();
                data.CreatedOnDate = DateTime.Now;
                data.LastModifiedOnDate = DateTime.Now;

                data.CreatedByUserId = request.UserId.HasValue ? request.UserId.Value : _userId.Value;
                data.LastModifiedByUserId = request.UserId.HasValue ? request.UserId.Value : _userId.Value;

                data.Name = request.Name;
                data.Code = request.Code;
                data.Description = request.Description;
                data.FormConfig = request.FormConfig;
                data.ApplicationId = request.ApplicationId;

                //exe query
                repo.Add(data);
                var repoInstant = unitOfWork.GetRepository<MetadataFieldTemplateRelationship>();
                //Start update relationship
                if (request.ListMetadataField != null && request.ListMetadataField.Count() > 0)
                {
                    for (int i = 0; i < request.ListMetadataField.Count; i++)
                    {
                        var field = request.ListMetadataField[i];
                        var instantIem = new MetadataFieldTemplateRelationship()
                        {
                            MetadataTemplateId = data.MetadataTemplateId,
                            Order = field.Order,
                            MetadataFieldId = field.MetadataFieldId,
                            Status = field.Status,
                        };
                        repoInstant.Add(instantIem);
                    }
                    Log.Error("create successfull + instant successfull with request :" + JsonConvert.SerializeObject(request), null);
                    return new OldResponse<MetadataTemplateModel>(1, "create successfull + instant successfull with request :" + JsonConvert.SerializeObject(request), ConvertData(data));
                }
                else
                {
                    Log.Error("create successfull with request :" + JsonConvert.SerializeObject(request), null);
                    return new OldResponse<MetadataTemplateModel>(1, "create successfull with request :" + JsonConvert.SerializeObject(request), ConvertData(data));
                }
            }
            catch (Exception ex)
            {
                Log.Error("Create error", ex);
                return new OldResponse<MetadataTemplateModel>(-1, "Create error" + ex.Message, null);
            }
        }
        public OldResponse<MetadataTemplateModel> Update(MetadataTemplateUpdateRequestModel request)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    //check exest
                    var repo = unitOfWork.GetRepository<MetadataTemplate>();
                    var current = repo.Find(request.MetadataTemplateId);
                    if (current == null)
                    {
                        Log.Error("Time constraint Id not found +'" + request.MetadataTemplateId + "'", null);
                        return new OldResponse<MetadataTemplateModel>(0, "Time constraint Id not found +'" + request.MetadataTemplateId + "'", null);
                    }
                    //check new unique name
                    if (request.Name != current.Name)
                    {
                        var currentHasUniqueName = repo.GetMany(s => s.Name == request.Name).FirstOrDefault();
                        if (currentHasUniqueName != null)
                        {
                            Log.Error("ColumnName:" + request.Name + "' is exiest !", null);
                            return new OldResponse<MetadataTemplateModel>(0, "ColumnName:" + request.Name + "' is exiest !", null);
                        }
                    }

                    //start update 
                    current.CreatedOnDate = DateTime.Now;
                    current.LastModifiedOnDate = DateTime.Now;
                    current.CreatedByUserId = request.UserId.HasValue ? request.UserId.Value : _userId.Value;
                    current.LastModifiedByUserId = request.UserId.HasValue ? request.UserId.Value : _userId.Value;
                    current.Name = request.Name;
                    current.Code = request.Code;
                    current.Description = request.Description;
                    current.FormConfig = request.FormConfig;
                    current.ApplicationId = request.ApplicationId;
                    //exe query
                    repo.Update(current);
                    var result = unitOfWork.Save();
                    if (result >= 1)
                    {
                        var repoInstant = unitOfWork.GetRepository<MetadataFieldTemplateRelationship>();
                        var listBeingDelete = repoInstant.GetMany(sp => sp.MetadataTemplateId == current.MetadataTemplateId).ToList();
                        foreach (var item in listBeingDelete)
                        {
                            repoInstant.Delete(item);
                        }
                        //Start update relationship
                        if (request.ListMetadataField != null && request.ListMetadataField.Count() > 0)
                        {
                            for (int i = 0; i < request.ListMetadataField.Count; i++)
                            {
                                var field = request.ListMetadataField[i];
                                var instantIem = new MetadataFieldTemplateRelationship()
                                {
                                    MetadataTemplateId = current.MetadataTemplateId,
                                    Order = field.Order,
                                    MetadataFieldId = field.MetadataFieldId,
                                    Status = field.Status,
                                };
                                repoInstant.Add(instantIem);
                            }
                        }
                        if (unitOfWork.Save() >= 1)
                        {
                            Log.Error("Update successfull + instant successfull with request :" + JsonConvert.SerializeObject(request), null);
                            return new OldResponse<MetadataTemplateModel>(1, "Update successfull + instant successfull with request :" + JsonConvert.SerializeObject(request), ConvertData(current));
                        }
                        else
                        {
                            Log.Error("Update successfull but instant false with request :" + JsonConvert.SerializeObject(request), null);
                            return new OldResponse<MetadataTemplateModel>(1, "Update successfull but instant false  with request :" + JsonConvert.SerializeObject(request), ConvertData(current));
                        }

                    }
                    else
                    {
                        Log.Error("SQL can not process query ", null);
                        return new OldResponse<MetadataTemplateModel>(0, "SQL can not process query", null);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error("Update error", ex);
                return new OldResponse<MetadataTemplateModel>(-1, "Update error" + ex.Message, null);
            }
        }
        public OldResponse<MetadataTemplateModel> Update(MetadataTemplateUpdateRequestModel request, UnitOfWork unitOfWork)
        {
            try
            {
                //check exest
                var repo = unitOfWork.GetRepository<MetadataTemplate>();
                var current = repo.Find(request.MetadataTemplateId);
                if (current == null)
                {
                    Log.Error("Time constraint Id not found +'" + request.MetadataTemplateId + "'", null);
                    return new OldResponse<MetadataTemplateModel>(0, "Time constraint Id not found +'" + request.MetadataTemplateId + "'", null);
                }
                //check new unique name
                if (request.Name != current.Name)
                {
                    var currentHasUniqueName = repo.GetMany(s => s.Name == request.Name).FirstOrDefault();
                    if (currentHasUniqueName != null)
                    {
                        Log.Error("ColumnName:" + request.Name + "' is exiest !", null);
                        return new OldResponse<MetadataTemplateModel>(0, "ColumnName:" + request.Name + "' is exiest !", null);
                    }
                }

                //start update 
                current.CreatedOnDate = DateTime.Now;
                current.LastModifiedOnDate = DateTime.Now;
                current.CreatedByUserId = request.UserId.HasValue ? request.UserId.Value : _userId.Value;
                current.LastModifiedByUserId = request.UserId.HasValue ? request.UserId.Value : _userId.Value;
                current.Name = request.Name;
                current.Code = request.Code;
                current.Description = request.Description;
                current.FormConfig = request.FormConfig;
                current.ApplicationId = request.ApplicationId;
                //exe query
                repo.Update(current);
                var repoInstant = unitOfWork.GetRepository<MetadataFieldTemplateRelationship>();
                var listBeingDelete = repoInstant.GetMany(sp => sp.MetadataTemplateId == current.MetadataTemplateId).ToList();
                foreach (var item in listBeingDelete)
                {
                    repoInstant.Delete(item);
                }
                //Start update relationship
                if (request.ListMetadataField != null && request.ListMetadataField.Count() > 0)
                {
                    for (int i = 0; i < request.ListMetadataField.Count; i++)
                    {
                        var field = request.ListMetadataField[i];
                        var instantIem = new MetadataFieldTemplateRelationship()
                        {
                            MetadataTemplateId = current.MetadataTemplateId,
                            Order = field.Order,
                            MetadataFieldId = field.MetadataFieldId,
                            Status = field.Status,
                        };
                        repoInstant.Add(instantIem);
                    }
                }
                Log.Error("Update successfull + instant successfull with request :" + JsonConvert.SerializeObject(request));
                return new OldResponse<MetadataTemplateModel>(1, "Update successfull + instant successfull with request :" + JsonConvert.SerializeObject(request), ConvertData(current));
            }
            catch (Exception ex)
            {
                Log.Error("Update error", ex);
                return new OldResponse<MetadataTemplateModel>(-1, "Update error" + ex.Message, null);
            }
        }
        public OldResponse<MetadataTemplateDeleteResponseModel> Delete(Guid MetadataTemplateId)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    var result = new OldResponse<MetadataTemplateDeleteResponseModel>(1, "", new MetadataTemplateDeleteResponseModel());

                    result.Data.Model = new BaseMetadataTemplateModel();

                    //check exitest
                    var repo = unitOfWork.GetRepository<MetadataTemplate>();
                    var current = repo.Find(MetadataTemplateId);
                    if (current == null)
                    {
                        Log.Error(" MetadataTemplateId not found +'" + MetadataTemplateId + "'");
                        result.Message = " MetadataTemplateId not found +'" + MetadataTemplateId + "'";
                        result.Data.Message = " MetadataTemplateId not found +'" + MetadataTemplateId + "'";
                        result.Data.Result = false;
                        result.Data.Model.MetadataTemplateId = MetadataTemplateId;
                        return result;
                    }
                    //check depen
                    var repoInstant = unitOfWork.GetRepository<MetadataFieldTemplateRelationship>();
                    var listBeingDelete = repoInstant.GetMany(sp => sp.MetadataTemplateId == MetadataTemplateId).ToList();
                    foreach (var item in listBeingDelete)
                    {
                        repoInstant.Delete(item);
                    }
                    repo.Delete(current);
                    var processSQL = unitOfWork.Save();
                    if (processSQL >= 1)
                    {
                        Log.Error("delete sucess");
                        result.Message = "delete sucess";
                        result.Data.Message = "delete sucess";
                        result.Data.Result = true;
                        result.Data.Model.MetadataTemplateId = MetadataTemplateId;
                        result.Data.Model.Name = current.Name;

                        return result;
                    }
                    else
                    {
                        Log.Error("SQL can not process query");
                        result.Message = "SQL can not process query";
                        result.Data.Message = "SQL can not process query";
                        result.Data.Result = false;
                        result.Data.Model.MetadataTemplateId = MetadataTemplateId;
                        result.Data.Model.Name = current.Name;
                        return result;
                    }
                }
            }
            catch (Exception ex)
            {

                Log.Error("Delete time constraint error", ex);
                var result = new OldResponse<MetadataTemplateDeleteResponseModel>(1, "", new MetadataTemplateDeleteResponseModel());

                result.Data.Model = new BaseMetadataTemplateModel();
                Log.Error("Delete error");
                result.Message = "Delete error";
                result.Data.Message = "Delete error";
                result.Data.Result = false;
                result.Data.Model.MetadataTemplateId = MetadataTemplateId;
                return result;
            }

        }

        public OldResponse<IList<MetadataTemplateDeleteResponseModel>> DeleteMany(IList<Guid> listMetadataTemplateId)
        {
            var result = new OldResponse<IList<MetadataTemplateDeleteResponseModel>>(1, "", new List<MetadataTemplateDeleteResponseModel>());
            foreach (var item in listMetadataTemplateId)
            {
                var deleteResult = Delete(item);
                result.Data.Add(deleteResult.Data);
            }
            return result;
        }
        #endregion
        #region Other

        public OldResponse<MetadataTemplateModel> CloneMetadataTemplate(Guid archiveTypeId)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    var repo = unitOfWork.GetRepository<MetadataTemplate>();
                    var current = repo.Find(archiveTypeId);
                    if (current != null)
                    {
                        //Step 1 clone Main
                        var data = new MetadataTemplate();
                        data.MetadataTemplateId = Guid.NewGuid();
                        data.CreatedOnDate = DateTime.Now;
                        data.LastModifiedOnDate = DateTime.Now;

                        data.CreatedByUserId = current.CreatedByUserId;
                        data.LastModifiedByUserId = current.LastModifiedByUserId;

                        data.Name = current.Name + "_copy";
                        data.Code = current.Code + "_copy";
                        data.Description = current.Description;
                        data.ApplicationId = current.ApplicationId;
                        data.FormConfig = current.FormConfig;
                        //exe query
                        repo.Add(data);
                        var result = unitOfWork.Save();
                        if (result >= 1)
                        {
                            //Step 2 add reference
                            var repoInstant = unitOfWork.GetRepository<MetadataFieldTemplateRelationship>();
                            var listField = repoInstant.GetMany(s => s.MetadataTemplateId == archiveTypeId);
                            foreach (var item in listField)
                            {
                                var instantIem = new MetadataFieldTemplateRelationship()
                                {
                                    MetadataTemplateId = data.MetadataTemplateId,
                                    Order = item.Order,
                                    MetadataFieldId = item.MetadataFieldId,
                                    Status = item.Status,
                                };
                                repoInstant.Add(instantIem);
                            }
                            unitOfWork.Save();
                            Log.Error("Clone success archiveTypeId :" + archiveTypeId);
                            return new OldResponse<MetadataTemplateModel>(1, "Clone success archiveTypeId :" + archiveTypeId, ConvertData(data));
                        }
                        else
                        {
                            Log.Error("SQL can not process query ");
                            return new OldResponse<MetadataTemplateModel>(0, "SQL can not process query", null);
                        }
                    }
                    else
                    {
                        Log.Error("MetadataTemplateId :" + archiveTypeId + " not found");
                        return new OldResponse<MetadataTemplateModel>(0, "MetadataTemplateId :" + archiveTypeId + " not found", null);

                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error("Error ", ex);
                return new OldResponse<MetadataTemplateModel>(-1, "Error  ", null, 0, 0);
            }
        }
        public OldResponse<IList<MetadataFieldModel>> GetFieldByMetadataTemplateId(Guid MetadataTemplateId)
        {
            try
            {
                // Create result variable
                var result = new OldResponse<IList<MetadataFieldModel>>(0, string.Empty, new List<MetadataFieldModel>(), 0, 0);

                using (var unitOfWork = new UnitOfWork())
                {
                    var datas = from f in unitOfWork.GetRepository<MetadataField>().GetAll()
                                join rf in unitOfWork.GetRepository<MetadataFieldTemplateRelationship>().GetAll()
                                on f.MetadataFieldId equals rf.MetadataFieldId
                                select new { Field = f, MetadataTemplateField = rf };
                    if (MetadataTemplateId != null & MetadataTemplateId != Guid.Empty)
                    {
                        datas = datas.Where(s => s.MetadataTemplateField.MetadataTemplateId == MetadataTemplateId);
                    }
                    //filter
                    #region filter

                    #endregion
                    var totalCount = datas.Count();
                    //Order
                    #region ordering
                    datas = datas.OrderBy(sp => sp.MetadataTemplateField.Order);
                    #endregion
                    // Pagination
                    #region Pagination

                    #endregion

                    var fullQueryData = datas.ToList();
                    // Convert Data
                    if (fullQueryData != null && fullQueryData.Count() >= 0)
                    {
                        result.Message = "Success  ";
                        result.Status = 1;
                        result.DataCount = fullQueryData.Count();
                        result.TotalCount = totalCount;
                        foreach (var item in fullQueryData)
                        {
                            result.Data.Add(DbMetadataFieldHandler.ConvertData(item.Field,item.MetadataTemplateField.Order));
                        }
                    }
                    return result;
                }
            }
            catch (Exception ex)
            {
                Log.Error("Error ", ex);
                return new OldResponse<IList<MetadataFieldModel>>(-1, "Error  ", null, 0, 0);
            }
        }

        #endregion
    }
}