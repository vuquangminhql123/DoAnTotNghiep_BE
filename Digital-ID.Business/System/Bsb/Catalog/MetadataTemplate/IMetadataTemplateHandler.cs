﻿using System;
using System.Collections.Generic;
using DigitalID.Data;

namespace DigitalID.Business
{
    public interface IMetadataTemplateHandler
    {
        void init(Guid applicationId, Guid userId);

        #region CRUD
         OldResponse<IList<MetadataTemplateModel>> GetFilter(MetadataTemplateQueryFilter filter);
         OldResponse<MetadataTemplateModel> GetById(Guid MetadataTemplateId);
         OldResponse<MetadataTemplateModel> Create(MetadataTemplateCreateRequestModel request);

         OldResponse<MetadataTemplateModel> Update(MetadataTemplateUpdateRequestModel request);
         OldResponse<MetadataTemplateDeleteResponseModel> Delete(Guid MetadataTemplateId);

         OldResponse<IList<MetadataTemplateDeleteResponseModel>> DeleteMany(IList<Guid> listMetadataTemplateId);
        #endregion
        #region Other

         OldResponse<MetadataTemplateModel> CloneMetadataTemplate(Guid archiveTypeId); 
         OldResponse<IList<MetadataFieldModel>> GetFieldByMetadataTemplateId(Guid MetadataTemplateId); 

        #endregion
    }
}
