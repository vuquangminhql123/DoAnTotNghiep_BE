﻿
using System;
using System.Collections.Generic;

namespace DigitalID.Business
{
    #region Main
    public class BaseMetadataTemplateModel { 
        public Guid MetadataTemplateId { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }  
        public string FormConfig { get; set; }
        public  Guid? ApplicationId { get; set; }
    } 
    public class MetadataTemplateModel : BaseMetadataTemplateModel
    { 
        public DateTime CreatedOnDate { get; set; }
        public DateTime LastModifiedOnDate { get; set; }
        public BaseUserModel CreatedByUser { get; set; }
        public BaseUserModel LastModifiedByUser { get; set; } 


    }

    public class MetadataTemplateCreateRequestModel
    { 
        public string Name { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public string FormConfig { get; set; }
        public Guid? ApplicationId { get; set; }
        public IList<MetadataTemplateFieldItem> ListMetadataField { get; set; }
        //
        public int Order { get; set; }
        public Guid? UserId { get; set; }
    }

    public class MetadataTemplateUpdateRequestModel
    {
        public Guid MetadataTemplateId { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public string FormConfig { get; set; }
        public Guid? ApplicationId { get; set; }
        public IList<MetadataTemplateFieldItem> ListMetadataField { get; set; }
        public Guid? UserId { get; set; }
    }

    public class MetadataTemplateQueryFilter {
        public int? PageSize { get; set; }
        public int? PageNumber { get; set; }
        public string TextSearch { get; set; }
        //
        public Guid? MetadataTemplateId { get; set; }   
        public MetadataTemplateQueryOrder Order { get; set; }
        public MetadataTemplateQueryFilter()
        {
            PageSize = 0;
            PageNumber = 1;
            TextSearch = string.Empty;
            Order = MetadataTemplateQueryOrder.CREATE_DATE_ASC;

        }
    }

    public class MetadataTemplateDeleteResponseModel
    {
        public BaseMetadataTemplateModel Model { get; set; }
        public bool Result { get; set; }
        public string Message { get; set; }
    }
    public class MetadataTemplateDeleteRequestModel
    {
        public IList<Guid> ListId { get; set; }
    }
    public enum MetadataTemplateQueryOrder
    {
        CREATE_DATE_ASC,
        MODIFIED_DATE_ASC, 
        CREATE_DATE_DESC,
        MODIFIED_DATE_DESC, 
       
    }
    public class MetadataTemplateFieldItem
    {
        public Guid MetadataFieldId { get; set; }
        public Guid MetadataTemplateId { get; set; }
        public int Order { get; set; }
        public int Status { get; set; } 
    }
    #endregion
}
