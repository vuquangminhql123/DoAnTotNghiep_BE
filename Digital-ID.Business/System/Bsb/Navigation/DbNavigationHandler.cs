﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DigitalID.Data;
using Microsoft.EntityFrameworkCore;
using Serilog;

namespace DigitalID.Business
{
    public class NavigationHandler : INavigationHanlder
    {
        public async Task<Response> GetByUserIdAsync(Guid userId, Guid applicationId)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    var query = (from user in unitOfWork.GetRepository<IdmUser>().GetAll()
                                 join userrole in unitOfWork.GetRepository<IdmUserMapRole>().GetAll()
                                     on user.Id equals userrole.UserId
                                 join navrole in unitOfWork.GetRepository<BsdNavigationMapRole>().GetAll()
                                     on userrole.RoleId equals navrole.RoleId
                                 join nav in unitOfWork.GetRepository<BsdNavigation>().GetAll()
                                     on navrole.NavigationId equals nav.Id
                                 where nav.Status == true &&
                                       user.Id == userId &&
                                       userrole.ApplicationId == applicationId
                                 select nav).Distinct().OrderBy(r => r.Order);

                    var sRights = await query.ToListAsync();
                    var rRights = new List<NavigationModel>();

                    foreach (var sRight in sRights)
                        if (sRight.ParentId == null)
                            rRights.Add(GetNav(sRights, sRight));
                    return new ResponseList<NavigationModel>(rRights);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return new ResponseError(Code.ServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }

        public async Task<Response> GetTreeAsync(Guid applicationId, bool isGetRoles)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    var rRights = new List<NavigationModel>();
                    var sRights = await unitOfWork.GetRepository<BsdNavigation>()
                        .Get(s => s.ApplicationId == applicationId).OrderBy(s => s.Order).ToListAsync();
                    if (sRights != null && sRights.Count > 0)
                    {
                        foreach (var sRight in sRights)
                            if (sRight.ParentId == null)
                                rRights.Add(GetNav(sRights, sRight, isGetRoles));
                        return new ResponseList<NavigationModel>(rRights);
                    }

                    return new ResponseList<NavigationModel>(null);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return new ResponseError(Code.ServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }

        public async Task<Response> CreateAsync(NavigationCreateModel request, Guid applicationId, Guid userId)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    var id = Guid.NewGuid();
                    var model = new BsdNavigation
                    {
                        Id = id,
                        Name = request.Name,
                        Code = request.Code,
                        Status = request.Status,
                        UrlRewrite = request.UrlRewrite,
                        IconClass = request.IconClass,
                        Order = request.Order,
                        SubUrl = request.SubUrl,
                        IdPath = request.ParentModel != null
                            ? (request.ParentModel.IdPath == null ? id + "/" : request.ParentModel.IdPath + id + "/")
                            : id + "/",
                        Path = request.ParentModel != null
                            ? (request.ParentModel.Path == null
                                ? request.Name + "/"
                                : request.ParentModel.Path + request.Name + "/")
                            : request.Name + "/",
                        Level = request.ParentModel?.Level + 1 ?? 0
                    }.InitCreate(applicationId, userId);

                    if (request.ParentModel != null) model.ParentId = request.ParentModel.Id;

                    if (!string.IsNullOrEmpty(model.UrlRewrite) && model.UrlRewrite != "#/")
                        if (CheckUrlRewrite(model.UrlRewrite, null))
                            return new ResponseError(Code.BadRequest,
                                $"Đường dẫn: {model.UrlRewrite} đã tồn tại");

                    if (!string.IsNullOrEmpty(model.Code))
                        if (CheckCode(model.Code, null))
                            return new ResponseError(Code.BadRequest, $"Mã: {model.Code} đã tồn tại"
                            );

                    unitOfWork.GetRepository<BsdNavigation>().Add(model);


                    var parentItem = await unitOfWork.GetRepository<BsdNavigation>()
                        .FindAsync(s => s.Id == model.ParentId);

                    if (parentItem != null)
                    {
                        parentItem.HasChild = true;
                        unitOfWork.GetRepository<BsdNavigation>().Update(parentItem);
                    }

                    if (await unitOfWork.SaveAsync() > 0)
                    {
                        if (request.RoleList != null && request.RoleList.Any())
                        {
                            await UpdateRoleInNavigations(model.Id, request.RoleList.ToList());
                        }
                        return new ResponseObject<NavigationModel>(new NavigationModel
                        {
                            Id = model.Id,
                            Name = model.Name,
                            ParentId = model.ParentId,
                            Code = model.Code,
                            UrlRewrite = model.UrlRewrite,
                            Order = model.Order,
                            Status = model.Status,
                            SubUrl = model.SubUrl
                        });

                    }

                    Log.Error("The sql statement is not executed!");
                    return new ResponseError(Code.BadRequest, "Câu lệnh sql không thể thực thi");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return new ResponseError(Code.ServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }

        public async Task<Response> UpdateAsync(Guid navigationId, NavigationUpdateModel request, Guid applicationId,
            Guid userId)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    var currentNav = await unitOfWork.GetRepository<BsdNavigation>().FindAsync(navigationId);
                    if (currentNav == null)
                    {
                        Log.Error($"{navigationId} is not found");
                        return new ResponseError(Code.BadRequest, "Id không tồn tại");
                    }

                    var navsRepo = unitOfWork.GetRepository<BsdNavigation>();
                    var childList = GetListChildNav(navigationId);
                    //kiem tra nav cha khong dk la chinh no hoac con no
                    childList.Add(navigationId);
                    foreach (var id in childList)
                        if (request.ParentModel != null)
                            if (id == request.ParentModel.Id)
                                return new ResponseError(Code.BadRequest,
                                    "Không được chọn điều hướng cha là điều hướng con của điều hướng hiện tại");
                    if (!string.IsNullOrEmpty(request.UrlRewrite) && request.UrlRewrite != "#/")
                        if (CheckUrlRewrite(request.UrlRewrite, navigationId))
                            return new ResponseError(Code.BadRequest,
                                $"Đường dẫn: {request.UrlRewrite} đã tồn tại");
                    if (!string.IsNullOrEmpty(request.Code))
                        if (CheckCode(request.Code, navigationId))
                            return new ResponseError(Code.BadRequest, $"Mã: {request.Code} đã tồn tại");
                    currentNav.Name = request.Name;
                    currentNav.UrlRewrite = request.UrlRewrite;
                    currentNav.IconClass = request.IconClass;
                    currentNav.Status = request.Status;
                    currentNav.Order = request.Order;
                    currentNav.Code = request.Code;
                    currentNav.SubUrl = request.SubUrl;
                    if (request.ParentModel == null)
                    {
                        currentNav.ParentId = null;
                        currentNav.IdPath = currentNav.Id + "/";
                        currentNav.Path = request.Name + "/";
                        currentNav.Level = 0;
                    }
                    else
                    {
                        var parentNavId = request.ParentModel.Id;
                        if (parentNavId != currentNav.ParentId)
                        {
                            var parentItem = await unitOfWork.GetRepository<BsdNavigation>()
                                .FindAsync(s => s.Id == parentNavId);
                            if (parentItem != null)
                            {
                                parentItem.HasChild = true;
                                unitOfWork.GetRepository<BsdNavigation>().Update(parentItem);
                                currentNav.ParentId = request.ParentModel.Id;
                                currentNav.IdPath = request.ParentModel.IdPath + currentNav.Id + "/";
                                currentNav.Path = request.ParentModel.Path + request.Name + "/";
                                currentNav.Level = request.ParentModel.Level + 1;
                            }
                        }
                    }

                    navsRepo.Update(currentNav.InitUpdate(applicationId, userId));
                    UpdateNavChilds(currentNav, navsRepo);
                    //DELETE role old parent
                    var navRoles = unitOfWork.GetRepository<BsdNavigationMapRole>();
                    //FromSubNavigation: từ các quyền lấy từ nav con => tìm các con
                    var updateNavRoles = (from d in unitOfWork.GetRepository<BsdNavigationMapRole>().GetAll()
                                          where d.NavigationId == navigationId && d.FromSubNavigation != null
                                          select d.FromSubNavigation).Distinct().ToList();
                    //lấy các quyền của nó cho cha nó để xóa đi
                    var deleteNavRoles = (from d in unitOfWork.GetRepository<BsdNavigationMapRole>().GetAll()
                                          where d.FromSubNavigation == navigationId
                                          select d).ToList();
                    if (updateNavRoles.Any())
                    {
                        //var uNavRoles2 = from d in unitOfWork.GetRepository<NavigationRole>().GetAll()
                        //                 where uNavRoles.Contains(d.FromSubNavigation)
                        //                 select d;

                        var listParent = GetListParentNav(navigationId);
                        if (listParent.Count > 0)
                            foreach (var temp in updateNavRoles)
                                foreach (var parentId in listParent)
                                {
                                    var uNavRoles2 = (from d in unitOfWork.GetRepository<BsdNavigationMapRole>().GetAll()
                                                      where d.FromSubNavigation == temp && d.NavigationId == parentId
                                                      select d).ToList();
                                    deleteNavRoles = deleteNavRoles.Concat(uNavRoles2).ToList();
                                }

                        //if (uNavRoles2.Count() > 0)
                        //    dNavRoles.Concat(uNavRoles2);
                    }

                    foreach (var item in deleteNavRoles) navRoles.Delete(item);
                    //unitOfWork.GetRepository<Navigation>().Update(uNav);
                    if (unitOfWork.Save() >= 1)
                    {
                        MoveRole(currentNav.Id);
                        if (request.RoleList != null && request.RoleList.Any())
                        {
                            await UpdateRoleInNavigations(navigationId, request.RoleList.ToList());
                        }
                        return new ResponseUpdate(currentNav.Id);
                    }

                    Log.Error("The sql statement is not executed!");
                    return new ResponseError(Code.BadRequest, "Câu lệnh sql không thể thực thi");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return new ResponseError(Code.ServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }

        public async Task<Response> DeleteAsync(Guid navigationId)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    var datas = unitOfWork.GetRepository<BsdNavigation>();
                    var dNav = await unitOfWork.GetRepository<BsdNavigation>().FindAsync(navigationId);
                    if (dNav != null)
                    {
                        //delete Role_Navigation
                        var navRoles = unitOfWork.GetRepository<BsdNavigationMapRole>();

                        navRoles.DeleteRange(s => s.NavigationId == dNav.Id || s.FromSubNavigation == dNav.Id);
                        datas.Delete(dNav);
                    }

                    if (await unitOfWork.SaveAsync() >= 1)
                        if (dNav != null)
                            return new ResponseDelete(dNav.Id, dNav.Name);

                    Log.Error("The sql statement is not executed!");
                    return new ResponseError(Code.BadRequest, "Câu lệnh sql không thể thực thi");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return new ResponseError(Code.ServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }

        public async Task<Response> UpdateRoleInNavigations(Guid navigationId, IList<Guid> listRoleId)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    // Lay ve nhom nguoi dung
                    var sNav = await unitOfWork.GetRepository<BsdNavigation>().FindAsync(sp => sp.Id == navigationId);
                    if (sNav == null)
                    {
                        Log.Error($"{navigationId} is not found");
                        return new ResponseError(Code.BadRequest, "Id không tồn tại");
                    }

                    var cNavRoleRepo = unitOfWork.GetRepository<BsdNavigationMapRole>();
                    //get all old roles of nav, tách những role bị trùng
                    var oldRoles = unitOfWork.GetRepository<BsdNavigationMapRole>().Get(o => o.NavigationId == navigationId)
                        .Select(s => s.RoleId).Distinct().ToList();
                    //new roles list
                    var newRoles = listRoleId;
                    //list all roles not changed
                    var temporaryRoles = new List<Guid>();
                    foreach (var oItem in oldRoles)
                        foreach (var nRoleId in newRoles)
                            if (oItem == nRoleId)
                            {
                                temporaryRoles.Add(oItem);
                                break;
                            }

                    if (temporaryRoles.Count == newRoles.Count && newRoles.Count == oldRoles.Count)
                        return new ResponseUpdate(navigationId);
                    //Remove roles unchanged
                    foreach (var temp in temporaryRoles)
                    {
                        oldRoles.Remove(temp);
                        newRoles.Remove(temp);
                    }

                    //delete old roles and add new roles
                    foreach (var oItem in oldRoles)
                    {
                        //vì ở trên select distinct nên phải get lại
                        var listNavRole = unitOfWork.GetRepository<BsdNavigationMapRole>()
                            .Get(g => g.NavigationId == navigationId && g.RoleId == oItem);
                        foreach (var item in listNavRole)
                            // xoa tat ca row an theo Fromsub = navId => các quyền cha kế thừa từ con
                            // xoa row co nav = fromsub => 
                            // xoa row fromsub = fromsub, xoa row
                            if (!string.IsNullOrEmpty(item.FromSubNavigation.ToString()))
                                unitOfWork.GetRepository<BsdNavigationMapRole>().Delete(rr =>
                                    rr.FromSubNavigation == item.NavigationId && rr.RoleId == item.RoleId ||
                                    rr.NavigationId == item.FromSubNavigation && rr.RoleId == item.RoleId ||
                                    rr.FromSubNavigation == item.FromSubNavigation && rr.RoleId == item.RoleId ||
                                    rr.Id == item.Id);
                            else
                                unitOfWork.GetRepository<BsdNavigationMapRole>().Delete(rr =>
                                    rr.FromSubNavigation == item.NavigationId && rr.RoleId == item.RoleId ||
                                    rr.Id == item.Id);
                    }

                    if (newRoles.Count > 0)
                    {
                        var listId = GetListParentNav(navigationId);
                        foreach (var roleId in newRoles)
                        {
                            foreach (var item in listId)
                            {
                                var navR = new BsdNavigationMapRole
                                {
                                    NavigationId = item,
                                    RoleId = roleId,
                                    FromSubNavigation = navigationId
                                };
                                cNavRoleRepo.Add(navR);
                            }

                            var navRole = new BsdNavigationMapRole
                            {
                                RoleId = roleId,
                                NavigationId = navigationId,


                            };
                            cNavRoleRepo.Add(navRole);
                        }
                    }

                    // Commit tat ca lenh
                    if (unitOfWork.Save() >= 1) return new ResponseUpdate(navigationId);

                    Log.Error("The sql statement is not executed!");
                    return new ResponseError(Code.BadRequest, "Câu lệnh sql không thể thực thi");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return new ResponseError(Code.ServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }

        private static NavigationModel GetNav(List<BsdNavigation> navs, BsdNavigation nav, bool isGetRoles)
        {
            var rNav = new NavigationModel
            {
                Id = nav.Id,
                ApplicationId = nav.ApplicationId,
                ParentId = nav.ParentId,
                IconClass = nav.IconClass,
                Code = nav.Code,
                Name = nav.Name,
                Order = nav.Order,
                Status = nav.Status,
                UrlRewrite = nav.UrlRewrite,
                IdPath = nav.IdPath,
                Path = nav.Path,
                Level = nav.Level,
                SubUrl = nav.SubUrl,
                SubChild = new List<NavigationModel>(),
                RoleList = new List<Guid>()
            };
            using (var unitOfWork = new UnitOfWork())

            {
                if (isGetRoles)
                {
                    var navR = unitOfWork.GetRepository<BsdNavigationMapRole>().Get(s => s.NavigationId == nav.Id)
                        .Select(s => s.RoleId).Distinct().ToList();
                    var rL = new List<Guid>();
                    foreach (var item in navR) rL.Add(item);
                    rNav.RoleList = rL;
                }

                foreach (var tNav in navs)
                    if (tNav.ParentId == nav.Id)
                        rNav.SubChild.Add(GetNav(navs, tNav, isGetRoles));
            }

            return rNav;
        }

        private static void UpdateNavChilds(BsdNavigation parentNav, IRepository<BsdNavigation> navRepo)
        {
            try
            {
                var parentNavId = parentNav.Id;
                // 1. Lay ve danh sach tat ca cac nav con
                var childNavs = from navs in navRepo.GetAll()
                                where navs.ParentId == parentNavId
                                select navs;
                foreach (var childNav in childNavs)
                {
                    childNav.ParentId = parentNav.Id;
                    childNav.Level = parentNav.Level + 1;
                    childNav.Path = parentNav.Path + childNav.Name + "/";
                    childNav.IdPath = parentNav.IdPath + childNav.Id + "/";
                    childNav.LastModifiedOnDate = DateTime.Now;

                    navRepo.Update(childNav);
                    // 2.1 Cap nhat duong dan tat ca nav con
                    UpdateNavChilds(childNav, navRepo);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                throw;
            }
        }

        private static void MoveRole(Guid navigationId)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    var cNavRoleRepo = unitOfWork.GetRepository<BsdNavigationMapRole>();
                    var listParentsId = GetListParentNav(navigationId);
                    var listRole = unitOfWork.GetRepository<BsdNavigationMapRole>().Get(u => u.NavigationId == navigationId);
                    foreach (var navRole in listRole)
                        foreach (var item in listParentsId)
                        {
                            var navR = new BsdNavigationMapRole
                            {
                                RoleId = navRole.RoleId,
                                NavigationId = item,
                                FromSubNavigation = navRole.FromSubNavigation ?? navRole.NavigationId
                            };
                            cNavRoleRepo.Add(navR);
                        }

                    Log.Debug(unitOfWork.Save() >= 1 ? "Move role success!" : "Move role Error!");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
            }
        }

        private static bool CheckUrlRewrite(string url, Guid? navigationId)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    var data = from nav in unitOfWork.GetRepository<BsdNavigation>().GetAll()
                               where
                                   nav.UrlRewrite.ToLower() == url.ToLower()
                               select nav;
                    if (navigationId != null)
                        data = from dt in data where dt.Id != navigationId select dt;
                    if (data.Any())
                        return true;
                    return false;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return false;
            }
        }

        private static bool CheckCode(string code, Guid? navigationId)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    var data = from nav in unitOfWork.GetRepository<BsdNavigation>().GetAll()
                               where
                                   nav.Code.ToLower() == code.ToLower()
                               select nav;
                    if (navigationId != null)
                        data = from dt in data where dt.Id != navigationId select dt;
                    if (data.Any())
                        return true;
                    return false;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return false;
            }
        }

        private static List<Guid> GetListParentNav(Guid? navId)
        {
            using (var unitOfWork = new UnitOfWork())
            {
                var navIds = new List<Guid>();
                // Lay ve nhom nguoi dung
                var sNav = unitOfWork.GetRepository<BsdNavigation>().Get(sp => sp.Id == navId).FirstOrDefault();

                if (sNav?.ParentId != null)
                {
                    navIds.Add(sNav.ParentId.Value);
                    var navParentId = GetListParentNav(sNav.ParentId);
                    if (navParentId.Count > 0)
                        foreach (var item in navParentId)
                            navIds.Add(item);
                }

                return navIds;
            }
        }

        private static List<Guid> GetListChildNav(Guid? navId)
        {
            using (var unitOfWork = new UnitOfWork())
            {
                var navIds = new List<Guid>();
                // Lay ve tat ca dieu huong con cua dh hien tai
                var sNav = unitOfWork.GetRepository<BsdNavigation>().Get(sp => sp.ParentId == navId);

                if (sNav != null && sNav.Any())
                    foreach (var item in sNav)
                    {
                        navIds.Add(item.Id);
                        var navChildId = GetListChildNav(item.Id);
                        if (navChildId.Count > 0)
                            foreach (var request in navChildId)
                                navIds.Add(request);
                    }

                return navIds;
            }
        }

        private static NavigationModel GetNav(IList<BsdNavigation> navs, BsdNavigation nav)
        {
            var rNav = new NavigationModel
            {
                Id = nav.Id,
                ParentId = nav.ParentId,
                Code = nav.Code,
                Name = nav.Name,
                UrlRewrite = nav.UrlRewrite,
                IconClass = nav.IconClass,
                SubChild = new List<NavigationModel>()
            };
            //rNav.RoleList = Nav.NavigationRole.Select(s => s.RoleID.ToString()).ToArray();
            if (nav.HasChild)
                foreach (var tNav in navs)
                    if (tNav.ParentId == nav.Id)
                        rNav.SubChild.Add(GetNav(navs, tNav));
            return rNav;
        }
    }
}