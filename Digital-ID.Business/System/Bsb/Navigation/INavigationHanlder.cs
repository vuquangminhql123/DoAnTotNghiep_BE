﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DigitalID.Data;

namespace DigitalID.Business
{
    public interface INavigationHanlder
    {
        Task<Response> GetByUserIdAsync(Guid userId, Guid applicationId);
        Task<Response> GetTreeAsync(Guid applicationId, bool isGetRoles);
        Task<Response> CreateAsync(NavigationCreateModel request, Guid applicationId, Guid userId);
        Task<Response> UpdateAsync(Guid navigationId, NavigationUpdateModel request, Guid applicationId, Guid userId);
        Task<Response> DeleteAsync(Guid navigationId);
        Task<Response> UpdateRoleInNavigations(Guid navigationId, IList<Guid> listRoleId);
    }
}