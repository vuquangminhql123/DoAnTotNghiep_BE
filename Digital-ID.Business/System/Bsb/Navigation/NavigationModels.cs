﻿using System;
using System.Collections.Generic;

namespace DigitalID.Business
{
    public class BaseNavigationModel : BaseModel
    {
        public Guid Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string IdPath { get; set; }
        public string Path { get; set; }
        public int Level { get; set; }
        public int? Order { get; set; }
        public bool? Status { get; set; }
    }

    public class NavigationModel : BaseNavigationModel
    {
        public Guid? ParentId { get; set; }
        public string UrlRewrite { get; set; }
        public string IconClass { get; set; }
        public IList<NavigationModel> SubChild { get; set; }
        public List<Guid>  RoleList { get; set; }
        public string SubUrl { get; set; }
    }

    // Create request model
    public class NavigationCreateModel
    {
        public NavigationCreateModel()
        {
            Status = true;
            Order = 0;
        }

        public BaseNavigationModel ParentModel { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public bool? Status { get; set; }
        public int? Order { get; set; }
        public string UrlRewrite { get; set; }
        public string IconClass { get; set; }
        public List<Guid>  RoleList { get; set; }
        public string SubUrl { get; set; }
    }

    // Update request model
    public class NavigationUpdateModel
    {
        public BaseNavigationModel ParentModel { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public bool? Status { get; set; }
        public int? Order { get; set; }
        public string UrlRewrite { get; set; }
        public string IconClass { get; set; }
        public Guid? FromSubNavigation { get; set; }
        public List<Guid>  RoleList { get; set; }
        public string SubUrl { get; set; }
    }
}