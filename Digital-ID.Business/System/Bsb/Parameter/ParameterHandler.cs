﻿using LinqKit;
using DigitalID.Data;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace DigitalID.Business
{
    public class ParameterHandler : IParameterHandler
    {
        private readonly DbHandler<BsdParameter, ParameterModel, ParameterQueryModel> _dbHandler = DbHandler<BsdParameter, ParameterModel, ParameterQueryModel>.Instance;

        #region CRUD
        public Task<Response> FindAsync(Guid id)
        {
            return _dbHandler.FindAsync(id);
        }
        public async Task<Response> CreateAsync(ParameterCreateModel model, Guid applicationId, Guid userId)
        {

            BsdParameter request = AutoMapperUtils.AutoMap<ParameterCreateModel, BsdParameter>(model);
            request = request.InitCreate(applicationId, userId);
            request.Id = Guid.NewGuid();
            var result = await _dbHandler.CreateAsync(request);
            ParameterCollection.Instance.LoadToHashSet();
            return result;
        }
        public async Task<Response> UpdateAsync(Guid id, ParameterUpdateModel model, Guid applicationId, Guid userId)
        {
            BsdParameter request = AutoMapperUtils.AutoMap<ParameterUpdateModel, BsdParameter>(model);
            request = request.InitUpdate(applicationId, userId);
            request.Id = id;
            var result = await _dbHandler.UpdateAsync(id, request);
            ParameterCollection.Instance.LoadToHashSet();
            return result;
        }
        public async Task<Response> DeleteAsync(Guid id)
        {
            var result = await _dbHandler.DeleteAsync(id);
            ParameterCollection.Instance.LoadToHashSet();
            return result;
        }
        public async Task<Response> DeleteRangeAsync(IList<Guid> listId)
        {
            var result = await _dbHandler.DeleteRangeAsync(listId, "Id");
            ParameterCollection.Instance.LoadToHashSet();
            return result;
        }
        #endregion
        public Response GetAll()
        {
            return _dbHandler.GetAll();
        }
        public async Task<Response> GetAllAsync()
        {
            var predicate = PredicateBuilder.New<BsdParameter>(true);
            return await _dbHandler.GetAllAsync(predicate);
        }
        public Task<Response> GetAllAsync(ParameterQueryModel query)
        {
            var predicate = BuildQuery(query);
            return _dbHandler.GetAllAsync(predicate, query.Sort);
        }
        public Task<Response> GetPageAsync(ParameterQueryModel query)
        {
            var predicate = BuildQuery(query);
            return _dbHandler.GetPageAsync(predicate, query);
        }
        private Expression<Func<BsdParameter, bool>> BuildQuery(ParameterQueryModel query)
        {
            var predicate = PredicateBuilder.New<BsdParameter>(true);
            if (!string.IsNullOrEmpty(query.Name))
            {
                predicate.And(s => s.Name == query.Name);
            }
            if (!string.IsNullOrEmpty(query.Name))
            {
                predicate.And(s => s.Name == query.Name);
            }
            if (query.ListId != null && query.ListId.Count > 0)
            {
                predicate.And(s => query.ListId.Contains(s.Id));
            }
            if (!string.IsNullOrEmpty(query.FullTextSearch))
            {
                predicate.And(s => s.Name.ToLower().Contains(query.FullTextSearch.ToLower()) || s.Description.ToLower().Contains(query.FullTextSearch.ToLower()));
            }
            return predicate;
        }
    }
}
