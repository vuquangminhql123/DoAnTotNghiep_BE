﻿using DigitalID.Data;
using System;
namespace DigitalID.Business
{
    public class BaseParameterModel : BaseModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
    }
    public class ParameterModel : BaseParameterModel
    {
        public string Description { get; set; }
    }
    public class ParameterQueryModel : PaginationRequest
    {
        public string Value { get; set; }
        public string Name { get; set; }
    }

    public class ParameterCreateModel
    {
        public string Name { get; set; }
        public string Value { get; set; }
        public string Description { get; set; }
    }
    public class ParameterUpdateModel
    {
        public string Name { get; set; }
        public string Value { get; set; }
        public string Description { get; set; }
    }
}

