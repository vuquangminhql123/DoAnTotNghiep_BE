﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DigitalID.Data;

namespace DigitalID.Business
{
    public interface ILogHandler
    {
        Task<Response> GetPageAsync(LogQueryModel query);
    }
}