﻿using LinqKit;
using DigitalID.Data;
using System;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace DigitalID.Business
{
    public class LogHandler : ILogHandler
    {
        private readonly DbHandler<BsdLog, LogModel, LogQueryModel> _dbHandler = DbHandler<BsdLog, LogModel, LogQueryModel>.Instance;

        public Task<Response> GetPageAsync(LogQueryModel query)
        {
            var predicate = BuildQuery(query);
            return _dbHandler.GetPageAsync(predicate, query);
        }
        private Expression<Func<BsdLog, bool>> BuildQuery(LogQueryModel query)
        {
                
            var predicate = PredicateBuilder.New<BsdLog>(true);
            if (query.Level.HasValue)
            {
                predicate.And(s => s.Level == query.Level);
            }
            if (query.ApplicationId.HasValue)
            {
                predicate.And(s => s.ApplicationId == query.ApplicationId);
            }
            if (query.UserId.HasValue)
            {
                predicate.And(s => s.UserId == query.UserId);
            }
            if (query.FromDate.HasValue)
            {
                predicate.And(s => query.FromDate.Value <= s.Timestamp);
            }
            if (query.ToDate.HasValue)
            {
                predicate.And(s => query.ToDate.Value.AddDays(1) > s.Timestamp);
            }
            if (query.ListLevel != null && query.ListLevel.Count > 0)
            {
                predicate.And(s => query.ListLevel.Contains(s.Level.Value));
            }
            if (!string.IsNullOrEmpty(query.FullTextSearch))
            {
                predicate.And(s => s.Message.ToLower().Contains(query.FullTextSearch.ToLower()));
            }
            return predicate;
        }
    }
}
