﻿using DigitalID.Data;
using System;
using System.Collections.Generic;

namespace DigitalID.Business
{
    public class BaseLogModel
    {
        public long Id { get; set; }
        public string Message { get; set; }
        public byte Level { get; set; }
        public DateTime Timestamp { get; set; }
        public string Exception { get; set; }
        public Guid ApplicationId { get; set; }
        public Guid UserId { get; set; }
        public string MessageTemplate { get; set; }
        public string Properties { get; set; }
    }
    public class LogModel : BaseLogModel
    {
        public BaseUserModel User { get => UserCollection.Instance.GetModel(UserId); }
    }
    public class LogQueryModel : PaginationRequest
    {
        public byte? Level { get; set; }

        public List<byte> ListLevel { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public Guid? ApplicationId { get; set; }
        public Guid? UserId { get; set; }
    }
}

