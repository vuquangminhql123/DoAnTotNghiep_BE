﻿using DigitalID.Data;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DigitalID.Business
{
    public class DbTaxonomyTermsHandler : ITaxonomyTermsHandler
    {
        #region Function ConvertData
        private TaxonomyTermModel NewConvertData(TaxonomyTerm taxonomyTerm)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    TaxonomyTermModel taxonomyTermsClient = new TaxonomyTermModel
                    {
                        TermId = taxonomyTerm.TermId,
                        Vocabulary = new BaseTaxonomyVocabularyModel
                        {
                            VocabularyId = taxonomyTerm.VocabularyId,
                            Code = taxonomyTerm.VocabularyCode
                        },
                        ParentId = taxonomyTerm.ParentId,
                        Name = taxonomyTerm.Name,
                        Level = taxonomyTerm.Level.Value,
                        Order = taxonomyTerm.Order.Value,
                        IdPath = taxonomyTerm.IdPath,
                        Path = taxonomyTerm.Path,
                        ChildCount = taxonomyTerm.ChildCount,//childcount
                        Description = taxonomyTerm.Description,
                        CreatedOnDate = Convert.ToDateTime(taxonomyTerm.CreatedOnDate),
                        LastModifiedOnDate = Convert.ToDateTime(taxonomyTerm.LastModifiedOnDate),
                        CreatedByUserId = taxonomyTerm.CreatedByUserId.ToString(),
                        LastModifiedByUserId = taxonomyTerm.LastModifiedByUserId.ToString(),
                    };
                    //taxonomyTermsClient.VocabularyId = unitOfWork.GetRepository<Taxonomy_Vocabularies>().Find((Guid)taxonomyTerm.VocabularyId).Name;
                    return taxonomyTermsClient;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex,"");
                throw;
            }
        }
        private List<TaxonomyTermModel> NewConvertDatas(IEnumerable<TaxonomyTerm> taxonomyTerms)
        {
            try
            {
                List<TaxonomyTermModel> taxonomyList = new List<TaxonomyTermModel>();
                foreach (var taxonomyTerm in taxonomyTerms)
                {
                    taxonomyList.Add(NewConvertData(taxonomyTerm));
                }
                return taxonomyList;
            }
            catch (Exception ex)
            {
                Log.Error(ex,"");
                throw;
            }
        }

        #endregion

        #region Function Get term
        public OldResponse<IList<TaxonomyTermsClient>> Get()
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    List<TaxonomyTermsClient> taxonomyTermsList = new List<TaxonomyTermsClient>();
                    var taxonomyTerms = unitOfWork.GetRepository<TaxonomyTerm>().GetMany(sp => sp.ApplicationId == AppConstants.RootAppId,
                        sp => sp.OrderBy(s => s.Order), 0, "").ToList();
                    if (taxonomyTerms != null)
                    {
                        foreach (TaxonomyTerm taxonomyTerm in taxonomyTerms)
                        {
                            taxonomyTermsList.Add(ConvertData(taxonomyTerm));
                        }
                        return new OldResponse<IList<TaxonomyTermsClient>>(1, "", taxonomyTermsList);
                    }
                    else return new OldResponse<IList<TaxonomyTermsClient>>(0, "Taxonomy data not found", null);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex,"");
                return new OldResponse<IList<TaxonomyTermsClient>>(-1, ex.Message, null);
            }

        }

        public OldResponse<IList<TaxonomyTermsClient>> GetTermsByVocabularyId(string vocabularyId, int pageNumber, int pageSize, string textSearch)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    List<TaxonomyTermsClient> taxonomyTermsList = new List<TaxonomyTermsClient>();

                    if (pageNumber == 0)
                    {
                        pageNumber =1;
                    }

                    var guidVocabularyId = new Guid(vocabularyId);
                    pageSize = pageSize + 1;
                    
                    textSearch = textSearch != null ? textSearch.ToLower().Trim() : null;

                    if (string.IsNullOrEmpty(textSearch))
                    {
                        var taxonomyTerms = from term in unitOfWork.GetRepository<TaxonomyTerm>().GetAll()
                                            where term.VocabularyId == guidVocabularyId
                                            select term;

                        var queryCanXuLy = taxonomyTerms.OrderBy(s => s.Order).Skip((pageNumber - 1) * (pageSize)).Take(pageSize);

                        foreach (TaxonomyTerm taxonomyTerm in queryCanXuLy)
                        {
                            taxonomyTermsList.Add(ConvertData(taxonomyTerm));
                        }
                    }
                    else
                    {
                        var taxonomyTerms = from term in unitOfWork.GetRepository<TaxonomyTerm>().GetAll()
                                            where term.VocabularyId == guidVocabularyId && term.Name.Contains(textSearch)
                                            select term;
                        var queryCanXuLy = taxonomyTerms.OrderBy(s => s.Order).Skip((pageNumber - 1) * (pageSize)).Take(pageSize);

                        foreach (TaxonomyTerm taxonomyTerm in queryCanXuLy)                        
                        {
                            taxonomyTermsList.Add(ConvertData(taxonomyTerm));
                        }
                    }
                                                            
                    //  return taxonomyTermsList;
                    return new OldResponse<IList<TaxonomyTermsClient>>(1, "", taxonomyTermsList);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex,"");
                return new OldResponse<IList<TaxonomyTermsClient>>(-1, ex.Message, null);
            }
        }

        public OldResponse<IList<TaxonomyTermsClient>> GetAllTermsByVocabularyId(string vocabularyId)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    List<TaxonomyTermsClient> taxonomyTermsList = new List<TaxonomyTermsClient>();

                    var guidVocabularyId = new Guid(vocabularyId);

                    var taxonomyTerms = from term in unitOfWork.GetRepository<TaxonomyTerm>().GetAll()
                                        where term.VocabularyId == guidVocabularyId
                                        orderby term.Order ascending
                                        select term;

                    foreach (TaxonomyTerm taxonomyTerm in taxonomyTerms)
                    {
                        taxonomyTermsList.Add(ConvertData(taxonomyTerm));
                    }
                    //  return taxonomyTermsList;
                    return new OldResponse<IList<TaxonomyTermsClient>>(1, "", taxonomyTermsList);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex,"");
                return new OldResponse<IList<TaxonomyTermsClient>>(-1, ex.Message, null);
            }
        }

        public OldResponse<IList<TaxonomyTermsClient>> GetAllTermsByVocabulary(string vocabularyName)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    // Get vocabulary name
                    var vocabulary = (from v in unitOfWork.GetRepository<TaxonomyVocabulary>().GetAll()
                                      where v.Name == vocabularyName
                                      select v).FirstOrDefault();
                    
                    if (vocabulary != null)
                    {
                        return new OldResponse<IList<TaxonomyTermsClient>>(1, "", GetAllTermsByVocabularyId(vocabulary.VocabularyId.ToString()).Data);
                    }
                    return new OldResponse<IList<TaxonomyTermsClient>>(0, "Not Found", null);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex,"");
                return new OldResponse<IList<TaxonomyTermsClient>>(-1, ex.Message, null);
            }
        }

        public OldResponse<IList<TaxonomyTermsClient>> GetAllTermsByVocabularyCode(string vocabularyCode)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    // Get vocabulary name
                    var vocabulary = (from v in unitOfWork.GetRepository<TaxonomyVocabulary>().GetAll()
                                      where v.Code == vocabularyCode
                                      select v).FirstOrDefault();
                    
                    if (vocabulary != null)
                    {
                        return new OldResponse<IList<TaxonomyTermsClient>>(1, "", GetAllTermsByVocabularyId(vocabulary.VocabularyId.ToString()).Data);
                    }
                    return new OldResponse<IList<TaxonomyTermsClient>>(0, "Not Found", null);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex,"");
                return new OldResponse<IList<TaxonomyTermsClient>>(-1, ex.Message, null);
            }
        }

        public OldResponse<IList<TaxonomyTermsClient>> GetTermsByVocabulary(string vocabularyName, int pageNumber, int pageSize, string textSearch)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    // Get vocabulary name
                    var vocabulary = (from v in unitOfWork.GetRepository<TaxonomyVocabulary>().GetAll()
                                      where v.Name == vocabularyName
                                      select v).FirstOrDefault();
                    pageSize = pageSize + 1;
                    textSearch = textSearch != null ? textSearch.ToLower().Trim() : null;

                    if (vocabulary != null)
                    {
                        return new OldResponse<IList<TaxonomyTermsClient>>(1, "", GetTermsByVocabularyId(vocabulary.VocabularyId.ToString(),pageNumber,pageSize,textSearch).Data);
                    }
                    return new OldResponse<IList<TaxonomyTermsClient>>(0, "Not Found", null);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex,"");
                return new OldResponse<IList<TaxonomyTermsClient>>(-1, ex.Message, null);
            }
        }

        public OldResponse<TaxonomyTermsClient> GetTermsByName(string name)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    var taxonomyTerm = unitOfWork.GetRepository<TaxonomyTerm>().Get(sp => sp.Name == name && sp.ApplicationId == AppConstants.RootAppId).FirstOrDefault();
                    //  return ConvertData(taxonomyTerm);
                    return new OldResponse<TaxonomyTermsClient>(1, "", ConvertData(taxonomyTerm));
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex,"");
                return new OldResponse<TaxonomyTermsClient>(-1, ex.Message, null);
            }
        }

        public OldResponse<TaxonomyTermsClient> GetTermsByTermId(string termId)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    var taxonomyTerm = unitOfWork.GetRepository<TaxonomyTerm>().Get(sp => sp.TermId == new Guid(termId) && sp.ApplicationId == AppConstants.RootAppId).FirstOrDefault();
                    // return ConvertData(taxonomyTerm);
                    return new OldResponse<TaxonomyTermsClient>(1, "", ConvertData(taxonomyTerm));
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex,"");
                return new OldResponse<TaxonomyTermsClient>(-1, ex.Message, null);
            }
        }

        /// <summary>
        /// Lấy về TaxonomyTerms theo ma
        /// </summary>
        /// <param name="taxonomyTermsId">TermId</param>        
        /// <remarks>
        /// </remarks>
        public OldResponse<TaxonomyTermsClient> Get(string taxonomyTermsId)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    var taxonomyTerm = unitOfWork.GetRepository<TaxonomyTerm>().Get(sp => sp.TermId == Guid.Parse(taxonomyTermsId) && sp.ApplicationId == AppConstants.RootAppId).FirstOrDefault();
                    //  return ConvertData(taxonomyTerm);
                    return new OldResponse<TaxonomyTermsClient>(1, "", ConvertData(taxonomyTerm));
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex,"");
                return new OldResponse<TaxonomyTermsClient>(-1, ex.Message, null);
            }
        }

        /// <summary>
        /// Lấy về TaxonomyTermsClient theo mã TaxonomyTermsClient
        /// </summary>
        /// <param name="id">mã TaxonomyTermsClient</param>        
        /// <remarks>
        /// Mã TaxonomyTermsClient kiểu unique
        /// </remarks>
        public TaxonomyTermsClient GetById(Guid id)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    return ConvertData(unitOfWork.GetRepository<TaxonomyTerm>().Find(id));
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex,"");
                return null;
            }
        }

        public OldResponse<IList<TaxonomyTermsClient>> Get(TaxonomyTermsQueryFilter filter)
        {
            try
            {
                var result = new OldResponse<IList<TaxonomyTermsClient>>(1, string.Empty, new List<TaxonomyTermsClient>());

                int pageSize = filter.PageSize;
                int pageNumber = filter.PageNumber;
                string textSearch = filter.TextSearch;

                pageSize = pageSize + 1;
                textSearch = textSearch != null ? textSearch.ToLower().Trim() : null;

                using (var unitOfWork = new UnitOfWork())
                {
                    IEnumerable<TaxonomyTerm> data;

                    if (string.IsNullOrEmpty(textSearch))
                    {
                        data = unitOfWork.GetRepository<TaxonomyTerm>().GetPageMany(sp => sp.ApplicationId == AppConstants.RootAppId, sp => sp.OrderBy(s => s.Order), pageNumber, pageSize);
                    }
                    else
                    {
                        data = unitOfWork.GetRepository<TaxonomyTerm>().GetPageMany(sp => sp.ApplicationId == AppConstants.RootAppId && sp.Name.ToLower().Contains(textSearch), sp => sp.OrderBy(s => s.Order), pageNumber, pageSize);
                    }
                    //  return ConvertDatas(data);
                    return new OldResponse<IList<TaxonomyTermsClient>>(1, "", ConvertDatas(data));
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex,"");
                return new OldResponse<IList<TaxonomyTermsClient>>(-1, ex.Message, null);
            }

        }

        public OldResponse<IList<TaxonomyTermsClient>> Get(string parentId, int level)
        {
            try
            {
                List<TaxonomyTermsClient> list = new List<TaxonomyTermsClient>();

                using (var unitOfWork = new UnitOfWork())
                {
                    var gparentID = String.IsNullOrEmpty(parentId) ? Guid.Empty : new Guid(parentId);
                    var taxonomyTerms = unitOfWork.GetRepository<TaxonomyTerm>();

                    // 2.1 Lay ve danh sach loai chuong trinh
                    var query = (from terms in taxonomyTerms.GetAll()
                                 where terms.ParentId == gparentID
                                 orderby terms.Order ascending
                                 select new
                                 {
                                     TaxonomyTermsClientId = terms.TermId,
                                     TaxonomyTermsClientName = terms.Name,
                                     Description = terms.Description,
                                     ParentId = terms.ParentId,
                                     RealName = terms.Name
                                 });

                    var queryResult = query.ToList();

                    var taxonomyTerm = from p in queryResult
                                       select new TaxonomyTermsClient
                                           {
                                               TermId = p.TaxonomyTermsClientId.ToString(),
                                               Name = p.TaxonomyTermsClientName,
                                               RealName = p.RealName,
                                               Description = p.Description,
                                               ParentTermId = p.ParentId.ToString()
                                           };


                    if (taxonomyTerm != null && taxonomyTerm.Count() > 0)
                    {
                        string strprefix = "";
                        for (int i = 0; i < level; i++)
                        {
                            strprefix += "----";
                        }

                        foreach (var taxonomyTermItem in taxonomyTerm)
                        {
                            TaxonomyTermsClient item = new TaxonomyTermsClient();
                            string id = taxonomyTermItem.TermId;
                            item.TermId = id;
                            item.Name = strprefix + taxonomyTermItem.Name;
                            item.RealName = taxonomyTermItem.RealName;

                            list.Add(item);

                            var childData = Get(id, level + 1).Data;
                            if (childData != null)
                            {
                                foreach (var childItem in childData)
                                {
                                    TaxonomyTermsClient itemChild = new TaxonomyTermsClient();
                                    string idChild = childItem.TermId;
                                    itemChild.TermId = idChild;
                                    itemChild.Name = strprefix + childItem.Name;
                                    itemChild.RealName = childItem.RealName;

                                    list.Add(itemChild);
                                }
                            }
                        }

                        // return list;
                        return new OldResponse<IList<TaxonomyTermsClient>>(1, "", list);
                    }
                    return new OldResponse<IList<TaxonomyTermsClient>>(0, "Not Found", null);
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex,"");
                return new OldResponse<IList<TaxonomyTermsClient>>(-1, ex.Message, null);
            }
        }

        /// <summary>
        /// Lay ten loai chuong trinh theo Id
        /// </summary>
        /// <param name="taxonomyTermsId"></param>
        /// <returns></returns>
        public OldResponse<string> GetParentName(string taxonomyTermsId)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    var guiprogrammecategoryId = String.IsNullOrEmpty(taxonomyTermsId) ? Guid.Empty : new Guid(taxonomyTermsId);
                    var cProgrammeCategoryRepo = unitOfWork.GetRepository<TaxonomyTerm>();

                    // 2.1 Lay ve danh sach loai chuong trinh
                    var programmeCategory = (from programmeCategorys in cProgrammeCategoryRepo.GetAll()
                                             where programmeCategorys.TermId == guiprogrammecategoryId
                                             select new TaxonomyTermsClient()
                                             {
                                                 Name = programmeCategorys.Name
                                             }).FirstOrDefault();
                    if (programmeCategory != null)
                    {
                        // return programmeCategory.Name;
                        return new OldResponse<string>(-1, "", programmeCategory.Name);
                    }
                    return new OldResponse<string>(-1, "Not Found", null);
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex,"");
                return new OldResponse<string>(-1, ex.Message, null);
            }
        }

        #endregion

        #region Function Add, Update, Delete Term
     
        public OldResponse<TaxonomyTermsClient> Add(TaxonomyTermsClient term)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    var TermId = Guid.NewGuid();
                    var taxonomyTerm = new TaxonomyTerm()
                    {
                        TermId = TermId,
                        VocabularyId = new Guid(term.VocabularyId),
                        ApplicationId = AppConstants.RootAppId,
                        Name = term.Name,
                        ParentId = term.ParentTermId != null ? new Guid(term.ParentTermId) : new Guid("00000000-0000-0000-0000-000000000000"),
                        Description = term.Description,
                        CreatedByUserId = term.CreatedByUserID != null ? new Guid(term.CreatedByUserID) : UserConstants.AdministratorId,
                        CreatedOnDate = DateTime.Now,
                        LastModifiedByUserId = term.LastModifiedByUserID != null ? new Guid(term.LastModifiedByUserID) : UserConstants.AdministratorId,
                        LastModifiedOnDate = DateTime.Now,
                    };
                    unitOfWork.GetRepository<TaxonomyTerm>().Add(taxonomyTerm);
                    if (unitOfWork.Save() >= 1)
                        return new OldResponse<TaxonomyTermsClient>(1, "", ConvertData(taxonomyTerm));
                    else
                        return new OldResponse<TaxonomyTermsClient>(-1, "Data save with error", null);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex,"");
                return new OldResponse<TaxonomyTermsClient>(-1, ex.Message, null);
            }

        }
        public OldResponse<TaxonomyTermsClient> Update(TaxonomyTermsClient term)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    var utaxonomyTerm = unitOfWork.GetRepository<TaxonomyTerm>().Get(sp => sp.TermId == new Guid(term.TermId) && sp.ApplicationId == AppConstants.RootAppId).FirstOrDefault();


                    utaxonomyTerm.Name = term.Name;
                    utaxonomyTerm.Description = term.Description;
                    utaxonomyTerm.VocabularyId = new Guid(term.VocabularyId);
                    utaxonomyTerm.LastModifiedByUserId = term.LastModifiedByUserID != null ? new Guid(term.LastModifiedByUserID) : UserConstants.AdministratorId;
                    utaxonomyTerm.LastModifiedOnDate = DateTime.Now;
                    utaxonomyTerm.Order = term.Order;

                    unitOfWork.GetRepository<TaxonomyTerm>().Update(utaxonomyTerm);
                    if (unitOfWork.Save() >= 1)
                        return new OldResponse<TaxonomyTermsClient>(1, "", ConvertData(utaxonomyTerm));
                    else
                        return new OldResponse<TaxonomyTermsClient>(-1, "Data save with error", null);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex,"");
                return new OldResponse<TaxonomyTermsClient>(-1, ex.Message, null);
            }

        }

        /// <summary>
        /// Add new term
        /// </summary>
        /// <param name="term"></param>
        /// <returns></returns>
        public OldResponse<TaxonomyTermModel> Create(TaxonomyTermCreateRequestModel term)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    var termId = Guid.NewGuid();

                    var taxonomyTerm = new TaxonomyTerm()
                    {
                        TermId = termId,
                        Name = term.Name,
                        //ParentTermId = Guid.Parse(term.ParentTerm.TermId),
                        Description = term.Description,
                        CreatedByUserId = term.CreatedByUserId != Guid.Empty ? term.CreatedByUserId : Guid.Empty,
                        LastModifiedByUserId = term.CreatedByUserId != Guid.Empty ? term.CreatedByUserId : Guid.Empty,
                        CreatedOnDate = DateTime.Now,
                        LastModifiedOnDate = DateTime.Now,
                        ChildCount = 0,
                    };
                    if (term.Order > 0)
                    {
                        taxonomyTerm.Order = term.Order;
                    }
                    else
                    {
                        if (term.ParentTerm != null)
                        {
                            if (term.ParentTerm.TermId != Guid.Empty)
                            {
                                var maxOrderItem = unitOfWork.GetRepository<TaxonomyTerm>().Get(s => s.ParentId == term.ParentTerm.TermId).OrderByDescending(s => s.Order).FirstOrDefault();

                                if (maxOrderItem != null)
                                {
                                    taxonomyTerm.Order = maxOrderItem.Order + 1;
                                }
                                else
                                {
                                    taxonomyTerm.Order = 0;
                                }
                            }
                            else
                            {
                                taxonomyTerm.Order = 0;
                            }
                        }
                        else
                        {
                            taxonomyTerm.Order = 0;
                        }
                    }
                    var vocabulary = unitOfWork.GetRepository<TaxonomyVocabulary>().Find(term.VocabularyId);
                    //todo: giá trị mặc định thay đổi -> xóa đi
                    if (vocabulary == null || vocabulary.VocabularyId == null)
                    {
                        //todo: Giá trị default đã đúng chưa?: [Chuyên mục tin]
                        var defaultVocab = unitOfWork.GetRepository<TaxonomyVocabulary>().Find(new Guid("994E5408-BCC1-4A7C-9E63-D286B1F2C032"));
                        taxonomyTerm.VocabularyId = defaultVocab.VocabularyId;
                        taxonomyTerm.VocabularyCode = defaultVocab.Code;
                    }
                    else
                    {
                        taxonomyTerm.VocabularyId = vocabulary.VocabularyId;
                        taxonomyTerm.VocabularyCode = vocabulary.Code;
                    }

                    if (term.ParentTerm != null)
                    {
                        if (term.ParentTerm.TermId != Guid.Empty)
                        {
                            taxonomyTerm.ParentId = term.ParentTerm.TermId;
                            //var parentItem  = GetById(term.ParentTerm.TermId).Data;

                            var parentItem = (from t in unitOfWork.GetRepository<TaxonomyTerm>().GetAll()
                                              where t.TermId == taxonomyTerm.ParentId
                                              select t).FirstOrDefault();

                            if (parentItem != null)
                            {
                                parentItem.ChildCount++;
                                unitOfWork.GetRepository<TaxonomyTerm>().Update(parentItem);

                                var parentItem2 = ConvertData(parentItem);
                                // Update term path
                                //taxonomyTerm.IdPath = parentItem2.IdPath + taxonomyTerm.TermId + "/";
                                taxonomyTerm.Path = parentItem2.Path + taxonomyTerm.Name + "/";
                                taxonomyTerm.Level = parentItem2.Level + 1;
                            }
                            else
                            {
                                taxonomyTerm.IdPath = taxonomyTerm.TermId + "/";
                                taxonomyTerm.Path = taxonomyTerm.Name + "/";
                                taxonomyTerm.Level = 0;
                            }
                        }
                        else
                        {
                            taxonomyTerm.IdPath = taxonomyTerm.TermId + "/";
                            taxonomyTerm.Path = taxonomyTerm.Name + "/";
                            taxonomyTerm.Level = 0;
                        }
                    }
                    else
                    {
                        taxonomyTerm.IdPath = taxonomyTerm.TermId + "/";
                        taxonomyTerm.Path = taxonomyTerm.Name + "/";
                        taxonomyTerm.Level = 0;
                    }

                    unitOfWork.GetRepository<TaxonomyTerm>().Add(taxonomyTerm);

                    if (unitOfWork.Save() >= 1)
                        return new OldResponse<TaxonomyTermModel>(1, "", NewConvertData(taxonomyTerm));
                    else
                        return new OldResponse<TaxonomyTermModel>(0, "Data save with error", null);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex,"");
                return new OldResponse<TaxonomyTermModel>(-1, ex.Message, null);
            }
        }

        /// <summary>
        /// Add new term
        /// </summary>
        /// <param name="term"></param>
        /// <returns></returns>
        public OldResponse<TaxonomyTermModel> Create(TaxonomyTermCreateRequestModel term, UnitOfWork unitOfWork)
        {
            try
            {
                var termId = Guid.NewGuid();

                var taxonomyTerm = new TaxonomyTerm()
                {
                    TermId = termId,
                    Name = term.Name,
                    //ParentTermId = Guid.Parse(term.ParentTerm.TermId),
                    Description = term.Description,
                    CreatedByUserId = term.CreatedByUserId != Guid.Empty ? term.CreatedByUserId : Guid.Empty,
                    LastModifiedByUserId = term.CreatedByUserId != Guid.Empty ? term.CreatedByUserId : Guid.Empty,
                    //UserConstants.AdministratorId,
                    CreatedOnDate = DateTime.Now,
                    LastModifiedOnDate = DateTime.Now,
                    ChildCount = 0,
                };
                if (term.Order > 0)
                {
                    taxonomyTerm.Order = term.Order;
                }
                else
                {
                    if (term.ParentTerm != null)
                    {
                        if (term.ParentTerm.TermId != Guid.Empty)
                        {
                            var maxOrderItem = unitOfWork.GetRepository<TaxonomyTerm>().Get(s => s.ParentId == term.ParentTerm.TermId).OrderByDescending(s => s.Order).FirstOrDefault();

                            if (maxOrderItem != null)
                            {
                                taxonomyTerm.Order = maxOrderItem.Order + 1;
                            }
                            else
                            {
                                taxonomyTerm.Order = 0;
                            }
                        }
                        else
                        {
                            taxonomyTerm.Order = 0;
                        }
                    }
                    else
                    {
                        taxonomyTerm.Order = 0;
                    }
                }
                //undone: co the lay tu form truyen lai de ko phai get lai
                var vocabulary = unitOfWork.GetRepository<TaxonomyVocabulary>().Find(term.VocabularyId);
                if (vocabulary == null || vocabulary.VocabularyId == null)
                {
                    //todo: Giá trị default đã đúng chưa?: [Chuyên mục tin]
                    var defaultVocab = unitOfWork.GetRepository<TaxonomyVocabulary>().Find(new Guid("994E5408-BCC1-4A7C-9E63-D286B1F2C032"));
                    taxonomyTerm.VocabularyId = defaultVocab.VocabularyId;
                    taxonomyTerm.VocabularyCode = defaultVocab.Code;
                }
                else
                {
                    taxonomyTerm.VocabularyId = vocabulary.VocabularyId;
                    taxonomyTerm.VocabularyCode = vocabulary.Code;
                }

                if (term.ParentTerm != null)
                {
                    if (term.ParentTerm.TermId != Guid.Empty)
                    {
                        taxonomyTerm.ParentId = term.ParentTerm.TermId;
                        //var parentItem  = GetById(term.ParentTerm.TermId).Data;

                        var parentItem = (from t in unitOfWork.GetRepository<TaxonomyTerm>().GetAll()
                                          where t.TermId == taxonomyTerm.ParentId
                                          select t).FirstOrDefault();

                        if (parentItem != null)
                        {
                            parentItem.ChildCount++;
                            unitOfWork.GetRepository<TaxonomyTerm>().Update(parentItem);

                            var parentItem2 = ConvertData(parentItem);
                            // Update term path
                            //taxonomyTerm.IdPath = parentItem2.IdPath + taxonomyTerm.TermId + "/";
                            taxonomyTerm.Path = parentItem2.Path + taxonomyTerm.Name + "/";
                            taxonomyTerm.Level = parentItem2.Level + 1;
                        }
                        else
                        {
                            taxonomyTerm.IdPath = taxonomyTerm.TermId + "/";
                            taxonomyTerm.Path = taxonomyTerm.Name + "/";
                            taxonomyTerm.Level = 0;
                        }
                    }
                    else
                    {
                        taxonomyTerm.IdPath = taxonomyTerm.TermId + "/";
                        taxonomyTerm.Path = taxonomyTerm.Name + "/";
                        taxonomyTerm.Level = 0;
                    }
                }
                else
                {
                    taxonomyTerm.IdPath = taxonomyTerm.TermId + "/";
                    taxonomyTerm.Path = taxonomyTerm.Name + "/";
                    taxonomyTerm.Level = 0;
                }

                unitOfWork.GetRepository<TaxonomyTerm>().Add(taxonomyTerm);
                return new OldResponse<TaxonomyTermModel>(1, "", NewConvertData(taxonomyTerm));
            }
            catch (Exception ex)
            {
                Log.Error(ex,"");
                return new OldResponse<TaxonomyTermModel>(-1, ex.Message, null);
            }
        }

        /// <summary>
        /// update term
        /// </summary>
        /// <param name="term"></param>
        /// <returns></returns>
        public OldResponse<TaxonomyTermModel> Update(TaxonomyTermUpdateRequestModel term)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    var termsRepo = unitOfWork.GetRepository<TaxonomyTerm>();
                    var utaxonomyTerm = unitOfWork.GetRepository<TaxonomyTerm>().Get(sp => sp.TermId == term.TermId).FirstOrDefault();

                    utaxonomyTerm.Name = term.Name;
                    utaxonomyTerm.Description = term.Description;
                    utaxonomyTerm.LastModifiedByUserId = term.LastModifiedByUserId != Guid.Empty ? term.LastModifiedByUserId : Guid.Empty;
                    // UserConstants.AdministratorId;
                    utaxonomyTerm.LastModifiedOnDate = DateTime.Now;
                    utaxonomyTerm.Order = term.Order;
                    var vocabulary = unitOfWork.GetRepository<TaxonomyVocabulary>().Find(term.VocabularyId);
                    if (vocabulary != null)
                    {
                        utaxonomyTerm.VocabularyId = vocabulary.VocabularyId;
                        utaxonomyTerm.VocabularyCode = vocabulary.Code;
                    }
                    //else
                    //{
                    //    var defaultVocab = unitOfWork.GetRepository<TaxonomyVocabulary>().Find(new Guid("994E5408-BCC1-4A7C-9E63-D286B1F2C032"));
                    //    utaxonomyTerm.VocabularyId = defaultVocab.VocabularyId;
                    //    utaxonomyTerm.VocabularyCode = defaultVocab.Code;
                    //}

                    // Indicate if parent term is changed
                    var isParentTermChanged = false;

                    var oldParent = utaxonomyTerm.ParentId;
                    // Get parent taxonomy
                    if (term.ParentTerm != null)
                    {
                        if (term.ParentTerm.TermId != Guid.Empty && term.ParentTerm.TermId != null)
                        {
                            if (term.ParentTerm.TermId != oldParent)
                            {
                                var childList = GetListChildTerm(utaxonomyTerm.TermId);
                                //kiem tra nav cha khong dk la chinh no hoac con no
                                childList.Add(utaxonomyTerm.TermId);
                                foreach (var termId in childList)
                                {
                                    if (termId == term.ParentTerm.TermId)
                                        return new OldResponse<TaxonomyTermModel>(0, "error: Không được chọn term cha là term con của điều hướng hiện tại", null);
                                }

                                var parentItem = (from t in unitOfWork.GetRepository<TaxonomyTerm>().GetAll()
                                                  where t.TermId == term.ParentTerm.TermId
                                                  select t).FirstOrDefault();
                                if (parentItem != null)
                                {
                                    isParentTermChanged = true;
                                    utaxonomyTerm.ParentId = term.ParentTerm.TermId;

                                    //todo
                                    //parentItem.ChildCount = 0;
                                    //unitOfWork.GetRepository<TaxonomyTerm>().Update(parentItem);

                                    utaxonomyTerm.IdPath = parentItem.IdPath + term.TermId + "/";
                                    utaxonomyTerm.Path = parentItem.Path + term.Name + "/";
                                    utaxonomyTerm.Level = parentItem.Level + 1;
                                }
                                else
                                {
                                    return new OldResponse<TaxonomyTermModel>(0, "error: Không tìm thấy thông tin điều hướng cha đã chọn", null);
                                }
                            }
                            //}
                            //nếu cha không thay đổi => giữ nguyên id path, path và level
                            //else{ }
                        }
                        //nếu cha rỗng
                        else
                        {
                            if (oldParent != Guid.Empty)
                            {
                                isParentTermChanged = true;
                                utaxonomyTerm.ParentId = null;
                                utaxonomyTerm.IdPath = utaxonomyTerm.TermId + "/";
                                utaxonomyTerm.Path = utaxonomyTerm.Name + "/";
                                utaxonomyTerm.Level = 0;
                            }
                        }
                    }
                    //nếu cha null
                    else
                    {
                        if (oldParent != null)
                        {
                            isParentTermChanged = true;
                            utaxonomyTerm.ParentId = null;
                            utaxonomyTerm.IdPath = utaxonomyTerm.TermId + "/";
                            utaxonomyTerm.Path = utaxonomyTerm.Name + "/";
                            utaxonomyTerm.Level = 0;
                        }
                    }
                    termsRepo.Update(utaxonomyTerm);

                    if (isParentTermChanged)
                        //UPDATE CHILDRENT TERM
                        UpdateTermChilds(utaxonomyTerm, termsRepo);
                    if (unitOfWork.Save() >= 1)
                    {
                        return new OldResponse<TaxonomyTermModel>(1, "Update success", null);
                    }
                    else
                        return new OldResponse<TaxonomyTermModel>(0, "Update failed", null);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex,"");
                return new OldResponse<TaxonomyTermModel>(-1, ex.Message, null);
            }

        }
        /// <summary>
        /// update term overwite
        /// </summary>
        /// <param name="term"></param>
        /// <returns></returns>
        public OldResponse<TaxonomyTermModel> Update(TaxonomyTermUpdateRequestModel term, UnitOfWork unitOfWork)
        {
            try
            {
                var termsRepo = unitOfWork.GetRepository<TaxonomyTerm>();
                var utaxonomyTerm = unitOfWork.GetRepository<TaxonomyTerm>().Get(sp => sp.TermId == term.TermId).FirstOrDefault();

                utaxonomyTerm.Name = term.Name;
                utaxonomyTerm.Description = term.Description;
                utaxonomyTerm.LastModifiedByUserId = term.LastModifiedByUserId != Guid.Empty ? term.LastModifiedByUserId : Guid.Empty;
                // UserConstants.AdministratorId;
                utaxonomyTerm.LastModifiedOnDate = DateTime.Now;
                utaxonomyTerm.Order = term.Order;
                var vocabulary = unitOfWork.GetRepository<TaxonomyVocabulary>().Find(term.VocabularyId);
                if (vocabulary != null)
                {
                    utaxonomyTerm.VocabularyId = vocabulary.VocabularyId;
                    utaxonomyTerm.VocabularyCode = vocabulary.Code;
                }
                //else
                //{
                //    var defaultVocab = unitOfWork.GetRepository<TaxonomyVocabulary>().Find(new Guid("994E5408-BCC1-4A7C-9E63-D286B1F2C032"));
                //    utaxonomyTerm.VocabularyId = defaultVocab.VocabularyId;
                //    utaxonomyTerm.VocabularyCode = defaultVocab.Code;
                //}

                // Indicate if parent term is changed
                var isParentTermChanged = false;

                var oldParent = utaxonomyTerm.ParentId;
                // Get parent taxonomy
                if (term.ParentTerm != null)
                {
                    if (term.ParentTerm.TermId != Guid.Empty && term.ParentTerm.TermId != null)
                    {
                        if (term.ParentTerm.TermId != oldParent)
                        {
                            var childList = GetListChildTerm(utaxonomyTerm.TermId);
                            //kiem tra nav cha khong dk la chinh no hoac con no
                            childList.Add(utaxonomyTerm.TermId);
                            foreach (var termId in childList)
                            {
                                if (termId == term.ParentTerm.TermId)
                                    return new OldResponse<TaxonomyTermModel>(0, "error: Không được chọn term cha là term con của điều hướng hiện tại", null);
                            }

                            var parentItem = (from t in unitOfWork.GetRepository<TaxonomyTerm>().GetAll()
                                              where t.TermId == term.ParentTerm.TermId
                                              select t).FirstOrDefault();
                            if (parentItem != null)
                            {
                                isParentTermChanged = true;
                                utaxonomyTerm.ParentId = term.ParentTerm.TermId;

                                //todo
                                //parentItem.ChildCount = 0;
                                //unitOfWork.GetRepository<TaxonomyTerm>().Update(parentItem);

                                utaxonomyTerm.IdPath = parentItem.IdPath + term.TermId + "/";
                                utaxonomyTerm.Path = parentItem.Path + term.Name + "/";
                                utaxonomyTerm.Level = parentItem.Level + 1;
                            }
                            else
                            {
                                return new OldResponse<TaxonomyTermModel>(0, "error: Không tìm thấy thông tin điều hướng cha đã chọn", null);
                            }
                        }
                        //}
                        //nếu cha không thay đổi => giữ nguyên id path, path và level
                        //else{ }
                    }
                    //nếu cha rỗng
                    else
                    {
                        if (oldParent != Guid.Empty)
                        {
                            isParentTermChanged = true;
                            utaxonomyTerm.ParentId = null;
                            utaxonomyTerm.IdPath = utaxonomyTerm.TermId + "/";
                            utaxonomyTerm.Path = utaxonomyTerm.Name + "/";
                            utaxonomyTerm.Level = 0;
                        }
                    }
                }
                //nếu cha null
                else
                {
                    if (oldParent != null)
                    {
                        isParentTermChanged = true;
                        utaxonomyTerm.ParentId = null;
                        utaxonomyTerm.IdPath = utaxonomyTerm.TermId + "/";
                        utaxonomyTerm.Path = utaxonomyTerm.Name + "/";
                        utaxonomyTerm.Level = 0;
                    }
                }
                termsRepo.Update(utaxonomyTerm);

                if (isParentTermChanged)
                    //UPDATE CHILDRENT TERM
                    UpdateTermChilds(utaxonomyTerm, termsRepo);
                return null;
            }
            catch (Exception ex)
            {
                Log.Error(ex,"");
                return new OldResponse<TaxonomyTermModel>(-1, ex.Message, null);
            }

        }

        private List<Guid> GetListChildTerm(Guid? termId)
        {
            using (var unitOfWork = new UnitOfWork())
            {
                List<Guid> termIdList = new List<Guid>();
                // Lay ve tat ca term con cua dh hien tai
                var termList = unitOfWork.GetRepository<TaxonomyTerm>().Get(sp => sp.ParentId == termId);

                if (termList != null || termList.Count() > 0)
                {
                    foreach (var item in termList)
                    {
                        termIdList.Add(item.TermId);
                        var termChildId = GetListChildTerm(item.TermId);
                        if (termChildId.Count > 0)
                        {
                            foreach (var obj in termChildId)
                            {
                                termIdList.Add(obj);
                            }
                        }
                    }
                }
                return termIdList;
            }
        }
        private void UpdateTermChilds(TaxonomyTerm ParentTerm, IRepository<TaxonomyTerm> termRepo)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    var parentTermId = ParentTerm.TermId;
                    // 1. Lay ve danh sach tat ca cac term con
                    var childTerms = (from t in termRepo.GetAll()
                                      where t.ParentId == parentTermId
                                      select t);
                    if (childTerms != null)
                    {
                        // 2. Duyet qua tung term con
                        foreach (var childTerm in childTerms)
                        {
                            childTerm.ParentId = parentTermId;
                            childTerm.Level = ParentTerm.Level + 1;
                            childTerm.Path = ParentTerm.Path + childTerm.Name + "/";
                            childTerm.IdPath = ParentTerm.IdPath + childTerm.TermId + "/";
                            childTerm.LastModifiedOnDate = DateTime.Now;

                            termRepo.Update(childTerm);
                            // 2.1 Cap nhat duong dan tat ca term con
                            UpdateTermChilds(childTerm, termRepo);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex,"");
                throw;
            }
        }

        #region Delete Term
        /// <summary>
        /// Xóa TaxonomyTermsClient
        /// </summary>
        /// <param name="taxonomyTermsId">mã TaxonomyTermsClient</param>        
        /// <remarks>
        /// mã TaxonomyTermsClient kiểu unique
        /// </remarks>
        public bool Delete(Guid taxonomyTermsId)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    var cTaxonomyTermsClientRepo = unitOfWork.GetRepository<TaxonomyTerm>();
                    var cTaxonomyTermsClient = cTaxonomyTermsClientRepo.Find(taxonomyTermsId);
                    cTaxonomyTermsClientRepo.Delete(cTaxonomyTermsClient);
                    if (unitOfWork.Save() >= 1)
                        return true;
                    return false;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex,"");
                return false;
            }
        }

        /// <summary>
        /// Xóa TaxonomyTermsClient
        /// </summary>
        /// <param name="taxonomyTermsId">mã TaxonomyTermsClient</param>        
        /// <remarks>
        /// mã TaxonomyTermsClient kiểu string
        /// </remarks>
        public bool Delete(string taxonomyTermsId)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    var cTaxonomyTermsClientRepo = unitOfWork.GetRepository<TaxonomyTerm>();
                    var cTaxonomyTermsClient = cTaxonomyTermsClientRepo.Find(new Guid(taxonomyTermsId));

                    cTaxonomyTermsClientRepo.Delete(cTaxonomyTermsClient);
                    if (unitOfWork.Save() >= 1)
                        return true;
                    return false;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex,"");
                return false;
            }
        }

        public bool DeleteMany(List<TaxonomyTermsClient> terms)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    for (int i = 0; i < terms.Count; i++)
                    {
                        var cTaxonomyTermsClientRepo = unitOfWork.GetRepository<TaxonomyTerm>();
                        var cTaxonomyTermsClient = cTaxonomyTermsClientRepo.Find(terms[i].TermId);

                        cTaxonomyTermsClientRepo.Delete(cTaxonomyTermsClient);
                    }

                    if (unitOfWork.Save() >= 1)
                        return true;
                    return false;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex,"");
                return false;
            }
        }

        #endregion

        #endregion

        #region Function Override
        public OldResponse<TaxonomyTermsClient> Add(TaxonomyTermsClient term, UnitOfWork unitOfWork)
        {
            try
            {
                var TermId = Guid.NewGuid();
                var taxonomyTerm = new TaxonomyTerm()
                {
                    TermId = TermId,
                    VocabularyId = new Guid(term.VocabularyId),
                    ApplicationId = AppConstants.RootAppId,
                    Name = term.Name,
                    ParentId = term.ParentTermId != null ? new Guid(term.ParentTermId) : new Guid("00000000-0000-0000-0000-000000000000"),
                    Description = term.Description,
                    CreatedByUserId = term.CreatedByUserID != null ? new Guid(term.CreatedByUserID) : UserConstants.AdministratorId,
                    CreatedOnDate = DateTime.Now,
                    LastModifiedByUserId = term.LastModifiedByUserID != null ? new Guid(term.LastModifiedByUserID) : UserConstants.AdministratorId,
                    LastModifiedOnDate = DateTime.Now,
                };
                unitOfWork.GetRepository<TaxonomyTerm>().Add(taxonomyTerm);
                return new OldResponse<TaxonomyTermsClient>(1, "", term);
                
            }
            catch (Exception ex)
            {
                Log.Error(ex,"");
                return new OldResponse<TaxonomyTermsClient>(-1, "Add Term error", term);
            }
        }
        public OldResponse<TaxonomyTermsClient> Update(TaxonomyTermsClient term,UnitOfWork unitOfWork)
        {
            try
            {
              
                    var utaxonomyTerm = unitOfWork.GetRepository<TaxonomyTerm>().Get(sp => sp.TermId == new Guid(term.TermId) && sp.ApplicationId == AppConstants.RootAppId).FirstOrDefault();


                    utaxonomyTerm.Name = term.Name;
                    utaxonomyTerm.Description = term.Description;
                    utaxonomyTerm.VocabularyId = new Guid(term.VocabularyId);
                    utaxonomyTerm.LastModifiedByUserId = term.LastModifiedByUserID != null ? new Guid(term.LastModifiedByUserID) : UserConstants.AdministratorId;
                    utaxonomyTerm.LastModifiedOnDate = DateTime.Now;

                    unitOfWork.GetRepository<TaxonomyTerm>().Update(utaxonomyTerm);
    
                    return new OldResponse<TaxonomyTermsClient>(1, "", ConvertData(utaxonomyTerm));
                  
            }
            catch (Exception ex)
            {
                Log.Error(ex,"");
                return new OldResponse<TaxonomyTermsClient>(-1, "Update Term error", term);
            }

        }
        public bool Delete(string taxonomyTermsId, UnitOfWork unitOfWork)
        { 
            try
            {
                    var cTaxonomyTermsClientRepo = unitOfWork.GetRepository<TaxonomyTerm>();
                    var cTaxonomyTermsClient = cTaxonomyTermsClientRepo.Find(new Guid(taxonomyTermsId));
                    cTaxonomyTermsClientRepo.Delete(cTaxonomyTermsClient);
                    return true;
                }
            
            catch (Exception ex)
            {
                Log.Error(ex,"");
                return false;
            }
        }

        #endregion
       
        #region Function ConvertData
        private TaxonomyTermsClient ConvertData(TaxonomyTerm taxonomyTerm)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    TaxonomyTermsClient taxonomyTermsClient = new TaxonomyTermsClient
                    {
                        TermId = taxonomyTerm.TermId.ToString(),
                        // VocabularyId = taxonomyTerm.VocabularyId.ToString(),
                        ParentTermId = taxonomyTerm.ParentId.ToString(),
                        Name = taxonomyTerm.Name,
                        Description = taxonomyTerm.Description,
                        Weight = taxonomyTerm.Weight.ToString(),
                        TermLeft = taxonomyTerm.TermLeft.ToString(),
                        TermRight = taxonomyTerm.TermRight.ToString(),
                        CreatedOnDate = Convert.ToDateTime(taxonomyTerm.CreatedOnDate),
                        Order = taxonomyTerm.Order
                    };
                    var voc = unitOfWork.GetRepository<TaxonomyVocabulary>().Find((Guid)taxonomyTerm.VocabularyId);
                    taxonomyTermsClient.VocabularyId = voc.VocabularyId.ToString();
                    taxonomyTermsClient.VocabularyName = voc.Name;
                    return taxonomyTermsClient;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex,"");
                throw;
            }
        }
        private List<TaxonomyTermsClient> ConvertDatas(IEnumerable<TaxonomyTerm> taxonomyTerms)
        {
            try
            {
                List<TaxonomyTermsClient> taxonomyList = new List<TaxonomyTermsClient>();
                foreach (TaxonomyTerm taxonomyTerm in taxonomyTerms)
                {
                    taxonomyList.Add(ConvertData(taxonomyTerm));
                }
                return taxonomyList;
            }
            catch (Exception ex)
            {
                Log.Error(ex,"");
                throw;
            }
        }
        #endregion

        #region Create Term
        public bool CreateTaxonomyTerms(TaxonomyTerm cTaxonomyTerms)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    unitOfWork.GetRepository<TaxonomyTerm>().Add(cTaxonomyTerms);
                    if (unitOfWork.Save() >= 1)
                    {
                        return true;
                    };
                    return false;
                }
            }

            catch (Exception ex)
            {
                Log.Error(ex,"");
                return true;
            }
        }
        #endregion

    }
}