﻿using DigitalID.Data;
using System;
using System.Collections.Generic;

namespace DigitalID.Business
{
    public interface ITaxonomyTermsHandler
    {
        OldResponse<IList<TaxonomyTermsClient>> Get();
        OldResponse<TaxonomyTermsClient> Get(string taxonomyTermsId);
        OldResponse<string> GetParentName(string taxonomyTermsId);
        OldResponse<TaxonomyTermsClient> GetTermsByName(string name);
        OldResponse<TaxonomyTermsClient> GetTermsByTermId(string termId);
        OldResponse<IList<TaxonomyTermsClient>> Get(TaxonomyTermsQueryFilter filter);
        OldResponse<IList<TaxonomyTermsClient>> Get(string parentId, int level);
        OldResponse<IList<TaxonomyTermsClient>> GetTermsByVocabulary(string vocabularyName, int pageNumber, int pageSize, string textSearch);
        OldResponse<IList<TaxonomyTermsClient>> GetAllTermsByVocabulary(string vocabularyName);
        bool CreateTaxonomyTerms(TaxonomyTerm cTaxonomyTerms);
        OldResponse<TaxonomyTermsClient> Add(TaxonomyTermsClient term);
        OldResponse<TaxonomyTermsClient> Update(TaxonomyTermsClient term);
        bool DeleteMany(List<TaxonomyTermsClient> terms);
        bool Delete(string taxonomyTermsId);
        bool Delete(Guid taxonomyTermsId);
        OldResponse<IList<TaxonomyTermsClient>> GetAllTermsByVocabularyCode(string vocabularyCode);
    }
}
