﻿using System;
using System.Collections.Generic;

namespace DigitalID.Business
{
    public class TaxonomyTermsClient
    {
        public string TermId { get; set; }
        public string VocabularyId { get; set; }
        public string ParentTermId { get; set; }
        public string Name { get; set; }
        public string RealName { get; set; }
        public string Description { get; set; }
        public string Weight { get; set; }
        public string TermLeft { get; set; }
        public string TermRight { get; set; }
        public string CreatedByUserID { get; set; }
        public DateTime CreatedOnDate { get; set; }
        public string LastModifiedByUserID { get; set; }
        public DateTime LastModifiedOnDate { get; set; }
        public string ApplicationID { get; set; }
        public bool DeleteSuccess { get; set; }
        public int? Order { get; set; }

        public string VocabularyName { get; set; }
        public Guid IdPath { get; set; }
        public string Path { get; set; }
        public int Level { get; set; }
    }
    
    public enum TaxonomyTermsQueryOrder
    {
        NGAY_TAO_DESC,
        NGAY_TAO_ASC,
        ID_DESC,
        ID_ASC
    }

    public class TaxonomyTermsQueryFilter
    {
        public int PageSize { get; set; }
        public int PageNumber { get; set; }
        public string TextSearch { get; set; }
        public TaxonomyTermsQueryOrder Order { get; set; }

        public TaxonomyTermsQueryFilter()
        {
            PageSize = 0;
            PageNumber = 1;
            TextSearch = string.Empty;
            Order = TaxonomyTermsQueryOrder.NGAY_TAO_DESC;
        }
    }




    public class BaseTaxonomyTermModel
    {
        public Guid TermId { get; set; }
        public string Name { get; set; }

    }
    public class ParentTaxonomyTermModel
    {
        public Guid? TermId { get; set; }
        //todo: delete
        //public string Name { get; set; }
        //public Nullable<bool> HasChild { get; set; }
        //public string IdPath { get; set; }
        //public int Level { get; set; }
        //public string Path { get; set; }
    }
    public class TaxonomyTermModel : BaseTaxonomyTermModel
    {
        public BaseTaxonomyVocabularyModel Vocabulary { get; set; }
        public Guid? ParentId { get; set; }
        public string Description { get; set; }
        public List<TaxonomyTermModel> SubChild { get; set; }
        public int? ChildCount { get; set; }
        public string IdPath { get; set; }
        public int Level { get; set; }
        public int Order { get; set; }
        public string Path { get; set; }
        //undone: base user
        public string CreatedByUserId { get; set; }//
        public DateTime CreatedOnDate { get; set; }
        public string LastModifiedByUserId { get; set; }//
        public DateTime LastModifiedOnDate { get; set; }
        //undone
        //sau bỏ trường Weight,ApplicationId  đi
        public string Weight { get; set; }
    }
    // Create request model
    public class TaxonomyTermCreateRequestModel
    {
        public string Name { get; set; }
        public Guid VocabularyId { get; set; }
        public ParentTaxonomyTermModel ParentTerm { get; set; }
        public string Description { get; set; }
        public int Order { get; set; }
        public Guid CreatedByUserId { get; set; }
    }
    // Update request model
    public class TaxonomyTermUpdateRequestModel : BaseTaxonomyTermModel
    {
        public Guid VocabularyId { get; set; }
        public ParentTaxonomyTermModel ParentTerm { get; set; }
        public string Description { get; set; }
        //public string IdPath { get; set; }
        //public string Path { get; set; }
        public int Level { get; set; }
        public int Order { get; set; }
        public Guid LastModifiedByUserId { get; set; }
        public DateTime LastModifiedOnDate { get; set; }
    }
    // RequestModel thường được sử dụng cho việc Delete nhiều bản ghi
    public class TaxonomyTermMultiDeleteRequestModel
    {
        public List<Guid> ListTermId { get; set; }
        public int? DeleteType { get; set; }//
    }
    public class TaxonomyTermDeleteResponseModel    // MANDATORY*
    {
        public Guid? TermId { get; set; }
        public string Name { get; set; }
        public int? Result { get; set; }
        public string Message { get; set; }
    }
    public enum TaxonomyTermQueryOrder
    {
        NGAY_TAO_DESC,
        NGAY_TAO_ASC,
        ID_DESC,
        ID_ASC
    }

    public class TaxonomyTermQueryFilter
    {
        public int? PageSize { get; set; }
        public int? PageNumber { get; set; }
        public string TextSearch { get; set; }
        public Guid? TermId { get; set; }
        public Guid? ParentId { get; set; }
        public string Name { get; set; }
        public Guid? VocabularyId { get; set; }
        //undone://chưa làm filter theo loại VocabularyTypeId
        public Guid? VocabularyTypeId { get; set; }
        public string VocabularyCode { get; set; }
        public string Description { get; set; }
        public int? Level { get; set; }
        public TaxonomyTermQueryOrder Order { get; set; }
        public TaxonomyTermQueryFilter()
        {
            PageSize = 0;
            PageNumber = 1;
            TextSearch = string.Empty;
            Order = TaxonomyTermQueryOrder.NGAY_TAO_DESC;
        }
    }

}
