﻿using DigitalID.Data;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DigitalID.Business
{
    public class DbTaxonomyVocabulariesHandler : ITaxonomyVocabulariesHandler
    {
        public OldResponse<IList<TaxonomyVocabulariesClient>> Get()
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    IEnumerable<TaxonomyVocabulary> data = unitOfWork.GetRepository<TaxonomyVocabulary>().Get(sp => sp.VocabularyId != null).OrderBy(sp => sp.Name);
                    if (data != null)
                        return new OldResponse<IList<TaxonomyVocabulariesClient>>(1, "", ConvertDatas(data));
                    else
                        return new OldResponse<IList<TaxonomyVocabulariesClient>>(0, " data not found", null);
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex,"");
                return new OldResponse<IList<TaxonomyVocabulariesClient>>(-1, ex.Message, null);
            }
        }
        public OldResponse<TaxonomyVocabulary> Get(string name)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    var vocabulary = (from s in unitOfWork.GetRepository<TaxonomyVocabulary>().GetAll()
                                      where s.Name == name
                                      select s).FirstOrDefault();
                    if (vocabulary != null)
                    {
                       // return vocabulary;
                        return new OldResponse<TaxonomyVocabulary>(1, "", vocabulary);
                    }

                    return new OldResponse<TaxonomyVocabulary>(0, "Not Found", null);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex,"");
                return new OldResponse<TaxonomyVocabulary>(-1, ex.Message, null);
            }
        }

        /// <summary>
        /// Thêm mới TaxonomyVocabularies
        /// </summary>
        /// <param name="taxonomyVocabularies">TaxonomyVocabularies được thêm mới</param>        
        /// <remarks>
        /// </remarks>
        public OldResponse<bool> Create(TaxonomyVocabulary taxonomyVocabularies)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    unitOfWork.GetRepository<TaxonomyVocabulary>().Add(taxonomyVocabularies);
                    if (unitOfWork.Save() >= 1)
                    {
                        return new OldResponse<bool>(1, "", true);
                    };
                    return new OldResponse<bool>(1, "", false);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex,"");
                return new OldResponse<bool>(1, ex.Message, false);
            }
        }

        private TaxonomyVocabulariesClient ConvertData(TaxonomyVocabulary data)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    TaxonomyVocabulariesClient vocaClient = new TaxonomyVocabulariesClient()
                    {
                        VocabularyId = data.VocabularyId.ToString(),
                        VocabularyTypeID = data.VocabularyTypeId.ToString(),
                        Name = data.Name,
                        Description = data.Description,
                        Weight = data.Weight,
                        CreatedByUserID = data.CreatedByUserId.ToString(),
                        CreatedOnDate = data.CreatedOnDate,
                        LastModifiedByUserID = data.LastModifiedByUserId.ToString(),
                        LastModifiedOnDate = data.LastModifiedOnDate

                    };
                   
                    return vocaClient;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex,"");
                throw;
            }
        }
        private List<TaxonomyVocabulariesClient> ConvertDatas(IEnumerable<TaxonomyVocabulary> vocabularies)
        {
            List<TaxonomyVocabulariesClient> vocaList = new List<TaxonomyVocabulariesClient>();
            foreach (TaxonomyVocabulary voca in vocabularies)
            {
                vocaList.Add(ConvertData(voca));
            }
            return vocaList;
        }
    }

    public class DbTaxonomyVocabularyHandler : ITaxonomyVocabularyHandler
    {
        private Guid? _applicationId { get; set; }
        private Guid? _userId { get; set; }

        public void init(Guid applicationId, Guid userId)
        {
            _applicationId = applicationId;
            _userId = userId;
        }

        private TaxonomyVocabularyModel ConvertData(TaxonomyVocabulary data)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    TaxonomyVocabularyModel vocaClient = new TaxonomyVocabularyModel()
                    {
                        VocabularyId = data.VocabularyId,
                        //undone: is system field
                        Code = data.Code,
                        Name = data.Name,
                        Description = data.Description,
                        CreatedByUserId = data.CreatedByUserId,
                        CreatedOnDate = data.CreatedOnDate,
                        LastModifiedByUserId = data.LastModifiedByUserId,
                        LastModifiedOnDate = data.LastModifiedOnDate
                    };
                    return vocaClient;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex,"");
                throw;
            }
        }
        private List<TaxonomyVocabularyModel> ConvertDatas(IEnumerable<TaxonomyVocabulary> vocabularies)
        {
            List<TaxonomyVocabularyModel> vocaList = new List<TaxonomyVocabularyModel>();
            foreach (TaxonomyVocabulary voca in vocabularies)
            {
                vocaList.Add(ConvertData(voca));
            }
            return vocaList;
        }

        public OldResponse<List<TaxonomyVocabularyModel>> GetAll()
        {
            try
            {
                var result = new OldResponse<List<TaxonomyVocabularyModel>>(0, string.Empty, new List<TaxonomyVocabularyModel>());
                using (var unitOfWork = new UnitOfWork())
                {
                    var fullQueryData = unitOfWork.GetRepository<TaxonomyVocabulary>().GetAll().ToList();

                    // Convert Data
                    if (fullQueryData != null && fullQueryData.Count > 0)
                    {
                        result.Message = "Lấy toàn bộ danh mục thành công!";
                        result.Status = 1;
                        result.DataCount = fullQueryData.Count;

                        foreach (var item in fullQueryData)
                        {
                            result.Data.Add(ConvertData(item));
                        }
                    }
                    else
                    {
                        result.Message = "Không tìm thấy bản ghi!";
                        result.Status = 0;
                        result.DataCount = fullQueryData.Count();
                    }
                    return result;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex,"");
                return new OldResponse<List<TaxonomyVocabularyModel>>(-1, "Error message: " + ex.Message, null);
            }
        }
        public OldResponse<List<TaxonomyVocabularyModel>> GetFilter(TaxonomyVocabularyQueryFilter filter)
        {
            try
            {
                filter.TextSearch = !string.IsNullOrEmpty(filter.TextSearch) ? filter.TextSearch.ToLower().Trim() : null;

                // Create result variable
                var result = new OldResponse<List<TaxonomyVocabularyModel>>(0, string.Empty, new List<TaxonomyVocabularyModel>());

                using (var unitOfWork = new UnitOfWork())
                {
                    var vocabularies = unitOfWork.GetRepository<TaxonomyVocabulary>().GetAll();

                    // Filter by VocabularyId
                    if (filter.VocabularyId.HasValue && filter.VocabularyId != Guid.Empty)
                    {
                        vocabularies = from dt in vocabularies where dt.VocabularyId == filter.VocabularyId select dt;
                    }

                    // Filter by Term Name
                    if (!string.IsNullOrEmpty(filter.Name))
                    {
                        vocabularies = from dt in vocabularies where dt.Name == filter.Name select dt;
                    }
                    // Filter by VocabularyCode
                    if (!string.IsNullOrEmpty(filter.Code))
                    {
                        vocabularies = from dt in vocabularies where dt.Code == filter.Code select dt;
                    }

                    // Filter by TextSearch
                    if (!string.IsNullOrEmpty(filter.TextSearch))
                    {
                        vocabularies = from dt in vocabularies where dt.Name.Contains(filter.TextSearch) || dt.Code.Contains(filter.TextSearch) || dt.Description.Contains(filter.TextSearch) select dt;
                    }

                    // Order
                    if (filter.Order == TaxonomyVocabularyQueryOrder.NGAY_TAO_DESC) vocabularies = vocabularies.OrderByDescending(sp => sp.CreatedOnDate);
                    else if (filter.Order == TaxonomyVocabularyQueryOrder.NGAY_TAO_ASC) vocabularies = vocabularies.OrderBy(sp => sp.CreatedOnDate);

                    // Calculate total count
                    var totalCount = vocabularies.Count();

                    // Pagination
                    if (filter.PageSize.HasValue && filter.PageNumber.HasValue && filter.PageSize > 0)
                    {
                        //Calculate number of rows to skip on pagesize
                        int excludedRows = (filter.PageNumber.Value - 1) * (filter.PageSize.Value - 1);
                        if (excludedRows <= 0) excludedRows = 0;

                        // Query
                        vocabularies = vocabularies.Skip(excludedRows).Take(filter.PageSize.Value);
                    }

                    /*--------------------------------------------*/
                    var fullQueryData = vocabularies.ToList();

                    // Convert Data
                    if (fullQueryData != null && fullQueryData.Count > 0)
                    {
                        result.Message = "Lấy danh sách theo bộ lọc thành công!";
                        result.Status = 1;
                        result.TotalCount = totalCount;
                        result.DataCount = fullQueryData.Count;

                        foreach (var item in fullQueryData)
                        {
                            result.Data.Add(ConvertData(item));
                        }
                    }
                    else
                    {
                        result.Message = "Không tìm thấy bản ghi!";
                        result.Status = 0;
                        result.DataCount = fullQueryData.Count();
                        result.TotalCount = totalCount;

                    }
                    return result;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex,"");
                return new OldResponse<List<TaxonomyVocabularyModel>>(-1, "Error message: " + ex.Message, null);
            }
        }
        public OldResponse<TaxonomyVocabularyModel> GetById(Guid vocabularyId)
        {
            TaxonomyVocabularyQueryFilter filter = new TaxonomyVocabularyQueryFilter();
            filter.VocabularyId = vocabularyId;
            var result = GetFilter(filter);
            if (result.Status == 1)
                return new OldResponse<TaxonomyVocabularyModel>(1, "Lấy vocabulary theo vocabulary Id thành công!", result.Data[0]);
            else return new OldResponse<TaxonomyVocabularyModel>(0, "Lấy vocabulary theo vocabulary Id không thành công!", null);
        }

        public OldResponse<List<TaxonomyVocabularyModel>> GetByType(Guid vocabularyTypeId)
        {
            TaxonomyVocabularyQueryFilter filter = new TaxonomyVocabularyQueryFilter();
            filter.VocabularyTypeId = vocabularyTypeId;
            var result = GetFilter(filter);
            if (result.Status == 1)
                return new OldResponse<List<TaxonomyVocabularyModel>>(1, "Lấy vocabulary theo vocabulary type Id thành công!", result.Data);
            else return new OldResponse<List<TaxonomyVocabularyModel>>(0, "Lấy vocabulary theo vocabulary type Id không thành công!", null);
        }

        public OldResponse<TaxonomyVocabularyModel> Create(TaxonomyVocabularyCreateRequestModel vocabulary)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    var newVocabulary = unitOfWork.GetRepository<TaxonomyVocabulary>();
                    var checkCode = newVocabulary.Get(s => s.Code == vocabulary.Code).FirstOrDefault();
                    if (checkCode != null)
                    {
                        Log.Error("Code: " + vocabulary.Code + " is exit!");
                        return new OldResponse<TaxonomyVocabularyModel>(0, "Mã danh mục: " + vocabulary.Name + " đã tồn tại!", null);
                    }
                    var vocabularyId = Guid.NewGuid();

                    var taxonomyVocabulary = new TaxonomyVocabulary()
                    {
                        VocabularyId = vocabularyId,
                        Name = vocabulary.Name,
                        Code = vocabulary.Code,
                        Description = vocabulary.Description,
                        CreatedOnDate = DateTime.Now,
                        LastModifiedOnDate = DateTime.Now,
                        CreatedByUserId = vocabulary.CreatedByUserId == Guid.Empty ? _userId.Value : vocabulary.CreatedByUserId, // Application.SuperUserID,
                        LastModifiedByUserId = vocabulary.CreatedByUserId == Guid.Empty ? _userId.Value : vocabulary.CreatedByUserId, // Application.SuperUserID,
                    };

                    newVocabulary.Add(taxonomyVocabulary);

                    if (unitOfWork.Save() >= 1)
                    {
                        //TaxonomyVocabularyCollection.Instance.LoadToHashset();
                        return new OldResponse<TaxonomyVocabularyModel>(1, "", ConvertData(taxonomyVocabulary));
                    }
                    else
                        return new OldResponse<TaxonomyVocabularyModel>(0, "Data save with error", null);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex,"");
                return new OldResponse<TaxonomyVocabularyModel>(-1, ex.Message, null);
            }

        }

        //tái sử dụng, gọi hàm nhưng chưa save --> save sau khi gọi
        public OldResponse<TaxonomyVocabularyModel> Create(TaxonomyVocabularyCreateRequestModel vocabulary, UnitOfWork unitOfWork)
        {
            try
            {
                var newVocabulary = unitOfWork.GetRepository<TaxonomyVocabulary>();
                var checkCode = newVocabulary.Get(s => s.Code == vocabulary.Code).FirstOrDefault();
                if (checkCode != null)
                {
                    Log.Error("Code: " + vocabulary.Code + " is exit!");
                    return new OldResponse<TaxonomyVocabularyModel>(0, "Mã danh mục: " + vocabulary.Name + " đã tồn tại!", null);
                }
                var vocabularyId = Guid.NewGuid();
                var taxonomyVocabulary = new TaxonomyVocabulary()
                {
                    VocabularyId = vocabularyId,
                    Name = vocabulary.Name,
                    Code = vocabulary.Code,
                    Description = vocabulary.Description,
                    CreatedOnDate = DateTime.Now,
                    LastModifiedOnDate = DateTime.Now,
                    CreatedByUserId = vocabulary.CreatedByUserId == Guid.Empty ? _userId.Value : vocabulary.CreatedByUserId, // Application.SuperUserID,
                    LastModifiedByUserId = vocabulary.CreatedByUserId == Guid.Empty ? _userId.Value : vocabulary.CreatedByUserId, // Application.SuperUserID,
                };

                newVocabulary.Add(taxonomyVocabulary);

                return new OldResponse<TaxonomyVocabularyModel>(1, "", ConvertData(taxonomyVocabulary));
            }
            catch (Exception ex)
            {
                Log.Error(ex,"");
                return new OldResponse<TaxonomyVocabularyModel>(-1, ex.Message, null);
            }
        }
        public OldResponse<TaxonomyVocabularyModel> Update(TaxonomyVocabularyUpdateRequestModel vocabulary)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    var taxonomyVocabulary = unitOfWork.GetRepository<TaxonomyVocabulary>().Get(sp => sp.VocabularyId == vocabulary.VocabularyId).FirstOrDefault();

                    taxonomyVocabulary.LastModifiedByUserId = vocabulary.LastModifiedByUserId == Guid.Empty ? vocabulary.LastModifiedByUserId : vocabulary.LastModifiedByUserId;
                    taxonomyVocabulary.LastModifiedOnDate = DateTime.Now;
                    if (vocabulary.Name != null)
                        taxonomyVocabulary.Name = vocabulary.Name;
                    if (vocabulary.Code != null)
                        taxonomyVocabulary.Code = vocabulary.Code;
                    if (vocabulary.Description != null)
                        taxonomyVocabulary.Description = vocabulary.Description;

                    unitOfWork.GetRepository<TaxonomyVocabulary>().Update(taxonomyVocabulary);

                    if (unitOfWork.Save() >= 1)
                    {
                        //Task.Run(() => TaxonomyVocabularyCollection.Instance.LoadToHashset());
                        // TaxonomyVocabularyCollection.Instance.LoadToHashset();
                        return new OldResponse<TaxonomyVocabularyModel>(1, "", ConvertData(taxonomyVocabulary));
                    }
                    else
                        return new OldResponse<TaxonomyVocabularyModel>(0, "Data save with error", null);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex,"");
                return new OldResponse<TaxonomyVocabularyModel>(-1, ex.Message, null);
            }
        }

        public OldResponse<TaxonomyVocabularyDeleteResponseModel> DeleteMany(List<TaxonomyVocabularyModel> Vocabularies)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    var deleteResponse = new OldResponse<TaxonomyVocabularyDeleteResponseModel>(0, string.Empty, new TaxonomyVocabularyDeleteResponseModel());

                    for (int i = 0; i < Vocabularies.Count; i++)
                    {
                        var cTaxonomyTermsClientRepo = unitOfWork.GetRepository<TaxonomyVocabulary>();
                        var cTaxonomyTermsClient = cTaxonomyTermsClientRepo.Find(Vocabularies[i].VocabularyId);

                        cTaxonomyTermsClientRepo.Delete(cTaxonomyTermsClient);
                    }

                    if (unitOfWork.Save() >= 1)
                    {
                        //Task.Run(() => TaxonomyVocabularyCollection.Instance.LoadToHashset());
                        // TaxonomyVocabularyCollection.Instance.LoadToHashset();
                        deleteResponse.Data = null;
                        deleteResponse.Message = "Xóa thành công";
                        deleteResponse.Status = 1;
                    }
                    else
                    {
                        deleteResponse.Data = null;
                        deleteResponse.Message = "Không tìm thấy bản ghi!";
                        deleteResponse.Status = 0;
                    }
                    return deleteResponse;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex,"");
                return new OldResponse<TaxonomyVocabularyDeleteResponseModel>(-1, "Error message: " + ex.Message, null);
            }
        }

        public OldResponse<TaxonomyVocabularyDeleteResponseModel> Delete(Guid vocabularyId)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    var deleteResponse = new OldResponse<TaxonomyVocabularyDeleteResponseModel>(0, string.Empty, new TaxonomyVocabularyDeleteResponseModel());
                    var taxonomyVocabularyRepo = unitOfWork.GetRepository<TaxonomyVocabulary>();
                    var dTaxonomyVocabulary = taxonomyVocabularyRepo.Find(vocabularyId);
                    if (dTaxonomyVocabulary != null)
                    {
                        taxonomyVocabularyRepo.Delete(dTaxonomyVocabulary);

                        if (unitOfWork.Save() >= 1)
                        {
                            //Task.Run(() => TaxonomyVocabularyCollection.Instance.LoadToHashset());
                            // TaxonomyVocabularyCollection.Instance.LoadToHashset();
                            deleteResponse.Data = new TaxonomyVocabularyDeleteResponseModel() { VocabularyId = dTaxonomyVocabulary.VocabularyId, Name = dTaxonomyVocabulary.Name, Result = 1, Message = "Xóa bản ghi thành công" };
                            deleteResponse.Status = 1;
                        }
                        else
                        {
                            deleteResponse.Data = new TaxonomyVocabularyDeleteResponseModel() { VocabularyId = dTaxonomyVocabulary.VocabularyId, Name = dTaxonomyVocabulary.Name, Result = 0, Message = "Vocabulary Anh/Chị muốn xóa chứa ràng buộc - Không xóa được" };
                            deleteResponse.Status = 0;
                        }
                    }
                    else
                    {
                        deleteResponse.Data = null;
                        deleteResponse.Message = "Không tìm thấy bản ghi!";
                        deleteResponse.Status = 0;
                    }
                    return deleteResponse;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex,"");
                return new OldResponse<TaxonomyVocabularyDeleteResponseModel>(-1, "Error message: " + ex.Message, null);
            }
        }
    }
}