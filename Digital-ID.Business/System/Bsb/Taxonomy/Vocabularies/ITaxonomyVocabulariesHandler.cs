﻿using DigitalID.Data;
using System;
using System.Collections.Generic;

namespace DigitalID.Business
{
    public interface ITaxonomyVocabulariesHandler
    {
        OldResponse<IList<TaxonomyVocabulariesClient>> Get();
        OldResponse<TaxonomyVocabulary> Get(string name);
        /// <summary>
        /// Thêm mới TaxonomyVocabularies
        /// </summary>
        /// <remarks>
        /// </remarks>
        OldResponse<bool> Create(TaxonomyVocabulary taxonomyVocabularies);
    }

    public interface ITaxonomyVocabularyHandler
    {
        //GET api/taxonomy/vocabulary_types/{vocabularyTypeId}/vocabularies
        void init(Guid applicationId, Guid userId);
        OldResponse<List<TaxonomyVocabularyModel>> GetAll();
        OldResponse<TaxonomyVocabularyModel> GetById(Guid vocabularyId);
        OldResponse<List<TaxonomyVocabularyModel>> GetFilter(TaxonomyVocabularyQueryFilter filter);
        OldResponse<List<TaxonomyVocabularyModel>> GetByType(Guid vocabularyTypeId);

        //insert, update, delete Term
        OldResponse<TaxonomyVocabularyModel> Create(TaxonomyVocabularyCreateRequestModel vocabulary);
        OldResponse<TaxonomyVocabularyModel> Update(TaxonomyVocabularyUpdateRequestModel vocabulary);
        OldResponse<TaxonomyVocabularyDeleteResponseModel> DeleteMany(List<TaxonomyVocabularyModel> vocabulary);
        OldResponse<TaxonomyVocabularyDeleteResponseModel> Delete(Guid taxonomyVocabularyId);
    }
}