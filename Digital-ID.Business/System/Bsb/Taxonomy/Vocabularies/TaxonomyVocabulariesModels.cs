﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitalID.Business
{
   public class TaxonomyVocabulariesClient
    {
        public string VocabularyId { get; set; }
        public string VocabularyTypeID { get; set; }
        public string Name { get; set; }
        public int Weight { get; set; }
        public string Description { get; set; }
        public string CreatedByUserID { get; set; }
        public Nullable<System.DateTime> CreatedOnDate { get; set; }
        public string LastModifiedByUserID { get; set; }
        public Nullable<System.DateTime> LastModifiedOnDate { get; set; }
    }


   public enum TaxonomyVocabulariesQueryOrder
   {
       NGAY_TAO_DESC,
       NGAY_TAO_ASC,
       ID_DESC,
       ID_ASC
   }
    public class TaxonomyVocabulariesQueryFilter
    {
        public int PageSize { get; set; }
        public int PageNumber { get; set; }
        public string TextSearch { get; set; }

        public TaxonomyVocabulariesQueryOrder Order { get; set; }

        public TaxonomyVocabulariesQueryFilter()
        {
            PageSize = 0;
            PageNumber = 1;
            TextSearch = string.Empty;
            Order = TaxonomyVocabulariesQueryOrder.NGAY_TAO_DESC;
        }
    }



    public class BaseTaxonomyVocabularyModel
    {
        public Guid VocabularyId { get; set; }
        public string Code { get; set; }
        //undone: public string Name { get; set; }
    }
    public class TaxonomyVocabularyModel : BaseTaxonomyVocabularyModel
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public Guid? CreatedByUserId { get; set; }
        public Nullable<System.DateTime> CreatedOnDate { get; set; }
        public Guid? LastModifiedByUserId { get; set; }
        public Nullable<System.DateTime> LastModifiedOnDate { get; set; }
    }
    // Create request model
    public class TaxonomyVocabularyCreateRequestModel
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Guid CreatedByUserId { get; set; }
        public Nullable<System.DateTime> CreatedOnDate { get; set; }
        public TaxonomyVocabularyCreateRequestModel()
        {
            CreatedOnDate = DateTime.Now;
        }
        public Guid? ApplicationId { get; set; }
    }
    // Update request model
    public class TaxonomyVocabularyUpdateRequestModel : BaseTaxonomyVocabularyModel
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public Guid LastModifiedByUserId { get; set; }
        public Nullable<System.DateTime> LastModifiedOnDate { get; set; }
    }

    // RequestModel sử dụng cho việc Delete nhiều bản ghi
    //public class TaxonomyVocabularyMultiDeleteRequestModel
    //{
    //    public List<Guid> ListVocabularyId { get; set; }
    //    public int? DeleteType { get; set; }
    //}
    //deltete response
    public class TaxonomyVocabularyDeleteResponseModel    // MANDATORY*
    {
        public Guid VocabularyId { get; set; }
        public string Name { get; set; }
        public int Result { get; set; }
        public string Message { get; set; }
    }
    public enum TaxonomyVocabularyQueryOrder
    {
        NGAY_TAO_DESC,
        NGAY_TAO_ASC,
        ID_DESC,
        ID_ASC
    }
    public class TaxonomyVocabularyQueryFilter
    {
        public int? PageSize { get; set; }
        public int? PageNumber { get; set; }
        public string TextSearch { get; set; }
        public TaxonomyVocabularyQueryOrder Order { get; set; }
        public Guid? VocabularyId { get; set; }
        public Guid? VocabularyTypeId { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public TaxonomyVocabularyQueryFilter()
        {
            PageSize = 10;
            PageNumber = 1;
            TextSearch = string.Empty;
            Order = TaxonomyVocabularyQueryOrder.NGAY_TAO_DESC;
        }
    }

}
