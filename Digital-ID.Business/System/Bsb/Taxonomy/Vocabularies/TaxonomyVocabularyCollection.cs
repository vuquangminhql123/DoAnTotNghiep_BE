﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitalID.Business
{
    public class TaxonomyVocabularyCollection
    {
        private readonly ITaxonomyVocabularyHandler _taxonomyVocabularyHandler;
        private HashSet<TaxonomyVocabularyModel> collection;

        public TaxonomyVocabularyCollection(ITaxonomyVocabularyHandler taxonomyVocabularyHandler)
        {
            _taxonomyVocabularyHandler = taxonomyVocabularyHandler;
            LoadToHashset();
        }

        public void LoadToHashset()
        {
            collection = new HashSet<TaxonomyVocabularyModel>();

            // Query to list
            var listResponse = _taxonomyVocabularyHandler.GetAll();

            // Add to hashset

            foreach (var response in listResponse.Data)
            {
                collection.Add(response);
            }
        }

        public TaxonomyVocabularyModel GetCategoryMaster()
        {
            var result = collection.FirstOrDefault(u => u.Code == "CATEGORY_MASTER");
            return result;
        }
    }
}