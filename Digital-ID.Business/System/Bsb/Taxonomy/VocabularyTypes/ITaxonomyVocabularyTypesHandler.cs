﻿using DigitalID.Data;
using System;

namespace DigitalID.Business
{
    public interface ITaxonomyVocabularyTypesHandler
    {
        /// <summary>
        /// Thêm mới TaxonomyVocabularyTypes
        /// </summary>
        /// <param name="entity">TaxonomyVocabularyTypes được thêm mới</param>        
        /// <remarks>
        /// </remarks>
        Guid CreateTaxonomyVocabularyTypes(TaxonomyVocabularyTypes taxonomyVocabularyTypes);
    }
}