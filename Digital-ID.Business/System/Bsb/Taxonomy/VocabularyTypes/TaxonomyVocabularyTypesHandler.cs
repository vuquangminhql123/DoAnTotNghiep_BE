﻿using DigitalID.Data;
using Serilog;
using System;

namespace DigitalID.Business
{
    public class TaxonomyVocabularyTypesHandler : ITaxonomyVocabularyTypesHandler
    {
        /// <summary>
        /// Thêm mới TaxonomyVocabularyTypes
        /// </summary>
        /// <param name="cTaxonomyVocabularyTypes">TaxonomyVocabularyTypes được thêm mới</param>        
        /// <remarks>
        /// </remarks>
        public Guid CreateTaxonomyVocabularyTypes(TaxonomyVocabularyTypes cTaxonomyVocabularyTypes)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    unitOfWork.GetRepository<TaxonomyVocabularyTypes>().Add(cTaxonomyVocabularyTypes);

                    if (unitOfWork.Save() >= 1)
                    {
                        return cTaxonomyVocabularyTypes.VocabularyTypeId;
                    };
                    return Guid.Empty;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex,"");
                return Guid.Empty;
            }
        }
    }
}