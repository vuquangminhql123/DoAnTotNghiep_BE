﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitalID.Business
{
    public class TaxonomyVocabularyTypesClient
    {
        public string SysID { get; set; }
        public string SysCode { get; set; }
        public string Description { get; set; }
        public string Value { get; set; }
        public DateTime CreatedOnDate { get; set; }
        public string CreatedByUserId { get; set; }
        public DateTime LastModifiedOnDate { get; set; }
        public string LastModifiedByUserId { get; set; }
    }

    public enum TaxonomyVocabularyTypesQueryOrder
    {
        NGAY_TAO_DESC,
        NGAY_TAO_ASC,
        ID_DESC,
        ID_ASC
    }

    public class TaxonomyVocabularyTypesQueryFilter
    {
        public int PageSize { get; set; }
        public int PageNumber { get; set; }
        public string TextSearch { get; set; }

        public TaxonomyVocabularyTypesQueryOrder Order { get; set; }

        public TaxonomyVocabularyTypesQueryFilter()
        {
            PageSize = 0;
            PageNumber = 1;
            TextSearch = string.Empty;
            Order = TaxonomyVocabularyTypesQueryOrder.NGAY_TAO_DESC;
        }
    }
}
