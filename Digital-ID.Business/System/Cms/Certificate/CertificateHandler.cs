﻿using DigitalID.Data;
using LinqKit;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;

namespace DigitalID.Business
{
    public class CertificateHandler : ICertificateHandler
    {
        private readonly DbHandler<CmsCert, CertificateModel, CertificateQueryModel> _dbHandler = DbHandler<CmsCert, CertificateModel, CertificateQueryModel>.Instance;

        #region CRUD
        public Task<Response> FindAsync(Guid id)
        {
            return _dbHandler.FindAsync(id);
        }
        public async Task<Response> CreateAsync(CertificateCreateUpdateModel model, Guid applicationId, Guid userId)
        {
            model.Key = "";
            CmsCert itemUpdate = AutoMapperUtils.AutoMap<CertificateCreateUpdateModel, CmsCert>(model);
            if (!string.IsNullOrWhiteSpace(model.P12Path))
            {
                itemUpdate.Cert = model.Cert;
                itemUpdate.P12Path = model.P12Path;

                #region Đọc thông tin trong P12 lưu vào DB
                string directoryCert = "";
                string fileCertName = "";
                string pathPfx = model.P12Path;
                string passPfx = model.UserPin;

                if (!File.Exists(@"" + pathPfx + ""))
                    return new ResponseError(Code.BadRequest, "File not exists");
                var fi = new FileInfo(@"" + pathPfx + "");
                directoryCert = Path.GetDirectoryName(@"" + pathPfx + "");
                fileCertName = Path.GetFileNameWithoutExtension(@"" + pathPfx + "");

                Chilkat.Cert cert = new Chilkat.Cert();

                bool success = cert.LoadPfxFile(@"" + pathPfx + "", passPfx);
                if (success == true)
                {
                    var certificate = new X509Certificate2(@"" + pathPfx + "", passPfx, X509KeyStorageFlags.MachineKeySet
                     | X509KeyStorageFlags.PersistKeySet
                     | X509KeyStorageFlags.Exportable);

                    itemUpdate.Supplier = cert.IssuerCN;
                    var privateKey = cert.ExportPrivateKey();
                    var publicKey = cert.ExportPublicKey();
                    itemUpdate.KeyLength = privateKey.BitLength.ToString();


                    var mapField = typeof(Org.BouncyCastle.Cms.CmsSignedData).Assembly.GetType("Org.BouncyCastle.Cms.CmsSignedHelper").GetField("digestAlgs", BindingFlags.Static | BindingFlags.NonPublic);
                    var map = (System.Collections.IDictionary)mapField.GetValue(null);

                    var hashAlgName = (string)map[certificate.SignatureAlgorithm.Value];// returns "SHA256" for OID of sha256 with RSA.
                    var hashAlg = HashAlgorithm.Create(hashAlgName); // your SHA256
                    itemUpdate.KeyAlgorithm = "SHA" + hashAlg.HashSize;

                    #region Cert
                    if (cert.ExportCertPemFile(Path.Combine(directoryCert, fileCertName + ".cer")))
                        itemUpdate.CertificationPath = Path.Combine(directoryCert, fileCertName + ".cer");
                    Byte[] bytes = File.ReadAllBytes(Path.Combine(directoryCert, fileCertName + ".cer"));
                    itemUpdate.Cert = Convert.ToBase64String(bytes);
                    itemUpdate.Subject = cert.SubjectDN;
                    itemUpdate.SubjectDN_CN = cert.SubjectCN;
                    itemUpdate.SubjectDN_O = cert.SubjectO;
                    itemUpdate.SubjectDN_ST = cert.SubjectS;
                    itemUpdate.SubjectDN_C = cert.SubjectC;
                    string exDate = certificate.GetExpirationDateString();
                    string effDate = certificate.GetEffectiveDateString();
                    string issueDate = cert.GetValidToDt().GetAsUnixTimeStr(true);
                    itemUpdate.ExpirationDate = DateTime.Parse(exDate);
                    itemUpdate.EffectiveDate = DateTime.Parse(effDate);
                    itemUpdate.TokenSerialNumber = certificate.GetSerialNumberString();
                    #endregion

                    #region Private key
                    Chilkat.PrivateKey privKey = null;
                    privKey = cert.ExportPrivateKey();
                    if (cert.LastMethodSuccess == false)
                    {
                        return new ResponseError(Code.BadRequest, "Not found private key");
                    }
                    if (privKey.SaveRsaPemFile(Path.Combine(directoryCert, fileCertName + "_private.pem")))
                    {
                        itemUpdate.PrivateKey = Convert.ToBase64String(File.ReadAllBytes(Path.Combine(directoryCert, fileCertName + "_private.pem")));
                    }
                    #endregion

                    #region Public key
                    Chilkat.PublicKey pubKey = null;
                    pubKey = cert.ExportPublicKey();
                    if (cert.LastMethodSuccess == false)
                    {
                        return new ResponseError(Code.BadRequest, "Not found public key");
                    }
                    if (pubKey.SavePemFile(true, Path.Combine(directoryCert, fileCertName + "_public.pem")))
                    {
                        itemUpdate.PublicKey = Convert.ToBase64String(File.ReadAllBytes(Path.Combine(directoryCert, fileCertName + "_public.pem")));
                    }
                    #endregion
                }
                else
                {
                    return new ResponseError(Code.BadRequest, "Not Found Cert");
                }
                #endregion
            }
            itemUpdate = itemUpdate.InitCreate(applicationId, userId);
            itemUpdate.Id = Guid.NewGuid();
            var result = await _dbHandler.CreateAsync(itemUpdate);
            return result;
        }

        public async Task<ResponseV2<int>> UpdateAsync(Guid id, CertificateCreateUpdateModel model, Guid applicationId, Guid userId)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    var itemUpdate = unitOfWork.GetRepository<CmsCert>().Find(id);
                    itemUpdate.Alias = model.Alias;
                    //itemUpdate.SlotID = model.SlotID;
                    itemUpdate.CertificateFileName = model.CertificateFileName;
                    itemUpdate.UserPin = model.UserPin;
                    itemUpdate.SlotLabel = model.SlotLabel;
                    if (!string.IsNullOrWhiteSpace(model.P12Path))
                    {
                        itemUpdate.Cert = model.Cert;
                        itemUpdate.P12Path = model.P12Path;

                        #region Đọc thông tin trong P12 lưu vào DB
                        string directoryCert = "";
                        string fileCertName = "";
                        string pathPfx = model.P12Path;
                        string passPfx = model.UserPin;

                        if (!File.Exists(@"" + pathPfx + ""))
                            return new ResponseV2<int>(-1, "File not exists", -1);
                        var fi = new FileInfo(@"" + pathPfx + "");
                        directoryCert = Path.GetDirectoryName(@"" + pathPfx + "");
                        fileCertName = Path.GetFileNameWithoutExtension(@"" + pathPfx + "");

                        Chilkat.Cert cert = new Chilkat.Cert();

                        var certificate = new X509Certificate2(@"" + pathPfx + "", passPfx, X509KeyStorageFlags.MachineKeySet
                             | X509KeyStorageFlags.PersistKeySet
                             | X509KeyStorageFlags.Exportable);
                        if(certificate == null)
                            return new ResponseV2<int>(0, "Wrong P12 Password", -1);
                        bool success = cert.LoadPfxFile(@"" + pathPfx + "", passPfx);
                        if (success == true)
                        {
                            itemUpdate.Supplier = cert.IssuerCN;
                            var privateKey = cert.ExportPrivateKey();
                            var publicKey = cert.ExportPublicKey();
                            itemUpdate.KeyLength = privateKey.BitLength.ToString();


                            var mapField = typeof(Org.BouncyCastle.Cms.CmsSignedData).Assembly.GetType("Org.BouncyCastle.Cms.CmsSignedHelper").GetField("digestAlgs", BindingFlags.Static | BindingFlags.NonPublic);
                            var map = (System.Collections.IDictionary)mapField.GetValue(null);

                            var hashAlgName = (string)map[certificate.SignatureAlgorithm.Value];// returns "SHA256" for OID of sha256 with RSA.
                            var hashAlg = HashAlgorithm.Create(hashAlgName); // your SHA256
                            itemUpdate.KeyAlgorithm = "SHA" + hashAlg.HashSize;

                            #region Cert
                            if (cert.ExportCertPemFile(Path.Combine(directoryCert, fileCertName + ".cer")))
                                itemUpdate.CertificationPath = Path.Combine(directoryCert, fileCertName + ".cer");
                            Byte[] bytes = File.ReadAllBytes(Path.Combine(directoryCert, fileCertName + ".cer"));
                            itemUpdate.Cert = Convert.ToBase64String(bytes);
                            itemUpdate.Subject = cert.SubjectDN;
                            itemUpdate.SubjectDN_CN = cert.SubjectCN;
                            itemUpdate.SubjectDN_O = cert.SubjectO;
                            itemUpdate.SubjectDN_ST = cert.SubjectS;
                            itemUpdate.SubjectDN_C = cert.SubjectC;
                            string exDate = certificate.GetExpirationDateString();
                            string effDate = certificate.GetEffectiveDateString();
                            itemUpdate.ExpirationDate = DateTime.Parse(exDate);
                            itemUpdate.EffectiveDate = DateTime.Parse(effDate);
                            itemUpdate.TokenSerialNumber = certificate.GetSerialNumberString();
                            #endregion

                            #region Private key
                            Chilkat.PrivateKey privKey = null;
                            privKey = cert.ExportPrivateKey();
                            if (cert.LastMethodSuccess == false)
                            {
                                return new ResponseV2<int>(0, "Not found private key", -1);
                            }
                            if (privKey.SaveRsaPemFile(Path.Combine(directoryCert, fileCertName + "_private.pem")))
                            {
                                itemUpdate.PrivateKey = Convert.ToBase64String(File.ReadAllBytes(Path.Combine(directoryCert, fileCertName + "_private.pem")));
                            }
                            #endregion

                            #region Public key
                            Chilkat.PublicKey pubKey = null;
                            pubKey = cert.ExportPublicKey();
                            if (cert.LastMethodSuccess == false)
                            {
                                return new ResponseV2<int>(0, "Not found public key", -1);
                            }
                            if (pubKey.SavePemFile(true, Path.Combine(directoryCert, fileCertName + "_public.pem")))
                            {
                                itemUpdate.PublicKey = Convert.ToBase64String(File.ReadAllBytes(Path.Combine(directoryCert, fileCertName + "_public.pem")));
                            }
                            #endregion
                        }
                        else
                        {
                            return new ResponseV2<int>(0, "Not found Cert", -1);
                        }
                        #endregion
                    }
                    unitOfWork.GetRepository<CmsCert>().Update(itemUpdate);
                    var result = await unitOfWork.SaveAsync();
                    if (result == 1)
                    {
                        return new ResponseV2<int>(1, "Success", 1);
                    }
                    else
                    {
                        return new ResponseV2<int>(-1, "Something wrong", 0);
                    }
                }
            }
            catch (Exception ex)
            {
                return new ResponseV2<int>(-1, "Lỗi: " + ex.Message, 0);
            }
        }
        public async Task<Response> DeleteAsync(Guid id)
        {
            var result = await _dbHandler.DeleteAsync(id);
            return result;
        }
        public async Task<Response> DeleteRangeAsync(IList<Guid> listId)
        {
            var result = await _dbHandler.DeleteRangeAsync(listId, "Id");
            return result;
        }
        #endregion
        public Response GetAll()
        {
            return _dbHandler.GetAll();
        }
        public async Task<Response> GetAllAsync()
        {
            var predicate = PredicateBuilder.New<CmsCert>(true);
            return await _dbHandler.GetAllAsync(predicate);
        }
        public Task<Response> GetAllAsync(CertificateQueryModel query)
        {
            var predicate = BuildQuery(query);
            return _dbHandler.GetAllAsync(predicate, query.Sort);
        }
        public Task<Response> GetPageAsync(CertificateQueryModel query)
        {
            var predicate = BuildQuery(query);
            return _dbHandler.GetPageAsync(predicate, query);
        }
        private Expression<Func<CmsCert, bool>> BuildQuery(CertificateQueryModel query)
        {
            var predicate = PredicateBuilder.New<CmsCert>(true);

            if (!string.IsNullOrEmpty(query.SlotLabel))
            {
                predicate.And(s => s.SlotLabel == query.SlotLabel);
            }
            if (!string.IsNullOrEmpty(query.UserPin))
            {
                predicate.And(s => s.UserPin == query.UserPin);
            }
            if (!string.IsNullOrEmpty(query.Alias))
            {
                predicate.And(s => s.Alias == query.Alias);
            }
            if (!string.IsNullOrEmpty(query.CertificateFileName))
            {
                predicate.And(s => s.CertificateFileName == query.CertificateFileName);
            }
            if (query.ListId != null && query.ListId.Count > 0)
            {
                predicate.And(s => query.ListId.Contains(s.Id));
            }
            if (!string.IsNullOrEmpty(query.FullTextSearch))
            {
                predicate.And(s =>
                    s.CertificateFileName.ToLower().Contains(query.FullTextSearch.ToLower())
                    || s.Alias.ToLower().Contains(query.FullTextSearch.ToLower())
                    || s.UserPin.ToLower().Contains(query.FullTextSearch.ToLower())
                    || s.SlotLabel.ToLower().Contains(query.FullTextSearch.ToLower())
                );
            }

            return predicate;
        }
        /// <summary>
        /// Lấy tất cả chứng thư số mà người dùng đang sở hữu 
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public OldResponse<List<UserCertificateModel>> GetAllCertificateOfUser(Guid userId)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    var datas = (from cu in unitOfWork.GetRepository<CmsCertUser>().GetAll()
                                 join c in unitOfWork.GetRepository<CmsCert>().GetAll()
                                     on cu.CertId equals c.Id
                                 where cu.UserId == userId
                                 select new
                                 {
                                     CertId = c.Id,
                                     CertificateFileName = c.CertificateFileName
                                 }).ToList();

                    if (datas.Count > 0)
                    {
                        var list = new List<UserCertificateModel>();

                        foreach (var cmsCert in datas)
                        {
                            list.Add(new UserCertificateModel()
                            {
                                CertId = cmsCert.CertId,
                                CertificateFileName = cmsCert.CertificateFileName
                            });
                        }
                        return new OldResponse<List<UserCertificateModel>>(1, "Success", list);
                    }
                    else
                    {
                        return new OldResponse<List<UserCertificateModel>>(0, "Không có chứng thư số", null);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OldResponse<List<UserCertificateModel>>(-1, ex.Message, null);
            }
        }

        /// <summary>
        /// Thay đổi key của chứng thư số
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="certId"></param>
        /// <param name="newKey"></param>
        /// <param name="currentKey"></param>
        /// <returns></returns>
        public OldResponse ChangeKeyOfCertificate(Guid userId, Guid certId, string newKey, string currentKey)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    var data = (from cu in unitOfWork.GetRepository<CmsCertUser>().GetAll()
                                where cu.CertId == certId && cu.UserId == userId
                                select cu).FirstOrDefault();

                    if (data == null)
                    {
                        return new OldResponse(-1, "Không có chứng thư số");
                    }

                    if (Utils.MD5Hash(currentKey) != data.Key)
                    {
                        return new OldResponse(0, "Không trùng key cũ");
                    }

                    data.Key = Utils.MD5Hash(newKey);

                    unitOfWork.GetRepository<CmsCertUser>().Update(data);

                    var result = unitOfWork.Save();
                    if (result >= 1)
                    {
                        return new OldResponse(1, "Cập nhật thành công");
                    }
                    else
                    {
                        return new OldResponse(0, "Có lỗi xảy ra");
                    }
                }
            }
            catch (Exception ex)
            {
                return new OldResponse(-1, ex.Message);
            }
        }

        /// <summary>
        /// Lấy tất cả chứng thư số trong kho (khi cấp chứng thư số)    
        /// </summary>
        /// <returns></returns>
        public ResponseV2<List<UserCertificateModel>> GetAllCertForUserByAdmin()
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    var datas = (from c in unitOfWork.GetRepository<CmsCert>().GetAll()
                                 select c).ToList();
                    if (datas.Count > 0)
                    {
                        var result = new List<UserCertificateModel>();
                        foreach (var cmsCert in datas)
                        {
                            result.Add(new UserCertificateModel()
                            {
                                CertId = cmsCert.Id,
                                CertificateFileName = cmsCert.CertificateFileName
                            });
                        }

                        return new ResponseV2<List<UserCertificateModel>>(1, "Success", result);

                    }

                    return new ResponseV2<List<UserCertificateModel>>(0, "Không có chứng thư số", null);
                }
            }
            catch (Exception ex)
            {
                return new ResponseV2<List<UserCertificateModel>>(-1, "Lỗi: " + ex.Message, null);
            }
        }

        /// <summary>
        /// Lấy tất cả chứng thư số của người dùng (trong trường hợp admin cấp chứng thư số cho user)
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public ResponseV2<List<UserCertificateModel>> GetUserCerts(Guid userId)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    var datas = (from cu in unitOfWork.GetRepository<CmsCertUser>().GetAll()
                                 join c in unitOfWork.GetRepository<CmsCert>().GetAll()
                                     on cu.CertId equals c.Id
                                 where cu.UserId == userId
                                 select c).ToList();
                    if (datas.Count > 0)
                    {
                        var result = new List<UserCertificateModel>();
                        foreach (var cmsCert in datas)
                        {
                            result.Add(new UserCertificateModel()
                            {
                                CertId = cmsCert.Id,
                                CertificateFileName = cmsCert.CertificateFileName
                            });
                        }

                        return new ResponseV2<List<UserCertificateModel>>(1, "Success", result);

                    }

                    return new ResponseV2<List<UserCertificateModel>>(0, "Không có chứng thư số", null);
                }
            }
            catch (Exception ex)
            {
                return new ResponseV2<List<UserCertificateModel>>(-1, "Lỗi: " + ex.Message, null);
            }
        }

        /// <summary>
        /// Trả về cert dạng base64
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        public ResponseV2<CertificateClientAPIModel> GetCertBase64ForUser(string userName)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    var cert = (from u in unitOfWork.GetRepository<IdmUser>().GetAll()
                                join uc in unitOfWork.GetRepository<CmsCertUser>().GetAll()
                                on u.Id equals uc.UserId
                                join c in unitOfWork.GetRepository<CmsCert>().GetAll()
                                on uc.CertId equals c.Id
                                where u.UserName == userName
                                select new CertificateClientAPIModel()
                                {
                                    Cert = c.Cert,
                                    CMT = "",
                                    CN = c.SubjectDN_CN,
                                    HashAlgorithm = c.KeyAlgorithm,
                                    Issuer = c.Supplier,
                                    SerialNumber = c.TokenSerialNumber,
                                    ValidFrom = c.EffectiveDate,
                                    ValidTo = c.ExpirationDate
                                }).FirstOrDefault();
                    if (cert == null)
                        return new ResponseV2<CertificateClientAPIModel>(0, "Not found", null);
                    else
                        return new ResponseV2<CertificateClientAPIModel>(1, "Success", cert);
                }
            }
            catch (Exception ex)
            {
                return new ResponseV2<CertificateClientAPIModel>(-1, ex.Message, null);
            }
        }
    }
}
