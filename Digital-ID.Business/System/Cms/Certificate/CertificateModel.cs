﻿using DigitalID.Data;
using Newtonsoft.Json;
using System;

namespace DigitalID.Business
{
    public class BaseCertificateModel : BaseModel
    {
        public Guid Id { get; set; }
        public new Guid? CreatedByUserId { get; set; }
        public new Guid? LastModifiedByUserId { get; set; }
        public new DateTime? LastModifiedOnDate { get; set; }
        public new DateTime? CreatedOnDate { get; set; }
        public new Guid ApplicationId { get; set; }
        [JsonProperty("supplier")]
        public string Supplier { get; set; }

        //Slot label
        [JsonProperty("slot_id")]
        public int SlotID { get; set; }

        //PIN
        [JsonProperty("user_pin")]
        public string UserPin { get; set; }

        [JsonProperty("key")]
        public string Key { get; set; }

        [JsonProperty("alias")]
        public string Alias { get; set; }

        [JsonProperty("slot_label")]
        public string SlotLabel { get; set; }

        //Độ dài khóa
        [JsonProperty("key_length")]
        public string KeyLength { get; set; }

        //Thuật toán
        [JsonProperty("key_algorithm")]
        public string KeyAlgorithm { get; set; }

        [JsonProperty("certificate_file_name")]
        public string CertificateFileName { get; set; }

        //Loại chứng thư số
        [JsonProperty("cert_type")]
        public string CertType { get; set; }

        //CMT/MST
        [JsonProperty("subject_dn_cmt_mst")]
        public string SubjectDN_CMT_MST { get; set; }

        [JsonProperty("subject_dn_cn")]
        public string SubjectDN_CN { get; set; }

        [JsonProperty("subject_dn_o")]
        public string SubjectDN_O { get; set; }

        [JsonProperty("subject_dn_st")]
        public string SubjectDN_ST { get; set; }

        [JsonProperty("subject_dn_c")]
        public string SubjectDN_C { get; set; }

        [JsonProperty("csr")]
        public string Csr { get; set; }

        [JsonProperty("cert")]
        public string Cert { get; set; }

        //res_partner	uniqueidentifier Bổ sung: Liên kết với cty cung cấp (FK)
        [JsonProperty("res_partner")]
        public Guid? ResPartnerId { get; set; }

        //Bổ sung: đường dẫn trên Thư mục lưu p12	p12_path	nvarchar
        [JsonProperty("p12_path")]
        public string P12Path { get; set; }

        //bổ sung	certification_path	nvarchar
        [JsonProperty("certification_path")]
        public string CertificationPath { get; set; }

        //bổ sung	certification_hash	nvarchar
        [JsonProperty("certification_hash")]
        public string CertificationHash { get; set; }

        //Bổ sung	subject	nvarchar	
        [JsonProperty("subject")]
        public string Subject { get; set; }

        //Bổ sung: Trạng thái	certification_status	int
        [JsonProperty("certification_status")]
        public int CertificationStatus { get; set; }

        //Bổ sung: 	push_notice_enabled	bit
        [JsonProperty("push_notice_enabled")]
        public bool PushNoticeEnabled { get; set; }

        //Bổ sung: 	certification_profile_id	uniqueidentifier
        [JsonProperty("certification_profile_id")]
        public Guid? CertificationProfileId { get; set; }

        //Bổ sung: 	token_serial_number	nvarchar
        [JsonProperty("token_serial_number")]
        public string TokenSerialNumber { get; set; }

        //Bổ sung: 	effective_date	datetime2
        [JsonProperty("effective_date")]
        public DateTime? EffectiveDate { get; set; }

        //Bổ sung: 	expiration_date	
        [JsonProperty("expiration_date")]
        public DateTime? ExpirationDate { get; set; }

        //Bổ sung: 	expiration_contract_date	datetime2
        [JsonProperty("expiration_contract_date")]
        public DateTime? ExpirationContractDate { get; set; }

        //Bổ sung: 	duration	int
        [JsonProperty("duration")]
        public int Duration { get; set; }

        //Bổ sung: 	private_key	nvarchar
        [JsonProperty("private_key")]
        public string PrivateKey { get; set; }

        //Bổ sung: 	private_key_enable	int
        [JsonProperty("private_key_enable")]
        public int PrivateKeyEnable { get; set; }

        //Bổ sung: 	public_key	nvarchar
        [JsonProperty("public_key")]
        public string PublicKey { get; set; }

        //Bổ sung: 	public_key_hash	nvarchar
        [JsonProperty("public_key_hash")]
        public string PublicKeyHash { get; set; }

        //Bổ sung: 	activation_code	nvarchar
        [JsonProperty("activation_code")]
        public string ActivationCode { get; set; }

        //Bổ sung: 	revoke_date	datetime2
        [JsonProperty("revoke_date")]
        public DateTime? RevokeDate { get; set; }

        //Bổ sung: 	operation_date	datetime2
        [JsonProperty("operation_date")]
        public DateTime? OperationDate { get; set; }

        //bổ sung	hmac	text
        [JsonProperty("hmac")]
        public string Hmac { get; set; }

    }
    public class CertificateModel : BaseCertificateModel
    {
    }

    public class CertificateQueryModel : PaginationRequest
    {
        public int SlotID { get; set; }
        public string UserPin { get; set; }
        public string Alias { get; set; }
        public string CertificateFileName { get; set; }
        public string SlotLabel { get; set; }
        public string Cert { get; set; }
    }

    public class CertificateCreateUpdateModel
    {
        [JsonProperty("supplier")]
        public string Supplier { get; set; }

        //Slot label
        [JsonProperty("slot_id")]
        public int? SlotID { get; set; }

        //PIN
        [JsonProperty("user_pin")]
        public string UserPin { get; set; }

        [JsonProperty("key")]
        public string Key { get; set; }

        [JsonProperty("alias")]
        public string Alias { get; set; }

        [JsonProperty("slot_label")]
        public string SlotLabel { get; set; }

        //Độ dài khóa
        [JsonProperty("key_length")]
        public string KeyLength { get; set; }

        //Thuật toán
        [JsonProperty("key_algorithm")]
        public string KeyAlgorithm { get; set; }

        [JsonProperty("certificate_file_name")]
        public string CertificateFileName { get; set; }

        //Loại chứng thư số
        [JsonProperty("cert_type")]
        public string CertType { get; set; }

        //CMT/MST
        [JsonProperty("subject_dn_cmt_mst")]
        public string SubjectDN_CMT_MST { get; set; }

        [JsonProperty("subject_dn_cn")]
        public string SubjectDN_CN { get; set; }

        [JsonProperty("subject_dn_o")]
        public string SubjectDN_O { get; set; }

        [JsonProperty("subject_dn_st")]
        public string SubjectDN_ST { get; set; }

        [JsonProperty("subject_dn_c")]
        public string SubjectDN_C { get; set; }

        [JsonProperty("csr")]
        public string Csr { get; set; }

        [JsonProperty("cert")]
        public string Cert { get; set; }

        //res_partner	uniqueidentifier Bổ sung: Liên kết với cty cung cấp (FK)
        [JsonProperty("res_partner")]
        public Guid? ResPartnerId { get; set; }

        //Bổ sung: đường dẫn trên Thư mục lưu p12	p12_path	nvarchar
        [JsonProperty("p12_path")]
        public string P12Path { get; set; }

        //bổ sung	certification_path	nvarchar
        [JsonProperty("certification_path")]
        public string CertificationPath { get; set; }

        //bổ sung	certification_hash	nvarchar
        [JsonProperty("certification_hash")]
        public string CertificationHash { get; set; }

        //Bổ sung	subject	nvarchar	
        [JsonProperty("subject")]
        public string Subject { get; set; }

        //Bổ sung: Trạng thái	certification_status	int
        [JsonProperty("certification_status")]
        public int? CertificationStatus { get; set; }

        //Bổ sung: 	push_notice_enabled	bit
        [JsonProperty("push_notice_enabled")]
        public bool? PushNoticeEnabled { get; set; }

        //Bổ sung: 	certification_profile_id	uniqueidentifier
        [JsonProperty("certification_profile_id")]
        public Guid? CertificationProfileId { get; set; }

        //Bổ sung: 	token_serial_number	nvarchar
        [JsonProperty("token_serial_number")]
        public string TokenSerialNumber { get; set; }

        //Bổ sung: 	effective_date	datetime2
        [JsonProperty("effective_date")]
        public DateTime? EffectiveDate { get; set; }

        //Bổ sung: 	expiration_date	
        [JsonProperty("expiration_date")]
        public DateTime? ExpirationDate { get; set; }

        //Bổ sung: 	expiration_contract_date	datetime2
        [JsonProperty("expiration_contract_date")]
        public DateTime? ExpirationContractDate { get; set; }

        //Bổ sung: 	duration	int
        [JsonProperty("duration")]
        public int? Duration { get; set; }

        //Bổ sung: 	private_key	nvarchar
        [JsonProperty("private_key")]
        public string PrivateKey { get; set; }

        //Bổ sung: 	private_key_enable	int
        [JsonProperty("private_key_enable")]
        public int? PrivateKeyEnable { get; set; }

        //Bổ sung: 	public_key	nvarchar
        [JsonProperty("public_key")]
        public string PublicKey { get; set; }

        //Bổ sung: 	public_key_hash	nvarchar
        [JsonProperty("public_key_hash")]
        public string PublicKeyHash { get; set; }

        //Bổ sung: 	activation_code	nvarchar
        [JsonProperty("activation_code")]
        public string ActivationCode { get; set; }

        //Bổ sung: 	revoke_date	datetime2
        [JsonProperty("revoke_date")]
        public DateTime? RevokeDate { get; set; }

        //Bổ sung: 	operation_date	datetime2
        [JsonProperty("operation_date")]
        public DateTime? OperationDate { get; set; }

        //bổ sung	hmac	text
        [JsonProperty("hmac")]
        public string Hmac { get; set; }
    }
    
    public class UserCertificateModel
    {
        public Guid CertId { get; set; }
        public string CertificateFileName { get; set; }
        public string Key { get; set; }
    }

    public class CertificateClientModel
    {
        public string Key { get; set; }
        public string Alias { get; set; }
        public string CertificateFileName { get; set; }
    }

    public class CertificateClientAPIModel
    {
        [JsonProperty("serial_number")]
        public string SerialNumber { get; set; }
        [JsonProperty("hash_algorithm")]
        public string HashAlgorithm { get; set; }
        [JsonProperty("issuer")]
        public string Issuer { get; set; }
        [JsonProperty("valid_from")]
        public DateTime? ValidFrom { get; set; }
        [JsonProperty("valid_to")]
        public DateTime? ValidTo { get; set; }
        [JsonProperty("cn")]
        public string CN { get; set; }
        [JsonProperty("cmt_cccd_hc")]
        public string CMT { get; set; }
        [JsonProperty("cert")]
        public string Cert { get; set; }
    }

}
