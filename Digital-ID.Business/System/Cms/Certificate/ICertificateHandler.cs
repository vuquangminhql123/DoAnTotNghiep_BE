﻿using DigitalID.Data;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DigitalID.Business
{
    public interface ICertificateHandler
    {
        Task<Response> FindAsync(Guid id);
        Task<Response> CreateAsync(CertificateCreateUpdateModel model, Guid applicationId, Guid userId);
        Task<ResponseV2<int>> UpdateAsync(Guid id, CertificateCreateUpdateModel model, Guid applicationId, Guid userId);
        Task<Response> DeleteAsync(Guid id);
        Task<Response> DeleteRangeAsync(IList<Guid> listId);
        Task<Response> GetAllAsync();
        Task<Response> GetAllAsync(CertificateQueryModel query);
        Task<Response> GetPageAsync(CertificateQueryModel query);
        Response GetAll();
        OldResponse<List<UserCertificateModel>> GetAllCertificateOfUser(Guid actorId);
        OldResponse ChangeKeyOfCertificate(Guid userId, Guid certId, string newKey, string currentKey);
        ResponseV2<List<UserCertificateModel>> GetAllCertForUserByAdmin();
        ResponseV2<List<UserCertificateModel>> GetUserCerts(Guid userId);
        ResponseV2<CertificateClientAPIModel> GetCertBase64ForUser(string userName);
    }
}
