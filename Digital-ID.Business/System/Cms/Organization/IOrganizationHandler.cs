﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DigitalID.Data;

namespace DigitalID.Business
{
    public interface IOrganizationHandler
    {
        Task<Response> FindAsync(Guid id);
        Task<Response> CreateAsync(OrganizationCreateModel model, Guid applicationId, Guid userId);
        Task<Response> UpdateAsync(Guid id, OrganizationUpdateModel model, Guid applicationId, Guid userId);
        Task<Response> DeleteAsync(Guid id);
        Task<Response> DeleteRangeAsync(IList<Guid> listId);
        Task<Response> GetAllAsync();
        Task<Response> GetAllAsync(OrganizationQueryModel query);
        Task<Response> GetAllTreeAsync(OrganizationQueryModel query);
        Task<Response> GetPageAsync(OrganizationQueryModel query);
        Response GetAll();
    }
}