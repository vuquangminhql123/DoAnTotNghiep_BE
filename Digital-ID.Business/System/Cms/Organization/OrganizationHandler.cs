﻿using LinqKit;
using DigitalID.Data;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Serilog;
using System.Linq;
using System.Data.SqlClient;

namespace DigitalID.Business
{
    public class OrganizationHandler : IOrganizationHandler
    {
        private readonly DbHandler<CmsOrganization, OrganizationModel, OrganizationQueryModel> _dbHandler = DbHandler<CmsOrganization, OrganizationModel, OrganizationQueryModel>.Instance;

        private async Task<Response> CheckRelationShip(Guid id)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {

                    var checkRelationShip = await unitOfWork.GetRepository<CmsOrganization>().FindAsync(x => x.ParentId == id);
                    if (checkRelationShip != null)
                    {
                        return new ResponseError(Code.BadRequest, string.Format("Không thể xóa, đơn vị phòng ban: \"{0}\" có đơn vị con",
                        checkRelationShip.Name), null);
                    }
                    return new Response(Code.Success, "");
                }
            }
            catch (Exception)
            {
                return new ResponseError(Code.BadRequest, "Đơn vị phòng bạn có đơn vị con", null);
            }
        }
        private async Task<Response> CheckRelationShip(IList<Guid> listId)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {

                    var checkRelationShip = await unitOfWork.GetRepository<CmsOrganization>().GetListAsync(x => listId.Contains(x.ParentId.Value));
                    if (checkRelationShip != null && checkRelationShip.Count > 0)
                    {
                        return new ResponseError(Code.BadRequest, string.Format("Không thể xóa, đơn vị phòng ban: \"{0}\" có đơn vị con",
                        string.Join(", ", checkRelationShip.Select(x => x.Name).ToList())), null);
                    }
                    return new Response(Code.Success, "");
                }
            }
            catch (Exception)
            {
                return new ResponseError(Code.BadRequest, "Đơn vị phòng bạn có đơn vị con", null);
            }
        }
        #region CRUD
        public Task<Response> FindAsync(Guid id)
        {
            return _dbHandler.FindAsync(id);
        }
        public async Task<Response> CreateAsync(OrganizationCreateModel model, Guid applicationId, Guid userId)
        {

            CmsOrganization request = AutoMapperUtils.AutoMap<OrganizationCreateModel, CmsOrganization>(model);
            request = request.InitCreate(applicationId, userId);
            request.Id = Guid.NewGuid();
            request.IdPath = request.IdPath + "/" + request.Id;
            var result = await _dbHandler.CreateAsync(request, "Code");
            return result;
        }
        public async Task<Response> UpdateAsync(Guid id, OrganizationUpdateModel model, Guid applicationId, Guid userId)
        {
            CmsOrganization request = AutoMapperUtils.AutoMap<OrganizationUpdateModel, CmsOrganization>(model);
            request = request.InitUpdate(applicationId, userId);
            request.Id = id;
            var result = await _dbHandler.UpdateAsync(id, request, "Code");
            await UpdateParentName(request.Id, request.Name);
            return result;
        }
        public async Task<Response> DeleteAsync(Guid id)
        {
            var response = await CheckRelationShip(id);
            if (response.Code!= Code.Success)
            {
                return response;
            }
            var result = await _dbHandler.DeleteAsync(id);
            return result;
        }
        public async Task<Response> DeleteRangeAsync(IList<Guid> listId)
        {
            var response = await CheckRelationShip(listId);
            if (response.Code!= Code.Success)
            {
                return response;
            }
            var result = await _dbHandler.DeleteRangeAsync(listId, "Id");
            return result;
        }
        #endregion
        public Response GetAll()
        {
            return _dbHandler.GetAll();
        }
        public async Task<Response> GetAllAsync()
        {
            var predicate = PredicateBuilder.New<CmsOrganization>(true);
            return await _dbHandler.GetAllAsync(predicate);
        }

        public async Task<Response> GetAllTreeAsync(OrganizationQueryModel query)
        {
            var predicate = BuildQuery(query);
            var result = await _dbHandler.GetAllAsync(predicate);
            if (result.Code == Code.Success && result is ResponseList<OrganizationModel> resultData)
            {

                var treeResult = ConvertToTreeData(resultData.Data.Select(x => new OrganizationTreeModel()
                {
                    Id = x.Id,
                    Name = x.Name,
                    ParentId = x.ParentId,
                    Source = x
                }).ToList());
                return new ResponseList<OrganizationTreeModel>(treeResult);
            }
            return result;
        }

        public Task<Response> GetAllAsync(OrganizationQueryModel query)
        {
            var predicate = BuildQuery(query);
            return _dbHandler.GetAllAsync(predicate, query.Sort);
        }
        public Task<Response> GetPageAsync(OrganizationQueryModel query)
        {
            var predicate = BuildQuery(query);
            return _dbHandler.GetPageAsync(predicate, query);
        }
        private Expression<Func<CmsOrganization, bool>> BuildQuery(OrganizationQueryModel query)
        {
            var predicate = PredicateBuilder.New<CmsOrganization>(true);

            if (!string.IsNullOrEmpty(query.Code))
            {
                predicate.And(s => s.Code == query.Code);
            }


            if (query.IdInPath.HasValue)
            {
                var compareStr = query.IdInPath.Value.ToString().ToLower();
                predicate.And(s => !s.IdPath.Contains(compareStr));
            }



            if (query.ParentId.HasValue)
            {
                predicate.And(s => s.ParentId == query.ParentId.Value);
            }

            if (query.Type.HasValue)
            {
                predicate.And(s => s.Type == query.Type.Value);
            }

            if (query.ListId != null && query.ListId.Count > 0)
            {
                predicate.And(s => query.ListId.Contains(s.Id));
            }
            if (!string.IsNullOrEmpty(query.FullTextSearch))
            {
                predicate.And(s =>
                s.Name.ToLower().Contains(query.FullTextSearch.ToLower())
                || s.Code.ToLower().Contains(query.FullTextSearch.ToLower())
                || s.Description.ToLower().Contains(query.FullTextSearch.ToLower())
                );
            }
            return predicate;
        }

        //    ----
        private async Task<int> UpdateParentName(Guid id, string parentName)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    return await unitOfWork.GetRepository<CmsOrganization>().ExecuteSqlCommandAsync("UPDATE cms_Organization SET ParentName = {0} WHERE ParentId = {1}", new object[] { parentName, id });
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return 0;
            }
        }

        private OrganizationTreeModel BuildTree(OrganizationTreeModel root, List<OrganizationTreeModel> nodes)
        {
            if (nodes.Count == 0) { return root; }

            var children = FetchChildren(root, nodes).ToList();
            root.SubChild.AddRange(children);
            root = RemoveChildren(root, nodes);

            for (int i = 0; i < children.Count; i++)
            {
                children[i] = BuildTree(children[i], nodes);
                if (nodes.Count == 0) { break; }
            }
            return root;
        }

        private IEnumerable<OrganizationTreeModel> FetchChildren(OrganizationTreeModel root, List<OrganizationTreeModel> nodes)
        {
            return nodes.Where(n => n.ParentId == root.Id);
        }

        private OrganizationTreeModel RemoveChildren(OrganizationTreeModel root, List<OrganizationTreeModel> nodes)
        {
            foreach (var node in root.SubChild)
            {
                nodes.Remove(node);
            }
            return root;
        }

        private List<OrganizationTreeModel> ConvertToTreeData(List<OrganizationTreeModel> current)
        {
            var result = new List<OrganizationTreeModel>();
            var listRoot = current.Where(x => !x.ParentId.HasValue);
            if (!listRoot.Any())
            {
                return result;
            }

            foreach (var item in listRoot.ToList())
            {
                result.Add(BuildTree(item, current));
            }
            return result;
        }
    }
}
