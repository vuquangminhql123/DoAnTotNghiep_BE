﻿using DigitalID.Data;
using System;
using System.Collections.Generic;

namespace DigitalID.Business
{
    public class BaseOrganizationModel : BaseModel
    {
        public Guid Id { get; set; }
        public Guid? ParentId { get; set; }
        public string IdPath { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Status { get; set; }
        public int? Order { get; set; }
        public string ParentName { get; set; }
        public int Type { get; set; }
        public string PhoneNumber { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
    }
    public class OrganizationModel : BaseOrganizationModel
    {
    }
    public class OrganizationTreeModel
    {
        public Guid Id { get; set; }
        public Guid? ParentId { get; set; }
        public string IdPath { get; set; }
        public string Name { get; set; }
        public List<OrganizationTreeModel> SubChild { get; set; } = new List<OrganizationTreeModel>();
        public OrganizationModel Source { get; set; }
    }
    public class OrganizationQueryModel : PaginationRequest
    {
        public string Code { get; set; }
        public string Status { get; set; }
        public Guid? ParentId { get; set; }
        public string ParentName { get; set; }
        public string Name { get; set; }
        public int? Type { get; set; }
        public Guid? IdInPath { get; set; }

    }

    public class OrganizationCreateModel
    {
        public Guid? ParentId { get; set; }
        public string IdPath { get; set; }
        public string ParentName { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Status { get; set; }
        public int? Order { get; set; }
        public int Type { get; set; }

        public string PhoneNumber { get; set; }
        public string Fax { get; set; }

        public string Email { get; set; }
        public string Address { get; set; }
    }
    public class OrganizationUpdateModel
    {

        public Guid Id { get; set; }
        public string IdPath { get; set; }
        public Guid? ParentId { get; set; }
        public string Code { get; set; }
        public int Type { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Status { get; set; }
        public int? Order { get; set; }
        public string ParentName { get; set; }

        public string PhoneNumber { get; set; }
        public string Fax { get; set; }

        public string Email { get; set; }
        public string Address { get; set; }
    }
}

