﻿using DigitalID.Data;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DigitalID.Business
{
    /// <summary>
    /// Interface quản lý đơn vị phòng ban
    /// </summary>
    public interface IOrganizationV2Handler
    {
        /// <summary>
        /// Thêm mới đơn vị phòng ban
        /// </summary>
        /// <param name="model">Model thêm mới đơn vị phòng ban</param>
        /// <returns>Id đơn vị phòng ban</returns>
        Task<Response> Create(OrganizationV2CreateModel model);

        /// <summary>
        /// Thêm mới đơn vị phòng ban theo danh sách
        /// </summary>
        /// <param name="list">Danh sách thông tin đơn vị phòng ban</param>
        /// <returns>Danh sách kết quả thêm mới</returns> 
        Task<Response> CreateMany(List<OrganizationV2CreateModel> list);

        /// <summary>
        /// Cập nhật đơn vị phòng ban
        /// </summary>
        /// <param name="model">Model cập nhật đơn vị phòng ban</param>
        /// <returns>Id đơn vị phòng ban</returns>
        Task<Response> Update(OrganizationV2UpdateModel model);

        /// <summary>
        /// Xóa đơn vị tính
        /// </summary>
        /// <param name="listId">Danh sách Id đơn vị phòng ban</param>
        /// <returns>Danh sách kết quả xóa</returns>
        Task<Response> Delete(List<Guid> listId);

        /// <summary>
        /// Lấy danh sách đơn vị phòng ban theo điều kiện lọc
        /// </summary>
        /// <param name="filter">Model điều kiện lọc</param>
        /// <returns>Danh sách đơn vị phòng ban</returns>
        Task<Response> Filter(OrganizationV2QueryFilter filter);

        /// <summary>
        /// Lấy đơn vị tính theo Id
        /// </summary>
        /// <param name="id">Id đơn vị phòng ban</param>
        /// <returns>Thông tin đơn vị phòng ban</returns>
        Task<Response> GetById(Guid id);

        /// <summary>
        /// Lấy danh sách đơn vị phòng ban cho combobox
        /// </summary>
        /// <param name="count">Số bản ghi tối đa</param>
        /// <param name="textSearch">Từ khóa tìm kiếm</param>
        /// <returns>Danh sách đơn vị phòng ban cho combobox</returns>
        Task<Response> GetListCombobox(int count = 0, string textSearch = "");
    }
}
