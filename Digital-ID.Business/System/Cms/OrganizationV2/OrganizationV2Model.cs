﻿using DigitalID.Data;
using System;

namespace DigitalID.Business
{
    public class OrganizationV2BaseModel
    {
        public Guid Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public bool Status { get; set; }
        public int? Order { get; set; }
        public string Description { get; set; }
        public string Address { get; set; }
        public DateTime? CreatedOnDate { get; set; }
        public Guid? AreaOperationId { get; set; }
        public string AreaOperationName { get; set; }
        public Guid? ParentId { get; set; }
        public string ParentOrganizationName { get; set; }
        

        

    }

    public class OrganizationV2Model : OrganizationV2BaseModel
    {
        public string PhoneNumber { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string ShortName { get; set; }
        public string IdPath { get; set; }
        public int Type { get; set; }
        public string TypeName { get; set; }
        public string LogoUrl { get; set; }
        public Guid? ProvinceId { get; set; }
        public string ProvinceName { get; set; }
        public Guid? DistrictId { get; set; }
        public string DistrictName { get; set; }
        public Guid? CommuneId { get; set; }
        public string CommuneName { get; set; }
    }

    public class OrganizationV2DetailModel : OrganizationV2Model
    {
        public Guid? CreatedByUserId { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public Guid? ModifiedUserId { get; set; }

        public Guid? ApplicationId { get; set; }
    }

    public class OrganizationV2CreateModel : OrganizationV2Model
    {
        public Guid? CreatedByUserId { get; set; }
        public Guid? ApplicationId { get; set; }
    }

    public class OrganizationV2UpdateModel : OrganizationV2Model
    {
        public Guid ModifiedUserId { get; set; }

        public void UpdateToEntity(CmsOrganization entity)
        {
            entity.Code = this.Code;
            entity.Name = this.Name;
            entity.Status = this.Status;
            entity.Description = this.Description;
            entity.Order = this.Order;
            entity.LastModifiedByUserId = this.ModifiedUserId;
            entity.LastModifiedOnDate = DateTime.Now;
            entity.Address = this.Address;
            entity.ParentId = this.ParentId;
            entity.Email = this.Email;
            entity.Fax = this.Fax;
            entity.PhoneNumber = this.PhoneNumber;
            entity.ShortName = this.ShortName;
            entity.Type = this.Type;
            entity.LogoUrl = this.LogoUrl;
        }
    }

    public class OrganizationV2QueryFilter
    {
        public string TextSearch { get; set; }
        public int? PageSize { get; set; }
        public int? PageNumber { get; set; }
        public bool? Status { get; set; }
        public Guid? AreaOperationId { get; set; }
        public string PropertyName { get; set; } = "CreatedOnDate";
        //asc - desc
        public string Ascending { get; set; } = "desc";
        public OrganizationV2QueryFilter()
        {
            PageNumber = QueryFilter.DefaultPageNumber;
            PageSize = QueryFilter.DefaultPageSize;
        }
    }
}