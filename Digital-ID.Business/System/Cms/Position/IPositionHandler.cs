﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DigitalID.Data;

namespace DigitalID.Business
{
    public interface IPositionHandler
    {
        Task<Response> FindAsync(Guid id);
        Task<Response> CreateAsync(PositionCreateModel model, Guid applicationId, Guid userId);
        Task<Response> UpdateAsync(Guid id, PositionUpdateModel model, Guid applicationId, Guid userId);
        Task<Response> DeleteAsync(Guid id);
        Task<Response> DeleteRangeAsync(IList<Guid> listId);
        Task<Response> GetAllAsync();
        Task<Response> GetAllAsync(PositionQueryModel query);
        Task<Response> GetPageAsync(PositionQueryModel query);
        Response GetAll();
    }
}