﻿using LinqKit;
using DigitalID.Data;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace DigitalID.Business
{
    public class PositionHandler : IPositionHandler
    {
        private readonly DbHandler<CmsPosition, PositionModel, PositionQueryModel> _dbHandler = DbHandler<CmsPosition, PositionModel, PositionQueryModel>.Instance;

        #region CRUD
        public Task<Response> FindAsync(Guid id)
        {
            return _dbHandler.FindAsync(id);
        }
        public async Task<Response> CreateAsync(PositionCreateModel model, Guid applicationId, Guid userId)
        {

            CmsPosition request = AutoMapperUtils.AutoMap<PositionCreateModel, CmsPosition>(model);
            request = request.InitCreate(applicationId, userId);
            request.Id = Guid.NewGuid();
            var result = await _dbHandler.CreateAsync(request,"Code");
            return result;
        }
        public async Task<Response> UpdateAsync(Guid id, PositionUpdateModel model, Guid applicationId, Guid userId)
        {
            CmsPosition request = AutoMapperUtils.AutoMap<PositionUpdateModel, CmsPosition>(model);
            request = request.InitUpdate(applicationId, userId);
            request.Id = id;
            var result = await _dbHandler.UpdateAsync(id, request,"Code");
            return result;
        }
        public async Task<Response> DeleteAsync(Guid id)
        {
            var result = await _dbHandler.DeleteAsync(id);
            return result;
        }
        public async Task<Response> DeleteRangeAsync(IList<Guid> listId)
        {
            var result = await _dbHandler.DeleteRangeAsync(listId, "Id");
            return result;
        }
        #endregion
        public Response GetAll()
        {
            return _dbHandler.GetAll();
        }
        public async Task<Response> GetAllAsync()
        {
            var predicate = PredicateBuilder.New<CmsPosition>(true);
            return await _dbHandler.GetAllAsync(predicate);
        }
        public Task<Response> GetAllAsync(PositionQueryModel query)
        {
            var predicate = BuildQuery(query);
            return _dbHandler.GetAllAsync(predicate, query.Sort);
        }
        public Task<Response> GetPageAsync(PositionQueryModel query)
        {
            var predicate = BuildQuery(query);
            return _dbHandler.GetPageAsync(predicate, query);
        }
        private Expression<Func<CmsPosition, bool>> BuildQuery(PositionQueryModel query)
        {
            var predicate = PredicateBuilder.New<CmsPosition>(true);
            if (!string.IsNullOrEmpty(query.Code))
            {
                predicate.And(s => s.Code == query.Code);
            }
            if (query.Status.HasValue)
            {
                predicate.And(s => s.Status == query.Status.Value);
            }
            if (query.ListId != null && query.ListId.Count > 0)
            {
                predicate.And(s => query.ListId.Contains(s.Id));
            }
            if (!string.IsNullOrEmpty(query.FullTextSearch))
            {
                predicate.And(s =>
                s.Name.ToLower().Contains(query.FullTextSearch.ToLower())
                || s.Code.ToLower().Contains(query.FullTextSearch.ToLower())
                || s.Description.ToLower().Contains(query.FullTextSearch.ToLower())
                );
            }
            return predicate;
        }
    }
}
