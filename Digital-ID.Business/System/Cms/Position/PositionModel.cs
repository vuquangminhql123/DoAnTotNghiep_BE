﻿using DigitalID.Data;
using System;
using System.Collections.Generic;

namespace DigitalID.Business
{
    public class BasePositionModel : BaseModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public bool Status { get; set; }
        public int Order { get; set; }
    }
    public class PositionModel : BasePositionModel
    {
    }
    public class PositionQueryModel : PaginationRequest
    {
        public string Code { get; set; }
        public bool? Status { get; set; }

    }

    public class PositionCreateModel
    {

        public string Name { get; set; }
        public string Code { get; set; }

        public string Description { get; set; }
        public bool Status { get; set; }
        public int Order { get; set; }
    }
    public class PositionUpdateModel
    {

        public string Name { get; set; }
        public string Code { get; set; }

        public string Description { get; set; }
        public bool Status { get; set; }
        public int Order { get; set; }
    }
}

