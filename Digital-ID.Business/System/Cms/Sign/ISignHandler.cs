﻿using DigitalID.Data;
using System;
using System.Threading.Tasks;

namespace DigitalID.Business
{
    public interface ISignHandler
    {
        /// <summary>
        /// Sign with service SignFile
        /// </summary>
        /// <param name="model"></param>
        /// <param name="applicationId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        Task<ResponseObject<FileOuputModel>> SignUrlPdfV1(SignModel model, Guid applicationId, Guid userId);

        /// <summary>
        /// Sign with service trustca
        /// </summary>
        /// <param name="model"></param>
        /// <param name="applicationId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        Task<ResponseObject<FileOuputModel>> SignUrlPdfV2(SignModel model, Guid applicationId, Guid userId);

        /// <summary>
        /// Sign with service trustca
        /// </summary>
        /// <param name="model"></param>
        /// <param name="applicationId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        Task<ResponseObject<FileOuputModel>> SignUrlPdfV3SaveFromUrl(SignModel model, Guid applicationId, Guid userId);

        /// <summary>
        /// Sign with service trustca
        /// </summary>
        /// <param name="model"></param>
        /// <param name="applicationId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        Task<ResponseObject<FileOuputModel>> SignUrlPdfV3SaveFromBase64Content(SignModel model, Guid applicationId, Guid userId);

        /// <summary>
        /// Ký PDF với service HSM của Newwave (CTS trên HSM) danh cho phần mềm khác kết nối
        /// </summary>
        /// <param name="model"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        Task<OutputFromSignMultiTrustCA> SignUrlPdfForThirdpartyFromUrl(DataInputSignPDFnCipher model, Guid userId);

        /// <summary>
        /// Ký PDF với service HSM của Newwave (CTS trên HSM) danh cho phần mềm khác kết nối
        /// </summary>
        /// <param name="model"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        Task<OutputFromSignMultiTrustCAFormat> SignUrlPdfForThirdpartyFromFileContentBase64(DataInputSignPDFnCipher model, Guid userId);

        /// <summary>
        /// Ký điện tử PDF
        /// </summary>
        /// <param name="model"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        OutputFromSignMultiDigital SignDigitalPdf(DataInputSignPDFDigital model, Guid userId);

        /// <summary>
        /// Ký PDF với service HSM của Newwave (CTS trên HSM) danh cho phần mềm khác kết nối with pfx
        /// </summary>
        /// <param name="model"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        Task<OutputFromSignMultiTrustCAFormat> SignUrlPdfForThirdpartyFromFileContentBase64WithPfx(DataInputSignPDFnCipher model, Guid userId);

        /// <summary>
        /// Ký XML với service HSM của Newwave (CTS trên HSM) danh cho phần mềm khác kết nối
        /// </summary>
        /// <param name="model"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        Task<OutputFromSignMultiTrustCA> SignUrlXMLForThirdparty(DataInputSignXMLnCipher model, Guid userId);

        /// <summary>
        /// Ký XML với service HSM của Newwave (CTS trên HSM) danh cho phần mềm khác kết nối với content Base64 XML
        /// </summary>
        /// <param name="model"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        Task<OutputFromSignMultiTrustCA> SignUrlXMLForThirdpartyV2FromUrl(DataInputSignXMLnCipherV2 model, Guid userId);

        /// <summary>
        /// Ký XML với service HSM của Newwave (CTS trên HSM) danh cho phần mềm khác kết nối với content Base64 XML, dữ liệu đã ký base64 từ KMS
        /// </summary>
        /// <param name="model"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        Task<OutputFromSignMultiTrustCA> SignUrlXMLForThirdpartyV2FromBase64(DataInputSignXMLnCipherV2 model, Guid userId);

        /// <summary>
        /// Ký XML với service HSM của Newwave (CTS trên HSM) danh cho phần mềm khác kết nối với content Base64 XML
        /// </summary>
        /// <param name="model"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        Task<OutputFromSignMultiTrustCA> SignUrlXMLForThirdpartyV2ExtraFromUrl(DataInputSignXMLnCipherV2 model, Guid userId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        Task<OutputFromSignMultiTrustCA> CheckAndSetLinkSign(DataInputSignXMLnCipherV2 model, Guid userId);

        /// <summary>
        /// Validate file pdf đã ký
        /// </summary>
        /// <param name="model"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        Task<DataOutputVerifySign> VerifyPdf(DataInputVerifySign model, Guid userId);

        /// <summary>
        /// Validate file XML đã ký
        /// </summary>
        /// <param name="model"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        Task<DataOutputVerifySign> VerifyXML(DataInputVerifySign model, Guid userId);

        /// <summary>
        /// UserPin MD5 Hash Generator
        /// </summary>
        /// <param name="model"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        OldResponse<KeyHashModel> CheckKey(string userPin, Guid userId);
    }
}