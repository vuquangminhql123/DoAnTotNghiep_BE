﻿using DigitalID.Data;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Newtonsoft.Json;
using Serilog;
using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using BcX509 = Org.BouncyCastle.X509;
using Org.BouncyCastle.Pkcs;
using Org.BouncyCastle.Crypto;
using DotNetUtils = Org.BouncyCastle.Security.DotNetUtilities;

namespace DigitalID.Business
{
    public class SignHandler : ISignHandler
    {
        /// <summary>
        /// Ký điện tử file PDF
        /// </summary>
        /// <param name="model"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public OutputFromSignMultiDigital SignDigitalPdf(DataInputSignPDFDigital model, Guid userId)
        {
            try
            {
                OutputFromSignMultiDigital dataReturn = new OutputFromSignMultiDigital();
                dataReturn.Code = 1;
                dataReturn.Message = "Success";
                dataReturn.Data = new List<DataFileSignDigital>();
                using (var unitOfWork = new UnitOfWork())
                {
                    var user = unitOfWork.GetRepository<IdmUser>().Find(x => x.Id == userId);
                    //Check application
                    if (string.IsNullOrEmpty(model.ApplicationId))
                        return new OutputFromSignMultiDigital() { Code = 0, Message = "ApplicationId trống", Data = null };
                    var app = unitOfWork.GetRepository<SysApplication>().Find(x => x.Id == new Guid(model.ApplicationId));
                    if (app == null)
                        return new OutputFromSignMultiDigital() { Code = 0, Message = "Không tìm thấy thông tin Application", Data = null };

                    //Check token
                    if (string.IsNullOrEmpty(model.OTP))
                        return new OutputFromSignMultiDigital() { Code = 0, Message = "OTP trống", Data = null };
                    FileOuputModel dataOut = new FileOuputModel();
                    string contentUrl = Utils.GetConfig("AppSettings:Host") + "StaticFiles/";
                    if (!string.IsNullOrWhiteSpace(model.Profile))
                    {
                        var profile = unitOfWork.GetRepository<CmsSignProfile>().Find(x => x.Code == model.Profile);
                        if (profile == null)
                            return new OutputFromSignMultiDigital() { Code = 0, Message = "Không tìm thấy thông tin profile: " + model.Profile, Data = null };
                        model.Llx = profile.LLX.ToString();
                        model.Lly = profile.LLY.ToString();
                        model.Urx = profile.URX.ToString();
                        model.Ury = profile.URY.ToString();
                    }

                    #region Khởi tạo thư mục
                    string partitionPhysicalPath;
                    string destinationPhysicalPath = "";
                    partitionPhysicalPath = Path.Combine(Utils.GetConfig("StaticFiles:Folder"));
                    // Get root path
                    var rootPath = partitionPhysicalPath;
                    Log.Debug("Upload root path : " + rootPath);
                    // Append destinationPhysicalPath
                    if (string.IsNullOrEmpty(destinationPhysicalPath))
                    {
                        destinationPhysicalPath = ParameterCollection.Instance.GetValue("DefaultAlias");
                        destinationPhysicalPath = destinationPhysicalPath + "\\" +
                                                  DateTime.Now.ToString("yyyy") + "\\" + DateTime.Now.ToString("MM") +
                                                  "\\" +
                                                  DateTime.Now.ToString("dd") + "\\";
                        bool isWindows = RuntimeInformation.IsOSPlatform(OSPlatform.Windows);
                        if (!isWindows)
                        {
                            destinationPhysicalPath = destinationPhysicalPath.Replace("\\", "/");
                        }
                    }
                    // Create folder path
                    var sourcefolder = destinationPhysicalPath;
                    var isExistDirectory = Directory.Exists(Path.Combine(rootPath, sourcefolder));
                    if (!isExistDirectory) Directory.CreateDirectory(Path.Combine(rootPath, sourcefolder));
                    var fullfolderPath = Path.Combine(rootPath, sourcefolder);
                    #endregion

                    #region Ký điện tử PDF
                    using (var client = new WebClient())
                    {
                        //load image
                        byte[] bytesImage = Convert.FromBase64String(model.Image);
                        var preImage = System.Drawing.Image.FromStream(new MemoryStream(bytesImage));
                        var image = Image.GetInstance(preImage, ImageFormat.Png);
                        preImage.Dispose();
                        List<DataFileSignDigital> dataFileOut = new List<DataFileSignDigital>();
                        foreach (var f in model.FileInfo)
                        {
                            string linkdownload = f.FileUrl;
                            var fileByteResponse = client.DownloadData(linkdownload);
                            PdfReader reader = new PdfReader(fileByteResponse);
                            var sizeWithRotation = reader.GetPageSizeWithRotation(1);
                            Document document = new Document(sizeWithRotation);

                            //optional: if image is wider than the page, scale down the image to fit the page                            
                            float withRact = (float)Convert.ToDouble(model.Urx);
                            if (image.Width > withRact)
                                image.ScalePercent(withRact / image.Width * 100);

                            //set image position in top left corner
                            //in pdf files, cooridinates start in the left bottom corner
                            float llx = (float)Convert.ToDouble(model.Llx);
                            float lly = (float)Convert.ToDouble(model.Lly);
                            image.SetAbsolutePosition(llx, lly);

                            //in production, I use MemoryStream
                            //I put FileStream here to test the code in console application
                            string pathFileSigned = Path.Combine(fullfolderPath, Guid.NewGuid() + "_with_logo_and_text.pdf");
                            using (var newFileStream = new FileStream(pathFileSigned, FileMode.Create, FileAccess.Write))
                            {
                                //setup PdfStamper
                                var stamper = new PdfStamper(reader, newFileStream);
                                //iterate through the pages in the original file
                                //for (var i = 1; i <= oldFile.NumberOfPages; i++)
                                //{
                                //get canvas for current page
                                var canvas = stamper.GetOverContent(Convert.ToInt32(model.Page));
                                //add image with pre-set position and size
                                canvas.AddImage(image);

                                canvas.BeginText();
                                BaseFont bfTimesRoman = BaseFont.CreateFont(Environment.GetEnvironmentVariable("windir") + @"\fonts\TIMESBD.TTF", BaseFont.IDENTITY_H, false);
                                canvas.SetFontAndSize(bfTimesRoman, 7);
                                canvas.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Họ và tên: Nguyễn Duy Trung", (llx + 20), (lly + 10), 0);
                                canvas.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "OTP: " + model.OTP, (llx + 20), lly, 0);
                                canvas.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Dấu thời gian: " + DateTime.Now.ToString("dd-MM-yyyy HH:mm"), (llx + 20), (lly - 10), 0);
                                canvas.EndText();
                                stamper.FormFlattening = true;

                                //}
                                stamper.Close();

                                string fileUrl = pathFileSigned.Replace("\\", "/");
                                dataFileOut.Add(new DataFileSignDigital() { FileId = f.FileId, FileUrl = fileUrl });
                            }
                        }
                        dataReturn.Data = dataFileOut;
                        return dataReturn;
                    }
                    #endregion                    
                }
            }
            catch (Exception ex)
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    //Ghi log
                    IdmHelper.AddSignLog(unitOfWork, model.ApplicationId, userId.ToString(), Guid.Empty, "-1", "Lỗi ngoại lệ SignUrlPdfForThirdparty: " + ex.Message + " Model: " + JsonConvert.SerializeObject(model), "");
                }
                return new OutputFromSignMultiDigital() { Code = -1, Message = "Lỗi ngoại lệ: " + ex.Message, Data = null };
            }
        }

        /// <summary>
        /// Ký với thư viện SignFile (demo)
        /// </summary>
        /// <param name="model"></param>
        /// <param name="applicationId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<ResponseObject<FileOuputModel>> SignUrlPdfV1(SignModel model, Guid applicationId, Guid userId)
        {
            try
            {
                FileOuputModel dataOut = new FileOuputModel();
                string destinationPhysicalPath = null;
                string contentUrl = Utils.GetConfig("AppSettings:Host") + "StaticFiles/";
                string apiSign = Utils.GetConfig("Sign:ApiUrl_V1");
                using (var httpClient = new HttpClient())
                {
                    var uri = apiSign + "api/Signing/SignURLPDF";
                    var dataForSign = new SigninginfoPDF();
                    dataForSign.Reason = "Author";
                    dataForSign.Location = "Viet Nam";
                    dataForSign.InputURL = model.TemplateFilePath.Replace("{{API_STATIC_FILE}}", contentUrl);
                    dataForSign.FileName = model.Name;
                    dataForSign.PageNumber = model.SignAtPage;
                    dataForSign.X = model.LLX;//Left
                    dataForSign.Y = GetMiroTop(model.URY, model.AreaHeight, model.SignAtPage, model.PageHeight);//Top
                    dataForSign.ElementWidth = model.AreaWidth;
                    dataForSign.ElementHeight = model.AreaHeight;
                    dataForSign.FontSize = 10;
                    dataForSign.SignatureImageType = 0;
                    dataForSign.PadesLtv = true;
                    dataForSign.TimeStamping = false;
                    dataForSign.PageHeight = model.PageHeight;
                    dataForSign.PageWidth = model.PageWidth;
                    var jsonString = JsonConvert.SerializeObject(dataForSign);
                    try
                    {
                        var content = new StringContent(jsonString, Encoding.UTF8);
                        content.Headers.ContentType.MediaType = "application/json";
                        content.Headers.ContentType.CharSet = "UTF-8";
                        httpClient.Timeout = TimeSpan.FromMinutes(30);
                        HttpResponseMessage response = await httpClient.PostAsync(uri, content);
                        string sd = response.Content.ReadAsStringAsync().Result;
                        var rsSign = JsonConvert.DeserializeObject<ResponseV2<ReturnModel>>(sd);
                        if (rsSign.Status != 200)
                        {
                            return new ResponseObject<FileOuputModel>(null, "1:" + rsSign.Message, Code.ServerError);
                        }
                        else
                        {
                            #region Lấy thông tin vùng lưu trữ vật lý

                            string partitionPhysicalPath;

                            partitionPhysicalPath = Path.Combine(Utils.GetConfig("StaticFiles:Folder"));

                            #endregion

                            #region Xác định thư mục đích

                            // Get root path
                            var rootPath = partitionPhysicalPath;
                            Log.Debug("Upload root path : " + rootPath);
                            // Append destinationPhysicalPath
                            if (string.IsNullOrEmpty(destinationPhysicalPath))
                            {
                                destinationPhysicalPath = ParameterCollection.Instance.GetValue("DefaultAlias");
                                destinationPhysicalPath = destinationPhysicalPath + "\\" +
                                                          DateTime.Now.ToString("yyyy") + "\\" + DateTime.Now.ToString("MM") +
                                                          "\\" +
                                                          DateTime.Now.ToString("dd") + "\\";
                                bool isWindows = RuntimeInformation.IsOSPlatform(OSPlatform.Windows);
                                if (!isWindows)
                                {
                                    destinationPhysicalPath = destinationPhysicalPath.Replace("\\", "/");
                                }
                            }

                            Log.Debug("DestinationPhysicalPath : " + destinationPhysicalPath);
                            var sourcefolder = destinationPhysicalPath;
                            Log.Debug("Upload source folder : " + sourcefolder);
                            // Create folder path
                            var isExistDirectory = Directory.Exists(Path.Combine(rootPath, sourcefolder));
                            if (!isExistDirectory) Directory.CreateDirectory(Path.Combine(rootPath, sourcefolder));
                            var fullfolderPath = Path.Combine(rootPath, sourcefolder);

                            #endregion

                            #region Lưu file upload vật lý

                            // get data image from client
                            var base64 = rsSign.Data.Base64Combined;
                            if (base64.IndexOf(',') >= 0) base64 = base64.Substring(base64.IndexOf(',') + 1);
                            var bytes = Convert.FromBase64String(base64);
                            var filename = rsSign.Data.FileName;

                            #region makefileName

                            var filePrefix = DateTime.Now.ToString("HH") + DateTime.Now.ToString("mm") +
                                             DateTime.Now.ToString("ss") +
                                             new Random(DateTime.Now.Millisecond).Next(10, 99);
                            var filePostFix = DateTime.Now.ToString("yy-MM-dd");
                            var newFileName = Path.GetFileNameWithoutExtension(filename) + "_" + filePrefix + "_" +
                                              filePostFix + Path.GetExtension(filename);
                            Log.Debug("New file name : " + newFileName);
                            if (newFileName.Length > 255)
                            {
                                var withoutExtName = Path.GetFileNameWithoutExtension(filename);
                                var extName = Path.GetExtension(filename);
                                var trimmed = withoutExtName.Substring(0,
                                    withoutExtName.Length - (255 - filePrefix.Length - filePostFix.Length - extName.Length));
                                newFileName = trimmed + "_" + filePrefix + "_" + filePostFix + Path.GetExtension(filename);
                            }

                            #endregion

                            var fileSavePath = Path.Combine(fullfolderPath, newFileName);
                            using (var file = new FileStream(fileSavePath, FileMode.Create))
                            {
                                await file.WriteAsync(bytes, 0, bytes.Length);
                                file.Flush();
                            }

                            var fileInfo = new FileInfo(fileSavePath);
                            string fileSignedPath = Path.Combine(sourcefolder, fileInfo.Name).Replace("\\", "/");
                            #endregion
                            dataOut.FileName = fileInfo.Name;
                            dataOut.FileUrl = fileSignedPath;
                            return new ResponseObject<FileOuputModel>(dataOut, "OK", Code.Success);
                        }
                    }
                    catch (Exception ex)
                    {
                        return new ResponseObject<FileOuputModel>(null, "2:" + ex.Message, Code.ServerError);
                    }
                }
            }
            catch (Exception ex)
            {
                return new ResponseObject<FileOuputModel>(null, ex.Message, Code.ServerError);
            }
        }

        /// <summary>
        /// Ký với service trustca của ThanhNH (file pfx)
        /// </summary>
        /// <param name="model"></param>
        /// <param name="applicationId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<ResponseObject<FileOuputModel>> SignUrlPdfV2(SignModel model, Guid applicationId, Guid userId)
        {
            try
            {
                FileOuputModel dataOut = new FileOuputModel();
                string destinationPhysicalPath = null;
                string partitionPhysicalPath = "";
                partitionPhysicalPath = Path.Combine(Utils.GetConfig("StaticFiles:Folder"));
                string sourcefolder = ParameterCollection.Instance.GetValue("DefaultAlias");
                string contentUrl = Utils.GetConfig("AppSettings:Host") + "StaticFiles/";
                string apiSignPdf = Utils.GetConfig("Sign:ApiUrl_V2");
                DataInputSignPDFTrustCA dataPdfTrustCA = new DataInputSignPDFTrustCA();
                List<DataFileSignTrustCA> fileInfoPdf = new List<DataFileSignTrustCA>();
                dataPdfTrustCA.ContactInfo = "";
                var fullPathLogoDefault = Path.Combine(partitionPhysicalPath, sourcefolder, "logo.png");
                byte[] bytesImg = System.IO.File.ReadAllBytes(fullPathLogoDefault);
                string base64StringImg = Convert.ToBase64String(bytesImg);
                dataPdfTrustCA.Image = base64StringImg;
                dataPdfTrustCA.IsVisible = Convert.ToInt32(Utils.GetConfig("Sign:IsVisible"));
                dataPdfTrustCA.Llx = model.LLX.ToString();
                dataPdfTrustCA.Lly = (model.URY + model.AreaHeight).ToString();
                dataPdfTrustCA.Urx = (model.LLX + model.AreaWidth).ToString();
                dataPdfTrustCA.Ury = model.URY.ToString();
                dataPdfTrustCA.Location = "VietNam";
                dataPdfTrustCA.Reason = "Signed";
                string filePdfUrl = model.TemplateFilePath.Replace("{{API_STATIC_FILE}}", contentUrl);
                fileInfoPdf.Add(new DataFileSignTrustCA() { FileId = Guid.NewGuid().ToString(), FileUrl = filePdfUrl });
                dataPdfTrustCA.FileInfo = fileInfoPdf;

                #region Ký PDF
                using (var httpClientPdf = new HttpClient())
                {
                    var jsonString_pdf = JsonConvert.SerializeObject(dataPdfTrustCA);
                    var content_pdf = new StringContent(jsonString_pdf, Encoding.UTF8);
                    content_pdf.Headers.ContentType.MediaType = "application/json";
                    content_pdf.Headers.ContentType.CharSet = "UTF-8";
                    HttpResponseMessage responsePdf = await httpClientPdf.PostAsync(apiSignPdf, content_pdf);
                    string sdPdf = responsePdf.Content.ReadAsStringAsync().Result;
                    var rsSign = JsonConvert.DeserializeObject<OutputFromSignMultiTrustCA>(sdPdf);

                    if (rsSign.Code == 1)
                    {
                        var fileSigning = rsSign.Data[0];

                        var currentInvoiceId = new Guid(fileSigning.FileId);
                        var linkdownload = fileSigning.FileUrl;
                        // Ký xong sẽ download file về
                        using (var client = new WebClient())
                        {
                            var fileByteResponse = client.DownloadData(linkdownload);
                            string newFileNamePdf = model.Name.Replace(".pdf", "") + "_signed.pdf";

                            #region Xác định thư mục đích

                            // Get root path
                            var rootPath = partitionPhysicalPath;
                            Log.Debug("Upload root path : " + rootPath);
                            // Append destinationPhysicalPath
                            if (string.IsNullOrEmpty(destinationPhysicalPath))
                            {
                                destinationPhysicalPath = ParameterCollection.Instance.GetValue("DefaultAlias");
                                destinationPhysicalPath = destinationPhysicalPath + "\\" +
                                                          DateTime.Now.ToString("yyyy") + "\\" + DateTime.Now.ToString("MM") +
                                                          "\\" +
                                                          DateTime.Now.ToString("dd") + "\\";
                                bool isWindows = RuntimeInformation.IsOSPlatform(OSPlatform.Windows);
                                if (!isWindows)
                                {
                                    destinationPhysicalPath = destinationPhysicalPath.Replace("\\", "/");
                                }
                            }

                            Log.Debug("DestinationPhysicalPath : " + destinationPhysicalPath);
                            sourcefolder = destinationPhysicalPath;
                            Log.Debug("Upload source folder : " + sourcefolder);
                            // Create folder path
                            var isExistDirectory = Directory.Exists(Path.Combine(rootPath, sourcefolder));
                            if (!isExistDirectory) Directory.CreateDirectory(Path.Combine(rootPath, sourcefolder));
                            var fullfolderPath = Path.Combine(rootPath, sourcefolder);

                            #endregion
                            var fileSavePath = Path.Combine(fullfolderPath, newFileNamePdf);
                            File.WriteAllBytes(fileSavePath, fileByteResponse);
                            var fileInfo = new FileInfo(fileSavePath);
                            string fileSignedPath = Path.Combine(sourcefolder, fileInfo.Name).Replace("\\", "/");
                            dataOut.FileName = fileInfo.Name;
                            dataOut.FileUrl = fileSignedPath;
                            return new ResponseObject<FileOuputModel>(dataOut, "OK", Code.Success);
                        }
                    }
                    else
                    {
                        return new ResponseObject<FileOuputModel>(null, "Ký thất bại: " + JsonConvert.SerializeObject(jsonString_pdf), Code.ServerError);
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                return new ResponseObject<FileOuputModel>(null, ex.Message, Code.ServerError);
            }
        }

        /// <summary>
        /// Ký với service HSM của Newwave (CTS trên HSM) danh cho nội bộ
        /// </summary>
        /// <param name="model"></param>
        /// <param name="applicationId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<ResponseObject<FileOuputModel>> SignUrlPdfV3SaveFromUrl(SignModel model, Guid applicationId, Guid userId)
        {
            try
            {
                FileOuputModel dataOut = new FileOuputModel();
                string destinationPhysicalPath = null;
                string partitionPhysicalPath = "";
                partitionPhysicalPath = Path.Combine(Utils.GetConfig("StaticFiles:Folder"));
                string sourcefolder = ParameterCollection.Instance.GetValue("DefaultAlias");
                string contentUrl = Utils.GetConfig("AppSettings:Host") + "StaticFiles/";
                string contentUrlPrivate = Utils.GetConfig("AppSettings:HostPrivate") + "StaticFiles/";
                string apiSignPdf = Utils.GetConfig("Sign:ApiUrl_V3");
                string basic_user = Utils.GetConfig("Sign:Basic_User");
                string basic_pass = Utils.GetConfig("Sign:Basic_Pass");
                DataInputSignPDFTrustCA dataPdfTrustCA = new DataInputSignPDFTrustCA();
                List<DataFileSignTrustCA> fileInfoPdf = new List<DataFileSignTrustCA>();
                dataPdfTrustCA.ContactInfo = "";
                string base64StringImg = "";
                try
                {
                    var fullPathSignDefault = Path.Combine(partitionPhysicalPath, model.SignFinalPath);
                    byte[] bytesImg = System.IO.File.ReadAllBytes(fullPathSignDefault);
                    base64StringImg = Convert.ToBase64String(bytesImg);
                }
                catch (Exception)
                {
                    new ResponseObject<FileOuputModel>(null, "Chưa định nghĩa chữ ký cá nhân", Code.ServerError);
                }
                dataPdfTrustCA.Image = base64StringImg;
                dataPdfTrustCA.IsVisible = Convert.ToInt32(Utils.GetConfig("Sign:IsVisible"));
                dataPdfTrustCA.Llx = model.LLX.ToString();
                dataPdfTrustCA.Lly = (model.LLY - 51).ToString();
                dataPdfTrustCA.Urx = model.AreaWidth.ToString();
                dataPdfTrustCA.Ury = model.AreaHeight.ToString();
                dataPdfTrustCA.Location = "VietNam";
                dataPdfTrustCA.Reason = "Signed";
                dataPdfTrustCA.Page = model.SignAtPage.ToString();
                string filePdfUrl = model.TemplateFilePath.Replace("{{API_STATIC_FILE}}", contentUrlPrivate);
                fileInfoPdf.Add(new DataFileSignTrustCA() { FileId = Guid.NewGuid().ToString(), FileUrl = filePdfUrl });
                dataPdfTrustCA.FileInfo = fileInfoPdf;

                var certificate = new CertificateSignModel();
                certificate = GetCertificate(model.Key, userId);

                if (certificate == null)
                    return new ResponseObject<FileOuputModel>(null, "Thông tin key không hợp lệ", Code.ServerError);

                dataPdfTrustCA.SlotLabel = certificate.SlotLabel;
                dataPdfTrustCA.UserPin = certificate.UserPin;
                dataPdfTrustCA.Alias = certificate.Alias;
                //dataPdfTrustCA.CertificateFileName = certificate.CertificateFileName;
                dataPdfTrustCA.CertString = certificate.CertString;

                #region Ký PDF
                using (var httpClientPdf = new HttpClient())
                {
                    var byteArray = Encoding.ASCII.GetBytes("" + basic_user + ":" + basic_pass + "");
                    httpClientPdf.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));
                    var jsonString_pdf = JsonConvert.SerializeObject(dataPdfTrustCA);
                    var content_pdf = new StringContent(jsonString_pdf, Encoding.UTF8);
                    content_pdf.Headers.ContentType.MediaType = "application/json";
                    content_pdf.Headers.ContentType.CharSet = "UTF-8";
                    HttpResponseMessage responsePdf = await httpClientPdf.PostAsync(apiSignPdf, content_pdf);
                    string sdPdf = responsePdf.Content.ReadAsStringAsync().Result;
                    var rsSign = JsonConvert.DeserializeObject<OutputFromSignMultiTrustCA>(sdPdf);

                    if (rsSign.Code == 1)
                    {
                        var fileSigning = rsSign.Data[0];

                        var currentInvoiceId = new Guid(fileSigning.FileId);
                        var linkdownload = fileSigning.FileUrl;
                        // Ký xong sẽ download file về
                        using (var client = new WebClient())
                        {
                            string credentials = Convert.ToBase64String(Encoding.ASCII.GetBytes("" + basic_user + "" + ":" + "" + basic_pass + ""));
                            client.Headers[HttpRequestHeader.Authorization] = string.Format("Basic {0}", credentials);
                            var fileByteResponse = client.DownloadData(linkdownload);
                            string newFileNamePdf = model.Name.Replace(".pdf", "") + "_signed.pdf";

                            #region Xác định thư mục đích
                            // Get root path
                            var rootPath = partitionPhysicalPath;
                            Log.Debug("Upload root path : " + rootPath);
                            // Append destinationPhysicalPath
                            if (string.IsNullOrEmpty(destinationPhysicalPath))
                            {
                                destinationPhysicalPath = ParameterCollection.Instance.GetValue("DefaultAlias");
                                destinationPhysicalPath = destinationPhysicalPath + "\\" +
                                                          DateTime.Now.ToString("yyyy") + "\\" + DateTime.Now.ToString("MM") +
                                                          "\\" +
                                                          DateTime.Now.ToString("dd") + "\\";
                                bool isWindows = RuntimeInformation.IsOSPlatform(OSPlatform.Windows);
                                if (!isWindows)
                                {
                                    destinationPhysicalPath = destinationPhysicalPath.Replace("\\", "/");
                                }
                            }

                            Log.Debug("DestinationPhysicalPath : " + destinationPhysicalPath);
                            sourcefolder = destinationPhysicalPath;
                            Log.Debug("Upload source folder : " + sourcefolder);
                            // Create folder path
                            var isExistDirectory = Directory.Exists(Path.Combine(rootPath, sourcefolder));
                            if (!isExistDirectory) Directory.CreateDirectory(Path.Combine(rootPath, sourcefolder));
                            var fullfolderPath = Path.Combine(rootPath, sourcefolder);

                            #endregion
                            var fileSavePath = Path.Combine(fullfolderPath, newFileNamePdf);
                            File.WriteAllBytes(fileSavePath, fileByteResponse);
                            var fileInfo = new FileInfo(fileSavePath);
                            string fileSignedPath = Path.Combine(sourcefolder, fileInfo.Name).Replace("\\", "/");
                            dataOut.FileName = fileInfo.Name;
                            dataOut.FileUrl = fileSignedPath;
                            return new ResponseObject<FileOuputModel>(dataOut, "OK", Code.Success);
                        }
                    }
                    else
                    {
                        return new ResponseObject<FileOuputModel>(null, "Ký thất bại: " + JsonConvert.SerializeObject(jsonString_pdf), Code.ServerError);
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                return new ResponseObject<FileOuputModel>(null, ex.Message, Code.ServerError);
            }
        }

        /// <summary>
        /// Ký với service HSM của Newwave (CTS trên HSM) danh cho nội bộ, dữ liệu đã ký base64 từ KMS
        /// </summary>
        /// <param name="model"></param>
        /// <param name="applicationId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<ResponseObject<FileOuputModel>> SignUrlPdfV3SaveFromBase64Content(SignModel model, Guid applicationId, Guid userId)
        {
            try
            {
                FileOuputModel dataOut = new FileOuputModel();
                string destinationPhysicalPath = null;
                string partitionPhysicalPath = "";
                partitionPhysicalPath = Path.Combine(Utils.GetConfig("StaticFiles:Folder"));
                string sourcefolder = ParameterCollection.Instance.GetValue("DefaultAlias");
                string contentUrl = Utils.GetConfig("AppSettings:Host") + "StaticFiles/";
                string contentUrlPrivate = Utils.GetConfig("AppSettings:HostPrivate") + "StaticFiles/";
                string apiSignPdf = Utils.GetConfig("Sign:ApiUrl_V3");
                string basic_user = Utils.GetConfig("Sign:Basic_User");
                string basic_pass = Utils.GetConfig("Sign:Basic_Pass");
                int distance_Margin = Convert.ToInt32(Utils.GetConfig("Sign:Distance_Margin"));
                DataInputSignPDFTrustCA dataPdfTrustCA = new DataInputSignPDFTrustCA();
                List<DataFileSignTrustCA> fileInfoPdf = new List<DataFileSignTrustCA>();
                dataPdfTrustCA.ContactInfo = "";
                string base64StringImg = "";
                try
                {
                    string fullPathSignDefault = Path.Combine(partitionPhysicalPath + "/sign/sign-default.png");
                    if (!string.IsNullOrEmpty(model.SignFinalPath))
                    {
                        fullPathSignDefault = Path.Combine(partitionPhysicalPath, model.SignFinalPath);
                    }
                    byte[] bytesImg = System.IO.File.ReadAllBytes(fullPathSignDefault);
                    base64StringImg = Convert.ToBase64String(bytesImg);
                }
                catch (Exception)
                {
                    new ResponseObject<FileOuputModel>(null, "Chưa định nghĩa chữ ký cá nhân", Code.ServerError);
                }
                #region Convert model into API SignPDF
                dataPdfTrustCA.Image = base64StringImg;
                dataPdfTrustCA.IsVisible = Convert.ToInt32(Utils.GetConfig("Sign:IsVisible"));
                dataPdfTrustCA.Llx = model.LLX.ToString();
                dataPdfTrustCA.Lly = (model.LLY - distance_Margin).ToString();
                dataPdfTrustCA.Urx = model.AreaWidth.ToString();
                dataPdfTrustCA.Ury = model.AreaHeight.ToString();
                dataPdfTrustCA.Location = "VietNam";
                dataPdfTrustCA.Reason = "Signed";
                dataPdfTrustCA.Page = model.SignAtPage.ToString();
                string filePdfUrl = model.TemplateFilePath.Replace("{{API_STATIC_FILE}}", contentUrlPrivate);
                fileInfoPdf.Add(new DataFileSignTrustCA() { FileId = Guid.NewGuid().ToString(), FileUrl = filePdfUrl });
                dataPdfTrustCA.FileInfo = fileInfoPdf;

                var certificate = new CertificateSignModel();
                certificate = GetCertificate(model.Key, userId);

                if (certificate == null)
                    return new ResponseObject<FileOuputModel>(null, "Thông tin key không hợp lệ", Code.ServerError);

                dataPdfTrustCA.SlotLabel = certificate.SlotLabel;
                dataPdfTrustCA.UserPin = certificate.UserPin;
                dataPdfTrustCA.Alias = certificate.Alias;
                //dataPdfTrustCA.CertificateFileName = certificate.CertificateFileName;
                dataPdfTrustCA.CertString = certificate.CertString;
                #endregion

                #region Ký PDF
                using (var httpClientPdf = new HttpClient())
                {
                    var byteArray = Encoding.ASCII.GetBytes("" + basic_user + ":" + basic_pass + "");
                    httpClientPdf.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));
                    var jsonString_pdf = JsonConvert.SerializeObject(dataPdfTrustCA);
                    var content_pdf = new StringContent(jsonString_pdf, Encoding.UTF8);
                    content_pdf.Headers.ContentType.MediaType = "application/json";
                    content_pdf.Headers.ContentType.CharSet = "UTF-8";
                    HttpResponseMessage responsePdf = await httpClientPdf.PostAsync(apiSignPdf, content_pdf);
                    string sdPdf = responsePdf.Content.ReadAsStringAsync().Result;
                    var rsSign = JsonConvert.DeserializeObject<OutputFromSignMultiTrustCA>(sdPdf);

                    if (rsSign.Code == 1)
                    {
                        //Ký xong sẽ tạo file từ base64content
                        var fileSigning = rsSign.Data[0];
                        string fileContentBase64 = fileSigning.OuputFileContentBase64;
                        string newFileNamePdf = model.Name.Replace(".pdf", "") + "_signed.pdf";

                        #region Xác định thư mục đích
                        // Get root path
                        var rootPath = partitionPhysicalPath;
                        Log.Debug("Upload root path : " + rootPath);
                        // Append destinationPhysicalPath
                        if (string.IsNullOrEmpty(destinationPhysicalPath))
                        {
                            destinationPhysicalPath = ParameterCollection.Instance.GetValue("DefaultAlias");
                            destinationPhysicalPath = destinationPhysicalPath + "\\" +
                                                      DateTime.Now.ToString("yyyy") + "\\" + DateTime.Now.ToString("MM") +
                                                      "\\" +
                                                      DateTime.Now.ToString("dd") + "\\";
                            bool isWindows = RuntimeInformation.IsOSPlatform(OSPlatform.Windows);
                            if (!isWindows)
                            {
                                destinationPhysicalPath = destinationPhysicalPath.Replace("\\", "/");
                            }
                        }

                        Log.Debug("DestinationPhysicalPath : " + destinationPhysicalPath);
                        sourcefolder = destinationPhysicalPath;
                        Log.Debug("Upload source folder : " + sourcefolder);
                        // Create folder path
                        var isExistDirectory = Directory.Exists(Path.Combine(rootPath, sourcefolder));
                        if (!isExistDirectory) Directory.CreateDirectory(Path.Combine(rootPath, sourcefolder));
                        var fullfolderPath = Path.Combine(rootPath, sourcefolder);

                        #endregion
                        var fileSavePath = Path.Combine(fullfolderPath, newFileNamePdf);
                        File.WriteAllBytes(@"" + fileSavePath + "", Convert.FromBase64String(fileContentBase64));
                        var fileInfo = new FileInfo(fileSavePath);
                        string fileSignedPath = Path.Combine(sourcefolder, fileInfo.Name).Replace("\\", "/");
                        dataOut.FileName = fileInfo.Name;
                        dataOut.FileUrl = fileSignedPath;
                        return new ResponseObject<FileOuputModel>(dataOut, "OK", Code.Success);
                    }
                    else
                    {
                        return new ResponseObject<FileOuputModel>(null, "Ký thất bại: " + JsonConvert.SerializeObject(jsonString_pdf), Code.ServerError);
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                return new ResponseObject<FileOuputModel>(null, ex.Message, Code.ServerError);
            }
        }

        /// <summary>
        /// Ký với service HSM của Newwave (CTS trên HSM) danh cho phần mềm khác kết nối
        /// </summary>
        /// <param name="model"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<OutputFromSignMultiTrustCA> SignUrlPdfForThirdpartyFromUrl(DataInputSignPDFnCipher model, Guid userId)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    //var org = (from ou in unitOfWork.GetRepository<IdmUserMapOrg>().GetAll()
                    //           join o in unitOfWork.GetRepository<CmsOrganization>().GetAll()
                    //           on ou.OrganizationId equals o.Id
                    //           where ou.UserId == userId
                    //           select o).FirstOrDefault();
                    var user = unitOfWork.GetRepository<IdmUser>().Find(x => x.Id == userId);
                    if (user == null)
                        return new OutputFromSignMultiTrustCA() { Code = 0, Message = "Không tìm thấy thông tin tài khoản", Data = null };
                    FileOuputModel dataOut = new FileOuputModel();
                    string basic_user = Utils.GetConfig("Sign:Basic_User");
                    string basic_pass = Utils.GetConfig("Sign:Basic_Pass");
                    string apiSignPdf = Utils.GetConfig("Sign:ApiUrl_V3");
                    string contentUrl = Utils.GetConfig("AppSettings:Host") + "StaticFiles/";
                    DataInputSignPDFTrustCA dataPdfTrustCA = new DataInputSignPDFTrustCA();
                    if (string.IsNullOrEmpty(model.Image))
                    {
                        string signImage = Utils.GetConfig("StaticFiles:Folder") + "/sign/sign-default.png";
                        if (!string.IsNullOrEmpty(user.SignFinalPath))
                        {
                            signImage = user.SignFinalPath.Replace("{{API_STATIC_FILE}}", Utils.GetConfig("StaticFiles:Folder") + "/");
                        }
                        byte[] arrByteImage = File.ReadAllBytes(signImage);
                        model.Image = Convert.ToBase64String(arrByteImage);
                    }
                    dataPdfTrustCA.Image = model.Image;
                    dataPdfTrustCA.IsVisible = model.IsVisible;
                    dataPdfTrustCA.Llx = model.Llx;
                    dataPdfTrustCA.Lly = model.Lly;
                    dataPdfTrustCA.Urx = model.Urx;
                    dataPdfTrustCA.Ury = model.Ury;
                    if (!string.IsNullOrWhiteSpace(model.Profile))
                    {
                        var profile = unitOfWork.GetRepository<CmsSignProfile>().Find(x => x.Code == model.Profile);
                        if (profile == null)
                            return new OutputFromSignMultiTrustCA() { Code = 0, Message = "Không tìm thấy thông tin profile: " + model.Profile, Data = null };
                        dataPdfTrustCA.Llx = profile.LLX.ToString();
                        dataPdfTrustCA.Lly = profile.LLY.ToString();
                        dataPdfTrustCA.Urx = profile.URX.ToString();
                        dataPdfTrustCA.Ury = profile.URY.ToString();
                    }
                    dataPdfTrustCA.Location = model.Location;
                    dataPdfTrustCA.Reason = model.Reason;
                    dataPdfTrustCA.Page = model.Page;
                    dataPdfTrustCA.ContactInfo = model.ContactInfo;
                    dataPdfTrustCA.FileInfo = model.FileInfo;

                    #region Khởi tạo thư mục
                    string partitionPhysicalPath;
                    string destinationPhysicalPath = "";
                    partitionPhysicalPath = Path.Combine(Utils.GetConfig("StaticFiles:Folder"));
                    // Get root path
                    var rootPath = partitionPhysicalPath;
                    Log.Debug("Upload root path : " + rootPath);
                    // Append destinationPhysicalPath
                    if (string.IsNullOrEmpty(destinationPhysicalPath))
                    {
                        destinationPhysicalPath = ParameterCollection.Instance.GetValue("DefaultAlias");
                        destinationPhysicalPath = destinationPhysicalPath + "\\" +
                                                  DateTime.Now.ToString("yyyy") + "\\" + DateTime.Now.ToString("MM") +
                                                  "\\" +
                                                  DateTime.Now.ToString("dd") + "\\";
                        bool isWindows = RuntimeInformation.IsOSPlatform(OSPlatform.Windows);
                        if (!isWindows)
                        {
                            destinationPhysicalPath = destinationPhysicalPath.Replace("\\", "/");
                        }
                    }
                    // Create folder path
                    var sourcefolder = destinationPhysicalPath;
                    var isExistDirectory = Directory.Exists(Path.Combine(rootPath, sourcefolder));
                    if (!isExistDirectory) Directory.CreateDirectory(Path.Combine(rootPath, sourcefolder));
                    var fullfolderPath = Path.Combine(rootPath, sourcefolder);
                    #endregion

                    //Lấy thông tin cert theo user + key
                    var certifcate = new CertificateSignModel();
                    certifcate = GetCertificateV2(model.Key, userId);

                    if (certifcate == null)
                        return new OutputFromSignMultiTrustCA() { Code = 0, Message = "Thông tin key không hợp lệ", Data = null };

                    dataPdfTrustCA.SlotLabel = certifcate.SlotLabel;
                    dataPdfTrustCA.UserPin = certifcate.UserPin;
                    dataPdfTrustCA.Alias = certifcate.Alias;
                    //dataPdfTrustCA.CertificateFileName = certifcate.CertificateFileName;
                    dataPdfTrustCA.CertString = certifcate.CertString;

                    #region Ký PDF
                    using (var httpClientPdf = new HttpClient())
                    {
                        var byteArray = Encoding.ASCII.GetBytes("" + basic_user + ":" + basic_pass + "");
                        httpClientPdf.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));
                        var jsonString_pdf = JsonConvert.SerializeObject(dataPdfTrustCA);
                        var content_pdf = new StringContent(jsonString_pdf, Encoding.UTF8);
                        content_pdf.Headers.ContentType.MediaType = "application/json";
                        content_pdf.Headers.ContentType.CharSet = "UTF-8";
                        HttpResponseMessage responsePdf = await httpClientPdf.PostAsync(apiSignPdf, content_pdf);
                        string sdPdf = responsePdf.Content.ReadAsStringAsync().Result;
                        var rsSign = JsonConvert.DeserializeObject<OutputFromSignMultiTrustCA>(sdPdf);
                        if (rsSign.Code == 1 && rsSign.Data.Count > 0)
                        {
                            rsSign.MST = DateTime.Now.ToString("dd/MM/yyyy HH:mm");
                            #region Tải file đã ký về  và trả cho thirdparty nếu thành công
                            using (var client = new WebClient())
                            {
                                string credentials = Convert.ToBase64String(Encoding.ASCII.GetBytes("" + basic_user + "" + ":" + "" + basic_pass + ""));
                                client.Headers[HttpRequestHeader.Authorization] = string.Format("Basic {0}", credentials);

                                foreach (var item in rsSign.Data)
                                {
                                    var f = model.FileInfo.FirstOrDefault(x => x.FileId == item.FileId);
                                    if (f != null)
                                    {
                                        string linkdownload = item.FileUrl;
                                        var fileByteResponse = client.DownloadData(linkdownload);
                                        var fileSavePath = Path.Combine(fullfolderPath, "file_signed.pdf");
                                        File.WriteAllBytes(fileSavePath, fileByteResponse);
                                        var fileInfo = new FileInfo(fileSavePath);
                                        string fileSignedPath = Path.Combine(sourcefolder, fileInfo.Name).Replace("\\", "/");
                                        string fileUrl = new Uri(contentUrl + fileSignedPath).ToString();
                                        item.FileUrl = fileUrl;
                                        //Ghi log
                                        IdmHelper.AddSignLog(unitOfWork, model.ApplicationId, userId.ToString(), certifcate.CertId.Value, rsSign.Code + "", "Ký thành công", item.FileId);
                                    }
                                    else
                                    {
                                        //Ghi log
                                        IdmHelper.AddSignLog(unitOfWork, model.ApplicationId, userId.ToString(), certifcate.CertId.Value, rsSign.Code + "", "Không tìm thấy dữ liệu file ký", item.FileId);
                                    }
                                }
                            }
                            if (user != null)
                            {
                                rsSign.TenDonVi = user.Name;
                            }
                            return rsSign;
                            #endregion
                        }
                        else
                        {
                            foreach (var f in model.FileInfo)
                            {
                                //Ghi log
                                IdmHelper.AddSignLog(unitOfWork, model.ApplicationId, userId.ToString(), certifcate.CertId.Value, rsSign.Code + "", "Ký không thành công: " + rsSign.Message, f.FileId);
                            }
                            return rsSign;
                        }
                    }
                    #endregion                    
                }
            }
            catch (Exception ex)
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    //Ghi log
                    IdmHelper.AddSignLog(unitOfWork, model.ApplicationId, userId.ToString(), Guid.Empty, "-1", "Lỗi ngoại lệ SignUrlPdfForThirdparty: " + ex.Message + " Model: " + JsonConvert.SerializeObject(model), "");
                }
                return new OutputFromSignMultiTrustCA() { Code = -1, Message = "Lỗi ngoại lệ: " + ex.Message, Data = null };
            }
        }

        /// <summary>
        /// Ký với service HSM của Newwave (CTS trên HSM) danh cho phần mềm khác kết nối, dữ liệu đã ký base64 từ KMS with pfx
        /// </summary>
        /// <param name="model"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<OutputFromSignMultiTrustCAFormat> SignUrlPdfForThirdpartyFromFileContentBase64WithPfx(DataInputSignPDFnCipher model, Guid userId)
        {
            try
            {
                string basic_user = Utils.GetConfig("Sign:Basic_User");
                string basic_pass = Utils.GetConfig("Sign:Basic_Pass");
                string apiSignPdf = Utils.GetConfig("Sign:ApiUrl_V3_From_Pfx");
                OutputFromSignMultiTrustCAFormat dataReturn = new OutputFromSignMultiTrustCAFormat();
                using (var unitOfWork = new UnitOfWork())
                {
                    //var org = (from ou in unitOfWork.GetRepository<IdmUserMapOrg>().GetAll()
                    //           join o in unitOfWork.GetRepository<CmsOrganization>().GetAll()
                    //           on ou.OrganizationId equals o.Id
                    //           where ou.UserId == userId
                    //           select o).FirstOrDefault();
                    var user = unitOfWork.GetRepository<IdmUser>().Find(x => x.Id == userId);
                    if (user == null)
                        return new OutputFromSignMultiTrustCAFormat() { Code = 0, Message = "Không tìm thấy thông tin tài khoản", Data = null };
                    FileOuputModel dataOut = new FileOuputModel();

                    DataInputSignPDFTrustCA dataPdfTrustCA = new DataInputSignPDFTrustCA();
                    //Lấy thông tin cert theo user + key
                    var certifcate = new CertificateSignModel();
                    certifcate = GetCertificateV2(model.Key, userId);

                    if (certifcate == null)
                        return new OutputFromSignMultiTrustCAFormat() { Code = 0, Message = "Thông tin key không hợp lệ", Data = null };

                    dataPdfTrustCA.SlotLabel = certifcate.SlotLabel;
                    dataPdfTrustCA.UserPin = certifcate.UserPin;
                    dataPdfTrustCA.Alias = certifcate.Alias;
                    //dataPdfTrustCA.CertificateFileName = certifcate.CertificateFileName;
                    dataPdfTrustCA.CertString = certifcate.P12Path;

                    string contentUrl = Utils.GetConfig("AppSettings:Host") + "StaticFiles/";
                    if (string.IsNullOrEmpty(model.Image))
                    {
                        string signImage = Utils.GetConfig("StaticFiles:Folder") + "/sign/sign-default.png";
                        if (!string.IsNullOrEmpty(user.SignFinalPath))
                        {
                            signImage = user.SignFinalPath.Replace("{{API_STATIC_FILE}}", Utils.GetConfig("StaticFiles:Folder") + "/");                            
                        }
                        byte[] arrByteImage = File.ReadAllBytes(signImage);
                        model.Image = Convert.ToBase64String(arrByteImage);
                    }
                    dataPdfTrustCA.Image = model.Image;
                    dataPdfTrustCA.IsVisible = model.IsVisible;
                    dataPdfTrustCA.Llx = model.Llx;
                    dataPdfTrustCA.Lly = model.Lly;
                    dataPdfTrustCA.Urx = model.Urx;
                    dataPdfTrustCA.Ury = model.Ury;
                    if (!string.IsNullOrWhiteSpace(model.Profile))
                    {
                        var profile = unitOfWork.GetRepository<CmsSignProfile>().Find(x => x.Code == model.Profile);
                        if (profile == null)
                            return new OutputFromSignMultiTrustCAFormat() { Code = 0, Message = "Không tìm thấy thông tin profile: " + model.Profile, Data = null };
                        dataPdfTrustCA.Llx = profile.LLX.ToString();
                        dataPdfTrustCA.Lly = profile.LLY.ToString();
                        dataPdfTrustCA.Urx = profile.URX.ToString();
                        dataPdfTrustCA.Ury = profile.URY.ToString();
                    }
                    dataPdfTrustCA.Location = model.Location;
                    dataPdfTrustCA.Reason = model.Reason;
                    dataPdfTrustCA.Page = model.Page;
                    dataPdfTrustCA.ContactInfo = model.ContactInfo;
                    dataPdfTrustCA.FileInfo = model.FileInfo;

                    #region Khởi tạo thư mục
                    string partitionPhysicalPath;
                    string destinationPhysicalPath = "";
                    partitionPhysicalPath = Path.Combine(Utils.GetConfig("StaticFiles:Folder"));
                    // Get root path
                    var rootPath = partitionPhysicalPath;
                    Log.Debug("Upload root path : " + rootPath);
                    // Append destinationPhysicalPath
                    if (string.IsNullOrEmpty(destinationPhysicalPath))
                    {
                        destinationPhysicalPath = ParameterCollection.Instance.GetValue("DefaultAlias");
                        destinationPhysicalPath = destinationPhysicalPath + "\\" +
                                                  DateTime.Now.ToString("yyyy") + "\\" + DateTime.Now.ToString("MM") +
                                                  "\\" +
                                                  DateTime.Now.ToString("dd") + "\\";
                        bool isWindows = RuntimeInformation.IsOSPlatform(OSPlatform.Windows);
                        if (!isWindows)
                        {
                            destinationPhysicalPath = destinationPhysicalPath.Replace("\\", "/");
                        }
                    }
                    // Create folder path
                    var sourcefolder = destinationPhysicalPath;
                    var isExistDirectory = Directory.Exists(Path.Combine(rootPath, sourcefolder));
                    if (!isExistDirectory) Directory.CreateDirectory(Path.Combine(rootPath, sourcefolder));
                    var fullfolderPath = Path.Combine(rootPath, sourcefolder);
                    #endregion                    

                    #region Ký PDF
                    using (var httpClientPdf = new HttpClient())
                    {
                        var byteArray = Encoding.ASCII.GetBytes("" + basic_user + ":" + basic_pass + "");
                        httpClientPdf.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));
                        var jsonString_pdf = JsonConvert.SerializeObject(dataPdfTrustCA);
                        var content_pdf = new StringContent(jsonString_pdf, Encoding.UTF8);
                        content_pdf.Headers.ContentType.MediaType = "application/json";
                        content_pdf.Headers.ContentType.CharSet = "UTF-8";
                        HttpResponseMessage responsePdf = await httpClientPdf.PostAsync(apiSignPdf, content_pdf);
                        string sdPdf = responsePdf.Content.ReadAsStringAsync().Result;
                        var rsSign = JsonConvert.DeserializeObject<OutputFromSignMultiTrustCA>(sdPdf);
                        if (rsSign.Code == 1 && rsSign.Data.Count > 0)
                        {
                            dataReturn.Code = rsSign.Code;
                            dataReturn.Message = rsSign.Message;
                            List<DataFileSignTrustCAFormat> dataFileReturn = new List<DataFileSignTrustCAFormat>();

                            //rsSign.MST = DateTime.Now.ToString("dd/MM/yyyy HH:mm");
                            #region Tải file đã ký về  và trả cho thirdparty nếu thành công

                            foreach (var item in rsSign.Data)
                            {
                                var f = model.FileInfo.FirstOrDefault(x => x.FileId == item.FileId);
                                if (f != null)
                                {
                                    string fileContentBase64 = item.OuputFileContentBase64;
                                    var fileSavePath = Path.Combine(fullfolderPath, Guid.NewGuid() + "_file_signed.pdf");
                                    File.WriteAllBytes(@"" + fileSavePath + "", Convert.FromBase64String(fileContentBase64));
                                    var fileInfo = new FileInfo(fileSavePath);
                                    string fileSignedPath = Path.Combine(sourcefolder, fileInfo.Name).Replace("\\", "/");
                                    string fileUrl = new Uri(contentUrl + fileSignedPath).ToString();
                                    item.FileUrl = fileUrl;
                                    //item.OuputFileContentBase64 = "";
                                    dataFileReturn.Add(new DataFileSignTrustCAFormat() { FileId = item.FileId, FileUrl = fileUrl, FileBase64Content = item.OuputFileContentBase64 });
                                    dataReturn.Data = dataFileReturn;
                                    //Ghi log
                                    IdmHelper.AddSignLog(unitOfWork, model.ApplicationId, userId.ToString(), certifcate.CertId.Value, rsSign.Code + "", "Ký thành công", item.FileId);
                                }
                                else
                                {
                                    //Ghi log
                                    IdmHelper.AddSignLog(unitOfWork, model.ApplicationId, userId.ToString(), certifcate.CertId.Value, rsSign.Code + "", "Không tìm thấy dữ liệu file ký", item.FileId);
                                }
                            }
                            if (user != null)
                            {
                                //rsSign.TenDonVi = JsonConvert.SerializeObject(dataPdfTrustCA);
                            }
                            return dataReturn;
                            #endregion
                        }
                        else
                        {
                            foreach (var f in model.FileInfo)
                            {
                                //Ghi log
                                IdmHelper.AddSignLog(unitOfWork, model.ApplicationId, userId.ToString(), certifcate.CertId.Value, rsSign.Code + "", "Ký không thành công: " + rsSign.Message, f.FileId);
                            }
                            // rsSign.TenDonVi = JsonConvert.SerializeObject(dataPdfTrustCA);
                            dataReturn.Code = rsSign.Code;
                            dataReturn.Message = rsSign.Message;
                            return dataReturn;
                        }
                    }
                    #endregion                    
                }
            }
            catch (Exception ex)
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    //Ghi log
                    IdmHelper.AddSignLog(unitOfWork, model.ApplicationId, userId.ToString(), Guid.Empty, "-1", "Lỗi ngoại lệ SignUrlPdfForThirdparty: " + ex.Message + " Model: " + JsonConvert.SerializeObject(model), "");
                }
                return new OutputFromSignMultiTrustCAFormat() { Code = -1, Message = "Lỗi ngoại lệ: " + ex.Message, Data = null };
            }
        }

        /// <summary>
        /// Ký với service HSM của Newwave (CTS trên HSM) danh cho phần mềm khác kết nối, dữ liệu đã ký base64 từ KMS
        /// </summary>
        /// <param name="model"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<OutputFromSignMultiTrustCAFormat> SignUrlPdfForThirdpartyFromFileContentBase64(DataInputSignPDFnCipher model, Guid userId)
        {
            try
            {
                OutputFromSignMultiTrustCAFormat dataReturn = new OutputFromSignMultiTrustCAFormat();
                using (var unitOfWork = new UnitOfWork())
                {
                    var user = unitOfWork.GetRepository<IdmUser>().Find(x => x.Id == userId);
                    if (user == null)
                        return new OutputFromSignMultiTrustCAFormat() { Code = 0, Message = "Không tìm thấy thông tin tài khoản", Data = null };
                    FileOuputModel dataOut = new FileOuputModel();
                    string basic_user = Utils.GetConfig("Sign:Basic_User");
                    string basic_pass = Utils.GetConfig("Sign:Basic_Pass");
                    string apiSignPdf = Utils.GetConfig("Sign:ApiUrl_V3");
                    string contentUrl = Utils.GetConfig("AppSettings:Host") + "StaticFiles/";
                    DataInputSignPDFTrustCA dataPdfTrustCA = new DataInputSignPDFTrustCA();
                    if (string.IsNullOrEmpty(model.Image))
                    {
                        string signImage = Utils.GetConfig("StaticFiles:Folder") + "/sign/sign-default.png";
                        if (!string.IsNullOrEmpty(user.SignFinalPath))
                        {
                            signImage = user.SignFinalPath.Replace("{{API_STATIC_FILE}}", Utils.GetConfig("StaticFiles:Folder") + "/");
                        }
                        byte[] arrByteImage = File.ReadAllBytes(signImage);
                        model.Image = Convert.ToBase64String(arrByteImage);
                    }
                    dataPdfTrustCA.Image = model.Image;
                    dataPdfTrustCA.IsVisible = model.IsVisible;
                    dataPdfTrustCA.Llx = model.Llx;
                    dataPdfTrustCA.Lly = model.Lly;
                    dataPdfTrustCA.Urx = model.Urx;
                    dataPdfTrustCA.Ury = model.Ury;
                    if (!string.IsNullOrWhiteSpace(model.Profile))
                    {
                        var profile = unitOfWork.GetRepository<CmsSignProfile>().Find(x => x.Code == model.Profile);
                        if (profile == null)
                            return new OutputFromSignMultiTrustCAFormat() { Code = 0, Message = "Không tìm thấy thông tin profile: " + model.Profile, Data = null };
                        dataPdfTrustCA.Llx = profile.LLX.ToString();
                        dataPdfTrustCA.Lly = profile.LLY.ToString();
                        dataPdfTrustCA.Urx = profile.URX.ToString();
                        dataPdfTrustCA.Ury = profile.URY.ToString();
                    }
                    dataPdfTrustCA.Location = model.Location;
                    dataPdfTrustCA.Reason = model.Reason;
                    dataPdfTrustCA.Page = model.Page;
                    dataPdfTrustCA.ContactInfo = model.ContactInfo;
                    dataPdfTrustCA.FileInfo = model.FileInfo;

                    #region Khởi tạo thư mục
                    string partitionPhysicalPath;
                    string destinationPhysicalPath = "";
                    partitionPhysicalPath = Path.Combine(Utils.GetConfig("StaticFiles:Folder"));
                    // Get root path
                    var rootPath = partitionPhysicalPath;
                    Log.Debug("Upload root path : " + rootPath);
                    // Append destinationPhysicalPath
                    if (string.IsNullOrEmpty(destinationPhysicalPath))
                    {
                        destinationPhysicalPath = ParameterCollection.Instance.GetValue("DefaultAlias");
                        destinationPhysicalPath = destinationPhysicalPath + "\\" +
                                                  DateTime.Now.ToString("yyyy") + "\\" + DateTime.Now.ToString("MM") +
                                                  "\\" +
                                                  DateTime.Now.ToString("dd") + "\\";
                        bool isWindows = RuntimeInformation.IsOSPlatform(OSPlatform.Windows);
                        if (!isWindows)
                        {
                            destinationPhysicalPath = destinationPhysicalPath.Replace("\\", "/");
                        }
                    }
                    // Create folder path
                    var sourcefolder = destinationPhysicalPath;
                    var isExistDirectory = Directory.Exists(Path.Combine(rootPath, sourcefolder));
                    if (!isExistDirectory) Directory.CreateDirectory(Path.Combine(rootPath, sourcefolder));
                    var fullfolderPath = Path.Combine(rootPath, sourcefolder);
                    #endregion

                    //Lấy thông tin cert theo user + key
                    var certifcate = new CertificateSignModel();
                    certifcate = GetCertificateV2(model.Key, userId);

                    if (certifcate == null)
                        return new OutputFromSignMultiTrustCAFormat() { Code = 0, Message = "Thông tin key không hợp lệ", Data = null };

                    dataPdfTrustCA.SlotLabel = certifcate.SlotLabel;
                    dataPdfTrustCA.UserPin = certifcate.UserPin;
                    dataPdfTrustCA.Alias = certifcate.Alias;
                    //dataPdfTrustCA.CertificateFileName = certifcate.CertificateFileName;
                    dataPdfTrustCA.CertString = certifcate.CertString;

                    #region Ký PDF
                    using (var httpClientPdf = new HttpClient())
                    {
                        var byteArray = Encoding.ASCII.GetBytes("" + basic_user + ":" + basic_pass + "");
                        httpClientPdf.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));
                        var jsonString_pdf = JsonConvert.SerializeObject(dataPdfTrustCA);
                        var content_pdf = new StringContent(jsonString_pdf, Encoding.UTF8);
                        content_pdf.Headers.ContentType.MediaType = "application/json";
                        content_pdf.Headers.ContentType.CharSet = "UTF-8";
                        HttpResponseMessage responsePdf = await httpClientPdf.PostAsync(apiSignPdf, content_pdf);
                        string sdPdf = responsePdf.Content.ReadAsStringAsync().Result;
                        var rsSign = JsonConvert.DeserializeObject<OutputFromSignMultiTrustCA>(sdPdf);
                        if (rsSign.Code == 1 && rsSign.Data.Count > 0)
                        {
                            dataReturn.Code = rsSign.Code;
                            dataReturn.Message = rsSign.Message;
                            List<DataFileSignTrustCAFormat> dataFileReturn = new List<DataFileSignTrustCAFormat>();

                            //rsSign.MST = DateTime.Now.ToString("dd/MM/yyyy HH:mm");
                            #region Tải file đã ký về  và trả cho thirdparty nếu thành công

                            foreach (var item in rsSign.Data)
                            {
                                var f = model.FileInfo.FirstOrDefault(x => x.FileId == item.FileId);
                                if (f != null)
                                {
                                    string fileContentBase64 = item.OuputFileContentBase64;
                                    var fileSavePath = Path.Combine(fullfolderPath, "file_signed.pdf");
                                    File.WriteAllBytes(@"" + fileSavePath + "", Convert.FromBase64String(fileContentBase64));
                                    var fileInfo = new FileInfo(fileSavePath);
                                    string fileSignedPath = Path.Combine(sourcefolder, fileInfo.Name).Replace("\\", "/");
                                    string fileUrl = new Uri(contentUrl + fileSignedPath).ToString();
                                    item.FileUrl = fileUrl;
                                    item.OuputFileContentBase64 = "";
                                    dataFileReturn.Add(new DataFileSignTrustCAFormat() { FileId = item.FileId, FileUrl = fileUrl });
                                    dataReturn.Data = dataFileReturn;
                                    //Ghi log
                                    IdmHelper.AddSignLog(unitOfWork, model.ApplicationId, userId.ToString(), certifcate.CertId.Value, rsSign.Code + "", "Ký thành công", item.FileId);
                                }
                                else
                                {
                                    //Ghi log
                                    IdmHelper.AddSignLog(unitOfWork, model.ApplicationId, userId.ToString(), certifcate.CertId.Value, rsSign.Code + "", "Không tìm thấy dữ liệu file ký", item.FileId);
                                }
                            }
                            if (user != null)
                            {
                                //rsSign.TenDonVi = JsonConvert.SerializeObject(dataPdfTrustCA);
                            }
                            return dataReturn;
                            #endregion
                        }
                        else
                        {
                            foreach (var f in model.FileInfo)
                            {
                                //Ghi log
                                IdmHelper.AddSignLog(unitOfWork, model.ApplicationId, userId.ToString(), certifcate.CertId.Value, rsSign.Code + "", "Ký không thành công: " + rsSign.Message, f.FileId);
                            }
                            // rsSign.TenDonVi = JsonConvert.SerializeObject(dataPdfTrustCA);
                            return dataReturn;
                        }
                    }
                    #endregion                    
                }
            }
            catch (Exception ex)
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    //Ghi log
                    IdmHelper.AddSignLog(unitOfWork, model.ApplicationId, userId.ToString(), Guid.Empty, "-1", "Lỗi ngoại lệ SignUrlPdfForThirdparty: " + ex.Message + " Model: " + JsonConvert.SerializeObject(model), "");
                }
                return new OutputFromSignMultiTrustCAFormat() { Code = -1, Message = "Lỗi ngoại lệ: " + ex.Message, Data = null };
            }
        }

        /// <summary>
        /// Ký với service HSM của Newwave (CTS trên HSM) danh cho phần mềm khác kết nối with link file
        /// </summary>
        /// <param name="model"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<OutputFromSignMultiTrustCA> SignUrlXMLForThirdparty(DataInputSignXMLnCipher model, Guid userId)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    var org = (from ou in unitOfWork.GetRepository<IdmUserMapOrg>().GetAll()
                               join o in unitOfWork.GetRepository<CmsOrganization>().GetAll()
                               on ou.OrganizationId equals o.Id
                               where ou.UserId == userId
                               select o).FirstOrDefault();
                    FileOuputModel dataOut = new FileOuputModel();
                    string basic_user = Utils.GetConfig("Sign:Basic_User");
                    string basic_pass = Utils.GetConfig("Sign:Basic_Pass");
                    string apiSignPdf = Utils.GetConfig("Sign:ApiUrl_V3_XML");
                    string contentUrl = Utils.GetConfig("AppSettings:Host") + "StaticFiles/";
                    DataInputSignXMLTrustCA dataxmlTrustCA = new DataInputSignXMLTrustCA();
                    dataxmlTrustCA.FileInfo = model.FileInfo;

                    #region Khởi tạo thư mục
                    string partitionPhysicalPath;
                    string destinationPhysicalPath = "";
                    partitionPhysicalPath = Path.Combine(Utils.GetConfig("StaticFiles:Folder"));
                    // Get root path
                    var rootPath = partitionPhysicalPath;
                    Log.Debug("Upload root path : " + rootPath);
                    // Append destinationPhysicalPath
                    if (string.IsNullOrEmpty(destinationPhysicalPath))
                    {
                        destinationPhysicalPath = ParameterCollection.Instance.GetValue("DefaultAlias");
                        destinationPhysicalPath = destinationPhysicalPath + "\\" +
                                                  DateTime.Now.ToString("yyyy") + "\\" + DateTime.Now.ToString("MM") +
                                                  "\\" +
                                                  DateTime.Now.ToString("dd") + "\\";
                        bool isWindows = RuntimeInformation.IsOSPlatform(OSPlatform.Windows);
                        if (!isWindows)
                        {
                            destinationPhysicalPath = destinationPhysicalPath.Replace("\\", "/");
                        }
                    }
                    // Create folder path
                    var sourcefolder = destinationPhysicalPath;
                    var isExistDirectory = Directory.Exists(Path.Combine(rootPath, sourcefolder));
                    if (!isExistDirectory) Directory.CreateDirectory(Path.Combine(rootPath, sourcefolder));
                    var fullfolderPath = Path.Combine(rootPath, sourcefolder);
                    #endregion

                    //Lấy thông tin cert theo user + key
                    var certifcate = new CertificateSignModel();
                    certifcate = GetCertificateV2(model.Key, userId);

                    if (certifcate == null)
                        return new OutputFromSignMultiTrustCA() { Code = 0, Message = "Thông tin key không hợp lệ", Data = null };

                    dataxmlTrustCA.SlotLabel = certifcate.SlotLabel;
                    dataxmlTrustCA.UserPin = certifcate.UserPin;
                    dataxmlTrustCA.Alias = certifcate.Alias;
                    // dataxmlTrustCA.CertificateFileName = certifcate.CertificateFileName;
                    dataxmlTrustCA.CertString = certifcate.CertString;

                    #region Ký 
                    using (var httpClientPdf = new HttpClient())
                    {
                        var byteArray = Encoding.ASCII.GetBytes("" + basic_user + ":" + basic_pass + "");
                        httpClientPdf.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));
                        var jsonString_pdf = JsonConvert.SerializeObject(dataxmlTrustCA);
                        var content_pdf = new StringContent(jsonString_pdf, Encoding.UTF8);
                        content_pdf.Headers.ContentType.MediaType = "application/json";
                        content_pdf.Headers.ContentType.CharSet = "UTF-8";
                        HttpResponseMessage responsePdf = await httpClientPdf.PostAsync(apiSignPdf, content_pdf);
                        string sdPdf = responsePdf.Content.ReadAsStringAsync().Result;
                        var rsSign = JsonConvert.DeserializeObject<OutputFromSignMultiTrustCA>(sdPdf);
                        if (rsSign.Code == 1 && rsSign.Data.Count > 0)
                        {
                            #region Tải file đã ký về  và trả cho thirdparty nếu thành công
                            using (var client = new WebClient())
                            {
                                string credentials = Convert.ToBase64String(Encoding.ASCII.GetBytes("" + basic_user + "" + ":" + "" + basic_pass + ""));
                                client.Headers[HttpRequestHeader.Authorization] = string.Format("Basic {0}", credentials);

                                foreach (var item in rsSign.Data)
                                {
                                    var f = model.FileInfo.FirstOrDefault(x => x.FileId == item.FileId);
                                    if (f != null)
                                    {
                                        string linkdownload = item.FileUrl;
                                        var fileByteResponse = client.DownloadData(linkdownload);
                                        var fileSavePath = Path.Combine(fullfolderPath, "file_signed.pdf");
                                        File.WriteAllBytes(fileSavePath, fileByteResponse);
                                        var fileInfo = new FileInfo(fileSavePath);
                                        string fileSignedPath = Path.Combine(sourcefolder, fileInfo.Name).Replace("\\", "/");
                                        string fileUrl = new Uri(contentUrl + fileSignedPath).ToString();
                                        item.FileUrl = fileUrl;
                                        //Ghi log
                                        IdmHelper.AddSignLog(unitOfWork, model.ApplicationId, userId.ToString(), certifcate.CertId.Value, rsSign.Code + "", "Ký thành công", item.FileId);
                                    }
                                }
                            }
                            if (org != null)
                            {
                                rsSign.MST = org.Code;
                                rsSign.TenDonVi = org.Name;
                            }
                            return rsSign;
                            #endregion
                        }
                        else
                        {
                            foreach (var f in model.FileInfo)
                            {
                                //Ghi log
                                IdmHelper.AddSignLog(unitOfWork, model.ApplicationId, userId.ToString(), certifcate.CertId.Value, rsSign.Code + "", "Ký không thành công: " + rsSign.Message, f.FileId);
                            }
                            return rsSign;
                        }
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    //Ghi log
                    IdmHelper.AddSignLog(unitOfWork, model.ApplicationId, userId.ToString(), Guid.Empty, "-1", "Lỗi ngoại lệ SignUrlXMLForThirdparty: " + ex.Message + " Model: " + JsonConvert.SerializeObject(model), "");
                }
                return new OutputFromSignMultiTrustCA() { Code = -1, Message = "Lỗi ngoại lệ: " + ex.Message, Data = null };
            }
        }

        #region Check service live anh switch server 45, 46
        public bool IsUrlValid(System.Threading.AutoResetEvent autores, string url, int timeout, ref double timeRequest, ref string message)
        {
            var Pattern = @"^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$";
            var Rgx = new Regex(Pattern, RegexOptions.Compiled | RegexOptions.IgnoreCase);
            if (Rgx.IsMatch(url))
            {
                var dateTime = DateTime.Now;
                try
                {
                    string basic_user = Utils.GetConfig("Sign:Basic_User");
                    string basic_pass = Utils.GetConfig("Sign:Basic_Pass");
                    Uri urlCheck = new Uri(url);
                    WebRequest request = WebRequest.Create(urlCheck);

                    NetworkCredential myNetworkCredential = new NetworkCredential(basic_user, basic_pass);

                    CredentialCache myCredentialCache = new CredentialCache();
                    myCredentialCache.Add(urlCheck, "Basic", myNetworkCredential);

                    request.PreAuthenticate = true;
                    request.Credentials = myCredentialCache;

                    request.Timeout = timeout;
                    WebResponse response;

                    try
                    {
                        response = request.GetResponse();
                        timeRequest = (DateTime.Now - dateTime).TotalMilliseconds;
                    }
                    catch (Exception ex)
                    {
                        timeRequest = (DateTime.Now - dateTime).TotalMilliseconds;
                        message = ex.Message;
                        if (ex.InnerException == null)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    string responseURL = response.ResponseUri.ToString();
                    if (string.Compare(responseURL, urlCheck.ToString(), true) != 0) //it was redirected, check to see if redirected to error page
                    {
                        var result = !(responseURL.IndexOf("404.php") > -1 ||
                              responseURL.IndexOf("500.php") > -1 ||
                              responseURL.IndexOf("404.htm") > -1 ||
                              responseURL.IndexOf("500.htm") > -1);
                        if (!result)
                            message = "Page error";
                        return result;
                    }
                    else
                    {
                        message = "Success";
                        return true;
                    }
                }
                catch (Exception ex)
                {
                    timeRequest = (DateTime.Now - dateTime).TotalMilliseconds;
                    message = ex.ToString();
                    return false;
                }
            }
            else
            {
                message = "The URL '" + url + "' is incorrect";
                return false;
            }
        }
        public async Task<OutputFromSignMultiTrustCA> CheckAndSetLinkSign(DataInputSignXMLnCipherV2 model, Guid userId)
        {
            try
            {
                //Main 45
                string apiSign = Utils.GetConfig("Sign:ApiUrl_V3_XML");
                //Extra 46
                string apiSign2 = Utils.GetConfig("Sign:ApiUrl_V3_XML_2");

                System.Threading.AutoResetEvent autores = new System.Threading.AutoResetEvent(false);
                double thoiGianPhanHoi = -1;
                var mota = string.Empty;
                bool rs = IsUrlValid(autores, apiSign, 5000, ref thoiGianPhanHoi, ref mota);

                if (rs == true)
                {
                    var rsSign = await SignUrlXMLForThirdpartyV2FromUrl(model, userId);
                    if (rsSign.Code != 1)
                    {
                        return await SignUrlXMLForThirdpartyV2ExtraFromUrl(model, userId);
                    }
                    else
                    {
                        return rsSign;
                    }
                }
                else
                {
                    return await SignUrlXMLForThirdpartyV2ExtraFromUrl(model, userId);
                }
            }
            catch
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    //Ghi log
                    IdmHelper.AddSignLog(unitOfWork, model.ApplicationId, userId.ToString(), Guid.Empty, "-1", "Lỗi ngoại lệ: Không kết nối được dịch vụ ký HSM - Model: " + JsonConvert.SerializeObject(model), "");
                }
                return new OutputFromSignMultiTrustCA() { Code = -1, Message = "Lỗi ngoại lệ: Không kết nối được dịch vụ ký HSM", Data = null };
            }
        }
        #endregion

        /// <summary>
        /// Ký với service HSM của Newwave (CTS trên HSM) danh cho phần mềm khác kết nối với with file base 64 content
        /// </summary>
        /// <param name="model"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<OutputFromSignMultiTrustCA> SignUrlXMLForThirdpartyV2FromUrl(DataInputSignXMLnCipherV2 model, Guid userId)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    var org = (from ou in unitOfWork.GetRepository<IdmUserMapOrg>().GetAll()
                               join o in unitOfWork.GetRepository<CmsOrganization>().GetAll()
                               on ou.OrganizationId equals o.Id
                               where ou.UserId == userId
                               select o).FirstOrDefault();

                    FileOuputModel dataOut = new FileOuputModel();
                    string basic_user = Utils.GetConfig("Sign:Basic_User");
                    string basic_pass = Utils.GetConfig("Sign:Basic_Pass");
                    //Main 45
                    string apiSign = Utils.GetConfig("Sign:ApiUrl_V3_XML");
                    string contentUrl = Utils.GetConfig("AppSettings:Host") + "StaticFiles/";
                    DataInputSignXMLTrustCA dataxmlTrustCA = new DataInputSignXMLTrustCA();
                    List<DataFileSignTrustCA> fileInfoXML = new List<DataFileSignTrustCA>();

                    #region Khởi tạo thư mục
                    string partitionPhysicalPath;
                    string destinationPhysicalPath = "";
                    partitionPhysicalPath = Path.Combine(Utils.GetConfig("StaticFiles:Folder"));
                    // Get root path
                    var rootPath = partitionPhysicalPath;
                    Log.Debug("Upload root path : " + rootPath);
                    // Append destinationPhysicalPath
                    if (string.IsNullOrEmpty(destinationPhysicalPath))
                    {
                        destinationPhysicalPath = ParameterCollection.Instance.GetValue("DefaultAlias");
                        destinationPhysicalPath = destinationPhysicalPath + "\\" +
                                                  DateTime.Now.ToString("yyyy") + "\\" + DateTime.Now.ToString("MM") +
                                                  "\\" +
                                                  DateTime.Now.ToString("dd") + "\\";
                        bool isWindows = RuntimeInformation.IsOSPlatform(OSPlatform.Windows);
                        if (!isWindows)
                        {
                            destinationPhysicalPath = destinationPhysicalPath.Replace("\\", "/");
                        }
                    }
                    // Create folder path
                    var sourcefolder = destinationPhysicalPath;
                    var isExistDirectory = Directory.Exists(Path.Combine(rootPath, sourcefolder));
                    if (!isExistDirectory) Directory.CreateDirectory(Path.Combine(rootPath, sourcefolder));
                    var fullfolderPath = Path.Combine(rootPath, sourcefolder);
                    #endregion

                    #region Convert model để gửi sang service sign
                    if (model.FileInfo != null && model.FileInfo.Count > 0)
                    {
                        foreach (var item in model.FileInfo)
                        {
                            #region Lưu file base64 rồi lấy link gửi đi
                            string filename = item.FileName;
                            var fileSavePath = Path.Combine(fullfolderPath, filename);
                            File.WriteAllBytes(@"" + fileSavePath + "", Convert.FromBase64String(item.FileContentBase64));
                            var fileInfo = new FileInfo(fileSavePath);
                            string physicalPath = Path.Combine(sourcefolder, fileInfo.Name).Replace("\\", "/");
                            string fileUrl = new Uri(contentUrl + physicalPath).ToString();
                            //string fileUrl = "http://web.signing.gosign.vn/StaticFiles/wf/hoa_don_test.xml";
                            #endregion
                            fileInfoXML.Add(new DataFileSignTrustCA() { FileId = item.FileId, FileUrl = fileUrl });
                        }
                    }
                    dataxmlTrustCA.FileInfo = fileInfoXML;

                    //Lấy thông tin cert theo user + key
                    var certifcate = new CertificateSignModel();
                    certifcate = GetCertificateV2(model.Key, userId);

                    if (certifcate == null)
                        return new OutputFromSignMultiTrustCA() { Code = 0, Message = "Thông tin key không hợp lệ", Data = null };

                    dataxmlTrustCA.SlotLabel = certifcate.SlotLabel;
                    dataxmlTrustCA.UserPin = certifcate.UserPin;
                    dataxmlTrustCA.Alias = certifcate.Alias;
                    //dataxmlTrustCA.CertificateFileName = certifcate.CertificateFileName;
                    dataxmlTrustCA.CertString = certifcate.CertString;
                    #endregion

                    #region Ký XML
                    using (var httpClient = new HttpClient())
                    {
                        var byteArray = Encoding.ASCII.GetBytes("" + basic_user + ":" + basic_pass + "");
                        httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));
                        var jsonString = JsonConvert.SerializeObject(dataxmlTrustCA);
                        var content = new StringContent(jsonString, Encoding.UTF8);
                        content.Headers.ContentType.MediaType = "application/json";
                        content.Headers.ContentType.CharSet = "UTF-8";
                        HttpResponseMessage responsePdf = await httpClient.PostAsync(apiSign, content);
                        string sdPdf = responsePdf.Content.ReadAsStringAsync().Result;
                        var rsSign = JsonConvert.DeserializeObject<OutputFromSignMultiTrustCA>(sdPdf);

                        if (rsSign.Code == 1 && rsSign.Data.Count > 0)
                        {
                            #region Tải file đã ký về  và trả cho thirdparty nếu thành công
                            using (var client = new WebClient())
                            {
                                string credentials = Convert.ToBase64String(Encoding.ASCII.GetBytes("" + basic_user + "" + ":" + "" + basic_pass + ""));
                                client.Headers[HttpRequestHeader.Authorization] = string.Format("Basic {0}", credentials);

                                foreach (var item in rsSign.Data)
                                {
                                    var f = model.FileInfo.FirstOrDefault(x => x.FileId == item.FileId);
                                    if (f != null)
                                    {
                                        string linkdownload = item.FileUrl;
                                        var fileByteResponse = client.DownloadData(linkdownload);
                                        var fileSavePath = Path.Combine(fullfolderPath, "signed_" + f.FileName);
                                        File.WriteAllBytes(fileSavePath, fileByteResponse);
                                        var fileInfo = new FileInfo(fileSavePath);
                                        string fileSignedPath = Path.Combine(sourcefolder, fileInfo.Name).Replace("\\", "/");
                                        string fileUrl = new Uri(contentUrl + fileSignedPath).ToString();
                                        item.FileUrl = fileUrl;
                                        //Ghi log
                                        IdmHelper.AddSignLog(unitOfWork, model.ApplicationId, userId.ToString(), certifcate.CertId.Value, rsSign.Code + "", "Ký thành công", item.FileId);
                                    }
                                }
                            }
                            if (org != null)
                            {
                                rsSign.MST = org.Code;
                                rsSign.TenDonVi = org.Name;
                            }
                            return rsSign;
                            #endregion
                        }
                        else
                        {
                            foreach (var f in model.FileInfo)
                            {
                                //Ghi log
                                IdmHelper.AddSignLog(unitOfWork, model.ApplicationId, userId.ToString(), certifcate.CertId.Value, rsSign.Code + "", "Ký HSM không thành công: " + sdPdf, f.FileId);
                            }
                            return rsSign;
                        }
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    //Ghi log
                    IdmHelper.AddSignLog(unitOfWork, model.ApplicationId, userId.ToString(), Guid.Empty, "-1", "Lỗi ngoại lệ Exception: " + ex.Message + " Model: " + JsonConvert.SerializeObject(model), "");
                }
                return new OutputFromSignMultiTrustCA() { Code = -1, Message = "Lỗi ngoại lệ: " + ex.Message, Data = null };
            }
        }

        /// <summary>
        /// Ký với service HSM của Newwave (CTS trên HSM) danh cho phần mềm khác kết nối với (máy dự phòng) with file base 64 content
        /// </summary>
        /// <param name="model"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<OutputFromSignMultiTrustCA> SignUrlXMLForThirdpartyV2ExtraFromUrl(DataInputSignXMLnCipherV2 model, Guid userId)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    var org = (from ou in unitOfWork.GetRepository<IdmUserMapOrg>().GetAll()
                               join o in unitOfWork.GetRepository<CmsOrganization>().GetAll()
                               on ou.OrganizationId equals o.Id
                               where ou.UserId == userId
                               select o).FirstOrDefault();

                    FileOuputModel dataOut = new FileOuputModel();
                    string basic_user = Utils.GetConfig("Sign:Basic_User");
                    string basic_pass = Utils.GetConfig("Sign:Basic_Pass");
                    //Main 46
                    string apiSign = Utils.GetConfig("Sign:ApiUrl_V3_XML_2");
                    string contentUrl = Utils.GetConfig("AppSettings:Host") + "StaticFiles/";
                    DataInputSignXMLTrustCA dataxmlTrustCA = new DataInputSignXMLTrustCA();
                    List<DataFileSignTrustCA> fileInfoXML = new List<DataFileSignTrustCA>();

                    #region Khởi tạo thư mục
                    string partitionPhysicalPath;
                    string destinationPhysicalPath = "";
                    partitionPhysicalPath = Path.Combine(Utils.GetConfig("StaticFiles:Folder"));
                    // Get root path
                    var rootPath = partitionPhysicalPath;
                    Log.Debug("Upload root path : " + rootPath);
                    // Append destinationPhysicalPath
                    if (string.IsNullOrEmpty(destinationPhysicalPath))
                    {
                        destinationPhysicalPath = ParameterCollection.Instance.GetValue("DefaultAlias");
                        destinationPhysicalPath = destinationPhysicalPath + "\\" +
                                                  DateTime.Now.ToString("yyyy") + "\\" + DateTime.Now.ToString("MM") +
                                                  "\\" +
                                                  DateTime.Now.ToString("dd") + "\\";
                        bool isWindows = RuntimeInformation.IsOSPlatform(OSPlatform.Windows);
                        if (!isWindows)
                        {
                            destinationPhysicalPath = destinationPhysicalPath.Replace("\\", "/");
                        }
                    }
                    // Create folder path
                    var sourcefolder = destinationPhysicalPath;
                    var isExistDirectory = Directory.Exists(Path.Combine(rootPath, sourcefolder));
                    if (!isExistDirectory) Directory.CreateDirectory(Path.Combine(rootPath, sourcefolder));
                    var fullfolderPath = Path.Combine(rootPath, sourcefolder);
                    #endregion

                    #region Convert model để gửi sang service sign
                    if (model.FileInfo != null && model.FileInfo.Count > 0)
                    {
                        foreach (var item in model.FileInfo)
                        {
                            #region Lưu file base64 rồi lấy link gửi đi
                            string filename = item.FileName;
                            var fileSavePath = Path.Combine(fullfolderPath, filename);
                            File.WriteAllBytes(@"" + fileSavePath + "", Convert.FromBase64String(item.FileContentBase64));
                            var fileInfo = new FileInfo(fileSavePath);
                            string physicalPath = Path.Combine(sourcefolder, fileInfo.Name).Replace("\\", "/");
                            string fileUrl = new Uri(contentUrl + physicalPath).ToString();
                            //string fileUrl = "http://web.signing.gosign.vn/StaticFiles/wf/hoa_don_test.xml";
                            #endregion
                            fileInfoXML.Add(new DataFileSignTrustCA() { FileId = item.FileId, FileUrl = fileUrl });
                        }
                    }
                    dataxmlTrustCA.FileInfo = fileInfoXML;

                    //Lấy thông tin cert theo user + key
                    var certifcate = new CertificateSignModel();
                    certifcate = GetCertificateV2(model.Key, userId);

                    if (certifcate == null)
                        return new OutputFromSignMultiTrustCA() { Code = 0, Message = "Thông tin key không hợp lệ", Data = null };

                    dataxmlTrustCA.SlotLabel = certifcate.SlotLabel;
                    dataxmlTrustCA.UserPin = certifcate.UserPin;
                    dataxmlTrustCA.Alias = certifcate.Alias;
                    //dataxmlTrustCA.CertificateFileName = certifcate.CertificateFileName;
                    dataxmlTrustCA.CertString = certifcate.CertString;
                    #endregion

                    #region Ký XML
                    using (var httpClient = new HttpClient())
                    {
                        var byteArray = Encoding.ASCII.GetBytes("" + basic_user + ":" + basic_pass + "");
                        httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));
                        var jsonString = JsonConvert.SerializeObject(dataxmlTrustCA);
                        var content = new StringContent(jsonString, Encoding.UTF8);
                        content.Headers.ContentType.MediaType = "application/json";
                        content.Headers.ContentType.CharSet = "UTF-8";
                        HttpResponseMessage responsePdf = await httpClient.PostAsync(apiSign, content);
                        string sdPdf = responsePdf.Content.ReadAsStringAsync().Result;
                        var rsSign = JsonConvert.DeserializeObject<OutputFromSignMultiTrustCA>(sdPdf);

                        if (rsSign.Code == 1 && rsSign.Data.Count > 0)
                        {
                            #region Tải file đã ký về  và trả cho thirdparty nếu thành công
                            using (var client = new WebClient())
                            {
                                string credentials = Convert.ToBase64String(Encoding.ASCII.GetBytes("" + basic_user + "" + ":" + "" + basic_pass + ""));
                                client.Headers[HttpRequestHeader.Authorization] = string.Format("Basic {0}", credentials);

                                foreach (var item in rsSign.Data)
                                {
                                    var f = model.FileInfo.FirstOrDefault(x => x.FileId == item.FileId);
                                    if (f != null)
                                    {
                                        string linkdownload = item.FileUrl;
                                        var fileByteResponse = client.DownloadData(linkdownload);
                                        var fileSavePath = Path.Combine(fullfolderPath, "signed_" + f.FileName);
                                        File.WriteAllBytes(fileSavePath, fileByteResponse);
                                        var fileInfo = new FileInfo(fileSavePath);
                                        string fileSignedPath = Path.Combine(sourcefolder, fileInfo.Name).Replace("\\", "/");
                                        string fileUrl = new Uri(contentUrl + fileSignedPath).ToString();
                                        item.FileUrl = fileUrl;
                                        //Ghi log
                                        IdmHelper.AddSignLog(unitOfWork, model.ApplicationId, userId.ToString(), certifcate.CertId.Value, rsSign.Code + "", "Ký thành công", item.FileId);
                                    }
                                }
                            }
                            if (org != null)
                            {
                                rsSign.MST = org.Code;
                                rsSign.TenDonVi = org.Name;
                            }
                            return rsSign;
                            #endregion
                        }
                        else
                        {
                            foreach (var f in model.FileInfo)
                            {
                                //Ghi log
                                IdmHelper.AddSignLog(unitOfWork, model.ApplicationId, userId.ToString(), certifcate.CertId.Value, rsSign.Code + "", "Ký HSM không thành công: " + sdPdf, f.FileId);
                            }
                            return rsSign;
                        }
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    //Ghi log
                    IdmHelper.AddSignLog(unitOfWork, model.ApplicationId, userId.ToString(), Guid.Empty, "-1", "Lỗi ngoại lệ Exception: " + ex.Message + " Model: " + JsonConvert.SerializeObject(model), "");
                }
                return new OutputFromSignMultiTrustCA() { Code = -1, Message = "Lỗi ngoại lệ: " + ex.Message, Data = null };
            }
        }

        /// <summary>
        /// Ký với service HSM của Newwave (CTS trên HSM) danh cho phần mềm khác kết nối dữ liệu input base64, dữ liệu đã ký base64 từ KMS
        /// </summary>
        /// <param name="model"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<OutputFromSignMultiTrustCA> SignUrlXMLForThirdpartyV2FromBase64(DataInputSignXMLnCipherV2 model, Guid userId)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    var org = (from ou in unitOfWork.GetRepository<IdmUserMapOrg>().GetAll()
                               join o in unitOfWork.GetRepository<CmsOrganization>().GetAll()
                               on ou.OrganizationId equals o.Id
                               where ou.UserId == userId
                               select o).FirstOrDefault();

                    FileOuputModel dataOut = new FileOuputModel();
                    string basic_user = Utils.GetConfig("Sign:Basic_User");
                    string basic_pass = Utils.GetConfig("Sign:Basic_Pass");
                    //Main 45
                    string apiSign = Utils.GetConfig("Sign:ApiUrl_V3_XML");
                    string contentUrl = Utils.GetConfig("AppSettings:Host") + "StaticFiles/";
                    DataInputSignXMLTrustCA dataxmlTrustCA = new DataInputSignXMLTrustCA();
                    List<DataFileSignTrustCA> fileInfoXML = new List<DataFileSignTrustCA>();

                    #region Khởi tạo thư mục
                    string partitionPhysicalPath;
                    string destinationPhysicalPath = "";
                    partitionPhysicalPath = Path.Combine(Utils.GetConfig("StaticFiles:Folder"));
                    // Get root path
                    var rootPath = partitionPhysicalPath;
                    Log.Debug("Upload root path : " + rootPath);
                    // Append destinationPhysicalPath
                    if (string.IsNullOrEmpty(destinationPhysicalPath))
                    {
                        destinationPhysicalPath = ParameterCollection.Instance.GetValue("DefaultAlias");
                        destinationPhysicalPath = destinationPhysicalPath + "\\" +
                                                  DateTime.Now.ToString("yyyy") + "\\" + DateTime.Now.ToString("MM") +
                                                  "\\" +
                                                  DateTime.Now.ToString("dd") + "\\";
                        bool isWindows = RuntimeInformation.IsOSPlatform(OSPlatform.Windows);
                        if (!isWindows)
                        {
                            destinationPhysicalPath = destinationPhysicalPath.Replace("\\", "/");
                        }
                    }
                    // Create folder path
                    var sourcefolder = destinationPhysicalPath;
                    var isExistDirectory = Directory.Exists(Path.Combine(rootPath, sourcefolder));
                    if (!isExistDirectory) Directory.CreateDirectory(Path.Combine(rootPath, sourcefolder));
                    var fullfolderPath = Path.Combine(rootPath, sourcefolder);
                    #endregion

                    #region Convert model để gửi sang service sign
                    if (model.FileInfo != null && model.FileInfo.Count > 0)
                    {
                        foreach (var item in model.FileInfo)
                        {
                            #region Lưu file base64 rồi lấy link gửi đi
                            string filename = item.FileName;
                            var fileSavePath = Path.Combine(fullfolderPath, filename);
                            File.WriteAllBytes(@"" + fileSavePath + "", Convert.FromBase64String(item.FileContentBase64));
                            var fileInfo = new FileInfo(fileSavePath);
                            string physicalPath = Path.Combine(sourcefolder, fileInfo.Name).Replace("\\", "/");
                            string fileUrl = new Uri(contentUrl + physicalPath).ToString();
                            //string fileUrl = "http://web.signing.gosign.vn/StaticFiles/wf/hoa_don_test.xml";
                            #endregion
                            fileInfoXML.Add(new DataFileSignTrustCA() { FileId = item.FileId, FileUrl = fileUrl });
                        }
                    }
                    dataxmlTrustCA.FileInfo = fileInfoXML;

                    //Lấy thông tin cert theo user + key
                    var certifcate = new CertificateSignModel();
                    certifcate = GetCertificateV2(model.Key, userId);

                    if (certifcate == null)
                        return new OutputFromSignMultiTrustCA() { Code = 0, Message = "Thông tin key không hợp lệ", Data = null };

                    dataxmlTrustCA.SlotLabel = certifcate.SlotLabel;
                    dataxmlTrustCA.UserPin = certifcate.UserPin;
                    dataxmlTrustCA.Alias = certifcate.Alias;
                    //dataxmlTrustCA.CertificateFileName = certifcate.CertificateFileName;
                    dataxmlTrustCA.CertString = certifcate.CertString;
                    #endregion

                    #region Ký XML
                    using (var httpClient = new HttpClient())
                    {
                        var byteArray = Encoding.ASCII.GetBytes("" + basic_user + ":" + basic_pass + "");
                        httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));
                        var jsonString = JsonConvert.SerializeObject(dataxmlTrustCA);
                        var content = new StringContent(jsonString, Encoding.UTF8);
                        content.Headers.ContentType.MediaType = "application/json";
                        content.Headers.ContentType.CharSet = "UTF-8";
                        HttpResponseMessage responsePdf = await httpClient.PostAsync(apiSign, content);
                        string sdPdf = responsePdf.Content.ReadAsStringAsync().Result;
                        var rsSign = JsonConvert.DeserializeObject<OutputFromSignMultiTrustCA>(sdPdf);

                        if (rsSign.Code == 1 && rsSign.Data.Count > 0)
                        {
                            #region Tải file đã ký về  và trả cho thirdparty nếu thành công
                            foreach (var item in rsSign.Data)
                            {
                                var f = model.FileInfo.FirstOrDefault(x => x.FileId == item.FileId);
                                if (f != null)
                                {
                                    string fileContentBase64 = item.OuputFileContentBase64;
                                    var fileSavePath = Path.Combine(fullfolderPath, "signed_" + f.FileName);
                                    File.WriteAllBytes(@"" + fileSavePath + "", Convert.FromBase64String(fileContentBase64));
                                    var fileInfo = new FileInfo(fileSavePath);
                                    string fileSignedPath = Path.Combine(sourcefolder, fileInfo.Name).Replace("\\", "/");
                                    string fileUrl = new Uri(contentUrl + fileSignedPath).ToString();
                                    item.FileUrl = fileUrl;
                                    //Ghi log
                                    IdmHelper.AddSignLog(unitOfWork, model.ApplicationId, userId.ToString(), certifcate.CertId.Value, rsSign.Code + "", "Ký thành công", item.FileId);
                                }
                            }
                            if (org != null)
                            {
                                rsSign.MST = org.Code;
                                rsSign.TenDonVi = org.Name;
                            }
                            return rsSign;
                            #endregion
                        }
                        else
                        {
                            foreach (var f in model.FileInfo)
                            {
                                //Ghi log
                                IdmHelper.AddSignLog(unitOfWork, model.ApplicationId, userId.ToString(), certifcate.CertId.Value, rsSign.Code + "", "Ký HSM không thành công: " + sdPdf, f.FileId);
                            }
                            return rsSign;
                        }
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    //Ghi log
                    IdmHelper.AddSignLog(unitOfWork, model.ApplicationId, userId.ToString(), Guid.Empty, "-1", "Lỗi ngoại lệ Exception: " + ex.Message + " Model: " + JsonConvert.SerializeObject(model), "");
                }
                return new OutputFromSignMultiTrustCA() { Code = -1, Message = "Lỗi ngoại lệ: " + ex.Message, Data = null };
            }
        }

        /// <summary>
        /// Validate file pdf đã ký
        /// </summary>
        /// <param name="model"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<DataOutputVerifySign> VerifyPdf(DataInputVerifySign model, Guid userId)
        {
            try
            {
                string basic_user = Utils.GetConfig("Sign:Basic_User");
                string basic_pass = Utils.GetConfig("Sign:Basic_Pass");
                string apiPdf = Utils.GetConfig("Sign:ApiUrl_V3_Verify_PDF");
                DataInputVerifySignTrustCA dataPdfTrustCA = new DataInputVerifySignTrustCA();
                dataPdfTrustCA.FileUrl = model.FileUrl;

                //Lấy thông tin cert theo user + key
                var certifcate = new CertificateSignModel();
                certifcate = GetCertificateV2(model.Key, userId);

                if (certifcate == null)
                    return new DataOutputVerifySign() { Result = "NO", Message = "Thông tin key không hợp lệ" };

                dataPdfTrustCA.SlotLabel = certifcate.SlotLabel;
                dataPdfTrustCA.UserPin = certifcate.UserPin;

                #region Ký PDF
                using (var httpClientPdf = new HttpClient())
                {
                    var byteArray = Encoding.ASCII.GetBytes("" + basic_user + ":" + basic_pass + "");
                    httpClientPdf.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));
                    var jsonString_pdf = JsonConvert.SerializeObject(dataPdfTrustCA);
                    var content_pdf = new StringContent(jsonString_pdf, Encoding.UTF8);
                    content_pdf.Headers.ContentType.MediaType = "application/json";
                    content_pdf.Headers.ContentType.CharSet = "UTF-8";
                    HttpResponseMessage responsePdf = await httpClientPdf.PostAsync(apiPdf, content_pdf);
                    string sdPdf = responsePdf.Content.ReadAsStringAsync().Result;
                    var rsSign = JsonConvert.DeserializeObject<DataOutputVerifySign>(sdPdf);

                    if (rsSign.Result == "YES")
                        return rsSign;
                    else
                        return new DataOutputVerifySign() { Result = "NO", Message = "Lỗi không thể verify" };
                }
                #endregion
            }
            catch (Exception ex)
            {
                return new DataOutputVerifySign() { Result = "NO", Message = "Lỗi ngoại lệ: " + ex.Message };
            }
        }

        /// <summary>
        /// Validate file XML đã ký
        /// </summary>
        /// <param name="model"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<DataOutputVerifySign> VerifyXML(DataInputVerifySign model, Guid userId)
        {
            try
            {
                string basic_user = Utils.GetConfig("Sign:Basic_User");
                string basic_pass = Utils.GetConfig("Sign:Basic_Pass");
                string apiXml = Utils.GetConfig("Sign:ApiUrl_V3_Verify_XML");
                DataInputVerifySignTrustCA dataTrustCA = new DataInputVerifySignTrustCA();
                dataTrustCA.FileUrl = model.FileUrl;

                //Lấy thông tin cert theo user + key
                var certifcate = new CertificateSignModel();
                certifcate = GetCertificateV2(model.Key, userId);

                if (certifcate == null)
                    return new DataOutputVerifySign() { Result = "NO", Message = "Thông tin key không hợp lệ" };

                dataTrustCA.SlotLabel = certifcate.SlotLabel;
                dataTrustCA.UserPin = certifcate.UserPin;

                #region Ký PDF
                using (var httpClientPdf = new HttpClient())
                {
                    var byteArray = Encoding.ASCII.GetBytes("" + basic_user + ":" + basic_pass + "");
                    httpClientPdf.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));
                    var jsonString = JsonConvert.SerializeObject(dataTrustCA);
                    var content = new StringContent(jsonString, Encoding.UTF8);
                    content.Headers.ContentType.MediaType = "application/json";
                    content.Headers.ContentType.CharSet = "UTF-8";
                    HttpResponseMessage responsePdf = await httpClientPdf.PostAsync(apiXml, content);
                    string sdPdf = responsePdf.Content.ReadAsStringAsync().Result;
                    var rsSign = JsonConvert.DeserializeObject<DataOutputVerifySign>(sdPdf);
                    if (rsSign.Result == "YES")
                        return rsSign;
                    else
                        return new DataOutputVerifySign() { Result = "NO", Message = "Lỗi không thể verify" };
                }
                #endregion
            }
            catch (Exception ex)
            {
                return new DataOutputVerifySign() { Result = "NO", Message = "Lỗi ngoại lệ: " + ex.Message };
            }
        }


        /// <summary>
        /// Kiểm tra key với tài khoản trong DB xem có dữ liệu không
        /// </summary>
        /// <param name="key"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public OldResponse<KeyHashModel> CheckKey(string key, Guid userId)
        {
            var keyHash = MD5Hash(key);

            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    var datas = (from cu in unitOfWork.GetRepository<CmsCertUser>().GetAll()
                                 where cu.UserId == userId && cu.Key == keyHash
                                 select cu).FirstOrDefault();

                    if (datas != null)
                    {
                        KeyHashModel item = new KeyHashModel();
                        item.KeyHash = datas.Key;
                        return new OldResponse<KeyHashModel>(1, "Success", item);
                    }
                    else
                    {
                        return new OldResponse<KeyHashModel>(0, "Không có mã pin trùng khớp", null);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OldResponse<KeyHashModel>(-1, ex.Message, null);
            }
        }

        /// <summary>
        /// Tính tọa độ cho khung ký theo chiều cao
        /// </summary>
        /// <param name="_Y"></param>
        /// <param name="EleHeight"></param>
        /// <param name="pagenum"></param>
        /// <param name="pageHeight"></param>
        /// <returns></returns>
        private int GetMiroTop(int _Y, int EleHeight, int pagenum, int pageHeight)
        {
            int Y = 0;
            int PageHeight = 0;
            for (int i = 0; i < pagenum; i++)
            {

                PageHeight += pageHeight;
            }

            Y = (PageHeight - _Y - EleHeight);

            return Y;
        }

        /// <summary>
        /// Lấy thông tin cert để ký nội bộ
        /// </summary>
        /// <param name="key"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        private CertificateSignModel GetCertificate(string key, Guid userId)
        {
            var certificate = new CertificateSignModel();
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    var datas = (from cu in unitOfWork.GetRepository<CmsCertUser>().GetAll()
                                 join c in unitOfWork.GetRepository<CmsCert>().GetAll()
                                     on cu.CertId equals c.Id
                                 where cu.UserId == userId && cu.Key == key
                                 select c).FirstOrDefault();

                    if (datas != null)
                    {
                        certificate.SlotId = datas.SlotID.ToString();
                        certificate.SlotLabel = datas.SlotLabel;
                        certificate.UserPin = datas.UserPin;
                        certificate.Alias = datas.Alias;
                        certificate.CertificateFileName = datas.CertificateFileName;
                        certificate.CertString = datas.Cert;
                        certificate.P12Path = datas.P12Path;
                        return certificate;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// Lấy thông tin cert cho service ký phơi ra ngoài
        /// </summary>
        /// <param name="key"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        private CertificateSignModel GetCertificateV2(string key, Guid userId)
        {
            var certificate = new CertificateSignModel();
            var keyHash = MD5Hash(key);
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    var datas = (from cu in unitOfWork.GetRepository<CmsCertUser>().GetAll()
                                 join c in unitOfWork.GetRepository<CmsCert>().GetAll()
                                     on cu.CertId equals c.Id
                                 where cu.UserId == userId && cu.Key == keyHash
                                 select c).FirstOrDefault();

                    if (datas != null)
                    {
                        certificate.CertId = datas.Id;
                        certificate.SlotId = datas.SlotID.ToString();
                        certificate.SlotLabel = datas.SlotLabel;
                        certificate.UserPin = datas.UserPin;
                        certificate.Alias = datas.Alias;
                        certificate.CertificateFileName = datas.CertificateFileName;
                        certificate.CertString = datas.Cert;
                        certificate.P12Path = datas.P12Path;
                        return certificate;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// Mã hóa MD5
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        private string MD5Hash(string text)
        {
            StringBuilder hash = new StringBuilder();
            MD5CryptoServiceProvider md5provider = new MD5CryptoServiceProvider();
            byte[] bytes = md5provider.ComputeHash(new UTF8Encoding().GetBytes(text));

            for (int i = 0; i < bytes.Length; i++)
            {
                hash.Append(bytes[i].ToString("x2"));
            }
            return hash.ToString();
        }
    }
}
