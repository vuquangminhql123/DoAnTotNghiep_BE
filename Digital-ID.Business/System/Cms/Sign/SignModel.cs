﻿using DigitalID.Data;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;

namespace DigitalID.Business
{
    public class KeyCreateModel
    {
        public string slotLabel { get; set; }
        public string userPin { get; set; }
        public string alias { get; set; }
        public string keyAlgorithmId { get; set; }
        public string keyLength { get; set; }
    }

    public class CsrCreateModel
    {
        public string slotLabel { get; set; }
        public string userPin { get; set; }
        public string alias { get; set; }
        public string subjectDN { get; set; }
        public string hashAlgorithm { get; set; }
    }

    public class CertCreateModel
    {
        public string encoding { get; set; }
        public UserDataCert userdata { get; set; }
        public string csr { get; set; }
    }

    public class UserDataCert
    {
        public string username { get; set; }
        public string subjectDN { get; set; }
        public string email { get; set; }
        public bool clearPwd { get; set; }
        public string caName { get; set; }
        public string endEntityProfileName { get; set; }
        public string certificateProfileName { get; set; }
    }

    public class OuputGenKeyModel
    {
        public string result { get; set; }
        public string message { get; set; }
    }

    public class OuputGenCsrModel
    {
        public string csr { get; set; }
    }

    public class OuputGenCertModel
    {
        public string errCode { get; set; }
        public string errMsg { get; set; }
        public string data { get; set; }
    }

    public class SignModel
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string StandardSign { get; set; } // PaDES, xaDes, caDes, LTANs
        public string HashingAlgorithm { get; set; } //SHA1, SHA224, SHA256, SHA384, SHA512, RipeMD128, RipeMD160
        public int SignZoneConfig { get; set; } = 0; // 0: AUTO, 1| MANUAL
        public int URX { get; set; } = 0;  // Upper Right X
        public int URY { get; set; } = 0; // Upper Right Y
        public int LLX { get; set; } = 0; // Lower Left X
        public int LLY { get; set; } = 0; // Lower Left Y
        public int SignAtPage { get; set; } = 0;
        public int AreaHeight { get; set; } = 0; // Height sign area
        public int AreaWidth { get; set; } = 0; // Width sign area

        public bool IsSignTime { get; set; }
        public bool IsRevoke { get; set; }
        public bool Status { get; set; }

        public int DigitalSignatureOption { get; set; } = 0; // 0| AND  , 1| OR
        public int PageType { get; set; } // 0| landscape    , 1| portrait
        public string TemplateFilePath { get; set; }
        public int PageHeight { get; set; }
        public int PageWidth { get; set; }

        public string SignFinalPath { get; set; }

        public string Key { get; set; }

    }

    public class SigninginfoPDF
    {
        public Guid? IdDocument { get; set; }
        public string Reason { get; set; }
        public string Location { get; set; }
        public string InputURL { get; set; }
        public string InputBase64 { get; set; }
        public Stream InputStream { get; set; }
        public string FileName { get; set; }
        public string OutputURL { get; set; }
        public int PageNumber { get; set; }
        public int X { get; set; }  // by px ; X Y bottom left       
        public int Y { get; set; } // by px ; X Y bottom left
        public int ElementWidth { get; set; } // by px ; X Y bottom left    
        public int ElementHeight { get; set; } // by px ; X Y bottom left    
        public int PageHeight { get; set; }
        public int PageWidth { get; set; }
        public int FontSize { get; set; }
        public string SigningIcon { get; set; }
        public int? SignatureImageType { get; set; } // 0: SignatureImage=false ; 1: ImageAndText; 2:ImageAsBackground; 3: ImageWithNoText;
        public string TimeStampingServer { get; set; }
        public bool PadesLtv { get; set; }
        public bool TimeStamping { get; set; }
    }

    public class ReturnModel
    {
        public string URL { get; set; }
        public string FileName { get; set; }
        public string Base64Combined { get; set; }
        //public Stream StreamCombined { get; set; }
        public long SignatureSize { get; set; }

    }

    public class DataInputSignPDFTrustCA
    {
        [JsonProperty("image")]
        public string Image { get; set; } //Base64 của ảnh ký
        [JsonProperty("reason")]
        public string Reason { get; set; } //lý do ký tài liệu
        [JsonProperty("location")]
        public string Location { get; set; } //địa điểm ký tài liệu
        [JsonProperty("contactInfo")]
        public string ContactInfo { get; set; }//thông tin liên hệ người ký
        [JsonProperty("isVisible")]
        public int IsVisible { get; set; } // tùy chọn hiển thị seal
        [JsonProperty("llx")]
        public string Llx { get; set; } // lower left x
        [JsonProperty("lly")]
        public string Lly { get; set; } // lower left y
        [JsonProperty("urx")]
        public string Urx { get; set; } //upper right x
        [JsonProperty("ury")]
        public string Ury { get; set; } // upper right y
        [JsonProperty("page")]
        public string Page { get; set; } // sign at page
        [JsonProperty("userPin")]
        public string UserPin { get; set; } // user pin test
        [JsonProperty("alias")]
        public string Alias { get; set; } // alias test
        [JsonProperty("certificateFileName")]
        public string CertificateFileName { get; set; } // certificate test
        [JsonProperty("certString")]
        public string CertString { get; set; } // certificate base64
        [JsonProperty("slotLabel")]
        public string SlotLabel { get; set; } // slot id test
        [JsonProperty("files")]
        public List<DataFileSignTrustCA> FileInfo { get; set; }
    }

    public class DataInputSignXMLTrustCA
    {
        [JsonProperty("userPin")]
        public string UserPin { get; set; } // user pin test
        [JsonProperty("alias")]
        public string Alias { get; set; } // alias test
        [JsonProperty("certificateFileName")]
        public string CertificateFileName { get; set; } // certificate test
        [JsonProperty("certString")]
        public string CertString { get; set; } // certificate base64
        [JsonProperty("slotLabel")]
        public string SlotLabel { get; set; } // slot label test
        [JsonProperty("files")]
        public List<DataFileSignTrustCA> FileInfo { get; set; }
    }

    public class DataInputSignPDFnCipher
    {
        [JsonProperty("applicationId")]
        public string ApplicationId { get; set; }//Id ứng dụng kết nối
        [JsonProperty("image")]
        public string Image { get; set; } //Base64 của ảnh ký
        [JsonProperty("reason")]
        public string Reason { get; set; } //lý do ký tài liệu
        [JsonProperty("location")]
        public string Location { get; set; } //địa điểm ký tài liệu
        [JsonProperty("contactInfo")]
        public string ContactInfo { get; set; }//thông tin liên hệ người ký
        [JsonProperty("isVisible")]
        public int IsVisible { get; set; } // tùy chọn hiển thị seal
        [JsonProperty("profile")]
        public string Profile { get; set; } //thông tin profile, nếu có giá trị sẽ lấy tọa độ theo thông tin profile
        [JsonProperty("llx")]
        public string Llx { get; set; } // lower left x
        [JsonProperty("lly")]
        public string Lly { get; set; } // lower left y
        [JsonProperty("urx")]
        public string Urx { get; set; } //upper right x
        [JsonProperty("ury")]
        public string Ury { get; set; } // upper right y
        [JsonProperty("page")]
        public string Page { get; set; } // sign at page
        [JsonProperty("key")]
        public string Key { get; set; } // key của CTS
        [JsonProperty("files")]
        public List<DataFileSignTrustCA> FileInfo { get; set; }
    }

    public class DataInputSignPDFDigital
    {
        [JsonProperty("applicationId")]
        public string ApplicationId { get; set; }//Id ứng dụng kết nối
        [JsonProperty("image")]
        public string Image { get; set; } //Base64 của ảnh ký
        [JsonProperty("reason")]
        public string Reason { get; set; } //lý do ký tài liệu
        [JsonProperty("location")]
        public string Location { get; set; } //địa điểm ký tài liệu
        [JsonProperty("otp")]
        public string OTP { get; set; }//token khi ký
        [JsonProperty("profile")]
        public string Profile { get; set; } //thông tin profile, nếu có giá trị sẽ lấy tọa độ theo thông tin profile
        [JsonProperty("llx")]
        public string Llx { get; set; } // lower left x
        [JsonProperty("lly")]
        public string Lly { get; set; } // lower left y
        [JsonProperty("urx")]
        public string Urx { get; set; } //with ract
        [JsonProperty("ury")]
        public string Ury { get; set; } //height ract
        [JsonProperty("page")]
        public string Page { get; set; } // sign at page
        [JsonProperty("files")]
        public List<DataFileSignTrustCA> FileInfo { get; set; }
    }

    public class DataInputSignXMLnCipher
    {
        [JsonProperty("application")]
        public string ApplicationId { get; set; }//Id ứng dụng kết nối
        [JsonProperty("key")]
        public string Key { get; set; } // key của CTS
        [JsonProperty("files")]
        public List<DataFileSignTrustCA> FileInfo { get; set; }
    }

    /// <summary>
    /// Dùng cho content Base64 XML
    /// </summary>
    public class DataInputSignXMLnCipherV2
    {
        [JsonProperty("application")]
        public string ApplicationId { get; set; }//Id ứng dụng kết nối
        [JsonProperty("key")]
        public string Key { get; set; } // key của CTS
        [JsonProperty("files")]
        public List<DataFileSignFromThirdparty> FileInfo { get; set; }
    }

    public class DataFileSignFromThirdparty
    {
        [JsonProperty("fileId")]
        public string FileId { get; set; }
        [JsonProperty("fileName")]
        public string FileName { get; set; }
        [JsonProperty("fileContent")]
        public string FileContentBase64 { get; set; }
    }

    public class DataFileSignTrustCA
    {
        [JsonProperty("fileId")]
        public string FileId { get; set; }
        [JsonProperty("outputFile")]
        public string OuputFileContentBase64 { get; set; }
        [JsonProperty("outputFileUrl")]
        public string OutputFileUrl { get; set; }
        [JsonProperty("fileUrl")]
        public string FileUrl { get; set; }
    }

    public class DataFileSignTrustCAFormat
    {
        [JsonProperty("fileId")]
        public string FileId { get; set; }
        [JsonProperty("fileUrl")]
        public string FileUrl { get; set; }
        [JsonProperty("fileBase64Content")]
        public string FileBase64Content { get; set; }
    }

    public class DataFileSignDigital
    {
        [JsonProperty("fileId")]
        public string FileId { get; set; }
        [JsonProperty("fileUrl")]
        public string FileUrl { get; set; }
    }

    public class OutputFromSignMultiTrustCA
    {
        [JsonProperty("code")]
        public int Code { get; set; }
        [JsonProperty("message")]
        public string Message { get; set; }
        [JsonProperty("data")]
        public List<DataFileSignTrustCA> Data { get; set; }
        [JsonProperty("tendonvi")]
        public string TenDonVi { get; set; }
        [JsonProperty("mst")]
        public string MST { get; set; }
    }

    public class OutputFromSignMultiDigital
    {
        [JsonProperty("code")]
        public int Code { get; set; }
        [JsonProperty("message")]
        public string Message { get; set; }
        [JsonProperty("data")]
        public List<DataFileSignDigital> Data { get; set; }
    }

    public class OutputFromSignMultiTrustCAFormat
    {
        [JsonProperty("code")]
        public int Code { get; set; }
        [JsonProperty("message")]
        public string Message { get; set; }
        [JsonProperty("data")]
        public List<DataFileSignTrustCAFormat> Data { get; set; }
        [JsonProperty("tendonvi"), JsonIgnore]
        public string TenDonVi { get; set; }
        [JsonProperty("mst"), JsonIgnore]
        public string MST { get; set; }
    }

    public class OutputFromSignMultiPFXFormat
    {
        [JsonProperty("code")]
        public int Code { get; set; }
        [JsonProperty("message")]
        public string Message { get; set; }
        [JsonProperty("data")]
        public List<DataFileSignTrustCAFormat> Data { get; set; }
    }

    public class FileOuputModel
    {
        public string FileName { get; set; }
        public string FileUrl { get; set; }
    }

    public class KeyHashModel
    {
        public string KeyHash { get; set; }
    }

    public class CertificateSignModel
    {
        public Guid? CertId { get; set; }
        public string SlotId { get; set; }
        public string UserPin { get; set; }
        public string Alias { get; set; }
        public string CertificateFileName { get; set; }
        public string SlotLabel { get; set; }
        public string CertString { get; set; }
        public string P12Path { get; set; }

    }

    public class DataInputVerifySign
    {
        [JsonProperty("application")]
        public string ApplicationId { get; set; }//Id ứng dụng kết nối
        [JsonProperty("fileUrl")]
        public string FileUrl { get; set; }
        [JsonProperty("key")]
        public string Key { get; set; }
    }

    public class DataInputVerifySignTrustCA
    {
        [JsonProperty("fileUrl")]
        public string FileUrl { get; set; }
        [JsonProperty("slotLabel")]
        public string SlotLabel { get; set; }
        [JsonProperty("userPin")]
        public string UserPin { get; set; }
    }

    public class DataOutputVerifySign
    {
        [JsonProperty("result")]
        public string Result { get; set; }
        [JsonProperty("message")]
        public string Message { get; set; }
    }

    public class KeyVerifyInputModel
    {
        public Guid userId { get; set; }
        public string userName { get; set; }
        public string key { get; set; }
    }
}

