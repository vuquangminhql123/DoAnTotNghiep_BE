﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DigitalID.Data;

namespace DigitalID.Business
{
    public interface ISignProfileHandler
    {
        Task<Response> FindAsync(Guid id);
        Task<Response> CreateAsync(SignProfileCreateModel model, Guid applicationId, Guid userId);
        Task<Response> UpdateAsync(Guid id, SignProfileUpdateModel model, Guid applicationId, Guid userId);
        Task<Response> DeleteAsync(Guid id);
        Task<Response> DeleteRangeAsync(IList<Guid> listId);
        Task<Response> GetAllAsync();
        Task<Response> GetAllAsync(SignProfileQueryModel query);
        Task<Response> GetPageAsync(SignProfileQueryModel query);
        Response GetAll();
    }
}