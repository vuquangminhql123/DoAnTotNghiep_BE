﻿using LinqKit;
using DigitalID.Data;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace DigitalID.Business
{
    public class SignProfileHandler : ISignProfileHandler
    {
        private readonly DbHandler<CmsSignProfile, SignProfileModel, SignProfileQueryModel> _dbHandler = DbHandler<CmsSignProfile, SignProfileModel, SignProfileQueryModel>.Instance;

        #region CRUD
        public Task<Response> FindAsync(Guid id)
        {
            return _dbHandler.FindAsync(id);
        }
        public async Task<Response> CreateAsync(SignProfileCreateModel model, Guid applicationId, Guid userId)
        {

            CmsSignProfile request = AutoMapperUtils.AutoMap<SignProfileCreateModel, CmsSignProfile>(model);
            request = request.InitCreate(applicationId, userId);
            request.Id = Guid.NewGuid();
            var result = await _dbHandler.CreateAsync(request);
            return result;
        }
        public async Task<Response> UpdateAsync(Guid id, SignProfileUpdateModel model, Guid applicationId, Guid userId)
        {
            CmsSignProfile request = AutoMapperUtils.AutoMap<SignProfileUpdateModel, CmsSignProfile>(model);
            request = request.InitUpdate(applicationId, userId);
            request.Id = id;
            var result = await _dbHandler.UpdateAsync(id, request);
            return result;
        }
        public async Task<Response> DeleteAsync(Guid id)
        {
            var result = await _dbHandler.DeleteAsync(id);
            return result;
        }
        public async Task<Response> DeleteRangeAsync(IList<Guid> listId)
        {
            var result = await _dbHandler.DeleteRangeAsync(listId, "Id");
            return result;
        }
        #endregion
        public Response GetAll()
        {
            return _dbHandler.GetAll();
        }
        public async Task<Response> GetAllAsync()
        {
            var predicate = PredicateBuilder.New<CmsSignProfile>(true);
            return await _dbHandler.GetAllAsync(predicate);
        }
        public Task<Response> GetAllAsync(SignProfileQueryModel query)
        {
            var predicate = BuildQuery(query);
            return _dbHandler.GetAllAsync(predicate, query.Sort);
        }
        public Task<Response> GetPageAsync(SignProfileQueryModel query)
        {
            var predicate = BuildQuery(query);
            return _dbHandler.GetPageAsync(predicate, query);
        }
        private Expression<Func<CmsSignProfile, bool>> BuildQuery(SignProfileQueryModel query)
        {
            var predicate = PredicateBuilder.New<CmsSignProfile>(true);
            if (!string.IsNullOrEmpty(query.Code))
            {
                predicate.And(s => s.Code == query.Code);
            }
            if (query.Status.HasValue)
            {
                predicate.And(s => s.Status == query.Status.Value);
            }
            if (query.ListId != null && query.ListId.Count > 0)
            {
                predicate.And(s => query.ListId.Contains(s.Id));
            }
            if (!string.IsNullOrEmpty(query.FullTextSearch))
            {
                predicate.And(s =>
                s.Name.ToLower().Contains(query.FullTextSearch.ToLower())
                || s.Code.ToLower().Contains(query.FullTextSearch.ToLower())
                || s.Description.ToLower().Contains(query.FullTextSearch.ToLower())
                );
            }
            if (!string.IsNullOrEmpty(query.HashingAlgorithm))
            {
                predicate.And(s => s.HashingAlgorithm == query.HashingAlgorithm);
            }
            if (!string.IsNullOrEmpty(query.StandardSign))
            {
                predicate.And(s => s.StandardSign == query.StandardSign);
            }

            return predicate;
        }
    }
}
