﻿using DigitalID.Data;
using System;
using System.Collections.Generic;

namespace DigitalID.Business
{
    public class BaseSignProfileModel : BaseModel
    {
        public Guid Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string StandardSign { get; set; } // PaDES, xaDes, caDes, LTANs
        public string HashingAlgorithm { get; set; } //SHA1, SHA224, SHA256, SHA384, SHA512, RipeMD128, RipeMD160
        public int SignZoneConfig { get; set; } = 0; // 0: AUTO, 1| MANUAL
        public int URX { get; set; } = 0;  // Upper Right X
        public int URY { get; set; } = 0; // Upper Right Y
        public int LLX { get; set; } = 0; // Lower Left X
        public int LLY { get; set; } = 0; // Lower Left Y
        public int SignAtPage { get; set; } = 0;

        public bool IsSignTime { get; set; }
        public bool IsRevoke { get; set; }
        public bool Status { get; set; }

        public int DigitalSignatureOption { get; set; } = 0; // 0| AND  , 1| OR
        public int PageType { get; set; } // 0| landscape    , 1| portrait
        public string TemplateFilePath { get; set; }
    }
    public class SignProfileModel : BaseSignProfileModel
    {
    }
    public class SignProfileQueryModel : PaginationRequest
    {
        public string Code { get; set; }
        public bool? Status { get; set; }
        public string HashingAlgorithm { get; set; } 
        public string StandardSign { get; set; } 
    }

    public class SignProfileCreateModel
    {

        public string Code { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string StandardSign { get; set; } // PaDES, xaDes, caDes, LTANs
        public string HashingAlgorithm { get; set; } //SHA1, SHA224, SHA256, SHA384, SHA512, RipeMD128, RipeMD160
        public int SignZoneConfig { get; set; } = 0; // 0: AUTO, 1| MANUAL
        public int URX { get; set; } = 0;  // Upper Right X
        public int URY { get; set; } = 0; // Upper Right Y
        public int LLX { get; set; } = 0; // Lower Left X
        public int LLY { get; set; } = 0; // Lower Left Y
        public int SignAtPage { get; set; } = 0;

        public bool IsSignTime { get; set; }
        public bool IsRevoke { get; set; }
        public bool Status { get; set; }

        public int DigitalSignatureOption { get; set; } = 0; // 0| AND  , 1| OR
        public int PageType { get; set; } // 0| landscape    , 1| portrait
        public string TemplateFilePath { get; set; }
    }
    public class SignProfileUpdateModel
    {

        public string Code { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string StandardSign { get; set; } // PaDES, xaDes, caDes, LTANs
        public string HashingAlgorithm { get; set; } //SHA1, SHA224, SHA256, SHA384, SHA512, RipeMD128, RipeMD160
        public int SignZoneConfig { get; set; } = 0; // 0: AUTO, 1| MANUAL
        public int URX { get; set; } = 0;  // Upper Right X
        public int URY { get; set; } = 0; // Upper Right Y
        public int LLX { get; set; } = 0; // Lower Left X
        public int LLY { get; set; } = 0; // Lower Left Y
        public int SignAtPage { get; set; } = 0;

        public bool IsSignTime { get; set; }
        public bool IsRevoke { get; set; }
        public bool Status { get; set; }

        public int DigitalSignatureOption { get; set; } = 0; // 0| AND  , 1| OR
        public int PageType { get; set; } // 0| landscape    , 1| portrait
        public string TemplateFilePath { get; set; }
    }
}

