﻿namespace DigitalID.Business
{
    public class AppSettings
    {
        public string RecoverPassworkLink { get; set; }

        public string DatabaseConnectionString { get; set; }

        public bool AllowGenerateSeedData { get; set; }

        public bool DatabaseAutomigration { get; set; }

        public string Secret { get; set; }

        public FirebaseSettings FirebaseSettings { get; set; }

        public EmailSettings EmailSettings { get; set; }
    }

    public class FirebaseSettings
    {
        /// <summary>
        /// The firebase json key.
        /// </summary>
        public string FirebaseJsonKey { get; set; }

        /// <summary>
        /// The firebase messaging settings.
        /// </summary>
        public FirebaseMessagingSettings MessagingSettings { get; set; }

        /// <summary>
        /// The firebase database settings.
        /// </summary>
        public FirebaseDatabaseSettings DatabaseSettings { get; set; }
    }

    public class FirebaseMessagingSettings
    {
        /// <summary>
        /// The icon url.
        /// </summary>
        public string IconUrl { get; set; }

        /// <summary>
        /// The click action.
        /// </summary>
        public string ClickAction { get; set; }
    }

    public class FirebaseDatabaseSettings
    {
        /// <summary>
        /// The fireabase project url.
        /// </summary>
        public string FirebaseProjectUrl { get; set; }

        /// <summary>
        /// The database secret token.
        /// </summary>
        public string DBToken { get; set; }
    }

    public class EmailSettings
    {
        public string Username { get; set; }
        public string Password { get; set; }

        public string Host { get; set; }
        public bool EnableSsl { get; set; }
        public int Port { get; set; }
        public bool UseDefaultCredentials { get; set; }
    }
}
