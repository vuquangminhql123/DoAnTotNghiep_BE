﻿using DigitalID.Data;
using Microsoft.EntityFrameworkCore;
using QRCoder;
using Serilog;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using ZXing;

namespace DigitalID.Business
{
    public class CrmAccountHandler : ICrmAccountHandler
    {
        #region Message
        private string MessageEmailIsNotUnique = "Email đã được đăng ký!";
        #endregion

        private const string CachePrefix = "CrmAccount";
        private const string SelectItemCacheSubfix = "list-select";
        private const string CodePrefix = "ACC.";
        private readonly DataContext _dataContext;
        private readonly ICacheService _cacheService;
        private readonly IEmailHandler _emailHandler;

        public CrmAccountHandler(DataContext dataContext, ICacheService cacheService, IEmailHandler emailHandler)
        {
            _dataContext = dataContext;
            _cacheService = cacheService;
            _emailHandler = emailHandler;
        }

        public async Task<Response> Create(CrmAccountCreateModel model)
        {
            try
            {
                var entity = AutoMapperUtils.AutoMap<CrmAccountCreateModel, CrmAccount>(model);

                #region Check Email Unique
                var ckEmail = await _dataContext.CrmAccount.AnyAsync(x => x.Email.Equals(entity.Email));
                if (ckEmail)
                {
                    return new ResponseError(Code.BadRequest, MessageEmailIsNotUnique);
                }
                #endregion

                entity.CreatedDate = DateTime.Now;

                long identityNumber = await _dataContext.CrmAccount.DefaultIfEmpty().MaxAsync(x => x.IdentityNumber);

                entity.IdentityNumber = ++identityNumber;
                entity.Code = Utils.GenerateAutoCode(CodePrefix, identityNumber);

                #region Generate FirstName LastName
                var names = entity.FullName.Split(' ');
                if (names.Length == 1)
                {
                    entity.FirstName = names[0];
                }
                else if (names.Length >= 2)
                {
                    entity.FirstName = names[0];
                    entity.LastName = names[names.Length - 1];
                }
                #endregion

                entity.Id = Guid.NewGuid();
                await _dataContext.CrmAccount.AddAsync(entity);

                int dbSave = await _dataContext.SaveChangesAsync();

                CrmAccountQRCodeModel qrCodeModel = new CrmAccountQRCodeModel()
                {
                    Id = entity.Id,
                    FullName = entity.FullName,
                    Email = entity.Email
                };

                if (dbSave > 0)
                {
                    Log.Information("Add success: " + JsonSerializer.Serialize(entity));
                    InvalidCache();

                    #region Gửi email thông báo đến khách hàng

                    string title = "[eTicket] - Đăng ký tài khoản thành công";

                    //var byteImage = GenerateMyQCCode(JsonSerializer.Serialize(qrCodeModel));
                    //string imageData = "data:image/png;base64," + Convert.ToBase64String(byteImage);

                    var bm = RenderQrCode(JsonSerializer.Serialize(qrCodeModel), "L");
                    var byteImage = ImageToByte(bm);

                    StringBuilder htmlBody = new StringBuilder();
                    htmlBody.Append("<html><body>");
                    htmlBody.Append("<p>Xin chào <b>" + entity.FullName + "</b>,</p>");
                    htmlBody.Append("<p>Bạn đã đăng ký tài khoản tham dự sự kiện thành công, vui lòng quét QRCode bên dưới khi checkin tại sự kiện.</p>");
                    htmlBody.Append("<p><img src=\"cid:imageUniqueId\" alt='QRCode' style='height: 500px;'> </p>");
                    htmlBody.Append("</body></html>");

                    _emailHandler.SendMailGoogleWithQRCode(new List<string>() { entity.Email }, null, null, title, htmlBody.ToString(), Convert.ToBase64String(byteImage));

                    #endregion

                    return new ResponseObject<CrmAccountQRCodeModel>(qrCodeModel, MessageConstants.CreateSuccessMessage, Code.Success);
                }
                else
                {
                    return new ResponseError(Code.ServerError, MessageConstants.CreateErrorMessage);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, MessageConstants.ErrorLogMessage);
                return new ResponseError(Code.ServerError, $"{MessageConstants.CreateErrorMessage} - {ex.Message}");
            }
        }

        public async Task<Response> CreateOrGetDetail(CrmAccountCreateModel model)
        {
            try
            {
                CrmAccountQRCodeModel qrCodeModel = new CrmAccountQRCodeModel();
                var entity = AutoMapperUtils.AutoMap<CrmAccountCreateModel, CrmAccount>(model);

                #region Check Email Exist
                var account = await _dataContext.CrmAccount
                        .FirstOrDefaultAsync(x => x.Email == model.Email);
                if (account == null)
                {
                    #endregion

                    entity.CreatedDate = DateTime.Now;

                    long identityNumber = await _dataContext.CrmAccount.DefaultIfEmpty().MaxAsync(x => x.IdentityNumber);

                    entity.IdentityNumber = ++identityNumber;
                    entity.Code = Utils.GenerateAutoCode(CodePrefix, identityNumber);

                    #region Generate FirstName LastName
                    var names = entity.FullName.Split(' ');
                    if (names.Length == 1)
                    {
                        entity.FirstName = names[0];
                    }
                    else if (names.Length >= 2)
                    {
                        entity.FirstName = names[0];
                        entity.LastName = names[names.Length - 1];
                    }
                    #endregion

                    entity.Id = Guid.NewGuid();
                    await _dataContext.CrmAccount.AddAsync(entity);

                    int dbSave = await _dataContext.SaveChangesAsync();

                    qrCodeModel = new CrmAccountQRCodeModel()
                    {
                        Id = entity.Id,
                        FullName = entity.FullName,
                        Email = entity.Email
                    };

                    if (dbSave > 0)
                    {
                        Log.Information("Add success: " + JsonSerializer.Serialize(entity));
                        InvalidCache();

                        #region Gửi email thông báo đến khách hàng

                        string title = "[eTicket] - Đăng ký tài khoản thành công";

                        //var byteImage = GenerateMyQCCode(JsonSerializer.Serialize(qrCodeModel));
                        //string imageData = "data:image/png;base64," + Convert.ToBase64String(byteImage);

                        var bm = RenderQrCode(JsonSerializer.Serialize(qrCodeModel), "L");
                        var byteImage = ImageToByte(bm);

                        StringBuilder htmlBody = new StringBuilder();
                        htmlBody.Append("<html><body>");
                        htmlBody.Append("<p>Xin chào <b>" + entity.FullName + "</b>,</p>");
                        htmlBody.Append("<p>Bạn đã đăng ký tài khoản tham dự sự kiện thành công, vui lòng quét QRCode bên dưới khi checkin tại sự kiện.</p>");
                        htmlBody.Append("<p><img src=\"cid:imageUniqueId\" alt='QRCode' style='height: 500px;'> </p>");
                        htmlBody.Append("</body></html>");

                        _emailHandler.SendMailGoogleWithQRCode(new List<string>() { entity.Email }, null, null, title, htmlBody.ToString(), Convert.ToBase64String(byteImage));

                        #endregion

                        return new ResponseObject<CrmAccountQRCodeModel>(qrCodeModel, MessageConstants.CreateSuccessMessage, Code.Success);
                    }
                    else
                    {
                        return new ResponseError(Code.ServerError, MessageConstants.CreateErrorMessage);
                    }
                }
                qrCodeModel = new CrmAccountQRCodeModel()
                {
                    Id = account.Id,
                    FullName = account.FullName,
                    Email = account.Email
                };

                return new ResponseObject<CrmAccountQRCodeModel>(qrCodeModel, MessageConstants.CreateSuccessMessage, Code.Success);
            }
            catch (Exception ex)
            {
                Log.Error(ex, MessageConstants.ErrorLogMessage);
                return new ResponseError(Code.ServerError, $"{MessageConstants.CreateErrorMessage} - {ex.Message}");
            }
        }

        public async Task<Response> CreateMany(List<CrmAccountCreateModel> list)
        {
            try
            {

                var listId = new List<Guid>();
                var listRS = new List<CrmAccount>();
                foreach (var item in list)
                {
                    var entity = AutoMapperUtils.AutoMap<CrmAccountCreateModel, CrmAccount>(item);

                    entity.CreatedDate = DateTime.Now;
                    await _dataContext.CrmAccount.AddAsync(entity);
                    listId.Add(entity.Id);
                    listRS.Add(entity);
                }

                int dbSave = await _dataContext.SaveChangesAsync();

                if (dbSave > 0)
                {
                    Log.Information("Add success: " + JsonSerializer.Serialize(listRS));
                    InvalidCache();

                    return new ResponseObject<List<Guid>>(listId, MessageConstants.CreateSuccessMessage, Code.Success);
                }
                else
                {
                    return new ResponseError(Code.ServerError, MessageConstants.CreateErrorMessage);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, MessageConstants.ErrorLogMessage);
                return new ResponseError(Code.ServerError, $"{MessageConstants.CreateErrorMessage} - {ex.Message}");
            }
        }

        public async Task<Response> Update(CrmAccountUpdateModel model)
        {
            try
            {
                var entity = await _dataContext.CrmAccount
                         .FirstOrDefaultAsync(x => x.Id == model.Id);
                Log.Information("Before Update: " + JsonSerializer.Serialize(entity));

                model.UpdateToEntity(entity);

                _dataContext.CrmAccount.Update(entity);

                int dbSave = await _dataContext.SaveChangesAsync();
                if (dbSave > 0)
                {
                    Log.Information("After Update: " + JsonSerializer.Serialize(entity));
                    InvalidCache(model.Id.ToString());

                    return new ResponseObject<Guid>(model.Id, MessageConstants.UpdateSuccessMessage, Code.Success);
                }
                else
                {
                    return new ResponseError(Code.ServerError, MessageConstants.UpdateErrorMessage);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, MessageConstants.ErrorLogMessage);
                return new ResponseError(Code.ServerError, $"{MessageConstants.GetDataErrorMessage} - {ex.Message}");
            }
        }

        public async Task<Response> Delete(List<Guid> listId)
        {
            try
            {
                var listResult = new List<ResponeDeleteModel>();
                var name = "";
                Log.Information("List Delete: " + JsonSerializer.Serialize(listId));
                foreach (var item in listId)
                {
                    name = "";
                    var entity = await _dataContext.CrmAccount.FindAsync(item);

                    if (entity == null)
                    {
                        listResult.Add(new ResponeDeleteModel()
                        {
                            Id = item,
                            Name = name,
                            Result = false,
                            Message = MessageConstants.DeleteItemNotFoundMessage
                        });
                    }
                    else
                    {
                        name = entity.FullName;
                        _dataContext.CrmAccount.Remove(entity);
                        try
                        {
                            int dbSave = await _dataContext.SaveChangesAsync();
                            if (dbSave > 0)
                            {
                                InvalidCache(item.ToString());

                                listResult.Add(new ResponeDeleteModel()
                                {
                                    Id = item,
                                    Name = name,
                                    Result = true,
                                    Message = MessageConstants.DeleteItemSuccessMessage
                                });
                            }
                            else
                            {
                                listResult.Add(new ResponeDeleteModel()
                                {
                                    Id = item,
                                    Name = name,
                                    Result = false,
                                    Message = MessageConstants.DeleteItemErrorMessage
                                });
                            }
                        }
                        catch (Exception ex)
                        {
                            Log.Error(ex, MessageConstants.ErrorLogMessage);
                            listResult.Add(new ResponeDeleteModel()
                            {
                                Id = item,
                                Name = name,
                                Result = false,
                                Message = ex.Message
                            });
                        }
                    }
                }
                Log.Information("List Result Delete: " + JsonSerializer.Serialize(listResult));
                return new ResponseObject<List<ResponeDeleteModel>>(listResult, MessageConstants.DeleteSuccessMessage, Code.Success);

            }
            catch (Exception ex)
            {
                Log.Error(ex, MessageConstants.ErrorLogMessage);
                return new ResponseError(Code.ServerError, $"{MessageConstants.DeleteErrorMessage} - {ex.Message}");
            }
        }

        public async Task<Response> Filter(CrmAccountQueryFilter filter)
        {
            try
            {
                var data = (from account in _dataContext.CrmAccount

                            select new CrmAccountBaseModel()
                            {
                                Id = account.Id,
                                Code = account.Code,
                                FullName = account.FullName,
                                Address = account.Address,
                                AvatarUrl = account.AvatarUrl,
                                Birthday = account.Birthday,
                                Email = account.Email,
                                FirstName = account.FirstName,
                                LastName = account.LastName,
                                OrganizationName = account.OrganizationName,
                                PhoneNumber = account.PhoneNumber,
                                Position = account.Position,
                                Status = account.StatusDefault,
                                InfomationChannel = account.InfomationChannel,
                                CreatedDate = account.CreatedDate
                            });

                if (!string.IsNullOrEmpty(filter.TextSearch))
                {
                    string ts = filter.TextSearch.Trim().ToLower();
                    data = data.Where(x => x.FullName.ToLower().Contains(ts) || x.FullName.ToLower().Contains(ts) || x.PhoneNumber.ToLower().Contains(ts));
                }

                if (filter.Status.HasValue)
                {
                    data = data.Where(x => x.Status == filter.Status);
                }

                data = data.OrderByField(filter.PropertyName, filter.Ascending);

                int totalCount = data.Count();

                // Pagination
                if (filter.PageSize.HasValue && filter.PageNumber.HasValue)
                {
                    if (filter.PageSize <= 0)
                    {
                        filter.PageSize = QueryFilter.DefaultPageSize;
                    }

                    //Calculate nunber of rows to skip on pagesize
                    int excludedRows = (filter.PageNumber.Value - 1) * (filter.PageSize.Value);
                    if (excludedRows <= 0)
                    {
                        excludedRows = 0;
                    }

                    // Query
                    data = data.Skip(excludedRows).Take(filter.PageSize.Value);
                }
                int dataCount = data.Count();

                var listResult = await data.ToListAsync();
                return new ResponseObject<PaginationList<CrmAccountBaseModel>>(new PaginationList<CrmAccountBaseModel>()
                {
                    DataCount = dataCount,
                    TotalCount = totalCount,
                    PageNumber = filter.PageNumber ?? 0,
                    PageSize = filter.PageSize ?? 0,
                    Data = listResult
                }, MessageConstants.GetDataSuccessMessage, Code.Success);
            }
            catch (Exception ex)
            {
                Log.Error(ex, MessageConstants.ErrorLogMessage);
                return new ResponseError(Code.ServerError, $"{MessageConstants.GetDataErrorMessage} - {ex.Message}");
            }
        }

        public async Task<Response> GetById(Guid id)
        {
            try
            {
                string cacheKey = BuildCacheKey(id.ToString());
                var rs = await _cacheService.GetOrCreate(cacheKey, async () =>
                {
                    var entity = await _dataContext.CrmAccount
                        .FirstOrDefaultAsync(x => x.Id == id);

                    return AutoMapperUtils.AutoMap<CrmAccount, CrmAccountModel>(entity);
                });
                return new ResponseObject<CrmAccountModel>(rs, MessageConstants.GetDataSuccessMessage, Code.Success);
            }
            catch (Exception ex)
            {
                Log.Error(ex, MessageConstants.ErrorLogMessage);
                return new ResponseError(Code.ServerError, $"{MessageConstants.GetDataErrorMessage} - {ex.Message}");
            }
        }

        public async Task<Response> GetListCombobox(int count = 0, string textSearch = "")
        {
            try
            {
                string cacheKey = BuildCacheKey(SelectItemCacheSubfix);
                var list = await _cacheService.GetOrCreate(cacheKey, async () =>
                {
                    var data = (from item in _dataContext.CrmAccount.Where(x => x.StatusDefault == true).OrderBy(x => x.Order).ThenBy(x => x.FullName)
                                select new SelectItemModel()
                                {
                                    Id = item.Id,
                                    Name = item.FullName,
                                    Note = item.Email
                                });

                    return await data.ToListAsync();
                });

                if (!string.IsNullOrEmpty(textSearch))
                {
                    textSearch = textSearch.ToLower().Trim();
                    list = list.Where(x => x.Name.ToLower().Contains(textSearch) || x.Note.ToLower().Contains(textSearch)).ToList();
                }

                if (count > 0)
                {
                    list = list.Take(count).ToList();
                }

                return new ResponseObject<List<SelectItemModel>>(list, MessageConstants.GetDataSuccessMessage, Code.Success);
            }
            catch (Exception ex)
            {
                Log.Error(ex, MessageConstants.ErrorLogMessage);
                return new ResponseError(Code.ServerError, $"{MessageConstants.GetDataErrorMessage} - {ex.Message}");
            }
        }

        private void InvalidCache(string id = "")
        {
            if (!string.IsNullOrEmpty(id))
            {
                string cacheKey = BuildCacheKey(id);
                _cacheService.Remove(cacheKey);
            }

            string selectItemCacheKey = BuildCacheKey(SelectItemCacheSubfix);
            _cacheService.Remove(selectItemCacheKey);
        }

        private string BuildCacheKey(string id)
        {
            return $"{CachePrefix}-{id}";
        }

        private Bitmap RenderQrCode(string text, string level)
        {
            var lv = QRCodeGenerator.ECCLevel.Q;
            QRCodeGenerator.ECCLevel eccLevel = (QRCodeGenerator.ECCLevel)(level == "L" ? 0 : level == "M" ? 1 : level == "Q" ? 2 : 3);
            //QRCodeGenerator.ECCLevel eccLevel = QRCodeGenerator.ECCLevel.L;
            using (QRCodeGenerator qrGenerator = new QRCodeGenerator())
            {
                using (QRCodeData qrCodeData = qrGenerator.CreateQrCode(text, eccLevel))
                {
                    using (QRCode qrCode = new QRCode(qrCodeData))
                    {
                        var image = qrCode.GetGraphic(20);
                        return image;
                    }
                }
            }
        }

        private byte[] ImageToByte(Image img)
        {
            using (var stream = new MemoryStream())
            {
                img.Save(stream, System.Drawing.Imaging.ImageFormat.Png);
                return stream.ToArray();
            }
        }

        //private byte[] GenerateMyQCCode(string QCText)
        //{
        //    var QCwriter = new BarcodeWriter();
        //    QCwriter.Format = BarcodeFormat.QR_CODE;
        //    var result = QCwriter.Write(QCText);
        //    var barcodeBitmap = new Bitmap(result);

        //    using (MemoryStream memory = new MemoryStream())
        //    {
        //        barcodeBitmap.Save(memory, ImageFormat.Jpeg);
        //        byte[] bytes = memory.ToArray();
        //        return bytes;
        //    }

        //}
    }
}