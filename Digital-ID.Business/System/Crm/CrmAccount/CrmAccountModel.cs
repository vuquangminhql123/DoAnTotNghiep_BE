﻿using DigitalID.Data;
using System;

namespace DigitalID.Business
{
    public class CrmAccountBaseModel
    {
        public Guid Id { get; set; }
        public string Code { get; set; }
        public string FullName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string OrganizationName { get; set; }
        public string Position { get; set; }
        public DateTime? Birthday { get; set; }
        public string AvatarUrl { get; set; }
        public string Address { get; set; }
        public string InfomationChannel { get; set; }
        public bool Status { get; set; } = true;
        public DateTime? CreatedDate { get; set; }
    }

    public class CrmAccountQRCodeModel
    {
        public Guid Id { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
    }

    public class CrmAccountModel : CrmAccountBaseModel
    {

    }

    public class CrmAccountDetailModel : CrmAccountModel
    {
        public long IdentityNumber { get; set; }
        public int Order { get; set; }
        public string Description { get; set; }
        public Guid? CreatedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Guid? ModifiedUserId { get; set; }
        public Guid? ApplicationId { get; set; }
        public Guid? CompanyId { get; set; }
    }

    public class CrmAccountCreateModel : CrmAccountModel
    {
        public string Description { get; set; }
        public Guid? CreatedByUserId { get; set; }
        public Guid? ApplicationId { get; set; }
    }

    public class CrmAccountUpdateModel : CrmAccountModel
    {
        public Guid ModifiedUserId { get; set; }

        public void UpdateToEntity(CrmAccount entity)
        {
            entity.Code = this.Code;
            entity.FullName = this.FullName;
            entity.FirstName = this.FirstName;
            entity.LastName = this.LastName;
            entity.PhoneNumber = this.PhoneNumber;
            entity.Email = this.Email;
            entity.OrganizationName = this.OrganizationName;
            entity.Position = this.Position;
            entity.Birthday = this.Birthday;
            entity.AvatarUrl = this.AvatarUrl;
            entity.Address = this.Address;
            entity.InfomationChannel = this.InfomationChannel;
        }
    }

    public class CrmAccountQueryFilter
    {
        public string TextSearch { get; set; }
        public int? PageSize { get; set; }
        public int? PageNumber { get; set; }
        public bool? Status { get; set; }
        public string PropertyName { get; set; } = "CreatedDate";
        //asc - desc
        public string Ascending { get; set; } = "desc";
        public CrmAccountQueryFilter()
        {
            PageNumber = QueryFilter.DefaultPageNumber;
            PageSize = QueryFilter.DefaultPageSize;
        }
    }
}