﻿using DigitalID.Data;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DigitalID.Business
{
    /// <summary>
    /// Interface quản lý khách hàng
    /// </summary>
    public interface ICrmAccountHandler
    {
        /// <summary>
        /// Thêm mới khách hàng
        /// </summary>
        /// <param name="model">Model thêm mới khách hàng</param>
        /// <returns>Id khách hàng</returns>
        Task<Response> Create(CrmAccountCreateModel model);

        /// <summary>
        /// Thêm mới hoặc lấy thông tin khách hàng
        /// </summary>
        /// <param name="model">Model thêm mới khách hàng</param>
        /// <returns>Id khách hàng</returns>
        Task<Response> CreateOrGetDetail(CrmAccountCreateModel model);

        /// <summary>
        /// Thêm mới khách hàng theo danh sách
        /// </summary>
        /// <param name="list">Danh sách thông tin khách hàng</param>
        /// <returns>Danh sách kết quả thêm mới</returns> 
        Task<Response> CreateMany(List<CrmAccountCreateModel> list);

        /// <summary>
        /// Cập nhật khách hàng
        /// </summary>
        /// <param name="model">Model cập nhật khách hàng</param>
        /// <returns>Id khách hàng</returns>
        Task<Response> Update(CrmAccountUpdateModel model);

        /// <summary>
        /// Xóa khách hàng
        /// </summary>
        /// <param name="listId">Danh sách Id khách hàng</param>
        /// <returns>Danh sách kết quả xóa</returns>
        Task<Response> Delete(List<Guid> listId);

        /// <summary>
        /// Lấy danh sách khách hàng theo điều kiện lọc
        /// </summary>
        /// <param name="filter">Model điều kiện lọc</param>
        /// <returns>Danh sách khách hàng</returns>
        Task<Response> Filter(CrmAccountQueryFilter filter);

        /// <summary>
        /// Lấy khách hàng theo Id
        /// </summary>
        /// <param name="id">Id khách hàng</param>
        /// <returns>Thông tin khách hàng</returns>
        Task<Response> GetById(Guid id);

        /// <summary>
        /// Lấy danh sách khách hàng cho combobox
        /// </summary>
        /// <param name="count">Số bản ghi tối đa</param>
        /// <param name="textSearch">Từ khóa tìm kiếm</param>
        /// <returns>Danh sách khách hàng cho combobox</returns>
        Task<Response> GetListCombobox(int count = 0, string textSearch = "");
    }
}
