﻿using DigitalID.Data;
using System;
namespace DigitalID.Business
{
    public class BaseFormControlModel : BaseModel
    {
        public System.Guid Id { get; set; }
        public string Name { get; set; }
        public string Note { get; set; }
        public string Content { get; set; }
    }
    public class FormControlModel : BaseFormControlModel
    {
    }
    public class FormControlQueryModel : PaginationRequest
    {
        public string Name { get; set; } 
    }

    public class FormControlCreateModel
    {
        public string Name { get; set; }
        public string Note { get; set; }
        public string Content { get; set; }
    }
    public class FormControlUpdateModel
    {
        public string Name { get; set; }
        public string Note { get; set; }
        public string Content { get; set; }
    }
}

