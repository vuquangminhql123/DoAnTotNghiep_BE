﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DigitalID.Data;

namespace DigitalID.Business
{
    public interface IFormControlHandler
    {
        Task<Response> FindAsync(Guid id);
        Task<Response> CreateAsync(FormControlCreateModel model, Guid applicationId, Guid userId);
        Task<Response> UpdateAsync(Guid id, FormControlUpdateModel model, Guid applicationId, Guid userId);
        Task<Response> DeleteAsync(Guid id);
        Task<Response> DeleteRangeAsync(IList<Guid> listId);
        Task<Response> GetAllAsync();
        Task<Response> GetAllAsync(FormControlQueryModel query);
        Task<Response> GetPageAsync(FormControlQueryModel query);
        Response GetAll();
    }
}