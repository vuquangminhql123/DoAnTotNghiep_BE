﻿using LinqKit;
using DigitalID.Data;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Data.SqlClient;
using Serilog;
using System.Linq;
using Newtonsoft.Json.Linq;

namespace DigitalID.Business
{
    public class FormHandler : IFormHandler
    {
        private readonly string tablePrefix = "bmd_";
        private SqlParameter CreateSqlParameter(string columnName, dynamic columnValue, string dbType, bool isJson, string operation = null)
        {
            try
            {
                if (columnValue == null)
                {
                    return new SqlParameter("@" + columnName, DBNull.Value);
                }
                else
                {
                    switch (dbType)
                    {
                        case "int":
                            return new SqlParameter("@" + columnName, columnValue.ToObject<int>());
                        case "bit":
                            return new SqlParameter("@" + columnName, columnValue.ToObject<bool>());
                        case "nvarchar(MAX)":
                            if (!string.IsNullOrEmpty(operation) && operation == "like")
                            {
                                return new SqlParameter("@" + columnName, "%" + columnValue.ToObject<string>() + "%");
                            }
                            else
                            {
                                if (isJson)
                                    return new SqlParameter("@" + columnName, JsonConvert.SerializeObject(columnValue));
                                else
                                    return new SqlParameter("@" + columnName, columnValue.ToObject<string>());
                            }
                        case "date":
                            return new SqlParameter("@" + columnName, columnValue.ToObject<DateTime>());
                        case "datetime":
                            return new SqlParameter("@" + columnName, columnValue.ToObject<DateTime>());
                        default:
                            return new SqlParameter("@" + columnName, DBNull.Value);
                    }
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return new SqlParameter("@" + columnName, DBNull.Value);
            }
        }

        public async Task<Response> AddDataAsync(Guid masterId, dynamic formCollection, Guid applicationId, Guid userId)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {

                    string objectStr = formCollection["ObjectId"];
                    if (string.IsNullOrEmpty(objectStr))
                    {
                        return new ResponseError(Code.BadRequest, "Không xác định module liên kết!");
                    }

                    // Lấy ra biểu mẫu 
                    var formlyMasterEntity = await unitOfWork.GetRepository<BsdFormMaster>().FindAsync(x => x.Id == masterId);

                    if (formlyMasterEntity == null)
                    {
                        return new ResponseError(Code.BadRequest, "Không tìm thấy biểu mẫu!");
                    }
                    #region Generate SQL
                    var allColumn = FormHelper.GetAllColumnOfForm(formlyMasterEntity.Content);
                    string columnTable = "";
                    string valueRow = "";
                    var isfirstIndex = true;
                    var listParameter = new List<SqlParameter>();
                    foreach (var column in allColumn)
                    {
                        if (isfirstIndex)
                        {
                            isfirstIndex = false;
                            columnTable += column.columnName;
                            valueRow += "@" + column.columnName;
                        }
                        else
                        {
                            columnTable += ", " + column.columnName;
                            valueRow += ", " + "@" + column.columnName;
                        }
                        var isAddData = false;
                        foreach (var item in formCollection)
                        {
                            if (column.columnName == item.Name)
                            {
                                isAddData = true;
                                listParameter.Add(CreateSqlParameter(column.columnName, item.Value, column.dbType, column.isJson));
                            }
                        }
                        if (!isAddData)
                        {
                            listParameter.Add(new SqlParameter("@" + column.columnName, DBNull.Value));
                        }
                    }



                    string tableName = tablePrefix + formlyMasterEntity.TableName;
                    columnTable += ", Id, ObjectId, CreatedByUserId, LastModifiedByUserId, LastModifiedOnDate, CreatedOnDate, ApplicationId";
                    Guid Id = Guid.NewGuid();

                    string idStr = formCollection["Id"];
                    if (!string.IsNullOrEmpty(idStr))
                    {
                        Id = new Guid(idStr);
                    }

                    Guid objectId = new Guid(objectStr);
                    var createdOnDate = DateTime.Now;
                    valueRow += ", @Id, @ObjectId, @CreatedByUserId, @LastModifiedByUserId, @LastModifiedOnDate, @CreatedOnDate, @ApplicationId";

                    listParameter.Add(new SqlParameter("@Id", Id));
                    listParameter.Add(new SqlParameter("@ObjectId", objectId));
                    listParameter.Add(new SqlParameter("@CreatedByUserId", userId));
                    listParameter.Add(new SqlParameter("@LastModifiedByUserId", userId));
                    listParameter.Add(new SqlParameter("@LastModifiedOnDate", createdOnDate));
                    listParameter.Add(new SqlParameter("@CreatedOnDate", createdOnDate));
                    listParameter.Add(new SqlParameter("@ApplicationId", applicationId));
                    #endregion 
                    //Thêm bản ghi
                    string rawSQL = $"INSERT INTO {tableName} ({columnTable}) VALUES ({valueRow})";
                    await unitOfWork.GetRepository<BsdFormMaster>().ExecuteSqlCommandAsync(rawSQL, listParameter.ToArray());
                    return new ResponseUpdate(Id);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return new ResponseError(Code.ServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }
        public async Task<Response> UpdateDataAsync(Guid id, Guid masterId, dynamic formCollection, Guid applicationId, Guid userId)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {

                    string objectStr = formCollection["ObjectId"];
                    if (string.IsNullOrEmpty(objectStr))
                    {
                        return new ResponseError(Code.BadRequest, "Không xác định module liên kết!");
                    }

                    Guid objectId = new Guid(objectStr);
                    // Lấy ra biểu mẫu 
                    var formlyMasterEntity = await unitOfWork.GetRepository<BsdFormMaster>().FindAsync(x => x.Id == masterId);

                    if (formlyMasterEntity == null)
                    {
                        return new ResponseError(Code.BadRequest, "Không tìm thấy biểu mẫu!");
                    }
                    #region Generate SQL
                    var allColumn = FormHelper.GetAllColumnOfForm(formlyMasterEntity.Content);
                    string updateCondition = "";
                    var isfirstIndex = true;
                    var listParameter = new List<SqlParameter>();
                    foreach (var column in allColumn)
                    {
                        if (isfirstIndex)
                        {
                            isfirstIndex = false;
                            updateCondition += $"{column.columnName}= @{column.columnName}";
                        }
                        else
                        {
                            updateCondition += $" , {column.columnName}= @{column.columnName}";
                        }
                        var isAddData = false;
                        foreach (var item in formCollection)
                        {
                            if (column.columnName == item.Name)
                            {
                                isAddData = true;
                                listParameter.Add(CreateSqlParameter(column.columnName, item.Value, column.dbType, column.isJson));
                            }
                        }
                        if (!isAddData)
                        {
                            listParameter.Add(new SqlParameter("@" + column.columnName, DBNull.Value));
                        }
                    }



                    string tableName = tablePrefix + formlyMasterEntity.TableName;
                    Guid Id = Guid.NewGuid();
                    var lastModifiedOnDate = DateTime.Now;
                    updateCondition += $" , LastModifiedByUserId= @LastModifiedByUserId";
                    updateCondition += $" , LastModifiedOnDate= @LastModifiedOnDate";
                    listParameter.Add(new SqlParameter("@LastModifiedByUserId", userId));
                    listParameter.Add(new SqlParameter("@LastModifiedOnDate", lastModifiedOnDate));
                    #endregion 
                    //Thêm bản ghi
                    string rawSQL = $"Update {tableName} set {updateCondition} where Id = '{id}';";
                    await unitOfWork.GetRepository<BsdFormMaster>().ExecuteSqlCommandAsync(rawSQL, listParameter.ToArray());
                    return new ResponseUpdate(Id);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return new ResponseError(Code.ServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }
        public async Task<Response> DeleteDataAsync(Guid id, Guid masterId)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    // Lấy ra biểu mẫu 
                    var formlyMasterEntity = await unitOfWork.GetRepository<BsdFormMaster>().FindAsync(x => x.Id == masterId);

                    if (formlyMasterEntity == null)
                    {
                        return new ResponseError(Code.BadRequest, "Không tìm thấy biểu mẫu!");
                    }


                    string tableName = tablePrefix + formlyMasterEntity.TableName;
                    var rawSQL = $"DELETE FROM { tableName} where Id = '{id.ToString()}'";
                    await unitOfWork.GetRepository<BsdFormMaster>().ExecuteSqlCommandAsync(rawSQL);
                }
                return new ResponseDelete(id, "");
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return new ResponseError(Code.ServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }
        public async Task<Response> GetDataByIdAsync(Guid id, Guid masterId)

        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {

                    // Lấy ra biểu mẫu 
                    var formlyMasterEntity = await unitOfWork.GetRepository<BsdFormMaster>().FindAsync(x => x.Id == masterId);

                    if (formlyMasterEntity == null)
                    {
                        return new ResponseError(Code.BadRequest, "Không tìm thấy biểu mẫu!");
                    }
                    var allColumn = FormHelper.GetAllColumnOfForm(formlyMasterEntity.Content);

                    string tableName = tablePrefix + formlyMasterEntity.TableName;
                    string rawSQL = $"SELECT * FROM {tableName} where Id = '{id.ToString()}'";
                    var items = unitOfWork.SelectByCommand(rawSQL, new List<SqlParameter>());
                    foreach (var item in items)
                    {
                        var listColumn = allColumn.Where(x => x.isJson);
                        foreach (var column in listColumn)
                        {
                            var a = ((IDictionary<String, Object>)item)[column.columnName];
                            ((IDictionary<String, Object>)item)[column.columnName] = JsonConvert.DeserializeObject(a.ToString());

                        }
                    }
                    var result = items.FirstOrDefault();
                    if (result != null)
                    {
                        return new ResponseObject<dynamic>(result);
                    }
                    return new ResponseError(Code.NotFound, "Không có dữ liệu");

                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return new ResponseError(Code.ServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }

        }
        public async Task<Response> GetDataFilter(Guid masterId, FormDataFilter filter, List<SearchCondition> conditions)

        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {

                    // Lấy ra biểu mẫu 
                    var formlyMasterEntity = await unitOfWork.GetRepository<BsdFormMaster>().FindAsync(x => x.Id == masterId);

                    if (formlyMasterEntity == null)
                    {
                        return new ResponseError(Code.BadRequest, "Không tìm thấy biểu mẫu!");
                    }
                    #region MakeQuerry
                    string condition = "1=1";
                    var listParameter = new List<SqlParameter>();
                    var allColumn = FormHelper.GetAllColumnOfForm(formlyMasterEntity.Content);
                    var isFirst = true;
                    if (filter.ObjectId.HasValue)
                    {
                        condition += " AND ";
                        condition += $"ObjectId = @ObjectId ";
                        listParameter.Add(new SqlParameter("@ObjectId", filter.ObjectId.Value));
                    }
                    if (!string.IsNullOrEmpty(filter.FullTextSearch))
                    {

                        condition += " AND ";
                        foreach (var item in allColumn)
                        {
                            if (item.isFullTextSearch && item.dbType == "nvarchar(MAX)")
                            {
                                if (!isFirst)

                                    condition += " OR ";
                                isFirst = false;
                                condition += $"{item.columnName} {"like"} @{"TextSearch"} ";
                            }

                        }
                        listParameter.Add(new SqlParameter("@TextSearch", "%" + filter.FullTextSearch + "%"));
                    }

                    for (int i = 0; i < conditions.Count; i++)
                    {
                        var item = conditions[i];
                        condition += " AND ";
                        listParameter.Add(CreateSqlParameter(item.ColumnName, item.Value, item.dbType, item.isJson, item.Operation));
                        condition += string.Format("{0} {1} @{2} ", item.ColumnName, item.Operation, item.ColumnName);
                    }
                    if (filter.Id.HasValue)
                    {
                        condition += $"Id = '{filter.Id.Value}' ";
                    }

                    string tableName = tablePrefix + formlyMasterEntity.TableName;
                    string rawSQL = $"SELECT * FROM {tableName} where {condition}";
                    //Add Orderby
                    rawSQL += " order by CreatedOnDate asc";
                    //Count
                    string countSQL = $"SELECT COUNT(Id) FROM {tableName} where {condition}";

                    var totals = unitOfWork.CountByCommand(countSQL, new List<SqlParameter>());
                    var totalsPages = (int)Math.Ceiling(totals / (float)filter.Size.Value);
                    //Add pagination
                    #region Pagination
                    if (filter.Page.HasValue && filter.Size.HasValue)
                    {
                        if (filter.Size.Value <= 0) filter.Size = 20;

                        filter.Page = filter.Page - 1;
                        int excludedRows = (filter.Page.Value) * (filter.Page.Value);
                        if (excludedRows <= 0) excludedRows = 0;
                        rawSQL += $" OFFSET {excludedRows} ROWS  FETCH NEXT {filter.Size.Value} ROWS ONLY";
                    }

                    #endregion
                    #endregion 
                    var items = unitOfWork.SelectByCommand(rawSQL, new List<SqlParameter>());
                    foreach (var item in items)
                    {
                        var listColumn = allColumn.Where(x => x.isJson);
                        foreach (var column in listColumn)
                        {
                            var a = ((IDictionary<String, Object>)item)[column.columnName];
                            ((IDictionary<String, Object>)item)[column.columnName] = JsonConvert.DeserializeObject(a.ToString());
                        }
                    }
                    var result = items.ToList();
                    return new ResponsePagination<dynamic>(new Pagination<dynamic>()
                    {
                        Page = filter.Page.Value,
                        Content = result,
                        NumberOfElements = result.Count(),
                        Size = filter.Size.Value,
                        TotalPages = totalsPages,
                        TotalElements = totals
                    });


                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return new ResponseError(Code.ServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }

        }



    }
}
