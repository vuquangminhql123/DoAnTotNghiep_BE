﻿using LinqKit;
using DigitalID.Data;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Serilog;
using Microsoft.EntityFrameworkCore;

namespace DigitalID.Business
{
    public class FormMasterHandler : IFormMasterHandler
    {
        private readonly DbHandler<BsdFormMaster, FormMasterModel, FormMasterQueryModel> _dbHandler = DbHandler<BsdFormMaster, FormMasterModel, FormMasterQueryModel>.Instance;
        private readonly string tablePrefix = "bmd_";

        #region CRUD
        public async Task<Response> UpdateListTemplate(List<BsdFormTemplate> model, Guid masterId, Guid applicationId, Guid userId)
        {

            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    unitOfWork.GetRepository<BsdFormTemplate>().DeleteRange(x => x.MasterId == masterId);
                    foreach (var item in model)
                    {
                        item.MasterId = masterId;
                    }
                    unitOfWork.GetRepository<BsdFormTemplate>().AddRange(model);
                    if (await unitOfWork.SaveAsync() > 0)
                        return new ResponseUpdate(masterId);
                    Log.Error("The sql statement is not executed!");
                    return new ResponseError(Code.BadRequest, "Câu lệnh sql không thể thực thi");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return new ResponseError(Code.ServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }
        public Task<Response> FindAsync(Guid id)
        {
            return _dbHandler.FindAsync(id);
        }
        public async Task<Response> CreateAsync(FormMasterCreateModel model, Guid applicationId, Guid userId)
        {

            BsdFormMaster request = AutoMapperUtils.AutoMap<FormMasterCreateModel, BsdFormMaster>(model);
            request = request.InitCreate(applicationId, userId);
            request.Id = Guid.NewGuid();
            var result = await _dbHandler.CreateAsync(request, "TableName");
            await UpdateListTemplate(model.ListTemplate, request.Id, applicationId, userId);
            return result;

        }
        public async Task<Response> UpdateAsync(Guid id, FormMasterUpdateModel model, Guid applicationId, Guid userId)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    var current = await unitOfWork.GetRepository<BsdFormMaster>().FindAsync(id);
                    if (current == null)
                    {
                        Log.Error($"{id} not found");
                        return new ResponseError(Code.BadRequest, "Id không tồn tại");
                    }
                    // Check table name
                    var checkTableName = await unitOfWork.GetRepository<BsdFormMaster>().FindAsync(x => x.Id != id && x.TableName == model.TableName);
                    if (checkTableName != null)
                    {
                        Log.Error($"Bảng {tablePrefix + model.TableName} đã tồn tại");
                        return new ResponseError(Code.BadRequest, $"Bảng {model.TableName} đã tồn tại");
                    }
                    if (current.State != "APPROVED" && model.State == "APPROVED")
                    {

                        // Start create table
                        var listColumn = FormHelper.GetAllColumnOfForm(current.Content);
                        var tableName = tablePrefix + current.TableName;
                        var rawSQL = $"CREATE TABLE {tableName}( {FormHelper.GenSQLFromControlData(listColumn)} Id uniqueidentifier primary key not null, ObjectId uniqueidentifier null, CreatedOnDate datetime null,CreatedByUserId uniqueidentifier null, LastModifiedOnDate datetime null,LastModifiedByUserId uniqueidentifier null, ApplicationId uniqueidentifier null)";
                        await unitOfWork.GetRepository<BsdFormMaster>().ExecuteSqlCommandAsync(rawSQL);
                    }
                    current.Content = model.Content;
                    current.Name = model.Name;
                    current.TableName = model.TableName;
                    current.State = model.State;
                    current.InitUpdate(applicationId, userId);
                    unitOfWork.GetRepository<BsdFormMaster>().Update(current);
                    if (await unitOfWork.SaveAsync() > 0)
                    {
                        await UpdateListTemplate(model.ListTemplate, id, applicationId, userId);
                        return new ResponseUpdate(id);
                    }

                    Log.Error("The sql statement is not executed!");
                    return new ResponseError(Code.BadRequest, "Câu lệnh sql không thể thực thi");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return new ResponseError(Code.ServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }
        public async Task<Response> CloneAsync(Guid id, FormMasterUpdateModel model, Guid applicationId, Guid userId)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    var current = await unitOfWork.GetRepository<BsdFormMaster>().FindAsync(id);
                    var newId = Guid.NewGuid();
                    var modelNew = new BsdFormMaster()
                    {
                        Id = newId,
                        Content = current.Content,
                        Name = current.Name + "_Copy",
                        State = "DRAFT",
                        TableName = current.TableName + "_Copy",
                    }.InitCreate(applicationId, userId);
                    unitOfWork.GetRepository<BsdFormMaster>().Add(current);
                    if (await unitOfWork.SaveAsync() > 0)
                        return new ResponseUpdate(id);

                    Log.Error("The sql statement is not executed!");
                    return new ResponseError(Code.BadRequest, "Câu lệnh sql không thể thực thi");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return new ResponseError(Code.ServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }
        public async Task<Response> DeleteAsync(Guid id)
        {
            var result = await _dbHandler.DeleteAsync(id);
            return result;
        }
        public async Task<Response> DeleteRangeAsync(IList<Guid> listId)
        {
            var result = await _dbHandler.DeleteRangeAsync(listId, "Id");

            return result;
        }
        #endregion
        public Response GetAll()
        {
            return _dbHandler.GetAll();
        }
        public async Task<Response> GetAllAsync()
        {
            var predicate = PredicateBuilder.New<BsdFormMaster>(true);
            return await _dbHandler.GetAllAsync(predicate);
        }
        public Task<Response> GetAllAsync(FormMasterQueryModel query)
        {
            var predicate = BuildQuery(query);
            return _dbHandler.GetAllAsync(predicate, query.Sort);
        }
        public Task<Response> GetPageAsync(FormMasterQueryModel query)
        {
            var predicate = BuildQuery(query);
            return _dbHandler.GetPageAsync(predicate, query);
        }
        private Expression<Func<BsdFormMaster, bool>> BuildQuery(FormMasterQueryModel query)
        {
            var predicate = PredicateBuilder.New<BsdFormMaster>(true);
            if (!string.IsNullOrEmpty(query.Name))
            {
                predicate.And(s => s.Name == query.Name);
            }
            if (query.Id.HasValue)
            {
                predicate.And(s => s.Id == query.Id.Value);
            }
            if (query.FilterParent)
            {
                predicate.And(s => s.ParentId == query.ParentId);
            }
            if (!string.IsNullOrEmpty(query.State))
            {
                predicate.And(s => s.State == query.State);
            }
            if (query.ListId != null && query.ListId.Count > 0)
            {
                predicate.And(s => query.ListId.Contains(s.Id));
            }
            if (!string.IsNullOrEmpty(query.FullTextSearch))
            {
                predicate.And(s => s.Name.ToLower().Contains(query.FullTextSearch.ToLower()) || s.TableName.ToLower().Contains(query.FullTextSearch.ToLower()));
            }
            return predicate;
        }

        public async Task<Response> GetAllColumn(Guid masterId)

        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {

                    // Lấy ra biểu mẫu 
                    var formlyMasterEntity = await unitOfWork.GetRepository<BsdFormMaster>().FindAsync(x => x.Id == masterId);

                    if (formlyMasterEntity == null)
                    {
                        return new ResponseError(Code.BadRequest, "Không tìm thấy biểu mẫu!");
                    }
                    var allColumn = FormHelper.GetAllColumnOfForm(formlyMasterEntity.Content);


                    if (allColumn != null && allColumn.Count > 0)
                    {
                        return new ResponseList<ControlData>(allColumn);
                    }
                    return new ResponseError(Code.NotFound, "Không có dữ liệu");

                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return new ResponseError(Code.ServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }

        }

        public async Task<Response> GetAllTemplate(Guid masterId)

        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {

                    // Lấy ra biểu mẫu 
                    var listTemplate = await unitOfWork.GetRepository<BsdFormTemplate>().Get(x => x.MasterId == masterId).ToListAsync();
                    if (listTemplate == null)
                    {
                        return new ResponseError(Code.BadRequest, "Không tìm thấy biểu mẫu!");
                    }


                    if (listTemplate != null && listTemplate.Count > 0)
                    {
                        return new ResponseList<BsdFormTemplate>(listTemplate);
                    }
                    return new ResponseError(Code.NotFound, "Không có dữ liệu");

                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return new ResponseError(Code.ServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }

        }
    }
}
