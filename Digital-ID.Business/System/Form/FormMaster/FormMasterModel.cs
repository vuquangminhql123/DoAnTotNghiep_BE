﻿using DigitalID.Data;
using System;
using System.Collections.Generic;

namespace DigitalID.Business
{
    public class BaseFormMasterModel : BaseModel
    {
        public Guid Id { get; set; }
        public string Content { get; set; }
        public string Name { get; set; }
        public string TableName { get; set; }
        public string State { get; set; }
        public Guid? ParentId { get; set; }
    }
    public class FormMasterModel : BaseFormMasterModel
    {
    }
    public class FormMasterQueryModel : PaginationRequest
    {
        public string Name { get; set; }
        public string State { get; set; }
        public Guid? ParentId { get; set; }
        public bool FilterParent { get; set; }
    }

    public class FormMasterCreateModel
    {
        public string Content { get; set; }
        public string Name { get; set; }
        public string TableName { get; set; }
        public string State { get; set; } = "DRAFT";
        public Guid? ParentId { get; set; }
        public List<BsdFormTemplate> ListTemplate { get; set; }
    }
    public class FormMasterUpdateModel
    {
        public string Content { get; set; }
        public string Name { get; set; }
        public string TableName { get; set; }
        public string State { get; set; }
        public List<BsdFormTemplate> ListTemplate { get; set; }
    }
}

