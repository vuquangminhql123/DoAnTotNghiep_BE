﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DigitalID.Data;

namespace DigitalID.Business
{
    public interface IFormMasterHandler
    {
        Task<Response> FindAsync(Guid id);
        Task<Response> CreateAsync(FormMasterCreateModel model, Guid applicationId, Guid userId);
        Task<Response> UpdateAsync(Guid id, FormMasterUpdateModel model, Guid applicationId, Guid userId);
        Task<Response> DeleteAsync(Guid id);
        Task<Response> DeleteRangeAsync(IList<Guid> listId);
        Task<Response> GetAllAsync();
        Task<Response> GetAllAsync(FormMasterQueryModel query);
        Task<Response> GetPageAsync(FormMasterQueryModel query);
        Response GetAll();
        Task<Response> GetAllColumn(Guid masterId);
        Task<Response> GetAllTemplate(Guid masterId);
    }
}