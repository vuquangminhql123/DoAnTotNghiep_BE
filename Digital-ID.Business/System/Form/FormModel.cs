﻿using DigitalID.Data;
using System;
using System.Collections.Generic;

namespace DigitalID.Business
{
    //Thông tin của layout
    public class Layout
    {
        public List<LayoutItem> fieldGroup { get; set; }
    }
    public class LayoutItem
    {
        public ControlData data { get; set; }
    }
    public class ControlData
    {
        // 0:control 1html5
        public int type { get; set; }
        // Định danh control trong Form [dùng làm tên cột]
        public string columnName { get; set; }
        // Giá trị mặc định của control
        public object defaultValue { get; set; }
        //Kiểu dữ liệu lưu trữ
        public string dbType { get; set; }
        // Tên hiển thị của cột
        public string columnDisplayName { get; set; }
        // Là loại JSON
        public bool isJson { get; set; }
        // Hiển thị ở danh sách
        public bool showOnList { get; set; }
        // Ẩn ở cột chi tiết
        public bool hideOnDetail { get; set; }
        // Tìm kiếm full Text
        public bool isFullTextSearch { get; set; }
    }
    public class SearchCondition
    {
        public string ColumnName { get; set; }
        public string Operation { get; set; }
        public dynamic Value { get; set; }
        public string dbType { get; set; }
        public bool isJson { get; set; }
    }
    public class FormDataFilter : PaginationRequest
    {
        public Guid? ObjectId { get; set; }
    }
}

