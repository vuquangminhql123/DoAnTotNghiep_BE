﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using Serilog;

namespace DigitalID.Business
{
    public static class FormHelper
    {
        public static IList<ControlData> GetAllColumnOfForm(string formContent)
        {
            try
            {
                var listLayOut = JsonConvert.DeserializeObject<List<Layout>>(formContent);
                var result = new List<ControlData>();
                foreach (var layout in listLayOut)
                {
                    foreach (var field in layout.fieldGroup)
                    {
                        if (field.data.type == 0)
                            result.Add(field.data);
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return null;
            }
        }
        public static string GenSQLFromControlData(IList<ControlData> listData)
        {
            var result = "";
            foreach (var data in listData)
            {
                result += string.Format("{0} {1} NULL ,", data.columnName, data.dbType);
            }
            return result;
        }
    }
}