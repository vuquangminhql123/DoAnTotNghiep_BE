﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DigitalID.Data;

namespace DigitalID.Business
{
    public interface IFormHandler
    {
       

        Task<Response> AddDataAsync(Guid masterId, dynamic formCollection, Guid applicationId, Guid userId);
        Task<Response> UpdateDataAsync(Guid id, Guid masterId, dynamic formCollection, Guid applicationId, Guid userId);
        Task<Response> DeleteDataAsync(Guid id, Guid masterId);
        Task<Response> GetDataByIdAsync(Guid id, Guid masterId);
        Task<Response> GetDataFilter(Guid masterId, FormDataFilter filter, List<SearchCondition> conditions);
    }
}