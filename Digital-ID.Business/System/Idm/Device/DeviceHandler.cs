﻿using DigitalID.Data;
using Newtonsoft.Json;
using QRCoder;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;

namespace DigitalID.Business
{
    public class DeviceHandler : IDeviceHandler
    {
        private readonly DbHandler<IdmDevice, DeviceModel, DeviceQueryModel> _dbHandler = DbHandler<IdmDevice, DeviceModel, DeviceQueryModel>.Instance;
        public async Task<Response> CreateAsync(DeviceCreateUpdateModel model, Guid applicationId, Guid userId)
        {
            try
            {
                IdmDevice itemUpdate = AutoMapperUtils.AutoMap<DeviceCreateUpdateModel, IdmDevice>(model);
                itemUpdate = itemUpdate.InitCreate(applicationId, userId);
                itemUpdate.Id = Guid.NewGuid();
                var result = await _dbHandler.CreateAsync(itemUpdate);
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ResponseV2<string> GetDeviceTokenForUser(string userName)
        {
            using (var unitOfWork = new UnitOfWork())
            {
                var user = unitOfWork.GetRepository<IdmUser>().Find(x => x.UserName == userName && x.IsLocked == false);
                if (user == null)
                    return new ResponseV2<string>(0, "Not found User", "");
                var devices = unitOfWork.GetRepository<IdmDevice>().GetMany(x => x.UserId == user.Id && x.Status == true && x.DeviceType != null && x.DeviceType != "");
                if (devices == null || devices.ToList().Count == 0)
                    return new ResponseV2<string>(0, "Not found Device", "");
                List<DeviceReturnModel> dataReturn = new List<DeviceReturnModel>();
                foreach (var item in devices)
                {
                    dataReturn.Add(new DeviceReturnModel() { DeviceToken = item.DeviceToken, DeviceType = item.DeviceType });
                }
                string rt = JsonConvert.SerializeObject(dataReturn);
                return new ResponseV2<string>(1, "Success", rt);
            }
        }

        public ResponseV2<BaseUserModel> UpdateAsync(DeviceCreateUpdateModel model, Guid userId)
        {
            try
            {
                BaseUserModel rt = new BaseUserModel();
                using (var unitOfWork = new UnitOfWork())
                {
                    #region Check App
                    var checkApp = unitOfWork.GetRepository<SysApplication>().Find(x => x.Id == model.ApplicationId);
                    if(checkApp == null)
                        return new ResponseV2<BaseUserModel>(0, "App not found", null);
                    #endregion

                    #region Lấy thông tin user trả về
                    var u = unitOfWork.GetRepository<IdmUser>().Find(x => x.Id == userId);
                    if (u == null)
                        return new ResponseV2<BaseUserModel>(0, "User not found", null);
                    rt.Id = userId;
                    rt.UserName = u.UserName;
                    rt.Name = u.Name;
                    rt.IsLocked = u.IsLocked;

                    string apiLink = "/api/v1/service/certbyuser?userName="+ u.UserName + "";//API get cert for QR
                    QRCodeGenerator qrGenerator = new QRCodeGenerator();
                    string content = "{\"uid\":\"" + u.Id + "\",\"uri\":\"" + apiLink + "\"}";
                    QRCodeData qrCodeData = qrGenerator.CreateQrCode(content, QRCodeGenerator.ECCLevel.Q);
                    QRCode qrCode = new QRCode(qrCodeData);
                    Bitmap qrCodeImage = qrCode.GetGraphic(20);
                    var byteQR = Utils.BitmapToBytes(qrCodeImage);
                    rt.QrCodeString = Convert.ToBase64String(byteQR);
                    #endregion

                    #region check thông tin device và lưu vào DB
                    var itemUpdate = unitOfWork.GetRepository<IdmDevice>().Find(x => x.UserId == userId 
                    && x.DeviceType == model.DeviceType && x.ApplicationId == model.ApplicationId 
                    && x.DeviceToken == model.DeviceToken);
                    if (itemUpdate != null)
                    {
                        itemUpdate.ApplicationId = Guid.Empty;
                        itemUpdate.LastModifiedByUserId = userId;
                        itemUpdate.LastModifiedOnDate = DateTime.Now;
                        itemUpdate.DeviceId = model.DeviceId;
                        itemUpdate.DeviceToken = model.DeviceToken;
                        itemUpdate.DeviceType = model.DeviceType;
                        itemUpdate.Status = model.Status;
                        itemUpdate.UserId = userId;
                        itemUpdate.ApplicationId = model.ApplicationId;
                        unitOfWork.GetRepository<IdmDevice>().Update(itemUpdate);
                    }
                    else
                    {
                        itemUpdate = new IdmDevice();
                        itemUpdate.Id = Guid.NewGuid();
                        itemUpdate.ApplicationId = Guid.Empty;
                        itemUpdate.CreatedByUserId = userId;
                        itemUpdate.CreatedOnDate = DateTime.Now;
                        itemUpdate.LastModifiedByUserId = userId;
                        itemUpdate.LastModifiedOnDate = DateTime.Now;
                        itemUpdate.DeviceId = model.DeviceId;
                        itemUpdate.DeviceToken = model.DeviceToken;
                        itemUpdate.DeviceType = model.DeviceType;
                        itemUpdate.Status = model.Status;
                        itemUpdate.UserId = userId;
                        itemUpdate.ApplicationId = model.ApplicationId;
                        unitOfWork.GetRepository<IdmDevice>().Add(itemUpdate);
                    }
                    #endregion;

                    #region Update trạng thái scan QRcode
                    var qr = unitOfWork.GetRepository<IdmQRCode>().Find(x => x.UserId == userId && x.ApplicationId == model.ApplicationId);
                    if(qr == null)
                        return new ResponseV2<BaseUserModel>(0, "QRCode info not found", null);
                    //if(qr.status == 1)
                        //return new ResponseV2<BaseUserModel>(0, "QRCode scanned", null);
                    //qr.status = 1;
                    //unitOfWork.GetRepository<IdmQRCode>().Update(qr);
                    #endregion

                    var result = unitOfWork.Save();
                    if (result > 0)
                    {
                        return new ResponseV2<BaseUserModel>(1, "SUCCESS", rt);
                    }
                    else
                    {
                        return new ResponseV2<BaseUserModel>(-1, "Something wrong", null);
                    }
                }
            }
            catch (Exception ex)
            {
                return new ResponseV2<BaseUserModel>(-1, "Error: " + ex.Message, null);
            }
        }
    }
}