﻿using DigitalID.Data;
using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace DigitalID.Business
{
    public class BaseDeviceModel : BaseModel
    {
        public Guid Id { get; set; }
        public new Guid? CreatedByUserId { get; set; }
        public new Guid? LastModifiedByUserId { get; set; }
        public new DateTime? LastModifiedOnDate { get; set; }
        public new DateTime? CreatedOnDate { get; set; }
        public new Guid ApplicationId { get; set; }
        [JsonProperty("user_id")]
        public Guid UserId { get; set; }

        [JsonProperty("device_id")]
        public string DeviceId { get; set; }

        [JsonProperty("device_token")]
        public string DeviceToken { get; set; }

        [JsonProperty("device_type")]
        public string DeviceType { get; set; }

        [JsonProperty("status")]
        public bool Status { get; set; }

    }
    public class DeviceModel : BaseDeviceModel
    {
    }

    public class DeviceQueryModel : PaginationRequest
    {
        public string DeviceId { get; set; }
        public string DeviceToken { get; set; }
    }

    public class DeviceCreateUpdateModel
    {
        [JsonProperty("user_id")]
        public Guid UserId { get; set; }
        [JsonProperty("app_id")]
        public Guid ApplicationId { get; set; }

        [JsonProperty("device_id")]
        public string DeviceId { get; set; }

        [JsonProperty("device_token")]
        public string DeviceToken { get; set; }

        [JsonProperty("device_type")]
        public string DeviceType { get; set; }

        [JsonProperty("status")]
        public bool Status { get; set; }
    }

    public class DeviceReturnModel
    {
        [JsonProperty("device_token")]
        public string DeviceToken { get; set; }

        [JsonProperty("device_type")]
        public string DeviceType { get; set; }
    }
}

