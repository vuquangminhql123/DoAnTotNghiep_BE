﻿using DigitalID.Data;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DigitalID.Business
{
    public interface IDeviceHandler
    {
        Task<Response> CreateAsync(DeviceCreateUpdateModel model, Guid applicationId, Guid userId);
        ResponseV2<BaseUserModel> UpdateAsync(DeviceCreateUpdateModel model, Guid userId);
        ResponseV2<string> GetDeviceTokenForUser(string userName);
    }
}
