﻿using DigitalID.Data;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DigitalID.Business
{
    public interface IRightHandler
    {
        Task<Response> CreateAsync(RightCreateModel model, Guid? appId, Guid? actorId);
        Task<Response> CreateAsync(Guid id, RightCreateModel model, Guid? appId, Guid? actorId);
        Task<Response> UpdateAsync(Guid id, RightUpdateModel model, Guid? appId, Guid? actorId);
        Task<Response> DeleteAsync(Guid id);
        Task<Response> DeleteRangeAsync(IList<Guid> listId);
        Task<Response> FindAsync(Guid id);
        Task<Response> GetAllAsync();
        Task<Response> GetAllAsync(RightQueryModel query);
        Task<Response> GetPageAsync(RightQueryModel query);
        Task<Response> GetDetail(Guid rightId, Guid applicationId);
        Response GetAll();
    }
}
