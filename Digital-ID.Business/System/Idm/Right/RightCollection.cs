﻿using DigitalID.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace DigitalID.Business
{
    public class RightCollection
    {
        private readonly IRightHandler _handler;
        public HashSet<RightModel> Collection;
        public static RightCollection Instance { get; } = new RightCollection();

        protected RightCollection()
        {
            _handler = new RightHandler();
            LoadToHashSet();
        }

        public void LoadToHashSet()
        {
            Collection = new HashSet<RightModel>();
            // Query to list
            var listResponse = _handler.GetAll();
            // Add to hashset
            if (listResponse.Code == Code.Success)
            {
                // Add to hashset
                if (listResponse is ResponseList<RightModel> listResponseData)
                    foreach (var response in listResponseData.Data)
                    {
                        Collection.Add(response);
                    }
            }
        }
        public string GetName(Guid id)
        {
            var result = Collection.FirstOrDefault(u => u.Id == id);
            return result?.Name;
        }
        public BaseRightModel GetModel(Guid id)
        {
            var result = Collection.FirstOrDefault(u => u.Id == id);
            return result;
        }
        public BaseRightModel GetModel(string userName)
        {
            var result = Collection.FirstOrDefault(u => u.Name == userName);
            return result;
        }
        public List<BaseRightModel> GetModel(Expression<Func<BaseRightModel, bool>> predicate)
        {
            var result = Collection.AsQueryable().Where(predicate);
            return result.ToList();
        }
    }
}