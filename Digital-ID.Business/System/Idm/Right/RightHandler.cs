﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using LinqKit;
using DigitalID.Data;
using Serilog;

namespace DigitalID.Business
{
    public class RightHandler : IRightHandler
    {
        private readonly DbHandler<IdmRight, RightModel, RightQueryModel> _dbHandler =
            DbHandler<IdmRight, RightModel, RightQueryModel>.Instance;

        private readonly IRightMapRoleHandler _rightMapRoleHandler;
        private readonly IRightMapUserHandler _rightMapUserHandler;

        public RightHandler(IRightMapRoleHandler rightMapRoleHandler, IRightMapUserHandler rightMapUserHandler)
        {
            _rightMapRoleHandler = rightMapRoleHandler;
            _rightMapUserHandler = rightMapUserHandler;
        }

        public RightHandler()
        {
        }

        public Task<Response> GetAllAsync()
        {
            var predicate = PredicateBuilder.New<IdmRight>(true);
            return _dbHandler.GetAllAsync(predicate);
        }

        public Task<Response> GetAllAsync(RightQueryModel query)
        {
            var predicate = BuildQuery(query);
            return _dbHandler.GetAllAsync(predicate, query.Sort);
        }

        public Response GetAll()
        {
            return _dbHandler.GetAll();
        }

        public Task<Response> GetPageAsync(RightQueryModel query)
        {
            var predicate = BuildQuery(query);
            return _dbHandler.GetPageAsync(predicate, query);
        }

        public async Task<Response> GetDetail(Guid rightId, Guid applicationId)
        {
            var model = await _dbHandler.FindAsync(rightId);
            var modelData = model as ResponseObject<RightModel>;
            if (model.Code == Code.Success)
            {
                if (modelData != null)
                {
                    var result = AutoMapperUtils.AutoMap<RightModel, RightDetailModel>(modelData.Data);
                    var listRoleResponse = await _rightMapRoleHandler.GetRoleMapRightAsync(rightId, applicationId);
                    var listUserResponse = await _rightMapUserHandler.GetUserMapRightAsync(rightId, applicationId);
                    if (listRoleResponse.Code == Code.Success && listUserResponse.Code == Code.Success)
                    {
                        var listRoleResponseData = listRoleResponse as ResponseList<BaseRoleModel>;
                        var listUserResponseData = listUserResponse as ResponseList<BaseUserModel>;
                        if (listRoleResponseData != null) result.ListRole = listRoleResponseData.Data;
                        if (listUserResponseData != null) result.ListUser = listUserResponseData.Data;
                        return new ResponseObject<RightDetailModel>(result);
                    }
                }

                return new ResponseError(Code.BadRequest, "Không thể lấy dữ liệu  chi tiết");
            }

            return model;
        }

        private static Expression<Func<IdmRight, bool>> BuildQuery(RightQueryModel query)
        {
            var predicate = PredicateBuilder.New<IdmRight>(true);
            if (!string.IsNullOrEmpty(query.Code)) predicate.And(s => s.Code == query.Code);
            if (!string.IsNullOrEmpty(query.Name)) predicate.And(s => s.Name == query.Name);
            if (query.Id.HasValue) predicate.And(s => s.Id == query.Id);
            if (query.ListId != null) predicate.And(s => query.ListId.Contains(s.Id));

            if (!string.IsNullOrEmpty(query.FullTextSearch))
                predicate.And(s =>
                    s.Code.ToLower().Contains(query.FullTextSearch.ToLower()) || s.Name.ToLower().Contains(query.FullTextSearch.ToLower()) ||
                    s.Description.ToLower().Contains(query.FullTextSearch.ToLower()));
            return predicate;
        }

        #region CRUD 

        public async Task<Response> CreateAsync(RightCreateModel model, Guid? appId, Guid? actorId)
        {
            var id = Guid.NewGuid();
            var result = await CreateAsync(id, model, appId, actorId);
            return result;
        }

        public async Task<Response> CreateAsync(Guid id, RightCreateModel model, Guid? appId, Guid? actorId)
        {
            var request = AutoMapperUtils.AutoMap<RightCreateModel, IdmRight>(model);
            request.Id = id;
            var result = await _dbHandler.CreateAsync(request);
            if (result.Code == Code.Success)
            {
                RightCollection.Instance.LoadToHashSet();

                #region Realtive

                if (model.ListAddUserId != null)
                    await _rightMapUserHandler.AddRightMapUserAsync(model.ListAddUserId, request.Id, model.ApplicatonId,
                        appId, actorId);
                if (model.ListAddRoleId != null)
                    await _rightMapRoleHandler.AddRightMapRoleAsync(model.ListAddRoleId, request.Id, model.ApplicatonId,
                        appId, actorId);

                #endregion
            }

            return result;
        }

        public async Task<Response> UpdateAsync(Guid id, RightUpdateModel model, Guid? appId, Guid? actorId)
        {
            var request = AutoMapperUtils.AutoMap<RightUpdateModel, IdmRight>(model);
            request.Id = id;
            var result = await _dbHandler.UpdateAsync(id, request);
            if (result.Code == Code.Success)
            {
                RightCollection.Instance.LoadToHashSet();

                #region Realtive

                if (model.ListAddUserId != null)
                    await _rightMapUserHandler.AddRightMapUserAsync(model.ListAddUserId, id, model.ApplicatonId, appId,
                        actorId);
                if (model.ListAddRoleId != null)
                    await _rightMapRoleHandler.AddRightMapRoleAsync(model.ListAddRoleId, id, model.ApplicatonId, appId,
                        actorId);
                if (model.ListDeleteRoleId != null)
                    await _rightMapUserHandler.DeleteRightMapUserAsync(model.ListAddUserId, id, model.ApplicatonId);
                if (model.ListDeleteUserId != null)
                    await _rightMapRoleHandler.DeleteRightMapRoleAsync(model.ListDeleteUserId, id, model.ApplicatonId);

                #endregion
            }

            return result;
        }

        public async Task<Response> DeleteAsync(Guid id)
        {
              try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    /// Delete user map role
                    unitOfWork.GetRepository<IdmRightMapRole>().DeleteRange(s => s.RightId == id);
                    /// Delete user map right
                    unitOfWork.GetRepository<IdmRightMapUser>().DeleteRange(s => s.RightId == id);

                    await unitOfWork.SaveAsync();
                }
                var result = await _dbHandler.DeleteAsync(id);
                RightCollection.Instance.LoadToHashSet();
                return result;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return new ResponseError(Code.ServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }

         
        }

        public async Task<Response> DeleteRangeAsync(IList<Guid> listId)
        {
            var result = await _dbHandler.DeleteRangeAsync(listId, "Id");

            RightCollection.Instance.LoadToHashSet();
            return result;
        }

        public Task<Response> FindAsync(Guid id)
        {
            return _dbHandler.FindAsync(id);
        }

        #endregion
    }
}