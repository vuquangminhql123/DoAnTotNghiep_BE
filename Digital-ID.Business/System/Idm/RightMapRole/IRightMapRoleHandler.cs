﻿using DigitalID.Data;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DigitalID.Business
{
    public interface IRightMapRoleHandler
    {

        /// <summary>
        /// Xóa một quyền trong nhóm người dùng
        /// </summary>
        /// <param name="roleId"></param>
        /// <param name="rightId"></param>
        /// <param name="applicationId"></param>
        /// <returns></returns>
        Task<Response> DeleteRightMapRoleAsync(Guid roleId, Guid rightId, Guid applicationId);
        /// <summary>
        /// Xóa nhiều quyền trong nhóm người dùng
        /// </summary>
        /// <param name="roleId"></param>
        /// <param name="listRightId"></param>
        /// <param name="applicationId"></param>
        /// <returns></returns>
        Task<Response> DeleteRightMapRoleAsync(Guid roleId, IList<Guid> listRightId, Guid applicationId);
        /// <summary>
        /// Xóa nhiều nhóm chứa quyền
        /// </summary>
        /// <param name="listRoleId"></param>
        /// <param name="applicationId"></param>
        /// <param name="rightId"></param>
        /// <returns></returns>
        Task<Response> DeleteRightMapRoleAsync(IList<Guid> listRoleId, Guid rightId, Guid applicationId);
        /// <summary>
        /// Thêm một quyền trong nhóm người dùng
        /// </summary>
        /// <param name="roleId"></param>
        /// <param name="rightId"></param>
        /// <param name="applicationId"></param>
        /// <param name="appId"></param>
        /// <param name="actorId"></param>
        /// <returns></returns>
        Task<Response> AddRightMapRoleAsync(Guid roleId, Guid rightId, Guid applicationId, Guid? appId, Guid? actorId);
        /// <summary>
        /// Thêm nhiều quyền trong nhóm người dùng
        /// </summary>
        /// <param name="roleId"></param>
        /// <param name="listRightId"></param>
        /// <param name="applicationId"></param>
        /// <param name="appId"></param>
        /// <param name="actorId"></param>
        /// <returns></returns>
        Task<Response> AddRightMapRoleAsync(Guid roleId, IList<Guid> listRightId, Guid applicationId, Guid? appId, Guid? actorId);
        /// <summary>
        /// Thêm nhiều nhóm chứa quyền
        /// </summary>
        /// <param name="listRoleId"></param>
        /// <param name="rightId"></param>
        /// <param name="applicationId"></param>
        /// <param name="appId"></param>
        /// <param name="actorId"></param>
        /// <returns></returns>
        Task<Response> AddRightMapRoleAsync(IList<Guid> listRoleId, Guid rightId, Guid applicationId, Guid? appId, Guid? actorId);
        /// <summary>
        /// Lấy sanh sách quyền của 1 nhóm người dùng
        /// </summary>
        /// <param name="roleId"></param>
        /// <param name="applicationId"></param>
        /// <returns></returns>
        Task<Response> GetRightMapRoleAsync(Guid roleId, Guid applicationId);
        /// <summary>
        /// Lấy danh sách nhóm người dùng chứa quyền
        /// </summary>
        /// <param name="rightId"></param>
        /// <param name="applicationId"></param>
        /// <returns></returns>
        Task<Response> GetRoleMapRightAsync(Guid rightId, Guid applicationId);

        /// <summary>
        /// Kiểm tra nhóm người dùng có quyền 
        /// </summary>
        /// <param name="roleId"></param>
        /// <param name="rightId"></param>
        /// <param name="applicationId"></param>
        /// <returns></returns>
        Task<Response> CheckRightMapRoleAsync(Guid roleId, Guid rightId, Guid applicationId);
    }
}
