﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DigitalID.Data;
using Microsoft.EntityFrameworkCore;
using Serilog;

namespace DigitalID.Business
{
    public class RightMapRoleHandler : IRightMapRoleHandler
    {
        public async Task<Response> DeleteRightMapRoleAsync(Guid roleId, Guid rightId, Guid applicationId)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    #region Check

                    var currentMap = await unitOfWork.GetRepository<IdmRightMapRole>().FindAsync(s =>
                        s.RoleId == roleId && s.RightId == rightId && s.ApplicationId == applicationId);
                    if (currentMap == null)
                        return new ResponseError(Code.BadRequest, "Nhóm người dùng không có quyền");

                    #endregion

                    //Delete Mapp
                    unitOfWork.GetRepository<IdmRightMapRole>().Delete(currentMap);
                    //Update child
                    var roleIdString = roleId.ToString();
                    var currentRmu = await (from rou in unitOfWork.GetRepository<IdmRightMapUser>().GetAll()
                        where rou.RightId == rightId && rou.InheritedFromRoles.Contains(roleIdString) &&
                              rou.ApplicationId == applicationId
                        select rou).ToListAsync();

                    var listItemDelete = new List<IdmRightMapUser>();

                    foreach (var mapItem in currentRmu)
                    {
                        //Nếu InheritedFromRoles chỉ chứa mình quyền đó =>> xóa
                        //Nếu không khai trừ role ra khỏi InheritedFromRoles 
                        var listRole = IdmHelper.LoadRolesInherited(mapItem.InheritedFromRoles);
                        if (listRole.Count > 1)
                        {
                            if (listRole.Contains(IdmHelper.MakeIndependentPermission())
                            ) //nếu có 2 inherist với 1 cái là self 
                                mapItem.Inherited = false;
                            mapItem.InheritedFromRoles =
                                IdmHelper.RemoveRolesInherited(mapItem.InheritedFromRoles, roleId);
                            unitOfWork.GetRepository<IdmRightMapUser>().Update(mapItem);
                        }
                        else
                        {
                            listItemDelete.Add(mapItem);
                        }
                    }

                    if (listItemDelete.Count == 0)
                        unitOfWork.GetRepository<IdmRightMapUser>().DeleteRange(listItemDelete);

                    if (await unitOfWork.SaveAsync() > 0)
                        return new Response(Code.Success, "Loại quyền của nhóm người dùng thành công");
                    Log.Error("The sql statement is not executed!");
                    return new ResponseError(Code.BadRequest, "Câu lệnh sql không thể thực thi");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return new ResponseError(Code.ServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }

        public async Task<Response> DeleteRightMapRoleAsync(Guid roleId, IList<Guid> listRightId, Guid applicationId)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    #region Check

                    var listCurrentMap = await unitOfWork.GetRepository<IdmRightMapRole>().GetListAsync(s =>
                        s.RoleId == roleId && listRightId.Contains(s.RightId) && s.ApplicationId == applicationId);
                    if (listCurrentMap.Count == 0)
                        return new ResponseError(Code.BadRequest, "Nhóm người dùng không có quyền nào");

                    #endregion

                    //Delete Mapp
                    unitOfWork.GetRepository<IdmRightMapRole>().DeleteRange(listCurrentMap.ToArray());
                    //Update child
                    var roleIdString = roleId.ToString();
                    var currentRmu = await (from rou in unitOfWork.GetRepository<IdmRightMapUser>().GetAll()
                        where listRightId.Contains(rou.RightId) && rou.InheritedFromRoles.Contains(roleIdString) &&
                              rou.ApplicationId == applicationId
                        select rou).ToListAsync();


                    var listItemDelete = new List<IdmRightMapUser>();
                    foreach (var mapItem in currentRmu)
                    {
                        //Nếu InheritedFromRoles chỉ chứa mình quyền đó =>> xóa
                        //Nếu không khai trừ role ra khỏi InheritedFromRoles 
                        var listRole = IdmHelper.LoadRolesInherited(mapItem.InheritedFromRoles);
                        if (listRole.Count > 1)
                        {
                            if (listRole.Contains(IdmHelper.MakeIndependentPermission())
                            ) //nếu có 2 inherist với 1 cái là self 
                                mapItem.Inherited = false;
                            mapItem.InheritedFromRoles =
                                IdmHelper.RemoveRolesInherited(mapItem.InheritedFromRoles, roleId);
                            unitOfWork.GetRepository<IdmRightMapUser>().Update(mapItem);
                        }
                        else
                        {
                            listItemDelete.Add(mapItem);
                        }
                    }

                    if (listItemDelete.Count == 0)
                        unitOfWork.GetRepository<IdmRightMapUser>().DeleteRange(listItemDelete);


                    if (await unitOfWork.SaveAsync() > 0)
                        return new Response(Code.Success, "Loại danh sách quyền của nhóm người dùng thành công");
                    Log.Error("The sql statement is not executed!");
                    return new ResponseError(Code.BadRequest, "Câu lệnh sql không thể thực thi");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return new ResponseError(Code.ServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }

        public async Task<Response> DeleteRightMapRoleAsync(IList<Guid> listRoleId, Guid rightId, Guid applicationId)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    #region Check

                    var listCurrentMap = await unitOfWork.GetRepository<IdmRightMapRole>().GetListAsync(s =>
                        s.RightId == rightId && listRoleId.Contains(s.RoleId) && s.ApplicationId == applicationId);
                    if (listCurrentMap.Count == 0)
                        return new ResponseError(Code.BadRequest, "Không nhóm người dùng nào có quyền");

                    #endregion

                    //Delete Mapp
                    unitOfWork.GetRepository<IdmRightMapRole>().DeleteRange(listCurrentMap.ToArray());
                    //Update child 
                    var listItemDelete = new List<IdmRightMapUser>();
                    foreach (var roleId in listRoleId)
                    {
                        var roleIdString = roleId.ToString();
                        var currentRmu = await (from rou in unitOfWork.GetRepository<IdmRightMapUser>().GetAll()
                            where rou.RightId == rightId && rou.InheritedFromRoles.Contains(roleIdString) &&
                                  rou.ApplicationId == applicationId
                            select rou).ToListAsync();


                        foreach (var mapItem in currentRmu)
                        {
                            //Nếu InheritedFromRoles chỉ chứa mình quyền đó =>> xóa
                            //Nếu không khai trừ role ra khỏi InheritedFromRoles 
                            var listRole = IdmHelper.LoadRolesInherited(mapItem.InheritedFromRoles);
                            if (listRole.Count > 1)
                            {
                                if (listRole.Contains(IdmHelper.MakeIndependentPermission())
                                ) //nếu có 2 inherist với 1 cái là self 
                                    mapItem.Inherited = false;
                                mapItem.InheritedFromRoles =
                                    IdmHelper.RemoveRolesInherited(mapItem.InheritedFromRoles, roleId);
                                unitOfWork.GetRepository<IdmRightMapUser>().Update(mapItem);
                            }
                            else
                            {
                                listItemDelete.Add(mapItem);
                            }
                        }
                    }

                    if (listItemDelete.Count == 0)
                        unitOfWork.GetRepository<IdmRightMapUser>().DeleteRange(listItemDelete);
                    if (await unitOfWork.SaveAsync() > 0)
                        return new Response(Code.Success, "Loại quyền của danh sách nhóm người dùng thành công");
                    Log.Error("The sql statement is not executed!");
                    return new ResponseError(Code.BadRequest, "Câu lệnh sql không thể thực thi");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return new ResponseError(Code.ServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }

        public async Task<Response> AddRightMapRoleAsync(Guid roleId, Guid rightId, Guid applicationId, Guid? appId,
            Guid? actorId)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    #region Check

                    var currentMap = await unitOfWork.GetRepository<IdmRightMapRole>().FindAsync(s =>
                        s.RoleId == roleId && s.RightId == rightId && s.ApplicationId == applicationId);
                    if (currentMap != null)
                        return new ResponseError(Code.BadRequest, "Nhóm người dùng đã có quyền");
                    var roleModel = RoleCollection.Instance.GetModel(roleId);
                    if (roleModel == null)
                        return new ResponseError(Code.BadRequest, "Nhóm người dùng không tồn tại!");
                    var rightModel = RightCollection.Instance.GetModel(rightId);
                    if (rightModel == null)
                        return new ResponseError(Code.BadRequest, "Quyền không tồn tại!");

                    #endregion

                    unitOfWork.GetRepository<IdmRightMapRole>().Add(new IdmRightMapRole
                    {
                        RightId = rightId,
                        RoleId = roleId,
                        ApplicationId = applicationId
                    }.InitCreate(applicationId, actorId));
                    /*Thêm mới right mapp user của user kế thừa*/
                    var childUsers = await unitOfWork.GetRepository<IdmUserMapRole>()
                        .Get(s => s.RoleId == roleId && s.ApplicationId == applicationId).Select(s => s.UserId)
                        .ToListAsync();
                    var listCurrentRmu = await unitOfWork.GetRepository<IdmRightMapUser>().GetListAsync(s =>
                        childUsers.Contains(s.UserId) && s.RightId == rightId && s.ApplicationId == applicationId);
                    var listRmuToAdd = new List<IdmRightMapUser>();
                    foreach (var currentRmu in listCurrentRmu)
                    {
                        currentRmu.InheritedFromRoles =
                            IdmHelper.AddRolesInherited(currentRmu.InheritedFromRoles, roleId);
                        currentRmu.Inherited = true;
                        currentRmu.Enable = true;
                        unitOfWork.GetRepository<IdmRightMapUser>().Update(currentRmu.InitUpdate(appId, actorId));
                    }

                    foreach (var userId in childUsers)
                        if (listCurrentRmu.All(s => s.UserId != userId))
                            listRmuToAdd.Add(new IdmRightMapUser
                            {
                                Enable = true,
                                InheritedFromRoles = IdmHelper.GenRolesInherited(roleId),
                                Inherited = true,
                                RightId = rightId,
                                UserId = userId,
                                ApplicationId = applicationId
                            }.InitCreate(applicationId, actorId));
                    if (listRmuToAdd.Any()) unitOfWork.GetRepository<IdmRightMapUser>().AddRange(listRmuToAdd);
                    if (await unitOfWork.SaveAsync() > 0)
                        return new Response(Code.Success, "Gán quyền cho nhóm người dùng thành công");
                    Log.Error("The sql statement is not executed!");
                    return new ResponseError(Code.BadRequest, "Câu lệnh sql không thể thực thi");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return new ResponseError(Code.ServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }

        public async Task<Response> AddRightMapRoleAsync(Guid roleId, IList<Guid> listRightId, Guid applicationId,
            Guid? appId, Guid? actorId)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    #region Check

                    var listCurrentMap = await unitOfWork.GetRepository<IdmRightMapRole>().GetListAsync(s =>
                        s.RoleId == roleId && listRightId.Contains(s.RightId) && s.ApplicationId == applicationId);
                    if (listCurrentMap.Count == listRightId.Count)
                        return new ResponseError(Code.BadRequest, "Nhóm người dùng đã có danh sách quyền");
                    var roleModel = RoleCollection.Instance.GetModel(roleId);
                    if (roleModel == null)
                        return new ResponseError(Code.BadRequest, "Nhóm người dùng không tồn tại");
                    var listRightModel = RightCollection.Instance.Collection.Where(s => listRightId.Contains(s.Id))
                        .ToList();
                    if (!listRightModel.Any())
                        return new ResponseError(Code.BadRequest, "Không quyền nào tồn tại");

                    #endregion

                    var listItemAdd = new List<IdmRightMapRole>();
                    foreach (var rightModel in listRightModel)
                        listItemAdd.Add(new IdmRightMapRole
                        {
                            RightId = rightModel.Id,
                            RoleId = roleId,
                            ApplicationId = applicationId
                        }.InitCreate(applicationId, actorId));
                    if (listItemAdd.Any()) unitOfWork.GetRepository<IdmRightMapRole>().AddRange(listItemAdd);
                    /*Thêm mới right mapp user của user kế thừa*/
                    var childUsers = await unitOfWork.GetRepository<IdmUserMapRole>()
                        .Get(s => s.RoleId == roleId && s.ApplicationId == applicationId).Select(s => s.UserId)
                        .ToListAsync();
                    var listCurrentRmu = await unitOfWork.GetRepository<IdmRightMapUser>().GetListAsync(s =>
                        childUsers.Contains(s.UserId) && listRightId.Contains(s.RightId) &&
                        s.ApplicationId == applicationId);
                    var listRmuToAdd = new List<IdmRightMapUser>();
                    foreach (var currentRmu in listCurrentRmu)
                    {
                        var newInheritedFromRoles =
                            IdmHelper.AddRolesInherited(currentRmu.InheritedFromRoles, roleModel.Id);
                        currentRmu.InheritedFromRoles = newInheritedFromRoles;
                        currentRmu.Inherited = true;
                        currentRmu.Enable = true;
                        unitOfWork.GetRepository<IdmRightMapUser>().Update(currentRmu.InitUpdate(appId, actorId));
                    }

                    foreach (var userId in childUsers)
                        if (listCurrentRmu.All(s => s.UserId != userId))
                        {
                            var newInheritedFromRoles = IdmHelper.GenRolesInherited(roleModel.Id);

                            foreach (var rightId in listRightId)
                                listRmuToAdd.Add(new IdmRightMapUser
                                {
                                    Enable = true,
                                    InheritedFromRoles = newInheritedFromRoles,
                                    Inherited = true,
                                    RightId = rightId,
                                    UserId = userId,
                                    ApplicationId = applicationId
                                }.InitCreate(applicationId, actorId));
                        }

                    if (listRmuToAdd.Any()) unitOfWork.GetRepository<IdmRightMapUser>().AddRange(listRmuToAdd);
                    if (await unitOfWork.SaveAsync() > 0)
                        return new Response(Code.Success, "Gán danh sách quyền cho nhóm người dùng thành công");
                    Log.Error("The sql statement is not executed!");
                    return new ResponseError(Code.BadRequest, "Câu lệnh sql không thể thực thi");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return new ResponseError(Code.ServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }

        public async Task<Response> AddRightMapRoleAsync(IList<Guid> listRoleId, Guid rightId, Guid applicationId,
            Guid? appId, Guid? actorId)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    #region Check

                    var listCurrentMap = await unitOfWork.GetRepository<IdmRightMapRole>().GetListAsync(s =>
                        s.RightId == rightId && listRoleId.Contains(s.RoleId) && s.ApplicationId == applicationId);
                    if (listCurrentMap.Count == listRoleId.Count)
                        return new ResponseError(Code.BadRequest, "Các nhóm người dùng đều có quyền");

                    var rightModel = RightCollection.Instance.GetModel(rightId);
                    if (rightModel == null)
                        return new ResponseError(Code.BadRequest, "Quyền không tồn tại");
                    var listRoleModel = RoleCollection.Instance.Collection.Where(s => listRoleId.Contains(s.Id))
                        .ToList();
                    if (listRoleModel.Count == 0)
                        return new ResponseError(Code.BadRequest, "Không nhóm người dùng nào tồn tại");

                    #endregion

                    var listItemAdd = new List<IdmRightMapRole>();
                    foreach (var roleModel in listRoleModel)
                        listItemAdd.Add(new IdmRightMapRole
                        {
                            RightId = rightModel.Id,
                            RoleId = roleModel.Id,
                            ApplicationId = applicationId
                        }.InitCreate(applicationId, actorId));
                    if (listItemAdd.Any()) unitOfWork.GetRepository<IdmRightMapRole>().AddRange(listItemAdd);
                    var listRmuToAdd = new List<IdmRightMapUser>();
                    foreach (var roleModel in listRoleModel)
                    {
                        /*Thêm mới right mapp user của user kế thừa*/
                        var childUsers = await unitOfWork.GetRepository<IdmUserMapRole>()
                            .Get(s => s.RoleId == roleModel.Id && s.ApplicationId == applicationId)
                            .Select(s => s.UserId).ToListAsync();
                        var listCurrentRmu = await unitOfWork.GetRepository<IdmRightMapUser>().GetListAsync(s =>
                            childUsers.Contains(s.UserId) && s.RightId == rightId && s.ApplicationId == applicationId);
                        foreach (var currentRmu in listCurrentRmu)
                        {
                            var newInheritedFromRoles =
                                IdmHelper.AddRolesInherited(currentRmu.InheritedFromRoles, roleModel.Id);
                            currentRmu.InheritedFromRoles = newInheritedFromRoles;
                            currentRmu.Inherited = true;
                            currentRmu.Enable = true;
                            unitOfWork.GetRepository<IdmRightMapUser>().Update(currentRmu.InitUpdate(appId, actorId));
                        }

                        foreach (var userId in childUsers)
                            if (listCurrentRmu.All(s => s.UserId != userId))
                            {
                                var newInheritedFromRoles = IdmHelper.GenRolesInherited(roleModel.Id);

                                listRmuToAdd.Add(new IdmRightMapUser
                                {
                                    Enable = true,
                                    InheritedFromRoles = newInheritedFromRoles,
                                    Inherited = true,
                                    RightId = rightId,
                                    UserId = userId,
                                    ApplicationId = applicationId
                                }.InitCreate(applicationId, actorId));
                            }
                    }

                    if (listRmuToAdd.Any()) unitOfWork.GetRepository<IdmRightMapUser>().AddRange(listRmuToAdd);
                    if (await unitOfWork.SaveAsync() > 0)
                        return new Response(Code.Success, "Gán quyền cho danh sách nhóm người dùng thành công");
                    Log.Error("The sql statement is not executed!");
                    return new ResponseError(Code.BadRequest, "Câu lệnh sql không thể thực thi");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return new ResponseError(Code.ServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }

        public async Task<Response> GetRightMapRoleAsync(Guid roleId, Guid applicationId)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    var datas = await (from rmu in unitOfWork.GetRepository<IdmRightMapRole>().GetAll()
                        join r in unitOfWork.GetRepository<IdmRight>().GetAll()
                            on rmu.RightId equals r.Id
                        where rmu.RoleId == roleId && rmu.ApplicationId == applicationId
                        select r).ToListAsync();

                    var resultData = AutoMapperUtils.AutoMap<IdmRight, BaseRightModel>(datas);
                    return new ResponseList<BaseRightModel>(resultData);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return new ResponseError(Code.ServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }

        public async Task<Response> GetRoleMapRightAsync(Guid rightId, Guid applicationId)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    var datas = await (from rmu in unitOfWork.GetRepository<IdmRightMapRole>().GetAll()
                        join r in unitOfWork.GetRepository<IdmRole>().GetAll()
                            on rmu.RoleId equals r.Id
                        where rmu.RightId == rightId && rmu.ApplicationId == applicationId
                        select r).ToListAsync();
                    var resultData = AutoMapperUtils.AutoMap<IdmRole, BaseRoleModel>(datas);
                    return new ResponseList<BaseRoleModel>(resultData);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return new ResponseError(Code.ServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }

        public async Task<Response> CheckRightMapRoleAsync(Guid roleId, Guid rightId, Guid applicationId)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    var currentMap = await unitOfWork.GetRepository<IdmRightMapRole>().FindAsync(s =>
                        s.RoleId == roleId && s.RightId == rightId && s.ApplicationId == applicationId);
                    if (currentMap == null)
                        return new ResponseObject<bool>(false);
                    return new ResponseObject<bool>(true);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return new ResponseError(Code.ServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }
    }
}