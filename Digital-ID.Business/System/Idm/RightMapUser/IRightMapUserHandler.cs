﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DigitalID.Data;

namespace DigitalID.Business
{
    public interface IRightMapUserHandler
    {
        /// <summary>
        ///     Xóa 1 quyền của người dùng
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="rightId"></param>
        /// <param name="applicationId"></param>
        /// <returns></returns>
        Task<Response> DeleteRightMapUserAsync(Guid userId, Guid rightId, Guid applicationId);

        /// <summary>
        ///     Xóa 1 danh sách quyền của người dùng
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="listRightId"></param>
        /// <param name="applicationId"></param>
        /// <returns></returns>
        Task<Response> DeleteRightMapUserAsync(Guid userId, IList<Guid> listRightId, Guid applicationId);

        /// <summary>
        ///     Xóa quyền của 1 danh sách các người dùng
        /// </summary>
        /// <param name="listUserId"></param>
        /// <param name="rightId"></param>
        /// <param name="applicationId"></param>
        /// <returns></returns>
        Task<Response> DeleteRightMapUserAsync(IList<Guid> listUserId, Guid rightId, Guid applicationId);

        /// <summary>
        ///     Thêm 1 quyền cho người dùng
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="rightId"></param>
        /// <param name="applicationId"></param>
        /// <param name="appId"></param>
        /// <param name="actorId"></param>
        /// <returns></returns>
        Task<Response> AddRightMapUserAsync(Guid userId, Guid rightId, Guid applicationId, Guid? appId, Guid? actorId);

        /// <summary>
        ///     Thêm 1 danh sách quyền cho người dùng
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="listRightId"></param>
        /// <param name="applicationId"></param>
        /// <param name="appId"></param>
        /// <param name="actorId"></param>
        /// <returns></returns>
        Task<Response> AddRightMapUserAsync(Guid userId, IList<Guid> listRightId, Guid applicationId, Guid? appId,
            Guid? actorId);

        /// <summary>
        ///     Thêm 1 quyền cho danh sách người dùng
        /// </summary>
        /// <param name="listUserId"></param>
        /// <param name="rightId"></param>
        /// <param name="applicationId"></param>
        /// <param name="appId"></param>
        /// <param name="actorId"></param>
        /// <returns></returns>
        Task<Response> AddRightMapUserAsync(IList<Guid> listUserId, Guid rightId, Guid applicationId, Guid? appId,
            Guid? actorId);

        /// <summary>
        ///     Bật/Tắt quyền của người dùng
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="rightId"></param>
        /// <param name="applicationId"></param>
        /// <param name="enable"></param>
        /// <param name="appId"></param>
        /// <param name="actorId"></param>
        /// <returns></returns>
        Task<Response> ToggleRightMapUserAsync(Guid userId, Guid rightId, Guid applicationId, bool enable, Guid? appId,
            Guid? actorId);

        /// <summary>
        ///     Bật/Tắt 1 danh sách quyền - người dùng
        /// </summary>
        /// <param name="listUserId"></param>
        /// <param name="listRightId"></param>
        /// <param name="applicationId"></param>
        /// <param name="enable"></param>
        /// <param name="appId"></param>
        /// <param name="actorId"></param>
        /// <returns></returns>
        Task<Response> ToggleRightMapUserAsync(IList<Guid> listUserId, IList<Guid> listRightId, Guid applicationId,
            bool enable, Guid? appId, Guid? actorId);

        /// <summary>
        ///     Lấy danh sách quyền của người dùng
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="applicationId"></param>
        /// <returns></returns>
        Task<Response> GetRightMapUserAsync(Guid userId, Guid applicationId);

        /// <summary>
        ///     Lấy danh sách người dùng có quyền
        /// </summary>
        /// <param name="rightId"></param>
        /// <param name="applicationId"></param>
        /// <returns></returns>
        Task<Response> GetUserMapRightAsync(Guid rightId, Guid applicationId);

        /// <summary>
        ///     Kiểm tra quyền của người dùng
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="rightId"></param>
        /// <param name="applicationId"></param>
        /// <returns></returns>
        Task<Response> CheckRightMapUserAsync(Guid userId, Guid rightId, Guid applicationId);
    }
}