﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DigitalID.Data;
using Microsoft.EntityFrameworkCore;
using Serilog;

namespace DigitalID.Business
{
    public class RightMapUserHandler : IRightMapUserHandler
    {
        public async Task<Response> DeleteRightMapUserAsync(Guid userId, Guid rightId, Guid applicationId)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    #region Check

                    var currentMap = await unitOfWork.GetRepository<IdmRightMapUser>().FindAsync(s =>
                        s.UserId == userId && s.RightId == rightId && s.ApplicationId == applicationId);
                    if (currentMap == null)
                        return new ResponseError(Code.BadRequest, "Người dùng không có quyền !");

                    #endregion

                    var self = IdmHelper.MakeIndependentPermission();
                    var selfString = IdmHelper.GenRolesInherited(self);
                    if (currentMap.InheritedFromRoles == selfString ||
                        string.IsNullOrEmpty(currentMap.InheritedFromRoles))
                    {
                        unitOfWork.GetRepository<IdmRightMapUser>().Delete(currentMap);
                    }
                    else
                    {
                        //var listRolesDepens = String.Join(", ", IdmHelper.LoadRolesInherited(currentMap.InheritedFromRoles).Select(s => s));
                        //return new Response(ResponseCode.VALIDATE_ERROR, "Cant delete mapping, depend on role:" + listRolesDepens, false);
                        currentMap.Enable = false;
                        unitOfWork.GetRepository<IdmRightMapUser>().Update(currentMap);
                    }

                    if (await unitOfWork.SaveAsync() > 0)
                        return new Response(Code.Success, "Loại quyền của người dùng thành công");
                    Log.Error("The sql statement is not executed!");
                    return new ResponseError(Code.BadRequest, "Câu lệnh sql không thể thực thi");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return new ResponseError(Code.ServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }

        public async Task<Response> DeleteRightMapUserAsync(Guid userId, IList<Guid> listRightId, Guid applicationId)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    #region Check
                    var listCurrenMap = await unitOfWork.GetRepository<IdmRightMapUser>().GetListAsync(s =>
                        s.UserId == userId && listRightId.Contains(s.RightId) && s.ApplicationId == applicationId);
                    if (listCurrenMap.Count == 0)
                        return new ResponseError(Code.BadRequest, "Người dùng không có quyền nào!");
                    #endregion

                    var self = IdmHelper.MakeIndependentPermission();
                    var selfString = IdmHelper.GenRolesInherited(self);
                    var listRmuToDelete = new List<IdmRightMapUser>();
                    foreach (var currentMap in listCurrenMap)
                        if (currentMap.InheritedFromRoles == selfString ||
                            string.IsNullOrEmpty(currentMap.InheritedFromRoles))
                        {
                            listRmuToDelete.Add(currentMap);
                        }
                        else
                        {
                            //var listRolesDepens = String.Join(", ", IdmHelper.LoadRolesInherited(currentMap.InheritedFromRoles).Select(s => s));
                            //return new Response(ResponseCode.VALIDATE_ERROR, "Cant delete mapping UserId: " + userId.ToString() + "- RightId: " + currentMap.RightId + ", depend on role:" + listRolesDepens, false);
                            currentMap.Enable = false;
                            unitOfWork.GetRepository<IdmRightMapUser>().Update(currentMap);
                        }

                    unitOfWork.GetRepository<IdmRightMapUser>().DeleteRange(listRmuToDelete);
                    if (await unitOfWork.SaveAsync() > 0)
                        return new Response(Code.Success, "Loại danh sách quyền của người dùng thành công");
                    Log.Error("The sql statement is not executed!");
                    return new ResponseError(Code.BadRequest, "Câu lệnh sql không thể thực thi");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return new ResponseError(Code.ServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }

        public async Task<Response> DeleteRightMapUserAsync(IList<Guid> listUserId, Guid rightId, Guid applicationId)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    #region Check

                    var listCurrenMap = await unitOfWork.GetRepository<IdmRightMapUser>().GetListAsync(s =>
                        s.RightId == rightId && listUserId.Contains(s.UserId) && s.ApplicationId == applicationId);
                    if (listCurrenMap.Count == 0)
                        return new ResponseError(Code.BadRequest, "Không người dùng nào có quyền!");

                    #endregion

                    var self = IdmHelper.MakeIndependentPermission();
                    var selfString = IdmHelper.GenRolesInherited(self);
                    var listRmuToDelete = new List<IdmRightMapUser>();
                    foreach (var currentMap in listCurrenMap)
                        if (currentMap.InheritedFromRoles == selfString ||
                            string.IsNullOrEmpty(currentMap.InheritedFromRoles))
                        {
                            listRmuToDelete.Add(currentMap);
                        }
                        else
                        {
                            currentMap.Enable = false;
                            unitOfWork.GetRepository<IdmRightMapUser>().Update(currentMap);
                            //var listRolesDepens = String.Join(", ", IdmHelper.LoadRolesInherited(currentMap.InheritedFromRoles).Select(s => s));
                            //return new Response(ResponseCode.VALIDATE_ERROR, "Cant delete mapping UserId: " + currentMap.UserId.ToString() + "- RightId: " + currentMap.RightId + ", depend on role:" + listRolesDepens, false);
                        }

                    unitOfWork.GetRepository<IdmRightMapUser>().DeleteRange(listRmuToDelete);
                    if (await unitOfWork.SaveAsync() > 0)
                        return new Response(Code.Success, "Loại danh sách quyền của người dùng thành công");
                    Log.Error("The sql statement is not executed!");
                    return new ResponseError(Code.BadRequest, "Câu lệnh sql không thể thực thi");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return new ResponseError(Code.ServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }

        public async Task<Response> AddRightMapUserAsync(Guid userId, Guid rightId, Guid applicationId, Guid? appId,
            Guid? actorId)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    #region Check

                    var userModel = UserCollection.Instance.GetModel(userId);
                    if (userModel == null) return new ResponseError(Code.BadRequest, "Người dùng không tồn tại");
                    var rightModel = RightCollection.Instance.GetModel(rightId);
                    if (rightModel == null) return new ResponseError(Code.BadRequest, "Quyền không tồn tại");

                    #endregion

                    var currentMap = await unitOfWork.GetRepository<IdmRightMapUser>().FindAsync(s =>
                        s.UserId == userId && s.RightId == rightId && s.ApplicationId == applicationId);
                    //Tạo role kế thừa . Quyền riêng
                    var role = IdmHelper.MakeIndependentPermission();
                    if (currentMap != null)
                    {
                        currentMap.InheritedFromRoles =
                            IdmHelper.AddRolesInherited(currentMap.InheritedFromRoles, role);
                        currentMap.Enable = true; //Auto enable
                        unitOfWork.GetRepository<IdmRightMapUser>().Update(currentMap);
                    }
                    else
                    {
                        var model = new IdmRightMapUser
                        {
                            UserId = userId,
                            RightId = rightId,
                            InheritedFromRoles = IdmHelper.GenRolesInherited(role),
                            Enable = true,
                            Inherited = false,
                            ApplicationId = applicationId
                        }.InitCreate(applicationId, actorId);
                        unitOfWork.GetRepository<IdmRightMapUser>().Add(model);
                    }

                    if (await unitOfWork.SaveAsync() > 0)
                        return new Response(Code.Success, "Gán quyền cho người dùng thành công");
                    Log.Error("The sql statement is not executed!");
                    return new ResponseError(Code.BadRequest, "Câu lệnh sql không thể thực thi");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return new ResponseError(Code.ServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }

        public async Task<Response> AddRightMapUserAsync(Guid userId, IList<Guid> listRightId, Guid applicationId,
            Guid? appId, Guid? actorId)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    #region Check

                    var userModel = UserCollection.Instance.GetModel(userId);
                    if (userModel == null)
                        return new ResponseError(Code.BadRequest, "Người dùng không tồn tại");

                    var listRightModel = RightCollection.Instance.Collection.Where(s => listRightId.Contains(s.Id))
                        .ToList();
                    if (listRightModel.Count == 0)
                        return new ResponseError(Code.BadRequest, "Không quyền nào không tồn tại");

                    #endregion

                    var listCurrentMap = await unitOfWork.GetRepository<IdmRightMapUser>().GetListAsync(s =>
                        s.UserId == userId && listRightId.Contains(s.RightId) && s.ApplicationId == applicationId);
                    //Tạo role kế thừa . Quyền riêng
                    var role = IdmHelper.MakeIndependentPermission();
                    var listRmuToAdd = new List<IdmRightMapUser>();
                    foreach (var currentMap in listCurrentMap)
                    {
                        currentMap.InheritedFromRoles =
                            IdmHelper.AddRolesInherited(currentMap.InheritedFromRoles, role);
                        currentMap.Enable = true; //Auto enable
                        unitOfWork.GetRepository<IdmRightMapUser>().Update(currentMap);
                    }

                    foreach (var rightId in listRightId)
                        if (listCurrentMap.All(s => s.RightId != rightId))
                            listRmuToAdd.Add(new IdmRightMapUser
                            {
                                UserId = userId,
                                RightId = rightId,
                                InheritedFromRoles = IdmHelper.GenRolesInherited(role),
                                Enable = true,
                                Inherited = false,
                                ApplicationId = applicationId
                            }.InitCreate(applicationId, actorId));
                    if (listRmuToAdd.Any()) unitOfWork.GetRepository<IdmRightMapUser>().AddRange(listRmuToAdd);

                    if (await unitOfWork.SaveAsync() > 0)
                        return new Response(Code.Success, "Gám danh sách quyền cho người dùng thành công");
                    Log.Error("The sql statement is not executed!");
                    return new ResponseError(Code.BadRequest, "Câu lệnh sql không thể thực thi");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return new ResponseError(Code.ServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }

        public Response AddRightMapUserAsyncV2(Guid userId, IList<Guid> listRightId, Guid applicationId,
            Guid? appId, Guid? actorId)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    #region Check

                    var userModel = UserCollection.Instance.GetModel(userId);
                    if (userModel == null)
                        return new ResponseError(Code.BadRequest, "Người dùng không tồn tại");

                    var listRightModel = RightCollection.Instance.Collection.Where(s => listRightId.Contains(s.Id))
                        .ToList();
                    if (listRightModel.Count == 0)
                        return new ResponseError(Code.BadRequest, "Không quyền nào không tồn tại");

                    #endregion

                    var listCurrentMap = unitOfWork.GetRepository<IdmRightMapUser>().GetMany(s =>
                        s.UserId == userId && listRightId.Contains(s.RightId) && s.ApplicationId == applicationId);
                    //Tạo role kế thừa . Quyền riêng
                    var role = IdmHelper.MakeIndependentPermission();
                    var listRmuToAdd = new List<IdmRightMapUser>();
                    foreach (var currentMap in listCurrentMap)
                    {
                        currentMap.InheritedFromRoles =
                            IdmHelper.AddRolesInherited(currentMap.InheritedFromRoles, role);
                        currentMap.Enable = true; //Auto enable
                        unitOfWork.GetRepository<IdmRightMapUser>().Update(currentMap);
                    }

                    foreach (var rightId in listRightId)
                        if (listCurrentMap.All(s => s.RightId != rightId))
                            listRmuToAdd.Add(new IdmRightMapUser
                            {
                                UserId = userId,
                                RightId = rightId,
                                InheritedFromRoles = IdmHelper.GenRolesInherited(role),
                                Enable = true,
                                Inherited = false,
                                ApplicationId = applicationId
                            }.InitCreate(applicationId, actorId));
                    if (listRmuToAdd.Any()) unitOfWork.GetRepository<IdmRightMapUser>().AddRange(listRmuToAdd);

                    if (unitOfWork.Save() > 0)
                        return new Response(Code.Success, "Gám danh sách quyền cho người dùng thành công");
                    Log.Error("The sql statement is not executed!");
                    return new ResponseError(Code.BadRequest, "Câu lệnh sql không thể thực thi");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return new ResponseError(Code.ServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }

        public async Task<Response> AddRightMapUserAsync(IList<Guid> listUserId, Guid rightId, Guid applicationId,
            Guid? appId, Guid? actorId)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    #region Check

                    var listUserModel = UserCollection.Instance.GetModel(s => listUserId.Contains(s.Id));
                    if (listUserModel.Count == 0)
                        return new ResponseError(Code.BadRequest, "Không người dùng nào không tồn tại");
                    var rightModel = RightCollection.Instance.GetModel(rightId);
                    if (rightModel == null)
                        return new ResponseError(Code.BadRequest, "Quyền không tồn tại");

                    #endregion

                    var listCurrentMap = await unitOfWork.GetRepository<IdmRightMapUser>().GetListAsync(s =>
                        s.RightId == rightId && listUserId.Contains(s.UserId) && s.ApplicationId == applicationId);
                    //Tạo role kế thừa . Quyền riêng
                    var role = IdmHelper.MakeIndependentPermission();
                    var listRmuToAdd = new List<IdmRightMapUser>();
                    foreach (var currentMap in listCurrentMap)
                    {
                        currentMap.InheritedFromRoles =
                            IdmHelper.AddRolesInherited(currentMap.InheritedFromRoles, role);
                        currentMap.Enable = true; //Auto enable
                        unitOfWork.GetRepository<IdmRightMapUser>().Update(currentMap);
                    }

                    foreach (var userId in listUserId)
                        if (listCurrentMap.All(s => s.UserId != userId))
                            listRmuToAdd.Add(new IdmRightMapUser
                            {
                                UserId = userId,
                                RightId = rightId,
                                InheritedFromRoles = IdmHelper.GenRolesInherited(role),
                                Enable = true,
                                Inherited = false,
                                ApplicationId = applicationId
                            }.InitCreate(applicationId, actorId));
                    if (listRmuToAdd.Any()) unitOfWork.GetRepository<IdmRightMapUser>().AddRange(listRmuToAdd);

                    if (await unitOfWork.SaveAsync() > 0)
                        return new Response(Code.Success, "Gán quyền cho danh sách người dùng thành công");
                    Log.Error("The sql statement is not executed!");
                    return new ResponseError(Code.BadRequest, "Câu lệnh sql không thể thực thi");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return new ResponseError(Code.ServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }

        public async Task<Response> ToggleRightMapUserAsync(Guid userId, Guid rightId, Guid applicationId, bool enable,
            Guid? appId, Guid? actorId)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    #region Check

                    var currentMap = await unitOfWork.GetRepository<IdmRightMapUser>().FindAsync(s =>
                        s.UserId == userId && s.RightId == rightId && s.ApplicationId == applicationId);
                    if (currentMap == null)
                        return new ResponseError(Code.BadRequest, "Người dùng chưa có quyền");

                    #endregion

                    currentMap.Enable = enable;
                    unitOfWork.GetRepository<IdmRightMapUser>().Update(currentMap.InitUpdate(applicationId, actorId));
                    if (await unitOfWork.SaveAsync() > 0)
                        return new Response(Code.Success, enable ? "Bật" : "Tắt" + " quyền của người dùng thành công");
                    Log.Error("The sql statement is not executed!");
                    return new ResponseError(Code.BadRequest, "Câu lệnh sql không thể thực thi");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return new ResponseError(Code.ServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }

        public async Task<Response> ToggleRightMapUserAsync(IList<Guid> listUserId, IList<Guid> listRightId,
            Guid applicationId, bool enable, Guid? appId, Guid? actorId)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    #region Check

                    var listCurrenMap = await unitOfWork.GetRepository<IdmRightMapUser>().GetListAsync(s =>
                        listUserId.Contains(s.UserId) && listRightId.Contains(s.RightId) &&
                        s.ApplicationId == applicationId);
                    if (listCurrenMap.Count == 0)
                        return new ResponseError(Code.BadRequest, "Không người dùng nào có quyền");

                    #endregion

                    foreach (var currentMap in listCurrenMap)
                    {
                        currentMap.Enable = enable;
                        unitOfWork.GetRepository<IdmRightMapUser>().Update(currentMap.InitUpdate(applicationId, actorId));
                    }

                    if (await unitOfWork.SaveAsync() > 0)
                        return new Response(Code.Success, enable ? "Bật" : "Tắt" + " quyền của người dùng thành công");
                    Log.Error("The sql statement is not executed!");
                    return new ResponseError(Code.BadRequest, "Câu lệnh sql không thể thực thi");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return new ResponseError(Code.ServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }

        public async Task<Response> GetRightMapUserAsync(Guid userId, Guid applicationId)

        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    var datas = from rmu in unitOfWork.GetRepository<IdmRightMapUser>().GetAll()
                        join r in unitOfWork.GetRepository<IdmRight>().GetAll()
                            on rmu.RightId equals r.Id
                        where rmu.UserId == userId && rmu.Enable && rmu.ApplicationId == applicationId
                        select new BaseRightModelOfUser
                        {
                            Enable = rmu.Enable,
                            InheritedFromRoles = rmu.InheritedFromRoles,
                            Inherited = rmu.Inherited,
                            Id = r.Id,
                            Code = r.Code,
                            Name = r.Name,
                            LastModifiedOnDate = rmu.LastModifiedOnDate,
                            LastModifiedByUserId = rmu.LastModifiedByUserId,
                            CreatedOnDate = rmu.CreatedOnDate,
                            CreatedByUserId = rmu.CreatedByUserId,
                            ApplicationId = rmu.ApplicationId
                        };


                    var queryList = await datas.ToListAsync();
                    var listRoleId = new List<Guid>();
                    foreach (var data in queryList)
                    {
                        data.ListRoleId = IdmHelper.LoadRolesInherited(data.InheritedFromRoles);
                        listRoleId.AddRange(data.ListRoleId);
                    }

                    var listRoleModel = await unitOfWork.GetRepository<IdmRole>()
                        .GetListAsync(s => listRoleId.Contains(s.Id));
                    foreach (var data in queryList)
                        if (data.ListRoleId != null)
                        {
                            var roleData = listRoleModel.Where(s => data.ListRoleId.Contains(s.Id)).ToList();
                            if (roleData.Any())
                                data.ListRole = AutoMapperUtils.AutoMap<IdmRole, BaseRoleModel>(roleData);
                        }


                    var resultData = queryList;
                    return new ResponseList<BaseRightModelOfUser>(resultData);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return new ResponseError(Code.ServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }

        public async Task<Response> GetUserMapRightAsync(Guid rightId, Guid applicationId)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    var listUserId = await (from rmu in unitOfWork.GetRepository<IdmRightMapUser>().GetAll()
                        where rmu.RightId == rightId && rmu.ApplicationId == applicationId
                        select rmu.UserId).ToListAsync();
                    var resultData = UserCollection.Instance.GetModel(s => listUserId.Contains(s.Id));
                    return new ResponseList<BaseUserModel>(resultData);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return new ResponseError(Code.ServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }

        public async Task<Response> CheckRightMapUserAsync(Guid userId, Guid rightId, Guid applicationId)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    var currentMap = await unitOfWork.GetRepository<IdmRightMapUser>().FindAsync(s =>
                        s.UserId == userId && s.RightId == rightId && s.ApplicationId == applicationId);
                    if (currentMap != null && currentMap.Enable)
                        return new ResponseObject<bool>(true);
                    return new ResponseObject<bool>(false);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return new ResponseError(Code.ServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }
    }
}