﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using LinqKit;
using DigitalID.Data;
using Serilog;

namespace DigitalID.Business
{
    public class RoleHandler : IRoleHandler
    {
        private readonly DbHandler<IdmRole, RoleModel, RoleQueryModel> _dbHandler =
            DbHandler<IdmRole, RoleModel, RoleQueryModel>.Instance;

        private readonly IRightMapRoleHandler _rightMapRoleHandler;
        private readonly IUserMapRoleHandler _userMapRoleHandler;

        public RoleHandler(IRightMapRoleHandler rightMapRoleHandler, IUserMapRoleHandler userMapRoleHandler)
        {
            _rightMapRoleHandler = rightMapRoleHandler;
            _userMapRoleHandler = userMapRoleHandler;
        }

        public RoleHandler()
        {
        }

        public async Task<Response> GetDetail(Guid id, Guid applicationId)
        {
            var model = await _dbHandler.FindAsync(id);

            if (model.Code == Code.Success)
            {
                var modelData = model as ResponseObject<RoleModel>;
                var result = AutoMapperUtils.AutoMap<RoleModel, RoleDetailModel>(modelData?.Data);
                var listRightResponse = await _rightMapRoleHandler.GetRightMapRoleAsync(id, applicationId);
                var listUserResponse = await _userMapRoleHandler.GetUserMapRoleAsync(id, applicationId);
                if (listRightResponse.Code == Code.Success && listUserResponse.Code == Code.Success)
                {
                    var listRightResponseData = model as ResponseList<BaseRightModel>;
                    var listUserResponseData = model as ResponseList<BaseUserModel>;
                    if (listRightResponseData != null) result.ListRight = listRightResponseData.Data;
                    if (listUserResponseData != null) result.ListUser = listUserResponseData.Data;
                    return new ResponseObject<RoleDetailModel>(result);
                }

                return new ResponseError(Code.BadRequest, "Không thể lấy dữ liệu  chi tiết");
            }

            return model;
        }

        public Task<Response> GetPageAsync(RoleQueryModel query)
        {
            var predicate = BuildQuery(query);
            return _dbHandler.GetPageAsync(predicate, query);
        }

        public Task<Response> GetAllAsync(RoleQueryModel query)
        {
            var predicate = BuildQuery(query);
            return _dbHandler.GetAllAsync(predicate, query.Sort);
        }

        public Task<Response> GetAllAsync()
        {
            var predicate = PredicateBuilder.New<IdmRole>(true);
            return _dbHandler.GetAllAsync(predicate);
        }

        public Response GetAll()
        {
            return _dbHandler.GetAll();
        }

        private static Expression<Func<IdmRole, bool>> BuildQuery(RoleQueryModel query)
        {
            var predicate = PredicateBuilder.New<IdmRole>(true);
            if (!string.IsNullOrEmpty(query.Code)) predicate.And(s => s.Code == query.Code);
            if (!string.IsNullOrEmpty(query.Name)) predicate.And(s => s.Name == query.Name);
            if (!string.IsNullOrWhiteSpace(query.ActorId) && query.ActorId != UserConstants.AdministratorId.ToString())
            {
                predicate.And(s => s.Id != RoleConstants.AdministratorId && s.Id != RoleConstants.UserId && s.Id != RoleConstants.CustomerId && s.Id != RoleConstants.GuestId);
            }
            if (query.Id.HasValue) predicate.And(s => s.Id == query.Id);
            if (query.ListId != null) predicate.And(s => query.ListId.Contains(s.Id));

            if (!string.IsNullOrEmpty(query.FullTextSearch))
                predicate.And(s =>
                    s.Code.ToLower().Contains(query.FullTextSearch.ToLower()) || s.Name.ToLower().Contains(query.FullTextSearch.ToLower()) ||
                    s.Description.ToLower().Contains(query.FullTextSearch.ToLower()));
            return predicate;
        }

        #region CRUD

        public async Task<Response> CreateAsync(RoleCreateModel model, Guid? appId, Guid? actorId)
        {
            var request = AutoMapperUtils.AutoMap<RoleCreateModel, IdmRole>(model);
            request.Id = Guid.NewGuid();
            var result = await _dbHandler.CreateAsync(request, "Code");
            if (result.Code == Code.Success)
            {
                RoleCollection.Instance.LoadToHashSet();

                #region Realtive

                if (model.ListAddRightId != null)
                    await _rightMapRoleHandler.AddRightMapRoleAsync(request.Id, model.ListAddRightId,
                        model.ApplicatonId.Value, appId, actorId);
                if (model.ListAddUserId != null)
                    await _userMapRoleHandler.AddUserMapRoleAsync(request.Id, model.ListAddUserId,
                        model.ApplicatonId.Value,
                        appId, actorId);

                #endregion
            }

            return result;
        }

        public async Task<Response> UpdateAsync(Guid id, RoleUpdateModel model, Guid? appId, Guid? actorId)
        {
            var request = AutoMapperUtils.AutoMap<RoleUpdateModel, IdmRole>(model);
            request.Id = id;
            var result = await _dbHandler.UpdateAsync(id, request, "Code");
            if (result.Code == Code.Success)
            {
                RoleCollection.Instance.LoadToHashSet();

                #region Realtive

                if (model.ListAddRightId != null)
                    await _rightMapRoleHandler.AddRightMapRoleAsync(id, model.ListAddRightId, model.ApplicatonId.Value,
                        appId, actorId);
                if (model.ListAddUserId != null)
                    await _userMapRoleHandler.AddUserMapRoleAsync(id, model.ListAddUserId, model.ApplicatonId.Value,
                        appId, actorId);
                if (model.ListDeleteRightId != null)
                    await _rightMapRoleHandler.DeleteRightMapRoleAsync(id, model.ListDeleteRightId,
                        model.ApplicatonId.Value);
                if (model.ListDeleteUserId != null)
                    await _userMapRoleHandler.DeleteUserMapRoleAsync(id, model.ListDeleteUserId,
                        model.ApplicatonId.Value);

                #endregion
            }

            return result;
        }

        public async Task<Response> DeleteAsync(Guid id)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    var currentRole = await unitOfWork.GetRepository<IdmRole>().FindAsync(id);
                    if (currentRole == null)
                        return new ResponseError(Code.BadRequest, "Nhóm người dùng không tồn tại", null);
                    //Lấy về danh sách các kế thừa
                    var listItemDelete = new List<IdmRightMapUser>();
                    var idString = id.ToString();
                    var currentRmu = await unitOfWork.GetRepository<IdmRightMapUser>()
                        .GetListAsync(s => s.InheritedFromRoles.Contains(idString));
                    foreach (var mapItem in currentRmu)
                    {
                        //Nếu InheritedFromRoles chỉ chứa mình quyền đó =>> xóa
                        //Nếu không khai trừ role ra khỏi InheritedFromRoles 
                        var listRole = IdmHelper.LoadRolesInherited(mapItem.InheritedFromRoles);
                        if (listRole.Count > 1)
                        {
                            if (listRole.Contains(IdmHelper.MakeIndependentPermission())
                            ) //nếu có 2 inherist với 1 cái là self 
                                mapItem.Inherited = false;
                            mapItem.InheritedFromRoles = IdmHelper.RemoveRolesInherited(mapItem.InheritedFromRoles, id);
                            unitOfWork.GetRepository<IdmRightMapUser>().Update(mapItem);
                        }
                        else
                        {
                            listItemDelete.Add(mapItem);
                        }
                    }

                    if (listItemDelete.Count == 0)
                        unitOfWork.GetRepository<IdmRightMapUser>().DeleteRange(listItemDelete);
                    /// Xóa user map role
                    unitOfWork.GetRepository<IdmUserMapRole>().DeleteRange(x => x.RoleId == id);
                    /// Xóa user map role
                    unitOfWork.GetRepository<IdmRightMapRole>().DeleteRange(x => x.RoleId == id);
                    unitOfWork.GetRepository<IdmRole>().Delete(currentRole);
                    if (await unitOfWork.SaveAsync() > 0)
                    {
                        RoleCollection.Instance.LoadToHashSet();
                        return new ResponseDelete(id, currentRole.Name);
                    }

                    Log.Error("The sql statement is not executed!");
                    return new ResponseError(Code.BadRequest, "Câu lệnh sql không thể thực thi");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return new ResponseError(Code.ServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }

        public async Task<Response> DeleteRangeAsync(IList<Guid> listId)
        {
            var resultData = new List<ResponseDelete>();
            foreach (var id in listId)
            {
                var model = await DeleteAsync(id) as ResponseDelete;
                resultData.Add(model);
            }

            var result = new ResponseDeleteMulti(resultData);
            return result;
        }

        public Task<Response> FindAsync(Guid id)
        {
            return _dbHandler.FindAsync(id);
        }

        #endregion
    }
}