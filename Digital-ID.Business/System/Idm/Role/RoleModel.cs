﻿using DigitalID.Data;
using System;
using System.Collections.Generic;

namespace DigitalID.Business
{
    public class BaseRoleModel 
    {
        public Guid Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
    }
    public class RoleModel : BaseRoleModel
    {
        public string Description { get; set; }
    }

    public class RoleDetailModel : RoleModel
    {
        public IList<BaseRightModel> ListRight { get; set; }
        public IList<BaseUserModel> ListUser { get; set; }
    }
    public class RoleQueryModel : PaginationRequest
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public string ActorId { get; set; }
    }

    public class RoleCreateModel
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Guid? ApplicatonId { get; set; }
        //Plus
        /// <summary>
        /// Danh sách quyền thêm
        /// </summary>
        public IList<Guid> ListAddRightId { get; set; }
        /// <summary>
        /// Danh sách người dùng thêm
        /// </summary>
        public IList<Guid> ListAddUserId { get; set; }
    }
    public class RoleUpdateModel
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Guid? ApplicatonId { get; set; }
        //Plus
        /// <summary>
        /// Danh sách quyền thêm
        /// </summary>
        public IList<Guid> ListAddRightId { get; set; }
        /// <summary>
        /// Danh sách người dùng thêm
        /// </summary>
        public IList<Guid> ListAddUserId { get; set; }
        //Plus
        /// <summary>
        /// Danh sách quyền xóa
        /// </summary>
        public IList<Guid> ListDeleteRightId { get; set; }
        /// <summary>
        /// Danh sách người dùng xóa
        /// </summary>
        public IList<Guid> ListDeleteUserId { get; set; }
    }
}

