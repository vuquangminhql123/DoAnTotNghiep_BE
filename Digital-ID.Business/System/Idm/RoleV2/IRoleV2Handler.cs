﻿using DigitalID.Data;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DigitalID.Business
{
    /// <summary>
    /// Interface quản lý nhóm người dùng
    /// </summary>
    public interface IRoleV2Handler
    {
        /// <summary>
        /// Thêm mới nhóm người dùng
        /// </summary>
        /// <param name="model">Model thêm mới nhóm người dùng</param>
        /// <returns>Id nhóm người dùng</returns>
        Task<Response> Create(RoleV2CreateModel model);

        /// <summary>
        /// Cập nhật nhóm người dùng
        /// </summary>
        /// <param name="model">Model cập nhật nhóm người dùng</param>
        /// <returns>Id nhóm người dùng</returns>
        Task<Response> Update(RoleV2UpdateModel model);

        /// <summary>
        /// Xóa đơn vị tính
        /// </summary>
        /// <param name="listId">Danh sách Id nhóm người dùng</param>
        /// <returns>Danh sách kết quả xóa</returns>
        Task<Response> Delete(List<Guid> listId);

        /// <summary>
        /// Lấy danh sách nhóm người dùng theo điều kiện lọc
        /// </summary>
        /// <param name="filter">Model điều kiện lọc</param>
        /// <returns>Danh sách nhóm người dùng</returns>
        Task<Response> Filter(RoleV2QueryFilter filter);

        /// <summary>
        /// Lấy đơn vị tính theo Id
        /// </summary>
        /// <param name="id">Id nhóm người dùng</param>
        /// <returns>Thông tin nhóm người dùng</returns>
        Task<Response> GetById(Guid id);

        /// <summary>
        /// Lấy danh sách nhóm người dùng cho combobox
        /// </summary>
        /// <param name="count">Số bản ghi tối đa</param>
        /// <param name="textSearch">Từ khóa tìm kiếm</param>
        /// <returns>Danh sách nhóm người dùng cho combobox</returns>
        Task<Response> GetListCombobox(int count = 0, string textSearch = "");
    }
}
