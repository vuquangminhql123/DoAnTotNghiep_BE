﻿using DigitalID.Data;
using Microsoft.EntityFrameworkCore;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using iTextSharp.text.xml.simpleparser;
using Nest;
using iTextSharp.text;

namespace DigitalID.Business
{
    public class RoleV2Handler : IRoleV2Handler
    {
        #region Message

        #endregion

        private const string CachePrefix = "IdmRoleV2";
        private const string SelectItemCacheSubfix = "list-select";
        private const string CodePrefix = "NND.";
        private readonly DataContext _dataContext;
        private readonly ICacheService _cacheService;

        public RoleV2Handler(DataContext dataContext, ICacheService cacheService)
        {
            _dataContext = dataContext;
            _cacheService = cacheService;
        }

        public async Task<Response> Create(RoleV2CreateModel model)
        {
            try
            {
                var entity = AutoMapperUtils.AutoMap<RoleV2CreateModel, IdmRole>(model);
                entity.CreatedOnDate = DateTime.Now;
                entity.Id = Guid.NewGuid();
                await _dataContext.IdmRole.AddAsync(entity);
                foreach (var item in model.ListOrganizationId)
                {
                    var organizationItem = new IdmRoleMapOrganization()
                    {
                        RoleId = entity.Id,
                        OrganizationId = item,
                        StatusDefault = true,
                        Order = 1,
                        IdentityNumber = 1
                    };
                    await _dataContext.IdmRoleMapOrganization.AddAsync(organizationItem);
                }

                int dbSave = await _dataContext.SaveChangesAsync();

                if (dbSave > 0)
                {
                    Log.Information("Add success: " + JsonSerializer.Serialize(entity));
                    InvalidCache();

                    return new ResponseObject<Guid>(entity.Id, MessageConstants.CreateSuccessMessage, Code.Success);
                }
                else
                {
                    return new ResponseError(Code.ServerError, MessageConstants.CreateErrorMessage);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, MessageConstants.ErrorLogMessage);
                return new ResponseError(Code.ServerError, $"{MessageConstants.CreateErrorMessage} - {ex.Message}");
            }
        }

        public async Task<Response> Update(RoleV2UpdateModel model)
        {
            try
            {
                var entity = await _dataContext.IdmRole
                         .FirstOrDefaultAsync(x => x.Id == model.Id);
                Log.Information("Before Update: " + JsonSerializer.Serialize(entity));
                model.UpdateToEntity(entity);
                _dataContext.IdmRole.Update(entity);
                var listOrgId = _dataContext.IdmRoleMapOrganization.Where(x => x.RoleId == model.Id);
                _dataContext.IdmRoleMapOrganization.RemoveRange(listOrgId);
                foreach (var item in model.ListOrganizationId)
                {
                    var organizationItem = new IdmRoleMapOrganization()
                    {
                        RoleId = entity.Id,
                        OrganizationId = item,
                        StatusDefault = true,
                        Order = 1,
                        IdentityNumber = 1
                    };
                    await _dataContext.IdmRoleMapOrganization.AddAsync(organizationItem);
                }
                int dbSave = await _dataContext.SaveChangesAsync();
                if (dbSave > 0)
                {
                    Log.Information("After Update: " + JsonSerializer.Serialize(entity));
                    InvalidCache(model.Id.ToString());

                    return new ResponseObject<Guid>(model.Id, MessageConstants.UpdateSuccessMessage, Code.Success);
                }
                else
                {
                    return new ResponseError(Code.ServerError, MessageConstants.UpdateErrorMessage);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, MessageConstants.ErrorLogMessage);
                return new ResponseError(Code.ServerError, $"{MessageConstants.GetDataErrorMessage} - {ex.Message}");
            }
        }

        public async Task<Response> Delete(List<Guid> listId)
        {
            try
            {
                var listResult = new List<ResponeDeleteModel>();
                var name = "";
                Log.Information("List Delete: " + JsonSerializer.Serialize(listId));
                foreach (var item in listId)
                {
                    name = "";
                    var entity = await _dataContext.IdmRole.FindAsync(item);

                    if (entity == null)
                    {
                        listResult.Add(new ResponeDeleteModel()
                        {
                            Id = item,
                            Name = name,
                            Result = false,
                            Message = MessageConstants.DeleteItemNotFoundMessage
                        });
                    }
                    else
                    {
                        name = entity.Name;
                        var listOrgId = _dataContext.IdmRoleMapOrganization.Where(x => x.RoleId == entity.Id);
                        _dataContext.IdmRoleMapOrganization.RemoveRange(listOrgId);
                        _dataContext.IdmRole.Remove(entity);
                        try
                        {
                            int dbSave = await _dataContext.SaveChangesAsync();
                            if (dbSave > 0)
                            {
                                InvalidCache(item.ToString());

                                listResult.Add(new ResponeDeleteModel()
                                {
                                    Id = item,
                                    Name = name,
                                    Result = true,
                                    Message = MessageConstants.DeleteItemSuccessMessage
                                });
                            }
                            else
                            {
                                listResult.Add(new ResponeDeleteModel()
                                {
                                    Id = item,
                                    Name = name,
                                    Result = false,
                                    Message = MessageConstants.DeleteItemErrorMessage
                                });
                            }
                        }
                        catch (Exception ex)
                        {
                            Log.Error(ex, MessageConstants.ErrorLogMessage);
                            listResult.Add(new ResponeDeleteModel()
                            {
                                Id = item,
                                Name = name,
                                Result = false,
                                Message = ex.Message
                            });
                        }
                    }
                }
                Log.Information("List Result Delete: " + JsonSerializer.Serialize(listResult));
                return new ResponseObject<List<ResponeDeleteModel>>(listResult, MessageConstants.DeleteSuccessMessage, Code.Success);

            }
            catch (Exception ex)
            {
                Log.Error(ex, MessageConstants.ErrorLogMessage);
                return new ResponseError(Code.ServerError, $"{MessageConstants.DeleteErrorMessage} - {ex.Message}");
            }
        }

        public async Task<Response> Filter(RoleV2QueryFilter filter)
        {
            try
            {
                var data = (from role in _dataContext.IdmRole

                            join org in _dataContext.IdmRoleMapOrganization
                            on role.Id equals org.RoleId
                            into org
                            from org_item in org.DefaultIfEmpty()

                            join organz in _dataContext.CmsOrganization
                            on org_item.OrganizationId equals organz.Id
                            into organz
                            from organz_item in organz.DefaultIfEmpty()

                            select new RoleV2BaseModel()
                            {
                                OrganizationId = organz_item.Id,
                                OrganizationName = organz_item.Name,
                                Id = role.Id,
                                Code = role.Code,
                                Name = role.Name,
                                Status = role.Status,
                                Order = role.Order,
                                Description = role.Description,
                                CreatedOnDate = role.CreatedOnDate,
                                IsDigitalSigned = role.IsDigitalSigned,

                            });

                if (!string.IsNullOrEmpty(filter.TextSearch))
                {
                    string ts = filter.TextSearch.Trim().ToLower();
                    data = data.Where(x => x.Code.ToLower().Contains(ts) || x.Name.ToLower().Contains(ts));
                }

                if (filter.Status.HasValue)
                {
                    data = data.Where(x => x.Status == filter.Status);
                }
                if (filter.Id.HasValue)
                {
                    data = data.Where(x => x.Id == filter.Id);
                }
                if (filter.OrganizationId.HasValue)
                {
                    data = data.Where(x => x.OrganizationId == filter.OrganizationId);
                }

                data = data.OrderByField(filter.PropertyName, filter.Ascending);

                int totalCount = data.Count();

                // Pagination
                if (filter.PageSize.HasValue && filter.PageNumber.HasValue)
                {
                    if (filter.PageSize <= 0)
                    {
                        filter.PageSize = QueryFilter.DefaultPageSize;
                    }

                    //Calculate nunber of rows to skip on pagesize
                    int excludedRows = (filter.PageNumber.Value - 1) * (filter.PageSize.Value);
                    if (excludedRows <= 0)
                    {
                        excludedRows = 0;
                    }

                    // Query
                    data = data.Skip(excludedRows).Take(filter.PageSize.Value);
                }
                int dataCount = data.Count();

                var listResult = await data.ToListAsync();
                return new ResponseObject<PaginationList<RoleV2BaseModel>>(new PaginationList<RoleV2BaseModel>()
                {
                    DataCount = dataCount,
                    TotalCount = totalCount,
                    PageNumber = filter.PageNumber ?? 0,
                    PageSize = filter.PageSize ?? 0,
                    Data = listResult
                }, MessageConstants.GetDataSuccessMessage, Code.Success);
            }
            catch (Exception ex)
            {
                Log.Error(ex, MessageConstants.ErrorLogMessage);
                return new ResponseError(Code.ServerError, $"{MessageConstants.GetDataErrorMessage} - {ex.Message}");
            }
        }

        public async Task<Response> GetById(Guid id)
        {
            try
            {
                string cacheKey = BuildCacheKey(id.ToString());
                var rs = await _cacheService.GetOrCreate(cacheKey, async () =>
                {
                    var entity = await _dataContext.IdmRole
                        .FirstOrDefaultAsync(x => x.Id == id);

                    return AutoMapperUtils.AutoMap<IdmRole, RoleV2Model>(entity);
                });
                return new ResponseObject<RoleV2Model>(rs, MessageConstants.GetDataSuccessMessage, Code.Success);
            }
            catch (Exception ex)
            {
                Log.Error(ex, MessageConstants.ErrorLogMessage);
                return new ResponseError(Code.ServerError, $"{MessageConstants.GetDataErrorMessage} - {ex.Message}");
            }
        }

        public async Task<Response> GetListCombobox(int count = 0, string textSearch = "")
        {
            try
            {
                string cacheKey = BuildCacheKey(SelectItemCacheSubfix);
                var list = await _cacheService.GetOrCreate(cacheKey, async () =>
                {
                    var data = (from item in _dataContext.IdmRole.Where(x => x.Status == true).OrderBy(x => x.Order).ThenBy(x => x.Name)
                                select new SelectItemModel()
                                {
                                    Id = item.Id,
                                    Name = item.Name,
                                    Note = ""
                                });

                    return await data.ToListAsync();
                });

                if (!string.IsNullOrEmpty(textSearch))
                {
                    textSearch = textSearch.ToLower().Trim();
                    list = list.Where(x => x.Name.ToLower().Contains(textSearch) || x.Note.ToLower().Contains(textSearch)).ToList();
                }

                if (count > 0)
                {
                    list = list.Take(count).ToList();
                }

                return new ResponseObject<List<SelectItemModel>>(list, MessageConstants.GetDataSuccessMessage, Code.Success);
            }
            catch (Exception ex)
            {
                Log.Error(ex, MessageConstants.ErrorLogMessage);
                return new ResponseError(Code.ServerError, $"{MessageConstants.GetDataErrorMessage} - {ex.Message}");
            }
        }

        private void InvalidCache(string id = "")
        {
            if (!string.IsNullOrEmpty(id))
            {
                string cacheKey = BuildCacheKey(id);
                _cacheService.Remove(cacheKey);
            }

            string selectItemCacheKey = BuildCacheKey(SelectItemCacheSubfix);
            _cacheService.Remove(selectItemCacheKey);
        }

        private string BuildCacheKey(string id)
        {
            return $"{CachePrefix}-{id}";
        }
    }
}