﻿using DigitalID.Data;
using System;
using System.Collections.Generic;

namespace DigitalID.Business
{
    public class RoleV2BaseModel
    {
        public Guid Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public bool Status { get; set; }
        public int Order { get; set; }
        public string Description { get; set; }
        public DateTime? CreatedOnDate { get; set; }
        public bool IsDigitalSigned { get; set; }
        public Guid? OrganizationId { get; set; }
        public string OrganizationName { get; set; }
        

    }

    public class RoleV2Model : RoleV2BaseModel
    {
        public List<Guid> ListOrganizationId { get; set; }
    }

    public class RoleV2DetailModel : RoleV2Model
    {
        public Guid? CreatedByUserId { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public Guid? ModifiedUserId { get; set; }

        public Guid? ApplicationId { get; set; }
    }

    public class RoleV2CreateModel : RoleV2Model
    {
        public Guid? CreatedByUserId { get; set; }
        public Guid? ApplicationId { get; set; }
       
    }

    public class RoleV2UpdateModel : RoleV2Model
    {
        public Guid ModifiedUserId { get; set; }
        public void UpdateToEntity(IdmRole entity)
        {
            entity.Code = this.Code;
            entity.Name = this.Name;
            entity.Status = this.Status;
            entity.Description = this.Description;
            entity.LastModifiedOnDate = DateTime.Now;
            entity.Order = this.Order;
            entity.IsDigitalSigned = this.IsDigitalSigned;
        }
    }

    public class RoleV2QueryFilter
    {
        public string TextSearch { get; set; }
        public int? PageSize { get; set; }
        public int? PageNumber { get; set; }
        public bool? Status { get; set; }
        public Guid? Id { get; set; }
        public Guid? OrganizationId { get; set; }
        public Guid? AreaOperationId { get; set; }
        public string PropertyName { get; set; } = "CreatedOnDate";
        //asc - desc
        public string Ascending { get; set; } = "desc";
        public RoleV2QueryFilter()
        {
            PageNumber = QueryFilter.DefaultPageNumber;
            PageSize = QueryFilter.DefaultPageSize;
        }
    }
}