﻿using DigitalID.Data;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DigitalID.Business
{
    public interface ITokenHandler
    {
        Task<Response> CreateAsync(TokenCreateUpdateModel model, Guid applicationId, Guid userId);
        ResponseV2<bool> DeleteByUser(Guid userId);
    }
}
