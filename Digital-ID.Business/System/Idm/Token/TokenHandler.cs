﻿using DigitalID.Data;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DigitalID.Business
{
    public class TokenHandler : ITokenHandler
    {
        private readonly DbHandler<IdmToken, TokenModel, TokenQueryModel> _dbHandler = DbHandler<IdmToken, TokenModel, TokenQueryModel>.Instance;
        public async Task<Response> CreateAsync(TokenCreateUpdateModel model, Guid applicationId, Guid userId)
        {
            try
            {
                IdmToken itemUpdate = AutoMapperUtils.AutoMap<TokenCreateUpdateModel, IdmToken>(model);
                itemUpdate = itemUpdate.InitCreate(applicationId, userId);
                itemUpdate.Id = Guid.NewGuid();
                var result = await _dbHandler.CreateAsync(itemUpdate);
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ResponseV2<bool> DeleteByUser(Guid userId)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    unitOfWork.GetRepository<IdmToken>().DeleteRange(unitOfWork.GetRepository<IdmToken>().GetMany(x => x.UserId == userId));
                    unitOfWork.Save();
                    return new ResponseV2<bool>(1, "Success", true);
                }
            }
            catch (Exception ex)
            {
                return new ResponseV2<bool>(-1, ex.Message, false);
            }
        }
    }
}