﻿using DigitalID.Data;
using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace DigitalID.Business
{
    public class BaseTokenModel : BaseModel
    {
        public Guid Id { get; set; }
        public new Guid? CreatedByUserId { get; set; }
        public new Guid? LastModifiedByUserId { get; set; }
        public new DateTime? LastModifiedOnDate { get; set; }
        public new DateTime? CreatedOnDate { get; set; }
        public new Guid ApplicationId { get; set; }
        //client_id uniqueidentifier    NO
        [JsonProperty("client_id")]
        public Guid? ClientId { get; set; }

        //access_token            varchar(max)    NO
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }

        //refresh_token   varchar(max)    YES
        [JsonProperty("refresh_token")]
        public string RefreshToken { get; set; }

        //token_type  varchar(50) YES
        [JsonProperty("token_type")]
        public string TokenType { get; set; }

        //expiry_time datetime2 NO
        [JsonProperty("expiry_time")]
        public DateTime ExpiryTime { get; set; }

        //user_id uniqueidentifier    NO
        [JsonProperty("user_id")]
        public Guid UserId { get; set; }

        //username    varchar(50) YES
        [JsonProperty("username")]
        public string UserName { get; set; }

        //is_active   bit NO
        [JsonProperty("is_active")]
        public bool IsActive { get; set; }

        //algorithm varchar(50) YES
        [JsonProperty("algorithm")]
        public string Algorithm { get; set; }

        //issuer  nvarchar(500)   YES
        [JsonProperty("issuer")]
        public string Issuer { get; set; }

        //issued_at   datetime2 NO
        [JsonProperty("issued_at")]
        public DateTime IssuedAt { get; set; }

        //hmac text    YES
        [JsonProperty("hmac")]
        public string Hmac { get; set; }


    }
    public class TokenModel : BaseTokenModel
    {
    }

    public class TokenQueryModel : PaginationRequest
    {
        public string AccessToken { get; set; }
        public string UserName { get; set; }
    }

    public class TokenCreateUpdateModel
    {
        //client_id uniqueidentifier    NO
        [JsonProperty("client_id")]
        public Guid? ClientId { get; set; }

        //access_token            varchar(max)    NO
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }

        //refresh_token   varchar(max)    YES
        [JsonProperty("refresh_token")]
        public string RefreshToken { get; set; }

        //token_type  varchar(50) YES
        [JsonProperty("token_type")]
        public string TokenType { get; set; }

        //expiry_time datetime2 NO
        [JsonProperty("expiry_time")]
        public DateTime ExpiryTime { get; set; }

        //user_id uniqueidentifier    NO
        [JsonProperty("user_id")]
        public Guid UserId { get; set; }

        //username    varchar(50) YES
        [JsonProperty("username")]
        public string UserName { get; set; }

        //is_active   bit NO
        [JsonProperty("is_active")]
        public bool IsActive { get; set; }

        //algorithm varchar(50) YES
        [JsonProperty("algorithm")]
        public string Algorithm { get; set; }

        //issuer  nvarchar(500)   YES
        [JsonProperty("issuer")]
        public string Issuer { get; set; }

        //issued_at   datetime2 NO
        [JsonProperty("issued_at")]
        public DateTime IssuedAt { get; set; }

        //hmac text    YES
        [JsonProperty("hmac")]
        public string Hmac { get; set; }
    }
}

