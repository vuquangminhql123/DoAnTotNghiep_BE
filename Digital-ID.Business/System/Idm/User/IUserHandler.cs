﻿using DigitalID.Data;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DigitalID.Business
{
    public interface IUserHandler
    {
        #region Challenges
        ResponseV2<ChallengesOutputModel> SignChallenge(ChallengesInputModel value);
        #endregion

        #region Cert
        Task<Response> CreateKey(CertCreatModel model, Guid applicationId, Guid userAdminId);
        Task<Response> CreateCert(CertCreatModel model, Guid applicationId, Guid userAdminId);
        ResponseObject<string> DowloadCert(Guid userId, Guid applicationId, Guid userAdminId);
        Task<ResponseObject<CertificateProfileRespone>> GetCertificateProfile();
        #endregion

        #region QR Code
        ResponseObject<string> CreateQRCode(QrCodeInputModel model, Guid applicationId, Guid userAdminId);
        #endregion

        #region TOTP
        Task<ResponseObject<string>> RegisterTOTPAsync(string username, Guid applicationId, Guid userAdminId);
        #endregion

        #region CRUD 
        Task<Response> CreateAsync(UserCreateModel model, Guid applicationId);        
        Task<Response> UpdateAsync(Guid id, UserUpdateModel model, Guid applicationId);
        Task<Response> DeleteAsync(Guid id);
        Task<Response> DeleteRangeAsync(IList<Guid> listId);
        Task<Response> FindAsync(Guid id);
        #endregion

        Task<Response> CheckNameAvailability(string name, Guid? applicationId = null);
        Task<Response> GetDetail(Guid id, Guid applicationId);
        Task<Response> GetPageAsync(UserQueryModel query);
        Task<Response> GetAllAsync(UserQueryModel query);
        Task<Response> GetAllAsync();
        Response GetAll();
        Response AuthenticationV2(string userName, string password, bool isNotLogin = false);
        Response Authentication(string userName, string password, bool isNotLogin = false);
        Task<Response> LockUser(Guid userId);
        Task<Response> UnLockUser(Guid userId);
        Task<Response> PatchUserInfo(Guid id, UserPatchModel patchModel);
        Task<Response> ChangePasswordAsync(Guid id, string password, Guid applicationId, string currentPassword);
    }
}
