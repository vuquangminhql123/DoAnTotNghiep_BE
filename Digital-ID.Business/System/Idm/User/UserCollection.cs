﻿using DigitalID.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace DigitalID.Business
{
    public class UserCollection
    {
        private readonly IUserHandler _handler;
        public HashSet<UserModel> Collection;
        public static UserCollection Instance { get; } = new UserCollection();

        protected UserCollection()
        {
            _handler = new UserHandler();
            LoadToHashSet();
        }

        public void LoadToHashSet()
        {
            Collection = new HashSet<UserModel>();
            // Query to list
            var listResponse = _handler.GetAll();
            // Add to hashset
            if (listResponse.Code == Code.Success)
            {
                // Add to hashset
                if (listResponse is ResponseList<UserModel> listResponseData)
                    foreach (var response in listResponseData.Data)
                    {
                        Collection.Add(response);
                    }
            }
        }
        public string GetName(Guid id)
        {
            var result = Collection.FirstOrDefault(u => u.Id == id);
            return result?.UserName;
        }
        public bool CheckName(string userName)
        {
            var result = Collection.FirstOrDefault(u => u.UserName == userName);
            return result != null;
        }
        public bool CheckOtherUser(string userName, Guid id)
        {
            var result = Collection.FirstOrDefault(u => u.UserName == userName && u.Id != id);
            return result != null;
        }
        public BaseUserModel GetModel(Guid id)
        {
            var result = Collection.FirstOrDefault(u => u.Id == id);
            return result;
        }
        public BaseUserModel GetModel(string userName)
        {
            var result = Collection.FirstOrDefault(u => u.UserName == userName);
            return result;
        }

        public List<BaseUserModel> GetModel(Expression<Func<BaseUserModel, bool>> predicate)
        {
            var result = Collection.AsQueryable().Where(predicate);
            return result.ToList();
        }
    }
}