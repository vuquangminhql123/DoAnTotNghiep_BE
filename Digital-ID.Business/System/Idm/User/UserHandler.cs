﻿using DigitalID.Data;
using LinqKit;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using QRCoder;
using Serilog;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Linq.Expressions;
using System.Net.Http;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace DigitalID.Business
{
    public class UserHandler : IUserHandler
    {
        private readonly DbHandler<IdmUser, UserModel, UserQueryModel> _dbHandler =
            DbHandler<IdmUser, UserModel, UserQueryModel>.Instance;
        private readonly IEmailHandler _emailHandler;
                private string frontendUrl = Utils.GetConfig("Url:Frontend");
        private readonly IRightMapUserHandler _rightMapUserHandler;
        private readonly IUserMapRoleHandler _userMapRoleHandler;
        private readonly IUserMapDVHandler _userMapDVHandler;
        private readonly ITokenHandler _tokenHandler;

        public UserHandler(IUserMapRoleHandler userMapRoleHandler, IEmailHandler emailHandler, IRightMapUserHandler rightMapUserHandler, IUserMapDVHandler userMapDVHandler, ITokenHandler tokenHandler)
        {
            _emailHandler = emailHandler;
            _userMapRoleHandler = userMapRoleHandler;
            _rightMapUserHandler = rightMapUserHandler;
            _userMapDVHandler = userMapDVHandler;
            _tokenHandler = tokenHandler;
        }

        public UserHandler()
        {
        }

        public Response GetAll()
        {
            return _dbHandler.GetAll();
        }
        public async Task<Response> CheckNameAvailability(string name, Guid? applicationId = null)
        {

            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    var result = await unitOfWork.GetRepository<IdmUser>().FindAsync(s => s.UserName == name);
                    return new ResponseObject<bool>(result == null);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return new ResponseError(Code.ServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }

        public async Task<Response> GetDetail(Guid id, Guid applicationId)
        {
            var model = await _dbHandler.FindAsync(id);
            if (model.Code == Code.Success)
            {
                var modelData = model as ResponseObject<UserModel>;
                var result = AutoMapperUtils.AutoMap<UserModel, UserDetailModel>(modelData?.Data);
                var listRightResponse = await _rightMapUserHandler.GetRightMapUserAsync(id, applicationId);
                var listRoleResponse = await _userMapRoleHandler.GetRoleMapUserAsync(id, applicationId);
                if (listRightResponse.Code == Code.Success && listRoleResponse.Code == Code.Success)
                {
                    var listRightResponseData = listRightResponse as ResponseList<BaseRightModelOfUser>;
                    var listRoleResponseData = listRoleResponse as ResponseList<BaseRoleModel>;
                    if (listRightResponseData != null) result.ListRight = listRightResponseData.Data;
                    if (listRoleResponseData != null) result.ListRole = listRoleResponseData.Data;
                    return new ResponseObject<UserDetailModel>(result);
                }

                return new ResponseError(Code.BadRequest, "Không thể lấy dữ liệu  chi tiết");
            }

            return model;
        }

        public Task<Response> GetPageAsync(UserQueryModel query)
        {
            var predicate = BuildQuery(query);
            var data = _dbHandler.GetPageAsync(predicate, query);
            return data;
        }

        public Task<Response> GetAllAsync(UserQueryModel query)
        {
            var predicate = BuildQuery(query);
            return _dbHandler.GetAllAsync(predicate, query.Sort);
        }

        public Task<Response> GetAllAsync()
        {
            var predicate = PredicateBuilder.New<IdmUser>(true);
            return _dbHandler.GetAllAsync(predicate);
        }

        public Response Authentication(string userName, string password, bool isNotLogin = false)
        {
            using (var unitOfWork = new UnitOfWork())
            {
                UserModel userReturn = new UserModel();
                var userCollection = UserCollection.Instance.Collection;
                var user = unitOfWork.GetRepository<IdmUser>().GetMany(x => x.UserName == userName).FirstOrDefault();
                if (user != null)
                {
                    var passhash = Utils.PasswordGenerateHmac(password, user.PasswordSalt);
                    if (passhash == user.Password)
                    {
                        if (!isNotLogin)
                        {
                            PatchUserInfo(user.Id, new UserPatchModel()
                            {
                                LastActivityDate = DateTime.Now
                            }).FireAndForget();
                        }
                        foreach (var item in userCollection)
                        {
                            if (item.UserName == userName)
                                userReturn = item;
                        }
                        return new ResponseObject<UserModel>(userReturn);
                    }
                    return new ResponseError(Code.BadRequest, "Sai mật khẩu");
                }

                return new ResponseError(Code.BadRequest, "Không tìm thấy tài khoản");
            }
        }

        /// <summary>
        /// Dùng cho API GetToken cho dịch vụ bên ngoài kết nối
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <param name="isNotLogin"></param>
        /// <returns></returns>
        public Response AuthenticationV2(string userName, string password, bool isNotLogin = false)
        {
            using (var unitOfWork = new UnitOfWork())
            {
                UserModel userReturn = new UserModel();
                var user = unitOfWork.GetRepository<IdmUser>().GetMany(x => x.UserName == userName).FirstOrDefault();
                if (user != null)
                {
                    var passhash = Utils.PasswordGenerateHmac(password, user.PasswordSalt);
                    if (passhash == user.Password)
                    {
                        userReturn.Id = user.Id;
                        userReturn.Name = user.Name;
                        userReturn.UserName = user.UserName;
                        return new ResponseObject<UserModel>(userReturn);
                    }
                    return new ResponseError(Code.BadRequest, "Sai mật khẩu");
                }
                return new ResponseError(Code.BadRequest, "Không tìm thấy tài khoản");
            }
        }

        private Expression<Func<IdmUser, bool>> BuildQuery(UserQueryModel query)
        {
            var predicate = PredicateBuilder.New<IdmUser>(true);

            if (!string.IsNullOrEmpty(query.UserName)) predicate.And(s => s.UserName == query.UserName);
            if (query.Id.HasValue) predicate.And(s => s.Id == query.Id);
            if (query.ListId != null) predicate.And(s => query.ListId.Contains(s.Id));
            predicate.And(s => s.Type == query.Type);

            if (!string.IsNullOrEmpty(query.FullTextSearch))
                predicate.And(s => s.UserName.ToLower().Contains(query.FullTextSearch.ToLower()));
            return predicate;
        }

        #region Challenges
        public ResponseV2<ChallengesOutputModel> SignChallenge(ChallengesInputModel value)
        {
            string pathPfx = "";
            try
            {
                ChallengesOutputModel data = new ChallengesOutputModel();
                using (var unitOfWork = new UnitOfWork())
                {
                    var checkApp = unitOfWork.GetRepository<SysApplication>().Find(x => x.Id == value.ApplicationId);
                    if (checkApp == null)
                        return new ResponseV2<ChallengesOutputModel>(0, "Not found APP", null);
                    var checkUser = unitOfWork.GetRepository<IdmUser>().Find(x => x.UserName == value.Username && x.IsLocked == false);
                    if (checkUser == null)
                        return new ResponseV2<ChallengesOutputModel>(0, "Not found User or Locked", null);
                    var checkCert = (from uc in unitOfWork.GetRepository<CmsCertUser>().GetAll()
                                     join c in unitOfWork.GetRepository<CmsCert>().GetAll()
                                     on uc.CertId equals c.Id
                                     where uc.UserId == checkUser.Id
                                     select c).FirstOrDefault();
                    if (checkCert == null)
                        return new ResponseV2<ChallengesOutputModel>(0, "Not found Cert for User", null);
                    //pathPfx = @"C:\Cert\vanhn.p12";
                    //string passPfx = "pFshQAOg";
                    pathPfx = checkCert.P12Path;
                    string passPfx = checkCert.UserPin;
                    X509Certificate2 certificate = null;
                    try
                    {
                        if (!File.Exists(@"" + pathPfx + ""))
                            return new ResponseV2<ChallengesOutputModel>(-1, "File not exists", null);
                        certificate = new X509Certificate2(@"" + pathPfx + "", passPfx, X509KeyStorageFlags.MachineKeySet
                             | X509KeyStorageFlags.PersistKeySet
                             | X509KeyStorageFlags.Exportable);
                    }
                    catch (Exception ex)
                    {
                        return new ResponseV2<ChallengesOutputModel>(-1, ex.Message, null);
                    }
                    if (certificate != null)
                    {
                        using (RSA rsa = certificate.GetRSAPrivateKey())
                        {
                            byte[] dataSign = Encoding.Default.GetBytes(value.ChallengeString);
                            // Signing
                            var signature = rsa.SignData(dataSign, HashAlgorithmName.SHA256, RSASignaturePadding.Pkcs1);

                            data.Signature = Convert.ToBase64String(signature);
                            BaseUserModel user = new BaseUserModel();
                            user.Id = checkUser.Id;
                            string tokenStr = IdmHelper.BuildToken(user, false, 31536000);//~ 1 năm Time To Live Token pass Check Authen on APP
                            string keyEncrypt = "k3mgc9JuQU7FC!Vg";
                            data.SecToken = IdmHelper.Encrypt(tokenStr, keyEncrypt);

                            #region Read token and save to DB
                            var handerJwt = new JwtSecurityTokenHandler();
                            SecurityToken validatedToken;
                            handerJwt.ValidateToken(tokenStr, new TokenValidationParameters()
                            {
                                ValidateIssuer = true,
                                ValidateAudience = true,
                                ValidateLifetime = true,
                                ValidateIssuerSigningKey = true,
                                ValidIssuer = Utils.GetConfig("Authentication:Jwt:Issuer"),
                                ValidAudience = Utils.GetConfig("Authentication:Jwt:Issuer"),
                                IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Utils.GetConfig(("Authentication:Jwt:Key")))),
                            }, out validatedToken);
                            TokenCreateUpdateModel modelToken = new TokenCreateUpdateModel();
                            modelToken.AccessToken = tokenStr;

                            modelToken.ExpiryTime = validatedToken.ValidFrom.AddSeconds(300);//300s Time To Live Token - lưu DB
                            modelToken.IsActive = true;
                            modelToken.IssuedAt = validatedToken.ValidFrom;
                            modelToken.Issuer = checkApp.Name;
                            modelToken.TokenType = "JWT";
                            modelToken.UserId = checkUser.Id;
                            modelToken.UserName = checkUser.UserName;

                            //Delete token by user before create new token
                            _tokenHandler.DeleteByUser(checkUser.Id);

                            //Add new token
                            var rt = _tokenHandler.CreateAsync(modelToken, value.ApplicationId, checkUser.Id);
                            if (rt.Result.Code != Code.Success)
                                return new ResponseV2<ChallengesOutputModel>(0, "Create token fail", null);
                            #endregion

                            return new ResponseV2<ChallengesOutputModel>(1, "Success", data);
                        }
                    }
                    else
                    {
                        return new ResponseV2<ChallengesOutputModel>(0, "Not found Cert", null);
                    }
                }
            }
            catch (Exception ex)
            {
                return new ResponseV2<ChallengesOutputModel>(-1, ex.Message, null);
            }
        }
        #endregion

        #region QR Code
        /// <summary>
        /// Tạo qr code
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="applicationId"></param>
        /// <param name="userAdminId"></param>
        /// <returns></returns>
        public ResponseObject<string> CreateQRCode(QrCodeInputModel model, Guid applicationId, Guid userAdminId)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    applicationId = new Guid("E6152921-D220-4837-A25C-66D2A0E0629A");//Hiện tại để mặc định tạo QR cho FIDO
                    var user = unitOfWork.GetRepository<IdmUser>().Find(x => x.Id == model.userId);
                    if (user == null)
                        return new ResponseObject<string>("", "Không tìm thấy thông tin tài khoản", Code.NotFound);
                    var checkQR = unitOfWork.GetRepository<IdmQRCode>().Find(x => x.UserId == model.userId && x.ApplicationId == applicationId);
                    if (checkQR != null)
                        return new ResponseObject<string>(checkQR.QRCode, "Success", Code.Success);
                    string apiLink = "/api/v1/service/deviceupdate";//API update info device
                    QRCodeGenerator qrGenerator = new QRCodeGenerator();
                    string content = "{\"uid\":\"" + user.Id + "\",\"uri\":\"" + apiLink + "\"}";
                    QRCodeData qrCodeData = qrGenerator.CreateQrCode(content, QRCodeGenerator.ECCLevel.Q);
                    QRCode qrCode = new QRCode(qrCodeData);
                    Bitmap qrCodeImage = qrCode.GetGraphic(20);
                    var byteQR = Utils.BitmapToBytes(qrCodeImage);
                    #region Lưu vào DB
                    IdmQRCode item = new IdmQRCode();
                    item.ApiLink = apiLink;
                    item.ApplicationId = applicationId;
                    item.CreatedDate = DateTime.Now;
                    item.CreatedUserId = userAdminId;
                    item.ExpiryDate = DateTime.Now.AddMinutes(30);
                    item.Id = Guid.NewGuid();
                    item.ModifiedDate = DateTime.Now;
                    item.ModifiedUserId = userAdminId;
                    item.Purpose = "FIDO";
                    item.QRCode = Convert.ToBase64String(byteQR);
                    item.QRCodeCreateDate = DateTime.Now;
                    item.status = 0;//Chưa scan
                    item.UserId = model.userId;
                    item.UserName = user.UserName;
                    item.PeriodTime = 1;//Chưa biết ý nghĩa nên để mặc định
                    unitOfWork.GetRepository<IdmQRCode>().Add(item);
                    unitOfWork.Save();
                    #endregion
                    return new ResponseObject<string>(Convert.ToBase64String(byteQR), "Success", Code.Success);
                }
            }
            catch (Exception ex)
            {
                return new ResponseObject<string>("", ex.Message, Code.ServerError);
            }
        }
        #endregion

        #region TOTP
        public async Task<ResponseObject<string>> RegisterTOTPAsync(string username, Guid applicationId, Guid userAdminId)
        {
            try
            {
                string totpInitUri = Utils.GetConfig("TOTP:ApiUrl");
                string totpUserName = Utils.GetConfig("TOTP:Basic_User");
                string totpPassword = Utils.GetConfig("TOTP:Basic_Pass");
                HttpClientHandler clientHandler = new HttpClientHandler();
                clientHandler.ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) => { return true; };
                using (var unitOfWork = new UnitOfWork())
                {
                    QRCodeGenerator qrGenerator = new QRCodeGenerator();
                    #region Init TOTP
                    using (var httpClient = new HttpClient(clientHandler))
                    {
                        string requestUri = totpInitUri + "/initTOTP";
                        var byteArray = Encoding.ASCII.GetBytes(""+ totpUserName + ":"+ totpPassword + "");
                        httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));
                        var stringContent = new FormUrlEncodedContent(new[]
                        {
                            new KeyValuePair<string, string>("username", username),
                        });
                        var response = await httpClient.PostAsync(requestUri, stringContent);
                        var responseContent = await response.Content.ReadAsStringAsync();
                        XmlDocument xmlDoc = new XmlDocument();
                        xmlDoc.LoadXml(responseContent);
                        XmlNodeList datas = xmlDoc.GetElementsByTagName("ns:return");
                        XmlNode node = datas.Item(0);
                        if(node == null)
                            return new ResponseObject<string>("", "Tài khoản này chưa có trên IS(LDAP)", Code.NotFound);
                        string totpInitResult = node.InnerText;
                        var base64EncodedBytes = Convert.FromBase64String(totpInitResult);
                        string content = Encoding.UTF8.GetString(base64EncodedBytes);
                        QRCodeData qrCodeData = qrGenerator.CreateQrCode(content, QRCodeGenerator.ECCLevel.Q);
                        QRCode qrCode = new QRCode(qrCodeData);
                        Bitmap qrCodeImage = qrCode.GetGraphic(20);
                        var byteQR = Utils.BitmapToBytes(qrCodeImage);
                        return new ResponseObject<string>(Convert.ToBase64String(byteQR), "Success", Code.Success);
                    }
                    #endregion                    
                }
            }
            catch (Exception ex)
            {
                return new ResponseObject<string>("", ex.Message, Code.ServerError);
            }
        }
        #endregion

        #region Cert

        /// <summary>
        /// Tạo khóa cho người dùng
        /// </summary>
        /// <param name="model"></param>
        /// <param name="applicationId"></param>
        /// <param name="userAdminId"></param>
        /// <returns></returns>
        public async Task<Response> CreateKey(CertCreatModel model, Guid applicationId, Guid userAdminId)
        {
            try
            {
                string basic_user = Utils.GetConfig("Sign:Basic_User");
                string basic_pass = Utils.GetConfig("Sign:Basic_Pass");
                string apiGenKey = Utils.GetConfig("Sign:ApiUrl_GenKey");
                using (var unitOfWork = new UnitOfWork())
                {
                    #region Call API genkey
                    using (var httpClient = new HttpClient())
                    {
                        KeyCreateModel dataGenKey = new KeyCreateModel();
                        dataGenKey.alias = model.alias;
                        dataGenKey.keyAlgorithmId = model.keyAlgorithm.ToLower().Trim();
                        dataGenKey.keyLength = model.keyLenght;
                        dataGenKey.slotLabel = model.slotLabel;
                        dataGenKey.userPin = model.userPin;
                        var byteArray = Encoding.ASCII.GetBytes("" + basic_user + ":" + basic_pass + "");
                        httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));
                        var jsonString = JsonConvert.SerializeObject(dataGenKey);
                        var content = new StringContent(jsonString, Encoding.UTF8);
                        content.Headers.ContentType.MediaType = "application/json";
                        content.Headers.ContentType.CharSet = "UTF-8";
                        HttpResponseMessage rp = await httpClient.PostAsync(apiGenKey, content);
                        string sd = rp.Content.ReadAsStringAsync().Result;
                        var rs = JsonConvert.DeserializeObject<OuputGenKeyModel>(sd);

                        if (rs.result == "YES")
                        {
                            #region Add cert
                            CmsCert cert = new CmsCert();
                            cert.Alias = model.alias;
                            cert.ApplicationId = applicationId;
                            cert.CertType = model.certType;
                            cert.CreatedByUserId = userAdminId;
                            cert.CreatedOnDate = DateTime.Now;
                            cert.Id = Guid.NewGuid();
                            cert.Key = Utils.MD5Hash(model.key);//Mặc định
                            cert.KeyAlgorithm = model.keyAlgorithm;
                            cert.KeyLength = model.keyLenght;
                            cert.LastModifiedByUserId = userAdminId;
                            cert.LastModifiedOnDate = DateTime.Now;
                            cert.SlotID = 1;//Mặc định (không sử dụng)
                            cert.SlotLabel = model.slotLabel;//Mặc định slot hiện tại trên nCipher dev
                            cert.UserPin = model.userPin;//Mặc định của slot01 hiện tại trên nCipher dev
                            cert.Supplier = model.supplier;//Mặc định của dev CoreCA
                            cert.CertificateFileName = model.alias + ".cer";
                            unitOfWork.GetRepository<CmsCert>().Add(cert);
                            #endregion

                            #region Add cert user
                            CmsCertUser itemC = new CmsCertUser();
                            itemC.ApplicationId = applicationId;
                            itemC.CertId = cert.Id;
                            itemC.CreatedByUserId = userAdminId;
                            itemC.CreatedOnDate = DateTime.Now;
                            itemC.Id = Guid.NewGuid();
                            itemC.Key = Utils.MD5Hash(model.key);//Mặc định
                            itemC.LastModifiedByUserId = userAdminId;
                            itemC.LastModifiedOnDate = DateTime.Now;
                            itemC.UserId = model.userId;
                            unitOfWork.GetRepository<CmsCertUser>().Add(itemC);
                            #endregion

                            #region Update status user
                            var user = unitOfWork.GetRepository<IdmUser>().Find(x => x.Id == model.userId);
                            user.CertStatus = CustomerInfoCertStatus.GENERATED_KEY_NOT_CERT;//Đã tạo khóa chưa tạo cert
                            unitOfWork.GetRepository<IdmUser>().Update(user);
                            #endregion

                            await unitOfWork.SaveAsync();

                            return new Response(Code.Success, "Tạo khóa thành công");
                        }
                        else
                        {
                            return new Response(Code.ServerError, "Lỗi tạo khóa: " + rs.message);
                        }
                    }
                    #endregion                    
                }
            }
            catch (Exception ex)
            {
                return new Response(Code.ServerError, ex.Message);
            }
        }

        /// <summary>
        /// Lấy danh sách certificate profile từ core CA
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseObject<CertificateProfileRespone>> GetCertificateProfile()
        {
            try
            {
                string basic_user = Utils.GetConfig("Sign:Basic_User");
                string basic_pass = Utils.GetConfig("Sign:Basic_Pass");
                string apiGen = Utils.GetConfig("Sign:ApiUrl_GetCertProfile");
                #region Lấy danh sách
                using (var httpClient = new HttpClient())
                {
                    var byteArray = Encoding.ASCII.GetBytes("" + basic_user + ":" + basic_pass + "");
                    httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));
                    CertificateProfileResquest dataRequest = new CertificateProfileResquest();

                    NameFilter filter = new NameFilter();
                    filter.name = "G2";
                    dataRequest.Operator = "like";
                    dataRequest.NameNid = filter;

                    var jsonString = JsonConvert.SerializeObject(dataRequest);
                    var content = new StringContent(jsonString, Encoding.UTF8);
                    content.Headers.ContentType.MediaType = "application/json";
                    content.Headers.ContentType.CharSet = "UTF-8";
                    HttpResponseMessage rp = await httpClient.PostAsync(apiGen, content);
                    string sd = rp.Content.ReadAsStringAsync().Result;
                    var rs = JsonConvert.DeserializeObject<CertificateProfileRespone>(sd);
                    if (rs.errCode == "00")
                    {
                        return new ResponseObject<CertificateProfileRespone>(rs, "", Code.Success);
                    }
                    else
                    {
                        return new ResponseObject<CertificateProfileRespone>(null, rs.errMsg, Code.ServerError);
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                return new ResponseObject<CertificateProfileRespone>(null, ex.Message, Code.ServerError);
            }
        }

        /// <summary>
        /// Tạo cert cho người dùng
        /// </summary>
        /// <param name="model"></param>
        /// <param name="applicationId"></param>
        /// <param name="userAdminId"></param>
        /// <returns></returns>
        public async Task<Response> CreateCert(CertCreatModel model, Guid applicationId, Guid userAdminId)
        {
            try
            {
                string basic_user = Utils.GetConfig("Sign:Basic_User");
                string basic_pass = Utils.GetConfig("Sign:Basic_Pass");
                string apiGenCsr = Utils.GetConfig("Sign:ApiUrl_GenCSR");
                string apiGenCert = Utils.GetConfig("Sign:ApiUrl_GenCert");
                using (var unitOfWork = new UnitOfWork())
                {
                    model.endEntityProfileName = model.certificateProfileName;
                    //Lấy thông tin user và key
                    var user = unitOfWork.GetRepository<IdmUser>().Find(x => x.Id == model.userId);
                    CmsCert cert = (from a in unitOfWork.GetRepository<CmsCert>().GetAll()
                                    join b in unitOfWork.GetRepository<CmsCertUser>().GetAll()
                                    on a.Id equals b.CertId
                                    where b.UserId == model.userId
                                    select a).FirstOrDefault();

                    #region Call API gencsr
                    using (var httpClient = new HttpClient())
                    {
                        #region Tạo model cho API gen csr
                        CsrCreateModel dataGenCsr = new CsrCreateModel();
                        dataGenCsr.slotLabel = model.slotLabel;
                        dataGenCsr.userPin = model.userPin;
                        dataGenCsr.alias = cert.Alias;
                        string subjectDN = "";
                        if (model.certType == "1")//Cá nhân
                        {
                            subjectDN = "UID=CMT:" + model.subjectDN_CMT_MST + ",CN=" + model.subjectDN_CN + ",O=" + model.subjectDN_O + ",ST=" + model.subjectDN_ST + ",C=" + model.subjectDN_C;
                        }
                        else if (model.certType == "2")//Tổ chức, doanh nghiệp
                        {
                            subjectDN = "UID=MST:" + model.subjectDN_CMT_MST + ",CN=" + model.subjectDN_CN + ",O=" + model.subjectDN_O + ",ST=" + model.subjectDN_ST + ",C=" + model.subjectDN_C;
                        }
                        else if (model.certType == "3")//Cá nhân trong tổ chức (doanh nghiệp)
                        {
                            subjectDN = "UID=CMT:" + model.subjectDN_CMT_MST + ",CN=" + model.subjectDN_CN + ",O=" + model.subjectDN_O + ",ST=" + model.subjectDN_ST + ",C=" + model.subjectDN_C;
                        }

                        dataGenCsr.subjectDN = subjectDN;
                        dataGenCsr.hashAlgorithm = model.hashAlgorithm;
                        #endregion

                        #region Call API
                        var byteArray = Encoding.ASCII.GetBytes("" + basic_user + ":" + basic_pass + "");
                        httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));
                        var jsonString = JsonConvert.SerializeObject(dataGenCsr);
                        var content = new StringContent(jsonString, Encoding.UTF8);
                        content.Headers.ContentType.MediaType = "application/json";
                        content.Headers.ContentType.CharSet = "UTF-8";
                        HttpResponseMessage rp = await httpClient.PostAsync(apiGenCsr, content);
                        string sd = rp.Content.ReadAsStringAsync().Result;
                        var rs = JsonConvert.DeserializeObject<OuputGenCsrModel>(sd);
                        #endregion

                        if (Utils.IsValidBase64String(rs.csr))
                        {
                            #region update csr, cert, subject        
                            if (cert == null)
                                return new Response(Code.ServerError, "Lỗi tạo csr: Không tìm thấy thông tin key");

                            cert.LastModifiedByUserId = userAdminId;
                            cert.LastModifiedOnDate = DateTime.Now;
                            cert.Csr = rs.csr;

                            #region Tạo cert
                            using (var httpClientCert = new HttpClient())
                            {
                                httpClientCert.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));
                                CertCreateModel dataGenCert = new CertCreateModel();
                                dataGenCert.encoding = model.encoding;
                                dataGenCert.csr = rs.csr.Replace("-----BEGIN CERTIFICATE REQUEST-----", "").Replace("-----END CERTIFICATE REQUEST-----", "");
                                UserDataCert dataUser = new UserDataCert();
                                dataUser.username = user.UserName;
                                dataUser.subjectDN = subjectDN;
                                dataUser.email = user.Email;
                                dataUser.clearPwd = true;
                                dataUser.caName = model.supplier;
                                dataUser.endEntityProfileName = model.endEntityProfileName;
                                dataUser.certificateProfileName = model.certificateProfileName;
                                dataGenCert.userdata = dataUser;

                                var jsonStringCert = JsonConvert.SerializeObject(dataGenCert);
                                var contentCert = new StringContent(jsonStringCert, Encoding.UTF8);
                                contentCert.Headers.ContentType.MediaType = "application/json";
                                contentCert.Headers.ContentType.CharSet = "UTF-8";
                                HttpResponseMessage rpCert = await httpClientCert.PostAsync(apiGenCert, contentCert);
                                string sdCert = rpCert.Content.ReadAsStringAsync().Result;
                                var rsCert = JsonConvert.DeserializeObject<OuputGenCertModel>(sdCert);
                                if (rsCert.errCode == "00")
                                {
                                    //cert.Cert = rsCert.data;
                                    var base64EncodedBytes = System.Convert.FromBase64String(rsCert.data);
                                    string certString = System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
                                    int separatorIndex = certString.IndexOf("-----BEGIN CERTIFICATE-----");
                                    // Get everything else after 12th position     
                                    string certStringCut = certString.Substring(separatorIndex);
                                    var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(certStringCut);
                                    cert.Cert = System.Convert.ToBase64String(plainTextBytes);
                                }
                                else
                                {
                                    return new Response(Code.ServerError, "Lỗi tạo cert");
                                }
                            }
                            #endregion

                            cert.SubjectDN_C = model.subjectDN_C;
                            cert.SubjectDN_CMT_MST = model.subjectDN_CMT_MST;
                            cert.SubjectDN_CN = model.subjectDN_CN;
                            cert.SubjectDN_O = model.subjectDN_O;
                            cert.SubjectDN_ST = model.subjectDN_ST;
                            unitOfWork.GetRepository<CmsCert>().Update(cert);
                            #endregion                            

                            #region Update status user                            
                            user.CertStatus = CustomerInfoCertStatus.GENERATED_CERT;//Đã tạo cert
                            unitOfWork.GetRepository<IdmUser>().Update(user);
                            #endregion

                            await unitOfWork.SaveAsync();

                            return new Response(Code.Success, "Tạo chứng thư số thành công");
                        }
                        else
                        {
                            return new Response(Code.ServerError, "Lỗi tạo csr");
                        }
                    }
                    #endregion                    
                }
            }
            catch (Exception ex)
            {
                return new Response(Code.ServerError, ex.Message);
            }
        }

        /// <summary>
        /// Nén file cert để tải về
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="pathSource"></param>
        /// <returns></returns>
        public string ZipFileDataFromPath(string fileName, string pathSource)
        {
            try
            {
                #region Khởi tạo thư mục để lưu file cert
                string partitionPhysicalPath;
                string destinationPhysicalPath = "";
                partitionPhysicalPath = Path.Combine(Utils.GetConfig("StaticFiles:Folder"));
                // Get root path
                var rootPath = partitionPhysicalPath;
                // Append destinationPhysicalPath
                if (string.IsNullOrEmpty(destinationPhysicalPath))
                {
                    destinationPhysicalPath = ParameterCollection.Instance.GetValue("DefaultAlias");
                    destinationPhysicalPath = destinationPhysicalPath + "\\" +
                                              DateTime.Now.ToString("yyyy") + "\\" + DateTime.Now.ToString("MM") +
                                              "\\" +
                                              DateTime.Now.ToString("dd") + "\\";
                    bool isWindows = RuntimeInformation.IsOSPlatform(OSPlatform.Windows);
                    if (!isWindows)
                    {
                        destinationPhysicalPath = destinationPhysicalPath.Replace("\\", "/");
                    }
                }
                // Create folder path
                var sourcefolder = destinationPhysicalPath;
                var isExistDirectory = Directory.Exists(Path.Combine(rootPath, sourcefolder));
                if (!isExistDirectory) Directory.CreateDirectory(Path.Combine(rootPath, sourcefolder));
                var fullfolderPath = Path.Combine(rootPath, sourcefolder);
                #endregion

                string zipPath = @"" + fullfolderPath + "\\" + fileName + ".zip";
                if (File.Exists(zipPath))
                {
                    File.Delete(zipPath);
                }
                ZipFile.CreateFromDirectory(pathSource, zipPath, CompressionLevel.Fastest, false);

                return zipPath;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Tải về file cert
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="applicationId"></param>
        /// <param name="userAdminId"></param>
        /// <returns></returns>
        public ResponseObject<string> DowloadCert(Guid userId, Guid applicationId, Guid userAdminId)
        {
            try
            {
                string contentUrl = Utils.GetConfig("AppSettings:Host") + "StaticFiles/";
                using (var unitOfWork = new UnitOfWork())
                {
                    var cert = (from a in unitOfWork.GetRepository<CmsCert>().GetAll()
                                join b in unitOfWork.GetRepository<CmsCertUser>().GetAll()
                                on a.Id equals b.CertId
                                where b.UserId == userId
                                select a).FirstOrDefault();
                    if (cert == null)
                        return new ResponseObject<string>("", "Không tìm thấy thông tin chứng thư số", Code.NotFound);

                    #region Khởi tạo thư mục để lưu file cert
                    string partitionPhysicalPath;
                    string destinationPhysicalPath = "";
                    partitionPhysicalPath = Path.Combine(Utils.GetConfig("StaticFiles:Folder"));
                    // Get root path
                    var rootPath = partitionPhysicalPath;
                    Log.Debug("Upload root path : " + rootPath);
                    // Append destinationPhysicalPath
                    if (string.IsNullOrEmpty(destinationPhysicalPath))
                    {
                        destinationPhysicalPath = ParameterCollection.Instance.GetValue("DefaultAlias");
                        destinationPhysicalPath = destinationPhysicalPath + "\\" +
                                                  DateTime.Now.ToString("yyyy") + "\\" + DateTime.Now.ToString("MM") +
                                                  "\\" +
                                                  DateTime.Now.ToString("dd") + "\\";
                        bool isWindows = RuntimeInformation.IsOSPlatform(OSPlatform.Windows);
                        if (!isWindows)
                        {
                            destinationPhysicalPath = destinationPhysicalPath.Replace("\\", "/");
                        }
                    }
                    // Create folder path
                    var sourcefolder = destinationPhysicalPath;
                    var isExistDirectory = Directory.Exists(Path.Combine(rootPath, sourcefolder, cert.Alias));
                    if (!isExistDirectory) Directory.CreateDirectory(Path.Combine(rootPath, sourcefolder, cert.Alias));
                    var fullfolderPath = Path.Combine(rootPath, sourcefolder, cert.Alias);
                    #endregion

                    #region Lưu file cert from base64 rồi lấy link trả về
                    string filename = cert.Alias + ".cer";
                    var fileSavePath = Path.Combine(fullfolderPath, filename);
                    File.WriteAllBytes(@"" + fileSavePath + "", Convert.FromBase64String(cert.Cert));
                    var fileInfo = new FileInfo(fileSavePath);
                    string physicalPath = Path.Combine(sourcefolder, fileInfo.Name).Replace("\\", "/");
                    string fileUrl = new Uri(contentUrl + physicalPath).ToString();
                    #endregion
                    string zipFileUrl = ZipFileDataFromPath(cert.Alias, fullfolderPath);
                    string pathZipDownload = zipFileUrl.Replace("\\", "/").Replace(rootPath, contentUrl);
                    string zipDownload = new Uri(pathZipDownload).ToString();
                    return new ResponseObject<string>(zipDownload, cert.Alias + ".zip", Code.Success);
                }
            }
            catch (Exception ex)
            {
                return new ResponseObject<string>("", ex.Message, Code.ServerError);
            }
        }
        #endregion

        #region CRUD 

        public async Task<Response> CreateAsync(UserCreateModel model, Guid applicationId)
        {
            //Check trùng
            var userCheck = UserCollection.Instance.CheckName(model.UserName);
            if (userCheck) return new ResponseError(Code.BadRequest, "Tên tài khoản đã tồn tại");
            var request = AutoMapperUtils.AutoMap<UserCreateModel, IdmUser>(model);
            request.Id = Guid.NewGuid();
            request.Password = "admin@123";
            //Cập nhật pass
            if (!string.IsNullOrEmpty(request.Password))
            {
                request.PasswordSalt = Utils.PassowrdCreateSalt512();
                request.Password = Utils.PasswordGenerateHmac(request.Password, request.PasswordSalt);
            }
            request.LastActivityDate = DateTime.Now;
            request.Type = 1;//Loại tài khoản nội bộ
            var result = await _dbHandler.CreateAsync(request);
            if (result.Code == Code.Success)
            {
                #region Gửi email thông báo đến khách hàng

                string title = "[Vân Anh - Computer] - Đăng ký tài khoản thành công";


                StringBuilder htmlBody = new StringBuilder();
                htmlBody.Append("<html><body>");
                htmlBody.Append("<p>Xin chào <b>" + request.Name + "</b>,</p>");
                htmlBody.Append("<p>Bạn đã đăng ký tài khoản thành công.</p>");
                htmlBody.Append("<p><a href='"+frontendUrl+"'><span class='fas fa - laptop'></span> <p>Vân Anh<strong> Computer</strong> <span>Thế giới máy tính<span></p> </a> </p>");
                htmlBody.Append("</body></html>");

                _emailHandler.SendMailGoogle(new List<string>() { request.Email }, null, null, title, htmlBody.ToString());

                #endregion
                UserCollection.Instance.LoadToHashSet();
                // User In App
                await _rightMapUserHandler.AddRightMapUserAsync(request.Id, new List<Guid>(){
                    RightConstants.AccessAppId,
                    RightConstants.DefaultAppId
                }, applicationId, applicationId, request.Id);

                #region Realtive
                if (model.ListAddRightId != null)
                    await _rightMapUserHandler.AddRightMapUserAsync(request.Id, model.ListAddRightId, applicationId,
                         applicationId, request.Id);
                if (model.ListAddRoleId != null)
                    await _userMapRoleHandler.AddUserMapRoleAsync(model.ListAddRoleId, request.Id, applicationId,
                         applicationId, request.Id);
                if (model.ListAddDVId != null)
                    await _userMapDVHandler.AddUserMapDVAsync(model.ListAddDVId, request.Id, applicationId, applicationId,
                        request.Id);

                if (model.CertsList != null && model.CertsList.Count > 0)
                {
                    var createUserCert = UpdateUserCert(request.Id, model.CertsList);
                    if (createUserCert.Status != 1)
                        return new Response(Code.ServerError, "Không lưu được chứng thư số");
                }

                #endregion
            }

            return result;
        }

        public async Task<Response> UpdateAsync(Guid id, UserUpdateModel model, Guid applicationId)
        {
            //Check trùng
            var userCheck = UserCollection.Instance.CheckOtherUser(model.UserName, id);
            if (userCheck) return new ResponseError(Code.BadRequest, "Tên người dùng đã tồn tại");
            var request = AutoMapperUtils.AutoMap<UserUpdateModel, IdmUser>(model);
            //Cập nhật pass
            if (!string.IsNullOrWhiteSpace(request.Password))
            {
                request.PasswordSalt = Utils.PassowrdCreateSalt512();
                request.Password = Utils.PasswordGenerateHmac(request.Password, request.PasswordSalt);
            }
            else
            {
                //keep hash
                var currentUser = UserCollection.Instance.Collection.FirstOrDefault(u => u.Id == id);
                request.PasswordSalt = currentUser.PasswordSalt;
                request.Password = currentUser.Password;
            }
            request.Id = id;
            if (model.CertsList != null && model.CertsList.Count > 0)
                request.CertStatus = CustomerInfoCertStatus.GENERATED_CERT;
            var result = await _dbHandler.UpdateAsync(id, request);
            if (result.Code == Code.Success)
            {
                UserCollection.Instance.LoadToHashSet();

                #region Realtive

                if (model.ListAddRightId != null)
                    await _rightMapUserHandler.AddRightMapUserAsync(id, model.ListAddRightId, applicationId,
                        applicationId, id);
                if (model.ListAddRoleId != null)
                    await _userMapRoleHandler.AddUserMapRoleAsync(model.ListAddRoleId, id, applicationId, applicationId,
                        id);
                if (model.ListAddDVId != null)
                    await _userMapDVHandler.AddUserMapDVAsync(model.ListAddDVId, id, applicationId, applicationId,
                        id);
                if (model.ListDeleteRightId != null)
                    await _rightMapUserHandler.DeleteRightMapUserAsync(id, model.ListDeleteRightId, applicationId);
                if (model.ListDeleteRoleId != null)
                    await _userMapRoleHandler.DeleteUserMapRoleAsync(model.ListDeleteRoleId, id, applicationId);

                if (model.CertsList != null && model.CertsList.Count > 0)
                {
                    var updateUserCert = UpdateUserCert(model.Id, model.CertsList);
                    if (updateUserCert.Status != 1)
                        return new Response(Code.ServerError, "Không lưu được chứng thư số");
                }

                #endregion
            }

            return result;
        }

        public async Task<Response> DeleteAsync(Guid id)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    /// Delete user map role
                    unitOfWork.GetRepository<IdmUserMapRole>().DeleteRange(s => s.UserId == id);
                    /// Delete user map right
                    unitOfWork.GetRepository<IdmRightMapUser>().DeleteRange(s => s.UserId == id);
                    /// Delete user map org
                    unitOfWork.GetRepository<IdmUserMapOrg>().DeleteRange(s => s.UserId == id);
                    /// Delete user map cert
                    unitOfWork.GetRepository<CmsCertUser>().DeleteRange(s => s.UserId == id);

                    await unitOfWork.SaveAsync();
                }
                var result = await _dbHandler.DeleteAsync(id);
                UserCollection.Instance.LoadToHashSet();
                return result;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return new ResponseError(Code.ServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }

        public async Task<Response> ChangePasswordAsync(Guid id, string password, Guid applicationId, string currentPassword)
        {


            var currentUser = UserCollection.Instance.Collection.Where(s => s.Id == id).FirstOrDefault();
            if (currentUser != null)
            {
                // Validate mật khẩu 
                if (!string.IsNullOrEmpty(currentPassword))
                {
                    var validate = Authentication(currentUser.UserName, currentPassword, true);
                    if (validate.Code != Code.Success)
                    {
                        return new ResponseError(Code.BadRequest, "Thông tin mật khẩu không đúng");
                    }

                }
                currentUser.PasswordSalt = Utils.PassowrdCreateSalt512();
                currentUser.Password = Utils.PasswordGenerateHmac(password, currentUser.PasswordSalt);
            }
            var request = AutoMapperUtils.AutoMap<UserModel, IdmUser>(currentUser);
            request.Id = id;
            var result = await _dbHandler.UpdateAsync(id, request);
            if (result.Code == Code.Success)
            {
                UserCollection.Instance.LoadToHashSet();
            }

            return result;
        }

        public async Task<Response> DeleteRangeAsync(IList<Guid> listId)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    /// Delete user map role
                    unitOfWork.GetRepository<IdmUserMapRole>().DeleteRange(s => listId.Contains(s.UserId));
                    /// Delete user map right
                    unitOfWork.GetRepository<IdmRightMapUser>().DeleteRange(s => listId.Contains(s.UserId));

                    await unitOfWork.SaveAsync();
                }
                var result = await _dbHandler.DeleteRangeAsync(listId, "Id");
                UserCollection.Instance.LoadToHashSet();
                return result;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return new ResponseError(Code.ServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }

        }

        public Task<Response> FindAsync(Guid id)
        {
            return _dbHandler.FindAsync(id);
        }

        public Task<Response> LockUser(Guid id)
        {
            return PatchUserInfo(id, new UserPatchModel()
            {
                IsLocked = true
            });
        }

        public Task<Response> UnLockUser(Guid id)
        {
            return PatchUserInfo(id, new UserPatchModel()
            {
                IsLocked = false
            });
        }

        public async Task<Response> PatchUserInfo(Guid id, UserPatchModel patchModel)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {

                    var currentUser = unitOfWork.GetRepository<IdmUser>().Find(id);

                    if (currentUser == null)
                    {
                        return new ResponseError(Code.NotFound, "Không tìm thấy người dùng");
                    }
                    // Validate mật khẩu 
                    if (!string.IsNullOrEmpty(patchModel.Password))
                    {
                        var validate = Authentication(currentUser.UserName, patchModel.Password, true);
                        if (validate.Code != Code.Success)
                        {
                            return new ResponseError(Code.BadRequest, "Thông tin mật khẩu không đúng");
                        }

                    }
                    if (patchModel.IsLocked.HasValue)
                    {
                        currentUser.IsLocked = patchModel.IsLocked.Value;
                    }
                    if (patchModel.LastActivityDate.HasValue)
                    {
                        currentUser.LastActivityDate = patchModel.LastActivityDate.Value;
                    }

                    if (patchModel.Type.HasValue)
                    {
                        currentUser.Type = patchModel.Type.Value;
                    }
                    if (patchModel.PositionId.HasValue)
                    {
                        currentUser.PositionId = patchModel.PositionId.Value;
                        currentUser.PositionName = patchModel.PositionName;
                    }
                    if (!string.IsNullOrEmpty(patchModel.CompanyName))
                    {
                        currentUser.CompanyName = patchModel.CompanyName;
                    }
                    if (!string.IsNullOrEmpty(patchModel.SecurityQuestion))
                    {
                        currentUser.SecurityQuestion = patchModel.SecurityQuestion;
                    }
                    if (!string.IsNullOrEmpty(patchModel.SecurityQuestionAnswer))
                    {
                        currentUser.SecurityQuestionAnswer = patchModel.SecurityQuestionAnswer;
                    }
                    // ----------
                    if (!string.IsNullOrEmpty(patchModel.SignFullPath))
                    {
                        currentUser.SignFullPath = patchModel.SignFullPath;
                    }
                    // ----------
                    if (!string.IsNullOrEmpty(patchModel.SignFinalPath))
                    {
                        currentUser.SignFinalPath = patchModel.SignFinalPath;
                    }
                    // ----------

                    if (!string.IsNullOrEmpty(patchModel.SignShortPath))
                    {
                        currentUser.SignShortPath = patchModel.SignShortPath;
                    }
                    // ----------
                    if (!string.IsNullOrEmpty(patchModel.LogoPath))
                    {
                        currentUser.LogoPath = patchModel.LogoPath;
                    }
                    if (patchModel.SignTypeFull.HasValue)
                    {
                        currentUser.SignTypeFull = patchModel.SignTypeFull.Value;
                    }
                    if (patchModel.SignTypeShort.HasValue)
                    {
                        currentUser.SignTypeShort = patchModel.SignTypeShort.Value;
                    }
                    if (patchModel.SignDisplay.HasValue)
                    {
                        currentUser.SignDisplay = patchModel.SignDisplay.Value;
                    }

                    await unitOfWork.SaveAsync();
                }
                UserCollection.Instance.LoadToHashSet();
                return new ResponseUpdate(id);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return new ResponseError(Code.ServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }

        #endregion

        #region Update user's certs

        /// <summary>
        /// Thêm mới cấp chứng thư số cho người dùng
        /// </summary>
        /// <param name="userId">id của người dùng</param>
        /// <param name="model">danh sách chứng thư số được cấp</param>
        /// <returns></returns>
        private ResponseV2 UpdateUserCert(Guid userId, List<UserCertificateModel> model)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    var removes = (from cu in unitOfWork.GetRepository<CmsCertUser>().GetAll()
                                   where cu.UserId == userId
                                   select cu).ToList();

                    unitOfWork.GetRepository<CmsCertUser>().DeleteRange(removes);

                    foreach (var userCertificateModel in model)
                    {
                        var created = new CmsCertUser();
                        created.Id = new Guid();
                        created.UserId = userId;
                        created.CertId = userCertificateModel.CertId;
                        created.Key = Utils.MD5Hash("12345a@");

                        unitOfWork.GetRepository<CmsCertUser>().Add(created);
                    }


                    var result = unitOfWork.Save();
                    if (result != 0)
                    {
                        return new ResponseV2(1, "Success");
                    }
                    else
                    {
                        return new ResponseV2(0, "Something wrong");
                    }
                }
            }
            catch (Exception ex)
            {
                return new ResponseV2(-1, "Lỗi" + ex.Message);
            }
        }
        #endregion

    }
}