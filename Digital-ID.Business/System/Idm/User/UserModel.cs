﻿using DigitalID.Data;
using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace DigitalID.Business
{
    public class BaseUserModel
    {
        public Guid Id { get; set; }
        public string UserName { get; set; }
        public string Name { get; set; }
        public bool IsLocked { get; set; }
        public bool is_admin { get; set; }
        public string QrCodeString { get; set; }
        public Guid ApplicationId { get; set; }
        public List<string> ListRole { get; set; }
        public List<string> ListRight { get; set; }
    }
    public class UserModel : BaseUserModel
    {
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string AvatarUrl { get; set; }
        public int? Type { get; set; }
        public string Password { get; set; }
        public string PasswordSalt { get; set; }
        public DateTime? Birthdate { get; set; }
        public DateTime LastActivityDate { get; set; } = DateTime.Now;
        public bool IsAdmin { get; set; }
        public Guid? PositionId { get; set; }
        public string PositionName { get; set; }
        public string CompanyName { get; set; }
        public string SecurityQuestion { get; set; }
        public string SecurityQuestionAnswer { get; set; }

        public int SignTypeFull { get; set; } = 0;
        public string SignFullPath { get; set; }
        public string SignFinalPath { get; set; }
        public int SignTypeShort { get; set; } = 0;
        public string SignShortPath { get; set; }
        public string LogoPath { get; set; }
        public int SignDisplay { get; set; } = 0;
        public int SignBy { get; set; }
        public string Ca { get; set; }
        //Trạng thái sử dụng cert: 0 chưa tạo khóa, 1: đã tạo khóa chưa tạo cert, 2: đã tạo cert
        public int CertStatus { get; set; }
    }
    public class UserDetailModel : UserModel
    {
        public IList<BaseRightModelOfUser> ListRight { get; set; }
        public IList<BaseRoleModel> ListRole { get; set; }  
    }
    public class UserQueryModel : PaginationRequest
    {
        public string UserName { get; set; }
        public int Type { get; set; } = 1;
    }

    #region QR Code
    public class QrCodeInputModel
    {
        public string strText { get; set; }
        public Guid userId { get; set; }
    }
    #endregion

    public class CertCreatModel
    {
        public Guid userId { get; set; }
        public string slotLabel { get; set; } = "slot01";
        public string key { get; set; } = "12345a@";
        public string userPin { get; set; } = "12345";
        public string supplier { get; set; } = "TrustCA-SHA2";
        public string endEntityProfileName { get; set; }
        public string certificateProfileName { get; set; }
        public string encoding { get; set; } = "PEM";
        public string certType { get; set; } = "1";
        public string alias { get; set; }
        public string keyAlgorithm { get; set; }
        public string hashAlgorithm { get; set; }
        public string keyLenght { get; set; }
        public string subjectDN_C { get; set; }
        public string subjectDN_CMT_MST { get; set; }
        public string subjectDN_CN { get; set; }
        public string subjectDN_O { get; set; }
        public string subjectDN_ST { get; set; }
    }
    public class UserCreateModel
    {
        /// <summary>
        /// preset
        /// </summary>
        public Guid Id { get; set; }
        public string UserName { get; set; }
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string AvatarUrl { get; set; }
        public int? Type { get; set; }
        public string Password { get; set; }
        public bool IsLocked { get; set; }
        public DateTime? Birthdate { get; set; }
        //Plus
        /// <summary>
        /// Danh sách quyền thêm
        /// </summary>
        public IList<Guid> ListAddRightId { get; set; }
        /// <summary>
        /// Danh sách nhóm người dùng thêm
        /// </summary>

        public IList<Guid> ListAddRoleId { get; set; }
        public IList<Guid> ListAddDVId { get; set; }

        public Guid? PositionId { get; set; }
        public string PositionName { get; set; }
        public string CompanyName { get; set; }
        public string SecurityQuestion { get; set; }
        public string SecurityQuestionAnswer { get; set; }

        public int SignTypeFull { get; set; } = 0;
        public string SignFullPath { get; set; }
        public int SignTypeShort { get; set; } = 0;
        public string SignShortPath { get; set; }
        public string LogoPath { get; set; }
        public int SignDisplay { get; set; } = 0;
        public int SignBy { get; set; }
        //public string Ca { get; set; }
        public List<UserCertificateModel> CertsList { get; set; }
    }
    public class UserUpdateModel
    {
        public Guid Id { get; set; }
        public string UserName { get; set; }
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string AvatarUrl { get; set; }
        public int? Type { get; set; }
        public bool IsLocked { get; set; }
        public string Password { get; set; }
        public DateTime? Birthdate { get; set; }
        //Plus
        /// <summary>
        /// Danh sách quyền thêm
        /// </summary>
        public IList<Guid> ListAddRightId { get; set; }
        /// <summary>
        /// Danh sách nhóm người dùng thêm
        /// </summary>
        public IList<Guid> ListAddRoleId { get; set; }
        public IList<Guid> ListAddDVId { get; set; }
        //Plus
        /// <summary>
        /// Danh sách quyền xóa
        /// </summary>
        public IList<Guid> ListDeleteRightId { get; set; }
        /// <summary>
        /// Danh sách nhóm người dùng xóa
        /// </summary>
        public IList<Guid> ListDeleteRoleId { get; set; }

        public Guid? PositionId { get; set; }
        public string PositionName { get; set; }
        public string CompanyName { get; set; }
        public string SecurityQuestion { get; set; }
        public string SecurityQuestionAnswer { get; set; }

        public int SignTypeFull { get; set; } = 0;
        public string SignFullPath { get; set; }
        public int SignTypeShort { get; set; } = 0;
        public string SignShortPath { get; set; }
        public string LogoPath { get; set; }
        public int SignDisplay { get; set; } = 0;
        public int SignBy { get; set; }
        //public string Ca { get; set; }
        public List<UserCertificateModel> CertsList { get; set; }
    }
    public class UserPatchModel
    {
        public string UserName { get; set; }
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string AvatarUrl { get; set; }
        public int? Type { get; set; }
        public bool? IsLocked { get; set; }
        public string Password { get; set; }
        public DateTime? Birthdate { get; set; }
        public DateTime? LastActivityDate { get; set; }

        public Guid? PositionId { get; set; }
        public string PositionName { get; set; }
        public string CompanyName { get; set; }
        public string SecurityQuestion { get; set; }
        public string SecurityQuestionAnswer { get; set; }



        public int? SignTypeFull { get; set; } = 0;
        public string SignFullPath { get; set; }
        public string SignFinalPath { get; set; }
        public int? SignTypeShort { get; set; } = 0;
        public string SignShortPath { get; set; }
        public string LogoPath { get; set; }
        public int? SignDisplay { get; set; } = 0;
        public int SignBy { get; set; }

    }

    public class CertificateProfileResquest
    {
        [JsonProperty("operator")]
        public string Operator { get; set; }
        [JsonProperty("nameNid")]
        public NameFilter NameNid { get; set; }
    }

    public class NameFilter {
        public string name { get; set; }
    }

    public class CertificateProfileRespone
    {
        public string errCode { get; set; }
        public string errMsg { get; set; }
        public List<CertificateProfile> data { get; set; }
    }

    public class CertificateProfile
    {
        public double id { get; set; }
        public string name { get; set; }
    }

    public class ChallengesInputModel
    { 
        public string Username { get; set; }
        public string ChallengeString { get; set; }
        public Guid ApplicationId { get; set; }
    }

    public class ChallengesOutputModel
    {
        public string Signature { get; set; }
        public string SecToken { get; set; }
    }
}

