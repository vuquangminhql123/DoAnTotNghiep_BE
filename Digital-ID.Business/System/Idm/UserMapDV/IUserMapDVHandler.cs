﻿using DigitalID.Data;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DigitalID.Business
{
    public interface IUserMapDVHandler
    {
        /// <summary>
        /// Cập nhật đơn vị cho người dùng
        /// </summary>
        /// <param name="listDVId"></param>
        /// <param name="userId"></param>
        /// <param name="applicationId"></param>
        /// <param name="appId"></param>
        /// <param name="actorId"></param>
        /// <returns></returns>
        Task<Response> AddUserMapDVAsync(IList<Guid> listDVId, Guid userId, Guid applicationId, Guid? appId, Guid? actorId);
        /// <summary>
        /// Lấy danh sách người dùng nằm trong nhóm
        /// </summary>
        /// <param name="roleId"></param>
        /// <param name="applicationId"></param>
        /// <returns></returns>
        Task<Response> GetUserMapDVAsync(Guid roleId, Guid applicationId);

        /// <summary>
        /// Lấy danh sach các nhóm của người dùng
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="applicationId"></param>
        /// <returns></returns>
        Task<Response> GetDVMapUserAsync(Guid userId, Guid applicationId);

        /// <summary>
        /// Kiểm tra người dùng thuộc nhóm
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="roleId"></param>
        /// <param name="applicationId"></param>
        /// <returns></returns>
        Task<Response> CheckDVMapUserAsync(Guid userId, Guid roleId, Guid applicationId);

        Task<Response> CheckDVNameMapUserAsync(Guid userId, string roleName, Guid applicationId);
        
        Task<Response> GetUserMapDVNameAsync(string roleName, Guid applicationId);
    }
}
