﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DigitalID.Data;
using Microsoft.EntityFrameworkCore;
using Serilog;

namespace DigitalID.Business
{
    public class UserMapDVHandler : IUserMapDVHandler
    {
        public async Task<Response> AddUserMapDVAsync(IList<Guid> listDVId, Guid userId, Guid applicationId,
            Guid? appId, Guid? actorId)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    var deleteData = unitOfWork.GetRepository<IdmUserMapOrg>().GetMany(x => x.UserId == userId);
                    unitOfWork.GetRepository<IdmUserMapOrg>().DeleteRange(deleteData);
                    List<IdmUserMapOrg> listRmuToAdd = new List<IdmUserMapOrg>();
                    foreach (var orgId in listDVId)
                    {
                        listRmuToAdd.Add(new IdmUserMapOrg
                        {
                            UserId = userId,
                            OrganizationId = orgId,
                            ApplicationId = applicationId
                        }.InitCreate(applicationId, actorId));
                    }
                    if (listRmuToAdd.Any()) unitOfWork.GetRepository<IdmUserMapOrg>().AddRange(listRmuToAdd);
                    if (await unitOfWork.SaveAsync() > 0)
                        return new Response(Code.Success, "Gán người dùng vào đơn vị thành công");
                    Log.Error("The sql statement is not executed!");
                    return new ResponseError(Code.BadRequest, "Câu lệnh sql không thể thực thi");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return new ResponseError(Code.ServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }

        public async Task<Response> GetUserMapDVAsync(Guid roleId, Guid applicationId)

        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    var listUserId = await (from rmu in unitOfWork.GetRepository<IdmUserMapOrg>().GetAll()
                        where rmu.OrganizationId == roleId && rmu.ApplicationId == applicationId
                        select rmu.UserId).ToListAsync();
                    var resultData = UserCollection.Instance.GetModel(s => listUserId.Contains(s.Id)).ToList();
                    return new ResponseList<BaseUserModel>(resultData);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return new ResponseError(Code.ServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }

           public async Task<Response> GetUserMapDVNameAsync(string roleName, Guid applicationId)

        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    var role = await unitOfWork.GetRepository<CmsOrganization>().FindAsync(x => x.Name == roleName);
                    if(role == null){
                           return new ResponseList<BaseUserModel>(new List<BaseUserModel>());
                    }
                    var roleId = role.Id;

                    var listUserId = await (from rmu in unitOfWork.GetRepository<IdmUserMapOrg>().GetAll()
                        where rmu.OrganizationId == roleId && rmu.ApplicationId == applicationId
                        select rmu.UserId).ToListAsync();
                    var resultData = UserCollection.Instance.GetModel(s => listUserId.Contains(s.Id)).ToList();
                    return new ResponseList<BaseUserModel>(resultData);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return new ResponseError(Code.ServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }

        public async Task<Response> GetDVMapUserAsync(Guid userId, Guid applicationId)

        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    var datas = from rmu in unitOfWork.GetRepository<IdmUserMapOrg>().GetAll()
                        join r in unitOfWork.GetRepository<CmsOrganization>().GetAll()
                            on rmu.OrganizationId equals r.Id
                        where rmu.UserId == userId && rmu.ApplicationId == applicationId
                        select r;
                    var queryList = await datas.ToListAsync();
                    var resultData = AutoMapperUtils.AutoMap<CmsOrganization, BaseOrganizationModel>(queryList);
                    return new ResponseList<BaseOrganizationModel>(resultData);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return new ResponseError(Code.ServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }

        

        public async Task<Response> CheckDVMapUserAsync(Guid userId, Guid roleId, Guid applicationId)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    var currentMap = await unitOfWork.GetRepository<IdmUserMapOrg>().FindAsync(s =>
                        s.UserId == userId && s.OrganizationId == roleId && s.ApplicationId == applicationId);
                    if (currentMap != null)
                        return new ResponseObject<bool>(true);
                    return new ResponseObject<bool>(false);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return new ResponseError(Code.ServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }

        public async Task<Response> CheckDVNameMapUserAsync(Guid userId, string roleName, Guid applicationId)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    var role = await unitOfWork.GetRepository<CmsOrganization>().FindAsync(x => x.Name ==roleName);
                    if(role == null){
                          return new ResponseObject<bool>(false);
                    }
                    var roleId = role.Id;
                    var currentMap = await unitOfWork.GetRepository<IdmUserMapOrg>().FindAsync(s =>
                        s.UserId == userId && s.OrganizationId == roleId && s.ApplicationId == applicationId);
                    if (currentMap != null)
                        return new ResponseObject<bool>(true);
                    return new ResponseObject<bool>(false);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return new ResponseError(Code.ServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }
    }
}