﻿using DigitalID.Data;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DigitalID.Business
{
    public interface IUserMapRoleHandler
    {
        /// <summary>
        /// Xóa 1 người dùng trong nhóm
        /// </summary>
        /// <param name="roleId"></param>
        /// <param name="userId"></param>
        /// <param name="applicationId"></param>
        /// <returns></returns>
        Task<Response> DeleteUserMapRoleAsync(Guid roleId, Guid userId, Guid applicationId);

        /// <summary>
        /// Xóa 1 danh sách người dùng trong nhóm
        /// </summary>
        /// <param name="roleId"></param>
        /// <param name="listUserId"></param>
        /// <param name="applicationId"></param>
        /// <returns></returns>
        Task<Response> DeleteUserMapRoleAsync(Guid roleId, IList<Guid> listUserId, Guid applicationId);

        /// <summary>
        /// Xóa 1 danh sách nhóm của người dùng
        /// </summary>
        /// <param name="listRoleId"></param>
        /// <param name="userId"></param>
        /// <param name="applicationId"></param>
        /// <returns></returns>
        Task<Response> DeleteUserMapRoleAsync(IList<Guid> listRoleId, Guid userId, Guid applicationId);

        /// <summary>
        /// Thêm 1 người dùng trong nhóm
        /// </summary>
        /// <param name="roleId"></param>
        /// <param name="userId"></param>
        /// <param name="applicationId"></param>
        /// <param name="app"></param>
        /// <param name="actor"></param>
        /// <returns></returns>
        Task<Response> AddUserMapRoleAsync(Guid roleId, Guid userId, Guid applicationId, Guid? appId, Guid? actorId);

        /// <summary>
        /// Thêm 1 danh sách người dùng trong nhóm
        /// </summary>
        /// <param name="roleId"></param>
        /// <param name="listUserId"></param>
        /// <param name="applicationId"></param>
        /// <param name="app"></param>
        /// <param name="actor"></param>
        /// <returns></returns>
        Task<Response> AddUserMapRoleAsync(Guid roleId, IList<Guid> listUserId, Guid applicationId, Guid? appId, Guid? actorId);

        /// <summary>
        /// Thêm một danh sách nhóm cho người dùng
        /// </summary>
        /// <param name="listRoleId"></param>
        /// <param name="userId"></param>
        /// <param name="applicationId"></param>
        /// <param name="app"></param>
        /// <param name="actor"></param>
        /// <returns></returns>
        Task<Response> AddUserMapRoleAsync(IList<Guid> listRoleId, Guid userId, Guid applicationId, Guid? appId, Guid? actorId);

        /// <summary>
        /// Lấy danh sách người dùng nằm trong nhóm
        /// </summary>
        /// <param name="roleId"></param>
        /// <param name="applicationId"></param>
        /// <returns></returns>
        Task<Response> GetUserMapRoleAsync(Guid roleId, Guid applicationId);

        /// <summary>
        /// Lấy danh sach các nhóm của người dùng
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="applicationId"></param>
        /// <returns></returns>
        Task<Response> GetRoleMapUserAsync(Guid userId, Guid applicationId);

        /// <summary>
        /// Kiểm tra người dùng thuộc nhóm
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="roleId"></param>
        /// <param name="applicationId"></param>
        /// <returns></returns>
        Task<Response> CheckRoleMapUserAsync(Guid userId, Guid roleId, Guid applicationId);

        Task<Response> CheckRoleNameMapUserAsync(Guid userId, string roleName, Guid applicationId);
        
        Task<Response> GetUserMapRoleNameAsync(string roleName, Guid applicationId);
    }
}
