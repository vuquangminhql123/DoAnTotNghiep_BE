﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DigitalID.Data;
using Microsoft.EntityFrameworkCore;
using Serilog;

namespace DigitalID.Business
{
    public class UserMapRoleHandler : IUserMapRoleHandler
    {
        public async Task<Response> DeleteUserMapRoleAsync(Guid roleId, Guid userId, Guid applicationId)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    #region Check

                    var currentMap = await unitOfWork.GetRepository<IdmUserMapRole>().FindAsync(s =>
                        s.RoleId == roleId && s.UserId == userId && s.ApplicationId == applicationId);
                    if (currentMap == null)
                        return new ResponseError(Code.BadRequest, "Người dùng không thuộc nhóm người dùng");

                    #endregion

                    //Delete Mapp
                    unitOfWork.GetRepository<IdmUserMapRole>().Delete(currentMap);

                    //Update child
                    var roleIdString = roleId.ToString();
                    var currentRmu = await (from rmu in unitOfWork.GetRepository<IdmRightMapUser>().GetAll()
                        where rmu.UserId == userId && rmu.InheritedFromRoles.Contains(roleIdString) &&
                              rmu.ApplicationId == applicationId
                        select rmu).ToListAsync();
                    var listRmuToDelete = new List<IdmRightMapUser>();
                    foreach (var mapItem in currentRmu)
                    {
                        //Nếu InheritedFromRoles chỉ chứa mình quyền đó =>> xóa
                        //Nếu không khai trừ role ra khỏi InheritedFromRoles
                        var listRole = IdmHelper.LoadRolesInherited(mapItem.InheritedFromRoles);
                        if (listRole.Count > 1)
                        {
                            if (listRole.Contains(IdmHelper.MakeIndependentPermission())
                            ) //nếu có 2 inherist với 1 cái là self 
                                mapItem.Inherited = false;
                            mapItem.InheritedFromRoles =
                                IdmHelper.RemoveRolesInherited(mapItem.InheritedFromRoles, roleId);
                            unitOfWork.GetRepository<IdmRightMapUser>().Update(mapItem);
                        }
                        else
                        {
                            listRmuToDelete.Add(mapItem);
                        }
                    }

                    if (listRmuToDelete.Any()) unitOfWork.GetRepository<IdmRightMapUser>().DeleteRange(listRmuToDelete);


                    if (await unitOfWork.SaveAsync() > 0)
                        return new Response(Code.Success, "Loại người dùng ra khỏi nhóm thành công");
                    Log.Error("The sql statement is not executed!");
                    return new ResponseError(Code.BadRequest, "Câu lệnh sql không thể thực thi");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return new ResponseError(Code.ServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }

        public async Task<Response> DeleteUserMapRoleAsync(Guid roleId, IList<Guid> listUserId, Guid applicationId)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    #region Check

                    var listCurrenMap = await unitOfWork.GetRepository<IdmUserMapRole>().GetListAsync(s =>
                        s.RoleId == roleId && listUserId.Contains(s.UserId) && s.ApplicationId == applicationId);
                    if (listCurrenMap.Count == 0)
                        return new ResponseError(Code.BadRequest, "Không người dùng nào thuộc nhóm người dùng");

                    #endregion

                    //Delete Mapp
                    unitOfWork.GetRepository<IdmUserMapRole>().DeleteRange(listCurrenMap);
                    var roleIdString = roleId.ToString();
                    //Update child

                    var currentRmu = await (from rmu in unitOfWork.GetRepository<IdmRightMapUser>().GetAll()
                        where listUserId.Contains(rmu.UserId) && rmu.InheritedFromRoles.Contains(roleIdString) &&
                              rmu.ApplicationId == applicationId
                        select rmu).ToListAsync();


                    var listRmuToDelete = new List<IdmRightMapUser>();
                    foreach (var mapItem in currentRmu)
                    {
                        //Nếu InheritedFromRoles chỉ chứa mình quyền đó =>> xóa
                        //Nếu không khai trừ role ra khỏi InheritedFromRoles
                        var listRole = IdmHelper.LoadRolesInherited(mapItem.InheritedFromRoles);
                        if (listRole.Count > 1)
                        {
                            if (listRole.Contains(IdmHelper.MakeIndependentPermission())
                            ) //nếu có 2 inherist với 1 cái là self 
                                mapItem.Inherited = false;
                            mapItem.InheritedFromRoles =
                                IdmHelper.RemoveRolesInherited(mapItem.InheritedFromRoles, roleId);
                            unitOfWork.GetRepository<IdmRightMapUser>().Update(mapItem);
                        }
                        else
                        {
                            listRmuToDelete.Add(mapItem);
                        }
                    }

                    if (listRmuToDelete.Any()) unitOfWork.GetRepository<IdmRightMapUser>().DeleteRange(listRmuToDelete);


                    if (await unitOfWork.SaveAsync() > 0)
                        return new Response(Code.Success, "Loại danh sách người dùng ra khỏi nhóm thành công");
                    Log.Error("The sql statement is not executed!");
                    return new ResponseError(Code.BadRequest, "Câu lệnh sql không thể thực thi");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return new ResponseError(Code.ServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }

        public async Task<Response> DeleteUserMapRoleAsync(IList<Guid> listRoleId, Guid userId, Guid applicationId)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    #region Check

                    var listCurrenMap = await unitOfWork.GetRepository<IdmUserMapRole>().GetListAsync(s =>
                        listRoleId.Contains(s.RoleId) && userId == s.UserId && s.ApplicationId == applicationId);
                    if (listCurrenMap.Count == 0)
                        return new ResponseError(Code.BadRequest, "Người dùng không thuộc nhóm người dùng nào");

                    #endregion

                    //Delete Mapp
                    unitOfWork.GetRepository<IdmUserMapRole>().DeleteRange(listCurrenMap);
                    var listRmuToDelete = new List<IdmRightMapUser>();
                    foreach (var roleId in listRoleId)
                    {
                        //Update child

                        var roleIdString = roleId.ToString();
                        var currentRmu = await (from rmu in unitOfWork.GetRepository<IdmRightMapUser>().GetAll()
                            where rmu.UserId == userId && rmu.InheritedFromRoles.Contains(roleIdString) &&
                                  rmu.ApplicationId == applicationId
                            select rmu).ToListAsync();
                        foreach (var mapItem in currentRmu)
                        {
                            //Nếu InheritedFromRoles chỉ chứa mình quyền đó =>> xóa
                            //Nếu không khai trừ role ra khỏi InheritedFromRoles
                            var listRole = IdmHelper.LoadRolesInherited(mapItem.InheritedFromRoles);
                            if (listRole.Count > 1)
                            {
                                if (listRole.Contains(IdmHelper.MakeIndependentPermission())
                                ) //nếu có 2 inherist với 1 cái là self 
                                    mapItem.Inherited = false;
                                mapItem.InheritedFromRoles =
                                    IdmHelper.RemoveRolesInherited(mapItem.InheritedFromRoles, roleId);
                                unitOfWork.GetRepository<IdmRightMapUser>().Update(mapItem);
                            }
                            else
                            {
                                listRmuToDelete.Add(mapItem);
                            }
                        }
                    }

                    if (listRmuToDelete.Any()) unitOfWork.GetRepository<IdmRightMapUser>().DeleteRange(listRmuToDelete);


                    if (await unitOfWork.SaveAsync() > 0)
                        return new Response(Code.Success, "Loại người dùng ra khỏi danh sách nhóm thành công");
                    Log.Error("The sql statement is not executed!");
                    return new ResponseError(Code.BadRequest, "Câu lệnh sql không thể thực thi");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return new ResponseError(Code.ServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }

        public async Task<Response> AddUserMapRoleAsync(Guid roleId, Guid userId, Guid applicationId, Guid? app,
            Guid? actor)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    #region Check

                    var currentMap = await unitOfWork.GetRepository<IdmUserMapRole>().FindAsync(s =>
                        s.RoleId == roleId && s.UserId == userId && s.ApplicationId == applicationId);
                    if (currentMap != null)
                        return new ResponseError(Code.BadRequest, "Người dùng đã tồn tại trong nhóm");

                    var roleModel = RoleCollection.Instance.GetModel(roleId);
                    if (roleModel == null)
                        return new ResponseError(Code.BadRequest, "Nhóm người dùng không tồn tại");
                    var userModel = UserCollection.Instance.GetModel(userId);
                    if (userModel == null)
                        return new ResponseError(Code.BadRequest, "Người dùng không tồn tại");

                    #endregion

                    unitOfWork.GetRepository<IdmUserMapRole>().Add(new IdmUserMapRole
                    {
                        UserId = userId,
                        RoleId = roleId,
                        ApplicationId = applicationId
                    }.InitCreate(applicationId, actor));
                    /*Thêm mới User mapp user của user kế thừa*/
                    var childRights = await unitOfWork.GetRepository<IdmRightMapRole>()
                        .Get(s => s.RoleId == roleId && s.ApplicationId == applicationId).Select(s => s.RightId)
                        .ToListAsync();
                    var listCurrentRmu = await unitOfWork.GetRepository<IdmRightMapUser>().GetListAsync(s =>
                        s.UserId == userId && childRights.Contains(s.RightId) && s.ApplicationId == applicationId);
                    foreach (var currentRmu in listCurrentRmu)
                    {
                        currentRmu.InheritedFromRoles =
                            IdmHelper.AddRolesInherited(currentRmu.InheritedFromRoles, roleId);
                        currentRmu.Inherited = true;
                        currentRmu.Enable = true;
                        unitOfWork.GetRepository<IdmRightMapUser>().Update(currentRmu.InitUpdate(applicationId, actor));
                    }

                    var listRmuToAdd = new List<IdmRightMapUser>();
                    foreach (var rightId in childRights)
                        if (listCurrentRmu.All(s => s.RightId != rightId))
                            listRmuToAdd.Add(new IdmRightMapUser
                            {
                                Enable = true,
                                InheritedFromRoles = IdmHelper.GenRolesInherited(roleId),
                                Inherited = true,
                                RightId = rightId,
                                UserId = userId,
                                ApplicationId = applicationId
                            }.InitCreate(applicationId, actor));

                    unitOfWork.GetRepository<IdmRightMapUser>().AddRange(listRmuToAdd);
                    if (await unitOfWork.SaveAsync() > 0)
                        return new Response(Code.Success, "Gán người dùng vào nhóm thành công");
                    Log.Error("The sql statement is not executed!");
                    return new ResponseError(Code.BadRequest, "Câu lệnh sql không thể thực thi");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return new ResponseError(Code.ServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }

        public async Task<Response> AddUserMapRoleAsync(Guid roleId, IList<Guid> listUserId, Guid applicationId,
            Guid? appId, Guid? actorId)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    #region Check

                    var listCurrentMap = await unitOfWork.GetRepository<IdmUserMapRole>().GetListAsync(s =>
                        s.RoleId == roleId && listUserId.Contains(s.UserId) && s.ApplicationId == applicationId);
                    if (listCurrentMap.Count == listUserId.Count)
                        return new ResponseError(Code.BadRequest, "Tất cả người dùng đã tồn tại trong nhóm !");


                    var roleModel = RoleCollection.Instance.GetModel(roleId);
                    if (roleModel == null)
                        return new ResponseError(Code.BadRequest, "Nhóm người dùng không tồn tại");
                    var listUserModel = UserCollection.Instance.GetModel(s => listUserId.Contains(s.Id));
                    if (listUserModel == null || listUserModel.Count == 0)
                        return new ResponseError(Code.BadRequest, "Không người dùng nào tồn tại");

                    #endregion

                    var listUmrToAdd = new List<IdmUserMapRole>();
                    foreach (var userModel in listUserModel)
                        listUmrToAdd.Add(new IdmUserMapRole
                        {
                            UserId = userModel.Id,
                            RoleId = roleId,
                            ApplicationId = applicationId
                        }.InitCreate(applicationId, actorId));
                    if (listUmrToAdd.Any()) unitOfWork.GetRepository<IdmUserMapRole>().AddRange(listUmrToAdd);
                    /*Thêm mới User mapp user của user kế thừa*/
                    var childRights = await unitOfWork.GetRepository<IdmRightMapRole>()
                        .Get(s => s.RoleId == roleId && s.ApplicationId == applicationId).Select(s => s.RightId)
                        .ToListAsync();
                    var listCurrentRmu = await unitOfWork.GetRepository<IdmRightMapUser>().GetListAsync(s =>
                        listUserId.Contains(s.UserId) && childRights.Contains(s.RightId) &&
                        s.ApplicationId == applicationId);
                    foreach (var currentRmu in listCurrentRmu)
                    {
                        currentRmu.InheritedFromRoles =
                            IdmHelper.AddRolesInherited(currentRmu.InheritedFromRoles, roleId);
                        currentRmu.Inherited = true;
                        currentRmu.Enable = true;
                        unitOfWork.GetRepository<IdmRightMapUser>().Update(currentRmu.InitUpdate(applicationId, actorId));
                    }

                    var listRmuToAdd = new List<IdmRightMapUser>();

                    foreach (var rightId in childRights)
                        if (listCurrentRmu.All(s => s.RightId != rightId))
                            foreach (var userId in listUserId)
                                listRmuToAdd.Add(new IdmRightMapUser
                                {
                                    Enable = true,
                                    InheritedFromRoles = IdmHelper.GenRolesInherited(roleId),
                                    Inherited = true,
                                    RightId = rightId,
                                    UserId = userId,
                                    ApplicationId = applicationId
                                }.InitCreate(applicationId, actorId));
                    if (listRmuToAdd.Any()) unitOfWork.GetRepository<IdmRightMapUser>().AddRange(listRmuToAdd);
                    if (await unitOfWork.SaveAsync() > 0)
                        return new Response(Code.Success, "Thêm danh sách người dùng vào nhóm thành công");
                    Log.Error("The sql statement is not executed!");
                    return new ResponseError(Code.BadRequest, "Câu lệnh sql không thể thực thi");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return new ResponseError(Code.ServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }

        public async Task<Response> AddUserMapRoleAsync(IList<Guid> listRoleId, Guid userId, Guid applicationId,
            Guid? appId, Guid? actorId)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    #region Check

                    var listCurrentMap = await unitOfWork.GetRepository<IdmUserMapRole>().GetListAsync(s =>
                        s.UserId == userId && listRoleId.Contains(s.RoleId) && s.ApplicationId == applicationId);
                    if (listCurrentMap.Count == listRoleId.Count)
                        return new ResponseError(Code.BadRequest, "Người dùng đã tồn tại trong tất cả nhóm !");


                    var userModel = UserCollection.Instance.GetModel(userId);
                    if (userModel == null)
                        return new ResponseError(Code.BadRequest, "Người dùng không tồn tại");
                    var listRoleModel = RoleCollection.Instance.Collection.Where(s => listRoleId.Contains(s.Id))
                        .ToList();
                    if (listRoleModel.Count == 0)
                        return new ResponseError(Code.BadRequest, "Không nhóm người dùng nào tồn tại");

                    #endregion

                    var listUmrToAdd = new List<IdmUserMapRole>();
                    foreach (var roleModel in listRoleModel)
                        listUmrToAdd.Add(new IdmUserMapRole
                        {
                            UserId = userId,
                            RoleId = roleModel.Id,
                            ApplicationId = applicationId
                        }.InitCreate(applicationId, actorId));
                    if (listUmrToAdd.Any()) unitOfWork.GetRepository<IdmUserMapRole>().AddRange(listUmrToAdd);
                    /*Thêm mới User mapp user của user kế thừa*/
                    var listRmuToAdd = new List<IdmRightMapUser>();
                    foreach (var roleId in listRoleId)
                    {
                        var childRights = await unitOfWork.GetRepository<IdmRightMapRole>()
                            .Get(s => s.RoleId == roleId && s.ApplicationId == applicationId).Select(s => s.RightId)
                            .ToListAsync();
                        var listCurrentRmu = await unitOfWork.GetRepository<IdmRightMapUser>().GetListAsync(s =>
                            s.UserId == userId && childRights.Contains(s.RightId) && s.ApplicationId == applicationId);
                        foreach (var currentRmu in listCurrentRmu)
                        {
                            currentRmu.InheritedFromRoles =
                                IdmHelper.AddRolesInherited(currentRmu.InheritedFromRoles, roleId);
                            currentRmu.Inherited = true;
                            currentRmu.Enable = true;
                            unitOfWork.GetRepository<IdmRightMapUser>()
                                .Update(currentRmu.InitUpdate(applicationId, actorId));
                        }

                        foreach (var rightId in childRights)
                            if (listCurrentRmu.All(s => s.RightId != rightId))
                                listRmuToAdd.Add(new IdmRightMapUser
                                {
                                    Enable = true,
                                    InheritedFromRoles = IdmHelper.GenRolesInherited(roleId),
                                    Inherited = true,
                                    RightId = rightId,
                                    UserId = userId,
                                    ApplicationId = applicationId
                                }.InitCreate(appId, actorId));
                    }

                    if (listRmuToAdd.Any()) unitOfWork.GetRepository<IdmRightMapUser>().AddRange(listRmuToAdd);
                    if (await unitOfWork.SaveAsync() > 0)
                        return new Response(Code.Success, "Gán người dùng vào danh sách nhóm thành công");
                    Log.Error("The sql statement is not executed!");
                    return new ResponseError(Code.BadRequest, "Câu lệnh sql không thể thực thi");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return new ResponseError(Code.ServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }

        public Response AddUserMapRoleAsyncV2(IList<Guid> listRoleId, Guid userId, Guid applicationId,
            Guid? appId, Guid? actorId)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    #region Check

                    var listCurrentMap = unitOfWork.GetRepository<IdmUserMapRole>().GetMany(s =>
                        s.UserId == userId && listRoleId.Contains(s.RoleId) && s.ApplicationId == applicationId).ToList();
                    if (listCurrentMap.Count == listRoleId.Count)
                        return new ResponseError(Code.BadRequest, "Người dùng đã tồn tại trong tất cả nhóm !");


                    var userModel = UserCollection.Instance.GetModel(userId);
                    if (userModel == null)
                        return new ResponseError(Code.BadRequest, "Người dùng không tồn tại");
                    var listRoleModel = RoleCollection.Instance.Collection.Where(s => listRoleId.Contains(s.Id))
                        .ToList();
                    if (listRoleModel.Count == 0)
                        return new ResponseError(Code.BadRequest, "Không nhóm người dùng nào tồn tại");

                    #endregion

                    var listUmrToAdd = new List<IdmUserMapRole>();
                    foreach (var roleModel in listRoleModel)
                        listUmrToAdd.Add(new IdmUserMapRole
                        {
                            UserId = userId,
                            RoleId = roleModel.Id,
                            ApplicationId = applicationId
                        }.InitCreate(applicationId, actorId));
                    if (listUmrToAdd.Any()) unitOfWork.GetRepository<IdmUserMapRole>().AddRange(listUmrToAdd);
                    /*Thêm mới User mapp user của user kế thừa*/
                    var listRmuToAdd = new List<IdmRightMapUser>();
                    foreach (var roleId in listRoleId)
                    {
                        var childRights = unitOfWork.GetRepository<IdmRightMapRole>()
                            .GetMany(s => s.RoleId == roleId && s.ApplicationId == applicationId).Select(s => s.RightId);

                        var listCurrentRmu = unitOfWork.GetRepository<IdmRightMapUser>().GetMany(s =>
                            s.UserId == userId && childRights.Contains(s.RightId) && s.ApplicationId == applicationId);
                        foreach (var currentRmu in listCurrentRmu)
                        {
                            currentRmu.InheritedFromRoles =
                                IdmHelper.AddRolesInherited(currentRmu.InheritedFromRoles, roleId);
                            currentRmu.Inherited = true;
                            currentRmu.Enable = true;
                            unitOfWork.GetRepository<IdmRightMapUser>()
                                .Update(currentRmu.InitUpdate(applicationId, actorId));
                        }

                        foreach (var rightId in childRights)
                            if (listCurrentRmu.All(s => s.RightId != rightId))
                                listRmuToAdd.Add(new IdmRightMapUser
                                {
                                    Enable = true,
                                    InheritedFromRoles = IdmHelper.GenRolesInherited(roleId),
                                    Inherited = true,
                                    RightId = rightId,
                                    UserId = userId,
                                    ApplicationId = applicationId
                                }.InitCreate(appId, actorId));
                    }

                    if (listRmuToAdd.Any()) unitOfWork.GetRepository<IdmRightMapUser>().AddRange(listRmuToAdd);
                    if (unitOfWork.Save() > 0)
                        return new Response(Code.Success, "Gán người dùng vào danh sách nhóm thành công");
                    Log.Error("The sql statement is not executed!");
                    return new ResponseError(Code.BadRequest, "Câu lệnh sql không thể thực thi");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return new ResponseError(Code.ServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }

        public async Task<Response> GetUserMapRoleAsync(Guid roleId, Guid applicationId)

        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    var listUserId = await (from rmu in unitOfWork.GetRepository<IdmUserMapRole>().GetAll()
                        where rmu.RoleId == roleId && rmu.ApplicationId == applicationId
                        select rmu.UserId).ToListAsync();
                    var resultData = UserCollection.Instance.GetModel(s => listUserId.Contains(s.Id)).ToList();
                    return new ResponseList<BaseUserModel>(resultData);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return new ResponseError(Code.ServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }

           public async Task<Response> GetUserMapRoleNameAsync(string roleName, Guid applicationId)

        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    var role = await unitOfWork.GetRepository<IdmRole>().FindAsync(x => x.Name ==roleName);
                    if(role == null){
                           return new ResponseList<BaseUserModel>(new List<BaseUserModel>());
                    }
                    var roleId = role.Id;

                    var listUserId = await (from rmu in unitOfWork.GetRepository<IdmUserMapRole>().GetAll()
                        where rmu.RoleId == roleId && rmu.ApplicationId == applicationId
                        select rmu.UserId).ToListAsync();
                    var resultData = UserCollection.Instance.GetModel(s => listUserId.Contains(s.Id)).ToList();
                    return new ResponseList<BaseUserModel>(resultData);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return new ResponseError(Code.ServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }

        public async Task<Response> GetRoleMapUserAsync(Guid userId, Guid applicationId)

        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    var datas = from rmu in unitOfWork.GetRepository<IdmUserMapRole>().GetAll()
                        join r in unitOfWork.GetRepository<IdmRole>().GetAll()
                            on rmu.RoleId equals r.Id
                        where rmu.UserId == userId && rmu.ApplicationId == applicationId
                        select r;
                    var queryList = await datas.ToListAsync();
                    var resultData = AutoMapperUtils.AutoMap<IdmRole, BaseRoleModel>(queryList);
                    return new ResponseList<BaseRoleModel>(resultData);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return new ResponseError(Code.ServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }

        

        public async Task<Response> CheckRoleMapUserAsync(Guid userId, Guid roleId, Guid applicationId)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    var currentMap = await unitOfWork.GetRepository<IdmUserMapRole>().FindAsync(s =>
                        s.UserId == userId && s.RoleId == roleId && s.ApplicationId == applicationId);
                    if (currentMap != null)
                        return new ResponseObject<bool>(true);
                    return new ResponseObject<bool>(false);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return new ResponseError(Code.ServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }

          public async Task<Response> CheckRoleNameMapUserAsync(Guid userId, string roleName, Guid applicationId)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    var role = await unitOfWork.GetRepository<IdmRole>().FindAsync(x => x.Name ==roleName);
                    if(role == null){
                          return new ResponseObject<bool>(false);
                    }
                    var roleId = role.Id;
                    var currentMap = await unitOfWork.GetRepository<IdmUserMapRole>().FindAsync(s =>
                        s.UserId == userId && s.RoleId == roleId && s.ApplicationId == applicationId);
                    if (currentMap != null)
                        return new ResponseObject<bool>(true);
                    return new ResponseObject<bool>(false);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return new ResponseError(Code.ServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }
    }
}