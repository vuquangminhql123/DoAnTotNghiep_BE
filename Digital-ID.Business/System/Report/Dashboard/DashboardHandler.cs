﻿using DigitalID.Data;
using Microsoft.EntityFrameworkCore;
using QRCoder;
using Serilog;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using ZXing;

namespace DigitalID.Business
{
    public class DashboardHandler : IDashboardHandler
    {
        #region Message
        #endregion

        private readonly ICallStoreHelper _storeHelper;

        public DashboardHandler(ICallStoreHelper storeHelper)
        {
            _storeHelper = storeHelper;
        }

        public Response StatisticalAccountRegister(StartDateEndDateModel model)
        {
            try
            {
                var result = _storeHelper.CallStoreWithStartAndEndDateAsync("STA_HND_StatisticalAccountRegister", model.StartDate, model.EndDate);

                if (result.Code == Code.Success && result is ResponseObject<DataTable> resultData)
                {
                    var list = new List<StatisticalAccountRegisterModel>();
                    foreach (DataRow dr in resultData.Data.Rows)
                    {
                        var item = dr.ToObject<StatisticalAccountRegisterModel>();
                        item.DateFormat = item.Date.ToString("dd/MM/yyyy");
                        list.Add(item);
                    }
                    return new ResponseObject<List<StatisticalAccountRegisterModel>>(list.OrderBy(x => x.Date).ToList(), MessageConstants.GetDataSuccessMessage, Code.Success);
                }
                else
                {
                    return new ResponseObject<List<StatisticalAccountRegisterModel>>(null, result.Message, Code.ServerError);
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex, MessageConstants.ErrorLogMessage);
                return new ResponseObject<List<StatisticalAccountRegisterModel>>(null, ex.Message, Code.ServerError);
            }
        }

        public Response StatisticalAccountCheckin(StartDateEndDateModel model)
        {
            try
            {
                var result = _storeHelper.CallStoreWithStartAndEndDateAsync("STA_HND_StatisticalAccountCheckin", model.StartDate, model.EndDate);

                if (result.Code == Code.Success && result is ResponseObject<DataTable> resultData)
                {
                    var list = new List<StatisticalAccountRegisterModel>();
                    foreach (DataRow dr in resultData.Data.Rows)
                    {
                        var item = dr.ToObject<StatisticalAccountRegisterModel>();
                        item.DateFormat = item.Date.ToString("dd/MM/yyyy HH:mm");
                        list.Add(item);
                    }
                    return new ResponseObject<List<StatisticalAccountRegisterModel>>(list.OrderBy(x => x.Date).ToList(), MessageConstants.GetDataSuccessMessage, Code.Success);
                }
                else
                {
                    return new ResponseObject<List<StatisticalAccountRegisterModel>>(null, result.Message, Code.ServerError);
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex, MessageConstants.ErrorLogMessage);
                return new ResponseObject<List<StatisticalAccountRegisterModel>>(null, ex.Message, Code.ServerError);
            }
        }
    }
}