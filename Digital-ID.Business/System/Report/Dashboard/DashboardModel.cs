﻿using DigitalID.Data;
using System;

namespace DigitalID.Business
{
    public class StatisticalAccountRegisterModel
    {
        public DateTime Date { get; set; }
        public string DateFormat { get; set; }
        public int NumberOfAccount { get; set; }
    }
    public class StartDateEndDateModel
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}