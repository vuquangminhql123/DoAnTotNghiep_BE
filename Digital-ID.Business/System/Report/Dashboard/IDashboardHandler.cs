﻿using DigitalID.Data;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DigitalID.Business
{
    /// <summary>
    /// Interface thống kê dữ liệu cho dashboard
    /// </summary>
    public interface IDashboardHandler
    {
        /// <summary>
        /// Thống kê số lượng khách hàng đăng ký theo thời gian
        /// </summary>
        /// <param name="model">Model ngày tháng bắt đầu - kết thúc</param>
        /// <returns>Danh sách số lượng khách hàng đăng ký theo thời gian</returns>
        Response StatisticalAccountRegister(StartDateEndDateModel model);

        /// <summary>
        /// Thống kê số lượng khách hàng checkin theo thời gian
        /// </summary>
        /// <param name="model">Model ngày tháng bắt đầu - kết thúc</param>
        /// <returns>Danh sách số lượng khách hàng checkin theo thời gian</returns>
        Response StatisticalAccountCheckin(StartDateEndDateModel model);
    }
}
