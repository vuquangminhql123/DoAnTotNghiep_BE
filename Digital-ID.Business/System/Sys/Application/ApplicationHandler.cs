﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using LinqKit;
using DigitalID.Data;
using Microsoft.EntityFrameworkCore;
using Serilog;

namespace DigitalID.Business
{
    public class ApplicationHandler : IApplicationHandler
    {
        private readonly DbHandler<SysApplication, ApplicationModel, ApplicationQueryModel> _dbHandler =
            DbHandler<SysApplication, ApplicationModel, ApplicationQueryModel>.Instance;



        public ApplicationHandler()
        {
        }

        public Response GetAll()
        {
            return _dbHandler.GetAll();
        }

        public async Task<Response> GetAllAsync()
        {
            var predicate = PredicateBuilder.New<SysApplication>(true);
            return await _dbHandler.GetAllAsync(predicate);
        }

        public Task<Response> GetAllAsync(ApplicationQueryModel query)
        {
            var predicate = BuildQuery(query);
            return _dbHandler.GetAllAsync(predicate, query.Sort);
        }

        public Task<Response> GetPageAsync(ApplicationQueryModel query)
        {
            var predicate = BuildQuery(query);
            return _dbHandler.GetPageAsync(predicate, query);
        }

        public async Task<Response> BootstrapProjectDataAsync()
        {
            try
            {
                var stopWatch = new Stopwatch();
                stopWatch.Start();
                using (var unitOfWork = new UnitOfWork())
                {
                    await unitOfWork.MigrateAsync();
                    //Check data
                    var hasData = await unitOfWork.GetRepository<IdmRight>().GetAll().FirstOrDefaultAsync();
                    if (hasData==null)
                    {
                        #region seed
                        unitOfWork.GetRepository<SysApplication>().AddRange(
                            new SysApplication
                            {
                                Id = AppConstants.RootAppId,
                                Name = "Core",
                                Code = "1",
                                Description = "Giải pháp quản lý dữ liệu tập trung"
                            },
                            new SysApplication
                            {
                                Id = AppConstants.TestAppId,
                                Name = "Core2",
                                Code = "2",
                                Description = "Giải pháp quản lý dữ liệu tập trung"
                            });

                        unitOfWork.GetRepository<IdmUser>().AddRange(
                            new IdmUser
                            {
                                Name = "Administrator",
                                UserName = "administrator",
                                Id = UserConstants.AdministratorId,
                                Type = 0,
                                LastActivityDate = DateTime.Now,
                                AvatarUrl = "https://avatars0.githubusercontent.com/u/18421313?v=4",
                                Email = "mri.hannibal@gmail.com",
                                PhoneNumber = "0967267469",
                                Password = "",
                                PasswordSalt = ""
                            },
                            new IdmUser
                            {
                                Name = "GUEST",
                                UserName = "guest",
                                Id = UserConstants.UserId,
                                Type = 0,
                                LastActivityDate = DateTime.Now
                            });

                        unitOfWork.GetRepository<IdmRole>().AddRange(
                            new IdmRole
                            {
                                Id = RoleConstants.AdministratorId,
                                Code = "SYS_ADMIN",
                                Name = "Quản trị hệ thống",
                                Description = ""
                            },
                            new IdmRole
                            {
                                Id = RoleConstants.UserId,
                                Code = "USER",
                                Name = "Người dùng hệ thống",
                                Description = ""
                            });

                        unitOfWork.GetRepository<IdmRight>().AddRange(
                            new IdmRight
                            {
                                Id = RightConstants.AccessAppId,
                                Code = RightConstants.AccessAppCode,
                                GroupCode = "Hệ thống",
                                Order = 1,
                                Description = "Truy cập vào ứng dụng",
                                Name = "Truy cập ứng dụng",
                                IsSystem = true,
                                Status = true
                            },
                            new IdmRight

                            {
                                Id = RightConstants.DefaultAppId,
                                Code = RightConstants.DefaultAppCode,
                                GroupCode = "Hệ thống",
                                Order = 2,
                                Description = "Truy cập ứng dụng mặc định",
                                Name = "Truy cập ứng dụng mặc định",
                                IsSystem = true,
                                Status = true
                            },
                            new IdmRight
                            {
                                Id = RightConstants.PemissionId,
                                Code = RightConstants.PemissionCode,
                                GroupCode = "Hệ thống",
                                Order = 1,
                                Description = "Phân quyền người dùng và nhóm người dùng",
                                Name = "Phân quyền người dùng",
                                IsSystem = true,
                                Status = true
                            },
                            new IdmRight
                            {
                                Id = RightConstants.FileAdministratorId,
                                Code = RightConstants.FileAdministratorCode,
                                GroupCode = "Hệ thống",
                                Order = 1,
                                Description = "Quản trị file",
                                Name = "Quản trị file",
                                Status = true,
                                IsSystem = true
                            });

                        unitOfWork.GetRepository<IdmUserMapRole>().AddRange(
                            new IdmUserMapRole
                            {

                                UserId = UserConstants.AdministratorId,
                                RoleId = RoleConstants.AdministratorId
                            }.InitCreate(AppConstants.RootAppId, null),
                            new IdmUserMapRole
                            {

                                UserId = UserConstants.UserId,
                                RoleId = RoleConstants.UserId
                            }.InitCreate(AppConstants.RootAppId, null));

                        unitOfWork.GetRepository<IdmRightMapUser>().AddRange(
                            new IdmRightMapUser
                            {

                                UserId = UserConstants.AdministratorId,
                                RightId = RightConstants.AccessAppId,
                                Enable = true,
                                Inherited = false,
                                InheritedFromRoles = ""
                            }.InitCreate(AppConstants.RootAppId, null),
                            new IdmRightMapUser
                            {

                                UserId = UserConstants.AdministratorId,
                                RightId = RightConstants.DefaultAppId,
                                Enable = true,
                                Inherited = false,
                                InheritedFromRoles = ""
                            }.InitCreate(AppConstants.RootAppId, null),
                            new IdmRightMapUser
                            {

                                UserId = UserConstants.AdministratorId,
                                RightId = RightConstants.FileAdministratorId,
                                Enable = true,
                                Inherited = false,
                                InheritedFromRoles = ""
                            }.InitCreate(AppConstants.RootAppId, null),
                            new IdmRightMapUser
                            {

                                UserId = UserConstants.AdministratorId,
                                RightId = RightConstants.DefaultAppId,
                                Enable = true,
                                Inherited = false,
                                InheritedFromRoles = ""
                            }.InitCreate(AppConstants.TestAppId, null));


                        unitOfWork.GetRepository<BsdNavigation>().AddRange(
                            new BsdNavigation
                            {
                                Id = NavigationConstants.SystemNav,
                                Code = "SYSTEM",
                                Name = "QUẢN TRỊ HỆ THỐNG",
                                IconClass = "fa fa-cogs",
                                Path = "QUẢN TRỊ HỆ THỐNG/",
                                IdPath = NavigationConstants.SystemNav.ToString().ToLower() + "/",
                                Order = 0,
                                Status = true,
                                Level = 0,
                                HasChild = true,
                                ParentId = null,
                                SubUrl = "",
                                UrlRewrite = "",
                            }.InitCreate(AppConstants.RootAppId, null),
                            new BsdNavigation
                            {
                                Id = NavigationConstants.UserNav,
                                Code = "USERS",
                                Name = "Quản lý người dùng",
                                IconClass = "",
                                Path = "QUẢN TRỊ HỆ THỐNG/Quản lý người dùng/",
                                IdPath = NavigationConstants.SystemNav.ToString().ToLower() + "/" +
                                         NavigationConstants.UserNav.ToString().ToLower() + "/",
                                Order = 1,
                                Status = true,
                                Level = 1,
                                HasChild = true,
                                ParentId = NavigationConstants.SystemNav,
                                SubUrl = "",
                                UrlRewrite = "users",
                            }.InitCreate(AppConstants.RootAppId, null),
                            new BsdNavigation
                            {
                                Id = NavigationConstants.RoleNav,
                                Code = "ROLES",
                                Name = "Quản lý nhóm người dùng",
                                IconClass = "",
                                Path = "QUẢN TRỊ HỆ THỐNG/Quản lý nhóm người dùng/",
                                IdPath = NavigationConstants.SystemNav.ToString().ToLower() + "/" +
                                         NavigationConstants.RoleNav.ToString().ToLower() + "/",
                                Order = 2,
                                Status = true,
                                Level = 1,
                                HasChild = true,
                                ParentId = NavigationConstants.SystemNav,
                                SubUrl = "",
                                UrlRewrite = "roles",
                            }.InitCreate(AppConstants.RootAppId, null),
                            new BsdNavigation
                            {
                                Id = NavigationConstants.RightNav,
                                Code = "RIGHTS",
                                Name = "Quản lý quyền",
                                IconClass = "",
                                Path = "QUẢN TRỊ HỆ THỐNG/Quản lý quyền/",
                                IdPath = NavigationConstants.SystemNav.ToString().ToLower() + "/" +
                                         NavigationConstants.RightNav.ToString().ToLower() + "/",
                                Order = 3,
                                Status = true,
                                Level = 1,
                                HasChild = true,
                                ParentId = NavigationConstants.SystemNav,
                                SubUrl = "",
                                UrlRewrite = "rights",
                            }.InitCreate(AppConstants.RootAppId, null),
                            new BsdNavigation
                            {
                                Id = NavigationConstants.OrganizationNav,
                                Code = "ORGANIZATION",
                                Name = "Quản lý đơn vị/pb",
                                IconClass = "",
                                Path = "QUẢN TRỊ HỆ THỐNG/Quản lý đơn vị/pb/",
                                IdPath = NavigationConstants.SystemNav.ToString().ToLower() + "/" +
                                         NavigationConstants.OrganizationNav.ToString().ToLower() + "/",
                                Order = 4,
                                Status = true,
                                Level = 1,
                                HasChild = true,
                                ParentId = NavigationConstants.SystemNav,
                                SubUrl = "",
                                UrlRewrite = "organizations",
                            }.InitCreate(AppConstants.RootAppId, null),
                            new BsdNavigation
                            {
                                Id = NavigationConstants.PositionNav,
                                Code = "POSITION",
                                Name = "Quản lý chức vụ",
                                IconClass = "",
                                Path = "QUẢN TRỊ HỆ THỐNG/Quản lý chức vụ/",
                                IdPath = NavigationConstants.SystemNav.ToString().ToLower() + "/" +
                                            NavigationConstants.PositionNav.ToString().ToLower() + "/",
                                Order = 5,
                                Status = true,
                                Level = 1,
                                HasChild = true,
                                ParentId = NavigationConstants.SystemNav,
                                SubUrl = "",
                                UrlRewrite = "positions",
                            }.InitCreate(AppConstants.RootAppId, null),
                            new BsdNavigation
                            {
                                Id = NavigationConstants.LogNav,
                                Code = "LOG",
                                Name = "Nhật ký hệ thống",
                                IconClass = "",
                                Path = "QUẢN TRỊ HỆ THỐNG/Nhật ký hệ thống/",
                                IdPath = NavigationConstants.SystemNav.ToString().ToLower() + "/" +
                                            NavigationConstants.LogNav.ToString().ToLower() + "/",
                                Order = 5,
                                Status = true,
                                Level = 1,
                                HasChild = true,
                                ParentId = NavigationConstants.SystemNav,
                                SubUrl = "",
                                UrlRewrite = "logs",
                            }.InitCreate(AppConstants.RootAppId, null),
                            new BsdNavigation
                            {
                                Id = NavigationConstants.ParameterNav,
                                Code = "PARAMETER",
                                Name = "Cấu hình hệ thống",
                                IconClass = "",
                                Path = "QUẢN TRỊ HỆ THỐNG/Cấu hình hệ thống/",
                                IdPath = NavigationConstants.SystemNav.ToString().ToLower() + "/" +
                                            NavigationConstants.ParameterNav.ToString().ToLower() + "/",
                                Order = 5,
                                Status = true,
                                Level = 1,
                                HasChild = true,
                                ParentId = NavigationConstants.SystemNav,
                                SubUrl = "",
                                UrlRewrite = "parameters",
                            }.InitCreate(AppConstants.RootAppId, null),
                            new BsdNavigation
                            {
                                Id = NavigationConstants.SignProfileNav,
                                Code = "SIGNPROFILE",
                                Name = "Quản lý profile ký",
                                IconClass = "",
                                Path = "QUẢN TRỊ HỆ THỐNG/Quản lý profile ký/",
                                IdPath = NavigationConstants.SystemNav.ToString().ToLower() + "/" +
                                            NavigationConstants.SignProfileNav.ToString().ToLower() + "/",
                                Order = 5,
                                Status = true,
                                Level = 1,
                                HasChild = true,
                                ParentId = NavigationConstants.SystemNav,
                                SubUrl = "",
                                UrlRewrite = "sign-profiles",
                            }.InitCreate(AppConstants.RootAppId, null),

                            new BsdNavigation
                            {
                                Id = NavigationConstants.PermissionNav,
                                Code = "PERMISSION",
                                Name = "PHÂN QUYỀN",
                                IconClass = "fa fa-user-secret",
                                Path = "PHÂN QUYỀN/",
                                IdPath = NavigationConstants.PermissionNav.ToString().ToLower() + "/",
                                Order = 0,
                                Status = true,
                                Level = 0,
                                HasChild = true,
                                ParentId = null,
                                SubUrl = "",
                                UrlRewrite = "",
                            }.InitCreate(AppConstants.RootAppId, null),
                            new BsdNavigation
                            {
                                Id = NavigationConstants.RMUNav,
                                Code = "NAVIGATIONS",
                                Name = "Phân quyền điều hướng",
                                IconClass = "",
                                Path = "PHÂN QUYỀN/Phân quyền điều hướng/",
                                IdPath = NavigationConstants.PermissionNav.ToString().ToLower() + "/" +
                                         NavigationConstants.NavNav.ToString().ToLower() + "/",
                                Order = 1,
                                Status = true,
                                Level = 1,
                                HasChild = true,
                                ParentId = NavigationConstants.PermissionNav,
                                SubUrl = "",
                                UrlRewrite = "navigations",
                            }.InitCreate(AppConstants.RootAppId, null),
                            new BsdNavigation
                            {
                                Id = NavigationConstants.NavNav,
                                Code = "RIGHT MAP USERS",
                                Name = "Phân quyền người dùng",
                                IconClass = "",
                                Path = "PHÂN QUYỀN/Phân quyền người dùng/",
                                IdPath = NavigationConstants.PermissionNav.ToString().ToLower() + "/" +
                                         NavigationConstants.NavNav.ToString().ToLower() + "/",
                                Order = 1,
                                Status = true,
                                Level = 1,
                                HasChild = true,
                                ParentId = NavigationConstants.PermissionNav,
                                SubUrl = "",
                                UrlRewrite = "right-map-user",
                            }.InitCreate(AppConstants.RootAppId, null),

                            new BsdNavigation
                            {
                                Id = NavigationConstants.CatalogNav,
                                Code = "CATALOG",
                                Name = "Danh mục động",
                                IconClass = "fa fa-list-ol",
                                Path = "Danh mục động/",
                                IdPath = NavigationConstants.CatalogNav.ToString().ToLower() + "/",
                                Order = 3,
                                Status = true,
                                Level = 0,
                                HasChild = false,
                                ParentId = null,
                                SubUrl = "",
                                UrlRewrite = "catalogs",
                            }.InitCreate(AppConstants.RootAppId, null),

                            new BsdNavigation
                            {
                                Id = NavigationConstants.FormNav,
                                Code = "FORM",
                                Name = "BIỂU MẪU",
                                IconClass = "fa fa-wpforms",
                                Path = "BIỂU MẪU/",
                                IdPath = NavigationConstants.FormNav.ToString().ToLower() + "/",
                                Order = 4,
                                Status = true,
                                Level = 0,
                                HasChild = true,
                                ParentId = null,
                                SubUrl = "",
                                UrlRewrite = "",
                            }.InitCreate(AppConstants.RootAppId, null),
                            new BsdNavigation
                            {
                                Id = NavigationConstants.FormMasterNav,
                                Code = "FORM MASTER",
                                Name = "Quản lý biểu mẫu",
                                IconClass = "",
                                Path = "BIỂU MẪU/Quản lý biểu mẫu/",
                                IdPath = NavigationConstants.FormNav.ToString().ToLower() + "/" +
                                         NavigationConstants.FormMasterNav.ToString().ToLower() + "/",
                                Order = 1,
                                Status = true,
                                Level = 1,
                                HasChild = true,
                                ParentId = NavigationConstants.FormNav,
                                SubUrl = "",
                                UrlRewrite = "form/form-master",
                            }.InitCreate(AppConstants.RootAppId, null),
                            new BsdNavigation
                            {
                                Id = NavigationConstants.FormControlNav,
                                Code = "FORM CONTROL",
                                Name = "Quản lý trường tin",
                                IconClass = "",
                                Path = "BIỂU MẪU/Quản lý trường tin/",
                                IdPath = NavigationConstants.FormNav.ToString().ToLower() + "/" +
                                         NavigationConstants.FormControlNav.ToString().ToLower() + "/",
                                Order = 1,
                                Status = true,
                                Level = 1,
                                HasChild = true,
                                ParentId = NavigationConstants.FormNav,
                                SubUrl = "",
                                UrlRewrite = "form/form-control",
                            }.InitCreate(AppConstants.RootAppId, null),
                            new BsdNavigation
                            {
                                Id = NavigationConstants.WorkflowNav,
                                Code = "WORKFLOW",
                                Name = "QUY TRÌNH",
                                IconClass = "fa fa-code-fork",
                                Path = "WORKFLOW/",
                                IdPath = NavigationConstants.WorkflowNav.ToString().ToLower() + "/",
                                Order = 5,
                                Status = true,
                                Level = 0,
                                HasChild = true,
                                ParentId = null,
                                SubUrl = "",
                                UrlRewrite = "",
                            }.InitCreate(AppConstants.RootAppId, null),
                            new BsdNavigation
                            {
                                Id = NavigationConstants.WorkflowSchemeNav,
                                Code = "WORKFLOW SCHEME",
                                Name = "Quản lý scheme",
                                IconClass = "",
                                Path = "QUY TRÌNH/Quản lý scheme/",
                                IdPath = NavigationConstants.WorkflowNav.ToString().ToLower() + "/" +
                                         NavigationConstants.WorkflowSchemeNav.ToString().ToLower() + "/",
                                Order = 1,
                                Status = true,
                                Level = 1,
                                HasChild = true,
                                ParentId = NavigationConstants.WorkflowNav,
                                SubUrl = "",
                                UrlRewrite = "wf/scheme",
                            }.InitCreate(AppConstants.RootAppId, null),
                            new BsdNavigation
                            {
                                Id = NavigationConstants.WorkflowBussinessFlowNav,
                                Code = "WORKFLOW BUSSINESS FLOW",
                                Name = "Quản lý luồng logic",
                                IconClass = "",
                                Path = "QUY TRÌNH/Quản lý luồng logic/",
                                IdPath = NavigationConstants.WorkflowNav.ToString().ToLower() + "/" +
                                         NavigationConstants.WorkflowBussinessFlowNav.ToString().ToLower() + "/",
                                Order = 1,
                                Status = true,
                                Level = 1,
                                HasChild = true,
                                ParentId = NavigationConstants.WorkflowNav,
                                SubUrl = "",
                                UrlRewrite = "wf/bussiness-flow",
                            }.InitCreate(AppConstants.RootAppId, null),
                            new BsdNavigation
                            {
                                Id = NavigationConstants.WorkflowInstanceNav,
                                Code = "WORKFLOW DOCUMENT",
                                Name = "Danh sách xử lý",
                                IconClass = "",
                                Path = "QUY TRÌNH/Danh sách xử lý/",
                                IdPath = NavigationConstants.WorkflowNav.ToString().ToLower() + "/" +
                                         NavigationConstants.WorkflowInstanceNav.ToString().ToLower() + "/",
                                Order = 1,
                                Status = true,
                                Level = 1,
                                HasChild = true,
                                ParentId = NavigationConstants.WorkflowNav,
                                SubUrl = "",
                                UrlRewrite = "wf/document",
                            }.InitCreate(AppConstants.RootAppId, null),
                            new BsdNavigation
                            {
                                Id = NavigationConstants.WorkflowApplicationNav,
                                Code = "WORKFLOW APPLICATION",
                                Name = "Quản lý ứng dụng tích hợp ",
                                IconClass = "",
                                Path = "QUY TRÌNH/Quản lý ứng dụng tích hợp/",
                                IdPath = NavigationConstants.WorkflowNav.ToString().ToLower() + "/" +
                                         NavigationConstants.WorkflowApplicationNav.ToString().ToLower() + "/",
                                Order = 1,
                                Status = true,
                                Level = 1,
                                HasChild = true,
                                ParentId = NavigationConstants.WorkflowNav,
                                SubUrl = "",
                                UrlRewrite = "wf/application",
                            }.InitCreate(AppConstants.RootAppId, null)
                        );

                        unitOfWork.GetRepository<BsdNavigationMapRole>().AddRange(
                            new BsdNavigationMapRole
                            {

                                NavigationId = NavigationConstants.SystemNav,
                                RoleId = RoleConstants.AdministratorId
                            },
                            new BsdNavigationMapRole
                            {

                                NavigationId = NavigationConstants.UserNav,
                                RoleId = RoleConstants.AdministratorId
                            },
                            new BsdNavigationMapRole
                            {

                                NavigationId = NavigationConstants.RightNav,
                                RoleId = RoleConstants.AdministratorId
                            },
                            new BsdNavigationMapRole
                            {

                                NavigationId = NavigationConstants.RoleNav,
                                RoleId = RoleConstants.AdministratorId
                            },
                            new BsdNavigationMapRole
                            {

                                NavigationId = NavigationConstants.PositionNav,
                                RoleId = RoleConstants.AdministratorId
                            },
                            new BsdNavigationMapRole
                            {

                                NavigationId = NavigationConstants.OrganizationNav,
                                RoleId = RoleConstants.AdministratorId
                            },
                            new BsdNavigationMapRole
                            {

                                NavigationId = NavigationConstants.LogNav,
                                RoleId = RoleConstants.AdministratorId
                            },
                            new BsdNavigationMapRole
                            {

                                NavigationId = NavigationConstants.ParameterNav,
                                RoleId = RoleConstants.AdministratorId
                            },
                            new BsdNavigationMapRole
                            {

                                NavigationId = NavigationConstants.SignProfileNav,
                                RoleId = RoleConstants.AdministratorId
                            },


                            new BsdNavigationMapRole
                            {

                                NavigationId = NavigationConstants.PermissionNav,
                                RoleId = RoleConstants.AdministratorId
                            },
                            new BsdNavigationMapRole
                            {

                                NavigationId = NavigationConstants.NavNav,
                                RoleId = RoleConstants.AdministratorId
                            },
                            new BsdNavigationMapRole
                            {

                                NavigationId = NavigationConstants.RMUNav,
                                RoleId = RoleConstants.AdministratorId
                            },

                            new BsdNavigationMapRole
                            {

                                NavigationId = NavigationConstants.CatalogNav,
                                RoleId = RoleConstants.AdministratorId
                            },

                            new BsdNavigationMapRole
                            {
                                NavigationId = NavigationConstants.FormControlNav,
                                RoleId = RoleConstants.AdministratorId
                            },
                            new BsdNavigationMapRole
                            {

                                NavigationId = NavigationConstants.FormMasterNav,
                                RoleId = RoleConstants.AdministratorId
                            },
                            new BsdNavigationMapRole
                            {

                                NavigationId = NavigationConstants.FormNav,
                                RoleId = RoleConstants.AdministratorId
                            },
                            new BsdNavigationMapRole
                            {

                                NavigationId = NavigationConstants.WorkflowBussinessFlowNav,
                                RoleId = RoleConstants.AdministratorId
                            },
                            new BsdNavigationMapRole
                            {

                                NavigationId = NavigationConstants.WorkflowInstanceNav,
                                RoleId = RoleConstants.AdministratorId
                            },
                              new BsdNavigationMapRole
                              {

                                  NavigationId = NavigationConstants.WorkflowApplicationNav,
                                  RoleId = RoleConstants.AdministratorId
                              },
                            new BsdNavigationMapRole
                            {

                                NavigationId = NavigationConstants.WorkflowNav,
                                RoleId = RoleConstants.AdministratorId
                            },
                            new BsdNavigationMapRole
                            {

                                NavigationId = NavigationConstants.WorkflowSchemeNav,
                                RoleId = RoleConstants.AdministratorId
                            });




                        unitOfWork.GetRepository<TaxonomyVocabulary>().AddRange(
                            new TaxonomyVocabulary()
                            {
                                Code = "CATEGORY_MASTER",
                                Name = "CATEGORY_MASTER",
                                Description = "",
                                Weight = 0,
                                CreatedByUserId = UserConstants.AdministratorId,
                                LastModifiedByUserId = UserConstants.AdministratorId,
                                CreatedOnDate = DateTime.Now,
                                LastModifiedOnDate = DateTime.Now,
                                ApplicationId = AppConstants.RootAppId,

                                IsSystem = true,
                                VocabularyId = TaxonomyConstants.CATEGORY_MASTER,
                                VocabularyTypeId = TaxonomyConstants.CATEGORY_MASTER,


                            }
                        );


                        #endregion
                        if (await unitOfWork.SaveAsync() > 0)
                            return new ResponseUpdate(Guid.Empty);
                        Log.Error("The sql statement is not executed!");
                        return new ResponseError(Code.BadRequest, "Câu lệnh sql không thể thực thi");
                    }
                    
                    return new ResponseUpdate(Guid.Empty);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return new ResponseError(Code.ServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }

        private Expression<Func<SysApplication, bool>> BuildQuery(ApplicationQueryModel query)
        {
            var predicate = PredicateBuilder.New<SysApplication>(true);
            if (!string.IsNullOrEmpty(query.Name)) predicate.And(s => s.Name == query.Name);
            if (!string.IsNullOrEmpty(query.Name)) predicate.And(s => s.Name == query.Name);
            if (query.ListId != null && query.ListId.Count > 0) predicate.And(s => query.ListId.Contains(s.Id));
            if (!string.IsNullOrEmpty(query.FullTextSearch))
                predicate.And(s =>
                    s.Name.ToLower().Contains(query.FullTextSearch.ToLower()) || s.Name.ToLower().Contains(query.FullTextSearch.ToLower()) ||
                    s.Description.ToLower().Contains(query.FullTextSearch.ToLower()));
            return predicate;
        }

        #region CRUD

        public Task<Response> FindAsync(Guid id)
        {
            return _dbHandler.FindAsync(id);
        }

        public async Task<Response> CreateAsync(ApplicationCreateModel model)
        {
            var request = new SysApplication
            {
                Id = Guid.NewGuid(),
                Name = model.Name,
                Code = model.Code ?? model.Name.ToUniqueName(),
                Description = model.Description
            };
            var result = await _dbHandler.CreateAsync(request);
            ApplicationCollection.Instance.LoadToHashSet();
            return result;
        }

        public async Task<Response> UpdateAsync(Guid id, ApplicationUpdateModel model)
        {
            var request = new SysApplication
            {
                Id = id,
                Name = model.Name,
                Code = model.Code ?? model.Name.ToUniqueName(),
                Description = model.Description
            };
            var result = await _dbHandler.UpdateAsync(id, request);
            ApplicationCollection.Instance.LoadToHashSet();
            return result;
        }

        public async Task<Response> DeleteAsync(Guid id)
        {
            var result = await _dbHandler.DeleteAsync(id);
            ApplicationCollection.Instance.LoadToHashSet();
            return result;
        }

        public async Task<Response> DeleteRangeAsync(IList<Guid> listId)
        {
            var result = await _dbHandler.DeleteRangeAsync(listId, "Id");
            ApplicationCollection.Instance.LoadToHashSet();
            return result;
        }

        public async Task<Response> GetAllByUserIdAsync(Guid userId)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    var compare = RightConstants.DefaultAppId.CompareTo(RightConstants.AccessAppId);
                    if (compare == 1)
                    {
                        var result = await (from rmu in unitOfWork.GetRepository<IdmRightMapUser>().GetAll()
                                            join a in unitOfWork.GetRepository<SysApplication>().GetAll()
                                                on rmu.ApplicationId equals a.Id
                                            orderby rmu.RightId descending
                                            where rmu.UserId == userId &&
                                                  (rmu.RightId == RightConstants.DefaultAppId ||
                                                   rmu.RightId == RightConstants.AccessAppId)
                                            select a).Distinct().ToListAsync();
                        if (result != null)
                            return new ResponseList<ApplicationModel>(
                                AutoMapperUtils.AutoMap<SysApplication, ApplicationModel>(result));
                    }
                    else
                    {
                        var result = await (from rmu in unitOfWork.GetRepository<IdmRightMapUser>().GetAll()
                                            join a in unitOfWork.GetRepository<SysApplication>().GetAll()
                                                on rmu.ApplicationId equals a.Id
                                            orderby rmu.RightId
                                            where rmu.UserId == userId &&
                                                  (rmu.RightId == RightConstants.DefaultAppId ||
                                                   rmu.RightId == RightConstants.AccessAppId)
                                            select a).Distinct().ToListAsync();
                        if (result != null)
                            return new ResponseList<ApplicationModel>(
                                AutoMapperUtils.AutoMap<SysApplication, ApplicationModel>(result));
                    }

                    return new ResponseError(Code.NotFound, "Không tìm thấy dữ liệu");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return new ResponseError(Code.ServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }

        public async Task<Response> GetDefautByUserIdAsync(Guid userId)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    var compare = RightConstants.DefaultAppId.CompareTo(RightConstants.AccessAppId);
                    if (compare == 1)
                    {
                        var result = await (from rmu in unitOfWork.GetRepository<IdmRightMapUser>().GetAll()
                                            join a in unitOfWork.GetRepository<SysApplication>().GetAll()
                                                on rmu.ApplicationId equals a.Id
                                            orderby rmu.RightId descending
                                            where rmu.UserId == userId &&
                                                  (rmu.RightId == RightConstants.DefaultAppId ||
                                                   rmu.RightId == RightConstants.AccessAppId)
                                            select a).Distinct().FirstOrDefaultAsync();
                        if (result != null)
                            return new ResponseObject<ApplicationModel>(
                                AutoMapperUtils.AutoMap<SysApplication, ApplicationModel>(result));
                    }
                    else
                    {
                        var result = await (from rmu in unitOfWork.GetRepository<IdmRightMapUser>().GetAll()
                                            join a in unitOfWork.GetRepository<SysApplication>().GetAll()
                                                on rmu.ApplicationId equals a.Id
                                            orderby rmu.RightId
                                            where rmu.UserId == userId &&
                                                  (rmu.RightId == RightConstants.DefaultAppId ||
                                                   rmu.RightId == RightConstants.AccessAppId)
                                            select a).Distinct().FirstOrDefaultAsync();
                        if (result != null)
                            return new ResponseObject<ApplicationModel>(
                                AutoMapperUtils.AutoMap<SysApplication, ApplicationModel>(result));
                    }
                    var result1 = new SysApplication();
                    return new ResponseObject<ApplicationModel>(
                        AutoMapperUtils.AutoMap<SysApplication, ApplicationModel>(result1));
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return new ResponseError(Code.ServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }

        #endregion
    }
}