﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DigitalID.Data;

namespace DigitalID.Business
{
    public interface IApplicationHandler
    {
        Task<Response> FindAsync(Guid id);
        Task<Response> CreateAsync(ApplicationCreateModel model);
        Task<Response> UpdateAsync(Guid id, ApplicationUpdateModel model);
        Task<Response> DeleteAsync(Guid id);
        Task<Response> DeleteRangeAsync(IList<Guid> listId);
        Task<Response> GetAllAsync();
        Task<Response> GetAllAsync(ApplicationQueryModel query);
        Task<Response> GetPageAsync(ApplicationQueryModel query);
        Response GetAll();
        Task<Response> BootstrapProjectDataAsync();
        Task<Response> GetAllByUserIdAsync(Guid userId);
        Task<Response> GetDefautByUserIdAsync(Guid userId);
        
    }
}