﻿using System;
using System.Collections.Generic;
using System.Text;


namespace IO.Swagger.Models
{
  public  class BussinessFlow
    {

        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Scheme { get; set; }
        public string DefaultForm { get; set; }
        public string DefaultCreateForm { get; set; }
        public IList<BussinessFlowMap> Map { get; set; }
    }
    public class BussinessFlowMap
    {
        public IList<string> States { get; set; }
        public IList<string> Roles { get; set; }
        public string Form { get; set; } 
    }
}
