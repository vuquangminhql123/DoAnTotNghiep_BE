/*
 * Workflow Server API
 *
 * Describes the basic methods for managing WorkflowEngine processes
 *
 * OpenAPI spec version: 2.3.0
 * Contact: sales@optimajet.com
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace IO.Swagger.Models
{ 
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public partial class CommandParameter : IEquatable<CommandParameter>
    { 
        /// <summary>
        /// Gets or Sets ParameterName
        /// </summary>
        [Required]
        [DataMember(Name="ParameterName")]
        public string ParameterName { get; set; }

        /// <summary>
        /// Gets or Sets LocalizedName
        /// </summary>
        [DataMember(Name="LocalizedName")]
        public string LocalizedName { get; set; }

        /// <summary>
        /// Gets or Sets TypeName
        /// </summary>
        [DataMember(Name="TypeName")]
        public string TypeName { get; set; }

        /// <summary>
        /// Gets or Sets IsRequired
        /// </summary>
        [Required]
        [DataMember(Name="IsRequired")]
        public bool? IsRequired { get; set; }

        /// <summary>
        /// Gets or Sets DefaultValue
        /// </summary>
        [DataMember(Name="DefaultValue")]
        public string DefaultValue { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class CommandParameter {\n");
            sb.Append("  ParameterName: ").Append(ParameterName).Append("\n");
            sb.Append("  LocalizedName: ").Append(LocalizedName).Append("\n");
            sb.Append("  TypeName: ").Append(TypeName).Append("\n");
            sb.Append("  IsRequired: ").Append(IsRequired).Append("\n");
            sb.Append("  DefaultValue: ").Append(DefaultValue).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="obj">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == GetType() && Equals((CommandParameter)obj);
        }

        /// <summary>
        /// Returns true if CommandParameter instances are equal
        /// </summary>
        /// <param name="other">Instance of CommandParameter to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(CommandParameter other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;

            return 
                (
                    ParameterName == other.ParameterName ||
                    ParameterName != null &&
                    ParameterName.Equals(other.ParameterName)
                ) && 
                (
                    LocalizedName == other.LocalizedName ||
                    LocalizedName != null &&
                    LocalizedName.Equals(other.LocalizedName)
                ) && 
                (
                    TypeName == other.TypeName ||
                    TypeName != null &&
                    TypeName.Equals(other.TypeName)
                ) && 
                (
                    IsRequired == other.IsRequired ||
                    IsRequired != null &&
                    IsRequired.Equals(other.IsRequired)
                ) && 
                (
                    DefaultValue == other.DefaultValue ||
                    DefaultValue != null &&
                    DefaultValue.Equals(other.DefaultValue)
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                var hashCode = 41;
                // Suitable nullity checks etc, of course :)
                    if (ParameterName != null)
                    hashCode = hashCode * 59 + ParameterName.GetHashCode();
                    if (LocalizedName != null)
                    hashCode = hashCode * 59 + LocalizedName.GetHashCode();
                    if (TypeName != null)
                    hashCode = hashCode * 59 + TypeName.GetHashCode();
                    if (IsRequired != null)
                    hashCode = hashCode * 59 + IsRequired.GetHashCode();
                    if (DefaultValue != null)
                    hashCode = hashCode * 59 + DefaultValue.GetHashCode();
                return hashCode;
            }
        }

        #region Operators
        #pragma warning disable 1591

        public static bool operator ==(CommandParameter left, CommandParameter right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(CommandParameter left, CommandParameter right)
        {
            return !Equals(left, right);
        }

        #pragma warning restore 1591
        #endregion Operators
    }
}
