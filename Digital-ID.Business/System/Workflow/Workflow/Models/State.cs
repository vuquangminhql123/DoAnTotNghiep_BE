/*
 * Workflow Server API
 *
 * Describes the basic methods for managing WorkflowEngine processes
 *
 * OpenAPI spec version: 2.3.0
 * Contact: sales@optimajet.com
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace IO.Swagger.Models
{ 
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public partial class State : IEquatable<State>
    { 
        /// <summary>
        /// Gets or Sets Name
        /// </summary>
        [Required]
        [DataMember(Name="Name")]
        public string Name { get; set; }

        /// <summary>
        /// Gets or Sets VisibleName
        /// </summary>
        [DataMember(Name="VisibleName")]
        public string VisibleName { get; set; }

        /// <summary>
        /// Gets or Sets SchemeCode
        /// </summary>
        [Required]
        [DataMember(Name="SchemeCode")]
        public string SchemeCode { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class State {\n");
            sb.Append("  Name: ").Append(Name).Append("\n");
            sb.Append("  VisibleName: ").Append(VisibleName).Append("\n");
            sb.Append("  SchemeCode: ").Append(SchemeCode).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="obj">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == GetType() && Equals((State)obj);
        }

        /// <summary>
        /// Returns true if State instances are equal
        /// </summary>
        /// <param name="other">Instance of State to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(State other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;

            return 
                (
                    Name == other.Name ||
                    Name != null &&
                    Name.Equals(other.Name)
                ) && 
                (
                    VisibleName == other.VisibleName ||
                    VisibleName != null &&
                    VisibleName.Equals(other.VisibleName)
                ) && 
                (
                    SchemeCode == other.SchemeCode ||
                    SchemeCode != null &&
                    SchemeCode.Equals(other.SchemeCode)
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                var hashCode = 41;
                // Suitable nullity checks etc, of course :)
                    if (Name != null)
                    hashCode = hashCode * 59 + Name.GetHashCode();
                    if (VisibleName != null)
                    hashCode = hashCode * 59 + VisibleName.GetHashCode();
                    if (SchemeCode != null)
                    hashCode = hashCode * 59 + SchemeCode.GetHashCode();
                return hashCode;
            }
        }

        #region Operators
        #pragma warning disable 1591

        public static bool operator ==(State left, State right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(State left, State right)
        {
            return !Equals(left, right);
        }

        #pragma warning restore 1591
        #endregion Operators
    }
}
