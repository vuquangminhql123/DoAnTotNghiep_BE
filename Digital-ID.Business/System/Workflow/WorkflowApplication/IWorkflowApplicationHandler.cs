﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DigitalID.Data;

namespace DigitalID.Business
{
    public interface IWorkflowApplicationHandler
    {
        Task<Response> FindAsync(Guid id);
        Task<Response> CreateAsync(WorkflowApplicationCreateModel model, Guid applicationId, Guid userId);
        Task<Response> UpdateAsync(Guid id, WorkflowApplicationUpdateModel model, Guid applicationId, Guid userId);
        Task<Response> DeleteAsync(Guid id);
        Task<Response> DeleteRangeAsync(IList<Guid> listId);
        Task<Response> GetAllAsync();
        Task<Response> GetAllAsync(WorkflowApplicationQueryModel query);
        Task<Response> GetPageAsync(WorkflowApplicationQueryModel query);
        Response GetAll();
        string GetConfig(Guid documentId);
    }
}