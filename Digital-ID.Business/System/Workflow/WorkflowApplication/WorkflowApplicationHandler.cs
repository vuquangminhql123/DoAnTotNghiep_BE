﻿using LinqKit;
using DigitalID.Data;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Serilog;
using DigitalID.Data.Models;

namespace DigitalID.Business
{
    public class WorkflowApplicationHandler : IWorkflowApplicationHandler
    {
        private readonly DbHandler<WorkflowApplication, WorkflowApplicationModel, WorkflowApplicationQueryModel> _dbHandler = DbHandler<WorkflowApplication, WorkflowApplicationModel, WorkflowApplicationQueryModel>.Instance;

        #region CRUD
        public string GetConfig(Guid documentId)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    var currentDocument = unitOfWork.GetRepository<DigitalID.Data.Models.WorkflowDocument>().Find(documentId);
                    if (currentDocument == null) return null;
                    var currentApp = unitOfWork.GetRepository<DigitalID.Data.Models.WorkflowApplication>().Find(x => x.Id == currentDocument.WorkflowApplicationId && x.Status == true);
                    if (currentApp == null) return null;
                    return currentApp.ProcessStatusChangedConfig;

                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return null;
            }
        }
        public Task<Response> FindAsync(Guid id)
        {
            return _dbHandler.FindAsync(id);
        }
        public async Task<Response> CreateAsync(WorkflowApplicationCreateModel model, Guid applicationId, Guid userId)
        {
            WorkflowApplication request = AutoMapperUtils.AutoMap<WorkflowApplicationCreateModel, WorkflowApplication>(model);
            request = request.InitCreate(applicationId, userId);
            var result = await _dbHandler.CreateAsync(request);
            return result;
        }
        public async Task<Response> UpdateAsync(Guid id, WorkflowApplicationUpdateModel model, Guid applicationId, Guid userId)
        {
            WorkflowApplication request = AutoMapperUtils.AutoMap<WorkflowApplicationUpdateModel, WorkflowApplication>(model);
            request = request.InitUpdate(applicationId, userId);
            request.Id = id;
            var result = await _dbHandler.UpdateAsync(id, request);
            return result;
        }
        public async Task<Response> DeleteAsync(Guid id)
        {
            var result = await _dbHandler.DeleteAsync(id);
            return result;
        }
        public async Task<Response> DeleteRangeAsync(IList<Guid> listId)
        {
            var result = await _dbHandler.DeleteRangeAsync(listId, "Id");
            return result;
        }
        #endregion
        public Response GetAll()
        {
            return _dbHandler.GetAll();
        }
        public async Task<Response> GetAllAsync()
        {
            var predicate = PredicateBuilder.New<WorkflowApplication>(true);
            return await _dbHandler.GetAllAsync(predicate);
        }
        public Task<Response> GetAllAsync(WorkflowApplicationQueryModel query)
        {
            var predicate = BuildQuery(query);
            return _dbHandler.GetAllAsync(predicate, query.Sort);
        }
        public Task<Response> GetPageAsync(WorkflowApplicationQueryModel query)
        {
            var predicate = BuildQuery(query);
            return _dbHandler.GetPageAsync(predicate, query);
        }
        private Expression<Func<WorkflowApplication, bool>> BuildQuery(WorkflowApplicationQueryModel query)
        {
            var predicate = PredicateBuilder.New<WorkflowApplication>(true);
            if (!string.IsNullOrEmpty(query.Name))
            {
                predicate.And(s => s.Name == query.Name);
            }
            if (query.Status.HasValue)
            {
                predicate.And(s => s.Status == query.Status.Value);
            }
            if (query.ListId != null && query.ListId.Count > 0)
            {
                predicate.And(s => query.ListId.Contains(s.Id));
            }
            if (!string.IsNullOrEmpty(query.FullTextSearch))
            {
                predicate.And(s => s.Name.ToLower().Contains(query.FullTextSearch.ToLower()));
            }
            return predicate;
        }


    }
}
