﻿using DigitalID.Data;
using System;
using System.Collections.Generic;

namespace DigitalID.Business
{
    public class BaseWorkflowApplicationModel : BaseModel
    {

        public Guid Id { get; set; }
        public string Name { get; set; }
        public string ProcessStatusChangedConfig { get; set; }
        public bool Status { get; set; }
    }
    public class WorkflowApplicationModel : BaseWorkflowApplicationModel
    {
        public string TokenAPI => TokenHelpers.CreateBasicToken(Id.ToString().ToLower());
    }
    public class WorkflowApplicationQueryModel : PaginationRequest
    {

        public string Name { get; set; }
        public bool? Status { get; set; }
    }

    public class WorkflowApplicationCreateModel
    { 
        public Guid? Id { get; set; }
        public string TokenAPI { get; set; }
        public string Name { get; set; }
        public string ProcessStatusChangedConfig { get; set; }
        public bool Status { get; set; }
    }
    public class WorkflowApplicationUpdateModel
    {
        public Guid Id { get; set; }
        public string TokenAPI { get; set; }
        public string Name { get; set; }
        public string ProcessStatusChangedConfig { get; set; }
        public bool Status { get; set; }
    }

}

