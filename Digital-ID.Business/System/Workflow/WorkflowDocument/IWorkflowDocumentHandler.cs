﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DigitalID.Data;

namespace DigitalID.Business
{
    public interface IWorkflowDocumentHandler
    {
        Task<Response> FindAsync(Guid id);
        Task<Response> CreateAsync(WorkflowDocumentCreateModel model, Guid applicationId, Guid userId);
        Task<Response> UpdateAsync(Guid id, WorkflowDocumentUpdateModel model, Guid applicationId, Guid userId);
        Task<Response> DeleteAsync(Guid id);
        Task<Response> DeleteRangeAsync(IList<Guid> listId);
        Task<Response> GetAllAsync();
        Task<Response> GetAllAsync(WorkflowDocumentQueryModel query);
        Task<Response> GetPageAsync(WorkflowDocumentQueryModel query);
        Response GetAll();
        Task<Response> UpdateListAttachment(List<WorkflowDocumentAttachmentModel> model, Guid documentId, Guid applicationId, Guid userId);
        Task<Response> UpdateInfoAsync(Guid id, WorkflowDocumentUpdateInfoModel model);
        Task<Response> Statistic(DateTime? fromDate, DateTime? toDate);
        Task<Response> StaticInRange(DateTime? fromDate, DateTime? toDate, DateRangeType typeRange);
        void UpdateShowSign(Guid documentId, int value);
    }
}