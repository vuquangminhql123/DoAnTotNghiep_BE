﻿using LinqKit;
using DigitalID.Data;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Serilog;
using DigitalID.Data.Models;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace DigitalID.Business
{
    public class WorkflowDocumentHandler : IWorkflowDocumentHandler
    {
        private readonly DbHandler<WorkflowDocument, WorkflowDocumentModel, WorkflowDocumentQueryModel> _dbHandler = DbHandler<WorkflowDocument, WorkflowDocumentModel, WorkflowDocumentQueryModel>.Instance;
        public async Task<Response> UpdateListAttachment(List<WorkflowDocumentAttachmentModel> model, Guid documentId, Guid applicationId, Guid userId)
        {

            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    unitOfWork.GetRepository<WorkflowDocumentAttachment>().DeleteRange(x => x.WorkflowDocumentId == documentId);
                    foreach (var item in model)
                    {
                        item.WorkflowDocumentId = documentId;
                        //Fix
                        item.CreatedByUserId = userId;
                        item.LastModifiedByUserId = userId;
                        item.ApplicationId = applicationId;
                        item.CreatedOnDate = DateTime.Now;
                        item.LastModifiedOnDate = DateTime.Now;
                    }
                    unitOfWork.GetRepository<WorkflowDocumentAttachment>().AddRange(AutoMapperUtils.AutoMap<WorkflowDocumentAttachmentModel, WorkflowDocumentAttachment>(model));
                    if (await unitOfWork.SaveAsync() > 0)
                        return new ResponseUpdate(documentId);
                    Log.Error("The sql statement is not executed!");
                    return new ResponseError(Code.BadRequest, "Câu lệnh sql không thể thực thi");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return new ResponseError(Code.ServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }

        #region CRUD
        public Task<Response> FindAsync(Guid id)
        {
            return _dbHandler.FindAsync(id);
        }
        public async Task<Response> CreateAsync(WorkflowDocumentCreateModel model, Guid applicationId, Guid userId)
        {
            WorkflowDocument request = AutoMapperUtils.AutoMap<WorkflowDocumentCreateModel, WorkflowDocument>(model);
            request = request.InitCreate(applicationId, userId);
            #region Statistic
            request.IsDraft = 1;
            request.IsInProcess = 0;
            request.IsCompleted = 0;
            request.IsRejected = 0;

            request.DAY = request.ExecutedTransition.ToString("dd/MM/yyyy");
            request.MONTH = request.ExecutedTransition.ToString("MM/yyyy");
            request.YEAR = request.ExecutedTransition.ToString("yyyy");
            request.WEEK = Utils.GetIso8601WeekOfYear(request.ExecutedTransition).ToString();
            #endregion 
            var result = await _dbHandler.CreateAsync(request);
            return result;
        }
        public async Task<Response> UpdateAsync(Guid id, WorkflowDocumentUpdateModel model, Guid applicationId, Guid userId)
        {
            WorkflowDocument request = AutoMapperUtils.AutoMap<WorkflowDocumentUpdateModel, WorkflowDocument>(model);
            request = request.InitUpdate(applicationId, userId);
            request.Id = id;
            var result = await _dbHandler.UpdateAsync(id, request);
            await UpdateListAttachment(model.ListAttachment, id, applicationId, userId);

            return result;
        }
        public async Task<Response> DeleteAsync(Guid id)
        {
            var result = await _dbHandler.DeleteAsync(id);
            return result;
        }
        public async Task<Response> DeleteRangeAsync(IList<Guid> listId)
        {
            var result = await _dbHandler.DeleteRangeAsync(listId, "Id");
            return result;
        }
        #endregion
        public Response GetAll()
        {
            return _dbHandler.GetAll();
        }
        public async Task<Response> GetAllAsync()
        {
            var predicate = PredicateBuilder.New<WorkflowDocument>(true);
            return await _dbHandler.GetAllAsync(predicate);
        }
        public Task<Response> GetAllAsync(WorkflowDocumentQueryModel query)
        {
            var predicate = BuildQuery(query);
            return _dbHandler.GetAllAsync(predicate, query.Sort);
        }
        public Task<Response> GetPageAsync(WorkflowDocumentQueryModel query)
        {
            var predicate = BuildQuery(query);
            var dataReturn = _dbHandler.GetPageAsync(predicate, query);
            return dataReturn;
        }
        private Expression<Func<WorkflowDocument, bool>> BuildQuery(WorkflowDocumentQueryModel query)
        {
            var predicate = PredicateBuilder.New<WorkflowDocument>(true);
            if (!string.IsNullOrEmpty(query.Name))
            {
                predicate.And(s => s.Name == query.Name);
            }
            if (!string.IsNullOrEmpty(query.BussinessFlow))
            {
                predicate.And(s => s.BussinessFlow == query.BussinessFlow);
            }
            if (!string.IsNullOrEmpty(query.Scheme))
            {
                predicate.And(s => s.Scheme == query.Scheme);
            }
            if (!string.IsNullOrEmpty(query.Status))
            {
                switch (query.Status)
                {
                    case "isDraft":
                        predicate.And(s => s.IsDraft == 1);
                        break;
                    case "isInProcess":
                        predicate.And(s => s.IsInProcess == 1);
                        break;
                    case "isCompleted":
                        predicate.And(s => s.IsCompleted == 1);
                        break;
                    case "isRejected":
                        predicate.And(s => s.IsRejected == 1);
                        break;
                }

            }
            if (query.ListId != null && query.ListId.Count > 0)
            {
                predicate.And(s => query.ListId.Contains(s.Id));
            }
            if (!string.IsNullOrEmpty(query.FullTextSearch))
            {
                predicate.And(s => s.Name.ToLower().Contains(query.FullTextSearch.ToLower()));
            }
            if (!string.IsNullOrEmpty(query.AllowIdentityId))
            {
                predicate.And(s => s.AllowIdentityIds.ToLower().Contains(query.AllowIdentityId.ToLower()));
            }

            return predicate;
        }

        public async Task<Response> UpdateInfoAsync(Guid id, WorkflowDocumentUpdateInfoModel model)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {

                    var current = await unitOfWork.GetRepository<WorkflowDocument>().FindAsync(id);
                    if (current == null)
                    {
                        Log.Error($"{id} not found");
                        return new ResponseError(Code.BadRequest, "Id không tồn tại");
                    }
                    current.Status = string.IsNullOrEmpty(model.Status) ? current.Status : model.Status;
                    current.PrevState = string.IsNullOrEmpty(model.PrevState) ? current.PrevState : model.PrevState;
                    current.PrevActivity = string.IsNullOrEmpty(model.PrevActivity) ? current.PrevActivity : model.PrevActivity;
                    current.State = string.IsNullOrEmpty(model.State) ? current.State : model.State;
                    current.Activity = string.IsNullOrEmpty(model.Activity) ? current.Activity : model.Activity;
                    current.NextActivity = string.IsNullOrEmpty(model.NextActivity) ? current.NextActivity : model.NextActivity;
                    current.NextState = string.IsNullOrEmpty(model.NextState) ? current.NextState : model.NextState;
                    current.AllowIdentityIds = model.AllowIdentityIds;
                    current.ListSubState = model.ListSubState;
                    current.TransitionClassifier = model.TransitionClassifier;
                    current.ExecutedTransition = model.ExecutedTransition;
                    current.ExecutedIdentityId = model.ExecutedIdentityId;
                    current.ExecutedAction = model.ExecutedAction;
                    current.ExecutedActionValue = model.ExecutedActionValue;
                    current.ExecutedFormInputJson = model.ExecutedFormInputJson;
                    current.ExecutedFiles = model.ExecutedFiles;

                    #region Statistic
                    if (current.Status == "Idled" && current.PrevState == null)
                    {
                        current.IsDraft = 1;
                        current.IsInProcess = 0;
                        current.IsCompleted = 0;
                        current.IsRejected = 0;

                    }
                    if (current.Status == "Idled" && current.PrevState != null)
                    {
                        current.IsDraft = 0;
                        current.IsInProcess = 1;
                        current.IsCompleted = 0;
                        current.IsRejected = 0;

                    }
                    if (current.Status == "Finalized" && (current.TransitionClassifier == 1 || current.TransitionClassifier == 1))
                    {
                        current.IsDraft = 0;
                        current.IsInProcess = 0;
                        current.IsCompleted = 1;
                        current.IsRejected = 0;

                    }
                    if (current.Status == "Finalized" && current.TransitionClassifier == 2)
                    {
                        current.IsDraft = 0;
                        current.IsInProcess = 0;
                        current.IsCompleted = 0;
                        current.IsRejected = 1;

                    }
                    current.DAY = current.ExecutedTransition.ToString("dd/MM/yyyy");
                    current.MONTH = current.ExecutedTransition.ToString("MM/yyyy");
                    current.YEAR = current.ExecutedTransition.ToString("yyyy");
                    current.WEEK = Utils.GetIso8601WeekOfYear(current.ExecutedTransition).ToString();
                    #endregion 


                    unitOfWork.GetRepository<WorkflowDocument>().Update(current);

                    if (await unitOfWork.SaveAsync() > 0)
                        return new ResponseObject<WorkflowDocumentModel>(AutoMapperUtils.AutoMap<WorkflowDocument, WorkflowDocumentModel>(current));

                    Log.Error("The sql statement is not executed!");
                    return new ResponseError(Code.BadRequest, "Câu lệnh sql không thể thực thi");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return new ResponseError(Code.ServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }

        public async Task<Response> Statistic(DateTime? fromDate, DateTime? toDate)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    var predicate = PredicateBuilder.New<WorkflowDocument>(true);

                    // 


                    if (fromDate.HasValue)
                    {
                        predicate.And(s => s.ExecutedTransition == fromDate.Value);

                    }
                    if (fromDate.HasValue)
                    {
                        predicate.And(s => s.ExecutedTransition == toDate.Value);

                    }
                    var data = await (from s in unitOfWork.GetRepository<WorkflowDocument>().GetAll().Where(predicate)
                                      group s by true into groupStatitic
                                      select new WorkflowDocumentStatistic(
                                          groupStatitic.Sum(x => x.IsDraft),
                                          groupStatitic.Sum(x => x.IsInProcess),
                                          groupStatitic.Sum(x => x.IsCompleted),
                                          groupStatitic.Sum(x => x.IsRejected),
                                          "")).FirstOrDefaultAsync();

                    return new ResponseObject<WorkflowDocumentStatistic>(data);

                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return new ResponseError(Code.ServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }

        public async Task<Response> StaticInRange(DateTime? fromDate, DateTime? toDate, DateRangeType typeRange) //
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    var predicate = PredicateBuilder.New<WorkflowDocument>(true);

                    // 


                    if (fromDate.HasValue)
                    {
                        predicate.And(s => s.ExecutedTransition == fromDate.Value);

                    }
                    if (fromDate.HasValue)
                    {
                        predicate.And(s => s.ExecutedTransition == toDate.Value);

                    }
                    switch (typeRange)
                    {
                        case DateRangeType.DAY:
                            {
                                var dataStatistic = await (from s in unitOfWork.GetRepository<WorkflowDocument>().GetAll().Where(predicate)
                                                           group s by s.DAY into groupStatitic
                                                           select new WorkflowDocumentStatistic(
                                                               groupStatitic.Sum(x => x.IsDraft),
                                                               groupStatitic.Sum(x => x.IsInProcess),
                                                               groupStatitic.Sum(x => x.IsCompleted),
                                                               groupStatitic.Sum(x => x.IsRejected),
                                                               groupStatitic.Key)).ToListAsync();

                                return new ResponseList<WorkflowDocumentStatistic>(dataStatistic);
                            }
                        case DateRangeType.WEEK:
                            {
                                var dataStatistic = await (from s in unitOfWork.GetRepository<WorkflowDocument>().GetAll().Where(predicate)
                                                           group s by s.WEEK into groupStatitic
                                                           select new WorkflowDocumentStatistic(
                                                               groupStatitic.Sum(x => x.IsDraft),
                                                               groupStatitic.Sum(x => x.IsInProcess),
                                                               groupStatitic.Sum(x => x.IsCompleted),
                                                               groupStatitic.Sum(x => x.IsRejected),
                                                               groupStatitic.Key)).ToListAsync();

                                return new ResponseList<WorkflowDocumentStatistic>(dataStatistic);
                            }
                        case DateRangeType.MONTH:
                            {
                                var dataStatistic = await (from s in unitOfWork.GetRepository<WorkflowDocument>().GetAll().Where(predicate)
                                                           group s by s.MONTH into groupStatitic
                                                           select new WorkflowDocumentStatistic(
                                                               groupStatitic.Sum(x => x.IsDraft),
                                                               groupStatitic.Sum(x => x.IsInProcess),
                                                               groupStatitic.Sum(x => x.IsCompleted),
                                                               groupStatitic.Sum(x => x.IsRejected),
                                                               groupStatitic.Key)).ToListAsync();
                                return new ResponseList<WorkflowDocumentStatistic>(dataStatistic);
                            }
                        case DateRangeType.YEAR:
                            {
                                var dataStatistic = await (from s in unitOfWork.GetRepository<WorkflowDocument>().GetAll().Where(predicate)
                                                           group s by s.YEAR into groupStatitic
                                                           select new WorkflowDocumentStatistic(
                                                               groupStatitic.Sum(x => x.IsDraft),
                                                               groupStatitic.Sum(x => x.IsInProcess),
                                                               groupStatitic.Sum(x => x.IsCompleted),
                                                               groupStatitic.Sum(x => x.IsRejected),
                                                               groupStatitic.Key)).ToListAsync();
                                return new ResponseList<WorkflowDocumentStatistic>(dataStatistic);
                            }
                        default:
                            var data = await (from s in unitOfWork.GetRepository<WorkflowDocument>().GetAll().Where(predicate)
                                              group s by s.Id into groupStatitic
                                              select new WorkflowDocumentStatistic(
                                                  groupStatitic.Sum(x => x.IsDraft),
                                                  groupStatitic.Sum(x => x.IsInProcess),
                                                  groupStatitic.Sum(x => x.IsCompleted),
                                                  groupStatitic.Sum(x => x.IsRejected),
                                                  groupStatitic.Key.ToString())).ToListAsync();
                            return new ResponseList<WorkflowDocumentStatistic>(data);
                    }

                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return new ResponseError(Code.ServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }

        public void UpdateShowSign(Guid documentId, int value = 0)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {

                    var current = unitOfWork.GetRepository<WorkflowDocument>().Find(documentId);
                    if (current == null)
                    {
                        Log.Error($"{documentId} not found");
                    }
                    else
                    {
                        current.ShowSign = value;

                        unitOfWork.GetRepository<WorkflowDocument>().Update(current);

                        unitOfWork.Save();
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
            }
        }
    }
}
