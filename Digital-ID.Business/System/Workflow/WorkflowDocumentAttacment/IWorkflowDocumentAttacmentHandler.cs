﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DigitalID.Data;

namespace DigitalID.Business
{
    public interface IWorkflowDocumentAttachmentHandler
    {
        Task<Response> FindAsync(Guid id);
        Task<Response> CreateAsync(WorkflowDocumentAttachmentCreateModel model, Guid applicationId, Guid userId);
        Task<Response> UpdateAsync(Guid id, WorkflowDocumentAttachmentUpdateModel model, Guid applicationId, Guid userId);
        Task<Response> DeleteAsync(Guid id);
        Task<Response> DeleteRangeAsync(IList<Guid> listId);
        Task<Response> GetAllAsync();
        Task<Response> GetAllAsync(WorkflowDocumentAttachmentQueryModel query);
        Task<Response> GetPageAsync(WorkflowDocumentAttachmentQueryModel query);
        Response GetAll();
    }
}