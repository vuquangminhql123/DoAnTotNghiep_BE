﻿using LinqKit;
using DigitalID.Data;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using DigitalID.Data.Models;

namespace DigitalID.Business
{
    public class WorkflowDocumentAttachmentHandler : IWorkflowDocumentAttachmentHandler
    {
        private readonly DbHandler<WorkflowDocumentAttachment, WorkflowDocumentAttachmentModel, WorkflowDocumentAttachmentQueryModel> _dbHandler = DbHandler<WorkflowDocumentAttachment, WorkflowDocumentAttachmentModel, WorkflowDocumentAttachmentQueryModel>.Instance;

        #region CRUD
        public Task<Response> FindAsync(Guid id)
        {
            return _dbHandler.FindAsync(id);
        }
        public async Task<Response> CreateAsync(WorkflowDocumentAttachmentCreateModel model, Guid applicationId, Guid userId)
        {

            WorkflowDocumentAttachment request = AutoMapperUtils.AutoMap<WorkflowDocumentAttachmentCreateModel, WorkflowDocumentAttachment>(model);
            request = request.InitCreate(applicationId, userId);
            request.Id = Guid.NewGuid();
            var result = await _dbHandler.CreateAsync(request);
            return result;
        }
        public async Task<Response> UpdateAsync(Guid id, WorkflowDocumentAttachmentUpdateModel model, Guid applicationId, Guid userId)
        {
            WorkflowDocumentAttachment request = AutoMapperUtils.AutoMap<WorkflowDocumentAttachmentUpdateModel, WorkflowDocumentAttachment>(model);
            request = request.InitUpdate(applicationId, userId);
            request.Id = id;
            var result = await _dbHandler.UpdateAsync(id, request);
            return result;
        }
        public async Task<Response> DeleteAsync(Guid id)
        {
            var result = await _dbHandler.DeleteAsync(id);
            return result;
        }
        public async Task<Response> DeleteRangeAsync(IList<Guid> listId)
        {
            var result = await _dbHandler.DeleteRangeAsync(listId, "Id");
            return result;
        }
        #endregion
        public Response GetAll()
        {
            return _dbHandler.GetAll();
        }
        public async Task<Response> GetAllAsync()
        {
            var predicate = PredicateBuilder.New<WorkflowDocumentAttachment>(true);
            return await _dbHandler.GetAllAsync(predicate);
        }
        public Task<Response> GetAllAsync(WorkflowDocumentAttachmentQueryModel query)
        {
            var predicate = BuildQuery(query);
            return _dbHandler.GetAllAsync(predicate, query.Sort);
        }
        public Task<Response> GetPageAsync(WorkflowDocumentAttachmentQueryModel query)
        {
            var predicate = BuildQuery(query);
            return _dbHandler.GetPageAsync(predicate, query);
        }
        private Expression<Func<WorkflowDocumentAttachment, bool>> BuildQuery(WorkflowDocumentAttachmentQueryModel query)
        {
            var predicate = PredicateBuilder.New<WorkflowDocumentAttachment>(true);
            if (query.WorkflowDocumentId.HasValue)
            {
                predicate.And(s => s.WorkflowDocumentId == query.WorkflowDocumentId.Value);
            }
            if (!string.IsNullOrEmpty(query.Extension))
            {
                predicate.And(s => s.Name == query.Extension);
            }
            if (query.ListId != null && query.ListId.Count > 0)
            {
                predicate.And(s => query.ListId.Contains(s.Id));
            }
            if (!string.IsNullOrEmpty(query.FullTextSearch))
            {
                predicate.And(s=>s.Name.ToLower().Contains(query.FullTextSearch.ToLower()) || s.Note.ToLower().Contains(query.FullTextSearch.ToLower()));
            }
            return predicate;
        }
    }
}
