﻿using DigitalID.Data;
using System;
namespace DigitalID.Business
{
    public class BaseWorkflowDocumentAttachmentModel : BaseModel
    {
        public Guid Id { get; set; }
        public Guid WorkflowDocumentId { get; set; }
        public string PhysicalName { get; set; }
        public string Name { get; set; }
        public long Size { get; set; }
        public string Extension { get; set; }
        public string PhysicalPath { get; set; }
        public string RelativePath { get; set; }
        public string Note { get; set; }
    }
    public class WorkflowDocumentAttachmentModel : BaseWorkflowDocumentAttachmentModel
    {
    }
    public class WorkflowDocumentAttachmentQueryModel : PaginationRequest
    {
        public Guid? WorkflowDocumentId { get; set; }
        public string Extension { get; set; }
    }

    public class WorkflowDocumentAttachmentCreateModel
    {
        public Guid? Id { get; set; }
        public Guid WorkflowDocumentId { get; set; }
        public string PhysicalName { get; set; }
        public string Name { get; set; }
        public long Size { get; set; }
        public string Extension { get; set; }
        public string PhysicalPath { get; set; }
        public string RelativePath { get; set; }
        public string Note { get; set; }
    }
    public class WorkflowDocumentAttachmentUpdateModel
    {
        public Guid? Id { get; set; }
        public Guid WorkflowDocumentId { get; set; }
        public string PhysicalName { get; set; }
        public string Name { get; set; }
        public long Size { get; set; }
        public string Extension { get; set; }
        public string PhysicalPath { get; set; }
        public string RelativePath { get; set; }
        public string Note { get; set; }
    }
}
