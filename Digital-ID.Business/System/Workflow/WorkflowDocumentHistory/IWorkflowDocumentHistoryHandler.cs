﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DigitalID.Data;

namespace DigitalID.Business
{
    public interface IWorkflowDocumentHistoryHandler
    {
        Task<Response> FindAsync(Guid id);
        Task<Response> CreateAsync(WorkflowDocumentHistoryCreateModel model, Guid applicationId, Guid userId);
        Task<Response> GetAllAsync();
        Task<Response> GetAllAsync(WorkflowDocumentHistoryQueryModel query);
        Task<Response> GetPageAsync(WorkflowDocumentHistoryQueryModel query);
        Response GetAll();
    }
}