﻿using LinqKit;
using DigitalID.Data;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Serilog;
using DigitalID.Data.Models;

namespace DigitalID.Business
{
    public class WorkflowDocumentHistoryHandler : IWorkflowDocumentHistoryHandler
    {
        private readonly DbHandler<WorkflowDocumentHistory, WorkflowDocumentHistoryModel, WorkflowDocumentHistoryQueryModel> _dbHandler = DbHandler<WorkflowDocumentHistory, WorkflowDocumentHistoryModel, WorkflowDocumentHistoryQueryModel>.Instance;

        #region CRUD
        public Task<Response> FindAsync(Guid id)
        {
            return _dbHandler.FindAsync(id);
        }
        public async Task<Response> CreateAsync(WorkflowDocumentHistoryCreateModel model, Guid applicationId, Guid userId)
        {
            WorkflowDocumentHistory request = AutoMapperUtils.AutoMap<WorkflowDocumentHistoryCreateModel, WorkflowDocumentHistory>(model);
            var result = await _dbHandler.CreateAsync(request);
            return result;
        }

        #endregion
        public Response GetAll()
        {
            return _dbHandler.GetAll();
        }
        public async Task<Response> GetAllAsync()
        {
            var predicate = PredicateBuilder.New<WorkflowDocumentHistory>(true);
            return await _dbHandler.GetAllAsync(predicate);
        }
        public Task<Response> GetAllAsync(WorkflowDocumentHistoryQueryModel query)
        {
            var predicate = BuildQuery(query);
            return _dbHandler.GetAllAsync(predicate,query.Sort);
        }
        public Task<Response> GetPageAsync(WorkflowDocumentHistoryQueryModel query)
        {
            var predicate = BuildQuery(query);
            return _dbHandler.GetPageAsync(predicate, query);
        }
        private Expression<Func<WorkflowDocumentHistory, bool>> BuildQuery(WorkflowDocumentHistoryQueryModel query)
        {
            var predicate = PredicateBuilder.New<WorkflowDocumentHistory>(true);
            if (query.WorkflowDocumentId.HasValue)
            {
                predicate.And(s => s.WorkflowDocumentId == query.WorkflowDocumentId.Value);
            }

            if (query.Id.HasValue)
            {
                predicate.And(s => s.Id == query.Id.Value);
            }

            if (!string.IsNullOrEmpty(query.Scheme))
            {
                predicate.And(s => s.Scheme == query.Scheme);
            }
            if (!string.IsNullOrEmpty(query.Status))
            {
                predicate.And(s => s.Status == query.Status);
            }
            if (!string.IsNullOrEmpty(query.PrevState))
            {
                predicate.And(s => s.PrevState == query.PrevState);
            }
            if (!string.IsNullOrEmpty(query.PrevActivity))
            {
                predicate.And(s => s.PrevActivity == query.PrevActivity);
            }
            if (!string.IsNullOrEmpty(query.State))
            {
                predicate.And(s => s.State == query.State);
            }


            if (!string.IsNullOrEmpty(query.Activity))
            {
                predicate.And(s => s.Activity == query.Activity);
            }

            if (query.ExecutedTransition.HasValue)
            {
                predicate.And(s => query.ExecutedTransition == query.ExecutedTransition.Value);
            }

            if (query.ExecutedIdentityId.HasValue)
            {
                predicate.And(s => s.ExecutedIdentityId == query.ExecutedIdentityId.Value);
            }

            if (!string.IsNullOrEmpty(query.ExecutedAction))
            {
                predicate.And(s => s.ExecutedAction == query.ExecutedAction);
            }
            if (!string.IsNullOrEmpty(query.ExecutedActionValue))
            {
                predicate.And(s => s.ExecutedActionValue == query.ExecutedActionValue);
            }
            if (query.ListId != null && query.ListId.Count > 0)
            {
                predicate.And(s => query.ListId.Contains(s.Id));
            }

            if (query.TransitionClassifier.HasValue)
            {
                predicate.And(s => query.TransitionClassifier == query.TransitionClassifier.Value);
            }

            return predicate;
        }
    }
}
