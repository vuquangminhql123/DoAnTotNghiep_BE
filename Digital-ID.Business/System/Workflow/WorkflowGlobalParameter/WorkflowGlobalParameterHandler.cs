﻿using LinqKit;
using DigitalID.Data;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using DigitalID.Data.Models;

namespace DigitalID.Business
{
    public class WorkflowGlobalParameterHandler : IWorkflowGlobalParameterHandler
    {
        private readonly DbHandler<WorkflowGlobalParameter, WorkflowGlobalParameterModel, WorkflowGlobalParameterQueryModel> _dbHandler = DbHandler<WorkflowGlobalParameter, WorkflowGlobalParameterModel, WorkflowGlobalParameterQueryModel>.Instance;

        #region CRUD
        public Task<Response> FindAsync(Guid id)
        {
            return _dbHandler.FindAsync(id);
        }
        public async Task<Response> CreateAsync(WorkflowGlobalParameterCreateModel model, Guid applicationId, Guid userId)
        {

            WorkflowGlobalParameter request = AutoMapperUtils.AutoMap<WorkflowGlobalParameterCreateModel, WorkflowGlobalParameter>(model);
            request.Id = Guid.NewGuid();
            var result = await _dbHandler.CreateAsync(request);
            return result;
        }
        public async Task<Response> UpdateAsync(Guid id, WorkflowGlobalParameterUpdateModel model, Guid applicationId, Guid userId)
        {
            WorkflowGlobalParameter request = AutoMapperUtils.AutoMap<WorkflowGlobalParameterUpdateModel, WorkflowGlobalParameter>(model);
            request.Id = id;
            var result = await _dbHandler.UpdateAsync(id, request);
            return result;
        }
        public async Task<Response> DeleteAsync(Guid id)
        {
            var result = await _dbHandler.DeleteAsync(id);
            return result;
        }
        public async Task<Response> DeleteRangeAsync(IList<Guid> listId)
        {
            var result = await _dbHandler.DeleteRangeAsync(listId, "Id");
            return result;
        }
        #endregion
        public Response GetAll()
        {
            return _dbHandler.GetAll();
        }
        public async Task<Response> GetAllAsync()
        {
            var predicate = PredicateBuilder.New<WorkflowGlobalParameter>(true);
            return await _dbHandler.GetAllAsync(predicate);
        }
        public Task<Response> GetAllAsync(WorkflowGlobalParameterQueryModel query)
        {
            var predicate = BuildQuery(query);
            return _dbHandler.GetAllAsync(predicate, query.Sort);
        }
        public Task<Response> GetPageAsync(WorkflowGlobalParameterQueryModel query)
        {
            var predicate = BuildQuery(query);
            return _dbHandler.GetPageAsync(predicate, query);
        }
        private Expression<Func<WorkflowGlobalParameter, bool>> BuildQuery(WorkflowGlobalParameterQueryModel query)
        {
            var predicate = PredicateBuilder.New<WorkflowGlobalParameter>(true);
            if (!string.IsNullOrEmpty(query.Type))
            {
                predicate.And(s => s.Type == query.Type);
            }
            if (!string.IsNullOrEmpty(query.Name))
            {
                predicate.And(s => s.Name == query.Name);
            }
            if (query.ListId != null && query.ListId.Count > 0)
            {
                predicate.And(s => query.ListId.Contains(s.Id));
            }
            if (!string.IsNullOrEmpty(query.FullTextSearch))
            {
                predicate.And(s => s.Name.ToLower().Contains(query.FullTextSearch.ToLower()));
            }
            return predicate;
        }
    }
}
