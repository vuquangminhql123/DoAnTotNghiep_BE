﻿using DigitalID.Data;
using System;
namespace DigitalID.Business
{
    public class BaseWorkflowGlobalParameterModel : BaseModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
        public string Type { get; set; }
    }
    public class WorkflowGlobalParameterModel : BaseWorkflowGlobalParameterModel
    {
    }
    public class WorkflowGlobalParameterQueryModel : PaginationRequest
    {
        public string Value { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
    }

    public class WorkflowGlobalParameterCreateModel
    {
        public string Name { get; set; }
        public string Value { get; set; }
        public string Type { get; set; }
        public string Description { get; set; }
    }
    public class WorkflowGlobalParameterUpdateModel
    {
        public string Name { get; set; }
        public string Value { get; set; }
        public string Type { get; set; }
        public string Description { get; set; }
    }
}

