﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DigitalID.Data;

namespace DigitalID.Business
{
    public interface IWorkflowInboxHandler
    {
        Task<Response> CreateAsync(WorkflowInboxCreateModel model, Guid applicationId, Guid userId);
        Task<Response> UpdateAsync(Guid id, WorkflowInboxUpdateModel model, Guid applicationId, Guid userId);
        Task<Response> GettAllByUserId(Guid applicationId, Guid userId);
        Task<Response> CreateManyAsync(Guid documentId, List<string> listIdentityId, Guid documentHistoryId);
    }
}