﻿using DigitalID.Data;
using System;
using System.Linq;
using System.Threading.Tasks;
using DigitalID.Data.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace DigitalID.Business
{
    public class WorkflowInboxHandler : IWorkflowInboxHandler
    {
        private readonly DbHandler<WorkflowInbox, WorkflowInboxModel, WorkflowInboxQueryModel> _dbHandler = DbHandler<WorkflowInbox, WorkflowInboxModel, WorkflowInboxQueryModel>.Instance;

        #region CRUD
        private readonly IWorkflowDocumentHistoryHandler _workflowDocumentHistoryHandler;

        public WorkflowInboxHandler(IWorkflowDocumentHistoryHandler workflowDocumentHistoryHandler)
        {
            _workflowDocumentHistoryHandler = workflowDocumentHistoryHandler;
        }
        public async Task<Response> CreateAsync(WorkflowInboxCreateModel model, Guid applicationId, Guid userId)
        {
            WorkflowInbox request = AutoMapperUtils.AutoMap<WorkflowInboxCreateModel, WorkflowInbox>(model);
            var result = await _dbHandler.CreateAsync(request);
            return result;
        }

        public async Task<Response> UpdateAsync(Guid id, WorkflowInboxUpdateModel model, Guid applicationId, Guid userId)
        {

            WorkflowInbox request = AutoMapperUtils.AutoMap<WorkflowInboxUpdateModel, WorkflowInbox>(model);
            request.Id = id;
            var result = await _dbHandler.UpdateAsync(id, request);

            return result;
        }

        #endregion


        public async Task<Response> CreateManyAsync(Guid documentId, List<string> listIdentityId, Guid documentHistoryId)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    var listModel = new List<WorkflowInbox>();
                    unitOfWork.GetRepository<WorkflowInbox>().DeleteRange(x => x.DocumentId == documentId);
                    foreach (var item in listIdentityId)
                    {
                        listModel.Add(new WorkflowInbox()
                        {
                            DocumentHistoryId = documentHistoryId,
                            Id = Guid.NewGuid(),
                            IdentityId = item,
                            DocumentId = documentId,
                            Status = 0
                        });
                    }

                    unitOfWork.GetRepository<WorkflowInbox>().AddRange(listModel);
                    if (await unitOfWork.SaveAsync() > 0)
                    {

                        return new ResponseObject<bool>(true);
                    }


                    return new ResponseError(Code.BadRequest, "Câu lệnh sql không thể thực thi");
                }
            }
            catch (Exception ex)
            {

                return new ResponseError(Code.ServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }
        public async Task<Response> GettAllByUserId(Guid applicationId, Guid userId)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    string userIdStr = userId.ToString();
                    var listId = await unitOfWork.GetRepository<WorkflowInbox>().GetAll().Where(x => x.IdentityId == userIdStr).Select(x => x.DocumentHistoryId).ToListAsync();
                    return await _workflowDocumentHistoryHandler.GetAllAsync(new WorkflowDocumentHistoryQueryModel()
                    {
                        ListId = listId
                    });


                }
            }
            catch (Exception ex)
            {
                return new ResponseError(Code.ServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }

    }
}
