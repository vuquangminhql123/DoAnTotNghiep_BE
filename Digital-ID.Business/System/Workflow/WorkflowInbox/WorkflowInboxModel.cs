﻿using DigitalID.Data;
using System;
using System.Collections.Generic;

namespace DigitalID.Business
{
    public class BaseWorkflowInboxModel : BaseModel
    {


        public Guid Id { get; set; }
        public Guid DocumentId { get; set; }
        public string IdentityId { get; set; }
        public Guid DocumentHistoryId { get; set; }
        public int Status { get; set; } = 0; // 0|| not seen, 1 || viewed
    }
    public class WorkflowInboxModel : BaseWorkflowInboxModel
    {

    }
    public class WorkflowInboxQueryModel : PaginationRequest
    {

    }
    public class WorkflowInboxCreateModel
    {

        public Guid Id { get; set; }
        public Guid DocumentId { get; set; }
        public string IdentityId { get; set; }
        public Guid DocumentHistoryId { get; set; }
        public int Status { get; set; } = 0; // 0|| not seen, 1 || viewed
    }
     
    public class WorkflowInboxUpdateModel
    {

        public Guid Id { get; set; }
        public Guid DocumentId { get; set; }
        public string IdentityId { get; set; }
        public Guid ProcessHistoryId { get; set; }
        public int Status { get; set; } = 0; // 0|| not seen, 1 || viewed
    }
}

