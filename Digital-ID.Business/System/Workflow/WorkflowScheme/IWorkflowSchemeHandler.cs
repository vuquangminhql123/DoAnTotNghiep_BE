﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DigitalID.Data;

namespace DigitalID.Business
{
    public interface IWorkflowSchemeHandler
    {
        Task<Response> GetAllAsync();
        Task<Response> DeleteAsync(string code);
        Task<Response> GetByIdAsync(string code);
        Task<Response> CreateAsync(string code);
        Task<List<ActorModel>> GetAllActor(string schemeCode);
        Task<ActorModel> GetActor(string schemeCode, string actorName);
    }
}