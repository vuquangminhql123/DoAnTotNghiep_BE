﻿using LinqKit;
using DigitalID.Data;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Serilog;
using System.Linq;
using DigitalID.Data.Models;
using System.Xml.Linq;

namespace DigitalID.Business
{
    public class WorkflowSchemeHandler : IWorkflowSchemeHandler
    {


        public async Task<Response> GetAllAsync()
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    var query = await unitOfWork.GetRepository<WorkflowScheme>().GetAll().Select(x => new WorkflowScheme() { Code = x.Code, Scheme = "" }).ToListAsync();


                    return new ResponseList<WorkflowScheme>(query);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return new ResponseError(Code.ServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }
        public async Task<Response> GetByIdAsync(string code)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    var current = await unitOfWork.GetRepository<WorkflowScheme>().FindAsync(x => x.Code == code);

                    return new ResponseObject<WorkflowScheme>(current);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return new ResponseError(Code.ServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }
        public async Task<Response> CreateAsync(string code)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    // check trùng
                    var current  = await unitOfWork.GetRepository<WorkflowScheme>().FindAsync(x=>x.Code == code);
                    if(current!=null){
                        return new ResponseError(Code.BadRequest,"Mã quy trình bị trùng");
                    }
                    var mode = new WorkflowScheme()
                    {
                        Code = code,
                        Scheme = ""
                    };
                    unitOfWork.GetRepository<WorkflowScheme>().Add(mode);
                    await unitOfWork.SaveAsync();
                    return new ResponseObject<WorkflowScheme>(mode);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return new ResponseError(Code.ServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }
        public async Task<Response> DeleteAsync(string code)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    var checkDoc = unitOfWork.GetRepository<WorkflowDocument>().Find(x => x.Scheme == code);
                    if (checkDoc != null)
                    {
                        return new ResponseError(Code.ServerError, "Không thể xóa quy trình đã có tài liệu");
                    }
                    else
                    {
                        var objsProcessScheme = unitOfWork.GetRepository<WorkflowProcessScheme>().GetMany(x => x.SchemeCode == code);
                        unitOfWork.GetRepository<WorkflowProcessScheme>().DeleteRange(objsProcessScheme);
                        var objsSchemeVersion = unitOfWork.GetRepository<WorkflowSchemeVersion>().GetMany(x => x.Code == code);
                        unitOfWork.GetRepository<WorkflowSchemeVersion>().DeleteRange(objsSchemeVersion);
                        unitOfWork.GetRepository<WorkflowScheme>().Delete(s => s.Code == code);
                        await unitOfWork.SaveAsync();
                        return new ResponseObject<bool>(true);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return new ResponseError(Code.ServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }

        public async Task<List<ActorModel>> GetAllActor(string schemeCode)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    var current = await unitOfWork.GetRepository<WorkflowScheme>().FindAsync(x => x.Code == schemeCode);
                    var xml = XDocument.Parse(current.Scheme);
                    var result = from c in xml.Root.Elements("Actors").FirstOrDefault().Elements("Actor").ToList()
                                 select new ActorModel()
                                 {
                                     Name = c.Attribute("Name").Value,
                                     Value = c.Attribute("Value").Value,
                                     Rule = c.Attribute("Rule").Value,
                                 };
                    return result.ToList();

                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return null;
            }
        }

        public async Task<ActorModel> GetActor(string schemeCode, string actorName)
        {
            var allActor = await GetAllActor(schemeCode);
            if (allActor != null)
            {
                return allActor.FirstOrDefault(x => x.Name == actorName);
            }
            return null;
        }
    }
}
