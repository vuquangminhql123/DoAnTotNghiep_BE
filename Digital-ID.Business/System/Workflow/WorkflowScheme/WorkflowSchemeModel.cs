using LinqKit;
using DigitalID.Data;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Serilog;
using System.Linq;
using DigitalID.Data.Models;

namespace DigitalID.Business
{
     public class ActorModel
     {
         public string Name { get; set; }
         public string Rule { get; set; }
         public string Value { get; set; }
     }
}
