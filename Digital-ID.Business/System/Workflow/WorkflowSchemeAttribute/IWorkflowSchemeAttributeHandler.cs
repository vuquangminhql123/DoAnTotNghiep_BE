﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DigitalID.Data;

namespace DigitalID.Business
{
    public interface IWorkflowSchemeAttributeHandler
    {
        Task<Response> FindAsync(Guid id);
        ResponseObject<WorkflowSchemeAttribute> FindByCode(string code);
        Task<Response> CreateAsync(WorkflowSchemeAttributeCreateModel model, Guid applicationId, Guid userId);
        Task<Response> UpdateAsync(Guid id, WorkflowSchemeAttributeUpdateModel model, Guid applicationId, Guid userId);
        Task<Response> DeleteAsync(Guid id);
        Task<Response> DeleteRangeAsync(IList<Guid> listId);
        Task<Response> GetAllAsync();
        Task<Response> GetAllAsync(WorkflowSchemeAttributeQueryModel query);
        Task<Response> GetPageAsync(WorkflowSchemeAttributeQueryModel query);
        Response GetAll();
    }
}