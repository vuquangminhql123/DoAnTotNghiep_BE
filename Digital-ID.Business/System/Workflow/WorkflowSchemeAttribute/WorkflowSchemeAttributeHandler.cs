﻿using LinqKit;
using DigitalID.Data;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace DigitalID.Business
{
    public class WorkflowSchemeAttributeHandler : IWorkflowSchemeAttributeHandler
    {
        private readonly DbHandler<WorkflowSchemeAttribute, WorkflowSchemeAttributeModel, WorkflowSchemeAttributeQueryModel> _dbHandler = DbHandler<WorkflowSchemeAttribute, WorkflowSchemeAttributeModel, WorkflowSchemeAttributeQueryModel>.Instance;

        private readonly IWorkflowSchemeHandler _workflowSchemeHandler;

        public WorkflowSchemeAttributeHandler(IWorkflowSchemeHandler workflowSchemeHandler)
        {
            _workflowSchemeHandler = workflowSchemeHandler;
        }
        #region CRUD
        public Task<Response> FindAsync(Guid id)
        {
            return _dbHandler.FindAsync(id);
        }

        public ResponseObject<WorkflowSchemeAttribute> FindByCode(string code)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    var data = unitOfWork.GetRepository<WorkflowSchemeAttribute>().Find(x => x.Code == code);
                    return new ResponseObject<WorkflowSchemeAttribute>(data, "Success", Code.Success);
                }
            }
            catch (Exception ex)
            {
                return new ResponseObject<WorkflowSchemeAttribute>(null, ex.Message, Code.ServerError);
            }
        }

        public async Task<Response> CreateAsync(WorkflowSchemeAttributeCreateModel model, Guid applicationId, Guid userId)
        {

            WorkflowSchemeAttribute request = AutoMapperUtils.AutoMap<WorkflowSchemeAttributeCreateModel, WorkflowSchemeAttribute>(model);
            request.ActiveVersion = 1;
            request = request.InitCreate(applicationId, userId);
            request.Id = Guid.NewGuid();
            var result = await _dbHandler.CreateAsync(request, "Code");
            return result;
        }
        public async Task<Response> UpdateAsync(Guid id, WorkflowSchemeAttributeUpdateModel model, Guid applicationId, Guid userId)
        {
            WorkflowSchemeAttribute request = AutoMapperUtils.AutoMap<WorkflowSchemeAttributeUpdateModel, WorkflowSchemeAttribute>(model);
            request = request.InitUpdate(applicationId, userId);
            request.Id = id;
            var result = await _dbHandler.UpdateAsync(id, request, "Code");
            return result;
        }
        public async Task<Response> DeleteAsync(Guid id)
        {
            var result = await _dbHandler.DeleteAsync(id);
            return result;
        }
        public async Task<Response> DeleteRangeAsync(IList<Guid> listId)
        {
            var result = await _dbHandler.DeleteRangeAsync(listId, "Id");
            return result;
        }
        #endregion
        public Response GetAll()
        {
            return _dbHandler.GetAll();
        }
        public async Task<Response> GetAllAsync()
        {
            var predicate = PredicateBuilder.New<WorkflowSchemeAttribute>(true);
            return await _dbHandler.GetAllAsync(predicate);
        }
        public Task<Response> GetAllAsync(WorkflowSchemeAttributeQueryModel query)
        {
            var predicate = BuildQuery(query);
            return _dbHandler.GetAllAsync(predicate, query.Sort);
        }
        public Task<Response> GetPageAsync(WorkflowSchemeAttributeQueryModel query)
        {
            var predicate = BuildQuery(query);
            return _dbHandler.GetPageAsync(predicate, query);
        }
        private Expression<Func<WorkflowSchemeAttribute, bool>> BuildQuery(WorkflowSchemeAttributeQueryModel query)
        {
            var predicate = PredicateBuilder.New<WorkflowSchemeAttribute>(true);
            if (!string.IsNullOrEmpty(query.Code))
            {
                predicate.And(s => s.Code == query.Code);
            }

            if (query.ListId != null && query.ListId.Count > 0)
            {
                predicate.And(s => query.ListId.Contains(s.Id));
            }

            if (!string.IsNullOrEmpty(query.FullTextSearch))
                predicate.And(s =>
                    s.Code.ToLower().Contains(query.FullTextSearch.ToLower()) ||
                    s.Description.ToLower().Contains(query.FullTextSearch.ToLower()));
            return predicate;
        }
    }
}
