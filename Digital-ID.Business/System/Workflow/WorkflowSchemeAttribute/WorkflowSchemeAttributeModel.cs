﻿using DigitalID.Data;
using System;
using System.Collections.Generic;

namespace DigitalID.Business
{
    public class BaseWorkflowSchemeAttributeModel : BaseModel
    {
        
        public Guid Id { get; set; }
        public string Code { get; set; } // == code scheme
        public string Description { get; set; }
        public bool Status { get; set; }
        public int Order { get; set; }
        public int ActiveVersion { get; set; }
    }
    public class WorkflowSchemeAttributeModel : BaseWorkflowSchemeAttributeModel
    {
    }
    public class WorkflowSchemeAttributeQueryModel : PaginationRequest
    {
        public string Code { get; set; }
        public bool? Status { get; set; }
        public int ActiveVersion { get; set; }

    }

    public class WorkflowSchemeAttributeCreateModel
    {
   
        public string Code { get; set; } // == code scheme
        public string Description { get; set; }
        public bool Status { get; set; }
        public int Order { get; set; }
        public int ActiveVersion { get; set; }
    }
    public class WorkflowSchemeAttributeUpdateModel
    {

   
        public string Code { get; set; } // == code scheme
        public string Description { get; set; }
        public bool Status { get; set; }
        public int Order { get; set; }
        public int ActiveVersion { get; set; }
    }
}

