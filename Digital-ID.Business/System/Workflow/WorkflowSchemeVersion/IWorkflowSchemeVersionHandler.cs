﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DigitalID.Data;

namespace DigitalID.Business
{
    public interface IWorkflowSchemeVersionHandler
    {
        Task<Response> FindAsync(Guid id);
        Task<Response> CreateAsync(WorkflowSchemeVersionCreateModel model, Guid applicationId, Guid userId);
        Task<Response> UpdateAsync(Guid id, WorkflowSchemeVersionUpdateModel model, Guid applicationId, Guid userId);
        Task<Response> DeleteAsync(Guid id);
        Task<Response> DeleteRangeAsync(IList<Guid> listId);
        Task<Response> GetAllAsync();
        Task<Response> GetAllAsync(WorkflowSchemeVersionQueryModel query);
        Task<Response> GetPageAsync(WorkflowSchemeVersionQueryModel query);
        Response GetAll();
    }
}