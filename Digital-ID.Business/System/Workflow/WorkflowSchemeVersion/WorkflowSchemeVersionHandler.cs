﻿using LinqKit;
using DigitalID.Data;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using DigitalID.Data.Models;

namespace DigitalID.Business
{
    public class WorkflowSchemeVersionHandler : IWorkflowSchemeVersionHandler
    {
        private readonly DbHandler<WorkflowSchemeVersion, WorkflowSchemeVersionModel, WorkflowSchemeVersionQueryModel> _dbHandler = DbHandler<WorkflowSchemeVersion, WorkflowSchemeVersionModel, WorkflowSchemeVersionQueryModel>.Instance;

        private readonly IWorkflowSchemeHandler _workflowSchemeHandler;

        public WorkflowSchemeVersionHandler(IWorkflowSchemeHandler workflowSchemeHandler)
        {
            _workflowSchemeHandler = workflowSchemeHandler;
        }
        #region CRUD
        public Task<Response> FindAsync(Guid id)
        {
            return _dbHandler.FindAsync(id);
        }
        public async Task<Response> CreateAsync(WorkflowSchemeVersionCreateModel model, Guid applicationId, Guid userId)
        {

            WorkflowSchemeVersion request = AutoMapperUtils.AutoMap<WorkflowSchemeVersionCreateModel, WorkflowSchemeVersion>(model);
            request = request.InitCreate(applicationId, userId);
            request.Id = Guid.NewGuid();
            var result = await _dbHandler.CreateAsync(request);
            return result;
        }
        public async Task<Response> UpdateAsync(Guid id, WorkflowSchemeVersionUpdateModel model, Guid applicationId, Guid userId)
        {
            WorkflowSchemeVersion request = AutoMapperUtils.AutoMap<WorkflowSchemeVersionUpdateModel, WorkflowSchemeVersion>(model);
            request = request.InitUpdate(applicationId, userId);
            request.Id = id;
            var result = await _dbHandler.UpdateAsync(id, request);
            return result;
        }
        public async Task<Response> DeleteAsync(Guid id)
        {
            //var result = await _dbHandler.DeleteAsync(id);
            //return result;
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    var wfVersion = unitOfWork.GetRepository<WorkflowSchemeVersion>().Find(x => x.Id == id);
                    if (wfVersion != null)
                    {
                        string code = wfVersion.Code + "_" + wfVersion.Version;
                        unitOfWork.GetRepository<WorkflowScheme>().Delete(s => s.Code == code);
                        var objsProcessScheme = unitOfWork.GetRepository<WorkflowProcessScheme>().GetMany(x => x.SchemeCode == code);
                        unitOfWork.GetRepository<WorkflowProcessScheme>().DeleteRange(objsProcessScheme);
                        unitOfWork.GetRepository<WorkflowSchemeVersion>().Delete(wfVersion);
                        await unitOfWork.SaveAsync();
                        return new ResponseObject<bool>(true);
                    }
                    else
                    {
                        return new ResponseError(Code.NotFound, "Không tìm thấy dữ liệu cần xóa");
                    }
                }
            }
            catch (Exception ex)
            {
                return new ResponseError(Code.ServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }
        public async Task<Response> DeleteRangeAsync(IList<Guid> listId)
        {
            var result = await _dbHandler.DeleteRangeAsync(listId, "Id");
            return result;
        }
        #endregion
        public Response GetAll()
        {
            return _dbHandler.GetAll();
        }
        public async Task<Response> GetAllAsync()
        {
            var predicate = PredicateBuilder.New<WorkflowSchemeVersion>(true);
            return await _dbHandler.GetAllAsync(predicate);
        }
        public Task<Response> GetAllAsync(WorkflowSchemeVersionQueryModel query)
        {
            var predicate = BuildQuery(query);
            return _dbHandler.GetAllAsync(predicate, query.Sort);
        }
        public Task<Response> GetPageAsync(WorkflowSchemeVersionQueryModel query)
        {
            var predicate = BuildQuery(query);
            return _dbHandler.GetPageAsync(predicate, query);
        }
        private Expression<Func<WorkflowSchemeVersion, bool>> BuildQuery(WorkflowSchemeVersionQueryModel query)
        {
            var predicate = PredicateBuilder.New<WorkflowSchemeVersion>(true);
            if (!string.IsNullOrEmpty(query.Code))
            {
                predicate.And(s => s.Code == query.Code);
            }

            if (query.ListId != null && query.ListId.Count > 0)
            {
                predicate.And(s => query.ListId.Contains(s.Id));
            }

            if (!string.IsNullOrEmpty(query.FullTextSearch))
                predicate.And(s =>
                    s.Code.ToLower().Contains(query.FullTextSearch.ToLower()));
            return predicate;
        }
    }
}
