﻿using DigitalID.Data;
using System;
using System.Collections.Generic;

namespace DigitalID.Business
{
    public class BaseWorkflowSchemeVersionModel : BaseModel
    {
        public Guid Id { get; set; }
        public string Code { get; set; } //Mã quy trình cha
        public int Version { get; set; } // Phiên bản 
        public string Scheme { get; set; }
        public bool isActive { get; set; }
    }
    public class WorkflowSchemeVersionModel : BaseWorkflowSchemeVersionModel
    {
    }
    public class WorkflowSchemeVersionQueryModel : PaginationRequest
    {
        public string Code { get; set; }
        public bool? Status { get; set; }

    }

    public class WorkflowSchemeVersionCreateModel
    {
        public string Code { get; set; } //Mã quy trình cha
        public int Version { get; set; } // Phiên bản 
        public string Scheme { get; set; }
        public bool isActive { get; set; }
    }
    public class WorkflowSchemeVersionUpdateModel
    {

        public string Code { get; set; } //Mã quy trình cha
        public int Version { get; set; } // Phiên bản 
        public string Scheme { get; set; }
        public bool isActive { get; set; }
    }
}

