﻿using DigitalID.Data;
using Microsoft.EntityFrameworkCore;
using QRCoder;
using Serilog;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using ZXing;

namespace DigitalID.Business
{
    public class CheckinAccountHandler : ICheckinAccountHandler
    {
        #region Message
        #endregion

        private const string CachePrefix = "LogCheckinAccount";
        private const string SelectItemCacheSubfix = "list-select";
        private readonly DataContext _dataContext;

        public CheckinAccountHandler(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public async Task<Response> Create(CheckinAccountCreateModel model)
        {
            try
            {
                var entity = AutoMapperUtils.AutoMap<CheckinAccountCreateModel, LogCheckinAccount>(model);

                var acc = await _dataContext.CrmAccount.FindAsync(entity.AccountId);

                if (acc == null)
                {
                    return new ResponseError(Code.BadRequest, "Không tìm thấy tài khoản, vui lòng xác thực lại!");
                }

                entity.CreatedDate = DateTime.Now;

                entity.Id = Guid.NewGuid();
                await _dataContext.LogCheckinAccount.AddAsync(entity);

                int dbSave = await _dataContext.SaveChangesAsync();


                if (dbSave > 0)
                {
                    Log.Information("Add success: " + JsonSerializer.Serialize(entity));
                    InvalidCache();

                    return new ResponseObject<Guid>(entity.Id, $"Check in thành công. Xin chào <b>{acc.FullName}</b>", Code.Success);
                }
                else
                {
                    return new ResponseError(Code.ServerError, MessageConstants.CreateErrorMessage);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, MessageConstants.ErrorLogMessage);
                return new ResponseError(Code.ServerError, $"{MessageConstants.CreateErrorMessage} - {ex.Message}");
            }
        }

        public async Task<Response> Filter(CheckinAccountQueryFilter filter)
        {
            try
            {
                var data = (from checkin in _dataContext.LogCheckinAccount

                            join account in _dataContext.CrmAccount on checkin.AccountId equals account.Id
                            join user in _dataContext.IdmUser on checkin.CreatedUserId equals user.Id
                            select new CheckinAccountBaseModel()
                            {
                                Id = checkin.Id,
                                //AccountId = account.Id,
                                AccountName = account.FullName,
                                //CreatedByUserId = checkin.CreatedUserId,
                                UserName = user.Name,
                                CreatedDate = checkin.CreatedDate
                            });

                if (!string.IsNullOrEmpty(filter.TextSearch))
                {
                    string ts = filter.TextSearch.Trim().ToLower();
                    data = data.Where(x => x.AccountName.ToLower().Contains(ts) || x.UserName.ToLower().Contains(ts));
                }

                data = data.OrderByField(filter.PropertyName, filter.Ascending);

                int totalCount = data.Count();

                // Pagination
                if (filter.PageSize.HasValue && filter.PageNumber.HasValue)
                {
                    if (filter.PageSize <= 0)
                    {
                        filter.PageSize = QueryFilter.DefaultPageSize;
                    }

                    //Calculate nunber of rows to skip on pagesize
                    int excludedRows = (filter.PageNumber.Value - 1) * (filter.PageSize.Value);
                    if (excludedRows <= 0)
                    {
                        excludedRows = 0;
                    }

                    // Query
                    data = data.Skip(excludedRows).Take(filter.PageSize.Value);
                }
                int dataCount = data.Count();

                var listResult = await data.ToListAsync();
                return new ResponseObject<PaginationList<CheckinAccountBaseModel>>(new PaginationList<CheckinAccountBaseModel>()
                {
                    DataCount = dataCount,
                    TotalCount = totalCount,
                    PageNumber = filter.PageNumber ?? 0,
                    PageSize = filter.PageSize ?? 0,
                    Data = listResult
                }, MessageConstants.GetDataSuccessMessage, Code.Success);
            }
            catch (Exception ex)
            {
                Log.Error(ex, MessageConstants.ErrorLogMessage);
                return new ResponseError(Code.ServerError, $"{MessageConstants.GetDataErrorMessage} - {ex.Message}");
            }
        }

        private void InvalidCache(string id = "")
        {
            //if (!string.IsNullOrEmpty(id))
            //{
            //    string cacheKey = BuildCacheKey(id);
            //    _cacheService.Remove(cacheKey);
            //}

            //string selectItemCacheKey = BuildCacheKey(SelectItemCacheSubfix);
            //_cacheService.Remove(selectItemCacheKey);
        }

        private string BuildCacheKey(string id)
        {
            return $"{CachePrefix}-{id}";
        }
    }
}