﻿using DigitalID.Data;
using System;

namespace DigitalID.Business
{
    public class CheckinAccountBaseModel
    {
        public Guid Id { get; set; }
        public Guid AccountId { get; set; }
        public string AccountName { get; set; }
        public string UserName { get; set; }
        //public Guid? CreatedByUserId { get; set; }
        public DateTime? CreatedDate { get; set; }
    }

    public class CheckinAccountModel : CheckinAccountBaseModel
    {

    }

    public class CheckinAccountDetailModel : CheckinAccountModel
    {
        public long IdentityNumber { get; set; }
        public int Order { get; set; }
        public string Description { get; set; }
        //public Guid? CreatedByUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Guid? ModifiedUserId { get; set; }
        public Guid? ApplicationId { get; set; }
        public Guid? CompanyId { get; set; }
    }

    public class CheckinAccountCreateModel : CheckinAccountModel
    {
        public Guid? CreatedByUserId { get; set; }
        public Guid? ApplicationId { get; set; }
    }

    public class CheckinAccountUpdateModel : CheckinAccountModel
    {
        public Guid ModifiedUserId { get; set; }

        public void UpdateToEntity(LogCheckinAccount entity)
        {
        }
    }

    public class CheckinAccountQueryFilter
    {
        public string TextSearch { get; set; }
        public int? PageSize { get; set; }
        public int? PageNumber { get; set; }
        public bool? Status { get; set; }
        public string PropertyName { get; set; } = "CreatedDate";
        //asc - desc
        public string Ascending { get; set; } = "desc";
        public CheckinAccountQueryFilter()
        {
            PageNumber = QueryFilter.DefaultPageNumber;
            PageSize = QueryFilter.DefaultPageSize;
        }
    }
}