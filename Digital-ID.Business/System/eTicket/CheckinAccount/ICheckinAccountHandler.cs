﻿using DigitalID.Data;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DigitalID.Business
{
    /// <summary>
    /// Interface lịch sử checking lịch sử checkin
    /// </summary>
    public interface ICheckinAccountHandler
    {
        /// <summary>
        /// Thêm mới lịch sử checkin
        /// </summary>
        /// <param name="model">Model thêm mới lịch sử checkin</param>
        /// <returns>Id lịch sử checkin</returns>
        Task<Response> Create(CheckinAccountCreateModel model);

        /// <summary>
        /// Lấy danh sách lịch sử checkin theo điều kiện lọc
        /// </summary>
        /// <param name="filter">Model điều kiện lọc</param>
        /// <returns>Danh sách lịch sử checkin</returns>
        Task<Response> Filter(CheckinAccountQueryFilter filter);
    }
}
