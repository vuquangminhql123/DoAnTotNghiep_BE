﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DigitalID.Data.Migrations
{
    public partial class initdb : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "bsd_log",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    message = table.Column<string>(nullable: true),
                    level = table.Column<byte>(nullable: true),
                    timestamp = table.Column<DateTime>(nullable: false),
                    exception = table.Column<string>(nullable: true),
                    application_id = table.Column<Guid>(nullable: true),
                    user_id = table.Column<Guid>(nullable: true),
                    user_name = table.Column<string>(nullable: true),
                    action = table.Column<string>(nullable: true),
                    module = table.Column<string>(nullable: true),
                    message_template = table.Column<string>(nullable: true),
                    properties = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_bsd_log", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "bsd_metadata_field",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    name = table.Column<string>(maxLength: 512, nullable: false),
                    data_type = table.Column<string>(maxLength: 50, nullable: true),
                    formly_content = table.Column<string>(nullable: false),
                    description = table.Column<string>(nullable: true),
                    created_by_user_id = table.Column<Guid>(nullable: false),
                    created_on_date = table.Column<DateTime>(type: "datetime", nullable: false),
                    last_modified_by_user_id = table.Column<Guid>(nullable: false),
                    last_modified_on_date = table.Column<DateTime>(type: "datetime", nullable: false),
                    application_id = table.Column<Guid>(nullable: true),
                    code = table.Column<string>(maxLength: 256, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_bsd_metadata_field", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "bsd_metadata_template",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    code = table.Column<string>(maxLength: 512, nullable: false),
                    name = table.Column<string>(maxLength: 512, nullable: false),
                    description = table.Column<string>(nullable: true),
                    created_by_user_id = table.Column<Guid>(nullable: false),
                    created_on_date = table.Column<DateTime>(type: "datetime", nullable: false),
                    last_modified_by_user_id = table.Column<Guid>(nullable: false),
                    last_modified_on_date = table.Column<DateTime>(type: "datetime", nullable: false),
                    application_id = table.Column<Guid>(nullable: true),
                    form_config = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_bsd_metadata_template", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "bsd_taxonomy_vocabulary",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    vocabulary_type_id = table.Column<Guid>(nullable: false),
                    name = table.Column<string>(maxLength: 256, nullable: false),
                    description = table.Column<string>(nullable: true),
                    weight = table.Column<int>(nullable: false),
                    created_by_user_id = table.Column<Guid>(nullable: true),
                    created_on_date = table.Column<DateTime>(type: "datetime", nullable: true),
                    last_modified_by_user_id = table.Column<Guid>(nullable: true),
                    last_modified_on_date = table.Column<DateTime>(type: "datetime", nullable: true),
                    is_system = table.Column<bool>(nullable: false),
                    application_id = table.Column<Guid>(nullable: false),
                    code = table.Column<string>(maxLength: 256, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_bsd_taxonomy_vocabulary", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "bsd_taxonomy_vocabulary_types",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    vocabulary_type = table.Column<string>(maxLength: 64, nullable: false),
                    application_id = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_bsd_taxonomy_vocabulary_types", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Cart",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    application_id = table.Column<Guid>(nullable: true),
                    company_id = table.Column<Guid>(nullable: true),
                    created_user_id = table.Column<Guid>(nullable: true),
                    modified_user_id = table.Column<Guid>(nullable: true),
                    identity_number = table.Column<long>(nullable: false),
                    status_default = table.Column<bool>(nullable: false),
                    order = table.Column<int>(nullable: false),
                    description = table.Column<string>(nullable: true),
                    Code = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedDate = table.Column<DateTime>(nullable: false),
                    UserId = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    ListProducts = table.Column<string>(nullable: true),
                    Status = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Cart", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Category",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    application_id = table.Column<Guid>(nullable: true),
                    company_id = table.Column<Guid>(nullable: true),
                    created_user_id = table.Column<Guid>(nullable: true),
                    modified_user_id = table.Column<Guid>(nullable: true),
                    identity_number = table.Column<long>(nullable: false),
                    status_default = table.Column<bool>(nullable: false),
                    order = table.Column<int>(nullable: false),
                    description = table.Column<string>(nullable: true),
                    Code = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Status = table.Column<bool>(nullable: true),
                    Content = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedDate = table.Column<DateTime>(nullable: false),
                    ParentId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Category", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Category_Category_ParentId",
                        column: x => x.ParentId,
                        principalTable: "Category",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Category_Meta",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    application_id = table.Column<Guid>(nullable: true),
                    company_id = table.Column<Guid>(nullable: true),
                    created_user_id = table.Column<Guid>(nullable: true),
                    modified_user_id = table.Column<Guid>(nullable: true),
                    identity_number = table.Column<long>(nullable: false),
                    status_default = table.Column<bool>(nullable: false),
                    order = table.Column<int>(nullable: false),
                    description = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    Code = table.Column<string>(nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: false),
                    Key = table.Column<string>(nullable: true),
                    Status = table.Column<bool>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Category_Meta", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Cities",
                columns: table => new
                {
                    matp = table.Column<string>(nullable: false),
                    name = table.Column<string>(nullable: true),
                    type = table.Column<string>(nullable: true),
                    slug = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Cities", x => x.matp);
                });

            migrationBuilder.CreateTable(
                name: "Communes",
                columns: table => new
                {
                    xaid = table.Column<string>(nullable: false),
                    name = table.Column<string>(nullable: true),
                    type = table.Column<string>(nullable: true),
                    maqh = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Communes", x => x.xaid);
                });

            migrationBuilder.CreateTable(
                name: "crm_account",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    application_id = table.Column<Guid>(nullable: true),
                    company_id = table.Column<Guid>(nullable: true),
                    created_date = table.Column<DateTime>(nullable: true),
                    created_user_id = table.Column<Guid>(nullable: true),
                    modified_date = table.Column<DateTime>(nullable: true),
                    modified_user_id = table.Column<Guid>(nullable: true),
                    identity_number = table.Column<long>(nullable: false),
                    status_default = table.Column<bool>(nullable: false),
                    order = table.Column<int>(nullable: false),
                    description = table.Column<string>(nullable: true),
                    code = table.Column<string>(maxLength: 256, nullable: true),
                    full_name = table.Column<string>(maxLength: 256, nullable: false),
                    first_name = table.Column<string>(maxLength: 256, nullable: true),
                    last_name = table.Column<string>(maxLength: 256, nullable: true),
                    phone_number = table.Column<string>(maxLength: 20, nullable: true),
                    email = table.Column<string>(maxLength: 256, nullable: true),
                    organization_name = table.Column<string>(maxLength: 256, nullable: true),
                    position = table.Column<string>(maxLength: 256, nullable: true),
                    birthday = table.Column<DateTime>(type: "date", nullable: true),
                    avatar_url = table.Column<string>(nullable: true),
                    address = table.Column<string>(nullable: true),
                    infomation_channel = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_crm_account", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Districts",
                columns: table => new
                {
                    maqh = table.Column<string>(nullable: false),
                    name = table.Column<string>(nullable: true),
                    type = table.Column<string>(nullable: true),
                    matp = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Districts", x => x.maqh);
                });

            migrationBuilder.CreateTable(
                name: "idm_api",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    last_modified_on_date = table.Column<DateTime>(nullable: true),
                    created_on_date = table.Column<DateTime>(nullable: true),
                    company_id = table.Column<Guid>(nullable: true),
                    name = table.Column<string>(maxLength: 512, nullable: false),
                    api_url = table.Column<string>(maxLength: 512, nullable: false),
                    http_method = table.Column<string>(maxLength: 20, nullable: false),
                    allow_all_user = table.Column<bool>(nullable: false),
                    status = table.Column<bool>(nullable: false),
                    description = table.Column<string>(maxLength: 1024, nullable: true),
                    group_api = table.Column<string>(maxLength: 100, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_idm_api", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "idm_client",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    client_secret = table.Column<string>(nullable: false),
                    auto_approve = table.Column<bool>(nullable: false),
                    user_id = table.Column<Guid>(nullable: false),
                    redirect_uri = table.Column<string>(nullable: true),
                    grant_type = table.Column<string>(nullable: true),
                    is_active = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_idm_client", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "idm_otp",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    application_id = table.Column<Guid>(nullable: true),
                    company_id = table.Column<Guid>(nullable: true),
                    created_date = table.Column<DateTime>(nullable: true),
                    created_user_id = table.Column<Guid>(nullable: true),
                    modified_date = table.Column<DateTime>(nullable: true),
                    modified_user_id = table.Column<Guid>(nullable: true),
                    user_id = table.Column<Guid>(nullable: true),
                    username = table.Column<string>(nullable: true),
                    relying_party_id = table.Column<string>(nullable: true),
                    period_time = table.Column<int>(nullable: false),
                    expiry_date = table.Column<DateTime>(nullable: false),
                    otp = table.Column<string>(nullable: true),
                    otp_create_date = table.Column<string>(nullable: true),
                    send_to = table.Column<string>(nullable: true),
                    type = table.Column<string>(nullable: true),
                    purpose = table.Column<string>(nullable: true),
                    transaction_id = table.Column<string>(nullable: true),
                    transaction_type = table.Column<string>(nullable: true),
                    sec_token = table.Column<string>(nullable: true),
                    api_link = table.Column<string>(nullable: true),
                    status = table.Column<int>(nullable: false),
                    hmac = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_idm_otp", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "idm_qrcode",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    application_id = table.Column<Guid>(nullable: true),
                    company_id = table.Column<Guid>(nullable: true),
                    created_date = table.Column<DateTime>(nullable: true),
                    created_user_id = table.Column<Guid>(nullable: true),
                    modified_date = table.Column<DateTime>(nullable: true),
                    modified_user_id = table.Column<Guid>(nullable: true),
                    user_id = table.Column<Guid>(nullable: true),
                    username = table.Column<string>(nullable: true),
                    qrcode = table.Column<string>(nullable: true),
                    qrcode_create_date = table.Column<DateTime>(nullable: false),
                    period_time = table.Column<int>(nullable: false),
                    expiry_date = table.Column<DateTime>(nullable: false),
                    purpose = table.Column<string>(nullable: true),
                    transaction_id = table.Column<string>(nullable: true),
                    transaction_type = table.Column<string>(nullable: true),
                    sec_token = table.Column<string>(nullable: true),
                    api_link = table.Column<string>(nullable: true),
                    status = table.Column<int>(nullable: false),
                    hmac = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_idm_qrcode", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "idm_right",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    last_modified_on_date = table.Column<DateTime>(nullable: true),
                    created_on_date = table.Column<DateTime>(nullable: true),
                    company_id = table.Column<Guid>(nullable: true),
                    code = table.Column<string>(maxLength: 64, nullable: false),
                    name = table.Column<string>(maxLength: 128, nullable: false),
                    description = table.Column<string>(maxLength: 1024, nullable: true),
                    group_code = table.Column<string>(maxLength: 64, nullable: true),
                    status = table.Column<bool>(nullable: false),
                    order = table.Column<int>(nullable: false),
                    is_system = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_idm_right", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "idm_role",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    last_modified_on_date = table.Column<DateTime>(nullable: true),
                    created_on_date = table.Column<DateTime>(nullable: true),
                    company_id = table.Column<Guid>(nullable: true),
                    name = table.Column<string>(maxLength: 128, nullable: false),
                    code = table.Column<string>(maxLength: 64, nullable: false),
                    status = table.Column<bool>(nullable: false),
                    is_digital_signed = table.Column<bool>(nullable: false),
                    order = table.Column<int>(nullable: false),
                    description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_idm_role", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "idm_user",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    last_modified_on_date = table.Column<DateTime>(nullable: true),
                    created_on_date = table.Column<DateTime>(nullable: true),
                    company_id = table.Column<Guid>(nullable: true),
                    user_name = table.Column<string>(maxLength: 128, nullable: false),
                    name = table.Column<string>(maxLength: 128, nullable: false),
                    phone_number = table.Column<string>(maxLength: 256, nullable: true),
                    email = table.Column<string>(maxLength: 256, nullable: true),
                    avatar_url = table.Column<string>(maxLength: 1024, nullable: true),
                    type = table.Column<int>(nullable: true),
                    password = table.Column<string>(maxLength: 1024, nullable: true),
                    password_salt = table.Column<string>(maxLength: 1024, nullable: true),
                    birth_date = table.Column<DateTime>(nullable: true),
                    last_activity_date = table.Column<DateTime>(nullable: false),
                    is_locked = table.Column<bool>(nullable: false),
                    is_admin = table.Column<bool>(nullable: false),
                    position_id = table.Column<Guid>(nullable: true),
                    position_name = table.Column<string>(nullable: true),
                    company_name = table.Column<string>(nullable: true),
                    security_question = table.Column<string>(nullable: true),
                    security_question_answer = table.Column<string>(nullable: true),
                    sign_type_full = table.Column<int>(nullable: false),
                    sign_full_path = table.Column<string>(nullable: true),
                    sign_final_path = table.Column<string>(nullable: true),
                    sign_type_short = table.Column<int>(nullable: false),
                    sign_short_path = table.Column<string>(nullable: true),
                    logo_path = table.Column<string>(nullable: true),
                    sign_display = table.Column<int>(nullable: false),
                    sign_by = table.Column<int>(nullable: false),
                    ca = table.Column<string>(maxLength: 256, nullable: true),
                    cert_status = table.Column<int>(nullable: false),
                    res_partner = table.Column<Guid>(nullable: true),
                    is_otp_mobile = table.Column<bool>(nullable: false),
                    is_otp_email = table.Column<bool>(nullable: false),
                    is_otp_face = table.Column<bool>(nullable: false),
                    is_otp_sms = table.Column<bool>(nullable: false),
                    is_otp_soft_token = table.Column<bool>(nullable: false),
                    is_face = table.Column<bool>(nullable: false),
                    is_fingerprint = table.Column<bool>(nullable: false),
                    is_kms = table.Column<bool>(nullable: false),
                    is_smart_card = table.Column<bool>(nullable: false),
                    is_fido_uaf = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_idm_user", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "log_authentication",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    application_id = table.Column<Guid>(nullable: true),
                    company_id = table.Column<Guid>(nullable: true),
                    created_date = table.Column<DateTime>(nullable: true),
                    created_user_id = table.Column<Guid>(nullable: true),
                    modified_date = table.Column<DateTime>(nullable: true),
                    modified_user_id = table.Column<Guid>(nullable: true),
                    user_id = table.Column<Guid>(nullable: false),
                    username = table.Column<string>(nullable: true),
                    certificattion_id = table.Column<Guid>(nullable: false),
                    request_id = table.Column<string>(nullable: true),
                    relying_party_id = table.Column<string>(nullable: true),
                    relying_party_ip = table.Column<string>(nullable: true),
                    request = table.Column<string>(type: "text", nullable: true),
                    location = table.Column<string>(nullable: true),
                    request_time = table.Column<DateTime>(nullable: false),
                    response_time = table.Column<DateTime>(nullable: false),
                    response_status = table.Column<string>(nullable: true),
                    reponse_token_device = table.Column<string>(nullable: true),
                    response = table.Column<string>(nullable: true),
                    transaction_id = table.Column<string>(nullable: true),
                    transaction_type = table.Column<string>(nullable: true),
                    is_otp_email = table.Column<bool>(nullable: false),
                    is_otp_sms = table.Column<bool>(nullable: false),
                    is_otp_soft_token = table.Column<bool>(nullable: false),
                    is_face = table.Column<bool>(nullable: false),
                    is_fingerprint = table.Column<bool>(nullable: false),
                    is_kms = table.Column<bool>(nullable: false),
                    is_smart_card = table.Column<bool>(nullable: false),
                    is_fido_uaf = table.Column<bool>(nullable: false),
                    relying_party_ssl_cert = table.Column<string>(type: "text", nullable: true),
                    error_code = table.Column<string>(nullable: true),
                    message = table.Column<string>(nullable: true),
                    mobile_number = table.Column<string>(nullable: true),
                    hmac = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_log_authentication", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "log_signing",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    application_id = table.Column<Guid>(nullable: true),
                    company_id = table.Column<Guid>(nullable: true),
                    created_date = table.Column<DateTime>(nullable: true),
                    created_user_id = table.Column<Guid>(nullable: true),
                    modified_date = table.Column<DateTime>(nullable: true),
                    modified_user_id = table.Column<Guid>(nullable: true),
                    user_id = table.Column<Guid>(nullable: false),
                    username = table.Column<string>(nullable: true),
                    certificattion_id = table.Column<Guid>(nullable: true),
                    profile_id = table.Column<Guid>(nullable: true),
                    request_id = table.Column<string>(nullable: true),
                    request_type = table.Column<string>(nullable: true),
                    request_time = table.Column<DateTime>(nullable: false),
                    request_xml = table.Column<string>(nullable: true),
                    request_mode = table.Column<string>(nullable: true),
                    relying_party_id = table.Column<string>(nullable: true),
                    relying_party_ip = table.Column<string>(nullable: true),
                    location = table.Column<string>(nullable: true),
                    response_status = table.Column<string>(nullable: true),
                    response_time = table.Column<DateTime>(nullable: false),
                    response_xml = table.Column<string>(nullable: true),
                    backend_request = table.Column<string>(nullable: true),
                    backend_response = table.Column<string>(nullable: true),
                    is_document_exist = table.Column<string>(nullable: true),
                    message = table.Column<string>(nullable: true),
                    transaction_id = table.Column<string>(nullable: true),
                    transaction_type = table.Column<string>(nullable: true),
                    relying_party_signing_cert = table.Column<string>(nullable: true),
                    relying_party_ssl_cert = table.Column<string>(nullable: true),
                    is_otp_email = table.Column<bool>(nullable: false),
                    is_otp_sms = table.Column<bool>(nullable: false),
                    is_otp_soft_token = table.Column<bool>(nullable: false),
                    is_face = table.Column<bool>(nullable: false),
                    is_fingerprint = table.Column<bool>(nullable: false),
                    is_kms = table.Column<bool>(nullable: false),
                    is_smart_card = table.Column<bool>(nullable: false),
                    is_fido_uaf = table.Column<bool>(nullable: false),
                    error_code = table.Column<string>(nullable: true),
                    input_document_path = table.Column<string>(nullable: true),
                    input_document = table.Column<string>(type: "text", nullable: true),
                    input_document_hash = table.Column<string>(type: "text", nullable: true),
                    output_document_path = table.Column<string>(nullable: true),
                    output_document = table.Column<string>(type: "text", nullable: true),
                    output_document_hash = table.Column<string>(type: "text", nullable: true),
                    hmac = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_log_signing", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "sign_log",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    application_id = table.Column<Guid>(nullable: false),
                    user_name = table.Column<string>(nullable: false),
                    cert_id = table.Column<Guid>(nullable: false),
                    code = table.Column<string>(nullable: false),
                    content = table.Column<string>(nullable: false),
                    file_id = table.Column<string>(nullable: true),
                    create_on_date = table.Column<DateTime>(nullable: false),
                    company_id = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sign_log", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Supplier",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    application_id = table.Column<Guid>(nullable: true),
                    company_id = table.Column<Guid>(nullable: true),
                    created_user_id = table.Column<Guid>(nullable: true),
                    modified_user_id = table.Column<Guid>(nullable: true),
                    identity_number = table.Column<long>(nullable: false),
                    status_default = table.Column<bool>(nullable: false),
                    order = table.Column<int>(nullable: false),
                    description = table.Column<string>(nullable: true),
                    Code = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Status = table.Column<bool>(nullable: true),
                    AvatarUrl = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Supplier", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "sys_appliation",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    last_modified_on_date = table.Column<DateTime>(nullable: true),
                    created_on_date = table.Column<DateTime>(nullable: true),
                    company_id = table.Column<Guid>(nullable: true),
                    name = table.Column<string>(maxLength: 128, nullable: false),
                    description = table.Column<string>(maxLength: 1024, nullable: true),
                    code = table.Column<string>(maxLength: 64, nullable: false),
                    demo = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sys_appliation", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Tag",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    application_id = table.Column<Guid>(nullable: true),
                    company_id = table.Column<Guid>(nullable: true),
                    created_user_id = table.Column<Guid>(nullable: true),
                    modified_user_id = table.Column<Guid>(nullable: true),
                    identity_number = table.Column<long>(nullable: false),
                    status_default = table.Column<bool>(nullable: false),
                    order = table.Column<int>(nullable: false),
                    description = table.Column<string>(nullable: true),
                    Code = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Uri = table.Column<string>(nullable: true),
                    Status = table.Column<bool>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tag", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "VisitWebsites",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VisitWebsites", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Vouchers",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Code = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ExpiredTime = table.Column<DateTime>(nullable: false),
                    StartTime = table.Column<DateTime>(nullable: false),
                    Quantity = table.Column<int>(nullable: false),
                    Used = table.Column<int>(nullable: false),
                    Status = table.Column<bool>(nullable: false),
                    PercentDiscount = table.Column<double>(nullable: true),
                    Type = table.Column<int>(nullable: false),
                    Discount = table.Column<double>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Vouchers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "WorkflowGlobalParameter",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Type = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorkflowGlobalParameter", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "WorkflowInbox",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    DocumentId = table.Column<Guid>(nullable: false),
                    IdentityId = table.Column<string>(nullable: true),
                    DocumentHistoryId = table.Column<Guid>(nullable: false),
                    Status = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorkflowInbox", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "WorkflowProcessInstance",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    StateName = table.Column<string>(nullable: true),
                    ActivityName = table.Column<string>(nullable: true),
                    SchemeId = table.Column<Guid>(nullable: true),
                    PreviousState = table.Column<string>(nullable: true),
                    PreviousStateForDirect = table.Column<string>(nullable: true),
                    PreviousStateForReverse = table.Column<string>(nullable: true),
                    PreviousActivity = table.Column<string>(nullable: true),
                    PreviousActivityForDirect = table.Column<string>(nullable: true),
                    PreviousActivityForReverse = table.Column<string>(nullable: true),
                    ParentProcessId = table.Column<Guid>(nullable: true),
                    RootProcessId = table.Column<Guid>(nullable: false),
                    IsDeterminingParametersChanged = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorkflowProcessInstance", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "WorkflowProcessInstancePersistence",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ProcessId = table.Column<Guid>(nullable: false),
                    ParameterName = table.Column<string>(nullable: true),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorkflowProcessInstancePersistence", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "WorkflowProcessInstanceStatus",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Status = table.Column<byte>(nullable: false),
                    Lock = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorkflowProcessInstanceStatus", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "WorkflowProcessScheme",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Scheme = table.Column<string>(nullable: true),
                    DefiningParameters = table.Column<string>(nullable: true),
                    DefiningParametersHash = table.Column<string>(nullable: true),
                    SchemeCode = table.Column<string>(nullable: true),
                    IsObsolete = table.Column<bool>(nullable: false),
                    RootSchemeCode = table.Column<string>(nullable: true),
                    RootSchemeId = table.Column<Guid>(nullable: true),
                    AllowedActivities = table.Column<string>(nullable: true),
                    StartingTransition = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorkflowProcessScheme", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "WorkflowProcessTimer",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ProcessId = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    NextExecutionDateTime = table.Column<DateTime>(nullable: false),
                    Ignore = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorkflowProcessTimer", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "WorkflowProcessTransitionHistory",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ProcessId = table.Column<Guid>(nullable: false),
                    ExecutorIdentityId = table.Column<string>(nullable: true),
                    ActorIdentityId = table.Column<string>(nullable: true),
                    FromActivityName = table.Column<string>(nullable: true),
                    ToActivityName = table.Column<string>(nullable: true),
                    ToStateName = table.Column<string>(nullable: true),
                    TransitionTime = table.Column<DateTime>(nullable: false),
                    TransitionClassifier = table.Column<string>(nullable: true),
                    IsFinalised = table.Column<bool>(nullable: false),
                    FromStateName = table.Column<string>(nullable: true),
                    TriggerName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorkflowProcessTransitionHistory", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "WorkflowScheme",
                columns: table => new
                {
                    Code = table.Column<string>(nullable: false),
                    Scheme = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorkflowScheme", x => x.Code);
                });

            migrationBuilder.CreateTable(
                name: "bsd_metadata_field_template_relationship",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    metadata_template_id = table.Column<Guid>(nullable: false),
                    metadata_field_id = table.Column<Guid>(nullable: false),
                    order = table.Column<int>(nullable: false),
                    status = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_bsd_metadata_field_template_relationship", x => x.id);
                    table.ForeignKey(
                        name: "FK_bsd_metadata_field_template_relationship_bsd_metadata_field_~",
                        column: x => x.metadata_field_id,
                        principalTable: "bsd_metadata_field",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_bsd_metadata_field_template_relationship_bsd_metadata_templa~",
                        column: x => x.metadata_template_id,
                        principalTable: "bsd_metadata_template",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "bsd_taxonomy_term",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    vocabulary_id = table.Column<Guid>(nullable: false),
                    parent_id = table.Column<Guid>(nullable: true),
                    name = table.Column<string>(maxLength: 256, nullable: false),
                    description = table.Column<string>(nullable: true),
                    weight = table.Column<int>(nullable: false),
                    term_left = table.Column<Guid>(nullable: false),
                    term_right = table.Column<Guid>(nullable: false),
                    created_by_user_id = table.Column<Guid>(nullable: true),
                    created_on_date = table.Column<DateTime>(type: "datetime", nullable: true),
                    last_modified_by_user_id = table.Column<Guid>(nullable: true),
                    last_modified_on_date = table.Column<DateTime>(type: "datetime", nullable: true),
                    application_id = table.Column<Guid>(nullable: false),
                    order = table.Column<int>(nullable: true),
                    vocabulary_code = table.Column<string>(maxLength: 256, nullable: true),
                    child_count = table.Column<int>(nullable: true),
                    id_path = table.Column<string>(maxLength: 900, nullable: true),
                    path = table.Column<string>(maxLength: 450, nullable: true),
                    level = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_bsd_taxonomy_term", x => x.id);
                    table.ForeignKey(
                        name: "FK_bsd_taxonomy_term_bsd_taxonomy_vocabulary_vocabulary_id",
                        column: x => x.vocabulary_id,
                        principalTable: "bsd_taxonomy_vocabulary",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "User",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    application_id = table.Column<Guid>(nullable: true),
                    company_id = table.Column<Guid>(nullable: true),
                    created_user_id = table.Column<Guid>(nullable: true),
                    modified_user_id = table.Column<Guid>(nullable: true),
                    identity_number = table.Column<long>(nullable: false),
                    status_default = table.Column<bool>(nullable: false),
                    order = table.Column<int>(nullable: false),
                    description = table.Column<string>(nullable: true),
                    Code = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Username = table.Column<string>(nullable: true),
                    Password = table.Column<string>(nullable: true),
                    PasswordSalt = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    Phone = table.Column<string>(nullable: true),
                    IsLock = table.Column<bool>(nullable: false),
                    IsAdmin = table.Column<bool>(nullable: false),
                    IsQtv = table.Column<bool>(nullable: false),
                    IsSocialSign = table.Column<bool>(nullable: false),
                    Provider = table.Column<string>(nullable: true),
                    Avatar = table.Column<string>(nullable: true),
                    DateOfBirth = table.Column<DateTime>(nullable: false),
                    Sex = table.Column<bool>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedDate = table.Column<DateTime>(nullable: false),
                    CartId = table.Column<Guid>(nullable: true),
                    voucher_json = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_User", x => x.Id);
                    table.ForeignKey(
                        name: "FK_User_Cart_CartId",
                        column: x => x.CartId,
                        principalTable: "Cart",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "log_checkin_account",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    application_id = table.Column<Guid>(nullable: true),
                    company_id = table.Column<Guid>(nullable: true),
                    created_date = table.Column<DateTime>(nullable: true),
                    created_user_id = table.Column<Guid>(nullable: true),
                    modified_date = table.Column<DateTime>(nullable: true),
                    modified_user_id = table.Column<Guid>(nullable: true),
                    identity_number = table.Column<long>(nullable: false),
                    status_default = table.Column<bool>(nullable: false),
                    order = table.Column<int>(nullable: false),
                    description = table.Column<string>(nullable: true),
                    account_id = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_log_checkin_account", x => x.id);
                    table.ForeignKey(
                        name: "FK_log_checkin_account_crm_account_account_id",
                        column: x => x.account_id,
                        principalTable: "crm_account",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Product",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    application_id = table.Column<Guid>(nullable: true),
                    company_id = table.Column<Guid>(nullable: true),
                    created_user_id = table.Column<Guid>(nullable: true),
                    modified_user_id = table.Column<Guid>(nullable: true),
                    identity_number = table.Column<long>(nullable: false),
                    status_default = table.Column<bool>(nullable: false),
                    order = table.Column<int>(nullable: false),
                    Code = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedDate = table.Column<DateTime>(nullable: false),
                    Title = table.Column<string>(nullable: true),
                    MetaTitle = table.Column<string>(nullable: true),
                    LoaiBaoHanh = table.Column<string>(nullable: true),
                    Like = table.Column<int>(nullable: true),
                    Dislike = table.Column<int>(nullable: true),
                    VisitCount = table.Column<int>(nullable: true),
                    Summary = table.Column<string>(nullable: true),
                    ThoiGianBaoHanh = table.Column<int>(nullable: false),
                    Status = table.Column<bool>(nullable: true),
                    Price = table.Column<double>(nullable: false),
                    Discount = table.Column<double>(nullable: false),
                    SoLuong = table.Column<int>(nullable: false),
                    SupplierId = table.Column<Guid>(nullable: false),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Product", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Product_Supplier_SupplierId",
                        column: x => x.SupplierId,
                        principalTable: "Supplier",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "bsd_form_control",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    created_by_user_id = table.Column<Guid>(nullable: false),
                    created_on_date = table.Column<DateTime>(nullable: false),
                    last_modified_by_user_id = table.Column<Guid>(nullable: false),
                    last_modified_on_date = table.Column<DateTime>(nullable: false),
                    application_id = table.Column<Guid>(nullable: false),
                    company_id = table.Column<Guid>(nullable: true),
                    name = table.Column<string>(nullable: true),
                    note = table.Column<string>(nullable: true),
                    content = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_bsd_form_control", x => x.id);
                    table.ForeignKey(
                        name: "FK_bsd_form_control_sys_appliation_application_id",
                        column: x => x.application_id,
                        principalTable: "sys_appliation",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "bsd_form_master",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    created_by_user_id = table.Column<Guid>(nullable: false),
                    created_on_date = table.Column<DateTime>(nullable: false),
                    last_modified_by_user_id = table.Column<Guid>(nullable: false),
                    last_modified_on_date = table.Column<DateTime>(nullable: false),
                    application_id = table.Column<Guid>(nullable: false),
                    company_id = table.Column<Guid>(nullable: true),
                    parent_id = table.Column<Guid>(nullable: true),
                    content = table.Column<string>(nullable: true),
                    name = table.Column<string>(nullable: true),
                    table_name = table.Column<string>(nullable: true),
                    state = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_bsd_form_master", x => x.id);
                    table.ForeignKey(
                        name: "FK_bsd_form_master_sys_appliation_application_id",
                        column: x => x.application_id,
                        principalTable: "sys_appliation",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_bsd_form_master_bsd_form_master_parent_id",
                        column: x => x.parent_id,
                        principalTable: "bsd_form_master",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "bsd_navigation",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    created_by_user_id = table.Column<Guid>(nullable: false),
                    created_on_date = table.Column<DateTime>(nullable: false),
                    last_modified_by_user_id = table.Column<Guid>(nullable: false),
                    last_modified_on_date = table.Column<DateTime>(nullable: false),
                    application_id = table.Column<Guid>(nullable: false),
                    company_id = table.Column<Guid>(nullable: true),
                    parent_id = table.Column<Guid>(nullable: true),
                    code = table.Column<string>(maxLength: 64, nullable: false),
                    name = table.Column<string>(maxLength: 128, nullable: false),
                    url_rewrite = table.Column<string>(maxLength: 128, nullable: true),
                    id_path = table.Column<string>(maxLength: 450, nullable: false),
                    path = table.Column<string>(maxLength: 900, nullable: false),
                    sub_url = table.Column<string>(maxLength: 1024, nullable: true),
                    icon_class = table.Column<string>(maxLength: 64, nullable: true),
                    status = table.Column<bool>(nullable: true),
                    order = table.Column<int>(nullable: true),
                    has_child = table.Column<bool>(nullable: false),
                    level = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_bsd_navigation", x => x.id);
                    table.ForeignKey(
                        name: "FK_bsd_navigation_sys_appliation_application_id",
                        column: x => x.application_id,
                        principalTable: "sys_appliation",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_bsd_navigation_bsd_navigation_parent_id",
                        column: x => x.parent_id,
                        principalTable: "bsd_navigation",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "bsd_parameter",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    created_by_user_id = table.Column<Guid>(nullable: false),
                    created_on_date = table.Column<DateTime>(nullable: false),
                    last_modified_by_user_id = table.Column<Guid>(nullable: false),
                    last_modified_on_date = table.Column<DateTime>(nullable: false),
                    application_id = table.Column<Guid>(nullable: false),
                    company_id = table.Column<Guid>(nullable: true),
                    name = table.Column<string>(maxLength: 64, nullable: false),
                    description = table.Column<string>(maxLength: 1024, nullable: true),
                    value = table.Column<string>(maxLength: 128, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_bsd_parameter", x => x.id);
                    table.ForeignKey(
                        name: "FK_bsd_parameter_sys_appliation_application_id",
                        column: x => x.application_id,
                        principalTable: "sys_appliation",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "cms_organization",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    created_by_user_id = table.Column<Guid>(nullable: false),
                    created_on_date = table.Column<DateTime>(nullable: false),
                    last_modified_by_user_id = table.Column<Guid>(nullable: false),
                    last_modified_on_date = table.Column<DateTime>(nullable: false),
                    application_id = table.Column<Guid>(nullable: false),
                    company_id = table.Column<Guid>(nullable: true),
                    parent_id = table.Column<Guid>(nullable: true),
                    code = table.Column<string>(maxLength: 64, nullable: false),
                    name = table.Column<string>(maxLength: 128, nullable: false),
                    short_name = table.Column<string>(maxLength: 128, nullable: true),
                    id_path = table.Column<string>(nullable: true),
                    type = table.Column<int>(nullable: false),
                    description = table.Column<string>(nullable: true),
                    status = table.Column<bool>(nullable: false),
                    order = table.Column<int>(nullable: true),
                    phone_number = table.Column<string>(nullable: true),
                    fax = table.Column<string>(nullable: true),
                    email = table.Column<string>(nullable: true),
                    parent_name = table.Column<string>(nullable: true),
                    address = table.Column<string>(nullable: true),
                    LogoUrl = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_cms_organization", x => x.id);
                    table.ForeignKey(
                        name: "FK_cms_organization_sys_appliation_application_id",
                        column: x => x.application_id,
                        principalTable: "sys_appliation",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_cms_organization_cms_organization_parent_id",
                        column: x => x.parent_id,
                        principalTable: "cms_organization",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "cms_position",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    created_by_user_id = table.Column<Guid>(nullable: false),
                    created_on_date = table.Column<DateTime>(nullable: false),
                    last_modified_by_user_id = table.Column<Guid>(nullable: false),
                    last_modified_on_date = table.Column<DateTime>(nullable: false),
                    application_id = table.Column<Guid>(nullable: false),
                    company_id = table.Column<Guid>(nullable: true),
                    code = table.Column<string>(maxLength: 64, nullable: false),
                    name = table.Column<string>(maxLength: 128, nullable: false),
                    description = table.Column<string>(nullable: true),
                    status = table.Column<bool>(nullable: false),
                    order = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_cms_position", x => x.id);
                    table.ForeignKey(
                        name: "FK_cms_position_sys_appliation_application_id",
                        column: x => x.application_id,
                        principalTable: "sys_appliation",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "cms_sign_profile",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    created_by_user_id = table.Column<Guid>(nullable: false),
                    created_on_date = table.Column<DateTime>(nullable: false),
                    last_modified_by_user_id = table.Column<Guid>(nullable: false),
                    last_modified_on_date = table.Column<DateTime>(nullable: false),
                    application_id = table.Column<Guid>(nullable: false),
                    company_id = table.Column<Guid>(nullable: true),
                    code = table.Column<string>(maxLength: 64, nullable: false),
                    name = table.Column<string>(maxLength: 64, nullable: false),
                    description = table.Column<string>(maxLength: 1024, nullable: true),
                    standart_sign = table.Column<string>(nullable: true),
                    hashing_algorithm = table.Column<string>(nullable: true),
                    sign_zone_config = table.Column<int>(nullable: false),
                    urx = table.Column<int>(nullable: false),
                    ury = table.Column<int>(nullable: false),
                    llx = table.Column<int>(nullable: false),
                    lly = table.Column<int>(nullable: false),
                    sign_at_page = table.Column<int>(nullable: false),
                    is_sign_time = table.Column<bool>(nullable: false),
                    is_revoke = table.Column<bool>(nullable: false),
                    status = table.Column<bool>(nullable: false),
                    digital_signature_option = table.Column<int>(nullable: false),
                    page_type = table.Column<int>(nullable: false),
                    template_file_path = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_cms_sign_profile", x => x.id);
                    table.ForeignKey(
                        name: "FK_cms_sign_profile_sys_appliation_application_id",
                        column: x => x.application_id,
                        principalTable: "sys_appliation",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "idm_api_map_role",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    created_by_user_id = table.Column<Guid>(nullable: false),
                    created_on_date = table.Column<DateTime>(nullable: false),
                    last_modified_by_user_id = table.Column<Guid>(nullable: false),
                    last_modified_on_date = table.Column<DateTime>(nullable: false),
                    company_id = table.Column<Guid>(nullable: true),
                    role_id = table.Column<Guid>(nullable: false),
                    application_id = table.Column<Guid>(nullable: false),
                    api_id = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_idm_api_map_role", x => x.id);
                    table.ForeignKey(
                        name: "FK_idm_api_map_role_idm_api_api_id",
                        column: x => x.api_id,
                        principalTable: "idm_api",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_idm_api_map_role_sys_appliation_application_id",
                        column: x => x.application_id,
                        principalTable: "sys_appliation",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_idm_api_map_role_idm_role_role_id",
                        column: x => x.role_id,
                        principalTable: "idm_role",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "idm_device",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    created_by_user_id = table.Column<Guid>(nullable: false),
                    created_on_date = table.Column<DateTime>(nullable: false),
                    last_modified_by_user_id = table.Column<Guid>(nullable: false),
                    last_modified_on_date = table.Column<DateTime>(nullable: false),
                    application_id = table.Column<Guid>(nullable: false),
                    company_id = table.Column<Guid>(nullable: true),
                    user_id = table.Column<Guid>(nullable: false),
                    device_id = table.Column<string>(nullable: true),
                    device_token = table.Column<string>(nullable: true),
                    device_type = table.Column<string>(nullable: true),
                    status = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_idm_device", x => x.id);
                    table.ForeignKey(
                        name: "FK_idm_device_sys_appliation_application_id",
                        column: x => x.application_id,
                        principalTable: "sys_appliation",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "idm_right_map_role",
                columns: table => new
                {
                    role_id = table.Column<Guid>(nullable: false),
                    application_id = table.Column<Guid>(nullable: false),
                    right_id = table.Column<Guid>(nullable: false),
                    created_by_user_id = table.Column<Guid>(nullable: false),
                    created_on_date = table.Column<DateTime>(nullable: false),
                    last_modified_by_user_id = table.Column<Guid>(nullable: false),
                    last_modified_on_date = table.Column<DateTime>(nullable: false),
                    company_id = table.Column<Guid>(nullable: true),
                    id = table.Column<long>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_idm_right_map_role", x => new { x.application_id, x.right_id, x.role_id });
                    table.UniqueConstraint("AK_idm_right_map_role_id", x => x.id);
                    table.ForeignKey(
                        name: "FK_idm_right_map_role_sys_appliation_application_id",
                        column: x => x.application_id,
                        principalTable: "sys_appliation",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_idm_right_map_role_idm_right_right_id",
                        column: x => x.right_id,
                        principalTable: "idm_right",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_idm_right_map_role_idm_role_role_id",
                        column: x => x.role_id,
                        principalTable: "idm_role",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "idm_right_map_user",
                columns: table => new
                {
                    user_id = table.Column<Guid>(nullable: false),
                    application_id = table.Column<Guid>(nullable: false),
                    right_id = table.Column<Guid>(nullable: false),
                    created_by_user_id = table.Column<Guid>(nullable: false),
                    created_on_date = table.Column<DateTime>(nullable: false),
                    last_modified_by_user_id = table.Column<Guid>(nullable: false),
                    last_modified_on_date = table.Column<DateTime>(nullable: false),
                    company_id = table.Column<Guid>(nullable: true),
                    id = table.Column<long>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    inherited_from_roles = table.Column<string>(maxLength: 1024, nullable: true),
                    inherited = table.Column<bool>(nullable: false),
                    enable = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_idm_right_map_user", x => new { x.application_id, x.right_id, x.user_id });
                    table.UniqueConstraint("AK_idm_right_map_user_id", x => x.id);
                    table.ForeignKey(
                        name: "FK_idm_right_map_user_sys_appliation_application_id",
                        column: x => x.application_id,
                        principalTable: "sys_appliation",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_idm_right_map_user_idm_right_right_id",
                        column: x => x.right_id,
                        principalTable: "idm_right",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_idm_right_map_user_idm_user_user_id",
                        column: x => x.user_id,
                        principalTable: "idm_user",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "idm_token",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    created_by_user_id = table.Column<Guid>(nullable: false),
                    created_on_date = table.Column<DateTime>(nullable: false),
                    last_modified_by_user_id = table.Column<Guid>(nullable: false),
                    last_modified_on_date = table.Column<DateTime>(nullable: false),
                    application_id = table.Column<Guid>(nullable: false),
                    company_id = table.Column<Guid>(nullable: true),
                    client_id = table.Column<Guid>(nullable: true),
                    access_token = table.Column<string>(nullable: false),
                    refresh_token = table.Column<string>(nullable: true),
                    token_type = table.Column<string>(nullable: true),
                    expiry_time = table.Column<DateTime>(nullable: false),
                    user_id = table.Column<Guid>(nullable: false),
                    username = table.Column<string>(nullable: true),
                    is_active = table.Column<bool>(nullable: false),
                    algorithm = table.Column<string>(nullable: true),
                    issuer = table.Column<string>(nullable: true),
                    issued_at = table.Column<DateTime>(nullable: false),
                    hmac = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_idm_token", x => x.id);
                    table.ForeignKey(
                        name: "FK_idm_token_sys_appliation_application_id",
                        column: x => x.application_id,
                        principalTable: "sys_appliation",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "idm_user_map_role",
                columns: table => new
                {
                    user_id = table.Column<Guid>(nullable: false),
                    application_id = table.Column<Guid>(nullable: false),
                    role_id = table.Column<Guid>(nullable: false),
                    created_by_user_id = table.Column<Guid>(nullable: false),
                    created_on_date = table.Column<DateTime>(nullable: false),
                    last_modified_by_user_id = table.Column<Guid>(nullable: false),
                    last_modified_on_date = table.Column<DateTime>(nullable: false),
                    company_id = table.Column<Guid>(nullable: true),
                    id = table.Column<long>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_idm_user_map_role", x => new { x.application_id, x.user_id, x.role_id });
                    table.UniqueConstraint("AK_idm_user_map_role_id", x => x.id);
                    table.ForeignKey(
                        name: "FK_idm_user_map_role_sys_appliation_application_id",
                        column: x => x.application_id,
                        principalTable: "sys_appliation",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_idm_user_map_role_idm_role_role_id",
                        column: x => x.role_id,
                        principalTable: "idm_role",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_idm_user_map_role_idm_user_user_id",
                        column: x => x.user_id,
                        principalTable: "idm_user",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "tms_cert_user",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    created_by_user_id = table.Column<Guid>(nullable: false),
                    created_on_date = table.Column<DateTime>(nullable: false),
                    last_modified_by_user_id = table.Column<Guid>(nullable: false),
                    last_modified_on_date = table.Column<DateTime>(nullable: false),
                    application_id = table.Column<Guid>(nullable: false),
                    company_id = table.Column<Guid>(nullable: true),
                    user_id = table.Column<Guid>(nullable: false),
                    cert_id = table.Column<Guid>(nullable: false),
                    pass_phrase = table.Column<string>(nullable: true),
                    hmac = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tms_cert_user", x => x.id);
                    table.ForeignKey(
                        name: "FK_tms_cert_user_sys_appliation_application_id",
                        column: x => x.application_id,
                        principalTable: "sys_appliation",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "tms_certification",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    created_by_user_id = table.Column<Guid>(nullable: false),
                    created_on_date = table.Column<DateTime>(nullable: false),
                    last_modified_by_user_id = table.Column<Guid>(nullable: false),
                    last_modified_on_date = table.Column<DateTime>(nullable: false),
                    application_id = table.Column<Guid>(nullable: false),
                    company_id = table.Column<Guid>(nullable: true),
                    supplier = table.Column<string>(nullable: true),
                    slot_id = table.Column<int>(nullable: false),
                    user_pin = table.Column<string>(nullable: false),
                    key = table.Column<string>(nullable: false),
                    alias = table.Column<string>(nullable: false),
                    slot_label = table.Column<string>(nullable: true),
                    key_length = table.Column<string>(nullable: true),
                    key_algorithm = table.Column<string>(nullable: true),
                    certificate_file_name = table.Column<string>(nullable: false),
                    cert_type = table.Column<string>(nullable: true),
                    subject_dn_cmt_mst = table.Column<string>(nullable: true),
                    subject_dn_cn = table.Column<string>(nullable: true),
                    subject_dn_o = table.Column<string>(nullable: true),
                    subject_dn_st = table.Column<string>(nullable: true),
                    subject_dn_c = table.Column<string>(nullable: true),
                    csr = table.Column<string>(nullable: true),
                    cert = table.Column<string>(nullable: true),
                    res_partner = table.Column<Guid>(nullable: true),
                    p12_path = table.Column<string>(nullable: true),
                    certification_path = table.Column<string>(nullable: true),
                    certification_hash = table.Column<string>(nullable: true),
                    subject = table.Column<string>(nullable: true),
                    certification_status = table.Column<int>(nullable: true),
                    push_notice_enabled = table.Column<bool>(nullable: true),
                    certification_profile_id = table.Column<Guid>(nullable: true),
                    token_serial_number = table.Column<string>(nullable: true),
                    effective_date = table.Column<DateTime>(nullable: true),
                    expiration_date = table.Column<DateTime>(nullable: true),
                    expiration_contract_date = table.Column<DateTime>(nullable: true),
                    duration = table.Column<int>(nullable: true),
                    private_key = table.Column<string>(nullable: true),
                    private_key_enable = table.Column<int>(nullable: true),
                    public_key = table.Column<string>(nullable: true),
                    public_key_hash = table.Column<string>(nullable: true),
                    activation_code = table.Column<string>(nullable: true),
                    revoke_date = table.Column<DateTime>(nullable: true),
                    operation_date = table.Column<DateTime>(nullable: true),
                    hmac = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tms_certification", x => x.id);
                    table.ForeignKey(
                        name: "FK_tms_certification_sys_appliation_application_id",
                        column: x => x.application_id,
                        principalTable: "sys_appliation",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "WorkflowApplication",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    created_by_user_id = table.Column<Guid>(nullable: false),
                    created_on_date = table.Column<DateTime>(nullable: false),
                    last_modified_by_user_id = table.Column<Guid>(nullable: false),
                    last_modified_on_date = table.Column<DateTime>(nullable: false),
                    application_id = table.Column<Guid>(nullable: false),
                    company_id = table.Column<Guid>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    ProcessStatusChangedConfig = table.Column<string>(nullable: true),
                    Status = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorkflowApplication", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WorkflowApplication_sys_appliation_application_id",
                        column: x => x.application_id,
                        principalTable: "sys_appliation",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "WorkflowDocument",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    created_by_user_id = table.Column<Guid>(nullable: false),
                    created_on_date = table.Column<DateTime>(nullable: false),
                    last_modified_by_user_id = table.Column<Guid>(nullable: false),
                    last_modified_on_date = table.Column<DateTime>(nullable: false),
                    application_id = table.Column<Guid>(nullable: false),
                    company_id = table.Column<Guid>(nullable: true),
                    WorkflowApplicationId = table.Column<Guid>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Scheme = table.Column<string>(nullable: true),
                    Status = table.Column<string>(nullable: true),
                    BussinessFlow = table.Column<string>(nullable: true),
                    PrevState = table.Column<string>(nullable: true),
                    PrevActivity = table.Column<string>(nullable: true),
                    State = table.Column<string>(nullable: true),
                    Activity = table.Column<string>(nullable: true),
                    NextActivity = table.Column<string>(nullable: true),
                    NextState = table.Column<string>(nullable: true),
                    AllowIdentityIds = table.Column<string>(nullable: true),
                    ListSubState = table.Column<string>(nullable: true),
                    TransitionClassifier = table.Column<int>(nullable: false),
                    ExecutedTransition = table.Column<DateTime>(nullable: false),
                    ExecutedIdentityId = table.Column<Guid>(nullable: true),
                    ExecutedAction = table.Column<string>(nullable: true),
                    ExecutedActionValue = table.Column<string>(nullable: true),
                    ExecutedFormInputJson = table.Column<string>(nullable: true),
                    ExecutedFiles = table.Column<string>(nullable: true),
                    IsDraft = table.Column<int>(nullable: false),
                    IsInProcess = table.Column<int>(nullable: false),
                    IsCompleted = table.Column<int>(nullable: false),
                    IsRejected = table.Column<int>(nullable: false),
                    ShowSign = table.Column<int>(nullable: false),
                    DAY = table.Column<string>(nullable: true),
                    MONTH = table.Column<string>(nullable: true),
                    YEAR = table.Column<string>(nullable: true),
                    WEEK = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorkflowDocument", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WorkflowDocument_sys_appliation_application_id",
                        column: x => x.application_id,
                        principalTable: "sys_appliation",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "WorkflowSchemeAttribute",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    created_by_user_id = table.Column<Guid>(nullable: false),
                    created_on_date = table.Column<DateTime>(nullable: false),
                    last_modified_by_user_id = table.Column<Guid>(nullable: false),
                    last_modified_on_date = table.Column<DateTime>(nullable: false),
                    application_id = table.Column<Guid>(nullable: false),
                    company_id = table.Column<Guid>(nullable: true),
                    Code = table.Column<string>(maxLength: 64, nullable: false),
                    Description = table.Column<string>(nullable: true),
                    Status = table.Column<bool>(nullable: false),
                    Order = table.Column<int>(nullable: false),
                    ActiveVersion = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorkflowSchemeAttribute", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WorkflowSchemeAttribute_sys_appliation_application_id",
                        column: x => x.application_id,
                        principalTable: "sys_appliation",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "WorkflowSchemeVersion",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    created_by_user_id = table.Column<Guid>(nullable: false),
                    created_on_date = table.Column<DateTime>(nullable: false),
                    last_modified_by_user_id = table.Column<Guid>(nullable: false),
                    last_modified_on_date = table.Column<DateTime>(nullable: false),
                    application_id = table.Column<Guid>(nullable: false),
                    company_id = table.Column<Guid>(nullable: true),
                    Code = table.Column<string>(maxLength: 64, nullable: false),
                    Version = table.Column<int>(nullable: false),
                    Scheme = table.Column<string>(nullable: true),
                    isActive = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorkflowSchemeVersion", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WorkflowSchemeVersion_sys_appliation_application_id",
                        column: x => x.application_id,
                        principalTable: "sys_appliation",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "bsd_catalog_item",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    mapped_term_id = table.Column<Guid>(nullable: false),
                    name = table.Column<string>(maxLength: 256, nullable: false),
                    status = table.Column<bool>(nullable: false),
                    created_by_user_id = table.Column<Guid>(nullable: false),
                    created_on_date = table.Column<DateTime>(type: "datetime", nullable: false),
                    last_modified_by_user_id = table.Column<Guid>(nullable: false),
                    last_modified_on_date = table.Column<DateTime>(type: "datetime", nullable: false),
                    code = table.Column<string>(maxLength: 256, nullable: false),
                    description = table.Column<string>(nullable: true),
                    order = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_bsd_catalog_item", x => x.id);
                    table.ForeignKey(
                        name: "FK_bsd_catalog_item_bsd_taxonomy_term_mapped_term_id",
                        column: x => x.mapped_term_id,
                        principalTable: "bsd_taxonomy_term",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "bsd_catalog_master",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    metadata_template_id = table.Column<Guid>(nullable: false),
                    mapped_term_id = table.Column<Guid>(nullable: false),
                    mapped_vocabulary_id = table.Column<Guid>(nullable: false),
                    name = table.Column<string>(maxLength: 256, nullable: false),
                    code = table.Column<string>(maxLength: 256, nullable: true),
                    created_by_user_id = table.Column<Guid>(nullable: true),
                    created_on_date = table.Column<DateTime>(type: "datetime", nullable: true),
                    last_modified_by_user_id = table.Column<Guid>(nullable: true),
                    last_modified_on_date = table.Column<DateTime>(type: "datetime", nullable: true),
                    status = table.Column<bool>(nullable: false),
                    description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_bsd_catalog_master", x => x.id);
                    table.ForeignKey(
                        name: "FK_bsd_catalog_master_bsd_taxonomy_term_mapped_term_id",
                        column: x => x.mapped_term_id,
                        principalTable: "bsd_taxonomy_term",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_bsd_catalog_master_bsd_taxonomy_vocabulary_mapped_vocabulary~",
                        column: x => x.mapped_vocabulary_id,
                        principalTable: "bsd_taxonomy_vocabulary",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_bsd_catalog_master_bsd_metadata_template_metadata_template_id",
                        column: x => x.metadata_template_id,
                        principalTable: "bsd_metadata_template",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Notification",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Title = table.Column<string>(nullable: true),
                    UserId = table.Column<Guid>(nullable: false),
                    IsRead = table.Column<bool>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Notification", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Notification_User_UserId",
                        column: x => x.UserId,
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Order",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    application_id = table.Column<Guid>(nullable: true),
                    company_id = table.Column<Guid>(nullable: true),
                    created_user_id = table.Column<Guid>(nullable: true),
                    modified_user_id = table.Column<Guid>(nullable: true),
                    identity_number = table.Column<long>(nullable: false),
                    status_default = table.Column<bool>(nullable: false),
                    order = table.Column<int>(nullable: false),
                    Code = table.Column<string>(nullable: true),
                    Status = table.Column<int>(nullable: false),
                    SubTotal = table.Column<double>(nullable: false),
                    ItemDiscount = table.Column<double>(nullable: true),
                    Shipping = table.Column<double>(nullable: false),
                    Total = table.Column<double>(nullable: false),
                    MaGiamGia = table.Column<string>(nullable: true),
                    Discount = table.Column<double>(nullable: true),
                    UserId = table.Column<Guid>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    City = table.Column<string>(nullable: true),
                    CityId = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    District = table.Column<string>(nullable: true),
                    DistrictId = table.Column<string>(nullable: true),
                    Commune = table.Column<string>(nullable: true),
                    CommuneId = table.Column<string>(nullable: true),
                    PhuongThucThanhToan = table.Column<int>(nullable: false),
                    AddressDetail = table.Column<string>(nullable: true),
                    GrandTotal = table.Column<double>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ConfirmedDate = table.Column<DateTime>(nullable: false),
                    DeliverDate = table.Column<DateTime>(nullable: false),
                    RecivedDate = table.Column<DateTime>(nullable: false),
                    CanceledDate = table.Column<DateTime>(nullable: false),
                    ModifiedDate = table.Column<DateTime>(nullable: false),
                    ListProducts = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Order", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Order_User_UserId",
                        column: x => x.UserId,
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Cart_Product",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    application_id = table.Column<Guid>(nullable: true),
                    company_id = table.Column<Guid>(nullable: true),
                    created_user_id = table.Column<Guid>(nullable: true),
                    modified_user_id = table.Column<Guid>(nullable: true),
                    identity_number = table.Column<long>(nullable: false),
                    status_default = table.Column<bool>(nullable: false),
                    order = table.Column<int>(nullable: false),
                    description = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedDate = table.Column<DateTime>(nullable: false),
                    CartId = table.Column<Guid>(nullable: false),
                    ProductId = table.Column<Guid>(nullable: false),
                    Price = table.Column<double>(nullable: false),
                    Discount = table.Column<double>(nullable: false),
                    Active = table.Column<bool>(nullable: true),
                    Content = table.Column<string>(nullable: true),
                    Code = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Cart_Product", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Cart_Product_Cart_CartId",
                        column: x => x.CartId,
                        principalTable: "Cart",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Cart_Product_Product_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Product",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Category_Meta_Product",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    identity_number = table.Column<long>(nullable: false),
                    Code = table.Column<string>(nullable: true),
                    CategoryMetaId = table.Column<Guid>(nullable: false),
                    ProductId = table.Column<Guid>(nullable: false),
                    Status = table.Column<bool>(nullable: true),
                    Content = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Category_Meta_Product", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Category_Meta_Product_Category_Meta_CategoryMetaId",
                        column: x => x.CategoryMetaId,
                        principalTable: "Category_Meta",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Category_Meta_Product_Product_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Product",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Category_Product",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    application_id = table.Column<Guid>(nullable: true),
                    company_id = table.Column<Guid>(nullable: true),
                    created_user_id = table.Column<Guid>(nullable: true),
                    modified_user_id = table.Column<Guid>(nullable: true),
                    identity_number = table.Column<long>(nullable: false),
                    status_default = table.Column<bool>(nullable: false),
                    order = table.Column<int>(nullable: false),
                    description = table.Column<string>(nullable: true),
                    Code = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedDate = table.Column<DateTime>(nullable: false),
                    ProductId = table.Column<Guid>(nullable: false),
                    Status = table.Column<bool>(nullable: true),
                    CategoryId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Category_Product", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Category_Product_Category_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "Category",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Category_Product_Product_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Product",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Picture",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    application_id = table.Column<Guid>(nullable: true),
                    company_id = table.Column<Guid>(nullable: true),
                    created_user_id = table.Column<Guid>(nullable: true),
                    modified_user_id = table.Column<Guid>(nullable: true),
                    identity_number = table.Column<long>(nullable: false),
                    status_default = table.Column<bool>(nullable: false),
                    order = table.Column<int>(nullable: false),
                    description = table.Column<string>(nullable: true),
                    ProductId = table.Column<Guid>(nullable: false),
                    Code = table.Column<string>(nullable: true),
                    Status = table.Column<bool>(nullable: false),
                    PictureUrl = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Picture", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Picture_Product_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Product",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Product_Review",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    application_id = table.Column<Guid>(nullable: true),
                    company_id = table.Column<Guid>(nullable: true),
                    created_user_id = table.Column<Guid>(nullable: true),
                    modified_user_id = table.Column<Guid>(nullable: true),
                    identity_number = table.Column<long>(nullable: false),
                    status_default = table.Column<bool>(nullable: false),
                    order = table.Column<int>(nullable: false),
                    description = table.Column<string>(nullable: true),
                    ProductId = table.Column<Guid>(nullable: false),
                    UserId = table.Column<Guid>(nullable: false),
                    Code = table.Column<string>(nullable: true),
                    AvatarUrl = table.Column<string>(nullable: true),
                    ParentId = table.Column<Guid>(nullable: true),
                    Rating = table.Column<int>(nullable: false),
                    Status = table.Column<bool>(nullable: true),
                    IsRating = table.Column<bool>(nullable: false),
                    Content = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Product_Review", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Product_Review_Product_Review_ParentId",
                        column: x => x.ParentId,
                        principalTable: "Product_Review",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Product_Review_Product_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Product",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Product_Review_User_UserId",
                        column: x => x.UserId,
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ProductTag",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    application_id = table.Column<Guid>(nullable: true),
                    company_id = table.Column<Guid>(nullable: true),
                    created_user_id = table.Column<Guid>(nullable: true),
                    modified_user_id = table.Column<Guid>(nullable: true),
                    identity_number = table.Column<long>(nullable: false),
                    status_default = table.Column<bool>(nullable: false),
                    order = table.Column<int>(nullable: false),
                    description = table.Column<string>(nullable: true),
                    ProductId = table.Column<Guid>(nullable: false),
                    Code = table.Column<string>(nullable: true),
                    TagId = table.Column<Guid>(nullable: false),
                    Status = table.Column<bool>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductTag", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProductTag_Product_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Product",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProductTag_Tag_TagId",
                        column: x => x.TagId,
                        principalTable: "Tag",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "bsd_form_template",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    master_id = table.Column<Guid>(nullable: false),
                    name = table.Column<string>(nullable: true),
                    note = table.Column<string>(nullable: true),
                    html = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_bsd_form_template", x => x.id);
                    table.ForeignKey(
                        name: "FK_bsd_form_template_bsd_form_master_master_id",
                        column: x => x.master_id,
                        principalTable: "bsd_form_master",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "bsd_navigation_map_role",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    last_modified_on_date = table.Column<DateTime>(nullable: true),
                    created_on_date = table.Column<DateTime>(nullable: true),
                    company_id = table.Column<Guid>(nullable: true),
                    navigation_id = table.Column<Guid>(nullable: false),
                    role_id = table.Column<Guid>(nullable: false),
                    from_sub_navigation = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_bsd_navigation_map_role", x => x.id);
                    table.ForeignKey(
                        name: "FK_bsd_navigation_map_role_bsd_navigation_navigation_id",
                        column: x => x.navigation_id,
                        principalTable: "bsd_navigation",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "idm_role_map_organization",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    application_id = table.Column<Guid>(nullable: true),
                    company_id = table.Column<Guid>(nullable: true),
                    created_date = table.Column<DateTime>(nullable: true),
                    created_user_id = table.Column<Guid>(nullable: true),
                    modified_date = table.Column<DateTime>(nullable: true),
                    modified_user_id = table.Column<Guid>(nullable: true),
                    identity_number = table.Column<long>(nullable: false),
                    status_default = table.Column<bool>(nullable: false),
                    order = table.Column<int>(nullable: false),
                    description = table.Column<string>(nullable: true),
                    role_id = table.Column<Guid>(nullable: false),
                    organization_id = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_idm_role_map_organization", x => x.id);
                    table.ForeignKey(
                        name: "FK_idm_role_map_organization_cms_organization_organization_id",
                        column: x => x.organization_id,
                        principalTable: "cms_organization",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_idm_role_map_organization_idm_role_role_id",
                        column: x => x.role_id,
                        principalTable: "idm_role",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "idm_user_map_org",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    created_by_user_id = table.Column<Guid>(nullable: false),
                    created_on_date = table.Column<DateTime>(nullable: false),
                    last_modified_by_user_id = table.Column<Guid>(nullable: false),
                    last_modified_on_date = table.Column<DateTime>(nullable: false),
                    company_id = table.Column<Guid>(nullable: true),
                    user_id = table.Column<Guid>(nullable: false),
                    application_id = table.Column<Guid>(nullable: false),
                    organization_id = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_idm_user_map_org", x => x.id);
                    table.ForeignKey(
                        name: "FK_idm_user_map_org_sys_appliation_application_id",
                        column: x => x.application_id,
                        principalTable: "sys_appliation",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_idm_user_map_org_cms_organization_organization_id",
                        column: x => x.organization_id,
                        principalTable: "cms_organization",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_idm_user_map_org_idm_user_user_id",
                        column: x => x.user_id,
                        principalTable: "idm_user",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "WorkflowDocumentAttachment",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    created_by_user_id = table.Column<Guid>(nullable: false),
                    created_on_date = table.Column<DateTime>(nullable: false),
                    last_modified_by_user_id = table.Column<Guid>(nullable: false),
                    last_modified_on_date = table.Column<DateTime>(nullable: false),
                    application_id = table.Column<Guid>(nullable: false),
                    company_id = table.Column<Guid>(nullable: true),
                    WorkflowDocumentId = table.Column<Guid>(nullable: false),
                    PhysicalName = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Size = table.Column<long>(nullable: false),
                    Extension = table.Column<string>(nullable: true),
                    PhysicalPath = table.Column<string>(nullable: true),
                    RelativePath = table.Column<string>(nullable: true),
                    Note = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorkflowDocumentAttachment", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WorkflowDocumentAttachment_sys_appliation_application_id",
                        column: x => x.application_id,
                        principalTable: "sys_appliation",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_WorkflowDocumentAttachment_WorkflowDocument_WorkflowDocument~",
                        column: x => x.WorkflowDocumentId,
                        principalTable: "WorkflowDocument",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "WorkflowDocumentHistory",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    WorkflowDocumentId = table.Column<Guid>(nullable: false),
                    DocumentName = table.Column<string>(nullable: true),
                    Scheme = table.Column<string>(nullable: true),
                    Status = table.Column<string>(nullable: true),
                    PrevState = table.Column<string>(nullable: true),
                    PrevActivity = table.Column<string>(nullable: true),
                    State = table.Column<string>(nullable: true),
                    Activity = table.Column<string>(nullable: true),
                    ListSubState = table.Column<string>(nullable: true),
                    ExecutedTransition = table.Column<DateTime>(nullable: false),
                    ExecutedIdentityId = table.Column<Guid>(nullable: true),
                    ExecutedAction = table.Column<string>(nullable: true),
                    ExecutedActionValue = table.Column<string>(nullable: true),
                    TransitionClassifier = table.Column<int>(nullable: false),
                    ExecutedFormInputJson = table.Column<string>(nullable: true),
                    ExecutedFiles = table.Column<string>(nullable: true),
                    IsSubProcess = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorkflowDocumentHistory", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WorkflowDocumentHistory_WorkflowDocument_WorkflowDocumentId",
                        column: x => x.WorkflowDocumentId,
                        principalTable: "WorkflowDocument",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "bsd_catalog_item_attribute",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    catalog_item_id = table.Column<Guid>(nullable: false),
                    metadata_field_id = table.Column<Guid>(nullable: false),
                    metadata_field_name = table.Column<string>(maxLength: 256, nullable: false),
                    nvarchar_value = table.Column<string>(nullable: true),
                    int_value = table.Column<int>(nullable: true),
                    datetime_value = table.Column<DateTime>(type: "datetime", nullable: true),
                    bit_value = table.Column<bool>(nullable: true),
                    varchar_value = table.Column<string>(nullable: true),
                    guid_value = table.Column<Guid>(nullable: true),
                    data_type = table.Column<string>(maxLength: 50, nullable: true),
                    metadata_field_code = table.Column<string>(maxLength: 256, nullable: false),
                    json_value = table.Column<string>(nullable: true),
                    json_guid_value = table.Column<string>(nullable: true),
                    time_value = table.Column<DateTime>(type: "datetime", nullable: true),
                    date_value = table.Column<DateTime>(type: "date", nullable: true),
                    year_value = table.Column<DateTime>(type: "datetime", nullable: true),
                    month_value = table.Column<DateTime>(type: "datetime", nullable: true),
                    type_date_time = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_bsd_catalog_item_attribute", x => x.id);
                    table.ForeignKey(
                        name: "FK_bsd_catalog_item_attribute_bsd_catalog_item_catalog_item_id",
                        column: x => x.catalog_item_id,
                        principalTable: "bsd_catalog_item",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_bsd_catalog_item_attribute_bsd_metadata_field_metadata_field~",
                        column: x => x.metadata_field_id,
                        principalTable: "bsd_metadata_field",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "bsd_catalog_field",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    name = table.Column<string>(maxLength: 256, nullable: false),
                    code = table.Column<string>(maxLength: 256, nullable: false),
                    description = table.Column<string>(nullable: true),
                    formly_content = table.Column<string>(nullable: false),
                    is_record_common_field = table.Column<bool>(nullable: false),
                    is_document_common_field = table.Column<bool>(nullable: false),
                    created_on_date = table.Column<DateTime>(type: "datetime", nullable: false),
                    last_modified_on_date = table.Column<DateTime>(type: "datetime", nullable: false),
                    created_by_user_id = table.Column<Guid>(nullable: false),
                    last_modified_by_user_id = table.Column<Guid>(nullable: false),
                    display_catalog = table.Column<bool>(nullable: false),
                    display_report = table.Column<bool>(nullable: false),
                    display_reader = table.Column<bool>(nullable: false),
                    display_report_search = table.Column<bool>(nullable: false),
                    display_reader_search = table.Column<bool>(nullable: false),
                    catalog_master_id = table.Column<Guid>(nullable: true),
                    field_width = table.Column<int>(nullable: true),
                    data_type = table.Column<string>(maxLength: 100, nullable: true),
                    catalog_code = table.Column<string>(maxLength: 100, nullable: true),
                    field_name_new = table.Column<string>(maxLength: 50, nullable: true),
                    display_name = table.Column<string>(maxLength: 256, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_bsd_catalog_field", x => x.id);
                    table.ForeignKey(
                        name: "FK_bsd_catalog_field_bsd_catalog_master_catalog_master_id",
                        column: x => x.catalog_master_id,
                        principalTable: "bsd_catalog_master",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Order_Product",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    application_id = table.Column<Guid>(nullable: true),
                    company_id = table.Column<Guid>(nullable: true),
                    created_user_id = table.Column<Guid>(nullable: true),
                    modified_user_id = table.Column<Guid>(nullable: true),
                    identity_number = table.Column<long>(nullable: false),
                    status_default = table.Column<bool>(nullable: false),
                    description = table.Column<string>(nullable: true),
                    ProductId = table.Column<Guid>(nullable: false),
                    Code = table.Column<string>(nullable: true),
                    OrderId = table.Column<Guid>(nullable: false),
                    Price = table.Column<double>(nullable: false),
                    Discount = table.Column<double>(nullable: false),
                    Quantity = table.Column<int>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Order_Product", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Order_Product_Order_OrderId",
                        column: x => x.OrderId,
                        principalTable: "Order",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Order_Product_Product_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Product",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_bsd_catalog_field_catalog_master_id",
                table: "bsd_catalog_field",
                column: "catalog_master_id");

            migrationBuilder.CreateIndex(
                name: "IX_bsd_catalog_item_mapped_term_id",
                table: "bsd_catalog_item",
                column: "mapped_term_id");

            migrationBuilder.CreateIndex(
                name: "IX_bsd_catalog_item_attribute_catalog_item_id",
                table: "bsd_catalog_item_attribute",
                column: "catalog_item_id");

            migrationBuilder.CreateIndex(
                name: "IX_bsd_catalog_item_attribute_metadata_field_id",
                table: "bsd_catalog_item_attribute",
                column: "metadata_field_id");

            migrationBuilder.CreateIndex(
                name: "IX_bsd_catalog_master_mapped_term_id",
                table: "bsd_catalog_master",
                column: "mapped_term_id");

            migrationBuilder.CreateIndex(
                name: "IX_bsd_catalog_master_mapped_vocabulary_id",
                table: "bsd_catalog_master",
                column: "mapped_vocabulary_id");

            migrationBuilder.CreateIndex(
                name: "IX_bsd_catalog_master_metadata_template_id",
                table: "bsd_catalog_master",
                column: "metadata_template_id");

            migrationBuilder.CreateIndex(
                name: "IX_bsd_form_control_application_id",
                table: "bsd_form_control",
                column: "application_id");

            migrationBuilder.CreateIndex(
                name: "IX_bsd_form_master_application_id",
                table: "bsd_form_master",
                column: "application_id");

            migrationBuilder.CreateIndex(
                name: "IX_bsd_form_master_parent_id",
                table: "bsd_form_master",
                column: "parent_id");

            migrationBuilder.CreateIndex(
                name: "IX_bsd_form_template_master_id",
                table: "bsd_form_template",
                column: "master_id");

            migrationBuilder.CreateIndex(
                name: "IX_bsd_metadata_field_template_relationship_metadata_field_id",
                table: "bsd_metadata_field_template_relationship",
                column: "metadata_field_id");

            migrationBuilder.CreateIndex(
                name: "IX_bsd_metadata_field_template_relationship_metadata_template_id",
                table: "bsd_metadata_field_template_relationship",
                column: "metadata_template_id");

            migrationBuilder.CreateIndex(
                name: "IX_bsd_navigation_application_id",
                table: "bsd_navigation",
                column: "application_id");

            migrationBuilder.CreateIndex(
                name: "IX_bsd_navigation_parent_id",
                table: "bsd_navigation",
                column: "parent_id");

            migrationBuilder.CreateIndex(
                name: "IX_bsd_navigation_map_role_navigation_id",
                table: "bsd_navigation_map_role",
                column: "navigation_id");

            migrationBuilder.CreateIndex(
                name: "IX_bsd_parameter_application_id",
                table: "bsd_parameter",
                column: "application_id");

            migrationBuilder.CreateIndex(
                name: "IX_bsd_taxonomy_term_vocabulary_id",
                table: "bsd_taxonomy_term",
                column: "vocabulary_id");

            migrationBuilder.CreateIndex(
                name: "IX_Cart_Product_CartId",
                table: "Cart_Product",
                column: "CartId");

            migrationBuilder.CreateIndex(
                name: "IX_Cart_Product_ProductId",
                table: "Cart_Product",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_Category_ParentId",
                table: "Category",
                column: "ParentId");

            migrationBuilder.CreateIndex(
                name: "IX_Category_Meta_Product_CategoryMetaId",
                table: "Category_Meta_Product",
                column: "CategoryMetaId");

            migrationBuilder.CreateIndex(
                name: "IX_Category_Meta_Product_ProductId",
                table: "Category_Meta_Product",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_Category_Product_CategoryId",
                table: "Category_Product",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_Category_Product_ProductId",
                table: "Category_Product",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_cms_organization_application_id",
                table: "cms_organization",
                column: "application_id");

            migrationBuilder.CreateIndex(
                name: "IX_cms_organization_parent_id",
                table: "cms_organization",
                column: "parent_id");

            migrationBuilder.CreateIndex(
                name: "IX_cms_position_application_id",
                table: "cms_position",
                column: "application_id");

            migrationBuilder.CreateIndex(
                name: "IX_cms_sign_profile_application_id",
                table: "cms_sign_profile",
                column: "application_id");

            migrationBuilder.CreateIndex(
                name: "IX_idm_api_map_role_api_id",
                table: "idm_api_map_role",
                column: "api_id");

            migrationBuilder.CreateIndex(
                name: "IX_idm_api_map_role_application_id",
                table: "idm_api_map_role",
                column: "application_id");

            migrationBuilder.CreateIndex(
                name: "IX_idm_api_map_role_role_id",
                table: "idm_api_map_role",
                column: "role_id");

            migrationBuilder.CreateIndex(
                name: "IX_idm_device_application_id",
                table: "idm_device",
                column: "application_id");

            migrationBuilder.CreateIndex(
                name: "IX_idm_right_map_role_right_id",
                table: "idm_right_map_role",
                column: "right_id");

            migrationBuilder.CreateIndex(
                name: "IX_idm_right_map_role_role_id",
                table: "idm_right_map_role",
                column: "role_id");

            migrationBuilder.CreateIndex(
                name: "IX_idm_right_map_user_right_id",
                table: "idm_right_map_user",
                column: "right_id");

            migrationBuilder.CreateIndex(
                name: "IX_idm_right_map_user_user_id",
                table: "idm_right_map_user",
                column: "user_id");

            migrationBuilder.CreateIndex(
                name: "IX_idm_role_map_organization_organization_id",
                table: "idm_role_map_organization",
                column: "organization_id");

            migrationBuilder.CreateIndex(
                name: "IX_idm_role_map_organization_role_id",
                table: "idm_role_map_organization",
                column: "role_id");

            migrationBuilder.CreateIndex(
                name: "IX_idm_token_application_id",
                table: "idm_token",
                column: "application_id");

            migrationBuilder.CreateIndex(
                name: "IX_idm_user_map_org_application_id",
                table: "idm_user_map_org",
                column: "application_id");

            migrationBuilder.CreateIndex(
                name: "IX_idm_user_map_org_organization_id",
                table: "idm_user_map_org",
                column: "organization_id");

            migrationBuilder.CreateIndex(
                name: "IX_idm_user_map_org_user_id",
                table: "idm_user_map_org",
                column: "user_id");

            migrationBuilder.CreateIndex(
                name: "IX_idm_user_map_role_role_id",
                table: "idm_user_map_role",
                column: "role_id");

            migrationBuilder.CreateIndex(
                name: "IX_idm_user_map_role_user_id",
                table: "idm_user_map_role",
                column: "user_id");

            migrationBuilder.CreateIndex(
                name: "IX_log_checkin_account_account_id",
                table: "log_checkin_account",
                column: "account_id");

            migrationBuilder.CreateIndex(
                name: "IX_Notification_UserId",
                table: "Notification",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Order_UserId",
                table: "Order",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Order_Product_OrderId",
                table: "Order_Product",
                column: "OrderId");

            migrationBuilder.CreateIndex(
                name: "IX_Order_Product_ProductId",
                table: "Order_Product",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_Picture_ProductId",
                table: "Picture",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_Product_SupplierId",
                table: "Product",
                column: "SupplierId");

            migrationBuilder.CreateIndex(
                name: "IX_Product_Review_ParentId",
                table: "Product_Review",
                column: "ParentId");

            migrationBuilder.CreateIndex(
                name: "IX_Product_Review_ProductId",
                table: "Product_Review",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_Product_Review_UserId",
                table: "Product_Review",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductTag_ProductId",
                table: "ProductTag",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductTag_TagId",
                table: "ProductTag",
                column: "TagId");

            migrationBuilder.CreateIndex(
                name: "IX_tms_cert_user_application_id",
                table: "tms_cert_user",
                column: "application_id");

            migrationBuilder.CreateIndex(
                name: "IX_tms_certification_application_id",
                table: "tms_certification",
                column: "application_id");

            migrationBuilder.CreateIndex(
                name: "IX_User_CartId",
                table: "User",
                column: "CartId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_WorkflowApplication_application_id",
                table: "WorkflowApplication",
                column: "application_id");

            migrationBuilder.CreateIndex(
                name: "IX_WorkflowDocument_application_id",
                table: "WorkflowDocument",
                column: "application_id");

            migrationBuilder.CreateIndex(
                name: "IX_WorkflowDocumentAttachment_application_id",
                table: "WorkflowDocumentAttachment",
                column: "application_id");

            migrationBuilder.CreateIndex(
                name: "IX_WorkflowDocumentAttachment_WorkflowDocumentId",
                table: "WorkflowDocumentAttachment",
                column: "WorkflowDocumentId");

            migrationBuilder.CreateIndex(
                name: "IX_WorkflowDocumentHistory_WorkflowDocumentId",
                table: "WorkflowDocumentHistory",
                column: "WorkflowDocumentId");

            migrationBuilder.CreateIndex(
                name: "IX_WorkflowSchemeAttribute_application_id",
                table: "WorkflowSchemeAttribute",
                column: "application_id");

            migrationBuilder.CreateIndex(
                name: "IX_WorkflowSchemeVersion_application_id",
                table: "WorkflowSchemeVersion",
                column: "application_id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "bsd_catalog_field");

            migrationBuilder.DropTable(
                name: "bsd_catalog_item_attribute");

            migrationBuilder.DropTable(
                name: "bsd_form_control");

            migrationBuilder.DropTable(
                name: "bsd_form_template");

            migrationBuilder.DropTable(
                name: "bsd_log");

            migrationBuilder.DropTable(
                name: "bsd_metadata_field_template_relationship");

            migrationBuilder.DropTable(
                name: "bsd_navigation_map_role");

            migrationBuilder.DropTable(
                name: "bsd_parameter");

            migrationBuilder.DropTable(
                name: "bsd_taxonomy_vocabulary_types");

            migrationBuilder.DropTable(
                name: "Cart_Product");

            migrationBuilder.DropTable(
                name: "Category_Meta_Product");

            migrationBuilder.DropTable(
                name: "Category_Product");

            migrationBuilder.DropTable(
                name: "Cities");

            migrationBuilder.DropTable(
                name: "cms_position");

            migrationBuilder.DropTable(
                name: "cms_sign_profile");

            migrationBuilder.DropTable(
                name: "Communes");

            migrationBuilder.DropTable(
                name: "Districts");

            migrationBuilder.DropTable(
                name: "idm_api_map_role");

            migrationBuilder.DropTable(
                name: "idm_client");

            migrationBuilder.DropTable(
                name: "idm_device");

            migrationBuilder.DropTable(
                name: "idm_otp");

            migrationBuilder.DropTable(
                name: "idm_qrcode");

            migrationBuilder.DropTable(
                name: "idm_right_map_role");

            migrationBuilder.DropTable(
                name: "idm_right_map_user");

            migrationBuilder.DropTable(
                name: "idm_role_map_organization");

            migrationBuilder.DropTable(
                name: "idm_token");

            migrationBuilder.DropTable(
                name: "idm_user_map_org");

            migrationBuilder.DropTable(
                name: "idm_user_map_role");

            migrationBuilder.DropTable(
                name: "log_authentication");

            migrationBuilder.DropTable(
                name: "log_checkin_account");

            migrationBuilder.DropTable(
                name: "log_signing");

            migrationBuilder.DropTable(
                name: "Notification");

            migrationBuilder.DropTable(
                name: "Order_Product");

            migrationBuilder.DropTable(
                name: "Picture");

            migrationBuilder.DropTable(
                name: "Product_Review");

            migrationBuilder.DropTable(
                name: "ProductTag");

            migrationBuilder.DropTable(
                name: "sign_log");

            migrationBuilder.DropTable(
                name: "tms_cert_user");

            migrationBuilder.DropTable(
                name: "tms_certification");

            migrationBuilder.DropTable(
                name: "VisitWebsites");

            migrationBuilder.DropTable(
                name: "Vouchers");

            migrationBuilder.DropTable(
                name: "WorkflowApplication");

            migrationBuilder.DropTable(
                name: "WorkflowDocumentAttachment");

            migrationBuilder.DropTable(
                name: "WorkflowDocumentHistory");

            migrationBuilder.DropTable(
                name: "WorkflowGlobalParameter");

            migrationBuilder.DropTable(
                name: "WorkflowInbox");

            migrationBuilder.DropTable(
                name: "WorkflowProcessInstance");

            migrationBuilder.DropTable(
                name: "WorkflowProcessInstancePersistence");

            migrationBuilder.DropTable(
                name: "WorkflowProcessInstanceStatus");

            migrationBuilder.DropTable(
                name: "WorkflowProcessScheme");

            migrationBuilder.DropTable(
                name: "WorkflowProcessTimer");

            migrationBuilder.DropTable(
                name: "WorkflowProcessTransitionHistory");

            migrationBuilder.DropTable(
                name: "WorkflowScheme");

            migrationBuilder.DropTable(
                name: "WorkflowSchemeAttribute");

            migrationBuilder.DropTable(
                name: "WorkflowSchemeVersion");

            migrationBuilder.DropTable(
                name: "bsd_catalog_master");

            migrationBuilder.DropTable(
                name: "bsd_catalog_item");

            migrationBuilder.DropTable(
                name: "bsd_form_master");

            migrationBuilder.DropTable(
                name: "bsd_metadata_field");

            migrationBuilder.DropTable(
                name: "bsd_navigation");

            migrationBuilder.DropTable(
                name: "Category_Meta");

            migrationBuilder.DropTable(
                name: "Category");

            migrationBuilder.DropTable(
                name: "idm_api");

            migrationBuilder.DropTable(
                name: "idm_right");

            migrationBuilder.DropTable(
                name: "cms_organization");

            migrationBuilder.DropTable(
                name: "idm_role");

            migrationBuilder.DropTable(
                name: "idm_user");

            migrationBuilder.DropTable(
                name: "crm_account");

            migrationBuilder.DropTable(
                name: "Order");

            migrationBuilder.DropTable(
                name: "Product");

            migrationBuilder.DropTable(
                name: "Tag");

            migrationBuilder.DropTable(
                name: "WorkflowDocument");

            migrationBuilder.DropTable(
                name: "bsd_metadata_template");

            migrationBuilder.DropTable(
                name: "bsd_taxonomy_term");

            migrationBuilder.DropTable(
                name: "User");

            migrationBuilder.DropTable(
                name: "Supplier");

            migrationBuilder.DropTable(
                name: "sys_appliation");

            migrationBuilder.DropTable(
                name: "bsd_taxonomy_vocabulary");

            migrationBuilder.DropTable(
                name: "Cart");
        }
    }
}
