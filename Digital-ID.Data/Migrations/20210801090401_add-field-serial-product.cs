﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DigitalID.Data.Migrations
{
    public partial class addfieldserialproduct : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "SerialNumber",
                table: "Product",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "SerialNumber",
                table: "Product");
        }
    }
}
