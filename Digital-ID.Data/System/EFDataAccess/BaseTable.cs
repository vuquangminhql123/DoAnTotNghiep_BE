using System.ComponentModel.DataAnnotations;
using System;
using System.ComponentModel.DataAnnotations.Schema;
namespace DigitalID.Data
{

    public class BaseTableDefault
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [Column("last_modified_on_date", Order = 100)]
        public DateTime? LastModifiedOnDate { get; set; } = DateTime.Now;

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("created_on_date", Order = 101)]
        public DateTime? CreatedOnDate { get; set; } = DateTime.Now;

        [Column("company_id", Order = 105)]
        public Guid? CompanyId { get; set; }
    }
    public class BaseTable<T> where T : BaseTable<T>
    {
        #region Init Create/Update
        public T InitCreate()
        {
            return InitCreate(AppConstants.RootAppId, UserConstants.AdministratorId);
        }
        public T InitCreate(Guid? application, Guid? userId)
        {
            return InitCreate(application ?? AppConstants.RootAppId, userId ?? UserConstants.AdministratorId);
        }
        public T InitCreate(Guid? application, Guid userId)
        {
            return InitCreate(application ?? AppConstants.RootAppId, userId);
        }
        public T InitCreate(Guid application, Guid? userId)
        {
            return InitCreate(application, userId ?? UserConstants.AdministratorId);
        }
        public T InitCreate(Guid application, Guid userId)
        {
            ApplicationId = application;
            CreatedByUserId = userId;
            LastModifiedByUserId = userId;
            CreatedOnDate = DateTime.Now;
            LastModifiedOnDate = DateTime.Now;
            return (T)this;
        }
        public T InitUpdate()
        {
            return InitUpdate(AppConstants.RootAppId, UserConstants.AdministratorId);
        }
        public T InitUpdate(Guid? application, Guid? userId)
        {
            return InitUpdate(application ?? AppConstants.RootAppId, userId ?? UserConstants.AdministratorId);
        }
        public T InitUpdate(Guid? application, Guid userId)
        {
            return InitUpdate(application ?? AppConstants.RootAppId, userId);
        }
        public T InitUpdate(Guid application, Guid? userId)
        {
            return InitUpdate(application, userId ?? UserConstants.AdministratorId);
        }
        public T InitUpdate(Guid application, Guid userId)
        {
            ApplicationId = application;
            LastModifiedByUserId = userId;
            LastModifiedOnDate = DateTime.Now;
            return (T)this;
        }
        #endregion
        [Column("created_by_user_id", Order = 100)]
        public Guid CreatedByUserId { get; set; } = UserConstants.AdministratorId;

        [Column("created_on_date", Order = 101)]
        public DateTime CreatedOnDate { get; set; } = DateTime.Now;

        [Column("last_modified_by_user_id", Order = 102)]
        public Guid LastModifiedByUserId { get; set; } = UserConstants.AdministratorId;

        [Column("last_modified_on_date", Order = 103)]
        public DateTime LastModifiedOnDate { get; set; } = DateTime.Now;

        [ForeignKey("Application")]
        [Column("application_id", Order = 104)]
        public virtual Guid ApplicationId { get; set; } = AppConstants.RootAppId;

        public virtual SysApplication Application { get; set; }

        [Column("company_id", Order = 105)]
        public Guid? CompanyId { get; set; }
    }

    public class BaseTableCompanyAndMoreDefault : BaseTableCompanyDefault
    {
        //indentity_number	number	bigint
        [Column("identity_number", Order = 96)]
        public long IdentityNumber { get; set; }

        //status	NO	bit
        [Column("status_default", Order = 97)]
        public bool StatusDefault { get; set; }

        //order	NO	int
        [Column("order", Order = 98)]
        public int Order { get; set; }

        //description	YES	nvarchar
        [Column("description", Order = 99)]
        public string Description { get; set; }
    }

    public class BaseTableCompanyDefault: BaseCompanyDefault
    {
        [Column("created_date", Order = 100)]
        public DateTime? CreatedDate { get; set; } = DateTime.Now;
            
        [Column("created_user_id", Order = 101)]
        public Guid? CreatedUserId { get; set; } = UserConstants.AdministratorId;

        [Column("modified_date", Order = 102)]
        public DateTime? ModifiedDate { get; set; } = DateTime.Now;

        [Column("modified_user_id", Order = 103)]
        public Guid? ModifiedUserId { get; set; } = UserConstants.AdministratorId;

        //[Column("application_id", Order = 104)]
        //public Guid? ApplicationId { get; set; } = AppConstants.RootAppId;

        //[Column("company_id", Order = 105)]
        //public Guid? CompanyId { get; set; }
    }

    public class BaseCompanyDefault
    {
        [Column("application_id", Order = 104)]
        public Guid? ApplicationId { get; set; } = AppConstants.RootAppId;

        [Column("company_id", Order = 105)]
        public Guid? CompanyId { get; set; }
    }


}
