﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DigitalID.Data
{
    public class CategoryMetaConfiguration:IEntityTypeConfiguration<Category_Meta>
    {
        public void Configure(EntityTypeBuilder<Category_Meta> builder)
        {
            builder.ToTable("Category_Meta");
            builder.HasKey(x => x.Id);
        }
    }
}
