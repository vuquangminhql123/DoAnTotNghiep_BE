﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DigitalID.Data
{
    public class CategoryMetaProductConfiguration:IEntityTypeConfiguration<Category_Meta_Product>
    {
        public void Configure(EntityTypeBuilder<Category_Meta_Product> builder)
        {
            builder.ToTable("Category_Meta_Product");
            builder.HasKey(x => x.Id);
            builder.HasOne(x => x.CategoryMeta).WithMany(x => x.CategoryMetaProducts).HasForeignKey(x => x.CategoryMetaId).OnDelete(DeleteBehavior.Cascade);
            builder.HasOne(x => x.Product).WithMany(x => x.CategoryMetaProducts).HasForeignKey(x => x.ProductId).OnDelete(DeleteBehavior.Cascade);
        }
    }
}
