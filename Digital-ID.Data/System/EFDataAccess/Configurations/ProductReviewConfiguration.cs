﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DigitalID.Data
{
    public class ProductReviewConfiguration:IEntityTypeConfiguration<Product_Review>
    {
        public void Configure(EntityTypeBuilder<Product_Review> builder)
        {
            builder.ToTable("Product_Review");
            builder.HasKey(x => x.Id);
            builder.HasOne(x => x.Product).WithMany(x => x.ProductReviews).HasForeignKey(x => x.ProductId).OnDelete(DeleteBehavior.Cascade);
            builder.HasOne(x => x.User).WithMany(x => x.ProductReviews).HasForeignKey(x => x.UserId).OnDelete(DeleteBehavior.Cascade);
            builder.HasOne(x => x.ProductReview).WithMany(x => x.ProductReviews).HasForeignKey(x => x.ParentId);
        }
    }
}
