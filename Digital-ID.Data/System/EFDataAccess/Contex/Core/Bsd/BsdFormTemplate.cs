using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DigitalID.Data
{


    [Table("bsd_form_template")]
    public partial class BsdFormTemplate  
    {
        [Key]
        [Column("id")]
        public Guid Id { get; set; }

        [ForeignKey("FormMaster")]
        [Column("master_id")]
        public Guid MasterId { get; set; }

        [Column("name")]
        public string Name { get; set; }

        [Column("note")]
        public string Note { get; set; }

        [Column("html")]
        public string Html { get; set; }

        public BsdFormMaster FormMaster { get; set; }

    }
}
