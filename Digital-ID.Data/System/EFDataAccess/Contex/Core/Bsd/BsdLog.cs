﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace DigitalID.Data
{
    [Table("bsd_log")]
    public class BsdLog
    {
        /// <summary>
        /// Id tự sinh
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        [Column("id")]
        public long Id { get; set; }

        /// <summary>
        /// Nội dung mô tả log
        /// </summary>
        [Column("message")]
        public string Message { get; set; }

        [Column("level")]
        public byte? Level { get; set; }

        /// <summary>
        /// Thời gian ghi log
        /// </summary>
        [Column("timestamp")]
        public DateTime Timestamp { get; set; }

        [Column("exception")]
        public string Exception { get; set; }

        /// <summary>
        /// AppId
        /// </summary>
        [Column("application_id")]
        public Guid? ApplicationId { get; set; }

        /// <summary>
        /// Id người dùng
        /// </summary>
        [Column("user_id")]
        public Guid? UserId { get; set; }

        /// <summary>
        /// Tên người dùng
        /// </summary>
        [Column("user_name")]
        public string UserName { get; set; }

        /// <summary>
        /// Hành động: thêm mới, update, xóa
        /// </summary>
        [Column("action")]
        public string Action { get; set; }

        /// <summary>
        /// Chức năng
        /// </summary>
        [Column("module")]
        public string Module { get; set; }

        [Column("message_template")]
        public string MessageTemplate { get; set; }

        /// <summary>
        /// Mô tả chi tiết
        /// </summary>
        [Column("properties")]
        public string Properties { get; set; }

    }
}

