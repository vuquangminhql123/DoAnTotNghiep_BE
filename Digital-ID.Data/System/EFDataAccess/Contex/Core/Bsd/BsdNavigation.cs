﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DigitalID.Data
{
    [Table("bsd_navigation")]
    public  class BsdNavigation: BaseTable<BsdNavigation>
    {
        public BsdNavigation()
        {
            NavigationRole = new HashSet<BsdNavigationMapRole>();
            InverseParent = new HashSet<BsdNavigation>();
        }
        [Key]
        [Column("id")]
        public Guid Id { get; set; }

        [ForeignKey("Parent")]
        [Column("parent_id")]
        public Guid? ParentId { get; set; }

        [Required]
        [StringLength(64)]
        [Column("code")]
        public string Code { get; set; }

        [Required]
        [StringLength(128)]
        [Column("name")]
        public string Name { get; set; }

        [StringLength(128)]
        [Column("url_rewrite")]
        public string UrlRewrite { get; set; }

        [Required]
        [StringLength(450)]
        [Column("id_path")]
        public string IdPath { get; set; }

        [Required]
        [StringLength(900)]
        [Column("path")]
        public string Path { get; set; }

        [StringLength(1024)]
        [Column("sub_url")]
        public string SubUrl { get; set; }

        [StringLength(64)]
        [Column("icon_class")]
        public string IconClass { get; set; }

        [Column("status")]
        public bool? Status { get; set; }

        [Column("order")]
        public int? Order { get; set; }

        [Column("has_child")]
        public bool HasChild { get; set; }

        [Column("level")]
        public int Level { get; set; }


        public BsdNavigation Parent { get; set; }
        [InverseProperty("Parent")]
        public ICollection<BsdNavigation> InverseParent { get; set; }
        public ICollection<BsdNavigationMapRole> NavigationRole { get; set; }
    }
}
