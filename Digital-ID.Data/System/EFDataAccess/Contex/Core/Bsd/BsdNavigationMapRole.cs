﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DigitalID.Data
{
    [Table("bsd_navigation_map_role")]
    public class BsdNavigationMapRole: BaseTableDefault
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        [Column("id")]
        public long Id { get; set; }

        [ForeignKey("Navigation")]
        [Column("navigation_id")]
        public Guid NavigationId { get; set; }

        [Column("role_id")]
        public Guid RoleId { get; set; }

        [Column("from_sub_navigation")]
        public Guid? FromSubNavigation { get; set; }

        public BsdNavigation Navigation { get; set; }
    }
}
