using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace DigitalID.Data
{
    [Table("bsd_parameter")]
    public  class BsdParameter : BaseTable<BsdParameter>
    {
        [Key]
        [Column("id")]
        public Guid Id { get; set; }

        [Required]
        [StringLength(64)]
        [Column("name")]
        public string Name { get; set; }

        [StringLength(1024)]
        [Column("description")]
        public string Description { get; set; }

        [StringLength(128)]
        [Column("value")]
        public string Value { get; set; }

    }
}
