﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DigitalID.Data
{
    [Table("bsd_catalog_field")]
    public partial class CatalogField
    {
        [Key]
        [Column("id")]
        public Guid FieldId { get; set; }
        [Required]
        [StringLength(256)]
        [Column("name")]
        public string Name { get; set; }
        [Required]
        [StringLength(256)]
        [Column("code")]
        public string Code { get; set; }
        [Column("description")]
        public string Description { get; set; }
        [Required]
        [Column("formly_content")]
        public string FormlyContent { get; set; }
        [Column("is_record_common_field")]
        public bool IsRecordCommonField { get; set; }
        [Column("is_document_common_field")]
        public bool IsDocumentCommonField { get; set; }
        [Column("created_on_date", TypeName = "datetime")]
        public DateTime CreatedOnDate { get; set; }
        [Column("last_modified_on_date", TypeName = "datetime")]
        public DateTime LastModifiedOnDate { get; set; }
        [Column("created_by_user_id")]
        public Guid CreatedByUserId { get; set; }
        [Column("last_modified_by_user_id")]
        public Guid LastModifiedByUserId { get; set; }
        [Required]
        [Column("display_catalog")]
        public bool? DisplayCatalog { get; set; }
        [Required]
        [Column("display_report")]
        public bool? DisplayReport { get; set; }
        [Required]
        [Column("display_reader")]
        public bool? DisplayReader { get; set; }
        [Column("display_report_search")]
        public bool DisplayReportSearch { get; set; }
        [Column("display_reader_search")]
        public bool DisplayReaderSearch { get; set; }
        [Column("catalog_master_id")]
        public Guid? CatalogMasterId { get; set; }
        [Column("field_width")]
        public int? FieldWidth { get; set; }
        [StringLength(100)]
        [Column("data_type")]
        public string DataType { get; set; }
        [StringLength(100)]
        [Column("catalog_code")]
        public string CatalogCode { get; set; }
        [Column("field_name_new")]
        [StringLength(50)]
        public string FieldNameNew { get; set; }
        [StringLength(256)]
        [Column("display_name")]
        public string DisplayName { get; set; }

        [ForeignKey("CatalogMasterId")]
        [InverseProperty("CatalogField")]
        public CatalogMaster CatalogMaster { get; set; }
    }
}
