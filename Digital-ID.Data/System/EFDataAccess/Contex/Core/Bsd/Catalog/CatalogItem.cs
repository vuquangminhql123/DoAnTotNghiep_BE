﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DigitalID.Data
{
    [Table("bsd_catalog_item")]
    public partial class CatalogItem
    {
        public CatalogItem()
        {
            CatalogItemAttribute = new HashSet<CatalogItemAttribute>();
        }

        [Key]
        [Column("id")]
        public Guid CatalogItemId { get; set; }

        [Column("mapped_term_id")]
        public Guid MappedTermId { get; set; }

        [Required]
        [StringLength(256)]
        [Column("name")]
        public string Name { get; set; }

        [Column("status")]
        public bool Status { get; set; }

        [Column("created_by_user_id")]
        public Guid CreatedByUserId { get; set; }

        [Column("created_on_date", TypeName = "datetime")]
        public DateTime CreatedOnDate { get; set; }

        [Column("last_modified_by_user_id")]
        public Guid LastModifiedByUserId { get; set; }

        [Column("last_modified_on_date", TypeName = "datetime")]
        public DateTime LastModifiedOnDate { get; set; }

        [Required]
        [StringLength(256)]
        [Column("code")]
        public string Code { get; set; }

        [Column("description")]
        public string Description { get; set; }

        [Column("order")]
        public int Order { get; set; }

        [ForeignKey("MappedTermId")]
        [InverseProperty("CatalogItem")]
        public TaxonomyTerm MappedTerm { get; set; }

        [InverseProperty("CatalogItem")]
        public ICollection<CatalogItemAttribute> CatalogItemAttribute { get; set; }
    }
}
