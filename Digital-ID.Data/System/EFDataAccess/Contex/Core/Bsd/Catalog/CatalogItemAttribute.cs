﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DigitalID.Data
{
    [Table("bsd_catalog_item_attribute")]
    public partial class CatalogItemAttribute
    {
        [Key]
        [Column("id")]
        public Guid CatalogItemAttributeId { get; set; }

        [Column("catalog_item_id")]
        public Guid CatalogItemId { get; set; }

        [Column("metadata_field_id")]
        public Guid MetadataFieldId { get; set; }

        [Required]
        [StringLength(256)]
        [Column("metadata_field_name")]
        public string MetadataFieldName { get; set; }

        [Column("nvarchar_value")]
        public string NvarcharValue { get; set; }

        [Column("int_value")]
        public int? IntValue { get; set; }

        [Column("datetime_value", TypeName = "datetime")]
        public DateTime? DatetimeValue { get; set; }

        [Column("bit_value")]
        public bool? BitValue { get; set; }

        [Column("varchar_value")]
        public string VarcharValue { get; set; }

        [Column("guid_value")]
        public Guid? GuidValue { get; set; }

        [StringLength(50)]
        [Column("data_type")]
        public string DataType { get; set; }

        [Required]
        [StringLength(256)]
        [Column("metadata_field_code")]
        public string MetadataFieldCode { get; set; }

        [Column("json_value")]
        public string JsonValue { get; set; }

        [Column("json_guid_value")]
        public string JsonGuidValue { get; set; }

        [Column("time_value", TypeName = "datetime")]
        public DateTime? TimeValue { get; set; }

        [Column("date_value", TypeName = "date")]
        public DateTime? DateValue { get; set; }

        [Column("year_value", TypeName = "datetime")]
        public DateTime? YearValue { get; set; }

        [Column("month_value", TypeName = "datetime")]
        public DateTime? MonthValue { get; set; }

        [Column("type_date_time")]
        public int? TypeDateTime { get; set; }

        [ForeignKey("CatalogItemId")]
        [InverseProperty("CatalogItemAttribute")]
        public CatalogItem CatalogItem { get; set; }

        [ForeignKey("MetadataFieldId")]
        [InverseProperty("CatalogItemAttribute")]
        public MetadataField MetadataField { get; set; }
    }
}
