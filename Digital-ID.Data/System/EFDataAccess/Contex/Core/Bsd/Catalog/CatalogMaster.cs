﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DigitalID.Data
{
    [Table("bsd_catalog_master")]
    public partial class CatalogMaster
    {
        public CatalogMaster()
        {
            CatalogField = new HashSet<CatalogField>();
        }

        [Key]
        [Column("id")]
        public Guid CatalogMasterId { get; set; }

        [Column("metadata_template_id")]
        public Guid MetadataTemplateId { get; set; }

        [Column("mapped_term_id")]
        public Guid MappedTermId { get; set; }

        [Column("mapped_vocabulary_id")]
        public Guid MappedVocabularyId { get; set; }

        [Required]
        [StringLength(256)]
        [Column("name")]
        public string Name { get; set; }

        [StringLength(256)]
        [Column("code")]
        public string Code { get; set; }

        [Column("created_by_user_id")]
        public Guid? CreatedByUserId { get; set; }

        [Column("created_on_date", TypeName = "datetime")]
        public DateTime? CreatedOnDate { get; set; }

        [Column("last_modified_by_user_id")]
        public Guid? LastModifiedByUserId { get; set; }

        [Column("last_modified_on_date", TypeName = "datetime")]
        public DateTime? LastModifiedOnDate { get; set; }

        [Required]
        [Column("status")]
        public bool? Status { get; set; }

        [Column("description")]
        public string Description { get; set; }

        [ForeignKey("MappedTermId")]
        [InverseProperty("CatalogMaster")]
        public TaxonomyTerm MappedTerm { get; set; }

        [ForeignKey("MappedVocabularyId")]
        [InverseProperty("CatalogMaster")]
        public TaxonomyVocabulary MappedVocabulary { get; set; }

        [ForeignKey("MetadataTemplateId")]
        [InverseProperty("CatalogMaster")]
        public MetadataTemplate MetadataTemplate { get; set; }

        [InverseProperty("CatalogMaster")]
        public ICollection<CatalogField> CatalogField { get; set; }
    }
}
