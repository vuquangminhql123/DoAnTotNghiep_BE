using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace DigitalID.Data
{
    [Table("bsd_form_control")]
    public partial class BsdFormControl :BaseTable<BsdFormControl>
    {
        [Key]
        [Column("id")]
        public Guid Id { get; set; }

        [Column("name")]
        public string Name { get; set; }

        [Column("note")]
        public string Note { get; set; }

        [Column("content")]
        public string Content { get; set; }

    }
}
