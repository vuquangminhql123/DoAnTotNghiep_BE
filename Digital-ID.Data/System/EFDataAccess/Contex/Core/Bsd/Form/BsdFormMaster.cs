using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DigitalID.Data
{
    [Table("bsd_form_master")]
    public partial class BsdFormMaster :BaseTable<BsdFormMaster>
    {
        public BsdFormMaster()
        {
            FormTemplates = new HashSet<BsdFormTemplate>();
        }
        [Key]
        [Column("id")]
        public Guid Id { get; set; }

        [ForeignKey("Parent")]
        [Column("parent_id")]
        public Guid? ParentId { get; set; }

        [Column("content")]
        public string Content { get; set; }

        [Column("name")]
        public string Name { get; set; }

        [Column("table_name")]
        public string TableName { get; set; }

        [Column("state")]
        public string State { get; set; }

        public ICollection<BsdFormTemplate> FormTemplates { get; set; }
        public BsdFormMaster Parent { get; set; }

    }
}
