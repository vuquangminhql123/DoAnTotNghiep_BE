﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DigitalID.Data;

namespace DigitalID.Data
{
    [Table("bsd_metadata_field")]
    public partial class MetadataField
    {
        public MetadataField()
        {
            CatalogItemAttribute = new HashSet<CatalogItemAttribute>();
            MetadataFieldTemplateRelationship = new HashSet<MetadataFieldTemplateRelationship>();
        }

        [Key]
        [Column("id")]
        public Guid MetadataFieldId { get; set; }

        [Required]
        [StringLength(512)]
        [Column("name")]
        public string Name { get; set; }

        [StringLength(50)]
        [Column("data_type")]
        public string DataType { get; set; }

        [Required]
        [Column("formly_content")]
        public string FormlyContent { get; set; }

        [Column("description")]
        public string Description { get; set; }

        [Column("created_by_user_id")]
        public Guid CreatedByUserId { get; set; }

        [Column("created_on_date", TypeName = "datetime")]
        public DateTime CreatedOnDate { get; set; }

        [Column("last_modified_by_user_id")]
        public Guid LastModifiedByUserId { get; set; }

        [Column("last_modified_on_date", TypeName = "datetime")]
        public DateTime LastModifiedOnDate { get; set; }

        [Column("application_id")]
        public Guid? ApplicationId { get; set; }

        [Required]
        [StringLength(256)]
        [Column("code")]
        public string Code { get; set; }


        [InverseProperty("MetadataField")]
        public ICollection<CatalogItemAttribute> CatalogItemAttribute { get; set; }
        [InverseProperty("MetadataField")]
        public ICollection<MetadataFieldTemplateRelationship> MetadataFieldTemplateRelationship { get; set; }
    }
}
