﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DigitalID.Data
{
    [Table("bsd_metadata_field_template_relationship")]
    public partial class MetadataFieldTemplateRelationship
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        [Column("id")]
        public long Id { get; set; }

        [Column("metadata_template_id")]
        public Guid MetadataTemplateId { get; set; }

        [Column("metadata_field_id")]
        public Guid MetadataFieldId { get; set; }

        [Column("order")]
        public int Order { get; set; }

        [Column("status")]
        public int Status { get; set; }


        [ForeignKey("MetadataFieldId")]
        [InverseProperty("MetadataFieldTemplateRelationship")]
        public MetadataField MetadataField { get; set; }
        [ForeignKey("MetadataTemplateId")]
        [InverseProperty("MetadataFieldTemplateRelationship")]
        public MetadataTemplate MetadataTemplate { get; set; }
    }
}
