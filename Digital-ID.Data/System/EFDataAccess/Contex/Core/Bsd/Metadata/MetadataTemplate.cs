﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DigitalID.Data
{
    [Table("bsd_metadata_template")]
    public partial class MetadataTemplate
    {
        public MetadataTemplate()
        {
            CatalogMaster = new HashSet<CatalogMaster>();
            MetadataFieldTemplateRelationship = new HashSet<MetadataFieldTemplateRelationship>();
        }

        [Key]
        [Column("id")]
        public Guid MetadataTemplateId { get; set; }

        [Required]
        [StringLength(512)]
        [Column("code")]
        public string Code { get; set; }

        [Required]
        [StringLength(512)]
        [Column("name")]
        public string Name { get; set; }

        [Column("description")]
        public string Description { get; set; }

        [Column("created_by_user_id")]
        public Guid CreatedByUserId { get; set; }

        [Column("created_on_date", TypeName = "datetime")]
        public DateTime CreatedOnDate { get; set; }

        [Column("last_modified_by_user_id")]
        public Guid LastModifiedByUserId { get; set; }

        [Column("last_modified_on_date", TypeName = "datetime")]
        public DateTime LastModifiedOnDate { get; set; }

        [Column("application_id")]
        public Guid? ApplicationId { get; set; }

        [Column("form_config")]
        public string FormConfig { get; set; }


        [InverseProperty("MetadataTemplate")]
        public ICollection<CatalogMaster> CatalogMaster { get; set; }
        [InverseProperty("MetadataTemplate")]
        public ICollection<MetadataFieldTemplateRelationship> MetadataFieldTemplateRelationship { get; set; }
    }
}
