﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DigitalID.Data
{
    [Table("bsd_taxonomy_term")]
    public partial class TaxonomyTerm
    {
        public TaxonomyTerm()
        {
            CatalogItem = new HashSet<CatalogItem>();
            CatalogMaster = new HashSet<CatalogMaster>();  
        }

        [Key]
        [Column("id")]
        public Guid TermId { get; set; }

        [Column("vocabulary_id")]
        public Guid VocabularyId { get; set; }

        [Column("parent_id")]
        public Guid? ParentId { get; set; }

        [Required]
        [StringLength(256)]
        [Column("name")]
        public string Name { get; set; }

        [Column("description")]
        public string Description { get; set; }

        [Column("weight")]
        public int Weight { get; set; }

        [Column("term_left")]
        public Guid TermLeft { get; set; }

        [Column("term_right")]
        public Guid TermRight { get; set; }

        [Column("created_by_user_id")]
        public Guid? CreatedByUserId { get; set; }

        [Column("created_on_date", TypeName = "datetime")]
        public DateTime? CreatedOnDate { get; set; }

        [Column("last_modified_by_user_id")]
        public Guid? LastModifiedByUserId { get; set; }

        [Column("last_modified_on_date", TypeName = "datetime")]
        public DateTime? LastModifiedOnDate { get; set; }

        [Column("application_id")]
        public Guid ApplicationId { get; set; }

        [Column("order")]
        public int? Order { get; set; }

        [StringLength(256)]
        [Column("vocabulary_code")]
        public string VocabularyCode { get; set; }

        [Column("child_count")]
        public int? ChildCount { get; set; }

        [StringLength(900)]
        [Column("id_path")]
        public string IdPath { get; set; }

        [StringLength(450)]
        [Column("path")]
        public string Path { get; set; }

        [Column("level")]
        public int? Level { get; set; }

        [ForeignKey("VocabularyId")]
        [InverseProperty("TaxonomyTerm")]
        public TaxonomyVocabulary Vocabulary { get; set; }
        [InverseProperty("MappedTerm")]
        public ICollection<CatalogItem> CatalogItem { get; set; }
        [InverseProperty("MappedTerm")]
        public ICollection<CatalogMaster> CatalogMaster { get; set; }        
    }
}
