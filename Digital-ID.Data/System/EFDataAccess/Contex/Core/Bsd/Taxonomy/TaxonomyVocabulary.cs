﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DigitalID.Data
{
    [Table("bsd_taxonomy_vocabulary")]
    public partial class TaxonomyVocabulary
    {
        public TaxonomyVocabulary()
        {
            CatalogMaster = new HashSet<CatalogMaster>();
            TaxonomyTerm = new HashSet<TaxonomyTerm>();
        }

        [Key]
        [Column("id")]
        public Guid VocabularyId { get; set; }

        [Column("vocabulary_type_id")]
        public Guid VocabularyTypeId { get; set; }

        [Required]
        [StringLength(256)]
        [Column("name")]
        public string Name { get; set; }

        [Column("description")]
        public string Description { get; set; }

        [Column("weight")]
        public int Weight { get; set; }

        [Column("created_by_user_id")]
        public Guid? CreatedByUserId { get; set; }

        [Column("created_on_date", TypeName = "datetime")]
        public DateTime? CreatedOnDate { get; set; }

        [Column("last_modified_by_user_id")]
        public Guid? LastModifiedByUserId { get; set; }

        [Column("last_modified_on_date", TypeName = "datetime")]
        public DateTime? LastModifiedOnDate { get; set; }

        [Column("is_system")]
        public bool IsSystem { get; set; }

        [Column("application_id")]
        public Guid ApplicationId { get; set; }

        [StringLength(256)]
        [Column("code")]
        public string Code { get; set; }

        [InverseProperty("MappedVocabulary")]
        public ICollection<CatalogMaster> CatalogMaster { get; set; }
        [InverseProperty("Vocabulary")]
        public ICollection<TaxonomyTerm> TaxonomyTerm { get; set; }
    }
}
