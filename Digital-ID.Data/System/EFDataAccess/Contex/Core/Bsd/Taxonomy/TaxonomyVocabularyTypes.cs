﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DigitalID.Data
{
    [Table("bsd_taxonomy_vocabulary_types")]
    public partial class TaxonomyVocabularyTypes
    {
        [Key]
        [Column("id")]
        public Guid VocabularyTypeId { get; set; }

        [Required]
        [StringLength(64)]
        [Column("vocabulary_type")]
        public string VocabularyType { get; set; }

        [Column("application_id")]
        public Guid ApplicationId { get; set; }
    }
}
