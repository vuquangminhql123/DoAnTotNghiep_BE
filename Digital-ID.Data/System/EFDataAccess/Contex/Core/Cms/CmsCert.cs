﻿namespace DigitalID.Data
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("tms_certification")]
    public class CmsCert : BaseTable<CmsCert>
    {
        [Key]
        [Column("id")]
        public Guid Id { get; set; }

        [Column("supplier")]
        public string Supplier { get; set; }

        //Slot label
        [Required]
        [Column("slot_id")]
        public int? SlotID { get; set; }

        //PIN
        [Required]
        [Column("user_pin")]
        public string UserPin { get; set; }

        [Required]
        [Column("key")]
        public string Key { get; set; }

        [Required]
        [Column("alias")]
        public string Alias { get; set; }

        [Column("slot_label")]
        public string SlotLabel { get; set; }

        //Độ dài khóa
        [Column("key_length")]
        public string KeyLength { get; set; }

        //Thuật toán
        [Column("key_algorithm")]
        public string KeyAlgorithm { get; set; }

        [Required]
        [Column("certificate_file_name")]
        public string CertificateFileName { get; set; }

        //Loại chứng thư số
        [Column("cert_type")]
        public string CertType { get; set; }

        //CMT/MST
        [Column("subject_dn_cmt_mst")]
        public string SubjectDN_CMT_MST { get; set; }

        [Column("subject_dn_cn")]
        public string SubjectDN_CN { get; set; }

        [Column("subject_dn_o")]
        public string SubjectDN_O { get; set; }

        [Column("subject_dn_st")]
        public string SubjectDN_ST { get; set; }

        [Column("subject_dn_c")]
        public string SubjectDN_C { get; set; }

        [Column("csr")]
        public string Csr { get; set; }

        [Column("cert")]
        public string Cert { get; set; }

        //res_partner	uniqueidentifier Bổ sung: Liên kết với cty cung cấp (FK)
        [Column("res_partner")]
        public Guid? ResPartnerId { get; set; }

        //Bổ sung: đường dẫn trên Thư mục lưu p12	p12_path	nvarchar
        [Column("p12_path")]
        public string P12Path { get; set; }

        //bổ sung	certification_path	nvarchar
        [Column("certification_path")]
        public string CertificationPath { get; set; }

        //bổ sung	certification_hash	nvarchar
        [Column("certification_hash")]
        public string CertificationHash { get; set; }

        //Bổ sung	subject	nvarchar	
        [Column("subject")]
        public string Subject { get; set; }

        //Bổ sung: Trạng thái	certification_status	int
        [Column("certification_status")]
        public int? CertificationStatus { get; set; }

        //Bổ sung: 	push_notice_enabled	bit
        [Column("push_notice_enabled")]
        public bool? PushNoticeEnabled { get; set; }

        //Bổ sung: 	certification_profile_id	uniqueidentifier
        [Column("certification_profile_id")]
        public Guid? CertificationProfileId { get; set; }

        //Bổ sung: 	token_serial_number	nvarchar
        [Column("token_serial_number")]
        public string TokenSerialNumber { get; set; }

        //Bổ sung: 	effective_date	datetime2
        [Column("effective_date")]
        public DateTime? EffectiveDate { get; set; }

        //Bổ sung: 	expiration_date	
        [Column("expiration_date")]
        public DateTime? ExpirationDate { get; set; }

        //Bổ sung: 	expiration_contract_date	datetime2
        [Column("expiration_contract_date")]
        public DateTime? ExpirationContractDate { get; set; }

        //Bổ sung: 	duration	int
        [Column("duration")]
        public int? Duration { get; set; }

        //Bổ sung: 	private_key	nvarchar
        [Column("private_key")]
        public string PrivateKey { get; set; }

        //Bổ sung: 	private_key_enable	int
        [Column("private_key_enable")]
        public int? PrivateKeyEnable { get; set; }

        //Bổ sung: 	public_key	nvarchar
        [Column("public_key")]
        public string PublicKey { get; set; }

        //Bổ sung: 	public_key_hash	nvarchar
        [Column("public_key_hash")]
        public string PublicKeyHash { get; set; }

        //Bổ sung: 	activation_code	nvarchar
        [Column("activation_code")]
        public string ActivationCode { get; set; }

        //Bổ sung: 	revoke_date	datetime2
        [Column("revoke_date")]
        public DateTime? RevokeDate { get; set; }

        //Bổ sung: 	operation_date	datetime2
        [Column("operation_date")]
        public DateTime? OperationDate { get; set; }

        //bổ sung	hmac	text
        [Column("hmac", TypeName = "ntext")]
        public string Hmac { get; set; }


    }
}
