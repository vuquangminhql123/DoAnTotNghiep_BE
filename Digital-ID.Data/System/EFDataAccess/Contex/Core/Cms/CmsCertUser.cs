﻿namespace DigitalID.Data
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("tms_cert_user")]
    public  class CmsCertUser : BaseTable<CmsCertUser>
    {
        [Key]
        [Column("id")]
        public Guid Id { get; set; }

        [Required]
        [Column("user_id", Order = 1)]
        public Guid UserId { get; set; }

        [Required]
        [Column("cert_id", Order = 2)]
        public Guid CertId { get; set; }   
        
        [Column("pass_phrase", Order = 3)]
        public string Key { get; set; }

        //bổ sung	hmac	text
        [Column("hmac", TypeName = "ntext")]
        public string Hmac { get; set; }
    }
}
