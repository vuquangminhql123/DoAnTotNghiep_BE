﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace DigitalID.Data
{
    [Table("cms_organization")]
    public class CmsOrganization : BaseTable<CmsOrganization>
    {
        [Key]
        [Column("id")]
        public Guid Id { get; set; }

        //Đơn vị cha
        [ForeignKey("Parent")]
        [Column("parent_id")]
        public Guid? ParentId { get; set; }

        public CmsOrganization Parent { get; set; }

        //Mã đơn vị
        [Required]
        [StringLength(64)]
        [Column("code")]
        public string Code { get; set; }

        //Tên đơn vị
        [Required]
        [StringLength(128)]
        [Column("name")]
        public string Name { get; set; }

        //Tên viết tắt
        [StringLength(128)]
        [Column("short_name")]
        public string ShortName { get; set; }

        [Column("id_path")]
        public string IdPath { get; set; }

        //Loại đơn vị (1: Đơn vị, 2: Phòng ban)
        [Column("type")]
        public int Type { get; set; }

        //Mô tả
        [Column("description")]
        public string Description { get; set; }

        //Trạng thái
        [Column("status")]
        public bool Status { get; set; } = true;

        //Thứ tự sắp xếp
        [Column("order")]
        public int? Order { get; set; }

        //Số điện thoại
        [Column("phone_number")]
        public string PhoneNumber { get; set; }

        [Column("fax")]
        public string Fax { get; set; }

        //Email
        [Column("email")]
        public string Email { get; set; }

        [Column("parent_name")]
        public string ParentName { get; set; }

        //Địa chỉ
        [Column("address")]
        public string Address { get; set; }

        public string LogoUrl { get; set; }
    }
}
