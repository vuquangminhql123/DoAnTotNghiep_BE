namespace DigitalID.Data
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("cms_sign_profile")]
    public  class CmsSignProfile : BaseTable<CmsSignProfile>
    {
        [Key]
        [Column("id")]
        public Guid Id { get; set; }

        [Required]
        [StringLength(64)]
        [Column("code")]
        public string Code { get; set; }

        [Required]
        [StringLength(64)]
        [Column("name")]
        public string Name { get; set; }

        [StringLength(1024)]
        [Column("description")]
        public string Description { get; set; }

        [Column("standart_sign")]
        public string StandardSign { get; set; } // PaDES, xaDes, caDes, LTANs

        [Column("hashing_algorithm")]
        public string HashingAlgorithm { get; set; } //SHA1, SHA224, SHA256, SHA384, SHA512, RipeMD128, RipeMD160

        [Column("sign_zone_config")]
        public int SignZoneConfig { get; set; } = 0; // 0: AUTO, 1| MANUAL

        [Column("urx")]
        public int URX { get; set; } = 0;  // Upper Right X

        [Column("ury")]
        public int URY { get; set; } = 0; // Upper Right Y

        [Column("llx")]
        public int LLX { get; set; } = 0; // Lower Left X

        [Column("lly")]
        public int LLY { get; set; } = 0; // Lower Left Y

        [Column("sign_at_page")]
        public int SignAtPage { get; set; } = 0;

        [Column("is_sign_time")]
        public bool IsSignTime { get; set; }

        [Column("is_revoke")]
        public bool IsRevoke { get; set; }

        [Column("status")]
        public bool Status { get; set; }

        [Column("digital_signature_option")]
        public int DigitalSignatureOption { get; set; } = 0; // 0| AND  , 1| OR

        [Column("page_type")]
        public int PageType { get; set; } // 0| landscape    , 1| portrait

        [Column("template_file_path")]
        public string TemplateFilePath { get; set; }
    }
}
