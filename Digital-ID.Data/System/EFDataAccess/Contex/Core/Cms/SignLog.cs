namespace DigitalID.Data
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("sign_log")]
    public class SignLog
    {
        [Key]
        [Column("id")]
        public Guid Id { get; set; }

        [Required]
        [Column("application_id")]
        public Guid ApplicationId { get; set; }

        [Required]
        [Column("user_name")]
        public string UserName { get; set; }

        [Required]
        [Column("cert_id")]
        public Guid CertId { get; set; }

        [Required]
        [Column("code")]
        public string Code { get; set; }

        [Required]
        [Column("content")]
        public string Content { get; set; }

        [Column("file_id")]
        public string FileId { get; set; }

        [Required]
        [Column("create_on_date")]
        public DateTime CreateOnDate { get; set; }

        [Column("company_id", Order = 105)]
        public Guid? CompanyId { get; set; }
    }
}
