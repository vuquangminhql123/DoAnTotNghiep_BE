﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DigitalID.Data
{
    public class Cart : BaseTableCompanyAndMoreDefault
    {
        public Guid Id { get; set; }
        public string Code { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public Guid UserId { get; set; }
        public User User { get; set; }
        public string Name { get; set; }
        public string ListProducts { get; set; }
        public int Status { get; set; }
        public IEnumerable<Cart_Product> CartProducts { get; set; }
    }
}
