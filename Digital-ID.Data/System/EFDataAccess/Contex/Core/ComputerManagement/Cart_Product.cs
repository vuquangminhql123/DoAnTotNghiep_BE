﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DigitalID.Data
{
    public class Cart_Product : BaseTableCompanyAndMoreDefault
    {
        public Guid Id { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public Guid CartId { get; set; }
        public Cart Cart { get; set; }
        public Guid ProductId { get; set; }
        public Product Product { get; set; }
        public double Price { get; set; }
        public double Discount { get; set; }
        public bool? Active { get; set; }
        public string Content { get; set; }
        public string Code { get; set; }
    }
}
