﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DigitalID.Data
{
   public class CategoryProduct:BaseTableCompanyAndMoreDefault
    {
        public Guid Id { get; set; }
        public string Code { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public Guid ProductId { get; set; }
        public Product Product { get; set; }
        public bool? Status { get; set; }
        public Guid CategoryId { get; set; }
        public Category Category { get; set; }
    }
}
