﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DigitalID.Data
{
    public class Category_Meta : BaseTableCompanyAndMoreDefault
    {
        public Guid Id { get; set; }
        public DateTime CreatedDate { get; set; }
        public string Code { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string Key { get; set; }
        public bool? Status { get; set; }
        public List<Category_Meta_Product> CategoryMetaProducts { get; set; }
    }
}
