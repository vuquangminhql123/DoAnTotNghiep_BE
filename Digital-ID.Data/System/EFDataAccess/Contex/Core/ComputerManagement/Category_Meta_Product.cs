﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DigitalID.Data
{
    public class Category_Meta_Product
    {
        //indentity_number	number	bigint
        [Column("identity_number", Order = 96)]
        public long IdentityNumber { get; set; }
        public Guid Id { get; set; }
        public string Code { get; set; }
        public Guid CategoryMetaId { get; set; }
        public Guid ProductId { get; set; }
        public Category_Meta CategoryMeta { get; set; }
        public Product Product { get; set; }
        public bool? Status { get; set; }
        public string Content { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
    }
}
