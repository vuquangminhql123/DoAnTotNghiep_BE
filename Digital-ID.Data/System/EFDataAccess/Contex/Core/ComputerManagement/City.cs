﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DigitalID.Data
{
    public class City
    {
        public string matp { get; set; }
        public string name { get; set; }
        public string type { get; set; }
        public string slug { get; set; }
    }
}
