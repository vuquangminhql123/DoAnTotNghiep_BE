﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DigitalID.Data
{
    public class District
    {
        public string maqh { get; set; }
        public string name { get; set; }
        public string type { get; set; }
        public string matp { get; set; }
    }
}
