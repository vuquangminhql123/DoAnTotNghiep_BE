﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DigitalID.Data
{
   public class Order : BaseTableCompanyAndMoreDefault
    {
        public Guid Id { get; set; }
        public string Code { get; set; }
        public int Status { get; set; }
        public double SubTotal { get; set; }
        public double? ItemDiscount { get; set; }
        public double Shipping { get; set; }
        public double Total { get; set; }
        public string MaGiamGia { get; set; }
        //giam gia cua voucher
        public double? Discount { get; set; }

        public Guid? UserId { get; set; }

        public User User { get; set; }
        //Tong gia phai tra
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string City { get; set; }
        public string CityId { get; set; }
        public string Description { get; set; }
        public string District { get; set; }
        public string DistrictId { get; set; }
        public string Commune { get; set; }
        public string CommuneId { get; set; }
        public int PhuongThucThanhToan { get; set; }
        public string AddressDetail { get; set; }
        public double GrandTotal { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ConfirmedDate { get; set; }
        //Đang giao
        public DateTime DeliverDate { get; set; }
        public DateTime RecivedDate { get; set; }
        public DateTime CanceledDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string ListProducts { get; set; }
        public List<Order_Product> OrderProducts { get; set; }
    }
}
