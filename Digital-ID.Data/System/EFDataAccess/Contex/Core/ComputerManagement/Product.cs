﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DigitalID.Data
{
    public class Product : BaseTableCompanyAndMoreDefault
    {
        public Guid Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string Title { get; set; }
        public string SerialNumber { get; set; }
        public string MetaTitle { get; set; }
        public string LoaiBaoHanh { get; set; }
        public int? Like { get; set; }
        public int? Dislike { get; set; }
        public int? VisitCount { get; set; }
        public string Summary { get; set; }
        public int ThoiGianBaoHanh { get; set; }
        public bool? Status { get; set; }
        public double Price { get; set; }
        public double Discount { get; set; }
        public int SoLuong { get; set; }
        public Guid SupplierId { get; set; }
        public Supplier Supplier { get; set; }
        public string Description { get; set; }
        public List<CategoryProduct> CategoryProducts { get; set; }
        public List<Product_Review> ProductReviews { get; set; }
        public List<Product_Tag> ProductTags { get; set; }
        public List<Order_Product> OrderProducts { get; set; }
        public List<Cart_Product> CartProducts { get; set; }
        public List<Category_Meta_Product> CategoryMetaProducts { get; set; }
        public List<Picture> Pictures { get; set; }
    }
}
