﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DigitalID.Data
{
    public class Product_Review : BaseTableCompanyAndMoreDefault
    {
        public Guid Id { get; set; }
        public Guid ProductId { get; set; }
        public Product Product { get; set; }
        public Guid UserId { get; set; }
        public User User { get; set; }
        public string Code { get; set; }
        public string AvatarUrl { get; set; }
        public Guid? ParentId { get; set; }
        public Product_Review ProductReview { get; set; }
        public int Rating { get; set; }
        public bool? Status { get; set; }
        public bool IsRating { get; set; }
        public string Content { get; set; }
        public List<Product_Review> ProductReviews { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
    }
}
