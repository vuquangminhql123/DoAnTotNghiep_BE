﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DigitalID.Data
{
    public class Product_Tag: BaseTableCompanyAndMoreDefault
    {
        public Guid Id { get; set; }
        public Guid ProductId { get; set; }
        public string Code { get; set; }
        public Product Product { get; set; }
        public Guid TagId { get; set; }
        public Tag Tag { get; set; }
        public bool? Status { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
    }
}
