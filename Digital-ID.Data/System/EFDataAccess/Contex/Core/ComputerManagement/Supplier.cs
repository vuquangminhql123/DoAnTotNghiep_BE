﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DigitalID.Data
{
    public class Supplier : BaseTableCompanyAndMoreDefault
    {
        public Guid Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public bool? Status { get; set; }
        public string AvatarUrl { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public List<Product> Products { get; set; }
    }
}
