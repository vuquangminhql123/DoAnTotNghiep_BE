﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using System.Text.Json;

namespace DigitalID.Data
{
    public class User: BaseTableCompanyAndMoreDefault
    {
        public Guid Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string PasswordSalt { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public bool IsLock { get; set; }
        public bool IsAdmin { get; set; }
        public bool IsQtv { get; set; }
        public bool IsSocialSign { get; set; }
        public string Provider { get; set; }
        public string Avatar { get; set; }
        public DateTime DateOfBirth { get; set; }
        public bool Sex { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public Guid? CartId { get; set; }
        public Cart Cart { get; set; }
        public List<Order> Orders { get; set; }
        [Column("voucher_json")]
        public string VoucherJson
        {
            get
            {
                return Vouchers == null ? null : JsonSerializer.Serialize(Vouchers);
            }
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                    Vouchers = null;
                else
                    Vouchers = JsonSerializer.Deserialize<List<Voucher>>(value);
            }
        }

        [NotMapped] 
        public List<Voucher> Vouchers { get; set; }
        public List<Notification> Notifications { get; set; }
        public List<Product_Review> ProductReviews { get; set; }
    }
}
