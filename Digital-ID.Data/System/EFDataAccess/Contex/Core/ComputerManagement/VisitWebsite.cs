﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DigitalID.Data
{
    public class VisitWebsite
    {
        public Guid Id { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
