﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DigitalID.Data
{
    public class Voucher
    {
        public Guid Id { get; set; }
        public string Code { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ExpiredTime { get; set; }
        public DateTime StartTime { get; set; }
        public int Quantity { get; set; }
        public int Used { get; set; } = 0;
        public bool Status { get; set; }
        //Phần trăm giảm giá
        public double? PercentDiscount { get; set; }
        public int Type { get; set; }
        public double? Discount { get; set; }
    }
}
