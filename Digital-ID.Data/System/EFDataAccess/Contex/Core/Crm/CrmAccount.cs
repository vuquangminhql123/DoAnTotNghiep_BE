﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace DigitalID.Data
{
    [Table("crm_account")]
    public class CrmAccount : BaseTableCompanyAndMoreDefault
    {
        public CrmAccount()
        {
        }

        [Key]
        [Column("id")]
        public Guid Id { get; set; }

        [StringLength(256)]
        [Column("code")]
        public string Code { get; set; }

        [Required]
        [StringLength(256)]
        [Column("full_name")]
        public string FullName { get; set; }

        [StringLength(256)]
        [Column("first_name")]
        public string FirstName { get; set; }

        [StringLength(256)]
        [Column("last_name")]
        public string LastName { get; set; }

        [StringLength(20)]
        [Column("phone_number")]
        public string PhoneNumber { get; set; }

        [StringLength(256)]
        [Column("email")]
        public string Email { get; set; }

        [StringLength(256)]
        [Column("organization_name")]
        public string OrganizationName { get; set; }

        [StringLength(256)]
        [Column("position")]
        public string Position { get; set; }

        [Column("birthday", TypeName = "date")]
        public DateTime? Birthday { get; set; }

        [Column("avatar_url")]
        public string AvatarUrl { get; set; }

        [Column("address")]
        public string Address { get; set; }

        [Column("infomation_channel")]
        public string InfomationChannel { get; set; }
    }
}
