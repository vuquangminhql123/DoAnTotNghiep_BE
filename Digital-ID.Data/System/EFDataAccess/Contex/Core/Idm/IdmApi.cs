﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DigitalID.Data
{
    [Table("idm_api")]
    public  class IdmApi:BaseTableDefault
    {
        public IdmApi()
        {
            IdmApiMapRole = new HashSet<IdmApiMapRole>();
        }

        [Key]
        [Column("id")]
        public Guid Id { get; set; }

        [Required]
        [StringLength(512)]
        [Column("name")]
        public string Name { get; set; }

        [Required]
        [StringLength(512)]
        [Column("api_url")]
        public string ApiUrl { get; set; }

        [Required]
        [StringLength(20)]
        [Column("http_method")]
        public string HttpMethod { get; set; }

        [Column("allow_all_user")]
        public bool AllowAllUser { get; set; }

        [Column("status")]
        public bool Status { get; set; }

        [StringLength(1024)]
        [Column("description")]
        public string Description { get; set; }

        [StringLength(100)]
        [Column("group_api")]
        public string GroupApi { get; set; }

        public ICollection<IdmApiMapRole> IdmApiMapRole { get; set; }
    }
}
