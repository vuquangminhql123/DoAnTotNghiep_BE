﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DigitalID.Data
{
    [Table("idm_api_map_role")]
    public  class IdmApiMapRole : BaseTable<IdmApiMapRole>
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("id")]
        public long Id { get; set; }

        [Column("role_id", Order = 1),   ForeignKey("Role")]
        public Guid RoleId { get; set; }

        [Column("application_id", Order = 2),  ForeignKey("Application")]
        public override Guid ApplicationId { get; set; }

        [Column("api_id", Order = 3),   ForeignKey("Api")]
        public Guid ApiId { get; set; }

        public virtual IdmApi Api { get; set; }

        public virtual IdmRole Role { get; set; }
    }
}
