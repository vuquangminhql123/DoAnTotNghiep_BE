﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DigitalID.Data
{
    [Table("idm_client")]
    public class IdmClient
    {
        [Key]
        [Column("id")]
        public Guid Id { get; set; }


        //client_secret varchar(255)    NO
        [Required]
        [Column("client_secret")]
        public string ClientSecret { get; set; }


        //auto_approve    bit YES
        [Column("auto_approve")]
        public bool AutoApprove { get; set; }


        //user_id uniqueidentifier    NO
        [Column("user_id")]
        public Guid UserId { get; set; }


        //redirect_uri    varchar(255)    NO
        [Column("redirect_uri")]
        public string RedirectUri { get; set; }


        //grant_type  varchar(40) NO
        [Column("grant_type")]
        public string GrantType { get; set; }


        //is_active   bit NO
        [Column("is_active")]
        public bool IsActive { get; set; }

    }
}
