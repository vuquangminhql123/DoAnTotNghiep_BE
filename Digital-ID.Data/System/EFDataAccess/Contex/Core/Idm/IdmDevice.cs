﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DigitalID.Data
{
    [Table("idm_device")]
    public  class IdmDevice : BaseTable<IdmDevice>
    {
        [Key]
        [Column("id")]
        public Guid Id { get; set; }

        [Column("user_id")]
        public Guid UserId { get; set; }

        [Column("device_id")]
        public string DeviceId { get; set; }

        [Column("device_token")]
        public string DeviceToken { get; set; }

        [Column("device_type")]
        public string DeviceType { get; set; }

        [Column("status")]
        public bool Status { get; set; }

    }
}
