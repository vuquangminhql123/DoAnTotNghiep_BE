﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DigitalID.Data
{
    [Table("idm_otp")]
    public class IdmOTP : BaseTableCompanyDefault
    {
        [Key]
        [Column("id")]
        public Guid Id { get; set; }


        //PK: Người dùng nào đăng ký user_id uniqueidentifier--
        [Column("user_id")]
        public Guid? UserId { get; set; }


        //Tên đăng nhập   username nvarchar--
        [Column("username")]
        public string UserName { get; set; }


        //RelyingPartyId varchar
        [Column("relying_party_id")]
        public string RelyingPartyId { get; set; }


        //Thời gian hết hạn   period_time int
        [Column("period_time")]
        public int PeriodTime { get; set; }


        //H hết hạn   expiry_date datetime2
        [Column("expiry_date")]
        public DateTime ExpiryDate { get; set; }


        //otp	varchar
        [Column("otp")]
        public string OTP { get; set; }


        //otp_create_date datetime2
        [Column("otp_create_date")]
        public string OTPCreateDate { get; set; }


        //Mobile or Email	SentTo	nvarchar
        [Column("send_to")]
        public string SentTo { get; set; }

        //1: Mobile, 2: Email	type	varchar
        [Column("type")]
        public string Type { get; set; }


        //Mục đích    purpose varchar--
        [Column("purpose")]
        public string Purpose { get; set; }


        //transaction_id nvarchar--
        [Column("transaction_id")]
        public string TransactionId { get; set; }


        //transaction_type varchar--
        [Column("transaction_type")]
        public string TransactionType { get; set; }


        //security token của phiên tạo ra QR sec_token   varchar--
        [Column("sec_token")]
        public string SececurityToken { get; set; }


        //Link liên kết của API   api_link varchar--
        [Column("api_link")]
        public string ApiLink { get; set; }


        //Trạng thái: 0 hết hạn, 1 còn hạn    status int--
        [Column("status")]
        public int status { get; set; }


        //Hashed Message Authentication Code: SHA256 Hmac    ntext--
        [Column("hmac", TypeName = "ntext")]
        public string Hmac { get; set; }




    }
}
