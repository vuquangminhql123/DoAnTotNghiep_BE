﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DigitalID.Data
{
    [Table("idm_qrcode")]
    public class IdmQRCode: BaseTableCompanyDefault
    {
        [Key]
        [Column("id")]
        public Guid Id { get; set; }


        //PK: Người dùng nào đăng ký user_id uniqueidentifier
        [Column("user_id")]
        public Guid? UserId { get; set; }


        //Tên đăng nhập   username nvarchar
        [Column("username")]
        public string UserName { get; set; }


        //Mã qr là gì qrcode nvarchar
        [Column("qrcode")]
        public string QRCode { get; set; }


        //qrcode_create_date datetime2
        [Column("qrcode_create_date")]
        public DateTime QRCodeCreateDate { get; set; }


        //Thời gian hết hạn   period_time int
        [Column("period_time")]
        public int PeriodTime { get; set; }


        //H hết hạn   expiry_date datetime2
        [Column("expiry_date")]
        public DateTime ExpiryDate { get; set; }


        //Mục đích    purpose varchar
        [Column("purpose")]
        public string Purpose { get; set; }


        //transaction_id nvarchar
        [Column("transaction_id")]
        public string TransactionId { get; set; }


        //transaction_type varchar
        [Column("transaction_type")]
        public string TransactionType { get; set; }


        //security token của phiên tạo ra QR sec_token   varchar
        [Column("sec_token")]
        public string SececurityToken { get; set; }


        //Link liên kết của API   api_link varchar
        [Column("api_link")]
        public string ApiLink { get; set; }


        //Trạng thái: 0 hết hạn, 1 còn hạn    status int
        [Column("status")]
        public int status { get; set; }





        //Hashed Message Authentication Code: SHA256 Hmac    ntext
        [Column("hmac", TypeName = "ntext")]
        public string Hmac { get; set; }




    }
}
