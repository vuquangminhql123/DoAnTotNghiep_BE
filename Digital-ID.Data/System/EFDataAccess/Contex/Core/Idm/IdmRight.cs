﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DigitalID.Data
{
    [Table("idm_right")]
    public  class IdmRight  :BaseTableDefault
    {
        public IdmRight()
        {
            RightsInRole = new HashSet<IdmRightMapRole>();
            RightsOfUser = new HashSet<IdmRightMapUser>();
        }

        [Key]
        [Column("id")]
        public Guid Id { get; set; }

        [Required]
        [StringLength(64)]
        [Column("code")]
        public string Code { get; set; }

        [Required]
        [StringLength(128)]
        [Column("name")]
        public string Name { get; set; }

        [StringLength(1024)]
        [Column("description")]
        public string Description { get; set; }

        [StringLength(64)]
        [Column("group_code")]
        public string GroupCode { get; set; }

        [Column("status")]
        public bool Status { get; set; }

        [Column("order")]
        public int Order { get; set; }

        [Column("is_system")]
        public bool IsSystem { get; set; }

        public ICollection<IdmRightMapRole> RightsInRole { get; set; }
        public ICollection<IdmRightMapUser> RightsOfUser { get; set; }
    }
}
