﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DigitalID.Data
{
    [Table("idm_right_map_role")]
    public  class IdmRightMapRole : BaseTable<IdmRightMapRole>
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("id")]
        public long Id { get; set; }

        [Column("role_id", Order = 1),   ForeignKey("Role")]
        public Guid RoleId { get; set; } 

        [Column("application_id", Order = 2),  ForeignKey("Application")]
        public override Guid ApplicationId { get; set; }

        [Column("right_id", Order = 3),   ForeignKey("Right")]
        public Guid RightId { get; set; }

        public virtual IdmRight Right { get; set; }

        public virtual IdmRole Role { get; set; }
    }
}
