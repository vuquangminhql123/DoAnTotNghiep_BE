﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DigitalID.Data
{
    [Table("idm_right_map_user")]
    public  class IdmRightMapUser: BaseTable<IdmRightMapUser>
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("id")]
        public long Id { get; set; }

        [Column("user_id", Order = 1),   ForeignKey("User")]
        public Guid UserId { get; set; }

        [Column("application_id", Order = 2),   ForeignKey("Application")]
        public override Guid ApplicationId { get; set; }

        [Column("right_id", Order = 3),  ForeignKey("Right")]
        public Guid RightId { get; set; }

        [StringLength(1024)]
        [Column("inherited_from_roles")]
        public string InheritedFromRoles { get; set; }

        [Column("inherited")]
        public bool Inherited { get; set; }

        [Column("enable")]
        public bool Enable { get; set; }

        public virtual IdmRight Right { get; set; }

        public virtual IdmUser User { get; set; }
    }
}
