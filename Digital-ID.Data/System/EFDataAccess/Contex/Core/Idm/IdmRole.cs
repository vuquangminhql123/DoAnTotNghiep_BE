﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DigitalID.Data
{
    [Table("idm_role")]
    public  class IdmRole:BaseTableDefault
    {
        public IdmRole()
        {
            IdmUserMapRole = new HashSet<IdmUserMapRole>();
            IdmRightMapRole = new HashSet<IdmRightMapRole>();
        }

        [Key]
        [Column("id")]
        public Guid Id { get; set; }

        [Required]
        [StringLength(128)]
        [Column("name")]
        public string Name { get; set; }

        [Required]
        [StringLength(64)]
        [Column("code")]
        public string Code { get; set; }

        //status	NO	bit
        [Column("status")]
        public bool Status { get; set; }

        /// <summary>
        /// Ký số
        /// </summary>
        [Column("is_digital_signed")]
        public bool IsDigitalSigned { get; set; }

        //order	NO	int
        [Column("order")]
        public int Order { get; set; }

        //description	YES	nvarchar
        [Column("description")]
        public string Description { get; set; }

        public ICollection<IdmUserMapRole> IdmUserMapRole { get; set; }

        public ICollection<IdmRightMapRole> IdmRightMapRole { get; set; }
    }
}
