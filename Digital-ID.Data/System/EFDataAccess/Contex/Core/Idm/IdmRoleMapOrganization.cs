﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DigitalID.Data
{
    [Table("idm_role_map_organization")]
    public  class IdmRoleMapOrganization : BaseTableCompanyAndMoreDefault
    {
        public IdmRoleMapOrganization()
        {
        }

        [Key]
        [Column("id")]
        public Guid Id { get; set; }

        [ForeignKey("Role")]
        [Column("role_id")]
        public Guid RoleId { get; set; }

        public IdmRole Role { get; set; }

        [ForeignKey("Organization")]
        [Column("organization_id")]
        public Guid OrganizationId { get; set; }

        public CmsOrganization Organization { get; set; }
    }
}
