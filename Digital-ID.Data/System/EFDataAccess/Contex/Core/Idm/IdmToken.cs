﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DigitalID.Data
{
    [Table("idm_token")]
    public class IdmToken : BaseTable<IdmToken>
    {
        [Key]
        [Column("id")]
        public Guid Id { get; set; }


        //client_id uniqueidentifier    NO
        [Column("client_id")]
        public Guid? ClientId { get; set; }

        //access_token            varchar(max)    NO
        [Required]
        [Column("access_token")]
        public string AccessToken { get; set; }

        //refresh_token   varchar(max)    YES
        [Column("refresh_token")]
        public string RefreshToken { get; set; }

        //token_type  varchar(50) YES
        [Column("token_type")]
        public string TokenType { get; set; }

        //expiry_time datetime2 NO
        [Column("expiry_time")]
        public DateTime ExpiryTime { get; set; }

        //user_id uniqueidentifier    NO
        [Column("user_id")]
        public Guid UserId { get; set; }

        //username    varchar(50) YES
        [Column("username")]
        public string UserName { get; set; }

        //is_active   bit NO
        [Column("is_active")]
        public bool IsActive { get; set; }

        //algorithm varchar(50) YES
        [Column("algorithm")]
        public string Algorithm { get; set; }

        //issuer  nvarchar(500)   YES
        [Column("issuer")]
        public string Issuer { get; set; }

        //issued_at   datetime2 NO
        [Column("issued_at")]
        public DateTime IssuedAt { get; set; }

        //hmac text    YES
        [Column("hmac", TypeName = "ntext")]
        public string Hmac { get; set; }
    }
}
