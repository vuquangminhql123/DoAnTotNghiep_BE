﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DigitalID.Data
{
    [Table("idm_user")]
    public class IdmUser : BaseTableDefault
    {
        public IdmUser()
        {
            IdmUserMapRole = new HashSet<IdmUserMapRole>();
            IdmRightMapUser = new HashSet<IdmRightMapUser>();
        }

        [Key]
        [Column("id")]
        public Guid Id { get; set; }

        [Required]
        [StringLength(128)]
        [Column("user_name")]
        public string UserName { get; set; }

        /*Attribute */
        [Required]
        [StringLength(128)]
        [Column("name")]
        public string Name { get; set; }

        [StringLength(256)]
        [Column("phone_number")]
        public string PhoneNumber { get; set; }

        [StringLength(256)]
        [Column("email")]
        public string Email { get; set; }

        [StringLength(1024)]
        [Column("avatar_url")]
        public string AvatarUrl { get; set; }

        [Column("type")]
        public int? Type { get; set; }

        [StringLength(1024)]
        [Column("password")]
        public string Password { get; set; }

        [StringLength(1024)]
        [Column("password_salt")]
        public string PasswordSalt { get; set; }

        [Column("birth_date")]
        public DateTime? Birthdate { get; set; }

        [Column("last_activity_date")]
        public DateTime LastActivityDate { get; set; }

        [Column("is_locked")]
        public bool IsLocked { get; set; }
        [Column("is_admin")]
        public bool IsAdmin { get; set; }

        public ICollection<IdmUserMapRole> IdmUserMapRole { get; set; }
        public ICollection<IdmRightMapUser> IdmRightMapUser { get; set; }

        [Column("position_id")]
        public Guid? PositionId { get; set; }

        [Column("position_name")]
        public string PositionName { get; set; }

        [Column("company_name")]
        public string CompanyName { get; set; }

        [Column("security_question")]
        public string SecurityQuestion { get; set; }

        [Column("security_question_answer")]
        public string SecurityQuestionAnswer { get; set; } 

        [Column("sign_type_full")]
        public int SignTypeFull { get; set; } = 0;

        [Column("sign_full_path")]
        public string SignFullPath { get; set; }

        [Column("sign_final_path")]
        public string SignFinalPath { get; set; }

        [Column("sign_type_short")]
        public int SignTypeShort { get; set; } = 0;

        [Column("sign_short_path")]
        public string SignShortPath { get; set; } 

        [Column("logo_path")]
        public string LogoPath { get; set; } 

        [Column("sign_display")]
        public int SignDisplay { get; set; } = 0;

        [Column("sign_by")]
        public int SignBy { get; set; } = 0;// 0: HSM, 1: USB token

        [StringLength(256)]
        [Column("ca")]
        public string Ca { get; set; }

        //Trạng thái sử dụng cert: 0 chưa tạo khóa, 1: đã tạo khóa chưa tạo cert, 2: đã tạo cert
        [Column("cert_status")]
        public int CertStatus { get; set; }

        //res_partner	uniqueidentifier
        [Column("res_partner")]
        public Guid? ResPartnerId { get; set; }
        //- is_otp_mobile
        [Column("is_otp_mobile")]
        public bool IsOTPMobile { get; set; }


        //- is_otp_email
        [Column("is_otp_email")]
        public bool IsOTPEmail { get; set; }


        //- is_otp_face (bit)
        [Column("is_otp_face")]
        public bool IsOTPFace { get; set; }

        //is_otp_sms bit
        [Column("is_otp_sms")]
        public bool IsOTPSms { get; set; }

        //is_otp_soft_token bit
        [Column("is_otp_soft_token")]
        public bool IsOTPSoftToken { get; set; }

        //is_face bit
        [Column("is_face")]
        public bool IsFace { get; set; }

        //is_fingerprint bit
        [Column("is_fingerprint")]
        public bool IsFingerprint { get; set; }

        //is_ksm bit
        [Column("is_kms")]
        public bool IsKMS { get; set; }

        //is_smart_card bit
        [Column("is_smart_card")]
        public bool IsSmartCard { get; set; }

        //is_fido_uaf bit
        [Column("is_fido_uaf")]
        public bool IsFidoUaf { get; set; }
    }
}
