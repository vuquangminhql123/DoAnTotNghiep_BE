﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DigitalID.Data
{
    [Table("idm_user_map_org")]
    public  class IdmUserMapOrg :BaseTable<IdmUserMapOrg>
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("id")]
        public long Id { get; set; }

        [Column("user_id", Order = 1) , ForeignKey("User")]
        public Guid UserId { get; set; }

        [Column("application_id", Order = 2) , ForeignKey("Application")]
        public override Guid ApplicationId { get; set; }

        [Column("organization_id", Order = 3),   ForeignKey("Organization")]
        public Guid OrganizationId { get; set; }

        public virtual CmsOrganization Organization { get; set; }
        public virtual IdmUser User { get; set; }
    }
}
