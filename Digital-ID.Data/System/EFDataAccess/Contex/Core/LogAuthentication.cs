﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DigitalID.Data
{
    [Table("log_authentication")]
    public class LogAuthentication : BaseTableCompanyDefault
    {
        [Key]
        [Column("id")]
        public Guid Id { get; set; }


        //FK: Xuất phát từ ứng dụng nào    application_id uniqueidentifier        NO


        //id người ký user_id uniqueidentifier	50	NO
        [Column("user_id")]
        public Guid UserId { get; set; }


        //username ng ký  username varchar	50	NO
        [Column("username")]
        public string UserName { get; set; }


        //Liên kết với bảng Certification certificattion_id uniqueidentifier        YES
        [Column("certificattion_id")]
        public Guid CertificattionId { get; set; }


        //request_id  varchar	500	YES
        [Column("request_id")]
        public string RequestId { get; set; }


        //relying_party_id    varchar	255	YES
        [Column("relying_party_id")]
        public string RelyingPartyId { get; set; }


        //relying_party_ip    varchar	255	YES
        [Column("relying_party_ip")]
        public string RelyingPartyIP { get; set; }


        //Request text MAX YES
        [Column("request", TypeName = "ntext")]
        public string Request { get; set; }


        //location    varchar	500	YES
        [Column("location")]
        public string Location { get; set; }


        //thời gian yêu cầu RequestTime datetime2 NO
        [Column("request_time")]
        public DateTime RequestTime { get; set; }


        //Thời gian đáp ứng   ResponseTime datetime2       NO
        [Column("response_time")]
        public DateTime ResponseTime { get; set; }


        //Trạng thái đáp ứng ResponseStatus  varchar	50	NO
        [Column("response_status")]
        public string ResponseStatus { get; set; }


        //Reponse_token_device
        [Column("reponse_token_device")]
        public string ReponseTokenDevice { get; set; }


        //Response text    MAX YES
        [Column("response")]
        public string Response { get; set; }


        //transaction_id nvarchar	255	YES
        [Column("transaction_id")]
        public string TransactionId { get; set; }


        //transaction_type    varchar	50	YES
        [Column("transaction_type")]
        public string TransactionType { get; set; }


        //is_otp_email    bit YES
        [Column("is_otp_email")]
        public bool IsOTPEmail { get; set; }


        //is_otp_sms bit     YES
        [Column("is_otp_sms")]
        public bool IsOTPSms { get; set; }


        //is_otp_soft_token   bit YES
        [Column("is_otp_soft_token")]
        public bool IsOTPSoftToken { get; set; }


        //is_face bit     YES
        [Column("is_face")]
        public bool IsFace { get; set; }


        //is_fingerprint  bit YES
        [Column("is_fingerprint")]
        public bool IsFingerprint { get; set; }


        //is_ksm bit     YES
        [Column("is_kms")]
        public bool IsKMS { get; set; }


        // is_smart_card   bit YES
        [Column("is_smart_card")]
        public bool IsSmartCard { get; set; }


        //"Bổ sung: 0: chưa đăng ký FIDO 1: đã đk FIDO"	is_fido_uaf	bit		YES
        [Column("is_fido_uaf")]
        public bool IsFidoUaf { get; set; }


        //RelyingPartySslCert text    MAX YES
        [Column("relying_party_ssl_cert", TypeName = "ntext")]
        public string RelyingPartySslCert { get; set; }


        //ErrorCode nvarchar	500	YES
        [Column("error_code")]
        public string ErrorCode { get; set; }


        // Message nvarchar MAX YES
        [Column("message")]
        public string Message { get; set; }


        //MobileNumber    varchar	50	YES
        [Column("mobile_number")]
        public string MobileNumber { get; set; }


        //Hmac text	500	YES
        [Column("hmac", TypeName = "ntext")]
        public string Hmac { get; set; }

    }
}
