﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DigitalID.Data
{
    [Table("log_signing")]
    public class LogSigning : BaseTableCompanyDefault
    {
        [Key]
        [Column("id")]
        public Guid Id { get; set; }

        
        //"FK: Xuất phát từ ứng dụng nào: SXĐH ; Cần Thơ, HDSAISON, Vpbank"	application_id	uniqueidentifier		NO
        
        
        //id người ký, maping với HT eID  user_id uniqueidentifier	50	NO
        [Column("user_id")]
        public Guid UserId { get; set; }
        
        
        //username ng ký  username varchar	50	NO
        [Column("username")]
        public string UserName { get; set; }
        
        
        //Liên kết với bảng Certification certificattion_id uniqueidentifier        YES
        [Column("certificattion_id")]
        public Guid? CertificattionId { get; set; }
        
        
        //profile_id  uniqueidentifier YES
        [Column("profile_id")]
        public Guid? ProfileId { get; set; }
        
        
        //request_id varchar	500	YES
        [Column("request_id")]
        public string RequestId { get; set; }
        
        
        //RequestType varchar NO
        [Column("request_type")]
        public string RequestType { get; set; }
        
        
        //RequestTime datetime        NO
        [Column("request_time")]
        public DateTime RequestTime { get; set; }
        
        
        //RequestXml  text YES
        [Column("request_xml")]
        public string RequestXml { get; set; }
        
        
        //RequestMode varchar     YES
        [Column("request_mode")]
        public string RequestMode { get; set; }
        
        
        //relying_party_id    varchar	255	YES
        [Column("relying_party_id")]
        public string RelyingPartyId { get; set; }
        
        
        //relying_party_ip    varchar	255	YES
        [Column("relying_party_ip")]
        public string RelyingPartyIp { get; set; }
        
        
        //location    varchar	500	YES
        [Column("location")]
        public string Location { get; set; }
        
        
        //ResponseStatus  varchar NO
        [Column("response_status")]
        public string ResponseStatus { get; set; }
        
        
        //ResponseTime datetime        NO
        [Column("response_time")]
        public DateTime ResponseTime { get; set; }
        
        
        //ResponseXml text YES
        [Column("response_xml")]
        public string ResponseXml { get; set; }
        
        
        //BackendRequest text	1000	YES
        [Column("backend_request")]
        public string BackendRequest { get; set; }
        
        
        //BackendResponse text	1000	YES
        [Column("backend_response")]
        public string BackendResponse { get; set; }
        
        
        //IsDocumentExist varchar	500	YES
        [Column("is_document_exist")]
        public string IsDocumentExist { get; set; }
        
        
        //Message nvarchar MAX YES
        [Column("message")]
        public string Message { get; set; }
        
        
        //transaction_id  nvarchar	255	YES
        [Column("transaction_id")]
        public string TransactionId { get; set; }
        
        
        //transaction_type    varchar	50	YES
        [Column("transaction_type")]
        public string TransactionType { get; set; }
        
        
        //RelyingPartySigningCert text	500	YES
        [Column("relying_party_signing_cert")]
        public string RelyingPartySigningCert { get; set; }
        
        
        //RelyingPartySslCert text	500	YES
        [Column("relying_party_ssl_cert")]
        public string RelyingPartySslCert { get; set; }
        
        
        //is_otp_email    bit YES
        [Column("is_otp_email")]
        public bool IsOTPEmail { get; set; }
        
        
        //is_otp_sms bit     YES
        [Column("is_otp_sms")]
        public bool IsOTPSms { get; set; }
        
        
        //is_otp_soft_token   bit YES
        [Column("is_otp_soft_token")]
        public bool IsOTPSoftToken { get; set; }
        
        
        //is_face bit     YES
        [Column("is_face")]
        public bool IsFace { get; set; }
        
        
        //is_fingerprint  bit YES
        [Column("is_fingerprint")]
        public bool IsFingerprint { get; set; }
        
        
        //is_ksm bit     YES
        [Column("is_kms")]
        public bool IsKMS { get; set; }
        
        
        // is_smart_card   bit YES
        [Column("is_smart_card")]
        public bool IsSmartCard { get; set; }
        
        
        //"Bổ sung: 0: chưa đăng ký FIDO 1: đã đk FIDO"	is_fido_uaf	bit		YES
        [Column("is_fido_uaf")]
        public bool IsFidoUaf { get; set; }
        
        
        //ErrorCode nvarchar	500	YES
        [Column("error_code")]
        public string ErrorCode { get; set; }
        
        
        //Tài liệu ký, lưu đường dẫn  InputDocument_path
        [Column("input_document_path")]
        public string InputDocumentPath { get; set; }
        
        
        //Nội dung file ký: base64 encoding  InputDocument ntext	4000	YES
        [Column("input_document", TypeName = "ntext")]
        public string InputDocument { get; set; }
        
        
        //InputDocument_hash
        [Column("input_document_hash", TypeName = "ntext")]
        public string InputDocumentHash { get; set; }
        
        
        //Tài liệu đã được ký, lưu đường dẫn OutputDocument_path
        [Column("output_document_path")]
        public string OutputDocumentPath { get; set; }


        //Nội dung filen đã ký: base64 encoding   OutputDocument ntext	4000	YES
        [Column("output_document", TypeName = "ntext")]
        public string OutputDocument { get; set; }


        //OutputDocument_hash
        [Column("output_document_hash", TypeName = "ntext")]
        public string OutputDocumentHash { get; set; }


        //Hmac text	500	YES
        [Column("hmac", TypeName = "ntext")]
        public string Hmac { get; set; }

    }
}
