﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DigitalID.Data
{

    [Table("sys_appliation")]
    public class SysApplication : BaseTableDefault
    {
        public SysApplication()
        { }
        [Key]
        [Column("id")]
        public Guid Id { get; set; }

        [Required]
        [StringLength(128)]
        [Column("name")]
        public string Name { get; set; }

        [StringLength(1024)]
        [Column("description")]
        public string Description { get; set; }

        [Required]
        [StringLength(64)]
        [Column("code")]
        public string Code { get; set; }

        [Column("demo")]
        public string Demo { get; set; }
    }
}
