﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DigitalID.Data.Models
{
    [Table("WorkflowApplication")]
    public partial class WorkflowApplication : BaseTable<WorkflowApplication>
    {
        [Key]
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string ProcessStatusChangedConfig { get; set; }
        public bool Status { get; set; }

    }
}
