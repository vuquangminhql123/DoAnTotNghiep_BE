﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DigitalID.Data.Models
{
    [Table("WorkflowDocument")]
    public partial class WorkflowDocument : BaseTable<WorkflowDocument>
    {
        [Key]
        public Guid Id { get; set; }
        public Guid? WorkflowApplicationId { get; set; }
        public string Name { get; set; }
        public string Scheme { get; set; }
        public string Status { get; set; }
        public string BussinessFlow { get; set; }


        public string PrevState { get; set; }
        public string PrevActivity { get; set; }
        // --------------------------
        public string State { get; set; }
        public string Activity { get; set; }
        // --------------------------
        public string NextActivity { get; set; }
        public string NextState { get; set; }
        //-----------------------------
        public string AllowIdentityIds { get; set; } // allow who for next one

        public string ListSubState { get; set; }
        public int TransitionClassifier { get; set; } //direct revert unspec

        public DateTime ExecutedTransition { get; set; }
        public Guid? ExecutedIdentityId { get; set; } // who just execute
        public string ExecutedAction { get; set; } //command | auto | timer
        public string ExecutedActionValue { get; set; } // name of command or timer
        public string ExecutedFormInputJson { get; set; }
        public string ExecutedFiles { get; set; }

        public List<WorkflowDocumentHistory> WorkflowDocumentHistorys { get; set; }
        public List<WorkflowDocumentAttachment> WorkflowDocumentAttachments { get; set; }

        /// For static
        public int IsDraft { get; set; } = 0;
        public int IsInProcess { get; set; } = 0;
        public int IsCompleted { get; set; } = 0;
        public int IsRejected { get; set; } = 0;
        public int ShowSign { get; set; } = 0;

        public string DAY { get; set; } // DD/MM/YYYY
        public string MONTH { get; set; } // MM/YYYY
        public string YEAR { get; set; } // YYYY
        public string WEEK { get; set; } // NUMBER 

    }
}
