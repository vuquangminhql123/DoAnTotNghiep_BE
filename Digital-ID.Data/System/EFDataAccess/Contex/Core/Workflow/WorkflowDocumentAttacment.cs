﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DigitalID.Data.Models
{
    [Table("WorkflowDocumentAttachment")]
    public partial class WorkflowDocumentAttachment : BaseTable<WorkflowDocumentAttachment>
    {

        [Key]
        public Guid Id { get; set; }
        public Guid WorkflowDocumentId { get; set; }
        public string PhysicalName { get; set; }
        public string Name { get; set; }
        public long Size { get; set; }
        public string Extension { get; set; }
        public string PhysicalPath { get; set; }
        public string RelativePath { get; set; } 
        public string Note { get; set; }

        [ForeignKey("WorkflowDocumentId")]
        public WorkflowDocument WorkflowDocument { get; set; }
    }
} 