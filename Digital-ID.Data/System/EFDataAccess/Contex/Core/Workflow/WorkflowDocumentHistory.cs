﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DigitalID.Data.Models
{
    [Table("WorkflowDocumentHistory")]
    public partial class WorkflowDocumentHistory
    {

        [Key]
        public Guid Id { get; set; }
        public Guid WorkflowDocumentId { get; set; }
        public string DocumentName { get; set; }
        public string Scheme { get; set; }
        public string Status { get; set; }
        public string PrevState { get; set; }
        public string PrevActivity { get; set; }
        // --------------------------
        public string State { get; set; }
        public string Activity { get; set; }
        // --------------------------
        public string ListSubState { get; set; }
        public DateTime ExecutedTransition { get; set; }
        public Guid? ExecutedIdentityId { get; set; } // who just execute
        public string ExecutedAction { get; set; } //command | auto | timer
        public string ExecutedActionValue { get; set; } // name of command or timer
        public int TransitionClassifier { get; set; } //direct revert unspec
        public string ExecutedFormInputJson { get; set; }
        public string ExecutedFiles { get; set; }
        public bool IsSubProcess { get; set; }
        [ForeignKey("WorkflowDocumentId")]
        public WorkflowDocument WorkflowDocument { get; set; }
    }
}
