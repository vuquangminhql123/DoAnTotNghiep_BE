﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DigitalID.Data.Models
{
    [Table("WorkflowInbox")]
    public partial class WorkflowInbox
    {
        [Key]
        public Guid Id { get; set; }
        public Guid DocumentId { get; set; }
        public string IdentityId { get; set; }
        public Guid DocumentHistoryId { get; set; } 
        public int Status { get; set; } = 0; // 0|| not seen, 1 || viewed
    }
}
