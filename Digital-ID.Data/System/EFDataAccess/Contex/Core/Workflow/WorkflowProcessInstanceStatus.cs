﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DigitalID.Data.Models
{
    [Table("WorkflowProcessInstanceStatus")]
    public partial class WorkflowProcessInstanceStatus
    {
        [Key]
        public Guid Id { get; set; }
        public byte Status { get; set; }
        public Guid Lock { get; set; }
    }
}
