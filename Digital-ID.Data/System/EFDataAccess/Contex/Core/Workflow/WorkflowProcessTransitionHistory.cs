﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DigitalID.Data.Models
{
    [Table("WorkflowProcessTransitionHistory")]
    public partial class WorkflowProcessTransitionHistory
    {
        [Key]
        public Guid Id { get; set; }
        public Guid ProcessId { get; set; }
        public string ExecutorIdentityId { get; set; }
        public string ActorIdentityId { get; set; }
        public string FromActivityName { get; set; }
        public string ToActivityName { get; set; }
        public string ToStateName { get; set; }
        public DateTime TransitionTime { get; set; }
        public string TransitionClassifier { get; set; }
        public bool IsFinalised { get; set; }
        public string FromStateName { get; set; }
        public string TriggerName { get; set; }
    }
}
