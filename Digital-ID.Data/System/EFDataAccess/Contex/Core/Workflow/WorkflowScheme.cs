﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DigitalID.Data.Models
{
    [Table("WorkflowScheme")]
    public partial class WorkflowScheme
    {
        [Key]
        public string Code { get; set; }
        public string Scheme { get; set; }
    }
}
