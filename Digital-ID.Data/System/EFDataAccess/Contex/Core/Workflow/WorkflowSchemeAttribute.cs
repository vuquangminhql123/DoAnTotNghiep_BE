using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace DigitalID.Data
{
    [Table("WorkflowSchemeAttribute")]
    public class WorkflowSchemeAttribute : BaseTable<WorkflowSchemeAttribute>
    {
        [Key]
        public Guid Id { get; set; }
        [Required]
        [StringLength(64)]
        public string Code { get; set; } // == code scheme
        public string Description { get; set; }
        public bool Status { get; set; }
        public int Order { get; set; }
        public int ActiveVersion { get; set; } = 1;

    }
}
