using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace DigitalID.Data
{
    [Table("WorkflowSchemeVersion")]
    public class WorkflowSchemeVersion : BaseTable<WorkflowSchemeVersion>
    {
        [Key]
        public Guid Id { get; set; }
        [Required]
        [StringLength(64)]
        public string Code { get; set; } //Mã quy trình cha
        public int Version { get; set; } // Phiên bản 
        public string Scheme { get; set; }
        public bool isActive { get; set; }

    }
}
