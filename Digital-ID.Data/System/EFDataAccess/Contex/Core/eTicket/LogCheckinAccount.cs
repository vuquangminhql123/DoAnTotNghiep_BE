﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace DigitalID.Data
{
    [Table("log_checkin_account")]
    public class LogCheckinAccount : BaseTableCompanyAndMoreDefault
    {
        public LogCheckinAccount()
        {
        }

        [Key]
        [Column("id")]
        public Guid Id { get; set; }

        [Column("account_id", Order = 1),   ForeignKey("Account")]
        public Guid AccountId { get; set; }

        public virtual CrmAccount Account { get; set; }
    }
}
