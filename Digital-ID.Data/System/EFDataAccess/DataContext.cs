﻿using System;
using System.Linq;
using DigitalID.Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.EntityFrameworkCore.Infrastructure;

namespace DigitalID.Data
{
    public class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<DataContext>
    {
        public DataContext CreateDbContext(string[] args)
        {
            var isTesting = (Utils.GetConfig("ConnectionString:IsTesting") == "HAS_TEST");
            var databaseType = Utils.GetConfig("ConnectionString:DbType");
            var connectionString = Utils.GetConfig("ConnectionString:" + databaseType);
            return new DataContext("", databaseType, connectionString, isTesting);
        }
    }

    public class DataContext : DbContext
    {
        public DataContext()
        {
            var isTesting = (Utils.GetConfig("ConnectionString:IsTesting") == "HAS_TEST");
            var databaseType = Utils.GetConfig("ConnectionString:DbType");
            var connectionString = Utils.GetConfig("ConnectionString:" + databaseType); 
            DatabaseType = databaseType;
            ConnectionString = connectionString;
            IsTesting = isTesting;
        }

        #region ComputerManagement

        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<Voucher> Vouchers { get; set; }
        public virtual DbSet<VisitWebsite> VisitWebsites { get; set; }
        public virtual DbSet<Notification> Notifications { get; set; }
        public virtual DbSet<City> Cities { get; set; }
        public virtual DbSet<Commune> Communes { get; set; }
        public virtual DbSet<District> Districts { get; set; }
        public virtual DbSet<Product> Products { get; set; }
        public virtual DbSet<Category> Category { get; set; }
        public virtual DbSet<Tag> Tags { get; set; }
        public virtual DbSet<Supplier> Suppliers { get; set; }
        public virtual DbSet<Product_Tag> ProductTags { get; set; }
        public virtual DbSet<Product_Review> ProductReviews { get; set; }
        public virtual DbSet<Picture> Pictures { get; set; }
        public virtual DbSet<Order_Product> OrderProducts { get; set; }
        public virtual DbSet<Order> Orders { get; set; }
        public virtual DbSet<CategoryProduct> CategoryProducts { get; set; }
        public virtual DbSet<Category_Meta_Product> CategoryMetaProducts { get; set; }
        public virtual DbSet<Category_Meta> CategoryMetas { get; set; }
        public virtual DbSet<Cart_Product> CartProducts { get; set; }
        public virtual DbSet<Cart> Carts { get; set; }

        #endregion
        #region sys

        public virtual DbSet<SysApplication> SysApplication { get; set; }

        #endregion sys

        #region bsd

        public virtual DbSet<BsdParameter> BsdParameter { get; set; }
        public virtual DbSet<BsdNavigation> BsdNavigation { get; set; }
        public virtual DbSet<BsdNavigationMapRole> BsdNavigationMapRole { get; set; }
        public virtual DbSet<BsdLog> BsdLog { get; set; }

        public virtual DbSet<TaxonomyTerm> TaxonomyTerm { get; set; }
        public virtual DbSet<TaxonomyVocabulary> TaxonomyVocabulary { get; set; }
        public virtual DbSet<TaxonomyVocabularyTypes> TaxonomyVocabularyTypes { get; set; }
        public virtual DbSet<CatalogField> CatalogField { get; set; }
        public virtual DbSet<CatalogItem> CatalogItem { get; set; }
        public virtual DbSet<CatalogItemAttribute> CatalogItemAttribute { get; set; }
        public virtual DbSet<CatalogMaster> CatalogMaster { get; set; }
        public virtual DbSet<MetadataField> MetadataField { get; set; }
        public virtual DbSet<MetadataFieldTemplateRelationship> MetadataFieldTemplateRelationship { get; set; }
        public virtual DbSet<MetadataTemplate> MetadataTemplate { get; set; }

        #endregion bsd

        #region idm

        public virtual DbSet<IdmRight> IdmRight { get; set; }
        public virtual DbSet<IdmRightMapRole> IdmRightMapRole { get; set; }
        public virtual DbSet<IdmRightMapUser> IdmRightMapUser { get; set; }
        public virtual DbSet<IdmRole> IdmRole { get; set; }
        public virtual DbSet<IdmRoleMapOrganization> IdmRoleMapOrganization { get; set; }
        public virtual DbSet<IdmUser> IdmUser { get; set; }
        public virtual DbSet<IdmUserMapRole> IdmUserMapRole { get; set; }
        public virtual DbSet<IdmUserMapOrg> IdmUserMapOrg { get; set; }
        public virtual DbSet<IdmApi> IdmApi { get; set; }
        public virtual DbSet<IdmApiMapRole> IdmApiMapRole { get; set; }
        public virtual DbSet<IdmDevice> IdmDevice { get; set; }
        public virtual DbSet<IdmToken> IdmToken { get; set; }
        public virtual DbSet<IdmClient> IdmClient { get; set; }
        public virtual DbSet<IdmQRCode> IdmQRCode { get; set; }
        public virtual DbSet<IdmOTP> IdmOTP { get; set; }

        #endregion idm

        #region workflow

        public virtual DbSet<WorkflowInbox> WorkflowInboxes { get; set; }
        public virtual DbSet<WorkflowScheme> WorkflowSchemes { get; set; }
        public virtual DbSet<WorkflowGlobalParameter> WorkflowGlobalParameter { get; set; }
        public virtual DbSet<WorkflowDocument> WorkflowDocument { get; set; }
        public virtual DbSet<WorkflowDocumentHistory> WorkflowDocumentHistory { get; set; }
        public virtual DbSet<WorkflowApplication> WorkflowApplication { get; set; }
        public virtual DbSet<WorkflowInbox> WorkflowInbox { get; set; }
        public virtual DbSet<WorkflowProcessInstance> WorkflowProcessInstance { get; set; }
        public virtual DbSet<WorkflowProcessInstancePersistence> WorkflowProcessInstancePersistence { get; set; }
        public virtual DbSet<WorkflowProcessInstanceStatus> WorkflowProcessInstanceStatus { get; set; }
        public virtual DbSet<WorkflowProcessScheme> WorkflowProcessScheme { get; set; }
        public virtual DbSet<WorkflowProcessTimer> WorkflowProcessTimer { get; set; }
        public virtual DbSet<WorkflowProcessTransitionHistory> WorkflowProcessTransitionHistory { get; set; }

        public virtual DbSet<WorkflowSchemeVersion> WorkflowSchemeVersion { get; set; }
        public virtual DbSet<WorkflowSchemeAttribute> WorkflowSchemeAttribute { get; set; }

        #endregion workflow

        #region Form

        public virtual DbSet<BsdFormMaster> BsdFormMaster { get; set; }
        public virtual DbSet<BsdFormControl> BsdFormControl { get; set; }
        public virtual DbSet<BsdFormTemplate> BsdFormTemplate { get; set; }

        #endregion Form

        public virtual DbSet<LogSigning> LogSigning { get; set; }
        public virtual DbSet<LogAuthentication> LogAuthentication { get; set; }

        #region CRM
        public virtual DbSet<CrmAccount> CrmAccount { get; set; }
        public virtual DbSet<LogCheckinAccount> LogCheckinAccount { get; set; }
        #endregion

        #region Cms

        public virtual DbSet<CmsSignProfile> CmsSignProfile { get; set; }
        public virtual DbSet<CmsOrganization> CmsOrganization { get; set; }
        public virtual DbSet<CmsPosition> CmsPosition { get; set; }
        public virtual DbSet<CmsCert> CmsCert { get; set; }
        public virtual DbSet<CmsCertUser> CmsCertUser { get; set; }
        public virtual DbSet<SignLog> SignLog { get; set; }

        #endregion Cms

        public DataContext(string prefix, string databaseType, string connectionString, bool isTesting = false)
        {
            Prefix = prefix;
            DatabaseType = databaseType;
            ConnectionString = connectionString;
            IsTesting = isTesting;
        }

        public string Prefix { get; set; }
        public string DatabaseType { get; set; }
        public string ConnectionString { get; set; }
        public bool IsTesting { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                #region Config database

                if (IsTesting)
                {
                    optionsBuilder.UseInMemoryDatabase("testDatabase");
                }
                else
                {
                    switch (DatabaseType)
                    {
                        case "MySqlPomeloDatabase":
                            optionsBuilder.UseMySql(ConnectionString);
                            break;

                        case "MSSQLDatabase":
                            optionsBuilder.UseSqlServer(ConnectionString);
                            break;

                        case "OracleDatabase":
                            optionsBuilder.UseOracle(ConnectionString);
                            break;

                        case "PostgreSQLDatabase":
                            optionsBuilder.UseNpgsql(ConnectionString);
                            break;

                        case "Sqlite":
                            optionsBuilder.UseSqlite(ConnectionString);
                            break;

                        default:
                            optionsBuilder.UseSqlServer(ConnectionString);
                            break;
                    }
                }

                #endregion Config database

                optionsBuilder.ReplaceService<IModelCacheKeyFactory, DynamicModelCacheKeyFactory>();
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //modelBuilder.HasDefaultSchema("Core");
            foreach (var relationship in modelBuilder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
                relationship.DeleteBehavior = DeleteBehavior.Restrict;

            modelBuilder.Entity<WorkflowDocumentHistory>().HasOne(p => p.WorkflowDocument).WithMany(b => b.WorkflowDocumentHistorys)
                .HasForeignKey(p => p.WorkflowDocumentId)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<WorkflowDocumentAttachment>().HasOne(p => p.WorkflowDocument).WithMany(b => b.WorkflowDocumentAttachments)
                           .HasForeignKey(p => p.WorkflowDocumentId)
                           .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<BsdFormTemplate>().HasOne(p => p.FormMaster).WithMany(b => b.FormTemplates)
              .HasForeignKey(p => p.MasterId)
              .OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<VisitWebsite>().HasKey(x => x.Id);
            modelBuilder.Entity<Voucher>().HasKey(x => x.Id);
            modelBuilder.Entity<IdmRight>()
                .Property(b => b.LastModifiedOnDate)
                .ValueGeneratedOnAddOrUpdate();
            modelBuilder.ApplyConfiguration(new UserConfiguration());
            modelBuilder.ApplyConfiguration(new ProductConfiguration());
            modelBuilder.ApplyConfiguration(new CategoryConfiguration());
            modelBuilder.ApplyConfiguration(new Cart_ProductConfiguration());
            modelBuilder.ApplyConfiguration(new CartConfiguration());
            modelBuilder.ApplyConfiguration(new CategoryMetaConfiguration());
            modelBuilder.ApplyConfiguration(new CategoryMetaProductConfiguration());
            modelBuilder.ApplyConfiguration(new CategoryProductConfiguration());
            modelBuilder.ApplyConfiguration(new OrderConfiguration());
            modelBuilder.ApplyConfiguration(new OrderProductConfiguration());
            modelBuilder.ApplyConfiguration(new PictureConfiguration());
            modelBuilder.ApplyConfiguration(new ProductReviewConfiguration());
            modelBuilder.ApplyConfiguration(new ProductTagConfiguration());
            modelBuilder.ApplyConfiguration(new SupplierConfiguration());
            modelBuilder.ApplyConfiguration(new TagConfiguration());
            modelBuilder.ApplyConfiguration(new CityConfiguration());
            modelBuilder.ApplyConfiguration(new CommuneConfiguration());
            modelBuilder.ApplyConfiguration(new DistrictConfiguration());
            modelBuilder.ApplyConfiguration(new NotifiConfiguration());
            modelBuilder.Entity<IdmRightMapRole>().HasKey(c => new { c.ApplicationId, c.RightId, c.RoleId });
            modelBuilder.Entity<IdmRightMapUser>().HasKey(c => new { c.ApplicationId, c.RightId, c.UserId });
            modelBuilder.Entity<IdmUserMapRole>().HasKey(c => new { c.ApplicationId, c.UserId, c.RoleId });
        }
    }

    public class DynamicModelCacheKeyFactory : IModelCacheKeyFactory
    {
        public object Create(DbContext context)
        {
            var isTesting = (Utils.GetConfig("ConnectionString:IsTesting") == "HAS_TEST");
            var databaseType = Utils.GetConfig("ConnectionString:DbType");
            var connectionString = Utils.GetConfig("ConnectionString:" + databaseType);
            if (context is DataContext dynamicContext)
            {
                return (context.GetType(), dynamicContext.Prefix, databaseType, connectionString, isTesting);
            }

            return context.GetType();
        }
    }
}