using Microsoft.EntityFrameworkCore;

namespace DigitalID.Data
{
    public interface IDatabaseFactory
    {
        DbContext GetDbContext();
        string GetPrefix();
    }
}