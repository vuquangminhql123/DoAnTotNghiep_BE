﻿using System;
using System.Collections.Generic;

namespace DigitalID.Data
{
    #region Systems
    public class TaxonomyConstants
    {
        public static Guid CATEGORY_MASTER => new Guid("00000000-0000-0000-0000-000000000001");
    }

    public class AppConstants
    {
        public static string EnvironmentName = "production";
        public static Guid RootAppId => new Guid("00000000-0000-0000-0000-000000000001");
        public static Guid TestAppId => new Guid("00000000-0000-0000-0000-000000000002");
    }

    public class UserConstants
    {
        public static Guid AdministratorId => new Guid("00000000-0000-0000-0000-000000000001");
        public static Guid UserId => new Guid("00000000-0000-0000-0000-000000000002");
    }

    public class RoleConstants
    {
        public static Guid AdministratorId => new Guid("00000000-0000-0000-0000-000000000001");
        public static Guid UserId => new Guid("00000000-0000-0000-0000-000000000002");
        public static Guid GuestId => new Guid("00000000-0000-0000-0000-000000000002");
        public static Guid CustomerId => new Guid("00000000-0000-0000-0000-000000000003");
    }

    public class RightConstants
    {
        public static Guid AccessAppId => new Guid("00000000-0000-0000-0000-000000000001");
        public static string AccessAppCode = "TRUY_CAP_HE_THONG";

        public static Guid DefaultAppId => new Guid("00000000-0000-0000-0000-000000000002");
        public static string DefaultAppCode = "TRUY_CAP_MAC_DINH";

        public static Guid FileAdministratorId => new Guid("00000000-0000-0000-0000-000000000003");
        public static string FileAdministratorCode = "QUAN_TRI_FILE";

        public static Guid PemissionId => new Guid("00000000-0000-0000-0000-000000000004");
        public static string PemissionCode = "PHAN_QUYEN";
    }

    public class MdmConstants
    {
        public static Guid PartitionDefaultId => new Guid("00000000-0000-0000-0000-000000000001");
        public static Guid MetadataTemplateDefaultId => new Guid("00000000-0000-0000-0000-000000000001");
        public static Guid MetadataField1Id => new Guid("00000000-0000-0000-0000-000000000001");
        public static Guid MetadataField2Id => new Guid("00000000-0000-0000-0000-000000000002");
    }

    public class NavigationConstants
    {
        public static Guid SystemNav => new Guid("00000000-0000-0000-0000-000000000001");
        public static Guid RoleNav => new Guid("00000000-0000-0000-0000-000000000011");
        public static Guid RightNav => new Guid("00000000-0000-0000-0000-000000000021");
        public static Guid UserNav => new Guid("00000000-0000-0000-0000-000000000031");
        public static Guid PartitionNav => new Guid("00000000-0000-0000-0000-000000000041");
        public static Guid MetaTemplateNav => new Guid("00000000-0000-0000-0000-000000000051");
        public static Guid OrganizationNav => new Guid("00000000-0000-0000-0000-000000000061");
        public static Guid PositionNav => new Guid("00000000-0000-0000-0000-000000000071");
        public static Guid LogNav => new Guid("00000000-0000-0000-0000-000000000081");
        public static Guid ParameterNav => new Guid("00000000-0000-0000-0000-000000000091");
        public static Guid SignProfileNav => new Guid("00000000-0000-0000-0000-000000000101");

        public static Guid PermissionNav => new Guid("00000000-0000-0000-0000-000000000002");
        public static Guid NavNav => new Guid("00000000-0000-0000-0000-000000000012");
        public static Guid RMUNav => new Guid("00000000-0000-0000-0000-000000000022");

        public static Guid NodeNav => new Guid("00000000-0000-0000-0000-000000000003");

        public static Guid FormNav => new Guid("00000000-0000-0000-0000-000000000004");

        public static Guid FormMasterNav => new Guid("00000000-0000-0000-0000-000000000014");
        public static Guid FormControlNav => new Guid("00000000-0000-0000-0000-000000000024");

        public static Guid WorkflowNav => new Guid("00000000-0000-0000-0000-000000000005");
        public static Guid WorkflowSchemeNav => new Guid("00000000-0000-0000-0000-000000000015");
        public static Guid WorkflowInstanceNav => new Guid("00000000-0000-0000-0000-000000000025");
        public static Guid WorkflowBussinessFlowNav => new Guid("00000000-0000-0000-0000-000000000035");
        public static Guid WorkflowApplicationNav => new Guid("00000000-0000-0000-0000-000000000045");

        public static Guid CatalogNav => new Guid("00000000-0000-0000-0000-000000000006");
    }

    public static class ConfigType
    {
        #region NodeType

        public const int File = 1;
        public const int Folder = 0;

        #endregion NodeType

        #region NodeStatus

        public const int NodeStatusDeleted = -1;
        public const int NodeStatusRecycled = 0;
        public const int NodeStatusReady = 1;
        public const int NodeStatusScaningVirus = 2;

        //Đang xử lý
        public const int NodeStatusProcessing = 13;

        #endregion NodeStatus

        #region NodeMoving

        public const int NodeMoveValid = 0;
        public const int NodeMoveDuplicateName = 1;
        public const int NodeMoveParentConflict = 2;
        public const int NodeMoveSelf = 3;

        #endregion NodeMoving

        #region NodeProcessCmd

        public const string NodeProcessCmdSuccess = "SUCCESS";
        public const string NodeProcessCmdError = "ERROR";
        public const string NodeProcessCmdSchedule = "SCHEDULE";

        #endregion NodeProcessCmd

        #region Status Response

        public const int SUCCESS = 1;
        public const int NODATA = 0;
        public const int ERROR = -1;

        #endregion Status Response
    }

    public static class WFConstants
    {
        public const string NEW = "SCHEDULE";
        public const string INPROCESS = "SCHEDULE";
        public const string COMPLETE = "SCHEDULE";
        public const string REJECT = "SCHEDULE";

        public const string SIGNRURL = "notification/wf-process";
    }

    public static class NameConstants
    {
        public static string GetModuleName(string name)
        {
            switch (name)
            {
                case "Core.Data.BsdParameter":
                    return "Tham số hệ thống";

                case "Core.Data.BsdNavigation":
                    return "Điều hướng";

                case "Core.Data.IdmRight":
                    return "Quyền";

                case "Core.Data.IdmRole":
                    return "Nhóm người dùng";

                case "Core.Data.WorkflowApplication":
                    return "Ứng dụng bên thứ 3";

                case "Core.Data.WorkflowSchemeAttribute":
                    return "Quy trình";

                case "Core.Data.BsdFormMaster":
                    return "Biểu mẫu";

                case "Core.Data.CmsSignProfile":
                    return "Profile ký";

                case "Core.Data.CmsOrganization":
                    return "Đơn vị/pb";

                case "Core.Data.CmsPosition":
                    return "Chức vụ";

                default:
                    return null;
            }
        }

        public static string GetPropName(string name)
        {
            switch (name)
            {
                case "Code":
                    return "Mã";

                default:
                    return null;
            }
        }

        public static List<string> GetPropName(List<string> names)
        {
            var result = new List<string>();
            foreach (var name in names)
            {
                result.Add(GetPropName(name));
            }
            return result;
        }
    }

    public class ProductionState
    {
        public const string CONFIRMED = "confirmed";
        public const string PLANNED = "planned";
        public const string WORK_ORDER = "workorder";
        public const string PROGRESS = "progress";
        public const string DONE = "done";

        public static string ToString(string code)
        {
            switch (code)
            {
                case CONFIRMED:
                    return "Đã được xác nhận";

                case PLANNED:
                    return "Đã lên kế hoạch";

                case DONE:
                    return "Hoàn thành";

                case PROGRESS:
                    return "Đang thực hiện";
            }
            return "";
        }
    }

    public class SelectListItem
    {
        public string Text { get; set; }
        public string Value { get; set; }
    }

    public class SelectItemModel
    {
        public string Avatar { get; set; }
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Note { get; set; }
    }


    public class MessageConstants
    {
        public static string ErrorLogMessage = "An error occurred: ";
        public static string CreateSuccessMessage = "Thêm mới thành công";
        public static string CreateErrorMessage = "Thêm mới thất bại";
        public static string UpdateSuccessMessage = "Cập nhật thành công";
        public static string UpdateErrorMessage = "Cập nhật thất bại";
        public static string DeleteSuccessMessage = "Kết quả xóa";
        public static string DeleteErrorMessage = "Xóa thất bại";
        public static string DeleteItemSuccessMessage = "Xóa thành công";
        public static string DeleteItemErrorMessage = "Xóa không thành công";
        public static string DeleteItemNotFoundMessage = "Không tìm thấy đối tượng";
        public static string GetDataSuccessMessage = "Tải dữ liệu thành công";
        public static string GetDataErrorMessage = "Tải dữ liệu thất bại";
    }

    public class ClaimConstants
    {
        public static string USER_ID = "x-user-id";
        public static string USER_NAME = "x-user-name";
        public static string FULL_NAME = "x-full-name";
        public static string AVATAR = "x-avatar";
        public static string APP_ID = "x-app-id";
        public static string APPS = "x-app";
        public static string ROLES = "x-role";
        public static string RIGHTS = "x-right";
        public static string ISSUED_AT = "x-iat";
        public static string EXPIRES_AT = "x-exp";
        public static string CHANNEL = "x-channel";
    }

    #endregion

    #region Bussiness

    public class QueryFilter
    {
        public const int DefaultPageNumber = 1;
        public const int DefaultPageSize = 20;
    }

    #endregion

    public static class CustomerInfoCertStatus
    {
        public const int KEY_NOT_CREADED = 0;
        public const int GENERATED_KEY_NOT_CERT = 1;
        public const int GENERATED_CERT = 2;

        public static string CustomerInfoCertStatusToString(this int code)
        {
            switch (code)
            {
                case KEY_NOT_CREADED:
                    return "Chưa tạo khóa";

                case GENERATED_KEY_NOT_CERT:
                    return "Đã tạo khóa, chưa tạo cert";

                case GENERATED_CERT:
                    return "Đã tạo cert";
            }
            return "";
        }
    }

    public class CellStyle
    {
        public string BackgroundColor { get; set; }
        public string FontColor { get; set; }
        public string FontFamily { get; set; }
        public string FontSize { get; set; }
        public string FontWeight { get; set; }
    }
}