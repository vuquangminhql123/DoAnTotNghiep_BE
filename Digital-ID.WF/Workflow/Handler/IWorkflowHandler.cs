﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DigitalID.Data;

namespace DigitalID.WF
{
    public interface IWorkflowHandler
    {
        Task<Response> GetOutbox(string identityId);
        Task<Response> GetInbox(string identityId);
        Task<Response> GetFormByFlow(string name, Guid processId, string identityid);
        Task<Response> GetCreateFormByFlow(string name);


    }
}