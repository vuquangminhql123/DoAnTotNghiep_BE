﻿using DigitalID.Data;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Serilog;
using System.Linq;
using Newtonsoft.Json;
using DigitalID.WF.Workflow;
using IO.Swagger.Models;
using DigitalID.Business;
using DigitalID.Data.Models;

namespace DigitalID.WF
{
    public class WorkflowHandler : IWorkflowHandler
    {
        private readonly IWorkflowSchemeHandler _workflowScheme;
        public WorkflowHandler(IWorkflowSchemeHandler workflowScheme)
        {
            _workflowScheme = workflowScheme;
        }
        public async Task<Response> GetInbox(string identityid)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    var query = await unitOfWork.GetRepository<WorkflowInbox>().GetAll().Where(x => x.IdentityId == identityid).ToListAsync();

                    return new ResponseList<WorkflowInbox>(query);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return new ResponseError(Code.ServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }
        }
        public async Task<Response> GetOutbox(string identityId)
        {
            try
            {
                using (var unitOfWork = new UnitOfWork())
                {
                    var query = await unitOfWork.GetRepository<WorkflowProcessTransitionHistory>().GetAll()
                        .Where(x => x.ExecutorIdentityId == identityId).Select(s => new OutboxItem()
                        {
                            ExecutorIdentityId = identityId,
                            ProcessId = s.ProcessId
                        }).ToListAsync();
                    return new ResponseList<OutboxItem>(query);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return new ResponseError(Code.ServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }

        }
        public async Task<Response> GetFormByFlow(string name, Guid processId, string identityid)
        {

            try
            {

                using (var unitOfWork = new UnitOfWork())
                {
                    //step 1: get flow
                    var flow = await unitOfWork.GetRepository<WorkflowGlobalParameter>().FindAsync(x => x.Name == name);
                    if (flow == null)
                    {
                        return new ResponseError(Code.BadRequest, "BussinessFlow not found");
                    }
                    //step 2: caculate form
                    var bussinessFlow = JsonConvert.DeserializeObject<BussinessFlow>(flow.Value);
                    //step 2.1: get state
                    var currentstate = await WorkflowInit.Runtime.GetCurrentStateAsync(processId);
                    var formResultStr = bussinessFlow.DefaultForm;
                    var allActor = await _workflowScheme.GetAllActor(bussinessFlow.Scheme);
                    foreach (var map in bussinessFlow.Map)
                    {
                        if (map.States.Contains(currentstate.Name) || map.States.Contains("*"))
                        {
                            foreach (var role in map.Roles)
                            {
                                var processInstance = WorkflowInit.Runtime.GetProcessInstanceAndFillProcessParameters(processId);
                                var actorDetail = allActor.FirstOrDefault(x => x.Name == role);
                                if (actorDetail != null)
                                {
                                    var checkIdentity = WorkflowInit.Runtime.CheckIdentity(processInstance, identityid, actorDetail.Rule, actorDetail.Value);
                                    if (checkIdentity || role == "*")
                                    {
                                        formResultStr = map.Form;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    if(!Utils.IsGuid(formResultStr)){
                         return new ResponseError(Code.NotFound, "Không tìm được dữ liệu");
                    }
                    var formId = new Guid(formResultStr);
                    var currentForm = await unitOfWork.GetRepository<BsdFormMaster>().FindAsync(formId);
                    return new ResponseObject<FormMasterModel>(AutoMapperUtils.AutoMap<BsdFormMaster, FormMasterModel>(currentForm));
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return new ResponseError(Code.ServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }

        }

        public async Task<Response> GetCreateFormByFlow(string name)
        {

            try
            {

                using (var unitOfWork = new UnitOfWork())
                {
                    //step 1: get flow
                    var flow = await unitOfWork.GetRepository<WorkflowGlobalParameter>().FindAsync(x => x.Name == name);
                    if (flow == null)
                    {
                        return new ResponseError(Code.BadRequest, "BussinessFlow not found");
                    }
                    //step 2: caculate form
                    var bussinessFlow = JsonConvert.DeserializeObject<BussinessFlow>(flow.Value);
                    //step 2.1: get state
                    var formMasterId =bussinessFlow.DefaultCreateForm;
                    if(!Utils.IsGuid(formMasterId)){
                         return new ResponseError(Code.NotFound, "Không tìm được dữ liệu");
                    }
                    var formId = new Guid(formMasterId);
                    var currentForm = await unitOfWork.GetRepository<BsdFormMaster>().FindAsync(formId);
                    return new ResponseObject<FormMasterModel>(AutoMapperUtils.AutoMap<BsdFormMaster, FormMasterModel>(currentForm));
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "");
                return new ResponseError(Code.ServerError, "Có lỗi trong quá trình xử lý: " + ex.Message);
            }

        }


    }
}
