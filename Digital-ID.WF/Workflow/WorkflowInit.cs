﻿using System;
using System.Reflection;
using System.Xml.Linq;
using OptimaJet.Workflow.Core.Builder;
using OptimaJet.Workflow.Core.Bus;
using OptimaJet.Workflow.Core.Parser;
using OptimaJet.Workflow.Core.Runtime;
using OptimaJet.Workflow.Core.Persistence;
using DigitalID.WF.DataAccess;
using System.Threading.Tasks;
using System.Collections.Generic;
using DigitalID.Business;
using DigitalID.Data;
using DigitalID.WF.Implementation;
using Newtonsoft.Json;
using OptimaJet.Workflow.Core.Subprocess;
using OptimaJet.Workflow.Core.Model;
using Serilog;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Linq;

namespace DigitalID.WF.Workflow
{
    public static class WorkflowInit
    {
        private static volatile WorkflowRuntime _runtime;

        private static readonly object _sync = new object();

        public static IWorkflowDocumentHandler WorkflowDocumentHandler { get; private set; }
        public static IWorkflowApplicationHandler WorkflowApplicationHandler { get; private set; }
        public static IWorkflowDocumentHistoryHandler WorkflowDocumentHistoryHandler { get; private set; }
        public static IWorkflowInboxHandler WorkflowInboxHandler { get; private set; }
        private static IUserMapRoleHandler userMapRoles;
        public static IPersistenceProviderContainer provider { get; private set; }

        public static WorkflowRuntime Create()
        {
            WorkflowDocumentHandler = new WorkflowDocumentHandler();
            WorkflowDocumentHistoryHandler = new WorkflowDocumentHistoryHandler();
            WorkflowApplicationHandler = new WorkflowApplicationHandler();
            provider = new PersistenceProviderContainer();
            userMapRoles = new UserMapRoleHandler();
            WorkflowInboxHandler = new WorkflowInboxHandler(WorkflowDocumentHistoryHandler);
            //CreateRuntime();

            return Runtime;
        }

        //private static void CreateRuntime()
        //{
        //    if (_runtime == null)
        //    {
        //        lock (_sync)
        //        {
        //            if (_runtime == null)
        //            {
        //                var builder = new WorkflowBuilder<XElement>(provider.AsWorkflowGenerator,
        //                    new XmlWorkflowParser(),
        //                    provider.AsSchemePersistenceProvider
        //                    ).WithDefaultCache();
        //                _runtime = new WorkflowRuntime();
        //                _runtime.WithBuilder(builder);
        //                _runtime.WithActionProvider(new ActionProvider());
        //                _runtime.WithRuleProvider(new RuleProvider(userMapRoles));
        //                _runtime.WithPersistenceProvider(provider.AsPersistenceProvider);
        //                _runtime.WithTimerManager(new TimerManager());
        //                _runtime.WithBus(new NullBus());
        //                _runtime.SwitchAutoUpdateSchemeBeforeGetAvailableCommandsOn();
        //                _runtime.RegisterAssemblyForCodeActions(Assembly.GetExecutingAssembly());
        //                _runtime.Start();
        //                _runtime.ProcessStatusChanged += _runtime_ProcessStatusChanged;
        //            }
        //        }
        //    }

        //    WorkflowRuntime.RegisterLicense("Nexdata-TmV4ZGF0YTowMS4yOS4yMDIwOmV5Sk5ZWGhPZFcxaVpYSlBaa0ZqZEdsMmFYUnBaWE1pT2kweExDSk5ZWGhPZFcxaVpYSlBabFJ5WVc1emFYUnBiMjV6SWpvdE1Td2lUV0Y0VG5WdFltVnlUMlpUWTJobGJXVnpJam90TVN3aVRXRjRUblZ0WW1WeVQyWlVhSEpsWVdSeklqb3RNU3dpVFdGNFRuVnRZbVZ5VDJaRGIyMXRZVzVrY3lJNkxURjk6bG5BY0VvYnpTMmlzYzRBeDllRGtkVnNBbzJqdUduNU81aWYrQmZhNE1NeDlkNUM4U1MyTmM5RnFYaGpRdVlYQ0Y1ZUtuZmp6anA1cTgvQ3M0Qk5vWC9ZQlc3NTh3ZzNWa1o0cy9nQ240aGpPL3J5eEsreENEcnZPQXFsVUwvY2s3QzhRWSthSFNnbnhFY2IrQjhManNtMnBqRXNiV2gzdmdFVHpmZ1k0WHdvPQ==");

        //}

        public static WorkflowRuntime Runtime => _runtime;
        static void _runtime_ProcessStatusChanged(object sender, ProcessStatusChangedEventArgs e)
        {
            var status = "";
            #region status
            if (e.NewStatus == ProcessStatus.NotFound)
            {
                status = "NotFound";
            }
            if (e.NewStatus == ProcessStatus.Unknown)
            {
                status = "Unknown";
            }
            if (e.NewStatus == ProcessStatus.Initialized)
            {
                status = "Initialized";
            }
            if (e.NewStatus == ProcessStatus.Running)
            {
                status = "Running";
            }
            if (e.NewStatus == ProcessStatus.Idled)
            {
                status = "Idled";
            }
            if (e.NewStatus == ProcessStatus.Finalized)
            {
                status = "Finalized";
            }
            if (e.NewStatus == ProcessStatus.Terminated)
            {
                status = "Terminated";
            }
            if (e.NewStatus == ProcessStatus.Error)
            {
                status = "Error";
            }
            #endregion
            if (e.NewStatus != ProcessStatus.Idled && e.NewStatus != ProcessStatus.Finalized)
                return;


            if (string.IsNullOrEmpty(e.SchemeCode))
                return;

            if (!e.IsSubprocess)
            {
                // Prev
                var prevState = "";
                var prevActivity = "";
                prevState = e.ProcessInstance.PreviousActivityName;
                prevActivity = e.ProcessInstance.PreviousState;
                // Currrent
                var activity = "";
                var state = "";
                activity = e.ProcessInstance.CurrentActivityName;
                state = e.ProcessInstance.CurrentState;
                var allowIdentityIds = "";
                var allowIdentityIdsList = _runtime.GetAllActorsForAllCommandTransitions(e.ProcessId).ToList();
                allowIdentityIds = JsonConvert.SerializeObject(allowIdentityIdsList);
                var transitionClassifier = 0;
                if (e.ProcessInstance.ExecutedTransition != null)
                {
                    transitionClassifier = e.ProcessInstance.ExecutedTransition.Classifier.GetHashCode();
                }
                var executedTransition = DateTime.Now;
                Guid.TryParse(e.ProcessInstance.IdentityId, out Guid executedIdentityIdTry);
                Guid? executedIdentityId = null;
                if (executedIdentityIdTry != Guid.Empty)
                {
                    executedIdentityId = executedIdentityIdTry;
                }
                var executedAction = "";
                var executedActionValue = "";
                var timmerName = e.ProcessInstance.ExecutedTimer;
                var commandName = e.ProcessInstance.CurrentCommand;
                if (string.IsNullOrEmpty(commandName))
                {
                    executedAction = "auto";
                }
                else if (!string.IsNullOrEmpty(timmerName) && commandName == "Timer")
                {
                    executedAction = "timmer";
                    executedActionValue = timmerName;

                }
                else
                {
                    executedAction = "command";
                    executedActionValue = commandName;
                }
                var executedFormInputJson = "";
                var executedFiles = "";
                if (executedAction == "command")
                {
                    executedFormInputJson = e.ProcessInstance.GetParameter<string>("FormInputJson");
                    executedFiles = e.ProcessInstance.GetParameter<string>("FileInputJson");
                }
                //Do PreExecute
                _runtime.PreExecuteFromCurrentActivity(e.ProcessId);
                // Next
                var nextState = "";
                var nextActivity = "";
                nextState = e.ProcessInstance.CurrentState;
                nextActivity = e.ProcessInstance.CurrentActivityName;
                var listSubProcess = new List<CustomSubProcess>();
                var processTree = Runtime.GetProcessInstancesTree(e.ProcessId);
                if (processTree != null)
                {
                    foreach (var child in processTree.Children)
                    {
                        listSubProcess.Add(new CustomSubProcess()
                        {
                            State = Runtime.GetCurrentStateName(child.Id),
                            Activity = Runtime.GetCurrentActivityName(child.Id),

                        });
                    }
                }

                // Cập nhật thông tin 
                var wfDocumentInfo = new WorkflowDocumentUpdateInfoModel()
                {
                    Status = status,
                    PrevState = prevState,
                    PrevActivity = prevActivity,
                    State = state,
                    Activity = activity,
                    NextActivity = nextActivity,
                    NextState = nextState,
                    AllowIdentityIds = allowIdentityIds,
                    ListSubState = JsonConvert.SerializeObject(listSubProcess),
                    TransitionClassifier = transitionClassifier,
                    ExecutedTransition = executedTransition,
                    ExecutedIdentityId = executedIdentityId,
                    ExecutedAction = executedAction,
                    ExecutedActionValue = executedActionValue,
                    ExecutedFormInputJson = executedFormInputJson,
                    ExecutedFiles = executedFiles
                };
                var result = WorkflowDocumentHandler.UpdateInfoAsync(e.ProcessId, wfDocumentInfo).Result;
                var documentName = "";
                if (result.Code == Code.Success && result is ResponseObject<WorkflowDocumentModel> resultData)
                {
                    documentName = resultData.Data.Name;
                }
                // Lưu lịch sử
                var historyId = Guid.NewGuid();
                WorkflowDocumentHistoryHandler.CreateAsync(new WorkflowDocumentHistoryCreateModel()
                {
                    Id = historyId,
                    DocumentName = documentName,
                    WorkflowDocumentId = e.ProcessId,
                    Status = status,
                    Scheme = e.ProcessInstance.SchemeCode,
                    PrevState = prevState,
                    PrevActivity = prevActivity,
                    State = state,
                    Activity = activity,
                    ListSubState = JsonConvert.SerializeObject(listSubProcess),
                    TransitionClassifier = transitionClassifier,
                    ExecutedTransition = executedTransition,
                    ExecutedIdentityId = executedIdentityId,
                    IsSubProcess = e.IsSubprocess,
                    ExecutedAction = executedAction,
                    ExecutedActionValue = executedActionValue,
                    ExecutedFormInputJson = executedFormInputJson,
                    ExecutedFiles = executedFiles,

                }, AppConstants.RootAppId, UserConstants.AdministratorId);

                // Lưu thông báo
                if (allowIdentityIdsList != null && allowIdentityIdsList.Any())
                {
                    WorkflowInboxHandler.CreateManyAsync(e.ProcessId, allowIdentityIdsList, historyId);
                }
                // Lấy ra thông tin ứng dụng
                sendRequestTo3rdApp(e.ProcessId, wfDocumentInfo);
                sendRequestToSignR();

            }
            else
            {
                // Prev
                var prevState = "";
                var prevActivity = "";
                prevState = e.ProcessInstance.PreviousActivityName;
                prevActivity = e.ProcessInstance.PreviousState;
                // Currrent
                var activity = "";
                var state = "";
                activity = e.ProcessInstance.CurrentActivityName;
                state = e.ProcessInstance.CurrentState;
                var allowIdentityIds = "";
                var allowIdentityIdsList = _runtime.GetAllActorsForAllCommandTransitions(e.ProcessId).ToList();
                allowIdentityIds = JsonConvert.SerializeObject(allowIdentityIdsList);
                var transitionClassifier = 0;
                if (e.ProcessInstance.ExecutedTransition != null)
                {
                    transitionClassifier = e.ProcessInstance.ExecutedTransition.Classifier.GetHashCode();
                }
                var executedTransition = DateTime.Now;
                if (status == "Finalized")
                {
                    executedTransition = executedTransition.AddSeconds(-10);// Fix  3s
                }
                else
                {
                    executedTransition = executedTransition.AddSeconds(10);// Fix  3s
                }
                Guid.TryParse(e.ProcessInstance.IdentityId, out Guid executedIdentityIdTry);
                Guid? executedIdentityId = null;
                if (executedIdentityIdTry != Guid.Empty)
                {
                    executedIdentityId = executedIdentityIdTry;
                }
                var executedAction = "";
                var executedActionValue = "";
                var timmerName = e.ProcessInstance.ExecutedTimer;
                var commandName = e.ProcessInstance.CurrentCommand;
                if (string.IsNullOrEmpty(commandName))
                {
                    executedAction = "auto";
                }
                else if (!string.IsNullOrEmpty(timmerName) && commandName == "Timer")
                {
                    executedAction = "timmer";
                    executedActionValue = timmerName;

                }
                else
                {
                    executedAction = "command";
                    executedActionValue = commandName;
                }
                var executedFormInputJson = "";
                var executedFiles = "";
                if (executedAction == "command")
                {
                    executedFormInputJson = e.ProcessInstance.GetParameter<string>("FormInputJson");
                    executedFiles = e.ProcessInstance.GetParameter<string>("FileInputJson");
                }

                var listSubProcess = new List<CustomSubProcess>();
                var processTree = Runtime.GetProcessInstancesTree(e.ProcessInstance.ParentProcessId.Value);
                if (processTree != null)
                {
                    foreach (var child in processTree.Children)
                    {
                        listSubProcess.Add(new CustomSubProcess()
                        {
                            State = Runtime.GetCurrentStateName(child.Id),
                            Activity = Runtime.GetCurrentActivityName(child.Id),

                        });
                    }
                }
                var nextState = "";
                var nextActivity = "";
                //Do PreExecute
                if (_runtime.IsProcessExists(e.ProcessId))
                {
                    _runtime.PreExecuteFromCurrentActivity(e.ProcessId);
                    // Next
                    nextState = e.ProcessInstance.CurrentState;
                    nextActivity = e.ProcessInstance.CurrentActivityName;
                }
                // Cập nhật thông tin 
                var wfDocumentInfo = new WorkflowDocumentUpdateInfoModel()
                {
                    //Status = status,
                    //PrevState = prevState,
                    //PrevActivity = prevActivity,
                    //State = state,
                    //Activity = activity,
                    //NextActivity = nextActivity,
                    //NextState = nextState,
                    AllowIdentityIds = allowIdentityIds,
                    ListSubState = JsonConvert.SerializeObject(listSubProcess),
                    TransitionClassifier = transitionClassifier,
                    ExecutedTransition = executedTransition,
                    ExecutedIdentityId = executedIdentityId,
                    ExecutedAction = executedAction,
                    ExecutedActionValue = executedActionValue,
                    ExecutedFormInputJson = executedFormInputJson,
                    ExecutedFiles = executedFiles
                };
                var result = WorkflowDocumentHandler.UpdateInfoAsync(e.ProcessInstance.ParentProcessId.Value, wfDocumentInfo).Result;
                var documentName = "";
                if (result.Code == Code.Success && result is ResponseObject<WorkflowDocumentModel> resultData)
                {
                    documentName = resultData.Data.Name;
                }

                // Lưu lịch sử
                var historyId = Guid.NewGuid();
                WorkflowDocumentHistoryHandler.CreateAsync(new WorkflowDocumentHistoryCreateModel()
                {
                    Id = historyId,
                    DocumentName = documentName,
                    WorkflowDocumentId = e.ProcessInstance.ParentProcessId.Value,
                    Status = status,
                    Scheme = e.ProcessInstance.SchemeCode,
                    PrevState = prevState,
                    PrevActivity = prevActivity,
                    State = state,
                    Activity = activity,
                    ListSubState = JsonConvert.SerializeObject(listSubProcess),
                    TransitionClassifier = transitionClassifier,
                    ExecutedTransition = executedTransition,
                    ExecutedIdentityId = executedIdentityId,
                    IsSubProcess = e.IsSubprocess,
                    ExecutedAction = executedAction,
                    ExecutedActionValue = executedActionValue,
                    ExecutedFormInputJson = executedFormInputJson,
                    ExecutedFiles = executedFiles
                }, AppConstants.RootAppId, UserConstants.AdministratorId);
                // Lưu thông báo
                if (allowIdentityIdsList != null && allowIdentityIdsList.Any())
                {
                    WorkflowInboxHandler.CreateManyAsync(e.ProcessInstance.ParentProcessId.Value, allowIdentityIdsList, historyId);
                }
                // Lấy ra thông tin ứng dụng
                sendRequestTo3rdApp(e.ProcessInstance.ParentProcessId.Value, wfDocumentInfo);
                sendRequestToSignR();
            }
        }
        private static void sendRequestTo3rdApp(Guid id, WorkflowDocumentUpdateInfoModel bodyRequest)
        {
            try
            {
                var currentConfigStr = WorkflowApplicationHandler.GetConfig(id);
                if (!string.IsNullOrEmpty(currentConfigStr))
                {
                    var currentConfig = JsonConvert.DeserializeObject<WorkflowAppicationConfig>(currentConfigStr);
                    if (currentConfig != null && currentConfig.apiUri != null)
                    {
                        using (var httpClientHandler = new HttpClientHandler())
                        {
                            httpClientHandler.ServerCertificateCustomValidationCallback = (message, cert, chain, errors) => { return true; };
                            using (var httpClient = new HttpClient(httpClientHandler))
                            {
                                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                                currentConfig.apiUri = currentConfig.apiUri ?? "";
                                currentConfig.authorization = currentConfig.authorization ?? "";
                                string url = currentConfig.apiUri;
                                var headerSplit = currentConfig.authorization.Split(' ');
                                if (headerSplit != null && headerSplit.Length > 2)
                                {
                                    httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(headerSplit[0], headerSplit[1]);
                                }

                                var bodyPayload = new BodyPayloadModel()
                                {
                                    ProcessId = id,
                                    WorkflowDocumentUpdateInfoModel = bodyRequest
                                };
                                var content = new StringContent(JsonConvert.SerializeObject(bodyPayload), Encoding.UTF8, "application/json");
                                var response = httpClient.PostAsync(url, content).Result;
                                string jsonResult = response.Content.ReadAsStringAsync().Result;
                            }

                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                Log.Error(ex, "");
            }
        }
        private static void sendRequestToSignR()
        {
            try
            {
                using (var httpClientHandler = new HttpClientHandler())
                {
                    httpClientHandler.ServerCertificateCustomValidationCallback = (message, cert, chain, errors) => { return true; };
                    using (var httpClient = new HttpClient(httpClientHandler))
                    {
                        string url = Utils.GetConfig("AppSettings:Host") + WFConstants.SIGNRURL;

                        var response = httpClient.GetAsync(url).Result;
                    }

                }
            }
            catch (System.Exception ex)
            {
                Log.Error(ex, "");
            }
        }
    }


    public class CustomSubProcess
    {
        public string Activity { get; set; }
        public string State { get; set; }
    }
    public class WorkflowAppicationConfig
    {
        public string apiUri { get; set; }
        public string authorization { get; set; }
        public string bodyPayload { get; set; }
    }
    public class BodyPayloadModel
    {
        public Guid ProcessId { get; set; }
        public WorkflowDocumentUpdateInfoModel WorkflowDocumentUpdateInfoModel { get; set; }
    }

}
