#region BuildAPI
FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS build

WORKDIR /opt/core.api/src

# Copy file to image
COPY Digital-ID.API/Digital-ID.API.csproj Digital-ID.API/
COPY Digital-ID.Business/Digital-ID.Business.csproj Digital-ID.Business/
COPY Digital-ID.Data/Digital-ID.Data.csproj Digital-ID.Data/
COPY Digital-ID.WF/Digital-ID.WF.csproj Digital-ID.WF/

# Run restore dependency
RUN dotnet restore Digital-ID.API/Digital-ID.API.csproj

# Copy file all file to dir
COPY Digital-ID.API Digital-ID.API
COPY Digital-ID.Business Digital-ID.Business
COPY Digital-ID.Data Digital-ID.Data
COPY Digital-ID.WF Digital-ID.WF

#Replace appsettings.json with appsettings 
COPY Digital-ID.API/appsettings.Testing.json Digital-ID.API/appsettings.json

#region test
# Set work dir
WORKDIR /opt/core.api/src/Digital-ID.API

# Build image 
RUN dotnet build Digital-ID.API.csproj -c Release -o /opt/core.api/build
# Publish image
RUN dotnet publish Digital-ID.API.csproj -c Release -o /opt/core.api/publish
#endregion 

# Final Image
FROM mcr.microsoft.com/dotnet/core/aspnet:3.1 AS final
WORKDIR /opt/core.api/publish

COPY --from=build /opt/core.api/publish .
RUN  ["rm", "-rf", "/etc/localtime"]

RUN  ["ln", "-s", "/usr/share/zoneinfo/Asia/Ho_Chi_Minh", "/etc/localtime"]
EXPOSE 80
EXPOSE 587/tcp
CMD ["dotnet","Digital-ID.API.dll"]

# CMD ["sh", "-c", "tail -f /dev/null"]