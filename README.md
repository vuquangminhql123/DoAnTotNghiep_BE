﻿# Project: Workflow.Core

_Đây là tài liệu nằm giữa tài liệu HDSD và tài liệu thiết kế kệ thống, người đọc là các developer_
_Mục đích để developer nắm được là muốn làm việc này thì cần sửa chỗ nào, đọc code ở đâu_

## **I.Giới thiệu chung**

### **1. Mục đích tài liệu**

Tài liệu mô tả chi tiết các quy trình, chức năng, kiến trúc kỹ thuật nhằm hướng dẫn và bàn giao người khác một cách nhanh chóng

### **2. Đối tượng sử dụng tài liệu**

| STT | Người sử dụng       | Vai trò |
| --- | ------------------- | ------- |
| 1   | Người bàn giao      |         |
| 2   | Người nhận bàn giao |         |
| 3   | Team lead, PM       |         |

### **3. Thuật ngữ và chữ viết tắt**

| STT | Thuật ngữ/chữ viết tắt | Mô tả |
| --- | ---------------------- | ----- |
| 1   | Project                | Dự án |

## **II. Giới thiệu về hệ thống/dự án/sản phẩm**

### **1. Tổng quan hệ thống**

- Hệ thống Workflow Server StandAlone là hệ thống quản lý quy trình tập trung tích hợp và sử dụng sản phẩm của: https://workflowengine.io/pricing/.
- Workflow Engine StandAlone đáp ứng các tính năng sẵn có của Workflow Engine(https://workflowengine.io/features/) và dựa trên ý tưởng phiên bản server của Workflow Engine đã tự custom và thêm một số chức năng như tích hợp biểu mẫu, file gán vào tài liệu …
- Workflow Server StandAlone hỗ trợ các dự án dễ dàng tạo và thiết kế quy trình cho một đối tượng nào đó trong hệ thống mà không phải mất công fix code.

### **2. Kiến trúc và các công nghệ sử dụng**

- Hỗ trợ OS: Phu thuộc hỗ trợ của WorkflowEngine
- SourceControl: Git(Gitlab) : https://gitlab.com/SAVIS-GIT/wf.core.git
- Mô hình: SOA
- Framework: .NetCore 2.2(Backend) và AngularJs 1.7(FrontEnd)
- Cơ sở dữ liệu: SQLServer
- Công nghệ đi kèm:
  • Workflow Engine 3.5 (https://workflowengine.io/)
  • Formly (http://angular-formly.com/#!/)
  • Docker, DockerSwarm, Grunt, Yarn

## **III. Cài đặt ứng dụng**

### **1. Tải về source code**

```console
$ git clone https://gitlab.com/SAVIS-GIT/wf.core.git
```

### **2. Build & deployment trên môi trường development**

- Chạy API: kiểm tra tại : http://localhost:5000

```console
$ cd MISBI.API
$ dotnet run
```

- Chạy FE: sử dụng thư viện http-server của node, kiểm tra tại : http://localhost:5002

```console
$ npm i -g http-server
```

```console
$ cd MISBI.FE
$ npm install
$ http-server -p 5002
```

- Chạy ứng dụng sample tích hợp callback bên thứ 3, kiểm tra tại : http://localhost:6000

```console
$ cd MISBI.3rdApp
$ dotnet run
```

- Chạy ứng dụng sample tích hợp iframe, kiểm tra tại : http://localhost:6002

```console
$ cd MISBI.IframeIntegrated\Html5
$ http-server -p 6002
```

- Các script hỗ trợ migrate database
  - Tạo một migration
  ```console
     $ cd MISBI.Data
     $ dotnet ef --startup-project ../Core.API/ migrations add <MigateName>
  ```
  - Áp dụng migration
  ```console
     $ cd MISBI.Data
     $ dotnet ef --startup-project ../Core.API/ database update
  ```
  - Tạo một sript migration
  ```console
     $ cd MISBI.Data
     $ dotnet ef --startup-project ../Core.API/ migrations script  --output "./2.MigrateScript.sql"
  ```

### **3. Build & deployment trên môi trường production**

- **Môi trường docker**: cần chuẩn bị môi trường hỗ trợ docker, docker-compose (https://www.docker.com/get-started)
  ````console
     $ docker-compose up
   ```
  Kiểm tra tại: http://localhost:3004. Tài khoản đăng nhập mặc định: Admin/1Qaz2wsx
  ````
- **Môi trường window**: cần chuẩn bị môi trường hỗ trợ netcore2.2, sqlserver( cấu hình kết nối ở file appseting.production.json), IIS, Yarn(Dùng để build)
  - B1. Build: Netcore2.2 SDK, Yarn . Dữ liệu build thành công ở thư mục ./dist
  ```console
     $ cd MISBI.Builder
     $ ./build.bat
  ```
  - B2. Deploy: Netcore 2.2 Runtime, IIS
    - Cấu hình site deploy IIS theo hướng dẫn tại: https://docs.microsoft.com/en-us/aspnet/core/host-and-deploy/iis/?view=aspnetcore-2.2
    - Sao chép dữ liệu deploy vào thư mục deploy
    ```console
      $ cp dist <DeployFolder>
    ```
- **Môi trường linux**: cần chuẩn bị môi trường hỗ trợ netcore2.2, sqlserver(cấu hình kết nối ở file appseting.production.json), Nginx, Yarn(Dùng để build)
  - B1. Build: Netcore2.2 SDK, Yarn . Dữ liệu build thành công ở thư mục ./dist
  ```console
     $ cd MISBI.Builder
     $ ./build.bat
  ```
  - B2. Deploy: Netcore 2.2 Runtime, Nginx
    - Cấu hình site deploy Nginx theo hướng dẫn tại: https://docs.microsoft.com/en-us/aspnet/core/host-and-deploy/linux-nginx?view=aspnetcore-2.2
    - Sao chép dữ liệu deploy vào thư mục deploy
    ```console
      $ cp dist <DeployFolder>
    ```

### **4. Kết quả sau khi cài đặt**

- Giao diện ứng dụng
  ![ProjectImage](Core.Markdown/project.png)

- Hệ thống API Endpoint Swagger
  Truy cập vào địa chỉ swagger: /swagger/index.html hoặc import vào postman qua json:
  ![SwaggerImage](Core.Markdown/swagger.png)

## **IV. Phát triển hệ thống**

### **1. Giới thiệu cấu trúc Source code**

 <details><summary>Xem chi tiết</summary>

```javascript
|-- wf.core
    |-- .gitlab-ci.yml // ci-cd gitlab
    |-- docker-compose.yml
    |-- Dockerfile // Dockerfile project API
    |-- MISBI.3rdApp // Project sample tích hợp với hệ thống bên thứ 3
    |-- MISBI.IframeIntegrated //Project chứa các sample về tích hợp thông qua iframe.
    |   |-- Html5 // Html5 sample
    |-- MISBI.Builder // Thư mục chứa script build và deploy hệ thống
    |   |-- build.api.bat //build api window
    |   |-- build.api.sh //build api linux
    |   |-- build.bat //build toàn app window
    |   |-- build.sh //build toàn app linux
    |-- MISBI.API //Project dùng để phơi ra các service API
    |   |-- appsettings.json //file cấu hình
    |   |-- appsettings.cisco.json //file cấu hình deploy cisco cloud
    |   |-- appsettings.docker.json // file cấu hình deploy docker
    |   |-- appsettings.production.json //file cấu hình production
    |   |-- appsettings.test.json //file cấu hình XUnitTest
    |   |-- ProjectServiceCollectionExtensions.cs //Class đăng ký service DI
    |   |-- web.config // file cấu hình deploy IIS
    |   |-- AuthenConfig
    |   |   |-- CustomAuth.cs // Middleware áp dụng authencation
    |   |-- License
    |   |   |-- Devart.Data.Oracle.key //License sử dụng provider oracle
    |   |-- Properties
    |   |   |-- launchSettings.json // file cấu hình launch
    |   |-- System
    |   |   |-- Controllers // Thư mục chứa các controller
    |   |   |   |-- Bsd //Nghiệp vụ chung
    |   |   |   |   |-- Form
    |   |   |   |   |   |-- FormControl //Control biểu mẫu
    |   |   |   |   |   |-- FormMaster // Biểu mẫu
    |   |   |   |   |-- Navigation //Menu sidebar
    |   |   |   |   |-- Parameter //Tham số hệ thống
    |   |   |   |-- Idm //Phân quyền
    |   |   |   |   |-- Right //Quyền
    |   |   |   |   |-- Role //Nhóm người dùng
    |   |   |   |   |-- User //Người dùng
    |   |   |   |-- Sys //Hệ thống
    |   |   |   |   |-- CacheController.cs //Cache hệ thống
    |   |   |   |   |-- WSO2Controller.cs //Tích hợp với Wso2
    |   |   |   |   |-- Application // Ứng dụng
    |   |   |   |   |-- Authentication //Xác thực
    |   |   |   |-- Upload //Upload
    |   |   |   |-- Workflow //Workflow
    |   |   |       |-- DesignerControler.cs // Thiết kế quy trình
    |   |   |       |-- WorkflowApplication // Quản lý Ứng dụng bên thứ 3
    |   |   |       |   |-- WorkflowApplicationController.cs
    |   |   |       |-- WorkflowCustom // Endpoint workflow
    |   |   |       |   |-- WorkflowCustomController.cs
    |   |   |       |   |-- WorkflowCustomModel.cs
    |   |   |       |-- WorkflowDocument //Tài liệu gán với quy trình
    |   |   |       |   |-- WorkflowDocumentController.cs
    |   |   |       |-- WorkflowDocumentAttacment // File đính kèm tài liệu
    |   |   |       |   |-- WorkflowDocumentAttacmentController.cs
    |   |   |       |   |-- WorkflowDocumentAttacmentControllerModels.cs
    |   |   |       |-- WorkflowGlobalParameter // Tham số quy trình (luồng logic bussiness)
    |   |   |       |   |-- WorkflowGlobalParameterController.cs
    |   |   |       |-- WorkflowScheme //Quản lý các mẫu thiết kế quy trình
    |   |   |           |-- WorkflowSchemeController.cs
    |   |   |           |-- WorkflowSchemeControllerModel.cs
    |   |-- wwwroot // Folder deploy static web
    |-- MISBI.Business // Thư mục chứa phần nghiệp vụ, logic dự án
    |   |-- System
    |       |-- Bsb
    |       |-- Config
    |       |   |-- Generic //Hàm chung CRUD
    |       |       |-- BaseModel.cs
    |       |       |-- DbHandler.cs
    |       |-- Form
    |       |-- Idm
    |       |-- Sys
    |       |-- Workflow
    |-- MISBI.Data //Project dùng để tương tác database và các function “dùng chung”
    |   |-- Migrations // Các changelog dự án
    |   |-- System
    |       |-- EFDataAccess
    |       |   |-- BaseTable.cs //Model bảng chung
    |       |   |-- DataContext.cs // File cấu hình contex kết nối tới database
    |       |   |-- Contex // Các model map với bảng trong database
    |       |-- Utils // Các hàm, constant tái sử dụng
    |           |-- AutoMapperUtils.cs
    |           |-- Constant.cs
    |           |-- ExtensionMethods.cs
    |           |-- Random.cs
    |           |-- RequestData.cs
    |           |-- ResponseData.cs
    |           |-- Utils.cs
    |-- MISBI.FE //Project chứa frontend sử dụng angularjs
    |   |-- Gruntfile.js //File grunt cấu hình build
    |   |-- package.build.json // package định nghĩa phục vụ việc build
    |   |-- package.prod.json // package định nghĩa phục vụ việc chạy production
    |   |-- app-data
    |   |   |-- components
    |   |   |   |-- config
    |   |   |   |   |-- config-auth.js // Cấu hình authencation
    |   |   |   |   |-- config-route.js // Cấu hình route
    |   |   |   |-- constants
    |   |   |   |   |-- constant-app.js // Cấu hình constant ứng dụng
    |   |   |   |-- directives // Cấu hình directives custom
    |   |   |   |   |-- checklist-model.js
    |   |   |   |   |-- directives-common.js
    |   |   |   |   |-- jwplayer-directive.js
    |   |   |   |   |-- ng-ckeditor.js
    |   |   |   |   |-- timer-directive.js
    |   |   |   |-- env // Cấu hình môi trường
    |   |   |   |   |-- env.docker.js //Cấu hình môi trường deploy docker
    |   |   |   |   |-- env.js
    |   |   |   |   |-- env.production.js //Cấu hình môi trường deploy production
    |   |   |   |-- formly-template //Cấu hình biểu mẫu động
    |   |   |   |-- services // Cấu hình các service
    |   |   |       |-- service-amd.js
    |   |   |       |-- service-api.js // service map với các endpoint API
    |   |   |-- mandatory-js
    |   |   |   |-- app.js //Định nghĩa khởi động dự án áp dụng angular-ADM
    |   |   |   |-- main.js // Định nghĩa thư viện sử dụng requiedJs
    |   |   |-- views //Các màn hình module project
    |   |       |-- core
    |   |       |-- template // Các view hệ thống hoặc dùng chung
    |   |-- assets // Chứa css, image,plugin project
    |   |   |-- workflow // thư mục cấu hình hiển thị màn hình thiết kế workflow
    |   |       |-- js
    |   |       |   |-- workflowdesigner.js // file js cấu hình
    |   |       |-- Localization
    |   |           |-- readme.txt
    |   |           |-- workflowdesigner.localization_vi.js // File ngôn ngữ
    |-- MISBI.WF  //Project dùng để tương tác với thư viện workflow engine
    |   |-- DataAccess
    |   |   |-- IPersistenceProviderContainer.cs
    |   |   |-- Implementation
    |   |       |-- PersistenceProviderContainer.cs
    |   |-- Workflow
    |       |-- ActionProvider.cs // Implement viết Action
    |       |-- RuleProvider.cs // Implement viết Ruke
    |       |-- WorkflowInit.cs // Cấu hình sử dụng Engine WF. Cấu hình license
    |       |-- Handler
    |           |-- IWorkflowHandler.cs
    |           |-- WorkflowHandler.cs
    |-- MISBI.XUnitTest //Project dùng để test những class trong phần Business để xem ta có viết đúng hay chưa
    |   |-- API
    |   |   |-- PositionApiTest.cs // Ví dụ áp dụng test 1 service
    |   |-- Config
    |   |   |-- TestClientProvider.cs
    |-- Docker.mssql // project tạo docker cho database mssql
        |-- ...
```

</details>

### **2. Giới thiệu các phân hệ chính**

- Module multi application

  Module phục vụ việc cấu hình đa ứng dụng

  - Concept
    - Mỗi bảng, module thông thường được gán với một thuộc tính : ApplicationId.
    - Hỗ trợ việc map bảng động theo ApplicationId. Cụ thể với hệ thống bảng nghiệp vụ mdm có cấu trúc : `mdm_<Tên bảng>_<Mã ứng dụng>`
    - Khi thao tác với unitOfWork ta contructer `new UnitOfWork(MdmHelper.MakePrefix(appId)`

- Module xác thực

  Module hỗ trợ việc xác thực và cấu hình xác thực cho hệ thống API

  - Vị trí source code

  ```console
   MISBI.API/appsettings.json
   MISBI.API/System/Controllers/Sys/Authentication/*
   MISBI.API/System/Controllers/Sys/WSO2Controller.cs
   MISBI.API/AuthenConfig/*
  ```

  - Các bảng liên quan tới module

  ```console
   bsd_User
  ```

  - Concept
    - Hệ thống xác thực. Áp dụng một số xác thực thông dụng
  - Vị trí cấu hình : appsettings.json

  ```javascript
  "Authentication": {
    "Jwt": { // Phương thức JWT
      "Enable": "true",
      "Key": "SAVIS_SECRET_KEY",
      "Issuer": "SAVIS CORP",
      "TimeToLive": "1000"
    },
    "Facebook": { // Thông qua FB
      "AppId": "809227039429796",
      "AppSecret": "77f5db41295a3be0c8885811344a5e68"
    },
    "Google": { // Thông qua GG
      "ClientId": "286268379854-34o8hvd2he19hipa50up83l9tg3da5dm.apps.googleusercontent.com",
      "ClientSecret": "-ecAsh59qKefZJaBwrV2S5lC"
    },
    "Microsoft": { // Thông qua MS
      "ApplicationId": "",
      "Password": ""
    },
    "Twitter": { // Thông qua Twitter
      "ConsumerKey": "",
      "ConsumerSecret": ""
    },
    "Basic": { // Phương thức Basic
      "Enable": "false"
    },
    "WSO2": { // Thông qua WSO2
      "Enable": "false",
      "Uri": "http://210.245.26.132:8839/",
      "Clientid": "IGRbTgcRVTfSNPxeD7LNcCV4iQka",
      "Secret": "tJ7No33ebTY0TGZeRa2k5rUyQUAa",
      "Redirecturi": "http://localhost:5002/index.html"
    },
    "AdminUser": "Admin", //Tài khỏan admin mặc định
    "AdminPassWord": "1Qaz2wsx",
    "GuestUser": "Guest", //Tài khỏan user mặc định
    "GuestPassWord": "1Qaz2wsx"
  }
  ```

* Module phân quyền

  Module phục vụ viện định nghĩa và phân quyền chức năng cho người dùng hệ thống.
  Module gồm 3 đối tượng Right(Quyền), Role(Nhóm người dùng), User(Người dùng)
  ![erd-phanquyen](Core.Markdown/erd-phanquyen.png)

  - Vị trí source code

  ```console
   MISBI.API/System/Controllers/Idm/*
   MISBI.Business/System/Idm/*
   MISBI.FE/app-data/views/core/right/*
   MISBI.FE/app-data/views/core/role/*
   MISBI.FE/app-data/views/core/user/*
   MISBI.FE/app-data/views/core/right-of-user/*
  ```

  - Các bảng liên quan tới module

  ```console
   idm_Right
   idm_RightMapRole
   idm_RightMapUser
   idm_Role
   idm_User
   idm_UserMapRole
  ```

  - Concept
    - Nhóm người dùng chứa nhiều người dùng
    - Nhóm người dùng chứa nhiều quyền
    - Người dùng chứa nhiều quyền riêng
    - Quyền của người dùng = quyền riêng + quyền kế thừa từ nhóm.
    - Quyền riêng > quyền kế thừa

* Module điều hướng

  Module phục vụ việc hiển thị và phân quyền menu chức năng điều hướng.
  Module gồm 3 đối tượng Navigation(Điều hướng), Role(Nhóm người dùng), User(Người dùng)
  ![erd-dieuhuong](Core.Markdown/erd-dieuhuong.png)

  - Vị trí source code

  ```console
   MISBI.API/System/Controllers/Bsd/Navigation/*
   MISBI.Business/System/Bsb/Navigation/*
   MISBI.FE/app-data/views/core/navigation/*
   MISBI.FE/app-data/views/template/sidebar/*
  ```

  - Các bảng liên quan tới module

  ```console
   bsd_Navigation
   bsd_Navigation_Map_Role
   idm_Role
   idm_User
   idm_UserMapRole
  ```

  - Concept
    - Điều hướng có cơ chế cha/con phân cấp
    - Một nhóm được phân truy cập nhiều điều hướng
    - Quyền truy cập của người dùng được kế thừa quyền truy cập của nhóm người dùng chứa nó .

* Module tham số hệ thống

  Module phục vụ việc cấu hình và quản lý động các tham số hệ thống trên csdl.
  Module gồm 1 đối tượng Parameter(Tham số hệ thống)
  ![thamsohethong](Core.Markdown/thamsohethong.png)

  - Vị trí source code

  ```console
   MISBI.API/System/Controllers/Bsd/Parameter/*
   MISBI.Business/System/Bsb/Parameter/*
   MISBI.FE/app-data/views/core/parameter/*
  ```

  - Các bảng liên quan tới module

  ```console
   bsd_Parameter
  ```

  - Concept
    - Quản lý tham sô hệ thống.
    - Hàm truy cập lấy giá trị một cấu hình là : `ParameterCollection.Instance.GetValue("<Tên tham số>")`

* Module Biểu mẫu
  - Vị trí source code
    ```console
    $ MISBI.API\System\Controllers\Bsd\Form\*
    $ MISBI.Business\System\Form\*
    $ MISBI.Data\System\EFDataAccess\Contex\Bsd\Form\*
    $ MISBI.FE\app-data\views\core\workflow\form\*
    ```
  - Các bảng liên quan tới module
    ```
    bsd_FormControl
    bsd_FormMaster
    bmd_<Tên bảng động>
    ```
  - Quản lý biểu mẫu: cho phép tạo và thiết kế các biểu mẫu nhằm làm form nhập metadata cho tài liệu trong quy trình
    ![Form](Core.Markdown/form.png)
    - Concept:
      - Áp dụng công nghệ Formly (http://angular-formly.com/#/)
      - Một biểu mẫu gồm một bảng động gán dữ liệu
      - Một biểu mẫu có 1 danh sách các mẫu in để hiển thị dữ liệu
      - Một biểu mẫu được thiết kế có các control và HTML
      - Một biểu mẫu có thể có 0 hoặc nhiều biểu mẫu con
      - Biểu mẫu con chỉ chứa control của biểu mẫu cha
      - Biểu mẫu con lưu dữ liệu vào bảng của biểu mẫu cha
  - Quản lý trường tin: cho phép tạo và thiết kế các trường tin nhằm tái sử dụng dễ dàng hơn trong việc thiết kế form biểu mẫu
    - Concept:
      - Trường tin là 1 loại control trong form
      - Trường tin có thể là input text, input number, select, upload, radio .. ..
* Module Quy trình

  - Quản lý scheme: cho phép tạo và thiết kế luồng quy trình
    ![Scheme](Core.Markdown/scheme.png)

    - Concept:
      - Áp dụng công nghệ WorkflowEngine (https://workflowengine.io/documentation)
      - Có 1 hoặc nhiều quy trình trong hệ thống
      - Quy trình có điểm bắt đầu , kết thúc các bước chuyển theo chuẩn BPM v2

  - Quản lý ứng dụng tích hợp: cho phép tạo và cấu hình các ứng dụng kết nối tới hệ thống. Cấu hình callback khi bắt sự kiện chuyển quy trình
    ![3rdapp](Core.Markdown/3rdapp.png)
    - Concept:
      - Một tài liệu sử dụng quy trình sẽ được khai báo gán với một ứng dụng tích hợp
      - Ứng dụng tích hợp sẽ phơi ra một endpoint API cho phép hệ thống gọi callback khi tài liệu được xử lý
      - Mỗi ứng dụng sẽ được cấp 1 API token dùng để gọi các API của hệ thống ở bên trên. Để sử dụng đầu mỗi request HTTP gọi đến API hệ thống chèn một header có dạng : APIKEY <Token được cấp>
  - Quản lý luồng logic: cho phép tạo và cấu hình các luồng logic nhằm hiển thị biểu mẫu khi xử lý quy trình tài liệu
    ![bussinessflow](Core.Markdown/bussinessflow.png)
    - Concept:
      - Một luồng logic được gán với một quy trình
      - Chứa một biểu mẫu mặc định nhằm lưu trữ dữ liệu metadata mặc định
      - Có thể cấu hình chọn biểu mẫu hiển thị (Là biểu mẫu mặc định hoặc các biểu mẫu con của nó ) ở các bước khác nhau ứng với Actor khác nhau
  - Danh sách xử lý: cho phép tạo và quản lý các tài liệu xử lý vào quy trình
    ![wfdocument](Core.Markdown/wfdocument.png)
    - Concept:
      - Một tài liệu sẽ gán với 1 quy trình, 1 ứng dụng tích hợp, 1 luồng logic
      - Sử dụng bảng của biểu mẫu mặc định lưu metadata của từng tài liệu
      - Tài liệu vào quy trình sẽ hiển thị được lịch sử xử lý
      - Tài liệu vào quy trình hiển thị được biểu mẫu nhập ứng với từng actor và từng bước trong quy trình
      - Tài liệu vào quy trình có 1 danh sách file đính kèm (nếu có)
      - Có thể xem vị trí trực quan của tài liệu trong quy trình

### 3. Các database hỗ trợ

- SQL server

### 4. Phân hệ tích hợp

- Tích hợp qua Iframe
  - Concept:
    - Ứng dụng bên thứ 3 chỉ việc sử dụng Iframe sẽ hiển thị được thông tin chi tiết của một tài liệu trong quy trình
    - Trong Iframe ta cũng có các nút command nhằm xử lý tài liệu trong quy trình , các lịch sử xử lý , biểu mẫu nhập , file đính kèm …
  - Chạy thử demo
  ```console
  $ cd MISBI.IframeIntegrated\Html5
  $ http-server -p 6002
  ```
  - Iframe code sample src
    - http://localhost:5002/#!/shareexecute/FFDDAE5B-0035-4B56-B3B4-B8E405CF14FD/1/2/TVRJMVpHTTJPVE10WW1SbU1TMDBOREF5TFdFME9EQXRNRGhrTm1GaU5HVTNOR05o
    - #!/shareexecute/:id/:userid/:appid/:apikey
      - id: Id tài liệu cần xử lý
      - userid: Id người xử lý (Nhận diện actor)
      - appid: Id ứng dụng tích hợp
      - apiken: API token được cấp của ứng dụng tích hợp
- Tích hợp qua callback
  - Concept:
    - Ứng dụng bên bên thứ 3 sẽ phơi ra một enpoint nhằm hứng lấy sự kiện khi chuyển quy trình
    - Enpoint là 1 phương thức POST. Hệ thống sẽ truyền kèm theo thông tin xử lý bao gồm
    ```javascript
    {
        "ProcessId": "", // Id của process (tài liệu)
        "WorkflowDocumentUpdateInfoModel": {} // Thông tin update tài liệu
    }
    ```
  - Chạy thử demo
    ```console
      $ cd MISBI.3rdApp
      $ dotnet run
    ```
- Tích hợp vào dự án sẵn có
  - Concept:
    - Mục đích nhúng phần workflow engine vào dự án sẵn có. Từ đó có thể dễ dàng triển khai nghiệp vụ theo hướng database. Có thể query, join trực tiếp vào hệ thống bảng của WF
    - Dự án cần đáp ứng cùng sử dụng công nghệ .Netcore2.2 và database SQL server.
  - Các bước thực hiện
    - B1: Xóa các thông tin liên quan của workflow cũ (nếu có trong hệ thống)
    - B2: Thêm vào solution dự án project classlibary MISBI.WF từ solution WF.Core tại :
      https://gitlab.com/SAVIS-GIT/wf.core/tree/master/Core.WF
    - B3: Thêm reference MISBI.WF từ các project khác
    - B4: Thêm vào project Data các class model của WF tại:
      https://gitlab.com/SAVIS-GIT/wf.core/tree/master/Core.Data/System/EFDataAccess/Contex/Workflow
    - B5: Thêm vào project Bussiness các Handler tại:
      https://gitlab.com/SAVIS-GIT/wf.core/tree/master/Core.Business/System/Workflow
    - B6: Thêm vào project API các controller tại:
      https://gitlab.com/SAVIS-GIT/wf.core/tree/master/Core.API/System/Controllers/Workflow
    - B7: Cố gắng fix reference và các thứ liên quan

## **V. CÁC CÂU HỎI THƯỜNG GẶP (FAQS)**

- **Q**: Hệ thống có thể hỗ trợ chuẩn BPMv2 không?

- **A**: Lý thuyết có!. Nếu bạn không sử dụng các tính năng advance thì bạn có thể download/upload file quy trình dạng chuẩn BPMv2.

---

- **Q**: Hệ thống có thể hỗ trợ các loại database khác không?

- **A**: Lý thuyết có thể!. Nhưng hiện tại với project này chưa áp dụng đến. Có thể tự nghiên cứu thêm. Hiện tại wf engine hỗ trợ: MS SQL, PostgreSQL, Oracle, MySQL, MongoDB, Redis, RavenDB ,GridGain

---

- **Q**: Hệ thống có hỗ trợ quy trình song song không?

- **A**: Có! Có nhiều cách để áp dụng quy trình song song. Tôi đã bootstrap sample 1 scheme hỗ trợ quy trình song song.
  ![paralel](Core.Markdown/paralel.png)
  Khi đến vị trí Step3. Tôi tạo 2 transition auto tới SubProcess1 và Subprocess2 với thuộc tính "Is Fork" = true khi đó hệ thống sẽ tạo 2 quy trình con subprocess1 và subprocess2. Ta có thể chuyển quy trình như bình thường với 2 quy trình này.Và 1 transition auto tới WaitForSubProcess.

  Khi ở vị trí Subprocess1 và Subprocess2Pass tôi tạo một transition tới WaitForSubProcess command-auto với thuộc tích "Is Fork" = true và "Merge subprocess via set state" = false. Khi đó Subporcess sẽ kết thúc . Trigger vào state WaitForSubProcess check condition (nếu có)

  Khi ở vị trí Subprocess1 và Subprocess2 tôi tạo một transition tới Step1 command-auto với thuộc tích "Is Fork" = true và "Merge subprocess via set state" = true. Khi đó Subporcess sẽ kết thúc . Trigger vào state WaitForSubProcess check condition (nếu có) và state mới hệ thống là Step1 và các subprocess khác cũng bị xóa bỏ.

  Ở WaitForSubProcess tôi tạo 1 transition auto-condition với Condition là Action : CheckSubProcess. Action này sẽ kiểm tra xem hiện tại tài liệu còn quy trình con nào không. Nếu ko còn sẽ tự động chuyển đến Final

  > Tài liệu sẽ phải đợi 2 subpocess kết thúc. Đây chính là quy trình song song

  ***

- **Q**: Tôi có thể "Việt hóa" và chỉnh sửa màn hình vẽ quy trình không?

- **A**: Có!Hệ thống tích hợp với màn hình quy trình thông qua lib tại:

  ```javascript
              new WorkflowDesigner({
                  name: 'simpledesigner',
                  apiurl: WFSchemeApiService.GetDesignerAPI(),
                  renderTo: 'wfdesigner',
                  imagefolder: 'assets/workflow/images/',
                  graphwidth: graphwidth,
                  graphheight: graphheight
              });
  //*****************/
  |-- MISBI.FE //Project chứa frontend sử dụng angularjs
  |   |-- assets // Chứa css, image,plugin project
  |   |   |-- workflow // thư mục cấu hình hiển thị màn hình thiết kế workflow
  |   |       |-- js
  |   |       |   |-- workflowdesigner.js // file js cấu hình
  |   |       |-- Localization
  |   |           |-- readme.txt
  |   |           |-- workflowdesigner.localization_vi.js // File ngôn ngữ
  ```

  Ta có thể sửa nội dung file `workflowdesigner.localization_vi.js` để "Việt hóa"

  Ta có thể sửa file `workflowdesigner.js` để chỉnh sửa màn hình vẽ. Tôi đã ví dụ việc đổi control từ input => dropdown.

  ```javascript
  ... line 4272
   }, {
              name: t.Culture,
              field: "Culture",
              // type: "input"
              type: "select",
              datasource: getCulture()
          }, {
            ...
  ```

  ***

- **Q**: Việc tích hợp thông qua Iframe liệu có không bảo mật?

- **A**: Vâng đó là một rủi ro!. Nhưng hiện tại tôi đã áp dụng phương pháp API-TOKEN nhằm giảm thiểu rủi ro

  - #!/shareexecute/:id/:userid/:appid/:apikey

    - id: Id tài liệu cần xử lý
    - userid: Id người xử lý (Nhận diện actor)
    - appid: Id ứng dụng tích hợp
    - apiken: API token được cấp của ứng dụng tích hợp

    Ngoài ra ta có thể áp dụng cho 2 hệ thống chung phương thức xác thực wso2 hoặc jwt. Bên src của iframe ta truyền token. Token dạng này chỉ có timetolive thấp nên khá an tòan

# Armchart

https://live.amcharts.com/new/edit/
