<details><summary>Cấu trúc source code</summary> 

```
|-- wf.core 
    |-- .gitlab-ci.yml // ci-cd gitlab  
    |-- docker-compose.yml 
    |-- Dockerfile // Dockerfile project API 
    |-- Core.3rdApp // Project sample tích hợp với hệ thống bên thứ 3 
    |   |-- appsettings.json 
    |   |-- Program.cs
    |   |-- Startup.cs 
    |   |-- Controllers
    |   |   |-- ValuesController.cs  
    |-- Core.IframeIntegrated //Project chứa các sample về tích hợp thông qua iframe.
    |   |-- Html5 // Html5 sample
    |       |-- index.html
    |-- Core.Builder // Thư mục chứa script build và deploy hệ thống
    |   |-- build.api.bat //build api window
    |   |-- build.api.sh //build api linux
    |   |-- build.bat //build toàn app window
    |   |-- build.sh //build toàn app linux
    |-- Core.API //Project dùng để phơi ra các service API   
    |   |-- appsettings.json //file cấu hình
    |   |-- appsettings.cisco.json //file cấu hình deploy cisco cloud
    |   |-- appsettings.docker.json // file cấu hình deploy docker
    |   |-- appsettings.production.json //file cấu hình production
    |   |-- appsettings.test.json //file cấu hình XUnitTest  
    |   |-- ProjectServiceCollectionExtensions.cs //Class đăng ký service DI  
    |   |-- web.config // file cấu hình deploy IIS
    |   |-- AuthenConfig
    |   |   |-- CustomAuth.cs // Middleware áp dụng authencation  
    |   |-- License
    |   |   |-- Devart.Data.Oracle.key //License sử dụng provider oracle    
    |   |-- Properties
    |   |   |-- launchSettings.json // file cấu hình launch 
    |   |-- System 
    |   |   |-- Controllers // Thư mục chứa các controller
    |   |   |   |-- Bsd //Nghiệp vụ chung
    |   |   |   |   |-- Form  
    |   |   |   |   |   |-- FormController.cs
    |   |   |   |   |   |-- FormControllerModels.cs
    |   |   |   |   |   |-- FormControl //Control biểu mẫu
    |   |   |   |   |   |   |-- FormControlController.cs
    |   |   |   |   |   |   |-- FormControlControllerModels.cs
    |   |   |   |   |   |-- FormMaster // Biểu mẫu
    |   |   |   |   |       |-- FormMasterController.cs
    |   |   |   |   |       |-- FormMasterControllerModels.cs
    |   |   |   |   |-- Navigation //Menu sidebar
    |   |   |   |   |   |-- NavigationController.cs
    |   |   |   |   |   |-- NavigationControllerModels.cs
    |   |   |   |   |-- Parameter //Tham số hệ thống
    |   |   |   |       |-- ParameterController.cs
    |   |   |   |       |-- ParameterControllerModels.cs
    |   |   |   |-- Idm //Phân quyền
    |   |   |   |   |-- Right //Quyền
    |   |   |   |   |   |-- RightController.cs
    |   |   |   |   |   |-- RightControllerModels.cs
    |   |   |   |   |-- Role //Nhóm người dùng
    |   |   |   |   |   |-- RoleController.cs
    |   |   |   |   |   |-- RoleControllerModels.cs
    |   |   |   |   |-- User //Người dùng
    |   |   |   |       |-- UserController.cs
    |   |   |   |       |-- UserControllerModels.cs
    |   |   |   |-- Sys //Hệ thống
    |   |   |   |   |-- CacheController.cs //Cache hệ thống
    |   |   |   |   |-- WSO2Controller.cs //Tích hợp với Wso2
    |   |   |   |   |-- Application // Ứng dụng
    |   |   |   |   |   |-- ApplicationController.cs
    |   |   |   |   |   |-- ApplicationControllerModels.cs
    |   |   |   |   |-- Authentication //Xác thực
    |   |   |   |       |-- AuthenticationController.cs
    |   |   |   |       |-- AuthenticationModel.cs
    |   |   |   |       |-- Facebook
    |   |   |   |           |-- FacebookModel.cs
    |   |   |   |-- Upload //Upload
    |   |   |   |   |-- NodeUploadController.cs
    |   |   |   |   |-- NodeUploadControllerModel.cs
    |   |   |   |-- Workflow //Workflow
    |   |   |       |-- DesignerControler.cs // Thiết kế quy trình
    |   |   |       |-- WorkflowApplication // Quản lý Ứng dụng bên thứ 3
    |   |   |       |   |-- WorkflowApplicationController.cs
    |   |   |       |-- WorkflowCustom // Endpoint workflow
    |   |   |       |   |-- WorkflowCustomController.cs
    |   |   |       |   |-- WorkflowCustomModel.cs
    |   |   |       |-- WorkflowDocument //Tài liệu gán với quy trình
    |   |   |       |   |-- WorkflowDocumentController.cs
    |   |   |       |-- WorkflowDocumentAttacment // File đính kèm tài liệu
    |   |   |       |   |-- WorkflowDocumentAttacmentController.cs
    |   |   |       |   |-- WorkflowDocumentAttacmentControllerModels.cs
    |   |   |       |-- WorkflowGlobalParameter // Tham số quy trình (luồng logic bussiness)
    |   |   |       |   |-- WorkflowGlobalParameterController.cs
    |   |   |       |-- WorkflowScheme //Quản lý các mẫu thiết kế quy trình
    |   |   |           |-- WorkflowSchemeController.cs
    |   |   |           |-- WorkflowSchemeControllerModel.cs 
    |   |-- wwwroot // Folder deploy static web
    |       |-- favicon.ico
    |       |-- humans.txt
    |       |-- robots.txt
    |       |-- swagger.css 
    |-- Core.Business // Thư mục chứa phần nghiệp vụ, logic dự án
    |   |-- System
    |       |-- Bsb
    |       |   |-- Navigation
    |       |   |   |-- DbNavigationHandler.cs
    |       |   |   |-- INavigationHanlder.cs
    |       |   |   |-- NavigationModels.cs
    |       |   |-- Parameter
    |       |       |-- IParameterHandler.cs
    |       |       |-- ParameterCollection.cs
    |       |       |-- ParameterHandler.cs
    |       |       |-- ParameterModel.cs
    |       |-- Config 
    |       |   |-- Generic //Hàm chung CRUD
    |       |       |-- BaseModel.cs
    |       |       |-- DbHandler.cs
    |       |-- Form
    |       |   |-- FormHandler.cs
    |       |   |-- FormModel.cs
    |       |   |-- Helper.cs
    |       |   |-- IFormHandler.cs
    |       |   |-- FormControl
    |       |   |   |-- FormControlHandler.cs
    |       |   |   |-- FormControlModel.cs
    |       |   |   |-- IFormControlHandler.cs
    |       |   |-- FormMaster
    |       |       |-- FormMasterHandler.cs
    |       |       |-- FormMasterModel.cs
    |       |       |-- IFormMasterHandler.cs
    |       |-- Idm
    |       |   |-- Helper.cs
    |       |   |-- Right
    |       |   |   |-- IRightHandler.cs
    |       |   |   |-- RightCollection.cs
    |       |   |   |-- RightHandler.cs
    |       |   |   |-- RightModel.cs
    |       |   |-- RightMapRole
    |       |   |   |-- IRightMapRoleHandler.cs
    |       |   |   |-- RightMapRoleHandler.cs
    |       |   |-- RightMapUser
    |       |   |   |-- IRightMapUserHandler.cs
    |       |   |   |-- RightMapUserHandler.cs
    |       |   |-- Role
    |       |   |   |-- IRoleHandler.cs
    |       |   |   |-- RoleCollection.cs
    |       |   |   |-- RoleHandler.cs
    |       |   |   |-- RoleModel.cs
    |       |   |-- User
    |       |   |   |-- IUserHandler.cs
    |       |   |   |-- UserCollection.cs
    |       |   |   |-- UserHandler.cs
    |       |   |   |-- UserModel.cs
    |       |   |-- UserMapRole
    |       |       |-- IUserMapRoleHandler.cs
    |       |       |-- UserMapRoleHandler.cs
    |       |-- Sys
    |       |   |-- Application
    |       |       |-- ApplicationCollection.cs
    |       |       |-- ApplicationHandler.cs
    |       |       |-- ApplicationModel.cs
    |       |       |-- IApplicationHandler.cs
    |       |-- Workflow
    |           |-- Workflow
    |           |   |-- Models
    |           |       |-- BusinessFlowResult.cs
    |           |       |-- BussinessFlow.cs
    |           |       |-- Command.cs
    |           |       |-- CommandParameter.cs
    |           |       |-- ExecuteCommandInfo.cs
    |           |       |-- ExecuteCommandResult.cs
    |           |       |-- Form.cs
    |           |       |-- GetCommandsOperationResult.cs
    |           |       |-- GetSchemeListResult.cs
    |           |       |-- GetStatesOperationResult.cs
    |           |       |-- HistoryItem.cs
    |           |       |-- InboxItem.cs
    |           |       |-- InboxResult.cs
    |           |       |-- IsExistOperationResult.cs
    |           |       |-- OperationResult.cs
    |           |       |-- OutboxItem.cs
    |           |       |-- OutboxResult.cs
    |           |       |-- ProcessIds.cs
    |           |       |-- ProcessInfo.cs
    |           |       |-- ProcessInfoOperationResult.cs
    |           |       |-- ProcessInstance.cs
    |           |       |-- ProcessParameters.cs
    |           |       |-- State.cs
    |           |       |-- Transition.cs
    |           |-- WorkflowApplication
    |           |   |-- IWorkflowApplicationHandler.cs
    |           |   |-- WorkflowApplicationHandler.cs
    |           |   |-- WorkflowApplicationModel.cs
    |           |-- WorkflowDocument
    |           |   |-- IWorkflowDocumentHandler.cs
    |           |   |-- WorkflowDocumentHandler.cs
    |           |   |-- WorkflowDocumentModel.cs
    |           |-- WorkflowDocumentAttacment
    |           |   |-- IWorkflowDocumentAttacmentHandler.cs
    |           |   |-- WorkflowDocumentAttacmentHandler.cs
    |           |   |-- WorkflowDocumentAttacmentModel.cs
    |           |-- WorkflowDocumentHistory
    |           |   |-- IWorkflowDocumentHistoryHandler.cs
    |           |   |-- WorkflowDocumentHistoryHandler.cs
    |           |   |-- WorkflowDocumentHistoryModel.cs
    |           |-- WorkflowGlobalParameter
    |           |   |-- IWorkflowGlobalParameterHandler.cs
    |           |   |-- WorkflowGlobalParameterHandler.cs
    |           |   |-- WorkflowGlobalParameterModel.cs
    |           |-- WorkflowScheme
    |               |-- IWorkflowSchemeHandler.cs
    |               |-- WorkflowSchemeHandler.cs
    |               |-- WorkflowSchemeModel.cs
    |-- Core.Data //Project dùng để tương tác database và các function “dùng chung” 
    |   |-- Migrations // Các changelog dự án
    |   |   |-- 20190311183304_StartProject.cs
    |   |   |-- 20190311183304_StartProject.Designer.cs
    |   |   |-- 20190314031334_RunScript.cs
    |   |   |-- 20190314031334_RunScript.Designer.cs
    |   |   |-- 20190314184031_AddWFHis.cs
    |   |   |-- 20190314184031_AddWFHis.Designer.cs
    |   |   |-- 20190315042511_FixParamete.cs
    |   |   |-- 20190315042511_FixParamete.Designer.cs
    |   |   |-- 20190317141107_FixMode.cs
    |   |   |-- 20190317141107_FixMode.Designer.cs
    |   |   |-- 20190317163149_Application.cs
    |   |   |-- 20190317163149_Application.Designer.cs
    |   |   |-- 20190318030302_changeStatus.cs
    |   |   |-- 20190318030302_changeStatus.Designer.cs
    |   |   |-- 20190318063518_deleteCasade.cs
    |   |   |-- 20190318063518_deleteCasade.Designer.cs
    |   |   |-- 20190320071628_AddAttachment.cs
    |   |   |-- 20190320071628_AddAttachment.Designer.cs
    |   |   |-- DataContextModelSnapshot.cs
    |   |-- System
    |       |-- EFDataAccess
    |       |   |-- BaseTable.cs //Model bảng chung
    |       |   |-- DataContext.cs // File cấu hình contex kết nối tới database 
    |       |   |-- Contex // Các model map với bảng trong database
    |       |   |   |-- Bsd
    |       |   |   |   |-- BsdFormTemplate.cs
    |       |   |   |   |-- BsdNavigation.cs
    |       |   |   |   |-- BsdNavigationMapRole.cs
    |       |   |   |   |-- BsdParameter.cs
    |       |   |   |   |-- Form
    |       |   |   |       |-- BsdFormControl.cs
    |       |   |   |       |-- BsdFormMaster.cs
    |       |   |   |-- Idm
    |       |   |   |   |-- IdmRight.cs
    |       |   |   |   |-- IdmRightMapRole.cs
    |       |   |   |   |-- IdmRightMapUser.cs
    |       |   |   |   |-- IdmRole.cs
    |       |   |   |   |-- IdmUser.cs
    |       |   |   |   |-- IdmUserMapRole.cs
    |       |   |   |-- Sys
    |       |   |   |   |-- SysApplication.cs
    |       |   |   |-- Workflow
    |       |   |       |-- WorkflowApplication.cs
    |       |   |       |-- WorkflowDocument.cs
    |       |   |       |-- WorkflowDocumentAttacment.cs
    |       |   |       |-- WorkflowDocumentHistory.cs
    |       |   |       |-- WorkflowGlobalParameter.cs
    |       |   |       |-- WorkflowInbox.cs
    |       |   |       |-- WorkflowProcessInstance.cs
    |       |   |       |-- WorkflowProcessInstancePersistence.cs
    |       |   |       |-- WorkflowProcessInstanceStatus.cs
    |       |   |       |-- WorkflowProcessScheme.cs
    |       |   |       |-- WorkflowProcessTimer.cs
    |       |   |       |-- WorkflowProcessTransitionHistory.cs
    |       |   |       |-- WorkflowScheme.cs
    |       |   |-- Repositories // Repositories pattern
    |       |       |-- DatabaseFactory.cs
    |       |       |-- Disposable.cs
    |       |       |-- IDatabaseFactory.cs
    |       |       |-- IRepository.cs
    |       |       |-- IUnitOfWork.cs
    |       |       |-- Repository.cs
    |       |       |-- UnitOfWork.cs
    |       |-- Utils // Các hàm, constant tái sử dụng
    |           |-- AutoMapperUtils.cs
    |           |-- Constant.cs
    |           |-- ExtensionMethods.cs
    |           |-- Random.cs
    |           |-- RequestData.cs
    |           |-- ResponseData.cs
    |           |-- Utils.cs
    |-- Core.FE //Project chứa frontend sử dụng angularjs
    |   |-- favicon.ico
    |   |-- Gruntfile.js //File grunt cấu hình build
    |   |-- index.html 
    |   |-- package.backup.json
    |   |-- package.build.json // package định nghĩa phục vụ việc build
    |   |-- package.json
    |   |-- package.prod.json // package định nghĩa phục vụ việc chạy production
    |   |-- yarn.lock  
    |   |-- app-data
    |   |   |-- components
    |   |   |   |-- config
    |   |   |   |   |-- config-auth.js // Cấu hình authencation
    |   |   |   |   |-- config-route.js // Cấu hình route
    |   |   |   |-- constants
    |   |   |   |   |-- constant-app.js // Cấu hình constant ứng dụng
    |   |   |   |-- directives // Cấu hình directives custom
    |   |   |   |   |-- checklist-model.js
    |   |   |   |   |-- directives-common.js
    |   |   |   |   |-- jwplayer-directive.js
    |   |   |   |   |-- ng-ckeditor.js
    |   |   |   |   |-- timer-directive.js
    |   |   |   |-- env // Cấu hình môi trường  
    |   |   |   |   |-- env.docker.js //Cấu hình môi trường deploy docker
    |   |   |   |   |-- env.js 
    |   |   |   |   |-- env.production.js //Cấu hình môi trường deploy production
    |   |   |   |-- formly-template //Cấu hình biểu mẫu động
    |   |   |   |   |-- formly-custom.css
    |   |   |   |   |-- formly-factory.js
    |   |   |   |   |-- formly-template.js
    |   |   |   |   |-- formly.css
    |   |   |   |   |-- control
    |   |   |   |   |   |-- ag-grid.html
    |   |   |   |   |   |-- check-box-multiple-tree.html
    |   |   |   |   |   |-- check-box-multiple.html
    |   |   |   |   |   |-- check-box.html
    |   |   |   |   |   |-- datepicker.html
    |   |   |   |   |   |-- input.html
    |   |   |   |   |   |-- radio.html
    |   |   |   |   |   |-- text-area.html
    |   |   |   |   |   |-- timepicker.html
    |   |   |   |   |   |-- toggle.html
    |   |   |   |   |   |-- ui-select-multiple.html
    |   |   |   |   |   |-- ui-select-tree-multiple.html
    |   |   |   |   |   |-- ui-select-tree.html
    |   |   |   |   |   |-- ui-select.html
    |   |   |   |   |   |-- upload-multi.html
    |   |   |   |   |   |-- upload.html
    |   |   |   |   |-- wraper
    |   |   |   |       |-- color.html
    |   |   |   |       |-- error-message.html
    |   |   |   |       |-- horizontalLabel.html
    |   |   |   |       |-- horizotal.html
    |   |   |   |       |-- layout.html
    |   |   |   |       |-- loader.html
    |   |   |   |       |-- range.html
    |   |   |   |       |-- table.html
    |   |   |   |-- services // Cấu hình các service
    |   |   |       |-- service-amd.js 
    |   |   |       |-- service-api.js // service map với các endpoint API
    |   |   |-- mandatory-js
    |   |   |   |-- app.js //Định nghĩa khởi động dự án áp dụng angular-ADM
    |   |   |   |-- main.js // Định nghĩa thư viện sử dụng requiedJs
    |   |   |-- views //Các màn hình module project
    |   |       |-- core
    |   |       |   |-- navigation
    |   |       |   |   |-- navigation.html
    |   |       |   |   |-- navigation.js
    |   |       |   |-- right
    |   |       |   |   |-- right-list.html
    |   |       |   |   |-- right-list.js
    |   |       |   |-- right-of-user
    |   |       |   |   |-- right-of-user.html
    |   |       |   |   |-- right-of-user.js
    |   |       |   |-- role
    |   |       |   |   |-- role-list.html
    |   |       |   |   |-- role-list.js
    |   |       |   |   |-- role-modal-item.html
    |   |       |   |-- user
    |   |       |   |   |-- change-password.html
    |   |       |   |   |-- user-item.html
    |   |       |   |   |-- user.html
    |   |       |   |   |-- user.js
    |   |       |   |-- workflow
    |   |       |       |-- application
    |   |       |       |   |-- application-item.html
    |   |       |       |   |-- application-item.js
    |   |       |       |   |-- application.html
    |   |       |       |   |-- application.js
    |   |       |       |-- bussiness-flow
    |   |       |       |   |-- bussiness-flow-item.html
    |   |       |       |   |-- bussiness-flow-item.js
    |   |       |       |   |-- bussiness-flow.html
    |   |       |       |   |-- bussiness-flow.js
    |   |       |       |-- document
    |   |       |       |   |-- document-item.html
    |   |       |       |   |-- document-item.js
    |   |       |       |   |-- document.html
    |   |       |       |   |-- document.js
    |   |       |       |   |-- parameter-dialog.html
    |   |       |       |-- form
    |   |       |       |   |-- formly
    |   |       |       |   |   |-- formly-edit-control.html
    |   |       |       |   |   |-- formly-edit-control.js
    |   |       |       |   |   |-- formly-edit-label.html
    |   |       |       |   |   |-- formly-edit-label.js
    |   |       |       |   |   |-- formly-edit-row.html
    |   |       |       |   |   |-- formly-edit-row.js
    |   |       |       |   |   |-- formly.html
    |   |       |       |   |   |-- formly.js
    |   |       |       |   |-- formly-control
    |   |       |       |   |   |-- formly-control.html
    |   |       |       |   |   |-- formly-control.js
    |   |       |       |   |-- formly-data
    |   |       |       |   |   |-- formly-data-item.html
    |   |       |       |   |   |-- formly-data-item.js
    |   |       |       |   |   |-- formly-data.html
    |   |       |       |   |   |-- formly-data.js
    |   |       |       |   |-- formly-master
    |   |       |       |       |-- formly-master.html
    |   |       |       |       |-- formly-master.js
    |   |       |       |-- scheme
    |   |       |           |-- scheme.html
    |   |       |           |-- scheme.js
    |   |       |-- template // Các view hệ thống hoặc dùng chung
    |   |           |-- confirm
    |   |           |   |-- approve-dialog.html
    |   |           |   |-- connect-expired.html
    |   |           |   |-- info-dialog.html
    |   |           |   |-- input-dialog.html
    |   |           |   |-- workflow-dialog.html
    |   |           |-- footer
    |   |           |   |-- footer.html
    |   |           |   |-- footer.js
    |   |           |-- header
    |   |           |   |-- header.html
    |   |           |   |-- header.js
    |   |           |-- login
    |   |           |   |-- login.css
    |   |           |   |-- login.html
    |   |           |   |-- login.js
    |   |           |-- page-error
    |   |           |   |-- page-error.css
    |   |           |   |-- page-error.html
    |   |           |   |-- page-error.js
    |   |           |-- pagination
    |   |           |   |-- pagination.html
    |   |           |-- shareexecute
    |   |           |   |-- shareexecute.html
    |   |           |   |-- shareexecute.js
    |   |           |-- shareform
    |   |           |   |-- shareform.html
    |   |           |   |-- shareform.js
    |   |           |-- sidebar
    |   |           |   |-- sidebar.html
    |   |           |   |-- sidebar.js
    |   |           |-- site-access
    |   |           |   |-- site-access.html
    |   |           |   |-- site-access.js
    |   |           |-- touchkeybroad
    |   |               |-- keyboard.html
    |   |               |-- keyboard.js
    |   |-- assets // Chứa css, image,plugin project
    |   |   |-- workflow // thư mục cấu hình hiển thị màn hình thiết kế workflow
    |   |       |-- js
    |   |       |   |-- workflowdesigner.js // file js cấu hình
    |   |       |-- Localization
    |   |           |-- readme.txt 
    |   |           |-- workflowdesigner.localization_vi.js // File ngôn ngữ
    |-- Core.WF  //Project dùng để tương tác với thư viện workflow engine
    |   |-- DataAccess
    |   |   |-- IPersistenceProviderContainer.cs
    |   |   |-- Implementation
    |   |       |-- PersistenceProviderContainer.cs
    |   |-- Workflow
    |       |-- ActionProvider.cs // Implement viết Action
    |       |-- RuleProvider.cs // Implement viết Ruke
    |       |-- WorkflowInit.cs // Cấu hình sử dụng Engine WF. Cấu hình license
    |       |-- Handler
    |           |-- IWorkflowHandler.cs
    |           |-- WorkflowHandler.cs
    |-- Core.XUnitTest //Project dùng để test những class trong phần Business để xem ta có viết đúng hay chưa 
    |   |-- API
    |   |   |-- PositionApiTest.cs // Ví dụ áp dụng test 1 service
    |   |-- Config
    |   |   |-- TestClientProvider.cs
    |-- Docker.mssql // project tạo docker cho database mssql
        |-- CreatePersistenceObjects.sql
        |-- DropPersistenceObjects.sql
        |-- DropPersistenceObjects.sql
        |-- Dockerfile
```

</details>